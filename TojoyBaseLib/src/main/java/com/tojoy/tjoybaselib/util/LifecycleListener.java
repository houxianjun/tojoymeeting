package com.tojoy.tjoybaselib.util;

public interface LifecycleListener {
    void onShow();

    void onHide();

    void onBackToDesktop();
}
