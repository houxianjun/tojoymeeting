package com.tojoy.tjoybaselib.util.sys;

import android.app.Activity;
import android.util.Log;

import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;

import java.util.Iterator;
import java.util.Stack;

/**
 * Created by chengyanfang on 2018/3/13.
 */


public class ActivityStack {
    private static Stack<Activity> activityStack;
    private static ActivityStack instance;

    private ActivityStack() {
    }

    public static ActivityStack getActivityStack() {
        if (instance == null) {
            instance = new ActivityStack();
        }
        return instance;
    }

    public static Stack<Activity> getActivityStackList() {
        return activityStack;
    }

    public boolean isEmpty() {
        if (activityStack == null) {
            return true;
        }
        return activityStack.isEmpty();
    }


    // 退出栈顶Activity
    private void popActivity(Activity activity) {
        if (activity != null) {
            activity.finish();
        }
    }

    public void popAcitivityAndReomve(Activity activity){
        if (activity != null) {
            activity.finish();
            activityStack.remove(activity);
        }
    }
    /**MeetingSearchAct
     * 获得当前栈顶Activity
     */
    public Activity currentActivity() {
        Activity activity = activityStack.lastElement();
        return activity;
    }

    /**
     * 将当前Activity推入栈中
     */
    public void pushActivity(Activity activity) {
        if (activityStack == null) {
            activityStack = new Stack<>();
        }
        activityStack.add(activity);
    }


    public void popAllActivityExceptOne(Class cls) {
        if (activityStack != null) {
            for (int i = 0; i < activityStack.size(); i++) {
                if (activityStack.get(i) != null
                        && !activityStack.get(i).getClass().equals(cls)) {

                    popActivity(activityStack.get(i));
                }
            }

            for (int i = 0; i < activityStack.size(); i++) {
                if (activityStack.get(i) != null
                        && !activityStack.get(i).getClass().equals(cls)) {
                    activityStack.remove(i);
                }
            }
        }
    }

    /**
     * 关闭出指定class以外的activity,并移除站内所有的element
     * @param cls
     */
    public void popAllActivity(Class cls) {
        try {
        if (activityStack != null) {
            //不关闭指定的activity
            for (int i = 0; i < activityStack.size(); i++) {
                if(cls==null){
                    popActivity(activityStack.get(i));
                }
                else{
                    if (activityStack.get(i) != null
                            && !activityStack.get(i).getClass().equals(cls)) {
                        popActivity(activityStack.get(i));
                    }
                }
            }
            //全部清除栈
            activityStack.removeAllElements();
            //Log.d("activityStack","activityStack.size()"+activityStack.size());
         }
        }catch (Exception e){

        }
    }

    /**
     * 判断输入的类是否在栈顶
     * @param name
     * @return
     */
    public boolean isStackTopActivity(String  name){
        boolean isTop = false;
        try {

            int lastIndex=activityStack.size()-1;
            if (activityStack != null) {

                if (activityStack.get(lastIndex) != null
                        && activityStack.get(lastIndex).getClass().getName().equals(name)) {
                    isTop=true;
                }
            }
        }catch (Exception e){

        }
        return isTop;

    }
}