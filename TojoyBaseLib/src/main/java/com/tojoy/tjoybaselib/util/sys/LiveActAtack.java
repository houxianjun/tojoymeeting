package com.tojoy.tjoybaselib.util.sys;

import android.app.Activity;

import java.util.Stack;

public class LiveActAtack {
    private static Stack<Activity> activityStack;
    private static LiveActAtack instance;

    private LiveActAtack() {

    }

    public static LiveActAtack getActivityStack() {
        if (instance == null) {
            instance = new LiveActAtack();
        }
        return instance;
    }

    public boolean isEmpty() {
        if (activityStack == null) {
            return true;
        }
        return activityStack.isEmpty();
    }


    // 退出栈顶Activity
    private void popActivity(Activity activity) {
        if (activity != null) {
            activity.finish();
        }
    }


    /**
     * 将当前Activity推入栈中
     */
    public void pushActivity(Activity activity) {
        if (activityStack == null) {
            activityStack = new Stack<>();
        }
        activityStack.add(activity);
    }


    public void popAllActivity() {
        if (activityStack != null) {
            for (int i = 0; i < activityStack.size(); i++) {
                if (activityStack.get(i) != null) {
                    popActivity(activityStack.get(i));
                }
            }

            for (int i = 0; i < activityStack.size(); i++) {
                if (activityStack.get(i) != null) {
                    activityStack.remove(i);
                }
            }
        }
    }


}