package com.tojoy.tjoybaselib.util.storage;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;

import java.io.File;

/**
 * Created by chengyanfang on 2017/9/7.
 */

public class AppFileHelper {

    private static final String ROOT_PATH = "razerdp/github/friendcircle/";
    private static final String DATA_PATH = ROOT_PATH + ".data/";
    private static final String CACHE_PATH = ROOT_PATH + ".cache/";
    private static final String PIC_PATH = ROOT_PATH + ".pic/.nomedia/";
    private static final String LOG_PATH = ROOT_PATH + ".log/";
    private static final String TEMP_PATH = ROOT_PATH + ".temp/";

    private static String storagePath;

    public static void initStoryPath(Context context) {
        if (TextUtils.isEmpty(storagePath)) {
            //storagePath = FileUtil.getStoragePath(AppContext.getAppContext(), FileUtil.hasSDCard());
            //因为外置sd卡似乎无法写入，所以写到内置sd卡
            storagePath = StorageUtil.getStoragePath(context, false);
            if (TextUtils.isEmpty(storagePath)) {
                storagePath = Environment.getExternalStorageDirectory().getAbsolutePath();
                if (TextUtils.isEmpty(storagePath)) {
                    storagePath = context.getFilesDir().getAbsolutePath();
                }
            }
        }

        storagePath = StorageUtil.checkFileSeparator(storagePath);

        File rootDir = new File(storagePath.concat(ROOT_PATH));
        checkAndMakeDir(rootDir);

        File dataDir = new File(storagePath.concat(DATA_PATH));
        checkAndMakeDir(dataDir);

        File cacheDir = new File(storagePath.concat(CACHE_PATH));
        checkAndMakeDir(cacheDir);

        File picDir = new File(storagePath.concat(PIC_PATH));
        checkAndMakeDir(picDir);

        File logDir = new File(storagePath.concat(LOG_PATH));
        checkAndMakeDir(logDir);

        File tempDir = new File(storagePath.concat(TEMP_PATH));
        checkAndMakeDir(tempDir);

    }

    private static void checkAndMakeDir(File file) {
        if (!file.exists()) {
            file.mkdirs();
        }
    }
}
