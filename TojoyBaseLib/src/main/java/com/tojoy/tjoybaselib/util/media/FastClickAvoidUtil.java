package com.tojoy.tjoybaselib.util.media;


import android.util.Log;

/**
 * create by fanxi
 * create on 2019/4/17
 * description 防止控件快速点击
 */
public class FastClickAvoidUtil {
    private static long lastClickTime;
    private static int lastButtonId = -1;
    /**
     * 连续点击间隔时间
     */
    private final static int SPACE_TIME = 400;

    public synchronized static boolean isDoubleClick() {
        long currentTime = System.currentTimeMillis();
        boolean isClickDouble;
        if (currentTime - lastClickTime > SPACE_TIME) {
            isClickDouble = false;
        } else {
            isClickDouble = true;
        }
        lastClickTime = currentTime;
        return isClickDouble;
    }

    /**
     * 自定义间隔时间
     */
    public synchronized static boolean isDoubleClick(int spaceTime) {
        long currentTime = System.currentTimeMillis();
        boolean isClickDouble;
        if (currentTime - lastClickTime > spaceTime) {
            isClickDouble = false;
        } else {
            isClickDouble = true;
        }
        lastClickTime = currentTime;
        return isClickDouble;
    }

    /**
     * 判断两次点击的间隔，如果小于diff，则认为是多次无效点击
     *
     * @param diff
     * @return
     */
    public static boolean isFastDoubleClick(int buttonId, long diff) {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if (lastButtonId == buttonId && lastClickTime > 0 && timeD < diff) {
            Log.v("isFastDoubleClick", "短时间内按钮多次触发");
            return true;
        }
        lastClickTime = time;
        lastButtonId = buttonId;
        return false;
    }
    public static final int H5_JUMP_ID = 10001;
    /**
     * 判断两次点击的间隔，如果小于1000，则认为是多次无效点击
     *
     * @return
     */
    public static boolean isFastDoubleClick(int buttonId) {
        return isFastDoubleClick(buttonId, 1000);
    }

}
