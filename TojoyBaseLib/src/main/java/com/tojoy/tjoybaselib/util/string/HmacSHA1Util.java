package com.tojoy.tjoybaselib.util.string;

import android.text.TextUtils;
import android.util.Log;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.TreeMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * author: hp.long
 * create: 2019-07-17 19:57
 */
public class HmacSHA1Util {

    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";
    public static final String KEY = "ba3ea0bf8eabc48b";

    /**
     * 使用 HMAC-SHA1 签名方法对data进行签名
     *
     * @param data 被签名的字符串
     * @param key  密钥
     * @return 加密后的字符串
     */
    private static String genHMAC(String data, String key) {
        byte[] result = null;
        try {
            //根据给定的字节数组构造一个密钥,第二参数指定一个密钥算法的名称
            SecretKeySpec signinKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);
            //生成一个指定 Mac 算法 的 Mac 对象
            Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            //用给定密钥初始化 Mac 对象
            mac.init(signinKey);
            //完成 Mac 操作
            byte[] rawHmac = mac.doFinal(data.getBytes());
            result = Base64.encodeBase64(rawHmac);

        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            System.err.println(e.getMessage());
        }
        if (null != result) {
            return new String(result);
        } else {
            return null;
        }
    }

    /**
     * 构建签名
     *
     * @return
     */
    public static String buildSign(JSONObject dataJson, String secret) {
        Map<String, Object> params;
        StringBuilder sb = new StringBuilder();
        try {
            params = JsonMapUtil.parseOptions(dataJson);
            TreeMap<String, String> p = new TreeMap(params);

            sb.append(secret);
            for (Map.Entry<String, String> entry : p.entrySet()) {
                if (entry.getValue() != null && !TextUtils.isEmpty(String.valueOf(entry.getValue()))) {
                    sb.append(entry.getKey()).append("=").append(String.valueOf(entry.getValue())).append("&");
                }
            }
            if (sb.indexOf("&") != -1) {
                sb.deleteCharAt(sb.length() - 1);
            }
            sb.append(secret);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (dataJson != null) {
            Log.d("OM-OkHttp-sign-befor", dataJson.toString());
        }

        return HmacSHA1Util.genHMAC(sb.toString(), secret);
    }

}
