package com.tojoy.tjoybaselib.util.sys;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.text.TextUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.Locale;

/**
 * Created by chengyanfang on 2017/9/5.========
 */

public class DeviceInfoUtil {

    private final static String LANGUAGE_TW = "30";
    private final static String LANGUAGE_ZH = "31";
    private final static String LANGUAGE_EN = "1";

    /**
     * 系统语言 根据协议 需要上传阿拉伯数字
     *
     * @return 1表示手机显示语言为非中文。31表示手机语言为中文。30为繁体中文。
     */
    public static String getMobileLanguage() {
        String language = Locale.getDefault().getLanguage();
        if (language.equalsIgnoreCase("zh")) {
            // 中文
            return LANGUAGE_ZH;
        } else if (language.equalsIgnoreCase("en")) {
            // 英文
            return LANGUAGE_EN;
        } else if (language.equalsIgnoreCase("tw")) {
            language = LANGUAGE_TW;
        }
        return LANGUAGE_EN;
    }

    public static void getSerialNum(Application application) {
        android.telephony.TelephonyManager tm = (android.telephony.TelephonyManager) application.getSystemService(Context.TELEPHONY_SERVICE);
    }

    /**
     * 获取机器ID
     */
    @SuppressLint("HardwareIds")
    public static String getTojoyDeviceId(Context context) {
        String tojoydeviceidSerialnum = "";
        if (context != null) {
            if (TextUtils.isEmpty(tojoydeviceidSerialnum)) {
                return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
            return tojoydeviceidSerialnum + "-" + Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return tojoydeviceidSerialnum;
    }

    /**
     * 系统语言是否是英文
     *
     * @return true or false
     */
    public static boolean isEnglish() {
        try {
            if ("en".equalsIgnoreCase(Locale.getDefault().getLanguage())) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    /***
     * 返回手机型号
     *
     * @return 手机型号
     * */
    public static String getPhoneMODEL() {
        return android.os.Build.MODEL;
    }

    /**
     * 取设备厂商名称
     *
     * @return 设备厂商名称
     */
    public static String getMANUFACTURER() {
        return android.os.Build.MANUFACTURER;
    }

    public static String phoneVerionCode() {
        return "" + android.os.Build.VERSION.SDK_INT;
    }

    public static String getCpuModel() {
        String cputype = getFileCpuInfo();
        if (null == cputype) {
            cputype = getCpuModelFromRun();
        }
        if (null != cputype) {
            String[] tmpstr = cputype.split(":");
            if (2 >= tmpstr.length) {
                cputype = tmpstr[1];
            }
        }
        return cputype;
    }

    private static String getCpuModelFromRun() {
        Runtime runtime = Runtime.getRuntime();
        Process process;
        try {
            process = runtime.exec("/system/bin/cat /proc/cpuinfo");
            process.waitFor();
        } catch (Exception e) {
            return null;
        }
        InputStream in = process.getInputStream();
        BufferedReader boy = new BufferedReader(new InputStreamReader(in));
        String mystring;
        try {
            mystring = boy.readLine();
            while (mystring != null) {
                if (mystring.toLowerCase().contains("hardware")) {
                    return mystring;
                }
                mystring = boy.readLine();
            }
        } catch (IOException e) {
        }
        return null;
    }

    private static String getFileCpuInfo() {
        String cmdCpuinfo = "/proc/cpuinfo";
        String cpuhardware;
        try {
            FileReader fr = new FileReader(cmdCpuinfo);
            BufferedReader localBufferedReader = new BufferedReader(fr, 8192);
            while ((cpuhardware = localBufferedReader.readLine()) != null) {
                if (cpuhardware.toLowerCase().contains("hardware")) {
                    localBufferedReader.close();
                    return cpuhardware;
                }
            }
            localBufferedReader.close();
        } catch (IOException e) {
        }
        return null;
    }

    public static boolean isEmulator() {
        Runtime runtime = Runtime.getRuntime();
        Process process;
        try {
            process = runtime.exec("/system/bin/cat /proc/cpuinfo");
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return false;
        }

        InputStream in = process.getInputStream();
        BufferedReader boy = new BufferedReader(new InputStreamReader(in));
        String mystring;
        try {
            mystring = boy.readLine();
            while (mystring != null) {
                mystring = mystring.trim().toLowerCase();
                if ((mystring.startsWith("hardware")) && mystring.endsWith("goldfish")) {
                    return true;
                }
                mystring = boy.readLine();
            }
        } catch (IOException e) {
            return false;
        }
        return false;
    }

}
