package com.tojoy.tjoybaselib.util.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * @author chengyanfang
 * @version V 1.0.0
 * @function 网络管理工具
 */

public class NetWorkUtils {

    public static void checkNet(Context context){
        if(!isNetworkConnected(context)) {
            Toast.makeText(context, "网络异常", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * @param context
     * @return 是否连网
     */
    public static boolean isNetworkConnected(Context context) {
        try {
         if (context != null) {
            ConnectivityManager mConnectivityManager =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
             assert mConnectivityManager != null;
             NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable();
            }
         }
        }catch (Exception ignored){

        }
        return false;
    }

}
