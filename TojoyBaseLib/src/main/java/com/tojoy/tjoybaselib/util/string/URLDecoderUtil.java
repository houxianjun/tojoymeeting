package com.tojoy.tjoybaselib.util.string;

import android.text.TextUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * @author qll
 * @date 2019/12/18
 * URLDecoder转译工具类
 */
public class URLDecoderUtil {

    public static String getDecodeString (String data,String enc){
        String result = "";
        if (!TextUtils.isEmpty(data)){
            try {
                // 防止要转译的文字里面有100%等文字
                String replace = data.replaceAll("%(?![0-9a-fA-F]{2})","%25");
                result = URLDecoder.decode(replace, enc);
            } catch (UnsupportedEncodingException e){
                e.printStackTrace();
            }
        }
        return result;
    }
}
