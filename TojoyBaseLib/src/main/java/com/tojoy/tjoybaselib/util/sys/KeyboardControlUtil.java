package com.tojoy.tjoybaselib.util.sys;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.lang.ref.WeakReference;

/**
 * Created by chengyanfang on 2017/9/13.
 */

/**
 * funcs:
 * 1.监听键盘弹收、控制当前activity UI元素的显隐
 * 2.控制软键盘的显示隐藏
 * 3.保存/获取 键盘的高度
 */

public class KeyboardControlUtil {


    /**
     * =======  Observer Keyboard show & hide
     */

    private OnKeyboardStateChangeListener onKeyboardStateChangeListener;

    //使用弱引用，避免循环引用
    private WeakReference<Activity> act;

    private KeyboardControlUtil(Activity act, OnKeyboardStateChangeListener onKeyboardStateChangeListener) {
        this.act = new WeakReference<>(act);
        this.onKeyboardStateChangeListener = onKeyboardStateChangeListener;
    }

    public static void observerKeyboardVisibleChange(Activity act, OnKeyboardStateChangeListener onKeyboardStateChangeListener) {
        new KeyboardControlUtil(act, onKeyboardStateChangeListener).observerKeyboardVisibleChangeInternal();

    }


    private void setOnKeyboardStateChangeListener(OnKeyboardStateChangeListener onKeyboardStateChangeListener) {
        this.onKeyboardStateChangeListener = onKeyboardStateChangeListener;
    }

    public interface OnKeyboardStateChangeListener {
        void onKeyboardChange(int keyboardHeight, boolean isVisible);
    }


    private void observerKeyboardVisibleChangeInternal() {
        if (onKeyboardStateChangeListener == null) {
            return;
        }

        Activity activity = act.get();
        if (activity == null) {
            return;
        }

        setOnKeyboardStateChangeListener(onKeyboardStateChangeListener);
        final View decorView = activity.getWindow().getDecorView();

        decorView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            int preKeyboardHeight = -1;
            Rect rect = new Rect();
            boolean preVisible = false;

            @Override
            public void onGlobalLayout() {
                rect.setEmpty();
                decorView.getWindowVisibleDisplayFrame(rect);
                int displayHeight = rect.height();
                int windowHeight = decorView.getHeight();
                int keyboardHeight = windowHeight - displayHeight;

                if (preKeyboardHeight != keyboardHeight) {
                    //判定可见区域与原来的window区域占比是否小于0.75,小于意味着键盘弹出来了。
                    boolean isVisible = (displayHeight * 1.0f / windowHeight * 1.0f) < 0.75f;
                    if (isVisible != preVisible) {
                        onKeyboardStateChangeListener.onKeyboardChange(keyboardHeight, isVisible);
                        preVisible = isVisible;
                    }
                }

                preKeyboardHeight = keyboardHeight;
            }
        });
    }


    /***
     * =======  Control Keyboard show & hide
     */
    public static void openKeyboard(EditText mEditText, Context mContext) {
        if (!mEditText.hasFocus()) {
            mEditText.requestFocus();
        }
        InputMethodManager imm = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.showSoftInput(mEditText, InputMethodManager.RESULT_SHOWN);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static void closeKeyboard(EditText mEditText, Context mContext) {
        InputMethodManager imm = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(mEditText.getWindowToken(), 0);
    }

    public static void hideKeyboard(View v) {
        if (v == null) {
            return;
        }
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        if (imm.isActive()) {
            imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
        }
    }


    /**
     * =======  Get/Set Keyboard height (include landspace and vertical)
     */

    public static int getKeyboardHeight(Context context) {
        SharedPreferences sp = context.getSharedPreferences("vhall", Context.MODE_PRIVATE);
        return sp.getInt("keyboard_height", 0);
    }

    public static void setKeyboardHeight(Context context, int height) {
        if (context == null) {
            return;
        }

        SharedPreferences sp = context.getSharedPreferences("vhall", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("keyboard_height", height);
        editor.commit();
    }

    public static int getKeyboardHeightLandspace(Context context) {
        SharedPreferences sp = context.getSharedPreferences("vhall", Context.MODE_PRIVATE);
        return sp.getInt("keyboard_height_landspace", 0);
    }

    public static void setKeyboardHeightLandspace(Context context, int height) {
        SharedPreferences sp = context.getSharedPreferences("vhall", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("keyboard_height_landspace", height);
        editor.commit();
    }

}
