package com.tojoy.tjoybaselib.util.storage;

import android.webkit.CookieManager;


/**
 * Created by chengyanfang on 2018/1/7.
 */

public class CacheUtils {

    /**
     * 清除Webview的CookieCache
     *
     */
    public static void clearAllCache() {
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
    }
}
