package com.tojoy.tjoybaselib.util.media;


import com.tojoy.tjoybaselib.util.sys.ScreenUtil;

/**
 * Created by chengyanfang on 2017/9/25.
 *
 * @function:音频文件控制
 */

public class AudioBubbleUtil {

    public static int getAudioMaxEdge() {
        return (int) (0.6 * ScreenUtil.screenMin);
    }

    public static int getAudioMinEdge() {
        return (int) (0.1875 * ScreenUtil.screenMin);
    }
}
