package com.tojoy.tjoybaselib.util;

public interface ResumedListener {
    void onResumed();
}
