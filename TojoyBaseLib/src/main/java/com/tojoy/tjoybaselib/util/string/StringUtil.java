package com.tojoy.tjoybaselib.util.string;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.widget.TextView;

import com.tojoy.tjoybaselib.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

    /**
     * 删除字符串中的空白符
     *
     * @param content
     * @return String
     */
    public static String removeBlanks(String content) {
        if (content == null) {
            return null;
        }
        StringBuilder buff = new StringBuilder();
        buff.append(content);
        for (int i = buff.length() - 1; i >= 0; i--) {
            if (' ' == buff.charAt(i) || ('\n' == buff.charAt(i)) || ('\t' == buff.charAt(i))
                    || ('\r' == buff.charAt(i))) {
                buff.deleteCharAt(i);
            }
        }
        return buff.toString();
    }

    /**
     * 获取32位uuid
     */
    public static String get32UUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static boolean isEmpty(String input) {
        return TextUtils.isEmpty(input);
    }

    /**
     * 生成唯一号
     */
    public static String get36UUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
    /**
     * counter ASCII character as one, otherwise two
     *
     * @param str
     * @return count
     */
    public static int counterChars(String str) {
        // return
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            int tmp = (int) str.charAt(i);
            if (tmp > 0 && tmp < 127) {
                count += 1;
            } else {
                count += 2;
            }
        }
        return count;
    }


    public static boolean noEmpty(String originStr) {
        return !TextUtils.isEmpty(originStr);
    }

    /**
     * 获取带标准逗号的数字字符串
     */
    public static String getInsertedString(String originalString) {
        String dollStr = "";

        // 先去除小数点
        int dollIndex = originalString.indexOf(".");
        if (dollIndex > 0) {
            dollStr = originalString.substring(dollIndex, originalString.length());
            originalString = originalString.substring(0, dollIndex);
        }

        String tempString = "";
        int length = originalString.length();
        for (int i = length; i > 0; i--) {
            tempString = tempString + originalString.substring(i - 1, i);
            int index = length - i;
            if (index != 0 && (index + 1) % 3 == 0 && index + 1 != length) {
                tempString = tempString + ",";
            }
        }
        String insertedString = "";
        for (int i = tempString.length(); i > 0; i--) {
            insertedString = insertedString + tempString.substring(i - 1, i);
        }
        return insertedString + dollStr;
    }

    /**
     * 获取带标准逗号的数字字符串
     */
    public static SpannableString getInsertedString(String originalString, float proportion) {
        SpannableString ss1 = new SpannableString(originalString);
        ss1.setSpan(new RelativeSizeSpan(proportion), ss1.length() - 3, ss1.length(), 0);
        return ss1;
    }

    /**
     * 获取标准两位小数  Float
     */
    public static String getFormatedFloatString(String originalString) {
        try {
            double amount = Double.parseDouble(originalString);
            DecimalFormat df = new DecimalFormat("##0.00");
            return df.format(amount);
        } catch (Exception e) {
        }
        return "0.00";
    }


    public static String getFormatedIntString(String originalString) {
        int dollIndex = originalString.indexOf(".");
        String intStr = "";
        if (dollIndex > 0) {
            intStr = originalString.substring(0, dollIndex);
        }

        return intStr;
    }

    public static String getFormatedMinString(String originalString) {
        int dollIndex = originalString.indexOf(".");
        String dollStr;
        if (dollIndex > 0) {
            dollStr = originalString.substring(dollIndex, originalString.length());
        } else {
            return "";
        }
        return dollStr;
    }


    /**
     * 设置关键字颜色高亮
     *
     * @param context
     * @param left
     * @param right
     * @param content
     * @param textView
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void setColorText(Context context, int left, int right, String content, TextView textView) {
        SpannableString spannableString = new SpannableString(content);
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(ContextCompat.getColor(context, R.color.color_3A9AF4));
        spannableString.setSpan(colorSpan, left, right, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(spannableString);
    }

    /**
     * 字符串是否包含手机号
     */
    public static boolean isStringContainsPhone(String str) {

        if (TextUtils.isEmpty(str)) {
            return false;
        }

        // 过滤出纯数字
        str = Pattern.compile("[^0-9]").matcher(str.trim()).replaceAll("");
        if (str.length() != 11) {
            return false;
        }
        char[] chars = str.toCharArray();
        //所有11位数字的集合
        ArrayList<String> phoneList = new ArrayList<>();
        for (int i = 0; i < chars.length; i++) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int j = 0; j < 11; j++) {
                if (i + j < chars.length) {
                    stringBuilder.append(chars[i + j]);
                }
            }
            if (stringBuilder.length() == 11) {
                phoneList.add(stringBuilder.toString());
            }
        }

        List<String> regexList = new ArrayList<String>();
        /**
         * 手机号码
         * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
         * 联通：130,131,132,152,155,156,185,186
         * 电信：133,1349,153,180,189,181(增加)
         */
        regexList.add("^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$");
        /**
         * 中国移动：China Mobile
         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
         * 新增145 147
         */
        regexList.add("^1(34[0-8]|(3[5-9]|5[017-9]|8[2378]|4[57])\\d)\\d{7}$");
        /**
         * 中国联通：China Unicom
         * 130,131,132,152,155,156,185,186
         * 新增173，175，176，177，178，179  、 165、166
         */
        regexList.add("^1(3[0-2]|5[256]|6[56]|8[56]|7[35-9])\\d{8}$");
        /**
         * 中国电信：China Telecom
         * 133,1349,153,180,189,181(增加)
         * 新增191、198、199
         */
        regexList.add("^1((33|53|8[019]|9[189])[0-9]|349)\\d{7}$");
        for (String phone : phoneList) {
            for (String regex : regexList) {
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(phone);
                if (matcher.matches()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 字符串是否是链接
     */
    public static boolean isHttpUrl(String urls) {
        boolean isurl = false;
        String regex = "(((https|http)?://)?([a-z0-9]+[.])|(www.))"
                + "\\w+[.|\\/]([a-z0-9]{0,})?[[.]([a-z0-9]{0,})]+((/[\\S&&[^,;\u4E00-\u9FA5]]+)+)?([.][a-z0-9]{0,}+|/?)";//设置正则表达式
        //比对
        Pattern pat = Pattern.compile(regex.trim());
        if (!TextUtils.isEmpty(urls)) {
            Matcher mat = pat.matcher(urls.trim());
            isurl = mat.matches();
            //判断是否匹配
            return isurl;
        }
        if (!TextUtils.isEmpty(urls)) {
            String s = urls.trim();
            return s.startsWith("http：") || s.startsWith("https：") || s.startsWith("HTTP：") || s.startsWith("HTTPS：");
        }
        return false;
    }

    /**
     * 计算 MD5
     */
    public static String md5(String string) {
        if (TextUtils.isEmpty(string)) {
            return "";
        }
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
            byte[] bytes = md5.digest(string.getBytes());
            String result = "";
            for (byte b : bytes) {
                String temp = Integer.toHexString(b & 0xff);
                if (temp.length() == 1) {
                    temp = "0" + temp;
                }
                result += temp;
            }
            return result;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 根据fraction值来计算当前的颜色。
     */
    public static int getCurrentColor(float fraction, int startColor, int endColor) {
        int redCurrent;
        int blueCurrent;
        int greenCurrent;
        int alphaCurrent;

        int redStart = Color.red(startColor);
        int blueStart = Color.blue(startColor);
        int greenStart = Color.green(startColor);
        int alphaStart = Color.alpha(startColor);

        int redEnd = Color.red(endColor);
        int blueEnd = Color.blue(endColor);
        int greenEnd = Color.green(endColor);
        int alphaEnd = Color.alpha(endColor);

        int redDifference = redEnd - redStart;
        int blueDifference = blueEnd - blueStart;
        int greenDifference = greenEnd - greenStart;
        int alphaDifference = alphaEnd - alphaStart;

        redCurrent = (int) (redStart + fraction * redDifference);
        blueCurrent = (int) (blueStart + fraction * blueDifference);
        greenCurrent = (int) (greenStart + fraction * greenDifference);
        alphaCurrent = (int) (alphaStart + fraction * alphaDifference);

        return Color.argb(alphaCurrent, redCurrent, greenCurrent, blueCurrent);
    }

    public static SpannableString setDifferentTextSize(String msg,float sizeSpan,int startIndex,int endIndex){
        SpannableString spanStr = new SpannableString(msg);
        spanStr.setSpan(new RelativeSizeSpan(sizeSpan), startIndex, endIndex, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        return spanStr;
    }

    public static SpannableString setDifferentTextColor(String msg,int differentColor,int startIndex,int endIndex){
        SpannableString spannableString = new SpannableString(msg);
        spannableString.setSpan(new ForegroundColorSpan(differentColor), startIndex, endIndex, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        return spannableString;
    }

}