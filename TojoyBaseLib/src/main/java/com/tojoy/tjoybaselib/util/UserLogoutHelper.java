package com.tojoy.tjoybaselib.util;

import com.tojoy.tjoybaselib.BaseLibKit;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.model.meetingperson.SingleTonModel.MeetingPersonTon;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.KickOutLiveHelper;
import com.tojoy.tjoybaselib.util.sys.ActivityStack;

import org.greenrobot.eventbus.EventBus;

import rx.Observer;

public class UserLogoutHelper {

    /**
     *
     * @param isPushUnBind 是否需要调用解绑接口,只有在正常退出登录时传true,在被踢或者跳转着不需要解绑
     * @param cls 保留activity不被关闭，需要在回调中关闭该activity。传null则关闭栈中全部的活动activity
     * @param loginOutListener 回调
     */
    public static void loginOutClearCache(boolean isPushUnBind,Class cls,LoginOutListener loginOutListener){
        if(isPushUnBind){
            //个推解绑
            OMAppApiProvider.getInstance().pushUnBind(BaseUserInfoCache.getClientId(BaseLibKit.getContext()), new Observer<OMBaseResponse>() {
                @Override
                public void onCompleted() {
                    doLogout();
                }

                @Override
                public void onError(Throwable e) {


                    doLogout();
                }

                @Override
                public void onNext(OMBaseResponse baseResponse) {
                }
            });
        }

        KickOutLiveHelper.doLiveSomething(BaseLibKit.getContext(), new KickOutLiveHelper.KickOutLiveCallBack(){

            @Override
            public void OnSuccess() {

                clearCache(cls);
                loginOutListener.loginOutSuccess();
            }

            @Override
            public void OnFail() {

            }
        });

    }

    private static void doLogout() {

        OMAppApiProvider.getInstance().logout(new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(OMBaseResponse omBaseResponse) {


            }
        });

    }


    public static void clearCache(Class cls){
        BaseUserInfoCache.logout();
        AppConfig.KICK_OUT=false;
        AppConfig.isShowing = false;
        AppConfig.isLoginIM = false;
        MeetingPersonTon.getInstance().clear();
        ActivityStack.getActivityStack().popAllActivity(cls);
    }


    /**
     * 退出登录监听
     */
    public interface LoginOutListener{
        public void loginOutSuccess();
    }
}
