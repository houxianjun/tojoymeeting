package com.tojoy.tjoybaselib.util.storage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.tojoy.tjoybaselib.BaseLibKit;

import java.util.Map;

/**
 * @author chengyanfang
 * @version V 1.0.0
 * @function Preferences封装
 */

public class PreferencesUtil {
    private Context mContext;
    private String mName;

    public PreferencesUtil(Context context, String name) {
        this.mContext = context;
        this.mName = name;
    }

    @SuppressLint("WrongConstant")
    private SharedPreferences getSharedPreferences() {
        if (mContext == null) {
            mContext = BaseLibKit.getContext();
        }
        return mContext.getSharedPreferences(mName, Context.MODE_APPEND);
    }

    public void putString(String key, String value) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void putInt(String key, int value) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public void putBoolean(String key, Boolean value) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void putLong(String key, long value) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public String getString(String key) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        return sharedPreferences.getString(key, "");
    }

    public String getString(String key, String aDefault) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        return sharedPreferences.getString(key, aDefault);
    }

    public int getInt(String key) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        return sharedPreferences.getInt(key, 0);
    }

    public int getInt(String key, int defValue) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        return sharedPreferences.getInt(key, defValue);
    }

    public boolean getBoolean(String key) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        return sharedPreferences.getBoolean(key, true);
    }


    public boolean getBoolean(String key, boolean defaultBoolean) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        return sharedPreferences.getBoolean(key, defaultBoolean);
    }

    public long getLong(String key) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        if (sharedPreferences == null) {
            return 0;
        }
        return sharedPreferences.getLong(key, 0);
    }

    public Object getValue(String key) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        Map<String, ?> map = sharedPreferences.getAll();
        if (map != null && !map.isEmpty()) {
            return map.get(key);
        }
        return null;
    }

    public Map<String, ?> getAll() {
        SharedPreferences sharedPreferences = getSharedPreferences();
        Map<String, ?> map = sharedPreferences.getAll();
        return map;
    }

}
