package com.tojoy.tjoybaselib.util.string;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author qll
 * @date 2019/9/10
 * jsonObj与map转换
 */
public class JsonMapUtil {

    static Map<String, Object> parseOptions(JSONObject json) throws JSONException {
        Map<String, Object> options = new HashMap<String, Object>();
        if(json == null){
            return options;
        }

        Iterator<String> iter = json.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            options.put(key, json.get(key));
        }
        return options;
    }

    public static JSONObject optionsToJSON(Map<String, Object> options) throws JSONException {
        JSONObject json = new JSONObject();
        for (String key : options.keySet()) {
            json.put(key, options.get(key));
        }
        return json;
    }
}
