package com.tojoy.tjoybaselib.util.net;

import java.io.ByteArrayOutputStream;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * Created by chengyanfang on 2017/9/22.
 *
 * @fuction:压缩&加密方法
 */

public class CompressUtil {

    private static final int CRYPTKEY = 0x0A;

    /**
     * 压缩数据通过zlib压缩
     */
    private static byte[] compressData(byte[] bytes) {
        ByteArrayOutputStream bis = new ByteArrayOutputStream();
        try {
            byte[] tempByte = new byte[100];
            int compressedDataLength = -1;
            Deflater compresser = new Deflater();
            compresser.setInput(bytes);
            compresser.finish();
            while (compressedDataLength != 0) {
                compressedDataLength = compresser.deflate(tempByte);
                bis.write(tempByte, 0, compressedDataLength);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return bis.toByteArray();
    }


    /**
     * 解压数据通过zlib解压
     */
    private static byte[] decompressData(byte[] bytes) {
        ByteArrayOutputStream bis = new ByteArrayOutputStream();
        int resultLength = -1;
        try {
            Inflater decompresser = new Inflater();
            decompresser.setInput(bytes, 0, bytes.length);
            byte[] result = new byte[100];
            decompresser.finished();
            while (resultLength != 0) {
                resultLength = decompresser.inflate(result);
                bis.write(result, 0, resultLength);
            }
            decompresser.end();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return bis.toByteArray();
    }

    /**
     * 加密
     */
    private static byte[] ecrypt(byte[] bytes) {
        int len = bytes.length;
        for (int i = 0; i < len; i++) {
            bytes[i] = (byte) (bytes[i] ^ CRYPTKEY);
        }
        return bytes;
    }


    /**
     * 解密
     */
    private static byte[] decrypt(byte[] bytes) {
        int len = bytes.length;
        for (int i = 0; i < len; i++) {
            bytes[i] = (byte) (bytes[i] ^ CRYPTKEY);
        }
        return bytes;
    }


    /**
     * 先压缩再加密
     */
    public static byte[] compressAndEcrypt(String jsonStr) {
        byte[] compressData = compressData(jsonStr.getBytes());
        return ecrypt(compressData);
    }

    /**
     * 先解密再解压缩
     */
    public static byte[] deEcryptAndDeCompress(byte[] data) {
        byte[] deEcryptData = decrypt(data);
        return decompressData(deEcryptData);
    }
}
