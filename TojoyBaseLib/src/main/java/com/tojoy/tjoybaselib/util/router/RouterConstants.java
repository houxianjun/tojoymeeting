package com.tojoy.tjoybaselib.util.router;

/**
 * Created by yongchaowang
 * on 2020-03-03.
 */
public class RouterConstants {
    public static final String ORDER_ID = "order_id";
    public static final String ORDER_TYPE = "order_type";
    public static final int ORDER_TYPE_DEFAULT = 1;
    public static final String ORDER_CODE_ID = "order_code_id";
    public static final String ORDER_TOTAL_MONEY = "order_total_money";
    public static final String ORDER_PAY_FROM = "order_pay_from";
}
