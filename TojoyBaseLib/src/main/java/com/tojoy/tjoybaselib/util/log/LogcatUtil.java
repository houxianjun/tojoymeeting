package com.tojoy.tjoybaselib.util.log;

import android.util.Log;

/**
 * Created by chengyanfang on 2017/11/8.
 */

public class LogcatUtil {
    // 使用Log来显示调试信息,因为log在实现上每个message有4k字符长度限制
    // 所以这里使用自己分节的方式来输出足够长度的message
    public static void show(String TAG, String str) {
        if (str.length() > 3000) {
            Log.v(TAG, "sb.length = " + str.length());
            int chunkCount = str.length() / 3000;
            for (int i = 0; i <= chunkCount; i++) {
                int max = 3000 * (i + 1);
                if (max >= str.length()) {
                    Log.v(TAG, "chunk " + i + " of " + chunkCount + ":" + str.substring(3000 * i, str.length()));
                } else {
                    Log.v(TAG, "chunk " + i + " of " + chunkCount + ":" + str.substring(3000 * i, max));
                }
            }
        } else {
            Log.v(TAG, str);
        }

    }
}
