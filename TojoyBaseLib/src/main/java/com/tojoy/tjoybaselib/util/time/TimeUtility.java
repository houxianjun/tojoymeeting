package com.tojoy.tjoybaselib.util.time;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by chengyanfang on 2017/10/18.
 */

public class TimeUtility {
    public static String YEAR_HOUR_Notice = "yyyy-MM-dd HH:mm";

    private TimeUtility() {

    }

    /**
     * 根据时间获取几月几日
     */
    public static String getWeekDayHourFormat2(Long date) {
        Date currentTime = new Date(date);
        String[] weekDaysName = {"日", "一", "二", "三", "四", "五", "六"};
        // String[] weekDaysCode = { "0", "1", "2", "3", "4", "5", "6" };
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        int intWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        return weekDaysName[intWeek];
    }

    public static String getMonthToDateTime(Long milliseconds) {
        String dateFormatMonth = "M月d日";
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormatMonth, Locale.getDefault());
        return (formatter.format(new Date(milliseconds)));
    }

    public static String getTimeFormat(long timeStamp, String format) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(timeStamp));
    }
}
