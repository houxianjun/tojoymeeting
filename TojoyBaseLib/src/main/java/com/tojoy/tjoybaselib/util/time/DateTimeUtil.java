package com.tojoy.tjoybaselib.util.time;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by chengyanfang on 2017/9/5.
 */

public class DateTimeUtil {
    public static final String PATTERN_DAY_WITH_SPERATOR = "yyyy-MM-dd";
    public static final String PATTERN_CURRENT_TIME = "yyyy-MM-dd HH:mm:ss";
    public static final String PATTERN_CURRENT_TIME_HOUR = "MM-dd HH:mm";

    /**
     * 获取指定格式的当前时间
     */
    public static String getCurrentTime(String pattern) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Date data = new Date(System.currentTimeMillis());
        String time = format.format(data);
        return time;
    }

    /**
     * 当前时间：yyyy-MM-dd HH:mm:ss
     */
    public static String getFormatCurrentTime(String aTime, String format) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        Date date = new Date(Long.parseLong(aTime));
        return formatter.format(date);
    }
}
