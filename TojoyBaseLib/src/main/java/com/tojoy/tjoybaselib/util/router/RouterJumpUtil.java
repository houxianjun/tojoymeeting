package com.tojoy.tjoybaselib.util.router;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.BaseLibKit;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.services.live.KickOutLiveHelper;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;

/**
 * 作者：jiangguoqiu on 2020/2/23 14:41
 */
public class RouterJumpUtil {

    /**
     * 跳转登入界面不带参数
     */
    public static void startLoginActivity() {
        if (!FastClickAvoidUtil.isDoubleClick()) {
            KickOutLiveHelper.doLiveSomething(BaseLibKit.getContext());
            ARouter.getInstance().build(RouterPathProvider.Login).withBoolean("KICK_OUT", false).navigation();
        }
    }

    public static void startLoginActivity(boolean isKickOut) {
        if (!FastClickAvoidUtil.isDoubleClick()) {
            KickOutLiveHelper.doLiveSomething(BaseLibKit.getContext());
            ARouter.getInstance().build(RouterPathProvider.Login).withBoolean("KICK_OUT", isKickOut).navigation();
        }
    }

    /**
     * token 过期后跳入登入界面
     *
     * @param isInvalid
     */
    public static void startLoginActivityForTokenInvalid(boolean isInvalid) {
        if (!FastClickAvoidUtil.isDoubleClick() && !AppConfig.KICK_OUT) {
            KickOutLiveHelper.doLiveSomething(BaseLibKit.getContext());
            ARouter.getInstance().build(RouterPathProvider.Login)
                    .withBoolean("tokenInvalid", isInvalid)
                    .navigation();
        }
    }

    /**
     * 跳转支付详情
     */
    public static String SKIP_FROM_DETAIL = "skipFromDetail";

    public static String SKIP_FROM_RECHARGE_TIME ="skipFromRechargetime";

    public static String SKIP_FROM_RECHARGE_RECORD ="skipFromRechargeRecord";

    public static void startOrderPayDetailAct(String orderId, String orderCodeId, String totalMoney, String from) {
        ARouter.getInstance().build(RouterPathProvider.PayDetail)
                .withString(RouterConstants.ORDER_ID, orderId)
                .withString(RouterConstants.ORDER_CODE_ID, orderCodeId)
                .withString(RouterConstants.ORDER_TOTAL_MONEY, totalMoney)
                .withString(RouterConstants.ORDER_PAY_FROM, from)
                .navigation();
    }

    /**
     * 跳转订单列表
     */
    public static void startOrderListAct(int index) {
        if (!FastClickAvoidUtil.isDoubleClick()) {
            ARouter.getInstance().build(RouterPathProvider.Pay_OrderManagementListAct)
                    .withInt("index", index)
                    .navigation();
        }
    }

    /**
     * 跳转到商品详情
     *
     * @param goodsId
     */
    public static void startGoodsDetails(String goodsId) {
            ARouter.getInstance().build(RouterPathProvider.GoodsDetailNew)
                    .withString("detailId", goodsId)
                    .withString("agentCompanyCode","")
                    .withString("agentId","")
                    .withString("agentLive","0")
                    .navigation();
    }


    public static void startGoodsDetailFromLiveRoom(String goodsId, String roomId, String roomLiveId) {
            ARouter.getInstance().build(RouterPathProvider.GoodsDetailNew)
                    .withString("detailId", goodsId)
                    .withString("roomId", roomId)
                    .withString("roomLiveId", roomLiveId)
                    .withString("isFromLive", "1") // 悬浮播放器所需参数
                    .navigation();
    }
    public static void startGoodsDetails(String goodsId,String sourceWay,String agentCompanyCode,String agentId,String agentLive) {
        ARouter.getInstance().build(RouterPathProvider.GoodsDetailNew)
                .withString("detailId", goodsId)
                .withString("sourceWay",sourceWay)
                .withString("agentCompanyCode",agentCompanyCode)
                .withString("agentId",agentId)
                .withString("agentLive",agentLive)
                .navigation();
    }
    public static void startGoodsDetailFromLiveRoom(String goodsId, String roomId, String roomLiveId,String sourceWay,String agentCompanyCode,String agentId,String agentLive) {
        ARouter.getInstance().build(RouterPathProvider.GoodsDetailNew)
                .withString("detailId", goodsId)
                .withString("roomId", roomId)
                .withString("roomLiveId", roomLiveId)
                .withString("isFromLive", "1") // 悬浮播放器所需参数
                .withString("sourceWay",sourceWay)
                .withString("agentCompanyCode",agentCompanyCode)
                .withString("agentId",agentId)
                .withString("agentLive",agentLive)
                .navigation();
    }

    /**
     * 点击立即购买跳转至下单页面
     * 1 商品详情页面
     * 2 直播间推荐项目列表
     * 3 直播间发项目的中间弹窗
     * @param goodsId
     * @param roomLiveId
     */
    public static void startOderSureActivity(String goodsId, String roomLiveId) {
        ARouter.getInstance().build(RouterPathProvider.Pay_IntentionalOrderMakeSure)
                .withString("goodsId", goodsId)
                .withString("roomLiveId", roomLiveId)
                .navigation();
    }

    /**
     * 数据统计需求版本新增来源字段
     * 点击立即购买跳转至下单页面
     * 1 商品详情页面
     * 2 直播间推荐项目列表
     * 3 直播间发项目的中间弹窗 CenterProjectDialog
     * @param goodsId
     * @param roomLiveId
     * @param sourceWay  直播中:0   回放:1   默认企业主页:2（用户通过直播间进入企业主页算是直播间的）
     */
    public static void startOderSureActivity(String goodsId, String roomLiveId,String sourceWay,String agentCompanyCode,String agentId,String agentLive) {
        ARouter.getInstance().build(RouterPathProvider.Pay_IntentionalOrderMakeSure)
                .withString("goodsId", goodsId)
                .withString("roomLiveId", roomLiveId)
                .withString("sourceWay",sourceWay)
                .withString("agentCompanyCode",agentCompanyCode)
                .withString("agentId",agentId)
                .withString("agentLive",agentLive)
                .navigation();
    }

    /**
     * 跳入企业制定主页
     *
     * @param enterpriseId
     */
    public static void startEnterPrise(String enterpriseId) {
        ARouter.getInstance().build(RouterPathProvider.OnlineMeetingCustomizedEnterprisePageAct)
                .withString("companyCode", enterpriseId)
                .navigation();

    }


}
