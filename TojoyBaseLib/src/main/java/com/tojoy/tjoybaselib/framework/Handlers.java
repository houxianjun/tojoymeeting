package com.tojoy.tjoybaselib.framework;

import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;

import java.util.HashMap;

public final class Handlers {
    private static final String DEFAULT_TAG = "Default";

    private static Handlers instance;

    public static synchronized Handlers sharedInstance() {
        if (instance == null) {
            instance = new Handlers();
        }

        return instance;
    }

    private Handlers() {

    }

    /**
     * get new handler for a background stand alone looper identified by tag
     *
     * @param tag
     * @return
     */
    public final Handler newHandler(String tag) {
        return new Handler(getHandlerThread(tag).getLooper());
    }

    private final HashMap<String, HandlerThread> threads = new HashMap<String, HandlerThread>();

    private HandlerThread getHandlerThread(String tag) {
        HandlerThread handlerThread;

        synchronized (threads) {
            handlerThread = threads.get(tag);

            if (handlerThread == null) {
                handlerThread = new HandlerThread(nameOfTag(tag));

                handlerThread.start();

                threads.put(tag, handlerThread);
            }
        }

        return handlerThread;
    }

    private static String nameOfTag(String tag) {
        return "HT-" + (TextUtils.isEmpty(tag) ? DEFAULT_TAG : tag);
    }
}
