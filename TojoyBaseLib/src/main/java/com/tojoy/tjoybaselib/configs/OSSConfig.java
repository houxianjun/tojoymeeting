package com.tojoy.tjoybaselib.configs;

import android.content.Context;
import android.text.TextUtils;

import java.io.File;

/**
 * Created by chengyanfang on 2017//29.
 */

public class OSSConfig {

    //OSS路径
    public static final String OOSBucket = "cloudmeeting";
    public static final String OOSURL = "https://" + OOSBucket + ".oss-cn-beijing.aliyuncs.com/";
    public static final String getOSSRootFileName(){
        switch (AppConfig.environmentInstance) {
            case DEV:
                // 开发dev环境
                return "test/";
            case TEST:
                // 测试环境
                return "prod/";
            case FORMAL:
                // 正式环境
                return "prod/";
            default:
                // 正式环境
                return "prod/";
        }
    }

    private static final String OOSFileDir = "/oss/audio/";
    public static final String OOSAudioFileExtion = ".mp3";
    public static final String OOSAPKFileExtion = ".apk";
    public static final String OOSEndPoint = "http://oss-cn-beijing.aliyuncs.com";
    public static final String InvalidAccessKeyCode = "InvalidAccessKeyId";
    public static final String ResultFormattedMultiDefaultKey = "?x-oss-process=image/resize,m_fill,h_200,w_200";

    /**
     * 样式
     */
    public static final String OriginalFormattedStype = "?x-oss-process=style";
    public static final String ResultFormattedSingleKey = "?x-oss-process=style/YSMAX640";
    public static final String ResultFormattedMultiKey = "?x-oss-process=style/YS200200";

    public static String getOSSFileDir(Context context) {
        String fileDir = context.getExternalCacheDir().getAbsolutePath() + OOSFileDir;
        File file = new File(fileDir);
        if (!file.exists()) {
            try {
                file.mkdirs();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return fileDir;
    }

    /**
     * 获取单个图片的路径
     */
    public static String getFormattedSinglePicPath(String originalPath) {
        if (originalPath.contains(OriginalFormattedStype)) {
            int index = originalPath.indexOf("?");
            originalPath = originalPath.substring(0, index);
        }

        if (originalPath.contains(OOSURL)) {
            return originalPath + ResultFormattedSingleKey;
        } else {
            return OOSURL + originalPath + ResultFormattedSingleKey;
        }
    }

    /**
     * 获取多个图片的路径
     */
    public static String getFormattedMultiPicPath(String originalPath) {
        if (originalPath.contains(OriginalFormattedStype)) {
            int index = originalPath.indexOf("?");
            originalPath = originalPath.substring(0, index);
        }

        String url;

        if (originalPath.contains(OOSURL)) {
            url = originalPath + ResultFormattedMultiDefaultKey;
        } else {
            url = OOSURL + originalPath + ResultFormattedMultiDefaultKey;
        }

        return url;
    }

    /**
     * 去除图片路径所带的样式
     */
    public static String getRemoveStylePath(String path) {
        if (TextUtils.isEmpty(path)) {
            return "";
        }

        if (path.contains("?") && path.split("\\?").length > 0) {
            return path.split("\\?")[0];
        } else {
            return path;
        }
    }

    /**
     * 获取去除OOSURL后的地址
     */
    public static String getRemoveOSSURLStr(String originalURL) {
        if (originalURL.contains(OOSURL)) {
            int comIndex = originalURL.indexOf(".com/");
            return originalURL.substring(comIndex + 5, originalURL.length());
        } else {
            return originalURL;
        }
    }

    /**
     * 添加OSSApi
     */
    public static String getOSSURLedStr(String originalURL) {

        if (TextUtils.isEmpty(originalURL)) {
            return "";
        }

        if (originalURL.contains("http:") || originalURL.contains("https:")) {
            return originalURL;
        } else {
            return OOSURL + originalURL;
        }
    }

    /**
     * 获取制定样式的地址
     */
    public static String getStyledImagePath(String originalPath, String style) {

        originalPath = getOSSURLedStr(originalPath);

        if (originalPath.contains(OriginalFormattedStype)) {
            int index = originalPath.indexOf("?");
            originalPath = originalPath.substring(0, index);
        }

        return originalPath + style;
    }

}
