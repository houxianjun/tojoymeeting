package com.tojoy.tjoybaselib.configs;

public enum UserRoles {
    /**
     * 未知
     */
    Unkown(""),
    /**
     * 超级管理员
     */
    SUPERADMIN("SUPERADMIN"),
    /**
     * 管理员
     */
    ADMIN("ADMIN"),
    /**
     * 官方会议管理员
     */
    OFFICIALADMIN("OFFICIALADMIN"),
    /**
     * 财务
     */
    FINANCE("FINANCE"),
    /**
     *  助手
     */
    HELPER("HELPER"),
    /**
     * 员工
     */
    EMPLOYEE("EMPLOYEE"),
    /**
     * 特权主播
     */
    PRIVILEANCHOR("PRIVILEANCHOR"),
    /**
     * 主播
     */
    ANCHOR("ANCHOR");

    private String value;

    UserRoles(String netState) {
        this.value = netState;
    }

    public String getValue() {
        return value;
    }

    /**
     * 1、申请中  2、被驳回  3 已通过  4已完成(本次申请通过并且直播完成)
     * @param value
     * @return
     */
    public static UserRoles getStateCode(String value) {
        switch (value) {
            case "":
                return Unkown;
            case "SUPERADMIN":
                return SUPERADMIN;
            case "ADMIN":
                return ADMIN;
            case "OFFICIALADMIN":
                return OFFICIALADMIN;
            case "FINANCE":
                return FINANCE;
            case "HELPER":
                return FINANCE;
            case "EMPLOYEE":
                return FINANCE;
            case "PRIVILEANCHOR":
                return PRIVILEANCHOR;
            case "ANCHOR":
                return ANCHOR;
            default:
                return Unkown;
        }
    }
}
