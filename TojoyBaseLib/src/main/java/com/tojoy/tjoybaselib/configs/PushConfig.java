package com.tojoy.tjoybaselib.configs;


/**
 * Created by chengyanfang on 2017/10/17.
 */
public class PushConfig {

    //魅族
    public static final String MZ_PUSH_APPID = "111710";
    public static final String MZ_PUSH_APPKEY = "282bdd3a37ec4f898f47c5bbbf9d2369";

    //小米
    public static final String MI_PUSH_NAME = "XIMIPUSH";
    public static final String MI_PUSH_APPID = "2882303761517721833";
    public static final String MI_PUSH_APPKEY = "5911772184833";
    public static final String QUERY_PARAMETER = "param";//取推送内容的参数

    //华为
    public static final String HW_PUSH_NAME = "HUAWEIPUSH";

}
