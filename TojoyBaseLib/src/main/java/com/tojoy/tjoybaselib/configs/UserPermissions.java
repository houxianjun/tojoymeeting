package com.tojoy.tjoybaselib.configs;

/**
 * @author qll
 * @date 2020/6/5
 * 用户权限信息
 */
public enum UserPermissions {
    /**
     * 未知
     */
    Unkown(""),
    /**
     * 开播
     */
    STARTAUTH("STARTAUTH"),
    /**
     * OBS推流权限
     */
    OBSMANAGER("OBSMANAGER"),
    /**
     * 免审批-开播
     */
    NO_APPROVE("NO_APPROVE");

    private String value;

    UserPermissions(String netState) {
        this.value = netState;
    }

    public String getValue() {
        return value;
    }

    /**
     * 1、申请中  2、被驳回  3 已通过  4已完成(本次申请通过并且直播完成)
     */
    public static UserPermissions getStateCode(String value) {
        switch (value) {
            case "":
                return Unkown;
            case "STARTAUTH":
                return STARTAUTH;
            case "OBSMANAGER":
                return OBSMANAGER;
            case "NO_APPROVE":
                return NO_APPROVE;
            default:
                return Unkown;
        }
    }
}
