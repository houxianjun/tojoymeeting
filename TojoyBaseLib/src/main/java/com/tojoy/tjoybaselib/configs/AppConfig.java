package com.tojoy.tjoybaselib.configs;

/**
 * Created by chengyanfang on 2017/9/5~
 */

public class AppConfig {
    /**
     * 环境:
     * DEV：开发
     * TEST：测试
     * FORMAL：正式
     */
    public static EnvironmentInstance environmentInstance = EnvironmentInstance.FORMAL;

    /**
     * 标示是否拼接token
     * 有几个接口不需要拼接：
     * cloud-meeting-api/api/live/room/quitLiveRoom
     * cloud-meeting-api/api/sameLive/finishLive
     * cloud-meeting-api/api/roomLive/endLive
     * cloud-meeting-api/client-api/appVersion/check
     * cloud-meeting-auth/tc/login/app
     * cloud-meeting-upms/api/user/send/sms/code
     */
    public static boolean isAppendHeaderAuthorization = true;

    /**
     * 分页请求每页页数
     */
    public static final int PER_PAGE_COUNT = 10;

    /**
     * 是否成功登录IM
     */
    public static boolean isLoginIM = false;

    /**
     * 是否被踢掉
     */
    public static boolean KICK_OUT = false;


    /**
     * 支付是否跳入小程序
     */
    public static boolean hasSkipToMini;

    /**
     * 保存当前的运行环境，环境跟上一次不同需要重新登陆
     */
    public static int getAppNowEnvriment() {
        return AppConfig.environmentInstance.getValue();
//        return AppConfig.environmentInstance == EnvironmentInstance.DEV ? 1 : 0;
    }

    /**
     * 微信Pay AppID
     */
    public static final String WXPAY_APP_ID = "wxa12a3ed50ab91905";

    /**
     * buglySDK
     */
    public static final String BUGLY_APP_ID = "0d71344e20";

    /**
     * 返回顶部按钮是否在显示
     */
    public static boolean isShowing = false;

    public static boolean isHomePopShown;

    public static boolean isCanToast() {
        return false;
        //isDebug || isSimulate || isPreprod;
    }
}