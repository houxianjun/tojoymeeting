package com.tojoy.tjoybaselib.configs;

/**
 * sdk全局运行环境
 * 内部维护：开发、测试、正式，三套
 * @author qll
 */

public enum EnvironmentInstance {
    /**
     * 开发环境
     */
    DEV(1),
    /**
     * 测试环境
     */
    TEST(2),
    /**
     * 正式
     */
    FORMAL(3);

    private int value;

    EnvironmentInstance(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static EnvironmentInstance typeOfValue(int value) {
        for (EnvironmentInstance e : values()) {
            if (e.getValue() == value) {
                return e;
            }
        }
        return DEV;
    }
}
