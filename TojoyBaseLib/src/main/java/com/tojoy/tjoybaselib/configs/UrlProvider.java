package com.tojoy.tjoybaselib.configs;

import com.tojoy.tjoybaselib.net.RetrofitSingleton;


/**
 * 提供平台除Api接口除外的链接，主要包括
 */

public class UrlProvider {

    //用户注册服务协议
    public static String USERCENTER_RegistPolicy = RetrofitSingleton.getWWWBaseUrlH5() + "singleApp/agreement/agreement.html";

    //隐私政策
    public static String USERCENTER_Privacy = RetrofitSingleton.getWWWBaseUrlH5() + "singleApp/privacy-protocol.html";

    //使用明细说明
    public static String Usage_Details = RetrofitSingleton.getWWWBaseUrlH5() + "singleApp/chargeStatement/index.html";
    // 推广协议
    public static String DISTRIBUTION_AGREEMENT =  RetrofitSingleton.getWWWBaseUrlH5() + "singleApp/promotion-agreement/index.html";
}
