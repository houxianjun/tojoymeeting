package com.tojoy.tjoybaselib.configs;

public class RouterPathProvider {
    /**
     * Common
     */
    //APP首页
    public static final String APP_HOME = "/app/home";
    //外链
    public static final String WEBNew = "/common/outLink/new";

    //登录
    public static final String Login = "/app/login";
    public static final String SystemNotice = "/app/systemNotice";

    /**
     * 网上天洽会
     */
    public static final String ONLINE_MEETING_START_LIVE_BROADCAST_APPLY = "/onlineMeeting/StartLiveBroadcastApply";
    public static final String ONLINE_MEETING_EDIT_ROOM_NAME = "/onlineMeeting/StartLiveBroadcastApply/editRoomName";
    public static final String ONLINE_MEETING_MY_APPROVAL_LIST = "/onlineMeeting/MyApprovalList";
    public static final String ONLINE_MEETING_ENTER_ROOM_PSW = "/onlineMeeting/EnterRoomPsw";
    public static final String ONLINE_MEETING_USER_CENTER = "/onlineMeeting/UserCenter";

    public static final String ONLINE_MEETING_LIVE_ROOM = "/onlineMeeting/LiveRoom";
    public static final String ONLINE_MEETING_SEE_LIVE_ROOM = "/onlineMeeting/SeeLiveRoom";
    public static final String ONLINE_ENTERPRISE_AND_PRODUCT_DETAIL = "/onlineMeeting/EnterpriseAndProductDetail";

    public static final String ONLINE_MEETING_EXTEN_SION_POSTER = "/onlineMeeting/MeetingExtenSionPoster";
    public static final String OnlineMeetingMyExtenSionPoster = "/onlineMeeting/MyExtenSionPoster";

    /**
     * 独立app云洽会
     */
    public static final String JOIN_MEETING = "/app/Joinmeeting";
    public static final String ENTERPRISE_ROAD_OR_HOT_LIVE_LIST = "/EnterpriseEdition/RoadOrHotLiveList";
    public static final String APP_ADD_ADDRESS = "/app/AddAddress";
    public static final String App_AddressManager = "/app/App_AddressManager";
    public static final String MeetingSearchAct = "/EnterpriseEdition/MeetingSearchAct";
    public static final String HotLiveSearchAct = "/EnterpriseEdition/HotLiveSearchAct";
    public static final String BrowsedEnterpriseSearchAct = "/EnterpriseEdition/BrowsedEnterpriseSearch";
    public static final String GoodsSearchAct = "/EnterpriseEdition/GoodSearchAct";
    public static final String OnlineMeetingCustomizedEnterprisePageAct = "/onlineMeetin/CustomizedEnterprisePageAct";
    public static final String ServiceManager_RechargeTimeAct = "/ServiceManager/RechargeTimeAct";
    public static final String ServiceManager_PurchaseRecordAct = "/ServiceManager/PurchaseRecordAct";
    public static final String ServiceManager_ConsumptionDetailsAct = "/ServiceManager/ConsumptionDetailsAct";
    public static final String Distribution_MyMemberAct = "/Distribution/DistributionMyMemberAct";
    public static final String Distribution_MyProfitAct = "/Distribution/DistributionMyProfitAct";
    public static final String Distribution_MyMember_TeamList_Act = "/Distribution/DistributionMyMemberTeamListAct";
    public static final String Distribution_WithdrawalDetail_Act = "/Distribution/DistributionWithdrawalDetailAct";
    public static final String Distribution_WithdrawalRecord_Act = "/Distribution/DistributionWithdrawalRecordAct";
    public static final String Distribution_MyWithdrawal_Act = "/Distribution/DistributionMyWithdrawalAct";
    public static final String Distribution_Information_Entry_Act = "/Distribution/DistributionInformationEntryAct";

    /**
     * module_shop
     */
    public static final String Enterprise_Shop = "/moduleShop/GoodsDetailOld/enterpriseshop";
    public static final String GoodsDetailNew = "/moduleShop/GoodsDetai/goodsDetailNew";
    public static final String BrowsedEnterprise = "/moduleShop/home/BrowsedEnterprise";
    public static final String EnterpriseStructHome = "/moduleShop/home/EnterStructHome";
    public static final String Pay_OrderManagementListAct = "/moduleShop/OrderManagementListAct";
    public static final String Pay_IntentionalOrderMakeSure = "/moduleShop/IntentionalOrderMakeSure";
    public static final String Pay_OrderDetail = "/moduleShop/OrderDetail";
    public static final String PayDetail = "/moduleShop/OrderPayDetailAct";

    //企业
    public static final String ApplyEnterprise = "/moduleShop/Enterprise/ApplyEnterprise";
    public static final String MeetingPersonEnterprise = "/moduleShop/Enterprise/MeetingPersonEnterprise";
    //云洽会1.2.3需求增加，选择参会人员 添加外部联系人选择
    public static final String MeetingAddOutSidePersonEnterprise =  "/moduleShop/Enterprise/MeetingAddOutSidePersonEnterprise";


}
