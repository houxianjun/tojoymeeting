package com.tojoy.tjoybaselib;

import android.annotation.SuppressLint;
import android.content.Context;

public class BaseLibKit {

    @SuppressLint("StaticFieldLeak")
    private static Context mContext;

    public static void init(Context context, String versionName) {
        mContext = context;
        mVersionName = versionName;
    }

    public static Context getContext() {
        return mContext;
    }

    /**
     * 版本号
     */
    private static String mVersionName;

    public static String getVersionName() {
        return mVersionName;
    }
}
