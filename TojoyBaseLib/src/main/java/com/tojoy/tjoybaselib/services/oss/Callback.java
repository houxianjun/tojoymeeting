package com.tojoy.tjoybaselib.services.oss;

import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.ServiceException;

/**
 * Created by chengyanfang on 2017/9/27.
 */

public interface Callback<T1,T2> {
    void onSuccess(T1 request, T2 result);
    void onFailure(T1 request, ClientException clientException, ServiceException serviceException);
}
