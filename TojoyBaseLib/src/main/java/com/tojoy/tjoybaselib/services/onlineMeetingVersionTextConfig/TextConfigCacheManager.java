package com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.util.storage.PreferencesUtil;

import rx.Observer;

/**
 * @author fanxi
 * @date 2020-03-06.
 * description：天九云洽会版本配置全局控件名称
 */
public class TextConfigCacheManager {
    private static final String CACHE_TEXT_BEAN = "cache_text_bean";
    @SuppressLint("StaticFieldLeak")
    private static PreferencesUtil mPreferencesUtil;
    private static TextConfigCacheManager mTextConfigCacheManager;
    private TextConfigResponse mTextConfigResponse = new TextConfigResponse();

    private static PreferencesUtil getPreferenceUtil(Context context) {
        if (mPreferencesUtil == null) {
            mPreferencesUtil = new PreferencesUtil(context, CACHE_TEXT_BEAN);
        }
        return mPreferencesUtil;
    }


    public static TextConfigCacheManager getInstance(Context context) {
        getPreferenceUtil(context);
        if (mTextConfigCacheManager == null) {
            synchronized (TextConfigCacheManager.class) {
                mTextConfigCacheManager = new TextConfigCacheManager();
            }
        }
        return mTextConfigCacheManager;

    }

    /**
     * 更新天九云洽会版本的控件文本数据
     */
    public void updateOMVersionTextOfWidget() {
        mTextConfigResponse = null;
        OMAppApiProvider.getInstance().getOMVersionTextOfWidgets(new Observer<TextConfigResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(TextConfigResponse omBaseResponse) {
                mTextConfigResponse = omBaseResponse;
                if (mTextConfigResponse != null) {
                    String textConfigJson = new Gson().toJson(mTextConfigResponse);
                    if (!TextUtils.isEmpty(textConfigJson)) {
                        mPreferencesUtil.putString(CACHE_TEXT_BEAN, textConfigJson);

                    }
                }

            }
        });
    }

    /**
     * 根据key获取控件文案
     *
     * @param key
     */
    public String getWidgetTextByKey(String key) {
        if (mPreferencesUtil == null || TextUtils.isEmpty(mPreferencesUtil.getString(CACHE_TEXT_BEAN))) {
            return null;
        }
        mTextConfigResponse = new Gson().fromJson(mPreferencesUtil.getString(CACHE_TEXT_BEAN), TextConfigResponse.class);
        if (mTextConfigResponse == null) {
            return null;
        }
        String value = "";
        switch (key) {
            case TextConfigConstantKey.SLOGAN1:
                value = mTextConfigResponse.slogan1;
                break;
            case TextConfigConstantKey.SLOGAN2:
                value = mTextConfigResponse.slogan2;
                break;
            case TextConfigConstantKey.SLOGAN3:
                value = mTextConfigResponse.slogan3;
                break;

            ///导航-首页
            case TextConfigConstantKey.BAR1:
                value = mTextConfigResponse.bar1;
                break;
            ///导航-会议
            case TextConfigConstantKey.BAR2:
                value = mTextConfigResponse.bar2;
                break;
            ///导航-消息
            case TextConfigConstantKey.BAR3:
                value = mTextConfigResponse.bar3;
                break;
            ///导航-我的
            case TextConfigConstantKey.BAR4:
                value = mTextConfigResponse.bar4;
                break;
            //会议页面title
            case TextConfigConstantKey.MEETING_TITLE:
                value = mTextConfigResponse.meetingTitle;
                break;
            //直播间-评论
            case TextConfigConstantKey.MEETIGN_BAR1:
                value = mTextConfigResponse.meetingBar1;
                break;
            //直播间-分享
            case TextConfigConstantKey.MEETING_BAR2:
                value = mTextConfigResponse.meetingBar2;
                break;

            //直播间-观众
            case TextConfigConstantKey.MEETING_BAR3:
                value = mTextConfigResponse.meetingBar3;
                break;
            //直播间-企业
            case TextConfigConstantKey.MEETING_BAR4:
                value = mTextConfigResponse.meetingBar4;
                break;
            //列表
            case TextConfigConstantKey.MEETING_BAR5:
                value = mTextConfigResponse.meetingBar5;
                break;
            //我感兴趣
            case TextConfigConstantKey.PRODUCT1:
                value = mTextConfigResponse.product1;
                break;
            //我感兴趣
            case TextConfigConstantKey.PRODUCT2:
                value = mTextConfigResponse.product2;
                break;
            //立即购买
            case TextConfigConstantKey.PRODUCT3:
                value = mTextConfigResponse.product3;
                break;
            ////视频互动
            case TextConfigConstantKey.INTER_ACTION1:
                value = mTextConfigResponse.interAction1;
                break;
            //发起收款
            case TextConfigConstantKey.INTER_ACTION4:
                value = mTextConfigResponse.interAction4;
                break;
            //引流会议画面
            case TextConfigConstantKey.INTER_ACTION5:
                value = mTextConfigResponse.interAction5;
                break;
            default:
                break;
        }

        return value;
    }
}
