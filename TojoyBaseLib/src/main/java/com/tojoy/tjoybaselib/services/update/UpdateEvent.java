package com.tojoy.tjoybaselib.services.update;

public class UpdateEvent {
    public boolean needUpgrade;
    public boolean showPop;

    UpdateEvent(boolean needUpgrade, boolean showPop) {
        this.needUpgrade = needUpgrade;
        this.showPop = showPop;
    }
}
