package com.tojoy.tjoybaselib.services.Bugly;

import android.content.Context;

import com.tencent.bugly.crashreport.CrashReport;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;

/**
 * bugly 用于统计关键模块的bug上报：
 * 自定义标签，用于标明App的某个“场景”。
 * 在发生Crash时会显示该Crash所在的“场景”，以最后设置的标签为准，标签id需大于0。
 * @author qululu
 */
public class BuglyScenceManger {
    private static final Singleton<BuglyScenceManger> SINGLETON =
            new Singleton<BuglyScenceManger>() {
                @Override
                protected BuglyScenceManger create() {
                    return new BuglyScenceManger();
                }
            };

    public static BuglyScenceManger getInstance() {
        return SINGLETON.get();
    }

    // 首页
    public int homeTag = 165763;
    // 登录
    public int loginTag = 165764;
    // 开播申请
    public int applyLiveTag = 165765;
    // 直播间
    public int roomLiveTag = 165766;
    // 我的
    public int mineTag = 165767;
    // IM
    public int imTag = 165769;
    // 企业主页
    public int searchTag = 165771;
    // 企业管理
    public int newsTag = 165772;
    // 海报
    public int businessTag = 165773;
    // 会员、收益
    public int updateTag = 165774;
    // 提现
    public int moneyTag = 165777;

    public void setCarshUserDate(Context context){
        // 该用户本次启动后的异常日志用户ID都将是此值
        CrashReport.setUserId(BaseUserInfoCache.getUserId(context));
        CrashReport.putUserData(context, "userName", BaseUserInfoCache.getUserName(context));
        CrashReport.putUserData(context, "userId", BaseUserInfoCache.getUserId(context));
        CrashReport.putUserData(context, "userPhone", BaseUserInfoCache.getUserMobile(context));
    }

    public void setCarshTag(Context context,int tag){
        CrashReport.setUserSceneTag(context, tag);
    }
}
