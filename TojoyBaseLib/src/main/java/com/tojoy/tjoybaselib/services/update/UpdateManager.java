package com.tojoy.tjoybaselib.services.update;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.tojoy.tjoybaselib.net.HttpErrorUtil;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.oss.OSSService;
import com.tojoy.tjoybaselib.services.update.view.TJUpdateAppDialog;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.ui.hud.ProgressHUD;
import com.tojoy.tjoybaselib.util.sys.InstallUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import es.dmoral.toasty.Toasty;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import rx.Observer;

/**
 * Created by MPW on 2017/10/31.
 */

public class UpdateManager {
    private static volatile UpdateManager instance = null;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private TJUpdateAppDialog mDialog;
    /**
     * APK的文件名
     */
    private static final String FILENAME = "updata.apk";

    private UpdateManager() {
    }

    public static UpdateManager getInstance() {
        if (instance == null) {
            synchronized (UpdateManager.class) {
                instance = new UpdateManager();
            }
        }
        return instance;
    }

    /**
     * ProgressHUD处理
     */
    private ProgressHUD mProgressHUD;

    public void showProgressHUD(Activity context, String showMessage, boolean cancleAble) {
        mHandler.post(() ->
                mProgressHUD = ProgressHUD.show(context, showMessage, cancleAble, null));
    }

    public void dismissProgressHUD() {
        try {
            if (mProgressHUD != null && mProgressHUD.isShowing()) {
                mHandler.post(() ->
                        mProgressHUD.dismiss());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkUpdate(Activity context, boolean isManuCheck) {
        //19.6.2 避免OnResume重新检测，再次下载。
        if (mProgressHUD != null) {
            if (mProgressHUD.isShowing()) {
                return;
            }
        }

        OMAppApiProvider.getInstance().checkUpdate(new Observer<UpdateModel>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                if (isManuCheck) {
                    mHandler.post(() -> Toasty.normal(context, "您的APP已经是最新的版本了，不需要更新").show());
                }
            }

            @Override
            public void onNext(UpdateModel updateModel) {
                if (updateModel.data == null) {
                    if (isManuCheck) {
                        mHandler.post(() -> Toasty.normal(context, "您的APP已经是最新的版本了，不需要更新").show());
                    }
                } else {
                    if (updateModel.data.compel == 1) {
                        showAlertDialog(context, updateModel);
                    } else {
                        showConfirmDialog(context, updateModel);
                    }
                    EventBus.getDefault().post(new UpdateEvent(true, false));
                }
            }
        });
    }

    private void jumpMarket(Activity context, boolean isCanDismiss, UpdateModel updateModel) {
        try {
            Uri uri = Uri.parse(updateModel.data.url);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            context.startActivity(intent);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    /**
     * 强制更新
     *
     * @param context
     * @param updateModel
     */
    private void showAlertDialog(Activity context, UpdateModel updateModel) {
        if (TextUtils.isEmpty(updateModel.data.brief)) {
            updateModel.data.brief = "版本更新";
        }
        updateModel.data.brief = updateModel.data.brief.replace("\\n", "\n");
        mHandler.post(() -> {
            mDialog = new TJUpdateAppDialog(context);
            mDialog.setEvent(v -> {
                if (Build.VERSION.SDK_INT > 28) {
                    jumpMarket(context, false, updateModel);
                } else {
                    dowloadAPk(context, updateModel.data.url);
                    mDialog.dismiss();
                }
            });
            if (!mDialog.isShowing()) {
                mDialog.setTitleAndCotent("版本更新", updateModel.data.brief).showAlert();
            }
        });
    }

    /**
     * 选择更新
     *
     * @param context
     * @param updateModel
     */
    private void showConfirmDialog(Activity context, UpdateModel updateModel) {
        if (TextUtils.isEmpty(updateModel.data.brief)) {
            updateModel.data.brief = "版本更新";
        }
        updateModel.data.brief = updateModel.data.brief.replace("\\n", "\n");

        mHandler.post(() -> {

            mDialog = new TJUpdateAppDialog(context, view -> {
                if (Build.VERSION.SDK_INT > 28) {
                    jumpMarket(context, true, updateModel);
                } else {
                    dowloadAPk(context, updateModel.data.url);
                }
            });
            if (!mDialog.isShowing()) {
                mDialog.setTitleAndCotent("版本更新", updateModel.data.brief).showConfirm();

                mDialog.setCancelListener(() -> EventBus.getDefault().post(new UpdateEvent(false, true)));
            }
        });
    }

    /**
     * 下载apk
     * <p>
     *
     * @param context
     * @param downloadUrl
     */
    private void dowloadAPk(Activity context, String downloadUrl) {
        //版本不一样，更新
        if (TextUtils.isEmpty(downloadUrl) || (!downloadUrl.startsWith("http://") && !downloadUrl.startsWith("https://")) || !downloadUrl.contains(".com/")) {
            Toasty.normal(context, "获取APP下载链接失败").show();
            return;
        }

        String url = downloadUrl.split(".com/")[1];
        if (TextUtils.isEmpty(url) || !url.endsWith("apk")) {
            Toasty.normal(context, "获取APP下载链接失败").show();
            return;
        }

        showProgressHUD(context, "下载中：0%", false);

        OSSService.getInterface().initOSSService(new OSSService.InitOSSCallBack() {
            @Override
            public void onInitComplete() {
                OSSService.getInterface().downAPK(context, url, new OSSService.DownLoadCallback() {
                    @Override
                    public void onLoadSuccess(String filePath) {
                        dismissProgressHUD();
                        InstallUtil.installApk(context, filePath);
                    }

                    @Override
                    public void onLoadError(String errorReason) {
                        tryAgain(context, downloadUrl);
                    }

                    @Override
                    public void onLoadProgress(int progress) {
                        if (mProgressHUD != null) {
                            mHandler.post(() -> mProgressHUD.setMessage("下载中：" + String.valueOf(progress) + "%"));
                        }
                    }
                });
            }

            @Override
            public void onInitFail() {
                tryAgain(context, downloadUrl);
            }
        });
    }

    private void tryAgain(Activity context, String downloadUrl) {

        Request request = new Request.Builder().url(downloadUrl).build();
        OkHttpClient client = new OkHttpClient();

        if (mProgressHUD == null || !mProgressHUD.isShowing()) {
            showProgressHUD(context, "下载中：0%", false);
        }

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                dismissProgressHUD();
                HttpErrorUtil.error(context, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                InputStream is = null;
                byte[] buf = new byte[2048];
                int len = 0;
                FileOutputStream fos = null;
                // 储存下载文件的目录
                try {
                    is = response.body().byteStream();
                    long total = response.body().contentLength();
                    File file = getUpdateAPKFile(context);
                    fos = new FileOutputStream(file);
                    long sum = 0;
                    while ((len = is.read(buf)) != -1) {
                        fos.write(buf, 0, len);
                        sum += len;
                        int progress = (int) (sum * 1.0f / total * 100);
                        if (mProgressHUD != null) {
                            mHandler.post(() -> mProgressHUD.setMessage("下载中：" + String.valueOf(progress) + "%"));
                        }
                    }
                    fos.flush();
                    dismissProgressHUD();
                    InstallUtil.installApk(context, file.getAbsolutePath());
                } catch (Exception e) {
                    HttpErrorUtil.error(context, e);
                    dismissProgressHUD();
                } finally {
                    try {
                        if (is != null) {
                            is.close();
                        }
                    } catch (IOException e) {
                    }
                    try {
                        if (fos != null) {
                            fos.close();
                        }
                    } catch (IOException e) {
                    }
                }

            }
        });
    }

    private File getUpdateAPKFile(Activity context) {
        File updateDir = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
        if (!updateDir.exists()) {
            updateDir.mkdirs();
        }
        updateDir = new File(updateDir, "update");
        if (!updateDir.exists()) {
            updateDir.mkdirs();
        }
        File updateFile = new File(updateDir, FILENAME);
        if (updateFile.exists()) {
            updateFile.delete();
        }
        return updateFile;
    }

}
