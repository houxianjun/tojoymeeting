package com.tojoy.tjoybaselib.services.cache;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.tojoy.tjoybaselib.BaseLibKit;
import com.tojoy.tjoybaselib.BuildConfig;
import com.tojoy.tjoybaselib.configs.UserPermissions;
import com.tojoy.tjoybaselib.configs.UserRoles;
import com.tojoy.tjoybaselib.model.home.MineDistributionResponse;
import com.tojoy.tjoybaselib.util.log.LogUtil;
import com.tojoy.tjoybaselib.util.storage.PreferencesUtil;

public class BaseUserInfoCache {
    private static final String USER_INFO = "userinfo_propertiesInfo";
    @SuppressLint("StaticFieldLeak")
    private static PreferencesUtil mInfoUtil = null;

    /**
     * Preferences文件
     */
    private static PreferencesUtil getPreferencesUtil(Context context) {
        if (mInfoUtil == null) {
            mInfoUtil = new PreferencesUtil(context, USER_INFO);
        }
        return mInfoUtil;
    }

    public static boolean isMySelf(Context context, String accout) {
        return getUserId(context).equals(accout);
    }

    /**
     * 获取用户手机号
     *
     * @param context
     */
    public static String getUserMobile(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("userMobile");
    }


    public static void setCurrentTabIndex(int currentIndex, Context context) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putInt("currentIndex", currentIndex);
    }

    /**
     * 获取UserId
     */
    public static String getUserId(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("userId");
    }

    public static String getRoomCode(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("roomCode");
    }

    /**
     * 获取UserName
     */
    public static String getUserName(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("trueName");
    }


    public static void setUserName(String trueName, Context context) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putString("trueName", trueName);
    }

    /**
     * 获取UserJob
     */
    public static String getUserJob(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("job");
    }

    public static String getUserToken(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("token");
    }

    public static String getCompanyCode(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("companyCode");
    }


    public static void saveSign(String sign, Context context) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putString("sign", sign);
    }

    private static String getSign(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("sign");
    }


    public static void saveDataSign(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putString("dataSign", getSign(context));
    }

    public static String getDataSign(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("dataSign");
    }

    /**
     * 获取UserId
     */
    public static String getUserHeaderPic(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return TextUtils.isEmpty(mInfoUtil.getString("avatar")) ? "" : mInfoUtil.getString("avatar");
    }

    /**
     * 获取用户等级
     */
    public static String getUserLevevl(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("userLevel");
    }

    public static boolean isHasCompany(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        String companyCode = mInfoUtil.getString("companyCode");
        return !TextUtils.isEmpty(companyCode) && !"null".equals(companyCode);
    }

    /**
     * 获取IM前缀
     */
    public static String getIMAccountHeader(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("imaccountHeader");
    }

    /**
     * 获取前缀化的Im Account
     */
    public static String getIMHeaderedAccount(Context context) {
        String userId;
        userId = getUserId(context);
        return getIMAccountHeader(context) + userId;
    }

    public static void setIMAccountHeader(Context context, String accountHeaderStr) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putString("imaccountHeader", accountHeaderStr);
    }

    /**
     * 获取公司名称
     */
    public static String getCompanyName(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("companyName");
    }

    /**
     * 获取职位名称
     */
    public static String getJobName(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("job");
    }

    public static String getIMToken(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("imtoken");
    }

    public static void saveIMToken(Context context, String token) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putString("imtoken", token);
    }

    /**
     * 是否同意过隐私协议
     */
    public static boolean isAgreed(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getBoolean("isagreed", false);
    }

    /**
     * 设置同意过隐私协议
     */
    public static void putIsAgreed(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putBoolean("isagreed", true);
    }

    //用户是否登入
    public static boolean isLogined(Context context) {
        return !TextUtils.isEmpty(getUserMobile(context)) && !TextUtils.isEmpty(getAccessToken(context));
    }

    /**
     * 去除IMHeader
     */
    public static String removeIMHeader(Context context, String account) {
        if (account.contains(getIMAccountHeader(context))) {
            return account.substring(getIMAccountHeader(context).length());
        }
        return account;
    }

    /**
     * 添加IMHeader
     */
    public static String addIMHeader(Context context, String account) {
        return getIMAccountHeader(context) + account;
    }


    public static String getUserNickName(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("trueName");
    }

    /**
     * 获取地址缓存
     */
    public static String getAddress(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("address");
    }

    /**
     * 设置H5特殊来源标记
     *
     * @param context
     * @param path    H5地址
     */
    public static void savaH5SpecialSource(Context context, String path) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putString("H5SpecialSource", path);
    }

    public static String getH5SpecialSource(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("H5SpecialSource");
    }

    /**
     * 获取用户的角色
     */
    public static int getUserEmployeeFlag(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getInt("employeeFlag");
    }

    /**
     * 保存网上天洽会是否有未处理审批的消息
     */
    public static void saveOnlineMeetingCheck(Context context, boolean isHave) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putBoolean("OnlineMeetingCheck", isHave);
    }

    public static String getCheckAuth(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        LogUtil.d("checkAuth:", mInfoUtil.getString("checkAuth"));
        return mInfoUtil.getString("checkAuth");
    }
    private static String getUserPermissIons(Context context){
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("permissions");
    }

    private static String getUserRoles(Context context){
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("roles");
    }

    /**
     * 判断用户是否有某种权限
     * @param context
     * @param permiss   权限类型对应的code
     * @return  true：有权限；false：无权限
     */
    public static boolean getUserIsHavePermiss(Context context, UserPermissions permiss){
        String permissIons = getUserPermissIons(context);
        if (TextUtils.isEmpty(permissIons)) {
            return false;
        } else {
            String[] peimissList = permissIons.split(",");
            boolean isHavePermiss = false;
            for (String aPeimissList : peimissList) {
                if (!TextUtils.isEmpty(aPeimissList) && permiss.getValue().equals(aPeimissList)) {
                    isHavePermiss = true;
                    break;
                }
            }
            return isHavePermiss;
        }
    }

    /**
     * 判断用户是否有某种角色
     * @param roles   角色类型对应的code
     * @return  true：有；false：无
     */
    public static boolean getUserIsHaveRoles(Context context, UserRoles roles){
        String rolesStr = getUserRoles(context);
        if (TextUtils.isEmpty(rolesStr)) {
            return false;
        } else {
            String[] roleList = rolesStr.split(",");
            boolean isHavePermiss = false;
            for (String aRoleList : roleList) {
                if (!TextUtils.isEmpty(aRoleList) && roles.getValue().equals(aRoleList)) {
                    isHavePermiss = true;
                    break;
                }
            }
            return isHavePermiss;
        }
    }

    /**
     * 清除缓存
     */
    public static void logout() {
       // mInfoUtil.putString("userId", "");
        mInfoUtil.putString("sign", "");
        mInfoUtil.putString("userFrontRole", "");
        mInfoUtil.putString("userMobile", "");
        mInfoUtil.putString("token", "");
      //  mInfoUtil.putString("userId", "");
        mInfoUtil.putString("splashTime", "");
        mInfoUtil.putString("imtoken", "");
        mInfoUtil.putString("avatar", "");
        mInfoUtil.putString("userPhotoUrl", "");
        mInfoUtil.putString("roomId", "");
        mInfoUtil.putString("roomCode", "");
        mInfoUtil.putString("trueName", "");
        mInfoUtil.putString("registeredCapital", "");
        mInfoUtil.putString("userLevel", "");
        mInfoUtil.putString("inviteCode", "");
        mInfoUtil.putBoolean("IsFirstShowPop", true);
        mInfoUtil.putBoolean("savaIsFirstShowArrPop", true);
        mInfoUtil.putString("photoStatus", "0");
        mInfoUtil.putString("coin", "");
        mInfoUtil.putString("userRole", "");
        mInfoUtil.putString("idCard", "");
        mInfoUtil.putInt("employeeFlag", 0);
        mInfoUtil.putString("access_token","");

        //地区相关
        mInfoUtil.putString("city", "");
        mInfoUtil.putString("country", "");
        mInfoUtil.putString("provincy", "");
        mInfoUtil.putString("cityId", "");
        mInfoUtil.putString("industryId", "");

        //行业
        mInfoUtil.putString("industry", "");
        //是否上传通讯录
        mInfoUtil.putString("UpContactsStatus", "");
        //私人秘书
        mInfoUtil.putString("servantName", "");
        mInfoUtil.putString("servantMobile", "");
        mInfoUtil.putString("h5Token", "");
        mInfoUtil.putString("inviteCode", "");
        mInfoUtil.putInt("contactSize", -1);
        // 关注好友数量
        mInfoUtil.putInt("attentionCount", 0);
        mInfoUtil.putBoolean("isNeedUpIdenticationInfo", false);
        mInfoUtil.putBoolean("savaIsWebJump", true);

        // oss相关，防止更换账号后上传文件异常
        mInfoUtil.putLong("updateStsTime", 0);

        // 清除网上天洽会小红点
        mInfoUtil.putBoolean("OnlineMeetingCheck", false);
        // 权限相关
        mInfoUtil.putString("checkAuth", "");
        mInfoUtil.putString("companyCode", "");
        mInfoUtil.putString("company", "");
        mInfoUtil.putString("permissions", "");
        mInfoUtil.putString("roles", "");

        mInfoUtil.putInt("ifDistributor",0);
        mInfoUtil.putInt("distributorLevel",0);
        mInfoUtil.putString("inviteUrl","");
        mInfoUtil.putString("allMembers", "");
        mInfoUtil.putString("todayIncrease", "");
        mInfoUtil.putString("teamMembers", "");
        mInfoUtil.putString("monthIncome", "");
        mInfoUtil.putString("todayIncome", "");
        mInfoUtil.putString("totalIncome","");
        mInfoUtil.putString("rate","");
        mInfoUtil.putInt("isCompanyMemberShare",0);

        setNowEnvironment(BaseLibKit.getContext(),0);

    }

    public static String getDeviceId(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("tjDeviceId");
    }

    public static void setDeviceId(Context context, String tjDeviceId) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putString("tjDeviceId", tjDeviceId);
    }


    public static void setIMAccountHeaderBase(Context context, String accountHeaderStr) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putString("imaccountHeader", accountHeaderStr);
    }

    public static void saveIMTokenBase(Context context, String token) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putString("imtoken", token);
    }

    public static boolean getLiveTip1ForHost(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getBoolean("liveTip1ForHost", false);
    }

    public static void saveLiveTip1ForHost(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putBoolean("liveTip1ForHost", true);
    }

    public static boolean getLiveTip1ForHelper(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getBoolean("liveTip1ForHelper", false);
    }

    public static void saveLiveTip1ForHelper(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putBoolean("liveTip1ForHelper", true);
    }

    public static boolean getLiveTip2(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getBoolean("liveTip2", false);
    }

    public static void saveLiveTip2(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putBoolean("liveTip2", true);
    }


    public static void saveIsFirstVersion(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putBoolean("isFirstVersion", false);
    }

    public static boolean getIsFirstVersion(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getBoolean("isFirstVersion", true) && (BuildConfig.VERSION_NAME.equals("1.2.0"));
    }

    public static String getAccessToken(Context context){
        mInfoUtil = getPreferencesUtil(context);

        return mInfoUtil.getString("access_token");

    }

    public static boolean getiSDistributor(Context context){
        mInfoUtil = getPreferencesUtil(context);
        int ifDistributor =  mInfoUtil.getInt("ifDistributor",0);
        return ifDistributor != 0;
    }

    public static int getDistributorLevel(Context context){
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getInt("distributorLevel",0);
    }

    //获取个税费率
    public static String getDistributionRate(Context context){
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("rate");
    }

    /**
     * 本企业设置是否共享员工
     */
    public static boolean getIsShareStaff(Context context){
        mInfoUtil = getPreferencesUtil(context);
        int isCompanyMemberShare =  mInfoUtil.getInt("isCompanyMemberShare",0);
        if (isCompanyMemberShare == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 保存我的会员数据
     * @param mineDistributionModel
     * @param context
     */
    public static void setUserDistributionData(Context context, MineDistributionResponse.MineDistributionModel mineDistributionModel) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putString("inviteUrl", mineDistributionModel.inviteUrl);
        mInfoUtil.putString("allMembers", mineDistributionModel.allMembers);
        mInfoUtil.putString("todayIncrease", mineDistributionModel.todayIncrease);
        mInfoUtil.putString("teamMembers", mineDistributionModel.teamMembers);
        mInfoUtil.putString("monthIncome", mineDistributionModel.monthIncome);
        mInfoUtil.putString("todayIncome", mineDistributionModel.todayIncome);
        mInfoUtil.putString("totalIncome",mineDistributionModel.totalIncome);
        mInfoUtil.putInt("distributorLevel",mineDistributionModel.level);
        mInfoUtil.putString("rate",mineDistributionModel.rate);
        mInfoUtil.putInt("isCompanyMemberShare",mineDistributionModel.isCompanyMemberShare);

    }

    public static MineDistributionResponse.MineDistributionModel getUserDistributiionData(Context context){
        mInfoUtil = getPreferencesUtil(context);
        MineDistributionResponse.MineDistributionModel model = new MineDistributionResponse.MineDistributionModel();
        model.inviteUrl = mInfoUtil.getString("inviteUrl");
        model.allMembers = mInfoUtil.getString("allMembers");
        model.todayIncrease = mInfoUtil.getString("todayIncrease");
        model.teamMembers = mInfoUtil.getString("teamMembers");
        model.monthIncome = mInfoUtil.getString("monthIncome");
        model.todayIncome = mInfoUtil.getString("todayIncome");
        model.totalIncome = mInfoUtil.getString("totalIncome");
        model.level = mInfoUtil.getInt("distributorLevel");
        model.rate=mInfoUtil.getString("rate");
        model.isCompanyMemberShare=mInfoUtil.getInt("isCompanyMemberShare");
        return model;
    }

    public static String getUserInviteUrl(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("inviteUrl");
    }

    public static void setNowEnvironment(Context context, int environment) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putInt("NowEnvironment", environment);
        Log.w("setEnvironment........" ,environment + "");
    }

    public static int getNowEnvironment(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        Log.w("getEnvironment......" ,mInfoUtil.getInt("NowEnvironment") + "");
        return mInfoUtil.getInt("NowEnvironment");
    }

    /**
     * 存取个推clientid
     */
    public static String getClientId(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("clientId");
    }

    public static void saveClientId(Context context, String clientId) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putString("clientId", clientId);
    }

}
