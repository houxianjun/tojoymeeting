package com.tojoy.tjoybaselib.services.update;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * Created by Chengyanfang on 2017/10/31.
 */

public class UpdateModel extends OMBaseResponse {

    public ExtDataEntity data;

    public static class ExtDataEntity {
        public String url;
        int compel;
        String brief;
    }
}
