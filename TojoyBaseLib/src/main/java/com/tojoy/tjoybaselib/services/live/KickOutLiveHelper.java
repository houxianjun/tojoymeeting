package com.tojoy.tjoybaselib.services.live;

import android.content.Context;
import android.text.TextUtils;

import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;

import rx.Observer;

/**
 * 账号被踢掉的逻辑处理
 * @author qll
 */
public class KickOutLiveHelper {

    private static boolean isRequestIng = false;

    public static void doLiveSomething(Context context) {
        if (TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().liveId)) {
            // 直播间不存在
            return;
        }
        if (isRequestIng) {
            return;
        }
        isRequestIng = true;
        String userId  = BaseUserInfoCache.getUserId(context);
        if(LiveRoomInfoProvider.getInstance().isHost() && !LiveRoomInfoProvider.getInstance().isOfficalLive()) {
            OMAppApiProvider.getInstance().mStopExploreMe(false,BaseUserInfoCache.getUserId(context),
                    LiveRoomInfoProvider.getInstance().liveId, new Observer<OMBaseResponse>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(OMBaseResponse response) {

                        }
                    });
            OMAppApiProvider.getInstance().leaveLiveRoom(false,LiveRoomInfoProvider.getInstance().liveId, BaseUserInfoCache.getUserName(context), BaseUserInfoCache.getUserHeaderPic(context),
                    BaseUserInfoCache.getCompanyName(context), BaseUserInfoCache.getJobName(context),userId, new Observer<OMBaseResponse>() {
                        @Override
                        public void onCompleted() {
                            OMAppApiProvider.getInstance().endLive(false,LiveRoomInfoProvider.getInstance().liveId,userId, new Observer<OMBaseResponse>() {
                                @Override
                                public void onCompleted() {
                                    isRequestIng = false;

                                }

                                @Override
                                public void onError(Throwable e) {
                                    isRequestIng = false;

                                }

                                @Override
                                public void onNext(OMBaseResponse baseResponse) {

                                }
                            });
                        }

                        @Override
                        public void onError(Throwable e) {
                        }

                        @Override
                        public void onNext(OMBaseResponse baseResponse) {

                        }
                    });

        } else {
            OMAppApiProvider.getInstance().leaveLiveRoom(false,LiveRoomInfoProvider.getInstance().liveId, BaseUserInfoCache.getUserName(context), BaseUserInfoCache.getUserHeaderPic(context),
                    BaseUserInfoCache.getCompanyName(context), BaseUserInfoCache.getJobName(context),userId, new Observer<OMBaseResponse>() {
                        @Override
                        public void onCompleted() {
                            isRequestIng = false;
                        }

                        @Override
                        public void onError(Throwable e) {
                            isRequestIng = false;
                        }

                        @Override
                        public void onNext(OMBaseResponse baseResponse) {
                            if (baseResponse.isSuccess()) {

                            }
                        }
                    });
        }

    }

    public static void doLiveSomething(Context context,KickOutLiveCallBack callBack) {
        if (TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().liveId)) {
            // 直播间不存在
            callBack.OnSuccess();
            return;
        }
        if (isRequestIng) {
            callBack.OnSuccess();
            return;
        }
        isRequestIng = true;
        String userId  = BaseUserInfoCache.getUserId(context);
        if(LiveRoomInfoProvider.getInstance().isHost() && !LiveRoomInfoProvider.getInstance().isOfficalLive()) {
            OMAppApiProvider.getInstance().mStopExploreMe(false,BaseUserInfoCache.getUserId(context),
                    LiveRoomInfoProvider.getInstance().liveId, new Observer<OMBaseResponse>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(OMBaseResponse response) {

                        }
                    });
            OMAppApiProvider.getInstance().leaveLiveRoom(false,LiveRoomInfoProvider.getInstance().liveId, BaseUserInfoCache.getUserName(context), BaseUserInfoCache.getUserHeaderPic(context),
                    BaseUserInfoCache.getCompanyName(context), BaseUserInfoCache.getJobName(context),userId, new Observer<OMBaseResponse>() {
                        @Override
                        public void onCompleted() {
                            OMAppApiProvider.getInstance().endLive(false,LiveRoomInfoProvider.getInstance().liveId,userId, new Observer<OMBaseResponse>() {
                                @Override
                                public void onCompleted() {
                                    isRequestIng = false;

                                }

                                @Override
                                public void onError(Throwable e) {
                                    isRequestIng = false;

                                }

                                @Override
                                public void onNext(OMBaseResponse baseResponse) {

                                }
                            });
                            callBack.OnSuccess();
                        }

                        @Override
                        public void onError(Throwable e) {
                        }

                        @Override
                        public void onNext(OMBaseResponse baseResponse) {
                            callBack.OnSuccess();
                        }
                    });

        } else {
            OMAppApiProvider.getInstance().leaveLiveRoom(false,LiveRoomInfoProvider.getInstance().liveId, BaseUserInfoCache.getUserName(context), BaseUserInfoCache.getUserHeaderPic(context),
                    BaseUserInfoCache.getCompanyName(context), BaseUserInfoCache.getJobName(context),userId, new Observer<OMBaseResponse>() {
                        @Override
                        public void onCompleted() {
                            isRequestIng = false;
                        }

                        @Override
                        public void onError(Throwable e) {
                            isRequestIng = false;
                        }

                        @Override
                        public void onNext(OMBaseResponse baseResponse) {
                            if (baseResponse.isSuccess()) {
                                callBack.OnSuccess();
                            }
                        }
                    });
        }

    }

    public interface KickOutLiveCallBack{
        public void OnSuccess();
        public void OnFail();
    }
}
