package com.tojoy.tjoybaselib.services.OnlineMeeting;

/**
 * @author qll
 * @date 2019/4/16
 * 开会申请状态码控制
 */
public enum ApplyStateCode {
    /**
     * 未知
     */
    Unkown(0),
    /**
     * 审核中(待审核)(申请中)
     */
    ApplyReview(1),
    /**
     * 审核失败(被驳回)
     */
    ApplyError(2),
    /**
     * 审核成功(已通过)
     */
    ApplySuccess(3),
    /**
     * 已完成
     */
    ApplyOver(4),
    /**
     * 已撤回
     */
    ApplyReCall(5),;

    private int value;

    ApplyStateCode(int netState) {
        this.value = netState;
    }

    public int getValue() {
        return value;
    }

    /**
     * 1、申请中  2、被驳回  3 已通过  4已完成(本次申请通过并且直播完成)
     * @param value
     * @return
     */
    public static ApplyStateCode getStateCode(int value) {
        switch (value) {
            case 0:
                return Unkown;
            case 1:
                return ApplyReview;
            case 2:
                return ApplyError;
            case 3:
                return ApplySuccess;
            case 4:
                return ApplyOver;
            case 5:
                return ApplyReCall;
            default:
                return Unkown;
        }
    }
}
