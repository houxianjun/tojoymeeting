package com.tojoy.tjoybaselib.services.log;

/**
 * @author qll
 * @date 2019/9/12
 */
public class BaseLiveLog {
    //BaseLiveAct
    public static final String BaseLiveTag = "BaseLiveLog";
    //SeeLiveAct
    public static final String SeeLiveTag = "SeeLiveLog";

    //进出直播间
    public static final String IOLiveTag = "IOLive";

    //音视频窗体集合
    public static final String TRTCVideoLayoutTag = "TRTCVideo";

    //直播间消息
    public static final String LRMsgTag = "LRMsgTag";

    //直播间接口请求
    public static final String LRApiRequestTag = "LRApiRequest";

    //文档相关
    public static final String WhiteboardTag = "Whiteboard";

    //布局相关
    public static final String TRTCLayout = "TRTCLayout";
}
