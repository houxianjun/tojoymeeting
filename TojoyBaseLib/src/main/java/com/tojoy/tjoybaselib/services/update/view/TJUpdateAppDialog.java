package com.tojoy.tjoybaselib.services.update.view;

import android.app.Dialog;
import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.R;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.util.sys.AppUtils;

public class TJUpdateAppDialog {
    private Context mContext;

    private View mView;

    private Dialog alertDialog;
    //取消监听
    private TJMakeSureDialog.OnDialogCancelListener mCancelListener;
    // 非强制更新布局
    private RelativeLayout rlContentSelect;
    // 强制更新布局
    private RelativeLayout rlContentCorfirm;

    public TJUpdateAppDialog(Context context) {
        mContext = context;
        initUI();
    }

    public TJUpdateAppDialog(Context context, View.OnClickListener onClickListener) {
        mContext = context;
        initUI();
        initEvent(onClickListener, true);
    }

    private void initEvent(View.OnClickListener onClickListener, boolean isCanDismiss) {
        mView.findViewById(R.id.rlv_sure).setOnClickListener(v -> {
            if (isCanDismiss) {
                alertDialog.dismiss();
            }
            if (onClickListener != null) {
                onClickListener.onClick(v);
            }
        });
        mView.findViewById(R.id.rlv_cancle).setOnClickListener(v -> {
            if (isCanDismiss) {
                alertDialog.dismiss();
            }
            if (mCancelListener != null) {
                mCancelListener.onCancelClick();
            }
        });
    }

    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.popup_update_app_layout, null);
        rlContentSelect = mView.findViewById(R.id.rl_content_select);
        rlContentCorfirm = mView.findViewById(R.id.rl_content_corfirm);
        ((TextView) mView.findViewById(R.id.tv_content)).setMovementMethod(ScrollingMovementMethod.getInstance());
    }

    public void show() {

        if (mView.getParent() != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            parent.removeAllViews();
        }

        alertDialog = new Dialog(mContext, R.style.tjoy_dialog_style);
        alertDialog.setContentView(mView);
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        alertDialog.setOnKeyListener((dialog, keyCode, event) -> true);
        //新增dialog整体宽高设置 适配ui
        Window win = alertDialog.getWindow();
        win.getDecorView().setPadding(0, 0, 0, 0);
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        win.setAttributes(lp);

    }

    public void show(int marginWidth) {
        if (mView.getParent() != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            parent.removeAllViews();
        }

        alertDialog = new Dialog(mContext, R.style.tjoy_dialog_style);
        alertDialog.setContentView(mView, new ViewGroup.LayoutParams(AppUtils.getWidth(mContext) - AppUtils.dip2px(mContext, marginWidth), ViewGroup.LayoutParams.WRAP_CONTENT));
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        alertDialog.setOnKeyListener((dialog, keyCode, event) -> true);
    }

    /**
     * ****************** UI定制化
     */
    public TJUpdateAppDialog setTitleAndCotent(String title, String content) {
        ((TextView) mView.findViewById(R.id.tv_title)).setText(title);
        ((TextView) mView.findViewById(R.id.tv_content)).setText(content);
          return this;
    }

    /**
     * 提示内容显示两行并居中
     *
     * @param content
     * @param hint
     * @return
     */
    public TJUpdateAppDialog setcontent(String content, String hint) {
        mView.findViewById(R.id.rlv_contenthint_layout).setVisibility(View.VISIBLE);
        ((TextView) mView.findViewById(R.id.tv_content_meeting)).setText(content);
        ((TextView) mView.findViewById(R.id.tv_content_hint)).setText(hint);
        return this;
    }



    /**
     * 显示提示框
     */
    public void showAlert() {
        //显示强制更新布局
        rlContentCorfirm.setVisibility(View.VISIBLE);
        //隐藏非强制更新布局
        rlContentSelect.setVisibility(View.GONE);
        show();
    }



    public TJUpdateAppDialog hideTitle() {
        mView.findViewById(R.id.rlv_title_layout).setVisibility(View.GONE);
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lps.setMargins(0, AppUtils.dip2px(mContext, 35), 0, AppUtils.dip2px(mContext, 35));
        lps.addRule(RelativeLayout.CENTER_HORIZONTAL);
        (mView.findViewById(R.id.tv_content)).setLayoutParams(lps);
        ((TextView) mView.findViewById(R.id.tv_content)).setTextSize(15);

        return this;
    }



    /**
     * 设置按钮文字
     */

    public TJUpdateAppDialog setBtnText(String sure, String cancle) {
        ((TextView) mView.findViewById(R.id.button_sure)).setText(sure);
        ((TextView) mView.findViewById(R.id.button_cancle)).setText(cancle);
        return this;
    }


    /**
     * 显示确认框
     */
    public void showConfirm() {
        //隐藏强制更新布局
        rlContentCorfirm.setVisibility(View.GONE);
        //显示非强制更新布局
        rlContentSelect.setVisibility(View.VISIBLE);

        show();
    }

    /**
     * 判断是否在展示
     *
     * @return
     */
    public boolean isShowing() {
        if (alertDialog == null) {
            return false;
        }
        return alertDialog.isShowing();
    }

    public void dismiss() {
        if (isShowing()) {
            alertDialog.dismiss();
        }
    }


    //设置取消监听
    public TJUpdateAppDialog setCancelListener(TJMakeSureDialog.OnDialogCancelListener mCancelListener) {
        this.mCancelListener = mCancelListener;
        return this;
    }


    public void setEvent(View.OnClickListener onClickListener) {
        mView.findViewById(R.id.button_confirm_update).setOnClickListener(onClickListener);
    }
}
