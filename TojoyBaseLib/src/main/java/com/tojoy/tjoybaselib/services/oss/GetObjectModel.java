package com.tojoy.tjoybaselib.services.oss;

import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.OSS;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.common.OSSLog;
import com.alibaba.sdk.android.oss.model.GetObjectRequest;
import com.alibaba.sdk.android.oss.model.GetObjectResult;

import java.io.IOException;

/**
 * Created by chengyanfang on 2017/9/27.
 */

class GetObjectModel {

    private OSS oss;
    private String mBucket;
    private String mObject;

    GetObjectModel(OSS client, String bucket, String object) {
        this.oss = client;
        this.mBucket = bucket;
        this.mObject = object;
    }

    void getObject(final Callback<GetObjectRequest, GetObjectResult> callback) {

        // Constructs the GetObjectRequest.
        GetObjectRequest get = new GetObjectRequest(mBucket, mObject);
        GetObjectResult getResult = null;
        try {
            // Download the file in the synchronous way
            getResult = oss.getObject(get);
            callback.onSuccess(get, getResult);
        } catch (ClientException e) {
            // Client side exceptions, such as network exception
            e.printStackTrace();
            callback.onFailure(get, e, null);
        } catch (ServiceException e) {
            // Service side exception
            OSSLog.logError("RequestId:" + e.getRequestId(), false);
            OSSLog.logError("ErrorCode:" + e.getErrorCode(), false);
            OSSLog.logError("HostId:" + e.getHostId(), false);
            OSSLog.logError("RawMessage:" + e.getRawMessage(), false);
            callback.onFailure(get, null, e);
        } finally {
            if (getResult != null) {
                try {
                    getResult.getObjectContent().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
