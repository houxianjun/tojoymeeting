package com.tojoy.tjoybaselib.services.oss;

/**
 * Created by chengyanfang on 2017/9/28.
 */

public interface ProgressCallback<T1,T2> extends Callback<T1,T2> {
    void onProgress(T1 request, long currentSize, long totalSize);
}