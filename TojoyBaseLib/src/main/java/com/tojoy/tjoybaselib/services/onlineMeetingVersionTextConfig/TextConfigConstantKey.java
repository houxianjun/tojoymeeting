package com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig;

/**
 * @author fanxi
 * @date 2020-03-06.
 * description：企业配置key
 */
public class TextConfigConstantKey {

    //随时随地
    static final String SLOGAN1 = "slogan1";

    //开启视频会销
    static final String SLOGAN2 = "slogan2";

    //发布、反馈、洽谈、支付全OK
    static final String SLOGAN3 = "slogan3";


    //导航-首页
    static final String BAR1 = "bar1";

    //导航-会议
    static final String BAR2 = "bar2";

    //导航-消息
    static final String BAR3 = "bar3";

    //导航-我的
    static final String BAR4 = "bar4";

    //会议
    public static final String MEETING_TITLE = "meetingTitle";

    //直播间-评论
    public static final String MEETIGN_BAR1 = "meetingBar1";

    //直播间-分享
    public static final String MEETING_BAR2 = "meetingBar2";

    //直播间-观众
    public static final String MEETING_BAR3 = "meetingBar3";


    //直播间-企业
    public static final String MEETING_BAR4 = "meetingBar4";

    //列表
    public static final String MEETING_BAR5 = "meetingBar5";

    //我感兴趣
    public static final String PRODUCT1 = "product1";

    //意向购买
    public static final String PRODUCT2 = "product2";

    //立即购买
    public static final String PRODUCT3 = "product3";

    //视频互动
    public static final String INTER_ACTION1 = "interAction1";

    //发起收款
    public static final String INTER_ACTION4 = "interAction4";

    //引流会议画面
    public static final String INTER_ACTION5 = "interAction5";

}
