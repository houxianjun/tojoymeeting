package com.tojoy.tjoybaselib.services.imageManager;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.tojoy.tjoybaselib.R;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.ui.imageview.CircleBorderTransform;

import java.io.File;

import jp.wasabeef.glide.transformations.CropTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

import static com.bumptech.glide.load.resource.bitmap.VideoDecoder.FRAME_OPTION;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * 图片处理管理器
 */

public enum ImageLoaderManager {
    /**
     * 枚举类型数据
     */
    INSTANCE;

    /**
     * 加载图片
     *
     * @param imageView
     * @param placeHolder
     * @param imgUrl
     */
    public void loadImage(Context context, ImageView imageView, String imgUrl, int placeHolder, int radiu) {
        // 防止加载图片闪烁
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(placeHolder)
                .transform(new GlideRoundTransform(context, radiu))
                .disallowHardwareConfig();
        Glide.with(context)
                .load(OSSConfig.getOSSURLedStr(imgUrl))
                .apply(requestOptions)
                .into(imageView);
    }


    /**
     * 自定义加载头像，解决glide加载闪烁问题
     *
     * @param context
     * @param imageView requestOptions.disallowHardwareConfig();
     * @param imgUrl
     */
    public void loadHeadImage(Context context, ImageView imageView, String imgUrl, int placeHolder) {
        if (placeHolder == 0) {
            RequestOptions requestOptions = new RequestOptions()
                    .disallowHardwareConfig();
            Glide.with(context)
                    .load(OSSConfig.getOSSURLedStr(imgUrl))
                    .apply(requestOptions)
                    .into(imageView);
        } else {
            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(placeHolder)
                    .disallowHardwareConfig();
            try {
                Glide.with(context)
                        .load(OSSConfig.getOSSURLedStr(imgUrl))
                        .apply(requestOptions)
                        .into(imageView);
            } catch (Exception e) {
            }
        }
    }


    /**
     * 加载图片 没有默认图片
     *
     * @param imageView
     * @param imgUrl
     */

    public void loadImage(Context context, ImageView imageView, String imgUrl, int radiu) {
        RequestOptions requestOptions = new RequestOptions()
                .centerCrop()
                .priority(Priority.HIGH)
                .dontAnimate()
                .transform(new MultiTransformation<>(new GlideRoundTransform(context, radiu), new CenterCrop()));

        try {
            Glide.with(context)
                    .load(OSSConfig.getOSSURLedStr(imgUrl))
                    .apply(requestOptions)
                    .transition(withCrossFade())
                    .into(imageView);
        } catch (Exception e) {

        }
    }

    /**
     * 加载图片 没有默认图片
     *
     * @param imageView
     * @param imgUrl
     */

    public void loadImage(Context context, ImageView imageView, Bitmap imgUrl, int radiu) {
        RequestOptions requestOptions = new RequestOptions()
                .centerCrop()
                .priority(Priority.HIGH)
                .dontAnimate()
                .transform(new MultiTransformation<>(new GlideRoundTransform(context, radiu), new CenterCrop()));

        try {
            Glide.with(context)
                    .load(imgUrl)
                    .apply(requestOptions)
                    .transition(withCrossFade())
                    .into(imageView);
        } catch (Exception e) {

        }
    }


    /**
     * 加载图片
     * 不改变原图像比例缩放图片
     *
     * @param imageView
     * @param imgUrl
     */
    public void loadImageEqualProportion(Context context, ImageView imageView, String imgUrl, int radiu, int width) {

        RequestOptions requestOptions = new RequestOptions()
                .disallowHardwareConfig()
                .dontAnimate();

        Glide.with(context).load(OSSConfig.getOSSURLedStr(imgUrl)).apply(requestOptions).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                int imageWidth = resource.getIntrinsicWidth();
                int imageHeight = resource.getIntrinsicHeight();
                int height = width * imageHeight / imageWidth;
                ViewGroup.LayoutParams para = imageView.getLayoutParams();
                para.height = height;
                para.width = width;
                imageView.setImageDrawable(resource);
            }
        });
    }

    /**
     * 加载圆形图片
     *
     * @param imageView
     * @param placeHolder
     * @param imgUrl
     */
    public void loadCircleImage(Context context, ImageView imageView, String imgUrl, int placeHolder) {
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(placeHolder)
                .priority(Priority.HIGH)
                .dontAnimate()
                .transform(new MultiTransformation<>(new GlideCircleTransform(context)));

        Glide.with(context)
                .load(OSSConfig.getOSSURLedStr(imgUrl))
                .apply(requestOptions)
                .transition(withCrossFade())
                .into(imageView);
    }

    /**
     * 加载指定大小的图片
     *
     * @param context
     * @param imageView
     * @param imgUrl
     * @param placeHolder
     * @param radiu
     * @param width
     * @param height
     */
    public void loadSizedImage(Context context, ImageView imageView, String imgUrl, int placeHolder, int radiu, int width, int height) {
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(placeHolder)
                .priority(Priority.HIGH)
                .override(width, height)
                .transform(new MultiTransformation<>(new CropTransformation(width, height, CropTransformation.CropType.CENTER), new RoundedCornersTransformation(radiu, 0, RoundedCornersTransformation.CornerType.ALL)));

        Glide.with(context)
                .load(OSSConfig.getOSSURLedStr(imgUrl))
                .apply(requestOptions)
                .transition(withCrossFade())
                .into(imageView);
    }

    /**
     * 加载指定大小圆形图片
     *
     * @param imageView
     * @param placeHolder
     * @param imgUrl
     */
    public void loadCircleImage(Context context, ImageView imageView, String imgUrl, int placeHolder, int width, int height) {
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(placeHolder)
                .priority(Priority.HIGH)
                .override(width, height)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .transform(new MultiTransformation<>(new GlideCircleTransform(context)));

        Glide.with(context)
                .load(OSSConfig.getOSSURLedStr(imgUrl))
                .apply(requestOptions)
                .transition(withCrossFade())
                .into(imageView);
    }


    /**
     * 上面两角为圆角，下面为直角
     *
     * @param context
     * @param imageView
     * @param imgUrl
     * @param radiu
     * @param width
     * @param height
     */
    public void loadCoverSizedImage(Context context, ImageView imageView, String imgUrl, int radiu, int width, int height, int placeHolder) {
        RequestOptions requestOptions = new RequestOptions()
                .centerCrop()
                .priority(Priority.HIGH)
                .placeholder(placeHolder)
                .override(width, height)
                .transform(new MultiTransformation<>(new CropTransformation(width, height, CropTransformation.CropType.CENTER), new RoundedCornersTransformation(radiu, 0, RoundedCornersTransformation.CornerType.TOP)));

        Glide.with(context)
                .load(OSSConfig.getOSSURLedStr(imgUrl))
                .apply(requestOptions)
                .transition(withCrossFade())
                .into(imageView);
    }

    /**
     * 上面两角为圆角，下面为直角
     *
     * @param context
     * @param imageView
     * @param imgUrl
     * @param radiu
     * @param width
     * @param height
     */
    public void loadCoverLeftSizedImage(Context context, ImageView imageView, String imgUrl, int radiu, int width, int height, int placeHolder) {
        RequestOptions requestOptions = new RequestOptions()
                .centerCrop()
                .priority(Priority.HIGH)
                .placeholder(placeHolder)
                .override(width, height)
                .transform(new MultiTransformation<>(new CropTransformation(width, height, CropTransformation.CropType.CENTER), new RoundedCornersTransformation(radiu, 0, RoundedCornersTransformation.CornerType.LEFT)));

        Glide.with(context)
                .load(OSSConfig.getOSSURLedStr(imgUrl))
                .apply(requestOptions)
                .transition(withCrossFade())
                .into(imageView);
    }

    public void loadRadiuTop(Context context, ImageView imageView, String imgUrl, int radiu, int placeHolder) {
        // 防止加载图片闪烁
        RequestOptions requestOptions = new RequestOptions()
                .centerCrop()
                .priority(Priority.HIGH)
                .placeholder(placeHolder)
                .transform(new MultiTransformation<>(new CropTransformation(0, 0, CropTransformation.CropType.CENTER), new RoundedCornersTransformation(radiu, 0, RoundedCornersTransformation.CornerType.LEFT)))
                .disallowHardwareConfig();
        Glide.with(context)
                .load(OSSConfig.getOSSURLedStr(imgUrl))
                .apply(requestOptions)
                .transition(withCrossFade())
                .into(imageView);
    }

    /**
     * 上面两角为直角，下面为圆角
     *
     * @param context
     * @param imageView
     * @param imgUrl
     * @param radiu
     * @param width
     * @param height
     */
    public void loadUnderSizedImage(Context context, ImageView imageView, String imgUrl, int radiu, int width, int height) {
        RequestOptions requestOptions = new RequestOptions()
                .centerCrop()
                .priority(Priority.HIGH)
                .override(width, height)
                .transform(new MultiTransformation<>(new CropTransformation(width, height, CropTransformation.CropType.CENTER), new RoundedCornersTransformation(radiu, 0, RoundedCornersTransformation.CornerType.BOTTOM)));

        Glide.with(context)
                .load(OSSConfig.getOSSURLedStr(imgUrl))
                .apply(requestOptions)
                .transition(withCrossFade())
                .into(imageView);
    }

    /**
     * 上面两角为直角，下面为圆角
     *
     * @param context
     * @param imageView
     * @param imgUrl
     * @param radiu
     * @param width
     * @param height
     */
    public void loadSingleConorSizedImage(Context context, ImageView imageView, String imgUrl, int radiu, int width, int height, RoundedCornersTransformation.CornerType cornerType) {
        RequestOptions requestOptions = new RequestOptions()
                .centerCrop()
                .priority(Priority.HIGH)
                .override(width, height)
                .transform(new MultiTransformation<>(new CropTransformation(width, height, CropTransformation.CropType.CENTER), new RoundedCornersTransformation(radiu, 0, cornerType)));

        Glide.with(context)
                .load(OSSConfig.getOSSURLedStr(imgUrl))
                .apply(requestOptions)
                .transition(withCrossFade())
                .into(imageView);
    }

    /**
     * 上面两角为直角，下面为圆角
     *
     * @param context
     * @param imageView
     * @param imgUrl
     * @param radiu
     * @param width
     * @param height
     */
    public void loadSingleLeftConorSizedImage(Context context, ImageView imageView, String imgUrl, int radiu, int width, int height) {
        RequestOptions requestOptions = new RequestOptions()
                .centerCrop()
                .priority(Priority.HIGH)
                .override(width, height)
                .transform(new MultiTransformation<>(new CropTransformation(width, height, CropTransformation.CropType.CENTER), new RoundedCornersTransformation(radiu, 0, RoundedCornersTransformation.CornerType.LEFT)));

        Glide.with(context)
                .load(OSSConfig.getOSSURLedStr(imgUrl))
                .apply(requestOptions)
                .transition(withCrossFade())
                .into(imageView);
    }


    /**
     * 四个角都为圆角
     * 天洽会首页 图片圆角展示
     *
     * @param context
     * @param imageView
     * @param imgUrl
     * @param radiu
     * @param width
     * @param height
     */
    public void loadRoundCornerImage(Context context, ImageView imageView, String imgUrl, int radiu, int width, int height, int placeholder) {
        RequestOptions requestOptions = new RequestOptions()
                .centerCrop()
                .priority(Priority.HIGH)
                .placeholder(placeholder)
                .override(width, height)
                .transform(new MultiTransformation<>(new CropTransformation(width, height, CropTransformation.CropType.CENTER), new RoundedCornersTransformation(radiu, 0, RoundedCornersTransformation.CornerType.ALL)));

        Glide.with(context)
                .load(OSSConfig.getOSSURLedStr(imgUrl))
                .apply(requestOptions)
                .transition(withCrossFade())
                .into(imageView);
    }


    /**
     * 加载带有边框的图片
     *
     * @param context
     * @param imageView
     * @param imgUrl
     * @param placeHolder
     * @param radiu
     */
    public void loadBorderedImage(Context context, ImageView imageView, String imgUrl, int placeHolder, int radiu, int color) {
        RequestOptions requestOptions = new RequestOptions()
                .centerCrop()
                .placeholder(placeHolder)
                .error(placeHolder)
                .priority(Priority.HIGH)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .transform(new GlideCircleTransform(context, 1, color));


        Glide.with(context)
                .load(OSSConfig.getOSSURLedStr(imgUrl))
                .apply(requestOptions)
                .transition(withCrossFade())
                .into(imageView);
    }


    /**
     * 加载File图片
     *
     * @param context
     * @param file
     * @param imageView
     * @param placeHolder
     */
    public void loadFileImage(Context context, File file, ImageView imageView, int placeHolder) {
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(placeHolder)
                .priority(Priority.HIGH)
                .centerCrop();

        Glide.with(context)
                .load(file)
                .apply(requestOptions)
                .into(imageView);
    }

    /**
     * 上面两角为直角，下面为圆角
     *
     * @param context
     * @param imageView
     * @param imgUrl
     * @param radiu
     * @param width
     * @param height
     */
    public void loadSingleConorSizedImageAddPlaceHolder(Context context, ImageView imageView, String imgUrl, int radiu, int width, int height, int placeHolder, RoundedCornersTransformation.CornerType cornerType) {
        RequestOptions requestOptions = new RequestOptions()
                .centerCrop()
                .priority(Priority.HIGH)
                .error(placeHolder)
                .override(width, height)
                .transform(new MultiTransformation<>(new CropTransformation(width, height, CropTransformation.CropType.CENTER), new RoundedCornersTransformation(radiu, 0, cornerType)));

        Glide.with(context)
                .load(OSSConfig.getOSSURLedStr(imgUrl))
                .apply(requestOptions)
                .transition(withCrossFade())
                .into(imageView);
    }

    /**
     * 设置任意角为圆角（false为圆角，true为直角）
     *
     * @param context
     * @param imageView
     * @param imgUrl
     * @param placeHolder
     * @param radiu
     * @param leftTop
     * @param rightTop
     * @param leftBottom
     * @param rightBottom
     */
    public void loadWantonlyRadiu(Context context, ImageView imageView, String imgUrl, int placeHolder,int errorHolder, int radiu, boolean leftTop, boolean rightTop, boolean leftBottom, boolean rightBottom) {
        CornerTransform transformation = new CornerTransform(context, radiu);
        transformation.setExceptCorner(leftTop, rightTop, leftBottom, rightBottom);
        RequestOptions options = new RequestOptions()
                .error(errorHolder)
                .placeholder(placeHolder)
                .transforms(transformation)
                .disallowHardwareConfig()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
//                .asBitmap()
                .load(OSSConfig.getOSSURLedStr(OSSConfig.getRemoveStylePath((String) imgUrl)))
                .thumbnail(loadTransform(context, placeHolder, radiu))
                .apply(options)
                .into(imageView);
    }

    /**
     * 加载视频封面，设置任意角为圆角（false为圆角，true为直角）
     *
     * @param context
     * @param imageView
     * @param imgUrl
     * @param placeHolder
     * @param radiu
     * @param leftTop
     * @param rightTop
     * @param leftBottom
     * @param rightBottom
     */
    public void loadVideoScreenshot(Context context, ImageView imageView, String imgUrl, int placeHolder, int radiu, boolean leftTop, boolean rightTop, boolean leftBottom, boolean rightBottom, long frameTimeMicros) {
        RequestOptions requestOptions = RequestOptions.frameOf(frameTimeMicros);
        requestOptions.set(FRAME_OPTION, MediaMetadataRetriever.OPTION_CLOSEST);
        CornerTransform transformation = new CornerTransform(context, radiu);
        transformation.setExceptCorner(leftTop, rightTop, leftBottom, rightBottom);
        requestOptions.transform(transformation);
        Glide.with(context).load(OSSConfig.getOSSURLedStr(imgUrl))
                .thumbnail(loadTransform(context, placeHolder, radiu))
                .apply(requestOptions).into(imageView);

    }

    /**
     * 首页／热门企业 带有边线的圆形icon
     * @param context
     * @param imageView
     * @param imgUrl
     * @param placeHolder
     * @param color
     * @param width
     */
    public void loadHomeHotCompnayIcon(Context context, ImageView imageView,String imgUrl, int placeHolder,String color,int width){
        Glide.with(context)
                .load(OSSConfig.getOSSURLedStr(imgUrl))
                .apply(new RequestOptions().centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .transform(new CircleBorderTransform(context, width, Color.parseColor(color)))
                        .placeholder(placeHolder))
                .into(imageView);
    }


    /**
     * 对默认图片进行圆角处理
     *
     * @param context
     * @param placeholderId
     * @param radius
     * @return
     */
    private static RequestBuilder<Drawable> loadTransform(Context context, @DrawableRes int placeholderId, int radius) {

        return Glide.with(context)
                .load(placeholderId)
                .apply(new RequestOptions().centerCrop()
                        .transform(new GlideRoundTransform(context, radius)));

    }
}
