package com.tojoy.tjoybaselib.services.oss;

import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.OSS;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.callback.OSSCompletedCallback;
import com.alibaba.sdk.android.oss.callback.OSSProgressCallback;
import com.alibaba.sdk.android.oss.common.OSSLog;
import com.alibaba.sdk.android.oss.common.utils.BinaryUtil;
import com.alibaba.sdk.android.oss.internal.OSSAsyncTask;
import com.alibaba.sdk.android.oss.model.AppendObjectRequest;
import com.alibaba.sdk.android.oss.model.AppendObjectResult;
import com.alibaba.sdk.android.oss.model.DeleteObjectRequest;
import com.alibaba.sdk.android.oss.model.DeleteObjectResult;
import com.alibaba.sdk.android.oss.model.ObjectMetadata;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;

import java.io.IOException;
import java.util.Random;

/**
 * Created by chengyanfang on 2017/9/28.
 */

class PutObjectModel {

    private OSS oss;
    private String mBucket;
    private String mObject;
    private String uploadFilePath;

    PutObjectModel(OSS client, String testBucket, String testObject, String uploadFilePath) {
        this.oss = client;
        this.mBucket = testBucket;
        this.mObject = testObject;
        this.uploadFilePath = uploadFilePath;
    }


    // Upload from local files. Use asynchronous API
    OSSAsyncTask asyncPutObjectFromLocalFile(final ProgressCallback<PutObjectRequest, PutObjectResult> progressCallback) {
        PutObjectRequest put = new PutObjectRequest(mBucket, mObject, uploadFilePath);
        put.setProgressCallback(progressCallback::onProgress);

        return oss.asyncPutObject(put, new OSSCompletedCallback<PutObjectRequest, PutObjectResult>() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                progressCallback.onSuccess(request, result);
                OSSLog.logDebug("PutObject" + "UploadSuccess", true);
                OSSLog.logDebug("ETag" + result.getETag(), true);
                OSSLog.logDebug("RequestId" + result.getRequestId(), true);
            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientExcepion, ServiceException serviceException) {
                progressCallback.onFailure(request, clientExcepion, serviceException);
                // request exception
                if (clientExcepion != null) {
                    // client side exception,  such as network exception
                    clientExcepion.printStackTrace();
                }
                if (serviceException != null) {
                    // service side exception
                    OSSLog.logError("ErrorCode" + serviceException.getErrorCode(), true);
                    OSSLog.logError("RequestId" + serviceException.getRequestId(), true);
                    OSSLog.logError("HostId" + serviceException.getHostId(), true);
                    OSSLog.logError("RawMessage" + serviceException.getRawMessage(), true);
                }
            }
        });
    }
}
