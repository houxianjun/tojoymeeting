package com.tojoy.tjoybaselib.services.mobilecontacts;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.text.TextUtils;

import java.util.ArrayList;

public class MobileContactsProvider {


    private static MobileContactsProvider instance;

    private MobileContactsProvider() {
    }

    public static synchronized MobileContactsProvider getInstance() {
        if (instance == null) {
            instance = new MobileContactsProvider();
        }
        return instance;
    }


    /**
     * 获取通讯录联系人所需字段
     **/
    private static final String[] PHONES_PROJECTION = new String[]{
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.CONTACT_ID};
    private static final int PHONES_NAME_INDEX = 0;
    private static final int PHONES_NUMBER_INDEX = 1;
    private static final int PHONES_CONTACTID_INDEX = 2;


    /**
     * 获取手机通讯录 纯手机号
     *
     * @return
     */
    public ArrayList<ContactInfo> getPhoneContacts(Context context) {
        ArrayList<ContactInfo> contactInfos = new ArrayList<>();
        ContentResolver resolver = context.getContentResolver();
        // 获取手机联系人
        Cursor phoneCursor = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PHONES_PROJECTION, null, null, android.provider.ContactsContract.Contacts.SORT_KEY_PRIMARY);
        ContactInfo constactUser;
        if (phoneCursor != null) {
            while (phoneCursor.moveToNext()) {
                constactUser = new ContactInfo();

                //获取ContactId
                constactUser.contactId = phoneCursor.getString(PHONES_CONTACTID_INDEX);

                //得到手机号码
                String phoneNumber = phoneCursor.getString(PHONES_NUMBER_INDEX).replaceAll(" ", "").replaceAll("-", "");
                constactUser.mobile = phoneNumber;

                //当手机号码为空的或者为空字段 跳过当前循环
                if (TextUtils.isEmpty(phoneNumber)) {
                    continue;
                }

                //得到联系人名称
                String contactName = phoneCursor.getString(PHONES_NAME_INDEX);
                constactUser.trueName = contactName.replaceAll(" ", "").replaceAll("-", "");
                contactInfos.add(constactUser);
            }
            phoneCursor.close();
        }

        if (contactInfos.size() > 0) {
            ArrayList<ContactInfo> infoArrayList = new ArrayList<>();
            for (int i = 0; i < contactInfos.size(); i++) {
                if (isMobileNO(contactInfos.get(i).mobile)) {
                    infoArrayList.add(contactInfos.get(i));
                }
            }
            return infoArrayList;
        } else {
            return contactInfos;
        }
    }

    private boolean isMobileNO(String mobiles) {
        mobiles = mobiles.replaceAll(" ", "");

        if (TextUtils.isEmpty(mobiles)) return false;
        if (mobiles.startsWith("+86")) {
            mobiles = mobiles.substring(3, mobiles.length());
        }

        if (mobiles.startsWith("86")) {
            mobiles = mobiles.substring(2, mobiles.length());
        }

        if (mobiles.startsWith("0086")) {
            mobiles = mobiles.substring(4, mobiles.length());
        }

        String telRegex = "[1][34578]\\d{9}";
        return mobiles.matches(telRegex);
    }


    //去除+86 86 0086
    public String getSubString(String mobilds) {
        mobilds = mobilds.replaceAll(" ", "");
        if (mobilds.startsWith("+86")) {
            mobilds = mobilds.substring(3, mobilds.length());
        }

        if (mobilds.startsWith("86")) {
            mobilds = mobilds.substring(2, mobilds.length());
        }

        if (mobilds.startsWith("0086")) {
            mobilds = mobilds.substring(4, mobilds.length());
        }
        return mobilds;
    }

}
