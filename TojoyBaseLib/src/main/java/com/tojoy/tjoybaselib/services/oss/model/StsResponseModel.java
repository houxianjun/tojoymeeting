package com.tojoy.tjoybaselib.services.oss.model;

import com.google.gson.Gson;
import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * Created by chengyanfang on 2017/9/29.
 */

public class StsResponseModel extends OMBaseResponse {

    public TokenObject data;


    public class TokenObject {
        public String token;
    }

    public void saveKeyToLocal() {
        if (data != null) {
            OSSInfoCache.setSTSInfo(new Gson().fromJson(data.token, StsModel.class));
        }
    }
}
