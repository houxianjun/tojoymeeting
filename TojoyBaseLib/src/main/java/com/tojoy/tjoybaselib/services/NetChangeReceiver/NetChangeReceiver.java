package com.tojoy.tjoybaselib.services.NetChangeReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;


import org.greenrobot.eventbus.EventBus;


/**
 * Created by chengyanfang on 2018/4/4.
 */

public class NetChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo mobNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifiNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mobNetInfo == null || wifiNetInfo == null) {
            return;
        }

        if (!mobNetInfo.isConnected() && !wifiNetInfo.isConnected()) {
            EventBus.getDefault().post(new NetChangeEvent(0));
            Log.d("NetChangeReceiver", "onNetChange - 0");
        } else if (mobNetInfo.isConnected()) {
            EventBus.getDefault().post(new NetChangeEvent(1));
            Log.d("NetChangeReceiver", "onNetChange - 1");
        } else if (wifiNetInfo.isConnected()) {
            EventBus.getDefault().post(new NetChangeEvent(2));
            Log.d("NetChangeReceiver", "onNetChange - 2");
        }
    }

}
