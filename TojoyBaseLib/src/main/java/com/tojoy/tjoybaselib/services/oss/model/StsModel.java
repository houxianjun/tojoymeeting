package com.tojoy.tjoybaselib.services.oss.model;

/**
 * Created by chengyanfang on 2017/9/27.
 */

public class StsModel {
    public String RequestId;
    public AssumedRoleUser AssumedRoleUser;
    public Credentials Credentials;

    public StsModel(String AssumedRoleId, String Arn, String AccessKeySecret, String AccessKeyId, String SecurityToken) {
        AssumedRoleUser = new AssumedRoleUser(AssumedRoleId, Arn);
        Credentials = new Credentials(AccessKeySecret, AccessKeyId, SecurityToken);
    }

    public static class AssumedRoleUser {

        public AssumedRoleUser(String AssumedRoleId, String Arn) {
            this.AssumedRoleId = AssumedRoleId;
            this.Arn = Arn;
        }

        public String AssumedRoleId;
        public String Arn;
    }

    public static class Credentials {
        public Credentials(String AccessKeySecret, String AccessKeyId, String SecurityToken) {
            this.AccessKeyId = AccessKeyId;
            this.AccessKeySecret = AccessKeySecret;
            this.SecurityToken = SecurityToken;
        }

        public String AccessKeySecret;
        public String AccessKeyId;
        public String Expiration;
        public String SecurityToken;
    }

}
