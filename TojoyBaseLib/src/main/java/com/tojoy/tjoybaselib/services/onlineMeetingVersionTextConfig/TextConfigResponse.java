package com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig;

/**
 * @author fanxi
 * @date 2020-03-06.
 * description：全局控件文案替换资源以及默认文本
 */
public class TextConfigResponse {

    //默认：引流会议画面
    public  String interAction5;

    //默认：发起收款
    public String interAction4;

    //默认：意向购买
    public String product2;

    //默认：评论
    public String meetingBar1;

    //默认：我感兴趣
    public String product1;

    //默认：会议
    public String meetingTitle;

    //默认：分享
    public String meetingBar2;

    //默认：观众
    public String meetingBar3;

    //默认：企业
    public String meetingBar4;

    //默认：列表
    public String meetingBar5;

    //默认：消息
    public String bar3;

    //默认：我的
    public String bar4;

    //默认：立即购买
    public String product3;

    //默认：首页
    public String bar1;

    //默认：会议
    public String bar2;

    //默认：视频互动
    public String interAction1;

    //默认：随时随地开启视频会销\n发布、反馈、洽谈、支付全OK
    public String slogan1,slogan2,slogan3;


}
