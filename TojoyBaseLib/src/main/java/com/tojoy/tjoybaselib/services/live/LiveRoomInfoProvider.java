package com.tojoy.tjoybaselib.services.live;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;

import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.EnvironmentInstance;
import com.tojoy.tjoybaselib.model.user.UserInfoModel;
import com.tojoy.tjoybaselib.util.string.StringUtil;

import java.util.ArrayList;

/**
 * 当前直播间信息提供者
 */
public class LiveRoomInfoProvider {
    private static LiveRoomInfoProvider instance;

    public boolean isHostEndLive;
    public String comCode;
    public String lastLiveId;
    public String lastIMRoomId;

    public synchronized static LiveRoomInfoProvider getInstance() {
        if (instance == null) {
            instance = new LiveRoomInfoProvider();
        }

        return instance;
    }

    /**
     * 是不是被后台关闭了正在直播的直播间
     */
    public boolean isSysClosedLiveRoom;
    /**
     * 被后台关闭直播间时给的提示
     */
    public String tipsMsgHost;
    public String tipsMsgViewer;

    public String roomId;
    // 只做直播间标题展示用，其他传值暂时不用
    public String roomCode;
    public String imRoomId;
    public String imCreatorid;
    public String liveId;
    public String liveTitle;

    public String otherCompanyCode;


    //进入直播间的sign
    public String userSign;

    //直播间主播ID
    public String hostUserId;

    //主播头像
    public String hostAvatar;

    //主播名字
    public String hostName;

    //直播间密码
    public String joinPassword;

    //直播时长
    public long liveDuration;

    //进入直播间时的服务器时间戳
    public long sysCurTime;

    //开播时间戳
    public long startTime;

    //用户类型，0:主播；1:助手；2：成员
    public String myUserType;

    /**
     * 开播权限：1:有；0:无
     */
    public String isApply;

    //是否是禁言/全体禁言状态
    public boolean isMute;
    public boolean isMuteAll;

    //1:未开始；2:直播中；3:暂停；4:结束
    public String liveStatus;

    //聊天室公告
    public String announcement;

    //直播间类型
    public String roomType;

    //官方大会的流地址
    public String officalLiveUrl;


    // 是否支持回放 1:支持 0 不支持
    public String isReplay;

    // 回放地址
    public String videoPullUrl;

    public String coverOne;

    //是否暂离
    public boolean hasTemplateLeave = false;
    public String templateLeaveTip = "";
    public String templateLeaveTime = "";

    /**
     * 是否正在拨打电话
     */
    public boolean isCallPhoneIng = false;

    /**
     * 主播的音频状态
     */
    public boolean isAudioOpen = true;

    /**
     * 当前使用的是否是前置摄像头
     * true : 前置
     * false ：后置
     */
    public boolean isFrontCamera = true;

    //标签列表
    public ArrayList<UserInfoModel.RoomLive.Tags> mLiveTags = new ArrayList<>();

    /**
     * 是否计未读数-隐藏了小聊天框的情况
     */
    public boolean isCountingUnread;
    //是否计未读数-有同台的情况
    public boolean isCountingUnread2;

    public boolean isGuest() {
        return !isHost() && !isHostHelper();
    }

    public boolean isHostHelper() {
        return !TextUtils.isEmpty(myUserType) && "1".equals(myUserType);
    }

    public boolean isHost() {
        return !TextUtils.isEmpty(myUserType) && "0".equals(myUserType);
    }

    public boolean isHost(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return false;
        }
        return userId.equals(hostUserId);
    }

    public boolean isOfficalLive() {
        return "official".equals(roomType);
    }

    public boolean isTemplateLeaveWhenInit() {
        return !TextUtils.isEmpty(templateLeaveTip) && "3".equals(liveStatus);
    }


    /**
     * 是否正在接听电话
     * 若当前为暂离状态不考虑接听电话的问题
     * @return
     */
    public boolean isCallPhoneIng() {
        return !hasTemplateLeave && isCallPhoneIng;
    }

    /**
     * 标记文档演示（包括视频）是否为本人发起的
     */
    public boolean isMyselfOpenDocument;

    /**
     * 标记当前是否正在进行文档演示（包括视频）
     */
    public boolean isShowFile;

    /**
     * 标记当前是否正在展示手绘板
     */
    public boolean isShowDrawPad;

    public ArrayList<String> joinNamesArr = new ArrayList<>();
    public ArrayList<String> guestTipsArr = new ArrayList<>();

    //项目推荐 、 报名上会 按钮是否可展示，用于文档演示后
    public boolean isCanShowProjectTJ;

    /**
     * 标记当前是白板初始化成功
     */
    public boolean isInitWhiteBoardSuccess;

    public String getStreamIdWithUserId(String userId,int sdkAppId) {
        if (AppConfig.environmentInstance == EnvironmentInstance.FORMAL) {
            // 正式不管，还按照老的拼接方式走（老的是md5）
            return StringUtil.md5(liveId + "_" + userId + "_main");
        } else {
            // 测试、开发环境是新建的账号走新的拼接规则（新的直接拼接）
            return sdkAppId + "_" +  liveId + "_" + userId+ "_main";
        }
    }

    /**
     * 是否通过直播页的开播按钮 去开会申请页并停留
     */
    public boolean isApplying;

    /**
     * 私密会议需求新增字段
     * 是否隐藏分享按钮，0，隐藏 1不隐藏
     */
    public String isHideShare;

    /**
     * 会议权限类型
     */
    public String authType;

    /**
     * 最大人数
     */
    public String allowViewTotal;

    // 是否可以分销：0：不可以；1：可以
    public int ifDistribution;
    // 最大分销金额
    public String maxBudget;
    // 一级分销员佣金
    public String level1Commission;
    //二级分销员佣金
    public String level2Commission;
    /** 一级员工分销额佣金 */
    public String level1CommissionEmployee;
    /** 二级员工分销额佣金 */
    public String level2CommissionEmployee;
    /** 最少参会人数 */
    public String minPerson;
    /** 最低时长 */
    public String minViewTime;

    public void isHostEndLive() {
        isHostEndLive = true;
        new Handler().postDelayed(() -> isHostEndLive = false, 5000);
    }

    public void clearLiveRoomProviderData() {
        // 初始化是否为自己发起的文档演示为否
        isMyselfOpenDocument = false;
        isShowFile = false;
    }

    /**
     * push进来的话 ，留个标识别位置
     */
    public boolean isFromPush;

    /**
     * 申请ID
     */
    public String applyId;

    public void onRecoverFromInstanceState(Bundle savedInstanceState) {
        if (null != savedInstanceState) {
            roomId = savedInstanceState.getString("roomId");
            roomCode = savedInstanceState.getString("roomCode");
            imRoomId = savedInstanceState.getString("imRoomId");
            imCreatorid = savedInstanceState.getString("imCreatorid");
            liveId = savedInstanceState.getString("liveId");
            liveTitle = savedInstanceState.getString("liveTitle");
            userSign = savedInstanceState.getString("userSign");
            hostUserId = savedInstanceState.getString("hostUserId");
            hostAvatar = savedInstanceState.getString("hostAvatar");
            hostName = savedInstanceState.getString("hostName");
            joinPassword = savedInstanceState.getString("joinPassword");
            liveDuration = System.currentTimeMillis() - savedInstanceState.getLong("startTime");
            sysCurTime = System.currentTimeMillis();
            startTime = savedInstanceState.getLong("startTime");
            myUserType = savedInstanceState.getString("myUserType");
            roomType = savedInstanceState.getString("roomType");
            officalLiveUrl = savedInstanceState.getString("officalLiveUrl");
            otherCompanyCode = savedInstanceState.getString("otherCompanyCode");
            //新增
            isHideShare= savedInstanceState.getString("isHideShare");
            allowViewTotal= savedInstanceState.getString("allowViewTotal");
            authType= savedInstanceState.getString("authType");
            ifDistribution = savedInstanceState.getInt("ifDistribution");
            maxBudget = savedInstanceState.getString("maxBudget");
            level1Commission = savedInstanceState.getString("level1Commission");
            level2Commission = savedInstanceState.getString("level2Commission");
            level1CommissionEmployee = savedInstanceState.getString("level1CommissionEmployee");
            level2CommissionEmployee = savedInstanceState.getString("level2CommissionEmployee");
            minPerson = savedInstanceState.getString("minPerson");
            minViewTime = savedInstanceState.getString("minViewTime");
            applyId = savedInstanceState.getString("applyId");
        }
    }


    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            savedInstanceState.putString("roomId", LiveRoomInfoProvider.getInstance().roomId);
            savedInstanceState.putString("roomCode",LiveRoomInfoProvider.getInstance().roomCode);
            savedInstanceState.putString("otherCompanyCode", LiveRoomInfoProvider.getInstance().otherCompanyCode);
            savedInstanceState.putString("imRoomId", LiveRoomInfoProvider.getInstance().imRoomId);
            savedInstanceState.putString("imCreatorid", LiveRoomInfoProvider.getInstance().imCreatorid);
            savedInstanceState.putString("liveId", LiveRoomInfoProvider.getInstance().liveId);
            savedInstanceState.putString("liveTitle", LiveRoomInfoProvider.getInstance().liveTitle);
            savedInstanceState.putString("userSign", LiveRoomInfoProvider.getInstance().userSign);
            savedInstanceState.putString("hostUserId", LiveRoomInfoProvider.getInstance().hostUserId);
            savedInstanceState.putString("hostAvatar", LiveRoomInfoProvider.getInstance().hostAvatar);
            savedInstanceState.putString("hostName", LiveRoomInfoProvider.getInstance().hostName);
            savedInstanceState.putString("joinPassword", LiveRoomInfoProvider.getInstance().joinPassword);
            savedInstanceState.putLong("liveDuration", LiveRoomInfoProvider.getInstance().liveDuration);
            savedInstanceState.putLong("sysCurTime", LiveRoomInfoProvider.getInstance().sysCurTime);
            savedInstanceState.putLong("startTime", LiveRoomInfoProvider.getInstance().startTime);
            savedInstanceState.putString("myUserType", LiveRoomInfoProvider.getInstance().myUserType);
            savedInstanceState.putString("roomType", LiveRoomInfoProvider.getInstance().roomType);
            savedInstanceState.putString("officalLiveUrl", LiveRoomInfoProvider.getInstance().officalLiveUrl);
            //新增
            savedInstanceState.putString("isHideShare", LiveRoomInfoProvider.getInstance().isHideShare);
            savedInstanceState.putString("allowViewTotal", LiveRoomInfoProvider.getInstance().allowViewTotal);
            savedInstanceState.putString("authType", LiveRoomInfoProvider.getInstance().authType);
            savedInstanceState.putInt("ifDistribution",LiveRoomInfoProvider.getInstance().ifDistribution);
            savedInstanceState.putString("maxBudget",LiveRoomInfoProvider.getInstance().maxBudget);
            savedInstanceState.putString("level1Commission",LiveRoomInfoProvider.getInstance().level1Commission);
            savedInstanceState.putString("level2Commission",LiveRoomInfoProvider.getInstance().level2Commission);
            savedInstanceState.putString("level1CommissionEmployee",LiveRoomInfoProvider.getInstance().level1CommissionEmployee);
            savedInstanceState.putString("level2CommissionEmployee",LiveRoomInfoProvider.getInstance().level2CommissionEmployee);
            savedInstanceState.putString("minPerson",LiveRoomInfoProvider.getInstance().minPerson);
            savedInstanceState.putString("minViewTime",LiveRoomInfoProvider.getInstance().minViewTime);
            //数据二期新增
            savedInstanceState.putString("applyId",LiveRoomInfoProvider.getInstance().applyId);
        }
    }


    public void onDestory() {
        joinNamesArr.clear();
        guestTipsArr.clear();
        isCanShowProjectTJ = false;
        isCallPhoneIng = false;
        //isHideShare = "";
    }


    public void clearRoomData(){
        ifDistribution = 0;
        maxBudget = "";
        level1Commission = "";
        level2Commission = "";
        level1CommissionEmployee = "";
        level2CommissionEmployee = "";
        minPerson = "";
        minViewTime = "";
    }
}
