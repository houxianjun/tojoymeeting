package com.tojoy.tjoybaselib.services.oss;

import android.content.Context;
import android.util.Log;

import com.alibaba.sdk.android.oss.ClientConfiguration;
import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.OSS;
import com.alibaba.sdk.android.oss.OSSClient;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.callback.OSSCompletedCallback;
import com.alibaba.sdk.android.oss.common.auth.OSSCredentialProvider;
import com.alibaba.sdk.android.oss.common.auth.OSSStsTokenCredentialProvider;
import com.alibaba.sdk.android.oss.internal.OSSAsyncTask;
import com.alibaba.sdk.android.oss.model.GetObjectRequest;
import com.alibaba.sdk.android.oss.model.GetObjectResult;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.tojoy.tjoybaselib.BaseLibKit;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.oss.model.OSSInfoCache;
import com.tojoy.tjoybaselib.services.oss.model.StsResponseModel;
import com.tojoy.tjoybaselib.util.string.MD5;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import rx.Observer;


/**
 * Created by chengyanfang on 2017/9/27.
 */

public class OSSService {

    private OSS oss;

    public interface InitOSSCallBack {
        void onInitComplete();

        void onInitFail();
    }

    public interface RetryUpOrDownCallBack {
        void onNeedRetry();
    }


    /**
     * 创建单例模式
     */
    private static OSSService mInterface;

    public static OSSService getInterface() {
        if (mInterface == null) {
            synchronized (OSSService.class) {
                mInterface = new OSSService();
            }
        }
        return mInterface;
    }


    /**
     * 初始化OSS
     *
     * @return
     */
    public void initOSSService(InitOSSCallBack initOSSCallBack) {
        //判断是否需要从服务器重新获取Token
        if (OSSInfoCache.needAutoUpdateStSToken()) {
            getSTS(initOSSCallBack);
        } else {
            //直接从本地获取Token，更新OSS
            try {
                initOSSLocalKey();
                initOSSCallBack.onInitComplete();
            } catch (Exception e) {
                initOSSCallBack.onInitFail();
            }
        }
    }

    /**
     * ¬
     * 从服务器获取STS
     */
    private void getSTS(InitOSSCallBack initOSSCallBack) {
        OMAppApiProvider.getInstance().getStsModel(new Observer<StsResponseModel>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                initOSSCallBack.onInitFail();
            }

            @Override
            public void onNext(StsResponseModel stsResponseModel) {
                try {
                    stsResponseModel.saveKeyToLocal();
                    initOSSLocalKey();
                    initOSSCallBack.onInitComplete();
                } catch (Exception e) {
                    e.printStackTrace();
                    initOSSCallBack.onInitFail();
                }
            }
        });
    }


    /**
     * 初始化OSS
     */
    private void initOSSLocalKey() {
        String accessKeyId = OSSInfoCache.getAccessKeyId(BaseLibKit.getContext());
        String accessKeySecret = OSSInfoCache.getAccessKeySecret(BaseLibKit.getContext());
        String securityToken = OSSInfoCache.getSecurityToken(BaseLibKit.getContext());

        OSSCredentialProvider credentialProvider = new OSSStsTokenCredentialProvider(accessKeyId, accessKeySecret, securityToken);

        if (oss == null) {
            ClientConfiguration conf = new ClientConfiguration();
            conf.setConnectionTimeout(15 * 1000); // 连接超时，默认15秒
            conf.setSocketTimeout(15 * 1000); // socket超时，默认15秒
            conf.setMaxConcurrentRequest(3); // 最大并发请求书，默认5个
            conf.setMaxErrorRetry(2); // 失败后最大重试次数，默认2次
            oss = new OSSClient(BaseLibKit.getContext(), OSSConfig.OOSEndPoint, credentialProvider, conf);
        } else {
            oss.updateCredentialProvider(credentialProvider);
        }
    }


    /**
     * ************************************** 上传 ****************************************
     */
    public OSSAsyncTask uploadObject(String object, String fileStr, ProgressCallback progressCallback, RetryUpOrDownCallBack retryUpOrDownCallBack) {
        PutObjectModel putObjectModel = new PutObjectModel(oss, OSSConfig.OOSBucket, object, fileStr);

        Log.d("uploadObject", fileStr);

        return putObjectModel.asyncPutObjectFromLocalFile(new ProgressCallback<PutObjectRequest, PutObjectResult>() {
            @Override
            public void onProgress(PutObjectRequest request, long currentSize, long totalSize) {
                Log.d("uploadObject", "onProgress");
                progressCallback.onProgress(request, currentSize, totalSize);
            }

            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                Log.d("uploadObject", "onSuccess");
                progressCallback.onSuccess(request, result);
            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientException, ServiceException serviceException) {
                Log.d("uploadObject", "onFailure");
                if (clientException != null) {
                    progressCallback.onFailure(null, null, null);
                    Log.d("uploadObject", "clientException");
                    return;
                }

                if (serviceException != null) {
                    Log.d("uploadObject", "serviceException");
                    if (serviceException.getErrorCode().equals(OSSConfig.InvalidAccessKeyCode)) {
                        OSSService.getInterface().getSTS(new OSSService.InitOSSCallBack() {
                            @Override
                            public void onInitComplete() {
                                retryUpOrDownCallBack.onNeedRetry();
                            }

                            @Override
                            public void onInitFail() {
                                progressCallback.onFailure(null, null, null);
                            }
                        });
                    } else {
                        progressCallback.onFailure(null, null, null);
                    }
                }
            }
        });
    }


    /**
     * ************************************** 下载 ****************************************
     */
    public interface DownLoadCallback {
        void onLoadSuccess(String filePath);

        void onLoadError(String errorReason);

        /**
         * 2019/07/01
         * 下载进度
         *
         * @param progress
         */
        void onLoadProgress(int progress);
    }

    public void downLoadObject(Context context, String objectURL, DownLoadCallback downLoadCallback, RetryUpOrDownCallBack retryUpOrDownCallBack) {

        GetObjectModel getObjectModel = new GetObjectModel(oss, OSSConfig.OOSBucket, objectURL);

        getObjectModel.getObject(new Callback<GetObjectRequest, GetObjectResult>() {
            @Override
            public void onSuccess(GetObjectRequest request, GetObjectResult result) {
                String patchFileName = OSSConfig.getOSSFileDir(context).concat(MD5.encode(objectURL)).concat(OSSConfig.OOSAudioFileExtion);
                File downLoadFile = new File(patchFileName);

                // 请求成功 处理数据
                InputStream inputStream = result.getObjectContent();
                byte[] buffer = new byte[2048 * 8];
                long contentLength = result.getContentLength();
                try {
                    FileOutputStream os = new FileOutputStream(downLoadFile);
                    int read = 0;
                    int len = 0;
                    while ((len = inputStream.read(buffer)) != -1) {
                        os.write(buffer, 0, len);
                        read += len;
                        int progress = (int) (read * 1.0f / contentLength * 100);
                        downLoadCallback.onLoadProgress(progress);
                    }
                    os.close();
                    downLoadCallback.onLoadSuccess(patchFileName);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(GetObjectRequest request, ClientException clientException, ServiceException serviceException) {
                //KEY 过期 重新获取
                if (serviceException != null && OSSConfig.InvalidAccessKeyCode.equals(serviceException.getErrorCode())) {
                    OSSService.getInterface().getSTS(new OSSService.InitOSSCallBack() {
                        @Override
                        public void onInitComplete() {
                            retryUpOrDownCallBack.onNeedRetry();
                        }

                        @Override
                        public void onInitFail() {
                            downLoadCallback.onLoadError("下载失败");
                        }
                    });
                } else {
                    downLoadCallback.onLoadError("下载失败");
                }
            }
        });
    }


    public void downAPK(Context context, String url, DownLoadCallback downLoadCallback) {

        GetObjectRequest get = new GetObjectRequest(OSSConfig.OOSBucket, url);
        OSSAsyncTask task = oss.asyncGetObject(get, new OSSCompletedCallback<GetObjectRequest, GetObjectResult>() {
            @Override
            public void onSuccess(GetObjectRequest request, GetObjectResult result) {

                int lastProgress= 0;

                String patchFileName = OSSConfig.getOSSFileDir(context).concat(MD5.encode(url)).concat(OSSConfig.OOSAPKFileExtion);
                File downLoadFile = new File(patchFileName);

                // 请求成功 处理数据
                InputStream inputStream = result.getObjectContent();
                byte[] buffer = new byte[2048 * 8];
                long contentLength = result.getContentLength();
                try {
                    FileOutputStream os = new FileOutputStream(downLoadFile);
                    int read = 0;
                    int len = 0;
                    while ((len = inputStream.read(buffer)) != -1) {
                        os.write(buffer, 0, len);
                        read += len;
                        int progress = (int) (read * 1.0f / contentLength * 100);

                        if(lastProgress != progress) {
                            downLoadCallback.onLoadProgress(progress);
                            lastProgress = progress;
                        }
                    }
                    os.close();
                    downLoadCallback.onLoadSuccess(patchFileName);
                } catch (IOException e) {
                    downLoadCallback.onLoadError(e.getMessage());
                } finally {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(GetObjectRequest request, ClientException clientException, ServiceException serviceException) {
                downLoadCallback.onLoadError("下载失败");
            }
        });
    }


}
