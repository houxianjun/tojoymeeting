package com.tojoy.tjoybaselib.services.oss.model;

import android.annotation.SuppressLint;
import android.content.Context;

import com.tojoy.tjoybaselib.BaseLibKit;
import com.tojoy.tjoybaselib.util.storage.PreferencesUtil;

public class OSSInfoCache {
    private static final String USER_INFO = "userinfo_propertiesInfo";
    @SuppressLint("StaticFieldLeak")
    private static PreferencesUtil mInfoUtil = null;

    /**
     * Preferences文件
     */
    private static PreferencesUtil getPreferencesUtil(Context context) {
        if (mInfoUtil == null) {
            mInfoUtil = new PreferencesUtil(context, USER_INFO);
        }
        return mInfoUtil;
    }


    static void setSTSInfo(StsModel stsModel) {
        mInfoUtil = getPreferencesUtil(BaseLibKit.getContext());
        mInfoUtil.putString("accessKeyId", stsModel.Credentials.AccessKeyId);
        mInfoUtil.putString("accessKeySecret", stsModel.Credentials.AccessKeySecret);
        mInfoUtil.putString("securityToken", stsModel.Credentials.SecurityToken);
        updateLastSTSTime();
    }

    private static long getLastSTSTime() {
        mInfoUtil = getPreferencesUtil(BaseLibKit.getContext());
        return mInfoUtil==null ? 0:mInfoUtil.getLong("updateStsTime");
    }

    private static void updateLastSTSTime() {
        mInfoUtil = getPreferencesUtil(BaseLibKit.getContext());
        mInfoUtil.putLong("updateStsTime", System.currentTimeMillis());
    }

    public static boolean needAutoUpdateStSToken() {
        long currentTime = System.currentTimeMillis();

        long lastUpdateTime = getLastSTSTime();
        //是否间隔30分钟
        return (currentTime - lastUpdateTime) / 60000 > 25;
    }

    public static String getAccessKeyId(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("accessKeyId");
    }

    public static String getAccessKeySecret(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("accessKeySecret");
    }

    public static String getSecurityToken(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("securityToken");
    }

}
