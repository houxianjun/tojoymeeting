package com.tojoy.tjoybaselib.services.mobilecontacts;

import android.support.annotation.NonNull;

import com.tojoy.tjoybaselib.util.string.Cn2SpellUtil;

/**
 * Created by chengyanfang on 2017/10/11.
 */

public class ContactInfo implements Comparable<ContactInfo>{
    public String trueName;
    public String mobile;
    String contactId;

    public boolean isChecked;
    private String pinyin; // 姓名对应的拼音
    private String firstLetter;


    public String getPinyin() {
        return pinyin = Cn2SpellUtil.getPinYin(trueName); // 根据姓名获取拼音;
    }

    public String getFirstLetter() {
        pinyin = Cn2SpellUtil.getPinYin(trueName);
        firstLetter = pinyin.substring(0, 1).toUpperCase(); // 获取拼音首字母并转成大写
        if (!firstLetter.matches("[A-Z]")) { // 如果不在A-Z中则默认为“#”
            firstLetter = "#";
        }
        return firstLetter;
    }

    @Override
    public int compareTo(@NonNull ContactInfo another) {
        if ("#".equals(firstLetter) && !"#".equals(another.getFirstLetter())) {
            return 1;
        } else if (!"#".equals(firstLetter) && "#".equals(another.getFirstLetter())){
            return -1;
        } else {
            return pinyin.compareToIgnoreCase(another.getPinyin());
        }
    }
}
