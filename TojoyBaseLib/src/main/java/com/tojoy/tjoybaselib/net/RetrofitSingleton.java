package com.tojoy.tjoybaselib.net;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.meituan.android.walle.WalleChannelReader;
import com.tojoy.tjoybaselib.BaseLibKit;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.net.converter.ConverterAesJsonFactory;
import com.tojoy.tjoybaselib.net.converter.HttpInterceptor;
import com.tojoy.tjoybaselib.net.noEncryptConverter.NoEncryptConverterAesJsonFactory;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.util.net.NetworkUtil;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.ConnectionPool;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.platform.Platform;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

import static okhttp3.internal.platform.Platform.WARN;

/**
 * Created by chengyanfang on 2017/9/5.
 */

public class RetrofitSingleton {
    /**
     * 不同环境对应接口请求的域名
     * @return
     */
    public static String getWWWWBaseUrl() {
        switch (AppConfig.environmentInstance) {
            case DEV:
                // 开发dev环境
                return "http://tojoy-networkmeeting.tojoy-dev.com/";
            case TEST:
                // 测试环境
                return "http://test-networkmeeting.tojoy-dev.com/";
            case FORMAL:
                // 正式环境
                return "https://live.tojoycloud.com/";
            default:
                // 正式环境
                return "https://live.tojoycloud.com/";
        }
    }

    /**
     * 不同环境对应H5地址的请求域名
     * @return
     */
    public static String getWWWBaseUrlH5() {
        switch (AppConfig.environmentInstance) {
            case DEV:
                // 开发dev环境
                return "http://tojoy-networkmeeting.tojoy-dev.com/";
            case TEST:
                // 测试环境
                return "http://test-cloudmeeting.tojoycloud.org/";
            case FORMAL:
                // 正式环境
                return "https://live.tojoycloud.com/";
            default:
                // 正式环境
                return "https://live.tojoycloud.com/";
        }
    }

    /**
     * 独立云洽会
     */
    public static Retrofit app_retrofit = null;
    public static Retrofit retrofit = null;
    public static OkHttpClient okHttpClient = null;
    private static Context mContext = null;

    /**
     * 构造单例
     */
    private RetrofitSingleton() {
        init();
    }

    private static class SingletonHolder {
        private static final RetrofitSingleton INSTANCE = new RetrofitSingleton();
    }

    public static RetrofitSingleton getInstance(Context context) {
        if (mContext == null) {
            mContext = context;
        }
        return SingletonHolder.INSTANCE;
    }


    /**
     * 设置
     */
    private void init() {
        initOkHttp();
        initRetrofit();
    }

    /**
     * 初始化OkHttp
     */
    private static void initOkHttp() {

        if (mContext == null) {
            mContext = BaseLibKit.getContext();
        }

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        //Step1:Debug模式下配置网络拦截器、打印请求和返回数据
//        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
//        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        builder.addInterceptor(loggingInterceptor);

        HttpInterceptor.Logger DEFAULT = message -> Platform.get().log(WARN, message, null);
        HttpInterceptor logging = new HttpInterceptor(DEFAULT);
        logging.setLevel(HttpInterceptor.Level.BODY);
        builder.addInterceptor(logging);

        //Step2:设置缓存
        String cacheDir = "";
        boolean isExistSDCard = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        if (mContext.getExternalCacheDir() != null && isExistSDCard) {
            cacheDir = mContext.getExternalCacheDir().toString();
        } else {
            cacheDir = mContext.getCacheDir().toString();
        }

        File cacheFile = new File(cacheDir, "/NetCache");
        Cache cache = new Cache(cacheFile, 1024 * 1024 * 50);
        Interceptor cacheInterceptor = chain -> {
            Request request = chain.request();
            if (!NetworkUtil.isNetworkConnected(mContext)) {
                request = request.newBuilder()
                        .cacheControl(CacheControl.FORCE_CACHE)
                        .build();
            }

            Response response = chain.proceed(request);
            if (NetworkUtil.isNetworkConnected(mContext)) {
                //有网络时设置网络超时时间为0
                int maxAge = 0;
                response.newBuilder()
                        .header("Cache-Control", "public, max-age=" + maxAge)
                        .build();
            } else {
                //无网络时设置超时时间为4周
                int maxStale = 60 * 60 * 24 * 28;
                response.newBuilder()
                        .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
                        .build();
            }

            return response;
        };
        builder.cache(cache).addInterceptor(cacheInterceptor);

        //Step3:设置公共参数，避免每次都设置重复的大量基础参数
        Interceptor publicParamsInterceptor = chain ->
        {
            Request originalRequest = chain.request();
            Headers headers = originalRequest.headers();
            HttpUrl modifiedUrl = originalRequest.url().newBuilder()
                    .build();

            Request request = originalRequest.newBuilder().url(modifiedUrl).build();
            return chain.proceed(request);
        };
        builder.addInterceptor(publicParamsInterceptor);

        //Step4:设置请求头
        Interceptor headerInterceptor = chain -> {
            Request originalRequest = chain.request();
            Request.Builder requestBuilder = originalRequest.newBuilder()
                    .header("Content-Type", "application/json;charset=UTF-8")
                    .header("appVersion", BaseLibKit.getVersionName())
                    .header("channel", TextUtils.isEmpty(WalleChannelReader.getChannel(mContext)) ? "8888" : WalleChannelReader.getChannel(mContext))
                    .header("deviceId", BaseUserInfoCache.getDeviceId(mContext))
                    .header("token", BaseUserInfoCache.getUserToken(mContext))
                    .header("companyCode", BaseUserInfoCache.getCompanyCode(mContext))
                    .header("dataSign", BaseUserInfoCache.getDataSign(mContext))
                    .header("Authorization", AppConfig.isAppendHeaderAuthorization ? BaseUserInfoCache.getAccessToken(mContext) : "")
                    .header("userId", BaseUserInfoCache.getUserId(mContext))
                    .method(originalRequest.method(), originalRequest.body());

            Request request = requestBuilder.build();
            Log.d("OM-OkHttp-headers", request.headers().toString());
            return chain.proceed(request);
        };
        builder.addInterceptor(headerInterceptor);

        //builder.dns(OkHttpDns.getInstance(mContext));
        //Step5:设置超时和重连
        builder.connectTimeout(25, TimeUnit.SECONDS);//链接超时15s
        builder.readTimeout(25, TimeUnit.SECONDS);//写入缓存超时15s
        builder.writeTimeout(25, TimeUnit.SECONDS);//读取缓存超时15s
        builder.connectionPool(new ConnectionPool(32, 20, TimeUnit.MINUTES));
        builder.retryOnConnectionFailure(true);//错误重连
//        builder.cookieJar(new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(mContext)));

        //最终一步赋值给okHttpClient
        okHttpClient = builder.build();
    }

    /**
     * 初始化Retrofit
     */
    public static void initRetrofit() {

        app_retrofit = new Retrofit.Builder()
                .baseUrl(RetrofitSingleton.getWWWWBaseUrl())//设置基础URL
                .client(okHttpClient)//设置核心请求端为封装好的okHttpClient
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(NoEncryptConverterAesJsonFactory.create())//
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(RetrofitSingleton.getWWWWBaseUrl())//设置基础URL
                .client(okHttpClient)//设置核心请求端为封装好的okHttpClient
                .addConverterFactory(ConverterAesJsonFactory.create())//
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

    }


    public static class NullOnEmptyConverterFactory extends Converter.Factory {

        @Override
        public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
            final Converter<ResponseBody, ?> delegate = retrofit.nextResponseBodyConverter(this, type, annotations);
            return new Converter<ResponseBody, Object>() {
                @Override
                public Object convert(ResponseBody body) throws IOException {
                    if (body.contentLength() == 0) return null;
                    return delegate.convert(body);
                }
            };
        }
    }
}
