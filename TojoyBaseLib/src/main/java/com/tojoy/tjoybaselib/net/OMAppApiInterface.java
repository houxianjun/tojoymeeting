package com.tojoy.tjoybaselib.net;

import com.tojoy.tjoybaselib.model.OnlineMeeting.ApplyJoinLiveResponse;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApprovalListResponse;
import com.tojoy.tjoybaselib.model.OnlineMeeting.CheckUserInfoResponse;
import com.tojoy.tjoybaselib.model.enterprise.BrowsedEnterpriseResponse;
import com.tojoy.tjoybaselib.model.enterprise.CorporationMsgResponse;
import com.tojoy.tjoybaselib.model.enterprise.EnterpriseBannerResponse;
import com.tojoy.tjoybaselib.model.enterprise.EnterpriseHomeHotLiveListResponse;
import com.tojoy.tjoybaselib.model.enterprise.MyInvitationShareMsgResponse;
import com.tojoy.tjoybaselib.model.enterprise.NewGoodsDetailResponse;
import com.tojoy.tjoybaselib.model.enterprise.ProductDetailResponse;
import com.tojoy.tjoybaselib.model.home.HomePopResponse;
import com.tojoy.tjoybaselib.model.home.HomeReadCountResponse;
import com.tojoy.tjoybaselib.model.home.MeetingHomeModelResponse;
import com.tojoy.tjoybaselib.model.home.MeetingInviteNoticeResponse;
import com.tojoy.tjoybaselib.model.home.MineDistributionApplyRecordResponse;
import com.tojoy.tjoybaselib.model.home.MineDistributionChannelRecordResponse;
import com.tojoy.tjoybaselib.model.home.MineDistributionResponse;
import com.tojoy.tjoybaselib.model.home.MineDistributionSignStatusResponse;
import com.tojoy.tjoybaselib.model.home.MineDistributionWithdarwalApplyResponse;
import com.tojoy.tjoybaselib.model.home.MineDistributionWithdarwalDetailResponse;
import com.tojoy.tjoybaselib.model.home.MineMemberDistributionResponse;
import com.tojoy.tjoybaselib.model.home.MineProfitInfoDistributionResponse;
import com.tojoy.tjoybaselib.model.home.MineProfitListDistributionResponse;
import com.tojoy.tjoybaselib.model.home.MineWithdrawalAccountBindResponse;
import com.tojoy.tjoybaselib.model.home.ScreenAdvertisResponse;
import com.tojoy.tjoybaselib.model.intentionalorder.makesure.NewOrderMakeOrderResponse;
import com.tojoy.tjoybaselib.model.live.AgentLiveModel;
import com.tojoy.tjoybaselib.model.live.ApplyExploreCountResponse;
import com.tojoy.tjoybaselib.model.live.ApplyGuestListResponse;
import com.tojoy.tjoybaselib.model.live.CheckLiveStatusResponse;
import com.tojoy.tjoybaselib.model.live.DoumentIdStatusResponse;
import com.tojoy.tjoybaselib.model.live.DoumentVideoResponse;
import com.tojoy.tjoybaselib.model.live.EndLiveInfoResponse;
import com.tojoy.tjoybaselib.model.live.EnterLiveModel;
import com.tojoy.tjoybaselib.model.live.ExploreMemeberStatusResponse;
import com.tojoy.tjoybaselib.model.live.ExploreOnlineListResponse;
import com.tojoy.tjoybaselib.model.live.FileListResponse;
import com.tojoy.tjoybaselib.model.live.FileTagListResponse;
import com.tojoy.tjoybaselib.model.live.GoodsResponse;
import com.tojoy.tjoybaselib.model.live.InviteApplyResponse;
import com.tojoy.tjoybaselib.model.live.LiveNeedShowLeavePop;
import com.tojoy.tjoybaselib.model.live.LiveShareInfoModel;
import com.tojoy.tjoybaselib.model.live.LiveSignResponse;
import com.tojoy.tjoybaselib.model.live.LiveUserInfoResponse;
import com.tojoy.tjoybaselib.model.live.MyLiveRoomInfo;
import com.tojoy.tjoybaselib.model.live.MyLiveRoomListResponse;
import com.tojoy.tjoybaselib.model.live.NoticeModel;
import com.tojoy.tjoybaselib.model.live.OnlineFrameResponse;
import com.tojoy.tjoybaselib.model.live.OnlineUserInfoResponse;
import com.tojoy.tjoybaselib.model.live.OutlinkListResponse;
import com.tojoy.tjoybaselib.model.live.ProductListResponse;
import com.tojoy.tjoybaselib.model.live.PublishQRListResponse;
import com.tojoy.tjoybaselib.model.live.SelectOutLiveResponse;
import com.tojoy.tjoybaselib.model.live.SelectOutliveStatusResponse;
import com.tojoy.tjoybaselib.model.live.ShortLeaveListModel;
import com.tojoy.tjoybaselib.model.live.StartLiveResponse;
import com.tojoy.tjoybaselib.model.live.UserJoinResponse;
import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.model.pay.CheckDeviceResponse;
import com.tojoy.tjoybaselib.model.servicemanager.OrderModel;
import com.tojoy.tjoybaselib.model.servicemanager.RechargeCancelOrderResponse;
import com.tojoy.tjoybaselib.model.servicemanager.RechargeProductResponse;
import com.tojoy.tjoybaselib.model.servicemanager.RechargeSaveOrderResponse;
import com.tojoy.tjoybaselib.model.servicemanager.RemainTimeResponse;
import com.tojoy.tjoybaselib.model.servicemanager.UseDataResponse;
import com.tojoy.tjoybaselib.model.shopmall.ShopMallListResponse;
import com.tojoy.tjoybaselib.model.user.AddressResponse;
import com.tojoy.tjoybaselib.model.user.CustomerServiceResponse;
import com.tojoy.tjoybaselib.model.user.IMTokenResponse;
import com.tojoy.tjoybaselib.model.user.LoginResponse;
import com.tojoy.tjoybaselib.model.user.MemberListResponse;
import com.tojoy.tjoybaselib.model.user.MyAddressListResponse;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigResponse;
import com.tojoy.tjoybaselib.services.oss.model.StsResponseModel;
import com.tojoy.tjoybaselib.services.update.UpdateModel;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;
import rx.Observable;

/**
 * online-meeting ApiInterface
 */

public interface OMAppApiInterface {

    /**
     * 获取deviceID信息
     */
    @POST("/client-api/appInstall/save")
    Observable<CheckDeviceResponse> getDeviceId(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 更新deviceID
     */
    @POST("/client-api/appInstall/update")
    Observable<CheckDeviceResponse> updateDeviceId(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取OSS_token
     */
    @POST("/cloud-meeting-api/api/global/get_ststoken")
    Observable<StsResponseModel> mGetStsModel(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取验证码
     */
    @POST("/cloud-meeting-upms/api/user/send/sms/code")
    Observable<OMBaseResponse> mGetCode(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 新版本登录接口
     */
    @POST
    Observable<LoginResponse> newLogin(@Url String url);

    /**
     * 退出登录
     */
    @HTTP(method = "DELETE", path = "/cloud-meeting-auth/token/logout", hasBody = true)
    @Headers("Content-Type:application/x-www-form-urlencoded")
    Observable<OMBaseResponse> mLogout(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取IM token
     */
    @POST("/cloud-meeting-api/api/global/get_imtoken")
    Observable<IMTokenResponse> mGetIMToken(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 首页弹窗
     */
    @POST("/cloud-meeting-api/popubAd/findAdImg")
    Observable<HomePopResponse> mHomePop(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 开会申请页面参数（进入开会申请页面请求）
     */
    @POST("/cloud-meeting-api/api/live/apply/joinLiveApply")
    Observable<ApplyJoinLiveResponse> mJoinLiveApply(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 开会申请根据所选人数判断对应审批人
     */
    @POST("/cloud-meeting-api/api/live/apply/getCheckUser")
    Observable<CheckUserInfoResponse> mGetCheckUser(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 开会申请详情页
     */
    @POST("/cloud-meeting-api/api/live/apply/applyInfo")
    Observable<ApplyJoinLiveResponse> mApplyInfoDetail(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 提交开会申请接口
     */
    @POST("/cloud-meeting-api/api/live/apply/saveLiveApply")
    Observable<OMBaseResponse> mSubmitLiveApply(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 权限处理列表
     */
    @POST("/cloud-meeting-api/api/live/apply/queryLiveApplyCheck")
    Observable<ApprovalListResponse> mQueryLiveApplyCheck(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 权限处理（驳回、同意操作）
     */
    @POST("/cloud-meeting-api/api/live/apply/applyCheck")
    Observable<OMBaseResponse> mApplyCheck(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 撤回开会申请接口
     */
    @POST("/cloud-meeting-api/api/live/apply/revertLiveApply")
    Observable<OMBaseResponse> mRevertLiveApply(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取用户进入直播间的sign
     */
    @POST("/cloud-meeting-api/api/tencent/login")
    Observable<LiveSignResponse> mGetUserLiveSign(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 发消息到后台（直播间评论功能，需后台审核通过后其他人才可显示）
     */
    @POST("/cloud-meeting-api/api/live/room/publishComment")
    Observable<OMBaseResponse> mSendMsg(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 设置直播间助手
     */
    @POST("/cloud-meeting-api/api/live/room/setLiveRoomAssistant")
    Observable<OMBaseResponse> mAddLiveHelper(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 取消直播间助手
     */
    @POST("/cloud-meeting-api/api/live/room/cancelLiveRoomAssistant")
    Observable<OMBaseResponse> mCancleLiveHelper(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取直播间房间信息、主播信息、自己再此房间的信息
     */
    @POST("/cloud-meeting-api/api/live/room/getRedisLiveMemberInfo")
    Observable<LiveUserInfoResponse> mMemberInfo(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 查询直播间状态
     */
    @POST("/cloud-meeting-api/api/roomLive/queryLive")
    Observable<CheckLiveStatusResponse> mGetLiveRoomStatus(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 直播结束后获取本场直播数据
     */
    @POST("/cloud-meeting-api/api/live/room/endRoomLiveData")
    Observable<EndLiveInfoResponse> getEndLiveRoomInfo(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 当前用户是否对这个项目感兴趣
     */
    @POST("/cloud-meeting-api/api/live/room/intentionLiveProject")
    Observable<OMBaseResponse> mIsInterested(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 检测进入直播间是否要输入密码
     */
    @POST("/cloud-meeting-api/api/live/room/checkEnterLiveRoom")
    Observable<OMBaseResponse> mCheckEnterLiveRoom(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 马上开播
     */
    @POST("/cloud-meeting-api/api/roomLive/startLive")
    Observable<StartLiveResponse> mStartLive(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 进入直播间
     */
    @POST("/cloud-meeting-api/api/live/room/enterLiveRoom")
    Observable<EnterLiveModel> mJoinLiveRoom(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 观众进入大会
     */
    @POST("/cloud-meeting-api/api/live/room/joinMeeting")
    Observable<UserJoinResponse> mJoinMeeting(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 退出直播间
     */
    @POST("/cloud-meeting-api/api/live/room/quitLiveRoom")
    Observable<OMBaseResponse> mLeaveLiveRoom(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 结束直播
     */
    @POST("/cloud-meeting-api/api/roomLive/endLive")
    Observable<OMBaseResponse> mEndLive(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 主播暂离后恢复直播
     */
    @POST("/cloud-meeting-api/api/live/room/cancelRoomLivePause")
    Observable<OMBaseResponse> mGoOnLive(@Header("sign") String sign, @Body RequestBody body);

    /**
     *  设置手绘板、文档演示状态和数据
     */
    @POST("/cloud-meeting-api/liveDocument/setDocumentStatus")
    Observable<OMBaseResponse> setDocumentStatus(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取当前直播间的手绘板、文档演示的状态和数据
     */
    @POST("/cloud-meeting-api/liveDocument/getDocumentStatus")
    Observable<DoumentIdStatusResponse> getDocumentStatus(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 文档演示--播放、暂停视频
     */
    @POST("/cloud-meeting-api/liveDocument/setVideoStatus")
    Observable<OMBaseResponse> setVideoStatus(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取当前是否有文档演示播放视频
     */
    @POST("/cloud-meeting-api/liveDocument/getVideoStatus")
    Observable<DoumentVideoResponse> getVideoStatus(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取直播间分享信息
     */
    @POST("/cloud-meeting-api/api/live/room/getRoomLiveInfo")
    Observable<LiveShareInfoModel> mGetShareInfo(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 浏览过的企业列表
     */
    @POST("/cloud-meeting-api/api/log/browseCompany")
    Observable<BrowsedEnterpriseResponse> mBrowsedEnterprise(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 直播间已发布的 商品或广告 列表
     */
    @POST("/cloud-meeting-api/api/goods/getLiveGoodsList")
    Observable<ShopMallListResponse> mGetGoodsTJ(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 直播间发布 商品或广告
     */
    @POST("/cloud-meeting-api/api/live/room/addLiveProject")
    Observable<OMBaseResponse> mPublishProject(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 我感兴趣 商品或广告
     */
    @POST("/cloud-meeting-api/api/intention/selectCommodityV2")
    Observable<OMBaseResponse> mILikeV2(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 直播间在线成员列表
     * V1.2.3 改为观众端专用
     */
    @POST("/cloud-meeting-api/api/meetingMemberNew/searchMemberOnline")
    Observable<MemberListResponse> mGetLiveMemberList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 直播间在线 成员 搜索
     * V1.2.3 改为观众端专用
     * 1.4.3版本替换接口
     * /cloud-meeting-api/api/live/room/getLiveCustomerList
     *
     */
    @POST("/cloud-meeting-api/api/meetingMemberNew/searchMemberOnline")
    Observable<MemberListResponse> mGetMemberList(@Header("sign") String sign, @Body RequestBody body);

    @POST("/cloud-meeting-api/api/meetingMemberNew/searchMemberOnlinePage")
    Observable<MemberListResponse> searchMemberOnlinePage(@Header("sign") String sign, @Body RequestBody body);

    /**
     * V1.2.3 主播助手-已入会成员列表
     */
    @POST("/cloud-meeting-api/api/meetingMemberNew/selectLiveMember")
    Observable<MemberListResponse> getLiveMemberList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * V1.2.3 主播助手-未入会成员列表
     */
    @POST("/cloud-meeting-api/api/meetingMemberNew/unEnterMember")
    Observable<MemberListResponse> unEnterMemberList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * V1.2.3 主播助手-添加参会人
     */
    @POST("/cloud-meeting-api/api/roomLive/inviteMember")
    Observable<OMBaseResponse> addMember(@Header("sign") String sign, @Body RequestBody body);

    /**
     * V1.2.3 主播助手-邀请入会
     */
    @POST("/cloud-meeting-api/api/meetingMemberNew/inviteMember")
    Observable<OMBaseResponse> inviteMember(@Header("sign") String sign, @Body RequestBody body);

    /**
     * V1.2.3 主播助手-成员列表搜索
     */
    @POST("/cloud-meeting-api/api/meetingMemberNew/searchMember")
    Observable<MemberListResponse> searchMember(@Header("sign") String sign, @Body RequestBody body);



    /**
     * V1.4.3 主播助手-分页成员列表搜索
     */
    @POST("/cloud-meeting-api/api/meetingMemberNew/searchMemberPage")
    Observable<MemberListResponse> searchMemberPage(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 主播端获取暂离文字信息列表
     */
    @POST("/cloud-meeting-api/api/live/room/getRoomPauseList")
    Observable<ShortLeaveListModel> mShortLeaveTextList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 主播端提交暂离列表
     */
    @POST("/cloud-meeting-api/api/live/room/addRoomLivePause")
    Observable<OMBaseResponse> mCommitShortLeaveInfo(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 直播间举报
     */
    @POST("/cloud-meeting-api/api/live/room/insertRoomLiveTipoff")
    Observable<OMBaseResponse> mLiveReport(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 直播间发公告
     */
    @POST("/cloud-meeting-api/api/live/room/publishOfficial")
    Observable<OMBaseResponse> mPublishNotice(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取引流列表
     */
    @POST("/cloud-meeting-api/blendStream/liveRoomStream")
    Observable<OutlinkListResponse> mOuterLiveRoomList(@Header("sign") String sign, @Body RequestBody body);

    /**
     *  提交选中的引流房间
     */
    @POST("/cloud-meeting-api/blendStream/v2/selectLiveStream")
    Observable<SelectOutLiveResponse> mSelectAOuterLive(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 新流增加，调用接口增加引流
     */
    @POST("/cloud-meeting-api/blendStream/startCallBackStream")
    Observable<OMBaseResponse> mStartANewLive(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 关闭引的某个流
     */
    @POST("/cloud-meeting-api/blendStream/closeLiveStream")
    Observable<OMBaseResponse> closeLiveStream(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取此房间已经引流的数据
     */
    @POST("/cloud-meeting-api/blendStream/getLiveStreamStatus")
    Observable<SelectOutliveStatusResponse> getLiveStreamStatus(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取公告
     */
    @POST("/cloud-meeting-api/api/live/room/getLiveOfficial")
    Observable<NoticeModel> mGetNotice(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取产品详情
     */
    @POST("/cloud-meeting-api/api/share/productShareDetail")
    Observable<ProductDetailResponse> mProductShareDetail(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取企业文化详情
     */
    @POST("/cloud-meeting-api/api/share/corporationDetail")
    Observable<ProductDetailResponse> mCorporationDetail(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 观众 - 申请同台直播
     */
    @POST("/cloud-meeting-api/sameLive/applyLive")
    Observable<OMBaseResponse> mApplyExploreMe(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 观众 - 开始同台直播
     */
    @POST("/cloud-meeting-api/sameLive/startLive")
    Observable<OMBaseResponse> mStartExploreMe(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 主播 - 关闭直播间
     */
    @POST("/cloud-meeting-api/sameLive/finishLive")
    Observable<OMBaseResponse> mStopExploreMe(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 主播、观众 - 结束同台直播
     */
    @POST("/cloud-meeting-api/sameLive/closeinvitation")
    Observable<OMBaseResponse> mStopExploreSomebody(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 主播 - 邀请同台直播
     */
    @POST("/cloud-meeting-api/sameLive/invitationLive")
    Observable<InviteApplyResponse> mInviteExploreSomebody(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 主播 - 同意或拒绝同台直播申请
     * 观众 - 同意或拒绝同台直播邀请
     */
    @POST("/cloud-meeting-api/sameLive/selectRequest")
    Observable<OMBaseResponse> mResponseForApplyOrInviteExplore(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 设置同台观众的音视频状态
     */
    @POST("/cloud-meeting-api/sameLive/setLiveOpration")
    Observable<OMBaseResponse> mControlGuestMediaStatus(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 查询同台观众的音视频状态
     */
    @POST("/cloud-meeting-api/sameLive/getLiveOpration")
    Observable<ExploreMemeberStatusResponse> mGetGuestMediaStatus(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 设置同台用户上屏
     */
    @POST("/cloud-meeting-api/sameLive/setframeStatus")
    Observable<OMBaseResponse> mSetBigScreenUser(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 查询上屏的同台用户
     */
    @POST("/cloud-meeting-api/sameLive/getframeStatus")
    Observable<OnlineFrameResponse> mGetBigScreenUser(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 主播 - 模糊搜索在线成员
     */
    @POST("/cloud-meeting-api/sameLive/search")
    Observable<ExploreOnlineListResponse> mSearchOnlineGuestList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 主播 - 获取当前申请人名单
     */
    @POST("/cloud-meeting-api/sameLive/applyForList")
    Observable<ApplyGuestListResponse> mApplyExplorerList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 助手 & 主播 - 获取当前未处理申请人人数
     */
    @POST("/cloud-meeting-api/sameLive/applyForCount")
    Observable<ApplyExploreCountResponse> mGetCurrentApplyCount(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 用户信息 - 获取用户信息
     */
    @POST("/cloud-meeting-upms/api/user/userInformation")
    Observable<OnlineUserInfoResponse> mGetUserByUserId(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 全部禁言
     */
    @POST("/cloud-meeting-api/api/live/room/forbiddenAll")
    Observable<OMBaseResponse> muteAll(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 网上天洽会--结束直播-推荐的直播列表
     */
    @POST("/cloud-meeting-api/api/live/room/recommendLiveList")
    Observable<MyLiveRoomListResponse> mGetRecommendLiveList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取全量地址
     */
    @POST("/cloud-meeting-api/api/productOrder/getAllDistricts")
    Observable<AddressResponse> getAddressList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取自己设置过的联系地址列表
     */
    @POST("/cloud-meeting-api/api/address/selectMyAddressDetail")
    Observable<MyAddressListResponse> getMyAddressList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 添加自己的常用地址
     */
    @POST("/cloud-meeting-api/api/address/addAddress")
    Observable<OMBaseResponse> addAddress(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 修改已设置的地址
     */
    @POST("/cloud-meeting-api/api/address/updateAddressDetail")
    Observable<OMBaseResponse> editAddress(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取文档索引列表
     */
    @POST("/cloud-meeting-api/liveDocument/selectRoomTagList")
    Observable<FileTagListResponse> mGetFileTagList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取某个文件夹下的文件列表
     */
    @POST("/cloud-meeting-api/liveDocument/selectDir")
    Observable<FileListResponse> mGetFileListWithTagId(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 删除文件夹中得文件
     */
    @POST("/cloud-meeting-api/liveDocument/removeFile")
    Observable<OMBaseResponse> removeDocumentFile(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取企业信息
     */
    @POST("/cloud-meeting-api/api/corporation/query")
    Observable<CorporationMsgResponse> mCorporationQuery(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取企业定制首页banner
     */
    @POST("/cloud-meeting-api/api/banner/bannerList")
    Observable<EnterpriseBannerResponse> mGetCorporationBanner(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取企业定制首页热门推荐列表
     */
    @POST("/cloud-meeting-api/api/index/hotRoomLive")
    Observable<EnterpriseHomeHotLiveListResponse> mHotRoomLive(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 云洽会四期--商品详情
     */
    @POST("/cloud-meeting-api/api/live/goods/goodsDetail")
    Observable<NewGoodsDetailResponse> mNewGoodsDetail(@Header("sign") String sign, @Body RequestBody body);

    /**
     * jiang guoqiu 下单口
     */
    @POST("/cloud-meeting-api/api/liveorder/userOrder")
    Observable<NewOrderMakeOrderResponse> mNewSureOder(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 记录商品游览量
     */
    @POST("cloud-meeting-api/api/live/goods/addGoodsVisit")
    Observable<OMBaseResponse> mAddGoodsVisit(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 删除我的房间列表中的数据
     */
    @POST("/cloud-meeting-api/api/live/room/deleteLiveInfo")
    Observable<OMBaseResponse> mDeleteMyLive(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 删除观看记录
     */
    @POST("cloud-meeting-api/api/live/room/removeViewRecord")
    Observable<OMBaseResponse> removeViewRecord(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取我的房间列表的房间id和直播次数
     */
    @POST("/cloud-meeting-api/api/live/room/myRoom")
    Observable<MyLiveRoomInfo> mGetMyRoomInfo(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 我的房间列表
     */
    @POST("cloud-meeting-api/api/live/room/getLiveList")
    Observable<MyLiveRoomListResponse> mGetMyRoomList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 我的房间列表
     */
    @POST("cloud-meeting-api/api/live/room/getViewRecordList")
    Observable<MyLiveRoomListResponse> mGetViewRecordList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 给客服留言，反馈
     */
    @POST("/client-api/appSuggest/save")
    Observable<OMBaseResponse> mLeaveMsgSave(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 设置-联系客服电话获取
     */
    @POST("cloud-meeting-api/api/corporation/customer")
    Observable<CustomerServiceResponse> mGetCustomerServicePhone(@Header("sign") String sign);

    /**
     * 设置-修改我的头像
     */
    @POST("/cloud-meeting-upms/api/user/edit/image")
    Observable<OMBaseResponse> mEditUerImage(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取当前用户信息
     */
    @POST("/cloud-meeting-api/api/userInfo/detail")
    Observable<LoginResponse> mGetUserInfo(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 修改用户昵称
     */
    @POST("/cloud-meeting-upms/api/user/edit/name")
    Observable<OMBaseResponse> mUpdatUser(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 版本升级检测
     */
    @POST("/client-api/appVersion/check")
    Observable<UpdateModel> mUpgrade(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 权限处理小红点（进入页面通知后台已读）
     */
    @POST("/cloud-meeting-api/api/live/apply/updateReadStatus")
    Observable<OMBaseResponse> updateReadStatus(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 权限处理小红点（未处理消息个数）
     */
    @POST("/cloud-meeting-api/api/live/apply/showNum")
    Observable<HomeReadCountResponse> showNum(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 开屏广告
     */
    @POST("/cloud-meeting-api/screenAd/findAdImg")
    Observable<ScreenAdvertisResponse> mOpenScreenAdvertis(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 个推绑定
     */
    @POST("/cloud-meeting-api/appfront/m/app/push/bindAlias")
    Observable<OMBaseResponse> mPushBind(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 个推解绑
     */
    @POST("/cloud-meeting-api/appfront/m/app/push/unBindAlias")
    Observable<OMBaseResponse> mPushUnBind(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 用户启动APP累计次数(每个自然日)
     */
    @POST("/cloud-meeting-api/popubAd/startUpAppForTimingByUser")
    Observable<OMBaseResponse> startUpAppCountByUser(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 统计IM登录成败
     */
    @POST("/cloud-meeting-api/api/statistic/enterLivePageStatitisc")
    Observable<OMBaseResponse> statitiscIMLogin(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 主播需不需要弹xxx结束同台 应对游客同台后切换身份情况
     */
    @POST("/cloud-meeting-api/sameLive/selectUnLoginUser")
    Observable<LiveNeedShowLeavePop> needShowLeavePop(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 开播提醒：主播马上开播调用startLive接口返回成功后（调用）
     */
    @POST("/cloud-meeting-api/api/liveSubscribe/startLivePushRemind")
    Observable<OMBaseResponse> startLivePushRemind(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 开播提醒：直播间—互动弹窗—开播提醒按钮—发送（调用）废弃
     */
    @POST("/cloud-meeting-api/api/liveSubscribe/livePushRemind")
    Observable<OMBaseResponse> livePushRemind(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取发送邀请函分享参数
     */
    @POST("/cloud-meeting-api/api/liveSubscribe/generateInvitation")
    Observable<MyInvitationShareMsgResponse> generateInvitation(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 搜索商城商品
     */
    @POST("/cloud-meeting-api/api/goods/getGoodsList")
    Observable<ShopMallListResponse> mSearchGood(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 1.2.3版本新增 获取可参会部门和人员，外部联系人接口
     */
    @POST("/cloud-meeting-upms/api/company/orgemployees/list")
    Observable<MeetingPersonDepartmentResponse> selectMyDepartMentMemV2(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取企业定制首页热门推荐列表
     */
    @POST("/cloud-meeting-api/api/meetingCenter/index")
    Observable<EnterpriseHomeHotLiveListResponse> mGetMeetingList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取定制首页的产品列表
     */
    @POST("/cloud-meeting-api/api/live/goods/goodsRecommendList")
    Observable<ProductListResponse> mGetEnterprisePageProductList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 升级全局控件文案
     */
    @POST("/cloud-meeting-api/api/system/common/config/getDic")
    Observable<TextConfigResponse> mGetOMVersionTextOfWidgets(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 产品推荐列表页和产品推荐搜索页面  直播间所有商品
     */
    @POST("/cloud-meeting-api/api/live/goods/goodsList")
    Observable<GoodsResponse> mGetProductList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 产品推荐列表页和产品推荐搜索页面  直播间所有代播商品
     */
    @POST("/cloud-meeting-api/api/live/agentGoods/goodsList")
    Observable<GoodsResponse> mGetAgentGoodsList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 会议服务管理剩余分钟数接口
     */
    @POST("/cloud-meeting-api/api/liveManagemen/queryPlatCorporationRemaining")
    Observable<RemainTimeResponse> queryPlatCorporationRemaining(@Header("sign") String sign, @Body RequestBody body);


    /**
     * 会议服务管理产品接口
     */
    @POST("/cloud-meeting-api/api/liveManagemen/queryManagemenProduct")
    Observable<RechargeProductResponse> queryManagemenProduct(@Header("sign") String sign, @Body RequestBody body);


    /**
     * 会议服务管理：充值下单接口
     */
    @POST("/cloud-meeting-api/api/platservices/saveorder")
    Observable<RechargeSaveOrderResponse> rechargesaveorder(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 会议服务管理：充值取消订单接口
     */
    @POST("/cloud-meeting-api/api/platservices/cancelOrder")
    Observable<RechargeProductResponse> rechargeCancelOrder(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 我的订单（购买记录）
     */
    @POST("/cloud-meeting-api/api/platservices/list")
    Observable<OrderModel> orderList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 取消订单
     */
    @POST("/cloud-meeting-api/api/platservices/cancelOrder")
    Observable<RechargeCancelOrderResponse> cancelOrder(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 使用明细
     */
    @POST("/cloud-meeting-api/api/feeDetail/useData")
    Observable<UseDataResponse> useData(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 热门企业–更多
     */
    @POST("/cloud-meeting-upms/api/company/mainpage/companylist")
    Observable<BrowsedEnterpriseResponse> getHotCompanyMoreList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 首页
     */
    @POST("/cloud-meeting-api/api/cms/ad/getMainPageInfo")
    Observable<MeetingHomeModelResponse> getMainPageInfo(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 直播间--发布活动（二维码）列表
     * 主播/助手端调用
     */
    @POST("/cloud-meeting-api/api/cms/promotionLive/selectMyCompanyPromotion")
    Observable<PublishQRListResponse> selectMyCompanyPromotion(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 直播间--发布/隐藏活动
     */
    @POST("/cloud-meeting-api/api/cms/promotionLive/pushPromotion")
    Observable<OMBaseResponse> pushPromotion(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 直播间--获取已经发布且需要显示的活动二维码列表（初始化时调用）
     */
    @POST("/cloud-meeting-api/api/cms/promotionLive/selectLivePromotion")
    Observable<PublishQRListResponse> selectLivePromotion(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 点击进入小程序:统计
     */
    @POST("/cloud-meeting-api/api/cms/promotionLive/selectPromotion")
    Observable<OMBaseResponse> selectPromotion(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 新增：首页点击直播招商-开播的拦截接口（若由企业 && 有开播权限 && 有obs权限，进行判断是否有此用户正在开播的官方大会）
     */
    @POST("/cloud-meeting-api/api/live/apply/queryLiveStatus")
    Observable<OMBaseResponse> queryLiveStatus(@Header("sign") String sign, @Body RequestBody body);

    /**
     * V1.3.1申请成为分销客
     */
    @POST("/cloud-meeting-distribution/api/members/applyForDistributor")
    Observable<OMBaseResponse> applyForDistributor(@Header("sign") String sign,@Body RequestBody body);

    /**
     * 分销客我的会员列表
     */
    @POST("/cloud-meeting-distribution/api/members/memberList")
    Observable<MineMemberDistributionResponse> queryDistributionMyMemberList(@Header("sign") String sign,@Body RequestBody body);

    /**
     * 分销客我的团队列表
     */
    @POST("/cloud-meeting-distribution/api/members/teamList")
    Observable<MineMemberDistributionResponse> queryDistributionMyTeamList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 二级分销客我的团队列表
     */
    @POST("/cloud-meeting-distribution/api/members/teamMemberList")
    Observable<MineMemberDistributionResponse> queryTwoLevelDistributionMyTeamList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 我的分销会议收益列表
     */
    @POST("/cloud-meeting-distribution/api/meeting/commission/list/detail")
    Observable<MineProfitListDistributionResponse> queryDistributionCommissionList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 我的分销商品收益列表
     */
    @POST("/cloud-meeting-distribution/api/meeting/commission/list/goodsdetail")
    Observable<MineProfitListDistributionResponse> queryDistributionGoodsdetailList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 我的收益
     */
    @POST("/cloud-meeting-distribution/api/members/myProfit")
    Observable<MineProfitInfoDistributionResponse> queryDistributionMyProfit(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 我的页面--（我的收益、我的会员总体数据展示）
     */
    @POST("/cloud-meeting-distribution/api/members/memberInfo")
    Observable<MineDistributionResponse> getDistributionMemberInfo(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 我的收益-- 申请提现
     */
    @POST("/cloud-meeting-distribution/api/distributor/withdraw/apply")
    Observable<MineDistributionWithdarwalApplyResponse> querytDistributionWithdrawApply(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 我的收益-- 提现详情
     */
    @POST("/cloud-meeting-distribution/api/distributor/withdraw/applyDetail")
    Observable<MineDistributionWithdarwalDetailResponse> querytDistributionWithdrawApplyDetail(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 我的收益-- 提现记录
     */
    @POST("/cloud-meeting-distribution/api/distributor/withdraw/applyList")
    Observable<MineDistributionApplyRecordResponse> querytDistributionWithdrawApplpList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 我的收益-- 信息录入签约
     */
    @POST("/cloud-meeting-distribution/api/distributor/withdraw/sign")
    Observable<OMBaseResponse> querytDistributionWithdrawSign(@Header("sign") String sign, @Body RequestBody body);


    /**
     * 我的收益-- 信息录入签约状态
     */
    @POST("/cloud-meeting-distribution/api/distributor/withdraw/querySignStatus")
    Observable<MineDistributionSignStatusResponse> querytDistributionWithdrawSignStatus(@Header("sign") String sign, @Body RequestBody body);


    /**
     * 我的会议 - 我的被邀请记录
     */
    @POST("/cloud-meeting-api/api/liveSubscribe/selectMysubMeeting")
    Observable<MeetingInviteNoticeResponse> inviteMeRecordList(@Header("sign") String sign, @Body RequestBody body);


    /**
     * 提现-获取支付宝绑定状态
     */
    @POST("/cloud-meeting-upms/api/user/queryBinding")
    Observable<MineWithdrawalAccountBindResponse> queryAliAccountBindingStatus(@Header("sign") String sign, @Body RequestBody body);



    /**
     * 提现-支付宝绑定
     */
    @POST("/cloud-meeting-upms/api/user/binding")
    Observable<OMBaseResponse> queryAliAccountBinding(@Header("sign") String sign, @Body RequestBody body);


    /**
     * 提现-支付宝解除绑定
     */
    @POST("/cloud-meeting-upms/api/user/unBinding")
    Observable<OMBaseResponse> queryAliAccountUnBinding(@Header("sign") String sign, @Body RequestBody body);


    /**
     * 企业是否有代播权限
     */
    @POST("/cloud-meeting-upms/api/company/detail/by/code")
    Observable<AgentLiveModel> queryCompanyAgentPower(@Header("sign") String sign, @Body RequestBody body);

}
