package com.tojoy.tjoybaselib.net.converter;


import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.EnvironmentInstance;
import com.tojoy.tjoybaselib.util.net.CompressUtil;

import java.io.IOException;
import java.io.StringReader;

import okhttp3.ResponseBody;
import retrofit2.Converter;

/**
 * Created by chengyanfang on 2017/9/22.
 */

final class AesGsonResponseBodyConverter<T> implements Converter<ResponseBody, T> {
    private final Gson gson;
    private final TypeAdapter<T> adapter;

    AesGsonResponseBodyConverter(Gson gson, TypeAdapter<T> adapter) {
        this.gson = gson;
        this.adapter = adapter;
    }

    @Override
    public T convert(ResponseBody value) throws IOException {

        byte[] responseBytes = CompressUtil.deEcryptAndDeCompress(value.bytes());

        String responseStr = new String(responseBytes, "UTF-8");

        if (AppConfig.environmentInstance == EnvironmentInstance.DEV) {
            logOkHttp("OkHttp--ResponseJSON", responseStr);
        }
        logOkHttp("OkHttp--ResponseJSON", responseStr);

        JsonReader jsonReader = gson.newJsonReader(new StringReader(responseStr));
        try {
            return adapter.read(jsonReader);
        } finally {
            value.close();
        }


    }

    /**
     * 截断输出日志
     *
     * @param msg
     */
    public void logOkHttp(String tag, String msg) {
        if (tag == null || tag.length() == 0
                || msg == null || msg.length() == 0)
            return;

        int segmentSize = 3 * 1024;
        long length = msg.length();
        if (length <= segmentSize) {// 长度小于等于限制直接打印
            Log.d(tag, msg);
        } else {
            while (msg.length() > segmentSize) {// 循环分段打印日志
                String logContent = msg.substring(0, segmentSize);
                msg = msg.replace(logContent, "");
                Log.d(tag, logContent);
            }
            Log.d(tag, msg);// 打印剩余日志
        }
    }
}
