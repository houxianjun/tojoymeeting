package com.tojoy.tjoybaselib.net;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by chengyanfang on 2017/9/5.
 */

public class BaseResponse {
    @SerializedName("responseCode")
    public String responseCode;
    @SerializedName("responseTips")
    public String responseTips;

    public boolean isSuccess() {
        if (!TextUtils.isEmpty(responseCode) && responseCode.equals("1")) {
            return true;
        } else {
            return false;
        }
    }

    public CommandInfo commandInfo;

    public class CommandInfo {
        public String commandName;
    }

    public String getCommandName() {
        if (commandInfo == null) {
            return "";
        }
        return commandInfo.commandName;
    }
}
