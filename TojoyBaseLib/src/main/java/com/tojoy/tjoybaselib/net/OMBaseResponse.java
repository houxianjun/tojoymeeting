package com.tojoy.tjoybaselib.net;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by luuzhu on 2019/9/9.
 */

public class OMBaseResponse {
    @SerializedName("code")
    public String code;

    /**
     * 响应信息
     */
    public String msg;

    /**
     * 本次数据签名
     */
    public String dataSign;

    public boolean isSuccess(){
        return !TextUtils.isEmpty(code) && "1".equals(code);
    }

    public String extra;
}
