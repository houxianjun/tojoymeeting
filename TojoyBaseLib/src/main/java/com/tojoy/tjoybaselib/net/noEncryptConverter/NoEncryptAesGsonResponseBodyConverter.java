package com.tojoy.tjoybaselib.net.noEncryptConverter;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.tojoy.tjoybaselib.BaseLibKit;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;

import java.io.IOException;
import java.io.StringReader;

import okhttp3.ResponseBody;
import retrofit2.Converter;


final class NoEncryptAesGsonResponseBodyConverter<T> implements Converter<ResponseBody, T> {
    private final Gson gson;
    private final TypeAdapter<T> adapter;

    NoEncryptAesGsonResponseBodyConverter(Gson gson, TypeAdapter<T> adapter) {
        this.gson = gson;
        this.adapter = adapter;
    }

    @Override
    public T convert(ResponseBody value) throws IOException {
        byte[] responseBytes = value.bytes();

        String responseStr = new String(responseBytes, "UTF-8");

        try {
            OMBaseResponse omBaseResponse = new Gson().fromJson(responseStr, OMBaseResponse.class);
            String code = omBaseResponse.code;
            if ("20102".equals(code) || "20103".equals(code)||"401".equals(code)) {
                try {
                    if (BaseUserInfoCache.isLogined(BaseLibKit.getContext())) {
                        RouterJumpUtil.startLoginActivityForTokenInvalid(true);
                    }
                } catch (Exception e) {

                }
                return null;
            }
        } catch (Exception e) {
        } finally {
            value.close();
        }

        JsonReader jsonReader = gson.newJsonReader(new StringReader(responseStr));
        try {
            return adapter.read(jsonReader);
        } finally {
            value.close();
        }
    }
}