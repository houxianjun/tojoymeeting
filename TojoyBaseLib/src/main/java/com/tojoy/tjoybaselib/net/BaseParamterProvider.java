package com.tojoy.tjoybaselib.net;

import android.content.Context;
import android.text.TextUtils;

import com.meituan.android.walle.WalleChannelReader;
import com.tojoy.tjoybaselib.BaseLibKit;
import com.tojoy.tjoybaselib.util.sys.DeviceInfoUtil;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by chengyanfang on 2017/9/5.
 */

public class BaseParamterProvider {

    private static DeviceInfoUtil mDeviceInfoUtil;

    private static Context mContext;

    private static class SingletonHolder {
        private static final BaseParamterProvider INSTANCE = new BaseParamterProvider();
    }

    public static BaseParamterProvider getInstance(Context context) {
        mContext = context;

        if (mDeviceInfoUtil == null) {
            mDeviceInfoUtil = new DeviceInfoUtil();
        }

        return BaseParamterProvider.SingletonHolder.INSTANCE;
    }


    /**
     * 获取设备信息
     *
     * @return
     */
    public JSONObject getDeviceInfoJSON() {

        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("language", mDeviceInfoUtil.getMobileLanguage());
            jsonObj.put("imei", mDeviceInfoUtil.getTojoyDeviceId(mContext));
            jsonObj.put("model", mDeviceInfoUtil.getPhoneMODEL());
            jsonObj.put("isEmulator", mDeviceInfoUtil.isEmulator());
            jsonObj.put("cpu", mDeviceInfoUtil.getCpuModel());
            jsonObj.put("manufacturer", mDeviceInfoUtil.getMANUFACTURER());
            jsonObj.put("sdkLevel", mDeviceInfoUtil.phoneVerionCode());
            jsonObj.put("deviceName", mDeviceInfoUtil.getMANUFACTURER() + mDeviceInfoUtil.getPhoneMODEL());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObj;
    }


    public String getOSVersion() {
        return mDeviceInfoUtil.phoneVerionCode();
    }

    public String getEquipment() {
        return mDeviceInfoUtil.getMANUFACTURER();
    }

    public String getEquipmentType() {
        return mDeviceInfoUtil.getPhoneMODEL();
    }

    /**
     * 获取用户信息
     *
     * @return
     */
    public JSONObject getUserProperty(Context context) {
        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("mobile", TextUtils.isEmpty(BaseUserInfoCache.getUserMobile(context)) ? "" : BaseUserInfoCache.getUserMobile(context));
            jsonObj.put("userId", TextUtils.isEmpty(BaseUserInfoCache.getUserId(context)) ? "" : BaseUserInfoCache.getUserId(context));
            jsonObj.put("userLevel", TextUtils.isEmpty(BaseUserInfoCache.getUserLevevl(context)) ? "" : BaseUserInfoCache.getUserLevevl(context));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObj;
    }


    /**
     * 获取App信息
     *
     * @return
     */
    public JSONObject getAppInfo(Context application) {
        JSONObject jsonObj = new JSONObject();

        String subCoopID = WalleChannelReader.getChannel(application);

        try {
            jsonObj.put("productId", "10000");
            jsonObj.put("channelId", TextUtils.isEmpty(subCoopID) ? "8888" : subCoopID);
            jsonObj.put("versionCode", BaseLibKit.getVersionName());
            jsonObj.put("visitDevice", "android");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObj;
    }
}
