package com.tojoy.tjoybaselib.net;

import android.content.Context;
import android.net.ParseException;

import com.google.gson.JsonParseException;

import org.json.JSONException;

import java.net.UnknownHostException;

import es.dmoral.toasty.Toasty;
import retrofit2.HttpException;

/**
 * 网络异常，处理
 * Created by MPW on 2017/11/6.
 */

public class HttpErrorUtil {
    static final String ERROR_MSG = "网络异常，请重试";

    //对应HTTP的状态码
    private static final int UNAUTHORIZED = 401;
    private static final int FORBIDDEN = 403;
    private static final int NOT_FOUND = 404;
    private static final int REQUEST_TIMEOUT = 408;
    private static final int INTERNAL_SERVER_ERROR = 500;
    private static final int BAD_GATEWAY = 502;
    private static final int SERVICE_UNAVAILABLE = 503;
    private static final int GATEWAY_TIMEOUT = 504;

    public static void error(Context context, Object e) {
        String errorMsg;
        if (e == null) {
            errorMsg = ERROR_MSG;
        } else if (e instanceof String) {
            errorMsg = (String) e;
        } else if (e instanceof HttpException) {
            HttpException httpE = (HttpException) e;
            switch (httpE.code()) {
                case UNAUTHORIZED:
                case FORBIDDEN:
                    errorMsg = "权限错误";
                    break;
                case REQUEST_TIMEOUT:
                case GATEWAY_TIMEOUT:
                    errorMsg = "连接超时";
                    break;
                default:
                    errorMsg = ERROR_MSG;
            }
        } else if (e instanceof JsonParseException
                || e instanceof JSONException
                || e instanceof ParseException) {
            errorMsg = "数据解析错误";
        } else if (e instanceof UnknownHostException) {
            //域名解析错误，一般为断网情况下出现
            errorMsg = ERROR_MSG;
        } else {
            errorMsg = ERROR_MSG;
        }
        try {
            Toasty.normal(context, errorMsg).show();
        } catch (Exception ex) {

        }
    }
}
