package com.tojoy.tjoybaselib.net;

import android.content.Context;
import android.text.TextUtils;

import com.tojoy.tjoybaselib.BaseLibKit;
import com.tojoy.tjoybaselib.BuildConfig;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApplyJoinLiveResponse;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApprovalListResponse;
import com.tojoy.tjoybaselib.model.OnlineMeeting.CheckUserInfoResponse;
import com.tojoy.tjoybaselib.model.enterprise.BrowsedEnterpriseResponse;
import com.tojoy.tjoybaselib.model.enterprise.CorporationMsgResponse;
import com.tojoy.tjoybaselib.model.enterprise.EnterpriseBannerResponse;
import com.tojoy.tjoybaselib.model.enterprise.EnterpriseHomeHotLiveListResponse;
import com.tojoy.tjoybaselib.model.enterprise.MyInvitationShareMsgResponse;
import com.tojoy.tjoybaselib.model.enterprise.NewGoodsDetailResponse;
import com.tojoy.tjoybaselib.model.enterprise.ProductDetailResponse;
import com.tojoy.tjoybaselib.model.home.HomePopResponse;
import com.tojoy.tjoybaselib.model.home.HomeReadCountResponse;
import com.tojoy.tjoybaselib.model.home.MeetingHomeModelResponse;
import com.tojoy.tjoybaselib.model.home.MeetingInviteNoticeResponse;
import com.tojoy.tjoybaselib.model.home.MineDistributionApplyRecordResponse;
import com.tojoy.tjoybaselib.model.home.MineDistributionChannelRecordResponse;
import com.tojoy.tjoybaselib.model.home.MineDistributionResponse;
import com.tojoy.tjoybaselib.model.home.MineDistributionSignStatusResponse;
import com.tojoy.tjoybaselib.model.home.MineDistributionWithdarwalApplyResponse;
import com.tojoy.tjoybaselib.model.home.MineDistributionWithdarwalDetailResponse;
import com.tojoy.tjoybaselib.model.home.MineMemberDistributionResponse;
import com.tojoy.tjoybaselib.model.home.MineProfitInfoDistributionResponse;
import com.tojoy.tjoybaselib.model.home.MineProfitListDistributionResponse;
import com.tojoy.tjoybaselib.model.home.MineWithdrawalAccountBindResponse;
import com.tojoy.tjoybaselib.model.home.ScreenAdvertisResponse;
import com.tojoy.tjoybaselib.model.intentionalorder.makesure.NewOrderMakeOrderResponse;
import com.tojoy.tjoybaselib.model.live.AgentLiveModel;
import com.tojoy.tjoybaselib.model.live.ApplyExploreCountResponse;
import com.tojoy.tjoybaselib.model.live.ApplyGuestListResponse;
import com.tojoy.tjoybaselib.model.live.CheckLiveStatusResponse;
import com.tojoy.tjoybaselib.model.live.DoumentIdStatusResponse;
import com.tojoy.tjoybaselib.model.live.DoumentVideoResponse;
import com.tojoy.tjoybaselib.model.live.EndLiveInfoResponse;
import com.tojoy.tjoybaselib.model.live.EnterLiveModel;
import com.tojoy.tjoybaselib.model.live.ExploreMemeberStatusResponse;
import com.tojoy.tjoybaselib.model.live.ExploreOnlineListResponse;
import com.tojoy.tjoybaselib.model.live.FileListResponse;
import com.tojoy.tjoybaselib.model.live.FileTagListResponse;
import com.tojoy.tjoybaselib.model.live.GoodsResponse;
import com.tojoy.tjoybaselib.model.live.InviteApplyResponse;
import com.tojoy.tjoybaselib.model.live.LiveNeedShowLeavePop;
import com.tojoy.tjoybaselib.model.live.LiveShareInfoModel;
import com.tojoy.tjoybaselib.model.live.LiveSignResponse;
import com.tojoy.tjoybaselib.model.live.LiveUserInfoResponse;
import com.tojoy.tjoybaselib.model.live.MyLiveRoomInfo;
import com.tojoy.tjoybaselib.model.live.MyLiveRoomListResponse;
import com.tojoy.tjoybaselib.model.live.NoticeModel;
import com.tojoy.tjoybaselib.model.live.OnlineFrameResponse;
import com.tojoy.tjoybaselib.model.live.OnlineUserInfoResponse;
import com.tojoy.tjoybaselib.model.live.OutlinkListResponse;
import com.tojoy.tjoybaselib.model.live.ProductListResponse;
import com.tojoy.tjoybaselib.model.live.PublishQRListResponse;
import com.tojoy.tjoybaselib.model.live.SelectOutLiveResponse;
import com.tojoy.tjoybaselib.model.live.SelectOutliveStatusResponse;
import com.tojoy.tjoybaselib.model.live.ShortLeaveListModel;
import com.tojoy.tjoybaselib.model.live.StartLiveResponse;
import com.tojoy.tjoybaselib.model.live.UserJoinResponse;
import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.model.pay.CheckDeviceResponse;
import com.tojoy.tjoybaselib.model.servicemanager.OrderModel;
import com.tojoy.tjoybaselib.model.servicemanager.RechargeCancelOrderResponse;
import com.tojoy.tjoybaselib.model.servicemanager.RechargeProductResponse;
import com.tojoy.tjoybaselib.model.servicemanager.RechargeSaveOrderResponse;
import com.tojoy.tjoybaselib.model.servicemanager.RemainTimeResponse;
import com.tojoy.tjoybaselib.model.servicemanager.UseDataResponse;
import com.tojoy.tjoybaselib.model.shopmall.ShopMallListResponse;
import com.tojoy.tjoybaselib.model.user.AddressResponse;
import com.tojoy.tjoybaselib.model.user.CustomerServiceResponse;
import com.tojoy.tjoybaselib.model.user.IMTokenResponse;
import com.tojoy.tjoybaselib.model.user.LoginResponse;
import com.tojoy.tjoybaselib.model.user.MemberListResponse;
import com.tojoy.tjoybaselib.model.user.MyAddressListResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.services.log.BaseLiveLog;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigResponse;
import com.tojoy.tjoybaselib.services.oss.model.StsResponseModel;
import com.tojoy.tjoybaselib.services.update.UpdateModel;
import com.tojoy.tjoybaselib.util.log.LogUtil;
import com.tojoy.tjoybaselib.util.net.RxUtils;
import com.tojoy.tjoybaselib.util.sys.DeviceInfoUtil;

import org.json.JSONException;
import org.json.JSONObject;

import rx.Observer;
import rx.Subscriber;

/**
 * online-meeting ApiProvider
 */

public class OMAppApiProvider extends BaseApiProvider {

    private static OMAppApiInterface apiService = null;

    private static OMAppApiInterface apiServiceChat = null;
    /**
     * //////////////////////////////
     * 构造单例
     * //////////////////////////////
     */
    private OMAppApiProvider() {
        init();
    }

    private static class SingletonHolder {
        private static final OMAppApiProvider INSTANCE = new OMAppApiProvider();
    }

    public static OMAppApiProvider getInstance() {
        return OMAppApiProvider.SingletonHolder.INSTANCE;
    }

    public static OMAppApiProvider initialize(Context context) {
        if (mContext == null) {
            mContext = context;
        }
        return OMAppApiProvider.SingletonHolder.INSTANCE;
    }

    public void init() {
        apiService = RetrofitSingleton.getInstance(mContext).app_retrofit.create(OMAppApiInterface.class);
        apiServiceChat = RetrofitSingleton.getInstance(mContext).retrofit.create(OMAppApiInterface.class);
    }


    public void getDeviceId(Context context, Observer<CheckDeviceResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("appVersion", BuildConfig.VERSION_NAME);
            dataJson.put("os", "Android");
            dataJson.put("imei", DeviceInfoUtil.getTojoyDeviceId(context));
            dataJson.put("osVersion", BaseParamterProvider.getInstance(context).getOSVersion());
            dataJson.put("equipment", BaseParamterProvider.getInstance(context).getEquipment());
            dataJson.put("equipmentType", BaseParamterProvider.getInstance(context).getEquipmentType());
            dataJson.put("source", "Android");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.getDeviceId(getCommandSign(dataJson, "getCode"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<CheckDeviceResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(CheckDeviceResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    public void updateDeviceId(Context context, String deviceId, Observer<CheckDeviceResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("appVersion", BuildConfig.VERSION_NAME);
            dataJson.put("id", deviceId);
            dataJson.put("os", "Android");
            dataJson.put("imei", DeviceInfoUtil.getTojoyDeviceId(context));
            dataJson.put("osVersion", BaseParamterProvider.getInstance(context).getOSVersion());
            dataJson.put("equipment", BaseParamterProvider.getInstance(context).getEquipment());
            dataJson.put("equipmentType", BaseParamterProvider.getInstance(context).getEquipmentType());
            dataJson.put("source", "Android");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.updateDeviceId(getCommandSign(dataJson, "getCode"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<CheckDeviceResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(CheckDeviceResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * OSS
     */
    public void getStsModel(Observer<StsResponseModel> observer) {
        JSONObject dataJson = new JSONObject();
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetStsModel(getCommandSign(dataJson, "getStsModel"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<StsResponseModel>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(StsResponseModel response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取验证码
     */
    public void getCode(String mobile, Observer<OMBaseResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("mobile", mobile);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = false;
        apiService.mGetCode(getCommandSign(dataJson, "getCode"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }
    /**
     * SDK 获取token 腾讯id im appkey等信息
     */
    public void newLogin(String mobile, String verifyCode, Observer<LoginResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("username", mobile);
            dataJson.put("smsVcode", verifyCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // /cloud-meeting-auth
        String url = RetrofitSingleton.getWWWWBaseUrl() + "cloud-meeting-auth/tc/login/app?username="
                + mobile + "&smsVcode=" + verifyCode + "&source=andriod";
        AppConfig.isAppendHeaderAuthorization = false;
        apiService.newLogin(url)
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<LoginResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(LoginResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 退出登录
     */
    public void logout(Observer<OMBaseResponse> observer) {

        JSONObject dataJson = new JSONObject();
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mLogout(getCommandSign(dataJson, "logout"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 获取IMToken
     */
    public void getIMToken(String userId, String isNewToken, Observer<IMTokenResponse> observer) {

        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("isNewToken", isNewToken);
            dataJson.put("userId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetIMToken(getCommandSign(dataJson, "getIMToken"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<IMTokenResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(IMTokenResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 首页弹窗
     *
     * @param observer
     */
    public void homePop(Observer<HomePopResponse> observer) {

        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("userId", BaseUserInfoCache.getUserId(mContext));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mHomePop(getCommandSign(dataJson, "homePop"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<HomePopResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(HomePopResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 开会申请页面信息
     *
     * @param observer
     */
    public void joinLiveApply(Observer<ApplyJoinLiveResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("userId", BaseUserInfoCache.getUserId(mContext));
            dataJson.put("companyCode", BaseUserInfoCache.getCompanyCode(mContext));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mJoinLiveApply(getCommandSign(dataJson, "mJoinLiveApply"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<ApplyJoinLiveResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(ApplyJoinLiveResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 开会申请--根据选择观看人数显示判断对应审批人
     *
     * @param observer
     */
    public void mGetCheckUser(Observer<CheckUserInfoResponse> observer, String lookCount) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("userId", BaseUserInfoCache.getUserId(mContext));
            dataJson.put("perNum", lookCount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetCheckUser(getCommandSign(dataJson, "mGetCheckUser"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<CheckUserInfoResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(CheckUserInfoResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 开会申请详情页面信息
     *
     * @param observer
     */
    public void getApplyInfoDetail(Observer<ApplyJoinLiveResponse> observer, String applyId) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("userId", BaseUserInfoCache.getUserId(mContext));
            dataJson.put("applyId", applyId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mApplyInfoDetail(getCommandSign(dataJson, "mApplyInfoDetail"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<ApplyJoinLiveResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(ApplyJoinLiveResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 网上天洽会--提交开会申请接口
     */
    public void mSubmitLiveApply(Observer<OMBaseResponse> observer, String roomName, String roomPassword, String viewNum,
                                 String playReason, String tagIds, String docUrl, String picUrl, String authType, String drainageType, String docName,
                                 String userIds, String mobiles, String isPlayback
        ,String outMobiles,String outNames,String nameIds) {
        JSONObject dataJson = new JSONObject();
        try {
            // 房间名称
            dataJson.put("roomName", roomName);
            // 房间密码
            dataJson.put("roomPassword", roomPassword);
            // 观看人数
            dataJson.put("viewNum", viewNum);
            // 开播原因
            dataJson.put("playReason", playReason);
            // 直播标签（逗号分隔）
            dataJson.put("tagIds", tagIds);
            // 文档路径
            dataJson.put("docUrl", docUrl);
            // 封面路径
            dataJson.put("picUrl", picUrl);
            // 观看权限
            dataJson.put("authType", authType);
            // 视频引流权限
            dataJson.put("drainageType", drainageType);
            // 文档名称
            dataJson.put("docName", docName);
            dataJson.put("userId", BaseUserInfoCache.getUserId(mContext));
            dataJson.put("userIds", userIds);
            dataJson.put("mobiles", mobiles);
            dataJson.put("names", nameIds);
            dataJson.put("isPlayback", isPlayback);

            //1.2.3 新增字段 外部被邀请人手机号
            dataJson.put("outMobiles", outMobiles);
            //外部被邀请人姓名
            dataJson.put("outNames", outNames);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mSubmitLiveApply(getCommandSign(dataJson, "mSubmitLiveApply"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 网上天洽会--查询权限处理列表
     *
     * @param status 全部什么都不传；未处理0；已处理1；
     */
    public void mQueryLiveApplyCheck(Observer<ApprovalListResponse> observer, String status, String pageNum) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("operateId", BaseUserInfoCache.getUserId(mContext));
            dataJson.put("status", status);
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", String.valueOf(AppConfig.PER_PAGE_COUNT));
            dataJson.put("userId", BaseUserInfoCache.getUserId(mContext));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mQueryLiveApplyCheck(getCommandSign(dataJson, "mQueryLiveApplyCheck"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<ApprovalListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(ApprovalListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 网上天洽会--权限处理（驳回、同意操作）
     */
    public void mApplyCheck(Observer<OMBaseResponse> observer, String checkId, String applyId, String status, String remark) {
        JSONObject dataJson = new JSONObject();
        try {
            // 审核记录ID
            dataJson.put("checkId", checkId);
            // 申请记录ID
            dataJson.put("applyId", applyId);
            // 状态：2：驳回；3：通过
            dataJson.put("status", status);
            // 备注
            dataJson.put("remark", remark);

            dataJson.put("userId", BaseUserInfoCache.getUserId(mContext));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mApplyCheck(getCommandSign(dataJson, "mApplyCheck"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 网上天洽会--撤回申请
     */
    public void mRevertLiveApply(Observer<OMBaseResponse> observer, String applyId) {
        JSONObject dataJson = new JSONObject();
        try {
            // 申请记录ID
            dataJson.put("applyId", applyId);
            dataJson.put("userId", BaseUserInfoCache.getUserId(mContext));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mRevertLiveApply(getCommandSign(dataJson, "mRevertLiveApply"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取用户登录腾讯TRTC的userSign
     */
    public void getLiveUserSign(String liveId, Observer<LiveSignResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
//            dataJson.put("appId", appId);
//            dataJson.put("functionId", liveId);
//            dataJson.put("userId", BaseUserInfoCache.getUserId(mContext));
            dataJson.put("liveId", liveId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetUserLiveSign(getCommandSign(dataJson, "getLiveUserSign"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<LiveSignResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(LiveSignResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 直播间 走后台 发消息
     *
     * @param account     网易账号
     * @param userName
     * @param company
     * @param job
     * @param imRoomId
     * @param roomLiveId  直播id
     * @param imCreatorid 直播间创建者id
     * @param msg         要发送的内容
     * @param observer
     */
    public void sendMsg(String account, String userName, String company, String job, String imRoomId,
                        String msg, String msgId, String roomLiveId, String imCreatorid, String roomId,
                        Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("fromAccid", account);
            dataJson.put("userName", userName);
            dataJson.put("company", company);
            dataJson.put("job", job);
            dataJson.put("imRoomId", imRoomId);
            dataJson.put("msg", msg);
            dataJson.put("msgId", msgId);
            dataJson.put("roomLiveId", roomLiveId);
            dataJson.put("imCreatorid", imCreatorid);
            dataJson.put("roomId", roomId);
            dataJson.put("source","1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mSendMsg(getCommandSign(dataJson, "sendMsgLiveRoom"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 设置直播间助手
     *
     * @param liveId
     * @param imRoomId
     * @param targetUserId
     * @param targetUserName
     * @param observer
     */
    public void addLiveHelper(String liveId, String imRoomId, String targetUserId, String targetUserName, Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
            dataJson.put("imRoomId", imRoomId);
            dataJson.put("targetUserId", targetUserId);
            dataJson.put("targetUserName", targetUserName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mAddLiveHelper(getCommandSign(dataJson, "addLiveHelper"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 取消直播间助手
     *
     * @param liveId
     * @param imRoomId
     * @param targetUserId
     * @param targetUserName
     * @param observer
     */
    public void cancleLiveHelper(String liveId, String imRoomId, String targetUserId, String targetUserName, Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
            dataJson.put("imRoomId", imRoomId);
            dataJson.put("targetUserId", targetUserId);
            dataJson.put("targetUserName", targetUserName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mCancleLiveHelper(getCommandSign(dataJson, "cancleLiveHelper"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取直播间个人信息
     *
     * @param userId
     * @param roomLiveId
     * @param observer
     */
    public void getMemberInfo(String userId, String roomLiveId, Observer<LiveUserInfoResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("userId", userId);
            dataJson.put("roomLiveId", roomLiveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mMemberInfo(getCommandSign(dataJson, "getMemberInfo"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<LiveUserInfoResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(LiveUserInfoResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取直播间状态
     *
     * @param roomLiveId
     * @param observer
     */
    public void getLiveRoomStatus(String roomLiveId, Observer<CheckLiveStatusResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("roomLiveId", roomLiveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetLiveRoomStatus(getCommandSign(dataJson, "getLiveRoomStatus"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<CheckLiveStatusResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(CheckLiveStatusResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 获取直播间退出信息
     *
     * @param isEnd 是否是结束直播  1是   0被踢      依此计算直播时长
     */
    public void getEndLiveRoomInfo(String liveId, String isEnd, Observer<EndLiveInfoResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
            dataJson.put("isEnd", isEnd);
            dataJson.put("startTime", LiveRoomInfoProvider.getInstance().startTime + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.getEndLiveRoomInfo(getCommandSign(dataJson, "getEndLiveRoomInfo"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<EndLiveInfoResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);

                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(EndLiveInfoResponse response) {
                        observer.onNext(response);
                    }
                });

    }

    /**
     * 当前用户是否对这个项目感兴趣
     */
    public void isInterested(String projectId, String userId, Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("projectId", projectId);
            dataJson.put("userId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mIsInterested(getCommandSign(dataJson, "isInterested"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });

    }

    //------------------------  满足push跳转直播间，从OnlineMeetingApiInterface迁移来的接口

    /**
     * 用户进入直播间检测是否需要输入密码
     *
     * @param observer
     * @param liveId   直播间id
     */
    public void checkEnterLiveRoom(Observer<OMBaseResponse> observer, String liveId) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("roomLiveId", liveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mCheckEnterLiveRoom(getCommandSign(dataJson, "checkEnterLiveRoom"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 马上开播--创建IMRoomId
     *
     * @param observer
     */
    public void startLive(Observer<StartLiveResponse> observer) {
        LogUtil.i(BaseLiveLog.LRApiRequestTag, "startLive");
        JSONObject dataJson = new JSONObject();
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mStartLive(getCommandSign(dataJson, "startLive"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<StartLiveResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(StartLiveResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 进入直播间
     *
     * @param liveId
     * @param userName
     * @param userAvatar
     * @param company
     * @param job
     * @param livePassword
     * @param observer
     */
    public void joinLiveRoom(String liveId, String userName, String userAvatar, String company,
                             String job, String livePassword, Observer<EnterLiveModel> observer) {
        LogUtil.i(BaseLiveLog.LRApiRequestTag, "joinLiveRoom");
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
            dataJson.put("userName", userName);
            dataJson.put("userAvatar", userAvatar);
            dataJson.put("company", company);
            dataJson.put("job", job);
            dataJson.put("livePassword", livePassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mJoinLiveRoom(getCommandSign(dataJson, "joinLiveRoom"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<EnterLiveModel>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(EnterLiveModel response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 加入大会
     *
     * @param meetingId
     * @param observer
     */
    public void joinMeeting(String meetingId, Observer<UserJoinResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            //直播间SDK接口替换时修改
           // dataJson.put("roomId", meetingId);
            dataJson.put("roomCode",meetingId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mJoinMeeting(getCommandSign(dataJson, "joinMeeting"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<UserJoinResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(UserJoinResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 退出直播间
     *
     * @param liveId
     * @param userName
     * @param userAvatar
     * @param company
     * @param job
     * @param observer
     */
    public void leaveLiveRoom(boolean isAppendHeaderAuthorization,String liveId, String userName, String userAvatar, String company, String job,String userId, Observer<OMBaseResponse> observer) {
        LogUtil.i(BaseLiveLog.LRApiRequestTag, "leaveLiveRoom");
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
            dataJson.put("userName", userName);
            dataJson.put("userAvatar", userAvatar);
            dataJson.put("company", company);
            dataJson.put("job", job);
            dataJson.put("imRoomId", LiveRoomInfoProvider.getInstance().imRoomId);
            dataJson.put("userType", LiveRoomInfoProvider.getInstance().myUserType);
            dataJson.put("appId", "app");
            dataJson.put("userId",userId);
            dataJson.put("mobile",BaseUserInfoCache.getUserMobile(BaseLibKit.getContext()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = isAppendHeaderAuthorization;
        apiService.mLeaveLiveRoom(getCommandSign(dataJson, "leaveLiveRoom"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 结束直播
     *
     * @param observer
     */
    public void endLive(boolean isAppendHeaderAuthorization, String liveId,String userId, Observer<OMBaseResponse> observer) {
        LogUtil.i(BaseLiveLog.LRApiRequestTag, "endLive");
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("roomLiveId", liveId);
            dataJson.put("appId", "app");
            dataJson.put("userId",userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = isAppendHeaderAuthorization;
        apiService.mEndLive(getCommandSign(dataJson, "mEndLive"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 暂离后继续直播
     *
     * @param liveId
     * @param userId
     * @param observer
     */
    public void goOnLive(String liveId, String userId, Observer<OMBaseResponse> observer) {
        LogUtil.i(BaseLiveLog.LRApiRequestTag, "goOnLive");
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
            dataJson.put("userId", userId);
            dataJson.put("enterCompanyCode", LiveRoomInfoProvider.getInstance().otherCompanyCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGoOnLive(getCommandSign(dataJson, "goOnLive"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * ===== 绘画板
     */
    public void setDocumentStatus(String liveId, String isShow, String boardId, Observer<OMBaseResponse> observer) {
        LogUtil.i(BaseLiveLog.LRApiRequestTag, "setDocumentStatus");
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("isShow", isShow);
            dataJson.put("liveId", liveId);
            dataJson.put("documentId", boardId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.setDocumentStatus(getCommandSign(dataJson, "setDocumentStatus"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    public void getDocumentStatus(String liveId, Observer<DoumentIdStatusResponse> observer) {
        LogUtil.i(BaseLiveLog.LRApiRequestTag, "getDocumentStatus");
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.getDocumentStatus(getCommandSign(dataJson, "getDocumentStatus"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<DoumentIdStatusResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(DoumentIdStatusResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * ===== 文档演示播放视频
     */
    public void setVideoStatus(String liveId, String status, String videoUrl, String playTime, Observer<OMBaseResponse> observer) {
        LogUtil.i(BaseLiveLog.LRApiRequestTag, "setVideoStatus");
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("status", status);
            dataJson.put("liveId", liveId);
            dataJson.put("url", videoUrl);
            dataJson.put("playTime", playTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.setVideoStatus(getCommandSign(dataJson, "setVideoStatus"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    public void getVideoStatus(String liveId, Observer<DoumentVideoResponse> observer) {
        LogUtil.i(BaseLiveLog.LRApiRequestTag, "getVideoStatus");
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.getVideoStatus(getCommandSign(dataJson, "getVideoStatus"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<DoumentVideoResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(DoumentVideoResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取直播间分享信息
     *
     * @param roomLiveId
     * @param observer
     */
    public void getShareInfo(String roomLiveId, Observer<LiveShareInfoModel> observer) {
        LogUtil.i(BaseLiveLog.LRApiRequestTag, "getShareInfo");
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("roomLiveId", roomLiveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetShareInfo(getCommandSign(dataJson, "getShareInfo"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<LiveShareInfoModel>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(LiveShareInfoModel response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 直播间项目推荐列表
     *
     * @param liveId
     * @param nameTitleLike 搜索的key
     * @param observer
     */
    public void getGoodsTJ(String userId, String liveId, String nameTitleLike, String pageNum, Observer<ShopMallListResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
            dataJson.put("nameTitleLike", nameTitleLike);
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", "200");
            dataJson.put("userId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetGoodsTJ(getCommandSign(dataJson, "getGoodsTJ"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<ShopMallListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(ShopMallListResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 直播间发布 商品或广告
     *
     * @param liveId
     * @param imRoomId
     * @param projectId
     * @param projectName
     * @param projectImage
     * @param projectDesc
     * @param observer
     */
    public void publishProject(String liveId, String imRoomId, String projectId, String projectName, String projectImage,
                               String projectDesc, String projectType, String storeId,String agentCompanyCode,
                               String agentId,String agentLive,Observer<OMBaseResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
            dataJson.put("imRoomId", imRoomId);
            dataJson.put("projectId", projectId);
            dataJson.put("projectName", projectName);
            dataJson.put("projectImage", projectImage);
            dataJson.put("projectDesc", projectDesc);
            dataJson.put("projectType", projectType);
            dataJson.put("storeId", storeId);
            //发布代播商品传参
            dataJson.put("agentCompanyCode",agentCompanyCode);
            dataJson.put("agentId",agentId);
            dataJson.put("agentLive",agentLive);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mPublishProject(getCommandSign(dataJson, "publishProject"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 我感兴趣
     *
     * @param fkId 商品ID
     */
    public void interestedV2(String fkId, String roomId, String roomLiveId,String agentCompanyCode,
                             String agentId,String agentLive, Observer<OMBaseResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("fkId", fkId);
            if (!TextUtils.isEmpty(roomId) && !TextUtils.isEmpty(roomLiveId)) {
                dataJson.put("roomId", roomId);
                dataJson.put("roomLiveId", roomLiveId);
                dataJson.put("source", "app");
            } else {
                //原参数app_details 改为app 数据统计二期需求
                dataJson.put("source", "app");

            }
            dataJson.put("agentCompanyCode",agentCompanyCode);
            dataJson.put("agentId",agentId);
            dataJson.put("agentLive",agentLive);
            dataJson.put("enterCompanyCode", LiveRoomInfoProvider.getInstance().otherCompanyCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mILikeV2(getCommandSign(dataJson, "interestedV2"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取直播间在线用户列表
     * V1.2.3 改为观众端专用
     * @param roomId
     * @param liveId
     * @param observer
     */
    public void getMemberList(String roomId, String liveId, String pageNum, String pageSize, Observer<MemberListResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("roomId", roomId);
            dataJson.put("roomLiveId", liveId);
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", pageSize);
            dataJson.put("applyId",LiveRoomInfoProvider.getInstance().applyId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetLiveMemberList(getCommandSign(dataJson, "getMemberList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MemberListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MemberListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 直播间在线成员搜索
     * V1.2.3 改为观众端专用
     * @param customerFlag 0标识成员；1标识客户
     */
    public void searchMemberList(String customerFlag, String key, String roomId, String liveId, String pageNum, String pageSize, Observer<MemberListResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("customerFlag", customerFlag);
            dataJson.put("textValue", key);
            dataJson.put("roomId", roomId);
            dataJson.put("roomLiveId", liveId);
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", pageSize);
            dataJson.put("applyId",LiveRoomInfoProvider.getInstance().applyId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetMemberList(getCommandSign(dataJson, "getLiveMemberList-search"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MemberListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MemberListResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 观众端分页搜索人脉
     * @param customerFlag
     * @param key
     * @param roomId
     * @param liveId
     * @param pageNum
     * @param pageSize
     * @param observer
     */
    public void searchPageMemberList(String customerFlag, String key, String roomId, String liveId, String pageNum, String pageSize, Observer<MemberListResponse> observer){
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("customerFlag", customerFlag);
            dataJson.put("textValue", key);
            dataJson.put("roomId", roomId);
            dataJson.put("roomLiveId", liveId);
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", pageSize);
            dataJson.put("applyId",LiveRoomInfoProvider.getInstance().applyId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.searchMemberOnlinePage(getCommandSign(dataJson, "getLiveMemberList-search"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MemberListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MemberListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * V1.2.3 主播助手-已入会成员列表
     */
    public void getMemberListOwner(String roomId, String liveId, String pageNum, String pageSize,String applyId, Observer<MemberListResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("roomId", roomId);
            dataJson.put("roomLiveId", liveId);
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", pageSize);
            //新增会议申请Id
            dataJson.put("applyId",applyId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.getLiveMemberList(getCommandSign(dataJson, "getMemberListOwner"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MemberListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MemberListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * V1.2.3 主播助手-未入会成员列表
     */
    public void getUnEnterMemberList(String roomId, String liveId, String pageNum, String pageSize, Observer<MemberListResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("roomId", roomId);
            dataJson.put("roomLiveId", liveId);
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", pageSize);
            //新增
            dataJson.put("applyId",LiveRoomInfoProvider.getInstance().applyId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.unEnterMemberList(getCommandSign(dataJson, "getUnEnterMemberList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MemberListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MemberListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * V1.2.3 主播助手-添加参会人
     * @param mobiles 手机号串 ,隔开
     * @param names 名字串 ,隔开
     */
    public void addMember(String liveId, String mobiles, String names, Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("mobiles", mobiles);
            dataJson.put("names", names);
            dataJson.put("liveId", liveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.addMember(getCommandSign(dataJson, "inviteMember"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * V1.2.3 主播助手-邀请入会
     * @param userId 传“”邀请未入会全员
     */
    public void inviteMember(String userId, String liveId, Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("userId", userId);
            dataJson.put("roomLiveId", liveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.inviteMember(getCommandSign(dataJson, "inviteMember"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * V1.2.3 主播助手-成员列表搜索
     */
    public void searchMember(String textValue, String liveId, String pageNum, String pageSize, Observer<MemberListResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("textValue", textValue);
            dataJson.put("roomLiveId", liveId);
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", pageSize);
            //数据二期统计新增
            dataJson.put("applyId",LiveRoomInfoProvider.getInstance().applyId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.searchMember(getCommandSign(dataJson, "searchMember"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MemberListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MemberListResponse response) {
                        observer.onNext(response);
                    }
                });
    }
    /**
     * V1.4.3 主播助手-分页成员列表搜索
     */
    public void searchMemberPage(String textValue, String liveId, String pageNum, String pageSize, Observer<MemberListResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("textValue", textValue);
            dataJson.put("roomLiveId", liveId);
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", pageSize);
            //数据二期统计新增
            dataJson.put("applyId",LiveRoomInfoProvider.getInstance().applyId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.searchMemberPage(getCommandSign(dataJson, "searchMember"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MemberListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MemberListResponse response) {
                        observer.onNext(response);
                    }
                });
    }



    /**
     * 浏览过的企业
     *
     * @param companyName 为空：游览列表  不为空：搜索列表
     * @param pageNum
     * @param observer
     */
    public void browsedEnterprise(String companyName, int pageNum, Observer<BrowsedEnterpriseResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            if (!TextUtils.isEmpty(companyName)) {
                dataJson.put("companyName", companyName);
            } else {
                dataJson.put("pageNum", pageNum);
                dataJson.put("pageSize", String.valueOf(AppConfig.PER_PAGE_COUNT));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mBrowsedEnterprise(getCommandSign(dataJson, "browsedEnterprise"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<BrowsedEnterpriseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(BrowsedEnterpriseResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 主播端获取暂离文字信息列表
     *
     * @param observer
     */
    public void getShortLeaveTextLeave(Observer<ShortLeaveListModel> observer) {

        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("enterCompanyCode", LiveRoomInfoProvider.getInstance().otherCompanyCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mShortLeaveTextList(getCommandSign(dataJson, "getShortLeaveTextLeave"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<ShortLeaveListModel>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(ShortLeaveListModel response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 主播端提交暂离列表
     *
     * @param liveId
     * @param textMsg
     * @param pauseMin 暂离时长 单位分钟
     * @param observer
     */
    public void commitShortLeaveInfo(String liveId, String textMsg, String pauseMin, Observer<OMBaseResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "room/addRoomLivePause");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
            dataJson.put("textMsg", textMsg);
            dataJson.put("pauseMin", pauseMin);
            dataJson.put("enterCompanyCode", LiveRoomInfoProvider.getInstance().otherCompanyCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mCommitShortLeaveInfo(getCommandSign(dataJson, "commitShortLeaveInfo"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 聊天室举报
     *
     * @param userId
     * @param targetUserId  主播ID
     * @param tipoffContent 举报内容
     * @param observer
     */
    public void liveReport(String roomLiveId, String userId, String targetUserId, String tipoffContent, Observer<OMBaseResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "room/insertRoomLiveTipoff");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("roomLiveId", roomLiveId);
            dataJson.put("userId", userId);
            dataJson.put("targetUserId", targetUserId);
            dataJson.put("tipoffContent", tipoffContent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mLiveReport(getCommandSign(dataJson, "liveReport"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 直播间发公告
     *
     * @param imRoomId
     * @param roomLiveId
     * @param publicBoardName 公告内容
     * @param observer
     */
    public void livePublishNotice(String imRoomId, String roomLiveId, String publicBoardName, String isShow, Observer<OMBaseResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "room/publishOfficial");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("imRoomId", imRoomId);
            dataJson.put("roomLiveId", roomLiveId);
            dataJson.put("publicBoardName", publicBoardName);
            dataJson.put("enterCompanyCode", LiveRoomInfoProvider.getInstance().otherCompanyCode);
            dataJson.put("isShow", isShow);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mPublishNotice(getCommandSign(dataJson, "livePublishNotice"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取引流数据
     *
     * @param textValue
     * @param roomLiveId
     * @param observer
     */
    public void mOuterLiveRoomList(String roomLiveId, String textValue, int pageNum, Observer<OutlinkListResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "blendStream/liveRoomStream");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", roomLiveId);
            dataJson.put("textValue", textValue);
            dataJson.put("pageNum", String.valueOf(pageNum));
            dataJson.put("pageSize", String.valueOf(AppConfig.PER_PAGE_COUNT));
            dataJson.put("enterCompanyCode", LiveRoomInfoProvider.getInstance().otherCompanyCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mOuterLiveRoomList(getCommandSign(dataJson, "mOuterLiveRoomList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OutlinkListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OutlinkListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取引流数据
     *
     * @param textValue
     * @param roomLiveId
     * @param observer
     */
    public void mSelectAOuterLive(String roomLiveId, String textValue, String streamIds, String onlineCount, Observer<SelectOutLiveResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "blendStream/selectLiveStream");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("currentRoomLiveId", roomLiveId);
            dataJson.put("pushStreamRoomId", textValue);
            dataJson.put("streamIds", streamIds);
            dataJson.put("onlineCount", onlineCount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mSelectAOuterLive(getCommandSign(dataJson, "mSelectAOuterLive"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<SelectOutLiveResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(SelectOutLiveResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    public void startANewLiveStream(String liveId, String userId, String liveAcceptId, Observer<OMBaseResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "blendStream/closeLiveStream");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveAcceptId", liveAcceptId);
            dataJson.put("liveId", liveId);
            dataJson.put("userId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mStartANewLive(getCommandSign(dataJson, "startANewLiveStream"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    public void closeLiveStream(String videoStreamId, String currentLiveRoomLiveId, Observer<OMBaseResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "blendStream/closeLiveStream");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("closeStreamId", videoStreamId);
            dataJson.put("currentLiveId", currentLiveRoomLiveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.closeLiveStream(getCommandSign(dataJson, "closeLiveStream"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    public void getLiveStreamStatus(String liveId, Observer<SelectOutliveStatusResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "blendStream/getLiveStreamStatus");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.getLiveStreamStatus(getCommandSign(dataJson, "getLiveStreamStatus"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<SelectOutliveStatusResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(SelectOutliveStatusResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取公告
     *
     * @param roomLiveId
     * @param observer
     */
    public void getNotice(String roomLiveId, Observer<NoticeModel> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("roomLiveId", roomLiveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetNotice(getCommandSign(dataJson, "getNotice"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<NoticeModel>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(NoticeModel response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * ======================================= 同台直播模块
     */
    /**
     * 观众 - 申请同台直播
     */
    public void mApplyExploreMe(String roomId, String liveId, String postId, String receiveId, Observer<OMBaseResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "sameLive/applyLive");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("roomId", roomId);
            dataJson.put("liveId", liveId);
            dataJson.put("postId", postId);
            dataJson.put("receiveId", receiveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mApplyExploreMe(getCommandSign(dataJson, "mApplyExploreMe"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 观众 - 开始同台直播
     */
    public void mStartExploreMe(String userId, String liveId, Observer<OMBaseResponse> observer) {
        LogUtil.i(BaseLiveLog.LRApiRequestTag, "sameLive/startLive");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("applyForId", userId);
            dataJson.put("liveId", liveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mStartExploreMe(getCommandSign(dataJson, "mStartExploreMe"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 观众 - 主动结束同台直播
     */
    public void mStopExploreMe(boolean isAppendHeaderAuthorization,String userId, String liveId, Observer<OMBaseResponse> observer) {
        LogUtil.i(BaseLiveLog.LRApiRequestTag, "sameLive/finishLive");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("userId", userId);
            dataJson.put("liveId", liveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = isAppendHeaderAuthorization;
        apiService.mStopExploreMe(getCommandSign(dataJson, "mStopExploreMe"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);

                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 主播 - 结束同台直播
     */
    public void mStopExploreSomebody(String userId, String liveId, Observer<OMBaseResponse> observer) {
        LogUtil.i(BaseLiveLog.LRApiRequestTag, "sameLive/closeinvitation");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("userId", userId);
            dataJson.put("liveId", liveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mStopExploreSomebody(getCommandSign(dataJson, "mStopExploreSomebody"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 主播 - 邀请同台直播
     */
    public void mInviteExploreSomebody(String liveId, String receiveIds, Observer<InviteApplyResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "sameLive/invitationLive");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
            dataJson.put("receiveIds", receiveIds);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mInviteExploreSomebody(getCommandSign(dataJson, "mInviteExploreSomebody"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<InviteApplyResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(InviteApplyResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 主播 - 同意或拒绝同台直播申请
     * 观众 - 同意或拒绝同台直播邀请
     */
    public void mResponseForApplyOrInviteExplore(String roomId, String userId, String receiveId, String oprationType, String oprationStatus, Observer<OMBaseResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "sameLive/selectRequest");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("roomId", roomId);
            dataJson.put("applyForId", userId);
            dataJson.put("receiveId", receiveId);
            dataJson.put("oprationType", oprationType);
            dataJson.put("oprationStatus", oprationStatus);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mResponseForApplyOrInviteExplore(getCommandSign(dataJson, "mResponseForApplyOrInviteExplore"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 设置同台观众的音视频状态
     * @param isCalling 设置通话状态：0：挂断/没操作；1：正在打电话
     */
    public void mControlGuestMediaStatus(String liveId, String guestId, String status, String type, String isCalling, Observer<OMBaseResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "sameLive/setLiveOpration");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
            dataJson.put("status", status);
            dataJson.put("type", type);
            dataJson.put("selectUserId", guestId);
            if (!TextUtils.isEmpty(isCalling)) {
                dataJson.put("isCalling",isCalling);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mControlGuestMediaStatus(getCommandSign(dataJson, "mControlGuestMediaStatus"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 助手获取同台观众的音视频状态
     */
    public void mGetGuestMediaStatus(String liveId, Observer<ExploreMemeberStatusResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "sameLive/getLiveOpration");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetGuestMediaStatus(getCommandSign(dataJson, "mGetGuestMediaStatus"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<ExploreMemeberStatusResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(ExploreMemeberStatusResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 设置同台用户上屏
     */
    public void mSetBigScreenUser(String liveId, String userId, Observer<OMBaseResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "sameLive/setframeStatus");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
            dataJson.put("userId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mSetBigScreenUser(getCommandSign(dataJson, "mSetBigScreenUser"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);

                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 设置同台用户上屏
     */
    public void mGetBigScreenUser(String liveId, Observer<OnlineFrameResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "sameLive/getframeStatus");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetBigScreenUser(getCommandSign(dataJson, "mGetBigScreenUser"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OnlineFrameResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OnlineFrameResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 主播 - 模糊搜索在线成员
     */
    public void mSearchOnlineGuestList(String liveId, String textValue, String pageNum, Observer<ExploreOnlineListResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "sameLive/search");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
            dataJson.put("textValue", textValue);
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", "10");
            //数据统计二期新增
            dataJson.put("applyId",LiveRoomInfoProvider.getInstance().applyId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mSearchOnlineGuestList(getCommandSign(dataJson, "mSearchOnlineGuestList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<ExploreOnlineListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(ExploreOnlineListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 主播 - 获取当前申请人名单
     */
    public void mApplyExplorerList(String liveId, String pageNum, String pageSize, Observer<ApplyGuestListResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "sameLive/applyForList");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", pageSize);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mApplyExplorerList(getCommandSign(dataJson, "mApplyExplorerList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<ApplyGuestListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(ApplyGuestListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 主播 - 获取当前申请人名单
     */
    public void mGetApplyCount(String liveId, Observer<ApplyExploreCountResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "sameLive/applyForCount");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetCurrentApplyCount(getCommandSign(dataJson, "mGetCurrentApplyCount"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<ApplyExploreCountResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(ApplyExploreCountResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 用户 - 获取用户信息根据userId
     */
    public void mGetUserByUserId(String userId, Observer<OnlineUserInfoResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "friend/userInformation");

        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("userId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetUserByUserId(getCommandSign(dataJson, "mGetUserByUserId"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OnlineUserInfoResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OnlineUserInfoResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * @param imRoomId
     * @param roomLiveId
     * @param mute       1禁言  0解禁
     * @param observer
     */
    public void muteAll(String imRoomId, String roomLiveId, String mute, Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("imRoomId", imRoomId);
            dataJson.put("roomLiveId", roomLiveId);
            dataJson.put("mute", mute);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.muteAll(getCommandSign(dataJson, "muteAll"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取文档索引列表
     */
    public void mGetFileTagList(String liveId, String enterCompanyCode, Observer<FileTagListResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "liveDocument/selectRoomTagList");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
            dataJson.put("enterCompanyCode", enterCompanyCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetFileTagList(getCommandSign(dataJson, "mGetFileTagList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<FileTagListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(FileTagListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取某个文件夹下的文件列表
     */
    public void mGetFileListWithTagId(String liveId, String tagId, String fileName, String pageNum, String enterCompanyCode, Observer<FileListResponse> observer) {

        LogUtil.i(BaseLiveLog.LRApiRequestTag, "liveDocument/selectDir");

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("tagId", tagId);
            dataJson.put("liveId", liveId);
            dataJson.put("pageSize", String.valueOf(AppConfig.PER_PAGE_COUNT));
            dataJson.put("pageNum", pageNum);
            dataJson.put("enterCompanyCode", enterCompanyCode);
            if (!TextUtils.isEmpty(fileName)) {
                dataJson.put("fileName", fileName);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetFileListWithTagId(getCommandSign(dataJson, "mGetFileListWithTagId"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<FileListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(FileListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 删除文件夹下的文件
     *
     * @param extraId
     * @param isMe
     * @param liveId
     * @param observer
     */
    public void removeDocumentFile(String extraId, String isMe, String liveId, Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("extraId", extraId);
            dataJson.put("isMe", isMe);
            dataJson.put("liveId", liveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.removeDocumentFile(getCommandSign(dataJson, "removeDocumentFile"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 直播结束页面-推荐列表
     *
     * @param pageNum
     * @param pageSize
     * @param status
     * @param observer
     */
    public void getRecommendLiveList(String pageNum, String pageSize, String status, Observer<MyLiveRoomListResponse> observer) {
        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", pageSize);
            dataJson.put("status", status);
            dataJson.put("comCode", LiveRoomInfoProvider.getInstance().comCode);
            //SDk增加
            //dataJson.put("roomCode",roomCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetRecommendLiveList(getCommandSign(dataJson, "getRecommendLiveList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MyLiveRoomListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MyLiveRoomListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取全量地址
     */
    public void getAddress(String userId, Observer<AddressResponse> observer) {
        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("userId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.getAddressList(getCommandSign(dataJson, "getAddress"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<AddressResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(AddressResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 获取我的地址列表
     *
     * @param observer
     */
    public void getMyAddressList(int page, String userId, Observer<MyAddressListResponse> observer) {
        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("userId", userId);
            dataJson.put("pageNum", String.valueOf(page));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.getMyAddressList(getCommandSign(dataJson, "getMyAddressList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MyAddressListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MyAddressListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取我的地址列表
     *
     * @param observer
     */
    public void addAddress(String userId,
                           String provinceId,
                           String provinceName,
                           String cityId,
                           String cityName,
                           String districtId,
                           String districtName,
                           String userName,
                           String tag,
                           String userMobile,
                           String remark,
                           String isDefault,
                           Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("userId", userId);
            dataJson.put("provinceId", provinceId);
            dataJson.put("provinceName", provinceName);
            dataJson.put("cityId", cityId);
            dataJson.put("cityName", cityName);
            dataJson.put("districtId", districtId);
            dataJson.put("districtName", districtName);
            dataJson.put("userName", userName);
            dataJson.put("tag", tag);
            dataJson.put("userMobile", userMobile);
            dataJson.put("remark", remark);
            dataJson.put("isDefault", isDefault);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.addAddress(getCommandSign(dataJson, "addAddress"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    public void editAddress(String pkId,
                            String userId,
                            String provinceId,
                            String provinceName,
                            String cityId,
                            String cityName,
                            String districtId,
                            String districtName,
                            String userName,
                            String tag,
                            String userMobile,
                            String remark,
                            String isDefault,
                            Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("pkId", pkId);
            dataJson.put("userId", userId);
            dataJson.put("provinceId", provinceId);
            dataJson.put("provinceName", provinceName);
            dataJson.put("cityId", cityId);
            dataJson.put("cityName", cityName);
            dataJson.put("districtId", districtId);
            dataJson.put("districtName", districtName);
            dataJson.put("userName", userName);
            dataJson.put("tag", tag);
            dataJson.put("userMobile", userMobile);
            dataJson.put("remark", remark);
            dataJson.put("isDefault", isDefault);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.editAddress(getCommandSign(dataJson, "addAddress"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 查询企业信息
     *
     * @param code
     * @param isEnterprise 是否是企业定制首页并且想要包含导航栏的信息
     * @param observer
     */
    public void mCorporationQuery(String code, boolean isEnterprise, Observer<CorporationMsgResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("enterCompanyCode", code);
            if (isEnterprise) {
                dataJson.put("corporationNav", new JSONObject());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mCorporationQuery(getCommandSign(dataJson, "mCorporationQuery"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<CorporationMsgResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(CorporationMsgResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 热门直播间列表
     *
     * @param code
     * @param page
     * @param pageSize
     * @param type     1:列表；2:搜索
     * @param search
     * @param observer
     */
    public void mHotRoomLive(String code, int page, int pageSize, int type, String search, Observer<EnterpriseHomeHotLiveListResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            if (!TextUtils.isEmpty(code)) {
                dataJson.put("enterCompanyCode", code);
            }
            // 接口类型，1:列表；2:搜索
            dataJson.put("type", type);
            dataJson.put("page", page);
            dataJson.put("pageSize", pageSize);
            if (!TextUtils.isEmpty(search)) {
                dataJson.put("search", search);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mHotRoomLive(getCommandSign(dataJson, "mHotRoomLive"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<EnterpriseHomeHotLiveListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(EnterpriseHomeHotLiveListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取企业定制首页banner列表
     *
     * @param code
     * @param observer
     */
    public void mGetCorporationBanner(String code, Observer<EnterpriseBannerResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("code", code);
            dataJson.put("pageNum", 1);
            dataJson.put("pageSize", 10);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetCorporationBanner(getCommandSign(dataJson, "mGetCorporationBanner"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<EnterpriseBannerResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(EnterpriseBannerResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 新下单接口
     *
     * @param goodsId
     * @param addressId
     * @param number
     * @param orderSource
     * @param orderType
     * @param roomLiveId
     * @param storeId
     * @param enterpriseId
     * @param sourceWay （数据统计二期增加） 直播中:0   回放:1   默认企业主页:2（用户通过直播间进入企业主页算是直播间的）
     */
    public void newSureOder(String goodsId, String addressId,
                            String number, String orderSource,
                            String orderType, String roomLiveId,
                            String storeId, String enterpriseId,
                            String customerRemark, String price,String sourceWay,String agentCompanyCode,
                            String agentId,String agentLive, Observer<NewOrderMakeOrderResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("goodsId", goodsId);
            dataJson.put("addressId", addressId);
            dataJson.put("number", number);
            dataJson.put("orderSource", orderSource);
            dataJson.put("orderType", orderType);
            dataJson.put("roomLiveId", roomLiveId);
            dataJson.put("storeId", storeId);
            dataJson.put("enterpriseId", enterpriseId);
            dataJson.put("customerRemark", customerRemark);
            dataJson.put("orderPayPrice", price);
            //新增区分来源字段
            dataJson.put("sourceWay",sourceWay);
            dataJson.put("agentCompanyCode",agentCompanyCode);
            dataJson.put("agentId",agentId);
            dataJson.put("agentLive",agentLive);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mNewSureOder(getCommandSign(dataJson, "mUserOrder"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {
                }).subscribe(new Subscriber<NewOrderMakeOrderResponse>() {
            @Override
            public void onCompleted() {
                observer.onCompleted();
                BaseUserInfoCache.saveDataSign(mContext);

            }

            @Override
            public void onError(Throwable e) {
                observer.onError(e);

            }

            @Override
            public void onNext(NewOrderMakeOrderResponse orderMakeOrderResponse) {
                observer.onNext(orderMakeOrderResponse);
            }
        });
    }


    public void mAddVisit(String goodsId, Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("goodsId", goodsId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mAddGoodsVisit(getCommandSign(dataJson, "mAddVisit"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {
                }).subscribe(new Subscriber<OMBaseResponse>() {
            @Override
            public void onCompleted() {
                observer.onCompleted();
                BaseUserInfoCache.saveDataSign(mContext);

            }

            @Override
            public void onError(Throwable e) {
                observer.onError(e);

            }

            @Override
            public void onNext(OMBaseResponse omBaseResponse) {
                observer.onNext(omBaseResponse);
            }
        });
    }

    /**
     * 删除我的房间中的数据
     *
     * @param liveId
     * @param observer
     */
    public void deleteMyLive(String liveId, Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mDeleteMyLive(getCommandSign(dataJson, "deleteMyLive"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 删除删除观看记录
     *
     * @param roomLiveId
     * @param observer
     */
    public void removeViewRecord(String roomLiveId, Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("roomLiveId", roomLiveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.removeViewRecord(getCommandSign(dataJson, "removeViewRecord"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 网上天洽会--获取我的房间的房间id/直播次数信息
     */
    public void getMyRoomInfo(Observer<MyLiveRoomInfo> observer) {
        JSONObject dataJson = new JSONObject();
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetMyRoomInfo(getCommandSign(dataJson, "getMyRoomInfo"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MyLiveRoomInfo>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MyLiveRoomInfo response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 网上天洽会--我的房间列表
     *
     * @param status   10 直播数据， 11 回放数据
     *                 //直播状态（1:未开始；2:直播中；3:暂停；4:结束'），如果不传此值，返回该用户所有直播
     * @param observer
     */
    public void getMyLiveRoomList(String roomId, String pageNum, String pageSize, String status, Observer<MyLiveRoomListResponse> observer) {
        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("roomId", roomId);
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", pageSize);
            if (status != null) {
                dataJson.put("status", status);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetMyRoomList(getCommandSign(dataJson, "getMyLiveRoomList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MyLiveRoomListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MyLiveRoomListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 网上天洽会--我的观看记录列表
     *
     * @param status   10 直播数据， 11 回放数据
     *                 //直播状态（1:未开始；2:直播中；3:暂停；4:结束'），如果不传此值，返回该用户所有直播
     * @param observer
     */
    public void mGetViewRecordList(String pageNum, String pageSize, String status, Observer<MyLiveRoomListResponse> observer) {
        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("userId", BaseUserInfoCache.getUserId(mContext));
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", pageSize);
            dataJson.put("commandName", "getViewRecordList");
            if (status != null) {
                dataJson.put("status", status);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetViewRecordList(getCommandSign(dataJson, "mGetViewRecordList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MyLiveRoomListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MyLiveRoomListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 给客服留言，反馈
     * https://www.easyapi.com/api/view/75772?documentId=10385&themeId=&categoryId=18533
     *
     * @param observer
     */
    public void leaveMsgSave(String cont, Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("aiId", BaseUserInfoCache.getDeviceId(mContext));
            dataJson.put("uId", BaseUserInfoCache.getUserId(mContext));
            dataJson.put("appVersion", BuildConfig.VERSION_NAME);
            dataJson.put("cont", cont);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mLeaveMsgSave(getCommandSign(dataJson, "leaveMsgSave"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取客服电话
     *
     * @param observer
     */
    public void mGetCustomerServicePhone(Observer<CustomerServiceResponse> observer) {
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetCustomerServicePhone(getCommandSign(null, "mGetCustomerServicePhone"))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<CustomerServiceResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(CustomerServiceResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 上传个人头像
     *
     * @param observer
     */
    public void mEditUerImage(String userId, String imagePath, Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("id", userId);
            dataJson.put("imagePath", imagePath);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mEditUerImage(getCommandSign(dataJson, "mEditUerImage"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 获取产品详情数据
     *
     * @param observer
     */
    public void getProjectProductDetail(String productId, Observer<ProductDetailResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("productId", productId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mProductShareDetail(getCommandSign(dataJson, "getProjectProductDetail"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<ProductDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(ProductDetailResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取产品详情数据
     *
     * @param observer
     */
    public void mCorporationDetail(String id, Observer<ProductDetailResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mCorporationDetail(getCommandSign(dataJson, "getProjectProductDetail"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<ProductDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(ProductDetailResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 上传个人头像
     *
     * @param observer
     */
    public void mGetUserInfo(String userId, Observer<LoginResponse> observer) {
        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("userId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetUserInfo(getCommandSign(dataJson, "mGetUserInfo"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<LoginResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(LoginResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    public void mEditUserName(String userId,String userName, Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("id", userId);
            dataJson.put("name", userName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mUpdatUser(getCommandSign(dataJson, "mEditUserName"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 升级
     */
    public void checkUpdate(Observer<UpdateModel> observer) {

        JSONObject dataJson = new JSONObject();
        try {

            dataJson.put("version", BuildConfig.VERSION_NAME);
            dataJson.put("os", "android");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = false;
        apiService.mUpgrade(getCommandSign(dataJson, "checkUpdate"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<UpdateModel>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(UpdateModel response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 权限处理小红点（进入页面通知后台已读）
     */
    public void updateReadStatus(Observer<OMBaseResponse> observer) {

        JSONObject dataJson = new JSONObject();
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.updateReadStatus(getCommandSign(dataJson, "updateReadStatus"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 权限处理小红点（未处理消息数量）
     */
    public void showNum(Observer<HomeReadCountResponse> observer) {

        JSONObject dataJson = new JSONObject();
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.showNum(getCommandSign(dataJson, "showNum"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<HomeReadCountResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(HomeReadCountResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 开屏广告
     */
    public void openScreenAdvertis(int width, int height, Observer<ScreenAdvertisResponse> observer) {

        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("imgWidth", String.valueOf(width));
            dataJson.put("imgHeight", String.valueOf(height));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mOpenScreenAdvertis(getCommandSign(dataJson, "mOpenScreenAdvertis"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<ScreenAdvertisResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(ScreenAdvertisResponse response) {
                        observer.onNext(response);
                    }
                });

    }

    /**
     * 个推绑定账号
     */
    public void pushBind(String clientId, Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("clientId", clientId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mPushBind(getCommandSign(dataJson, "pushBind"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {
                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 个推解绑账号
     */
    public void pushUnBind(String clientId, Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("clientId", clientId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mPushUnBind(getCommandSign(dataJson, "mPushUnBind"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {
                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 用户启动APP累计次数(每个自然日)
     */
    public void startUpAppCountByUser(Observer<OMBaseResponse> observer) {

        JSONObject dataJson = new JSONObject();
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.startUpAppCountByUser(getCommandSign(dataJson, "startUpAppCountByUser"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });

    }

    /**
     * 统计IM登录成败
     *
     * @param objType  1 成功 0 失败  2进聊天室失败
     * @param objId    目前写 enterLivePage
     * @param observer
     */
    public void statitiscIMLogin(String objType, String objId, Observer<OMBaseResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("objType", objType);
            dataJson.put("objId", "enterLivePage");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.statitiscIMLogin(getCommandSign(dataJson, "statitiscIMLogin"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 主播需不需要弹xxx结束同台 应对游客同台后切换身份情况
     */
    public void needShowLeavePop(String roomLiveId, String oldUserId, Observer<LiveNeedShowLeavePop> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("roomLiveId", roomLiveId);
            dataJson.put("oldUserId", oldUserId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.needShowLeavePop(getCommandSign(dataJson, "needShowLeavePop"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<LiveNeedShowLeavePop>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(LiveNeedShowLeavePop response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 开播提醒：主播马上开播调用startLive接口返回成功后（调用）
     */
    public void startLivePushRemind(String roomId, String userId, String roomLiveId, Observer<OMBaseResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("roomId", roomId);
            dataJson.put("userId", userId);
            dataJson.put("roomLiveId", roomLiveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.startLivePushRemind(getCommandSign(dataJson, "startLivePushRemind"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 开播提醒：直播间—互动弹窗—开播提醒按钮—发送（调用）
     */
    public void livePushRemind(String roomId, String userId, String roomLiveId, Observer<OMBaseResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("roomId", roomId);
            dataJson.put("userId", userId);
            dataJson.put("roomLiveId", roomLiveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.livePushRemind(getCommandSign(dataJson, "livePushRemind"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取发送邀请函分享参数
     */
    public void generateInvitation(String roomId, String userId, String roomLiveId, Observer<MyInvitationShareMsgResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("roomId", roomId);
            dataJson.put("userId", userId);
            dataJson.put("roomLiveId", roomLiveId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.generateInvitation(getCommandSign(dataJson, "generateInvitation"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MyInvitationShareMsgResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MyInvitationShareMsgResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    public void mNewGoodsDetail(String goodsId, Observer<NewGoodsDetailResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("goodsId", goodsId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mNewGoodsDetail(getCommandSign(dataJson, "mNewGoodsDetail"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<NewGoodsDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(NewGoodsDetailResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 搜索商城商品
     *
     * @param nameTitleLike    名称介绍搜索
     * @param orderByTimeType  1 上架递减 默认 ， 2 上架递增
     * @param orderByPriceType 1 价格递减 2 价格递增
     * @param goodsType        商品类别 1 实物 2 虚拟
     * @param pageNum
     * @param pageType         1 首页， 2  更多
     * @param observer
     */
    public void mSearchGood(String enterCompanyCode, String nameTitleLike, String orderByTimeType, String orderByPriceType, String goodsType, int pageNum, String pageType, Observer<ShopMallListResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("enterCompanyCode", enterCompanyCode);
            dataJson.put("nameTitleLike", nameTitleLike);
            dataJson.put("pageType", pageType);
            dataJson.put("pageNum", String.valueOf(pageNum));
            dataJson.put("pageSize", String.valueOf(AppConfig.PER_PAGE_COUNT));
            if (orderByTimeType != null) {
                String orderByTime = "";
                if ("1".equals(orderByTimeType)) {
                    orderByTime = "desc";
                } else if ("2".equals(orderByTimeType)) {
                    orderByTime = "asc";
                }
                dataJson.put("orderByTimeType", orderByTime);
            }
            if (orderByPriceType != null) {
                String orderByPrice = "";
                if ("1".equals(orderByPriceType)) {
                    orderByPrice = "desc";
                } else if ("2".equals(orderByPriceType)) {
                    orderByPrice = "asc";
                }
                dataJson.put("orderByPriceType", orderByPrice);
            }
            if (goodsType != null) {
                dataJson.put("goodsType", goodsType);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mSearchGood(getCommandSign(dataJson, "mSearchGood"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {
                }).subscribe(new Subscriber<ShopMallListResponse>() {
            @Override
            public void onCompleted() {
                observer.onCompleted();

            }

            @Override
            public void onError(Throwable e) {
                observer.onError(e);

            }

            @Override
            public void onNext(ShopMallListResponse shopMallListResponse) {
                observer.onNext(shopMallListResponse);
            }
        });

    }

    /**
     * 1.2.3版本新增 获取可参会部门和人员
     *
     * @param observer
     */
    public void selectMyDepartMentMemV2(String anchorUserId,String anchorCompanyCode,Observer<MeetingPersonDepartmentResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("anchorUserId", anchorUserId);
            dataJson.put("anchorCompanyCode",anchorCompanyCode);
        }catch (Exception ex){

        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.selectMyDepartMentMemV2(getCommandSign(dataJson, "selectMyDepartMentMemV2"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MeetingPersonDepartmentResponse>() {

                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MeetingPersonDepartmentResponse response) {

                        observer.onNext(response);
                    }
                });
    }

    /**
     * 搜索会议中心列表、获取会议中心列表
     *
     * @param page
     * @param type     1:列表；2:搜索
     * @param search
     * @param observer
     */
    public void mGetMeetingList(int page, int type, String search, Observer<EnterpriseHomeHotLiveListResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            // 接口类型，1:列表；2:搜索
            dataJson.put("type", type);
            dataJson.put("page", page);
            dataJson.put("pageSize", String.valueOf(AppConfig.PER_PAGE_COUNT));
            if (!TextUtils.isEmpty(search)) {
                dataJson.put("search", search);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetMeetingList(getCommandSign(dataJson, "mGetMeetingList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<EnterpriseHomeHotLiveListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(EnterpriseHomeHotLiveListResponse response) {

                        observer.onNext(response);
                    }
                });
    }


    /**
     * 获取定制首页的产品列表
     *
     * @param otherCompanyCode
     * @param observer
     */
    public void getProductList(String otherCompanyCode, Observer<ProductListResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("enterpriseId", otherCompanyCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetEnterprisePageProductList(getCommandSign(dataJson, "getProductList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<ProductListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(ProductListResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 天九云洽会版本配置全局控件名称
     *
     * @param observer
     */
    public void getOMVersionTextOfWidgets(Observer<TextConfigResponse> observer) {
        JSONObject dataJson = new JSONObject();
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetOMVersionTextOfWidgets(getCommandSign(dataJson, "mGetOMVersionTextOfWidgets"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<TextConfigResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(TextConfigResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 获取直播间可发布商品列表
     */
    public void getLiveRoomProductList(String companyCode, String name, int pageNum, Observer<GoodsResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("enterpriseId", companyCode);
            dataJson.put("pageSize", String.valueOf(AppConfig.PER_PAGE_COUNT));
            dataJson.put("pageNum", String.valueOf(pageNum));
            if (!TextUtils.isEmpty(name)) {
                dataJson.put("name", name);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetProductList(getCommandSign(dataJson, "getLiveRoomProductList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<GoodsResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(GoodsResponse response) {
                        observer.onNext(response);
                    }
                });
    }
    /**
     * 获取直播间可发布代播商品列表
     */
    public void getLiveRoomAgentProductList(String companyCode, String name, int pageNum, Observer<GoodsResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("code", companyCode);
            dataJson.put("pageSize", String.valueOf(AppConfig.PER_PAGE_COUNT));
            dataJson.put("pageNum", String.valueOf(pageNum));
            if (!TextUtils.isEmpty(name)) {
                dataJson.put("name", name);
            }
            dataJson.put("liveId",LiveRoomInfoProvider.getInstance().liveId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.mGetAgentGoodsList(getCommandSign(dataJson, "getLiveRoomAgentProductList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<GoodsResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(GoodsResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 会议服务管理产品接口
     *
     * @param observer
     */
    public void queryManagemenProduct(Observer<RechargeProductResponse> observer) {

        JSONObject dataJson = new JSONObject();
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.queryManagemenProduct(getCommandSign(dataJson, "queryManagemenProduct"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<RechargeProductResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(RechargeProductResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 会议服务管理剩余分钟数接口
     *
     * @param observer
     */
    public void queryRechargePlatCorporationRemaining(String corporationCode, Observer<RemainTimeResponse> observer) {


        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("corporationCode", corporationCode);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.queryPlatCorporationRemaining(getCommandSign(dataJson, "queryPlatCorporationRemaining"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<RemainTimeResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(RemainTimeResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 会议服务管理：充值下单接口
     *
     * @param serviceTime 10000
     * @param orderPrice  2000
     * @param feeName     测3434343434
     * @param feePrice    0.2
     * @param feeCode     Live
     * @param observer
     */
    public void queryRechargesaveorder(String serviceTime, String orderPrice
            , String feeName, String feePrice, String feeCode, Observer<RechargeSaveOrderResponse> observer) {


        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("serviceTime", serviceTime);
            dataJson.put("orderPrice", orderPrice);
            dataJson.put("feeName", feeName);
            dataJson.put("feePrice", feePrice);
            dataJson.put("feeCode", feeCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.rechargesaveorder(getCommandSign(dataJson, "saveorder"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<RechargeSaveOrderResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(RechargeSaveOrderResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 会议服务管理：充值取消订单接口
     *
     * @param id
     * @param observer
     */
    public void queryRechargeCancelOrder(String id, Observer<RechargeProductResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("id", id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.rechargeCancelOrder(getCommandSign(dataJson, "cancelOrder"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<RechargeProductResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(RechargeProductResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 我的订单（购买记录）
     *
     * @param pageSize
     * @param pageNum
     * @param orderStatus
     * @param corporationCode
     * @param observer
     */
    public void queryOrderList(String pageSize, String pageNum, String orderStatus, String corporationCode,
                               Observer<OrderModel> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("pageSize", pageSize);
            dataJson.put("pageNum", pageNum);
            dataJson.put("orderStatus", orderStatus);
            dataJson.put("corporationCode", corporationCode);
//            dataJson.put("customerId", customerId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.orderList(getCommandSign(dataJson, "queryOrderList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OrderModel>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OrderModel response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 取消订单
     */
    public void cancelOrder(String id, Observer<RechargeCancelOrderResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("id", id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.cancelOrder(getCommandSign(dataJson, "cancelOrder"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<RechargeCancelOrderResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(RechargeCancelOrderResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 使用明细
     *
     * @param pageNum
     * @param pageSize
     * @param observer
     */
    public void useData(String pageNum, String pageSize, Observer<UseDataResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", pageSize);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.useData(getCommandSign(dataJson, "useData"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<UseDataResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(UseDataResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 首页数据
     *
     * @param observer
     */
    public void getMainPageInfo( Observer<MeetingHomeModelResponse> observer) {

        JSONObject dataJson = new JSONObject();

        AppConfig.isAppendHeaderAuthorization = true;
        apiService.getMainPageInfo(getCommandSign(dataJson, "getMainPageInfo"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MeetingHomeModelResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MeetingHomeModelResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 热门企业–更多
     *
     * @param pageNum
     * @param pageSize
     * @param observer
     */
    public void getHotCompanyMoreList(String companyName,String pageNum, String pageSize, Observer<BrowsedEnterpriseResponse> observer) {

        JSONObject dataJson = new JSONObject();
        try {
            if (!TextUtils.isEmpty(companyName)) {
                dataJson.put("companyName", companyName);


            }
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", String.valueOf(pageSize));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.getHotCompanyMoreList(getCommandSign(dataJson, "getCompanyList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<BrowsedEnterpriseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(BrowsedEnterpriseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 获取直播间可发布活动二维码列表
     */
    public void selectMyCompanyPromotion(String liveId,int pageNum,Observer<PublishQRListResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId",liveId);
//            dataJson.put("enterpriseId", companyCode);
            dataJson.put("pageSize", String.valueOf(AppConfig.PER_PAGE_COUNT));
            dataJson.put("pageNum", String.valueOf(pageNum));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.selectMyCompanyPromotion(getCommandSign(dataJson, "selectMyCompanyPromotion"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<PublishQRListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(PublishQRListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 发布/隐藏活动
     * @param id        // 活动id
     * @param imRoomId  // imRoomId
     * @param liveId    // liveId
     * @param status    // 1：发布 2：隐藏
     * @param observer
     */
    public void pushPromotion(String id,String imRoomId,String liveId,String status,Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("id", id);
            dataJson.put("imRoomId",imRoomId);
            dataJson.put("liveId",liveId);
            dataJson.put("status",status);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.pushPromotion(getCommandSign(dataJson, "pushPromotion"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 进入直播间初始化时获取已发布且正在显示的活动列表
     */
    public void selectLivePromotion(String liveId,Observer<PublishQRListResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("liveId", liveId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.selectLivePromotion(getCommandSign(dataJson, "selectLivePromotion"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<PublishQRListResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(PublishQRListResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 点击进入小程序:统计
     */
    public void selectPromotion(String id,String liveId,Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("id", id);
            dataJson.put("liveId", liveId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.selectPromotion(getCommandSign(dataJson, "selectPromotion"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 新增：首页点击直播招商-开播的拦截接口（若由企业 && 有开播权限 && 有obs权限，进行判断是否有此用户正在开播的官方大会）
     */
    public void queryLiveStatus(Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.queryLiveStatus(getCommandSign(dataJson, "queryLiveStatus"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                        BaseUserInfoCache.saveDataSign(mContext);
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 1.3.1 分销客功能版本新增接口
     */

    /**
     * 申请成为分销客接口
     * @param observer
     */
    public void queryApplyForDistributor(Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.applyForDistributor(getCommandSign(dataJson, "queryApplyForDistributor"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 分销客我的会员列表
     * @param observer
     */
    public void queryDistributionMyMemberList(int pageNum,Observer<MineMemberDistributionResponse> observer) {
        JSONObject dataJson = new JSONObject();

        try {

            dataJson.put("pageSize", String.valueOf(AppConfig.PER_PAGE_COUNT));
            dataJson.put("pageNum", String.valueOf(pageNum));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.queryDistributionMyMemberList(getCommandSign(dataJson, "queryDistributionMyMemberList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MineMemberDistributionResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MineMemberDistributionResponse response) {
                        observer.onNext(response);
                    }
                });
    }
    /**
     * 分销客我的团队列表
     * @param observer
     */
    public void queryDistributionMyTeamList(int pageNum,Observer<MineMemberDistributionResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {

            dataJson.put("pageSize", String.valueOf(AppConfig.PER_PAGE_COUNT));
            dataJson.put("pageNum", String.valueOf(pageNum));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.queryDistributionMyTeamList(getCommandSign(dataJson, "queryDistributionMyTeamList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MineMemberDistributionResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MineMemberDistributionResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 二级分销客团队人数列表
     * @param observer
     */
    public void queryTwoLevelDistributionMyTeamList(int pageNum,String distributorId,Observer<MineMemberDistributionResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("pageSize", String.valueOf(AppConfig.PER_PAGE_COUNT));
            dataJson.put("pageNum", String.valueOf(pageNum));
            dataJson.put("distributorId",distributorId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.queryTwoLevelDistributionMyTeamList(getCommandSign(dataJson, "queryDistributionMyTeamList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MineMemberDistributionResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MineMemberDistributionResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 分销客我的收益列表
     * @param observer
     */
    public void queryDistributionCommissionList(String pageNum,String pageSize,String status,Observer<MineProfitListDistributionResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", pageSize);
            //dataJson.put("status", status);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.queryDistributionCommissionList(getCommandSign(dataJson, "queryDistributionMyTeamList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MineProfitListDistributionResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MineProfitListDistributionResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 分销客我的商品收益列表
     * @param observer
     */
    public void queryDistributionGoodsdetailList(String pageNum,String pageSize,Observer<MineProfitListDistributionResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", pageSize);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.queryDistributionGoodsdetailList(getCommandSign(dataJson, "queryDistributionGoodsdetailList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MineProfitListDistributionResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MineProfitListDistributionResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 分销客 提现记录列表接口
     * @param observer
     */
    public void querytDistributionWithdrawApplpList(String pageNum,String pageSize,Observer<MineDistributionApplyRecordResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", pageSize);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.querytDistributionWithdrawApplpList(getCommandSign(dataJson, "queryDistributionMyTeamList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MineDistributionApplyRecordResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MineDistributionApplyRecordResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 分销客我的收益数据
     * @param observer
     */
    public void queryDistributionMyProfit(Observer<MineProfitInfoDistributionResponse> observer) {
        JSONObject dataJson = new JSONObject();
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.queryDistributionMyProfit(getCommandSign(dataJson, "queryDistributionMyProfit"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MineProfitInfoDistributionResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MineProfitInfoDistributionResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 我的页面--（我的收益、我的会员总体数据展示）
     * @param observer
     */
    public void getDistributionMemberInfo(Observer<MineDistributionResponse> observer) {
        JSONObject dataJson = new JSONObject();
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.getDistributionMemberInfo(getCommandSign(dataJson, "getDistributionMemberInfo"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MineDistributionResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MineDistributionResponse response) {
                        observer.onNext(response);
                    }
                });
    }
    /**
     * 我的收益--（提现）
     * @param amount 金额
     * @param observer
     */
    public void querytDistributionWithdrawApply(String amount ,String withdrawalAccountNo,
                                                String withdrawalChannel,
                                                String withdrawalName,Observer<MineDistributionWithdarwalApplyResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("amount", amount);
            dataJson.put("withdrawalAccountNo", withdrawalAccountNo);
            dataJson.put("withdrawalChannel", withdrawalChannel);
            dataJson.put("withdrawalName", withdrawalName);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.querytDistributionWithdrawApply(getCommandSign(dataJson, "querytDistributionWithdrawApply"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MineDistributionWithdarwalApplyResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MineDistributionWithdarwalApplyResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 我的收益--（提现）
     * @param id 金额
     * @param observer
     */
    public void querytDistributionWithdrawApplyDetail(String id ,Observer<MineDistributionWithdarwalDetailResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("id", id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.querytDistributionWithdrawApplyDetail(getCommandSign(dataJson, "querytDistributionWithdrawApplyDetail"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MineDistributionWithdarwalDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MineDistributionWithdarwalDetailResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 我的收益-- 提交签约信息
     * @param observer
     */
    public void querytDistributionWithdrawSign(String cardNo,String idCard,
                                               String mobile,String name,Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("cardNo",cardNo);
            dataJson.put("idCard", idCard);
            dataJson.put("mobile", mobile);
            dataJson.put("name", name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.querytDistributionWithdrawSign(getCommandSign(dataJson, "querytDistributionWithdrawSign"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 分销客 -- 签约状态信息
     * @param observer
     */
    public void querytDistributionWithdrawSignStatus(Observer<MineDistributionSignStatusResponse> observer) {
        JSONObject dataJson = new JSONObject();

        AppConfig.isAppendHeaderAuthorization = true;
        apiService.querytDistributionWithdrawSignStatus(getCommandSign(dataJson, "querytDistributionWithdrawSignStatus"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {

                })
                .subscribe(new Subscriber<MineDistributionSignStatusResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(MineDistributionSignStatusResponse response) {
                        observer.onNext(response);
                    }
                });
    }


    /**
     * 我的会议-流量使用情况
     *
     * @param observer
     */
    public void inviteMeRecordList(String pageNum, Observer<MeetingInviteNoticeResponse> observer) {
        JSONObject dataJson = new JSONObject();

        try {
            dataJson.put("pageNum", pageNum);
            dataJson.put("pageSize", AppConfig.PER_PAGE_COUNT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.inviteMeRecordList(getCommandSign(dataJson, "inviteMeRecordList"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {
                }).subscribe(new Subscriber<MeetingInviteNoticeResponse>() {
            @Override
            public void onCompleted() {
                observer.onCompleted();

            }

            @Override
            public void onError(Throwable e) {
                observer.onError(e);

            }

            @Override
            public void onNext(MeetingInviteNoticeResponse response) {
                observer.onNext(response);
            }
        });
    }


    /**
     * 提现账户绑定状态
     *
     * @param observer
     */
    public void queryAliAccountBindingStatus(Observer<MineWithdrawalAccountBindResponse> observer) {
        JSONObject dataJson = new JSONObject();
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.queryAliAccountBindingStatus(getCommandSign(dataJson, "queryAliAccountBindingStatus"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {
                }).subscribe(new Subscriber<MineWithdrawalAccountBindResponse>() {
            @Override
            public void onCompleted() {
                observer.onCompleted();

            }

            @Override
            public void onError(Throwable e) {
                observer.onError(e);

            }

            @Override
            public void onNext(MineWithdrawalAccountBindResponse response) {
                observer.onNext(response);
            }
        });
    }

    /**
     * 提现账户绑定
     *
     * @param authCode
     */
    public void queryAliAccountBinding(String authCode, Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("authCode", authCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.queryAliAccountBinding(getCommandSign(dataJson, "queryAliAccountBinding"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {
                }).subscribe(new Subscriber<OMBaseResponse>() {
            @Override
            public void onCompleted() {
                observer.onCompleted();

            }

            @Override
            public void onError(Throwable e) {
                observer.onError(e);

            }

            @Override
            public void onNext(OMBaseResponse response) {
                observer.onNext(response);
            }
        });
    }


    /**
     * 提现账户解除绑定
     *
     */
    public void queryAliAccountUnBinding(String id,Observer<OMBaseResponse> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.queryAliAccountUnBinding(getCommandSign(dataJson, "queryAliAccountUnBinding"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {
                }).subscribe(new Subscriber<OMBaseResponse>() {
            @Override
            public void onCompleted() {
                observer.onCompleted();

            }

            @Override
            public void onError(Throwable e) {
                observer.onError(e);

            }

            @Override
            public void onNext(OMBaseResponse response) {
                observer.onNext(response);
            }
        });
    }
    /**
     * 企业是否有代播商品权限接口
     *
     */
    public void queryCompanyAgentPower(String companyCode,Observer<AgentLiveModel> observer) {
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("companyCode", companyCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConfig.isAppendHeaderAuthorization = true;
        apiService.queryCompanyAgentPower(getCommandSign(dataJson, "queryCompanyAgentPower"), getCommandRequestBody(dataJson))
                .compose(RxUtils.rxSchedulerHelper())
                .doOnError(throwable -> {
                }).subscribe(new Subscriber<AgentLiveModel>() {
            @Override
            public void onCompleted() {
                observer.onCompleted();

            }

            @Override
            public void onError(Throwable e) {
                observer.onError(e);

            }

            @Override
            public void onNext(AgentLiveModel response) {
                observer.onNext(response);
            }
        });
    }


}
