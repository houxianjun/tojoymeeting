package com.tojoy.tjoybaselib.net;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.util.net.CompressUtil;
import com.tojoy.tjoybaselib.util.string.HmacSHA1Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class BaseApiProvider {

    @SuppressLint("StaticFieldLeak")
    protected static Context mContext = null;

    /**
     * //////////////////////////////
     * 构造基础请求参数
     * //////////////////////////////
     */
    private String getRequestStr(JSONObject commandInfo) {
        JSONObject requestJson = new JSONObject();

        try {
            requestJson.put("appInfo", BaseParamterProvider.getInstance(mContext).getAppInfo(mContext));
            requestJson.put("userProperty", BaseParamterProvider.getInstance(mContext).getUserProperty(mContext));
            requestJson.put("commandInfo", commandInfo);
            requestJson.put("deviceInfo", BaseParamterProvider.getInstance(mContext).getDeviceInfoJSON());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return requestJson.toString();
    }


    /**
     * //////////////////////////////
     * 获取经过拼装、压缩、加密后的请求体
     * //////////////////////////////
     */
    RequestBody getApiRequestBody(JSONObject commandInfoJson) {
        Log.d("OkHttp--RequestJSON", getRequestStr(commandInfoJson));
        return RequestBody.create(MediaType.parse("application/json"), CompressUtil.compressAndEcrypt(getRequestStr(commandInfoJson)));
    }

    /**
     * //////////////////////////////
     * 具体的请求接口
     * //////////////////////////////
     */
    JSONObject getCommandInfoJSON(JSONObject dataJson, String commandName) {
        JSONObject commandInfoJson = new JSONObject();

        try {
            commandInfoJson.put("data", dataJson);
            commandInfoJson.put("commandName", commandName);
            // 新增H5来源定位
            commandInfoJson.put("path", BaseUserInfoCache.getH5SpecialSource(mContext));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return commandInfoJson;
    }


    /**
     * 云洽会独立APP 新增
     */
    protected RequestBody getCommandRequestBody(JSONObject dataJson) {

        Log.d("OM-OkHttp-RequestJSON-", dataJson.toString());
        return RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), dataJson.toString());
    }


    protected String getCommandSign(JSONObject dataJson, String commandName) {
        if (dataJson != null) {
            try {
                dataJson.put("commandName", commandName);

                JSONArray names = dataJson.names();
                if (null != names) {
                    int length = names.length();
                    for (int i = length - 1; i >= 0; i--) {
                        String key = (String) names.get(i);
                        String value = dataJson.get(key).toString();
                        if (TextUtils.isEmpty(value)) {
                            dataJson.remove(key);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        String sign = HmacSHA1Util.buildSign(dataJson, HmacSHA1Util.KEY);
        BaseUserInfoCache.saveSign(sign, mContext);
        return sign;
    }

}
