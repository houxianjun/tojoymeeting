package com.tojoy.tjoybaselib.model.user;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * Created by chengyanfang on 2017/10/10.
 */

public class IMTokenResponse extends OMBaseResponse {
    public IMToken data;

    public class IMToken {
        public String token;
        public String env;
    }
}
