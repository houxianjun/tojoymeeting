package com.tojoy.tjoybaselib.model.home;

import java.util.ArrayList;

/**
 * 首页热门企业数据模型
 */
public class HomeModelResponse {

    /** 广告位标题 */
    public String spaceName;
    /** 描述 */
    public String des;
    public ArrayList<HotEnterPriseModel> dataList;

    public static class HotEnterPriseModel{
        /** 企业编码 */
        public String companyCode;
        /** 企业名称 */
        public String companyName;
        /** logo地址 */
        public String logoUrl;
        /** 名称 */
        public String adName;
        /** 跳转类型0:无;1:H5;2:APP */
        public String jumpType;
        /** 频道内容(跳转链接) */
        public String channelValue;
        /** 图片地址 */
        public String  h5Url;

        public String bannerUrl;

        public String appChannel;
    }
}
