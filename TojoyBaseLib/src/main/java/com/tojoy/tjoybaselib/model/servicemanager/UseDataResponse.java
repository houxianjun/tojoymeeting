package com.tojoy.tjoybaselib.model.servicemanager;

import android.text.TextUtils;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

public class UseDataResponse extends OMBaseResponse {

    public DataBean data;

    public class DataBean {
        
        public List<ListBean> list;

        public  class ListBean {
            public long deductionFeeTime;
            public String feeName;
            public long meetingEndTime;
            public String meetingId;
            public long meetingStartTime;
            public String totalTime;

            public String getTotalTime() {
                if(TextUtils.isEmpty(totalTime)) {
                    return "0";
                }
                if(!totalTime.contains(".")) {
                    return totalTime;
                }

                if (totalTime.endsWith("0")) {
                    totalTime = totalTime.substring(0, totalTime.length()-1);
                }
                if (totalTime.endsWith("0")) {
                    totalTime = totalTime.substring(0, totalTime.length()-1);
                }
                if (totalTime.endsWith(".")) {
                    totalTime = totalTime.substring(0, totalTime.length()-1);
                }
                if(TextUtils.isEmpty(totalTime)) {
                    return "0";
                }
                return totalTime;
            }
        }
    }
}
