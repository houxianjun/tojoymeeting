package com.tojoy.tjoybaselib.model.home;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

public class MineProfitListDistributionResponse extends OMBaseResponse {
    public MyProfitInfoModel data;
    public static class MyProfitInfoModel {

        public ArrayList<MineProfitListDistributionResponse.MyProfitRecordsModel> records;
        public int total;
        public int size;
        public int current;
    }

    public static class MyProfitRecordsModel {
        //1 已结算 0 待结算
        public int status;
        public String name;
        //参会会员
        public String trueName;
        //结算时间
        public String updateDate;
        //待结算文案
        public String withdrawalTips;
        //会议Id
        public String roomCode;
        //佣金金额
        public String commissionStr;
        //商品Id
        public String goodsId;
        //商品编码
        public String code;
    }
}
