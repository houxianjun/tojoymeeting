package com.tojoy.tjoybaselib.model.home;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * Created by chengyanfang on 2017/12/12.
 */

public class ScreenAdvertisResponse extends OMBaseResponse {

    public ScreenAdvertisImageInfo data;

    public class ScreenAdvertisImageInfo {
        public String imgUrl;
        public String adLinkKey;
        public String adLinkType;
        public String adLinkUrl;
        public String adLinkId;//关联会议id 或关联公司主页的公司Id
        public String adLinkLiveId;//直播间id
        public String isPw;//是否需要密码  1是0否
        public String adName;
        public String adTimeEnd;
        public String id;
    }
}


