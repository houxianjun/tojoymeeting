package com.tojoy.tjoybaselib.model.OnlineMeeting;

/**
 * @author qll
 * @date 2019/4/23
 */
public class CheckUserInfo {
    public int level;
    public String mobile;
    public String name;
}
