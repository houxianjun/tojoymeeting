package com.tojoy.tjoybaselib.model.live;

import android.text.TextUtils;

/**
 * @author qll
 * @date 2019/4/17
 * 项目标签(由于首页、开会申请返回的字段统一，提供方法判断后返回)
 */
public class ProjectTagModel {
    // 开会申请返回的
    public String projectTagId;
    public String projectTagName;
    public String tagName;
    public String id;
    public String roomLiveId;
    public String createDate;

    public ProjectTagModel() {
    }

    public String getTagName(){
        if (!TextUtils.isEmpty(projectTagName)) {
            return projectTagName;
        } else if (!TextUtils.isEmpty(tagName)){
            return tagName;
        } else {
            return "";
        }
    }
}
