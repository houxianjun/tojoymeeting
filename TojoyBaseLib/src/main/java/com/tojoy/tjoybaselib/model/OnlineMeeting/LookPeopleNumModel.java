package com.tojoy.tjoybaselib.model.OnlineMeeting;

import java.io.Serializable;

/**
 * @author qll
 * @date 2019/4/19
 * 设置参会人数
 */
public class LookPeopleNumModel implements Serializable {
    public String personNumber;
    public String personNumberId;
}
