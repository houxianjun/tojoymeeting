package com.tojoy.tjoybaselib.model.pay;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

public class CheckDeviceResponse extends OMBaseResponse {
    public DeviceModel data;

    public class DeviceModel {
        public String deviceId;
        public String token;
    }
}
