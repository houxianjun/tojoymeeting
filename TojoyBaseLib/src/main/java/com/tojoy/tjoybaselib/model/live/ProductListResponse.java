package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

/**
 * @author fanxi
 * @date 2020-03-02.
 * description：产品推荐的实体类
 */
public class ProductListResponse extends OMBaseResponse {
    public List<ProductDataBean> data;
    public static class ProductDataBean {
        /** 实际价格 */
        public String salePrice;
        /** 图片地址 */
        public String imgurl;
        /** 直播间图片 */
        public String liveRoomCoverImgUrl;
        /** 产品id */
        public String id;
        /** 产品名称 */
        public String name;
        /** 产品副标题 */
        public String subhead;
        /** 访问次数 */
        public String visitNum;

        /** 是否参与推广 1参与，0不参与 */
        public int ifDistribution;
        /** 本场次会议最大分销额预算 */
        public String maxBudget;
        /** 一级分销员佣金 */
        public String level1Commission;
        /** 二级分销员佣金 */
        public String level2Commission;
        /** 一级员工分销额佣金 */
        public String level1CommissionEmployee;
        /** 二级员工分销额佣金 */
        public String level2CommissionEmployee;
        /** 最少参会人数 */
        public String minPerson;
        /** 最低时长 */
        public String minViewTime;
        /** 此企业的公司码 */
        public String companyCode;
    }
}
