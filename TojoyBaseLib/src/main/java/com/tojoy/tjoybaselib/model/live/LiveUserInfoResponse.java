package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.model.user.UserInfoModel;
import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * Created by luuzhu on 2019/4/22.
 */

public class LiveUserInfoResponse extends OMBaseResponse {

    public UserInfoModel data;
}
