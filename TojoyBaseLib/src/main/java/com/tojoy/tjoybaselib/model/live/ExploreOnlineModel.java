package com.tojoy.tjoybaselib.model.live;

public class ExploreOnlineModel {
    public String userId;
    public String userAvatar;
    public String userName;
    public String company;
    public String job;
    public String userType;
    public int selectedSratus;
}
