package com.tojoy.tjoybaselib.model.servicemanager;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

public class RechargeCancelOrderResponse extends OMBaseResponse {
    String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
