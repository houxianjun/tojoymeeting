package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;


public class OutlinkListResponse extends OMBaseResponse {

    public OutlinkListData data;

    public class OutlinkListData {
        public ArrayList<OnlineMeetingHomeGridModel> list = new ArrayList<>();
    }

}

