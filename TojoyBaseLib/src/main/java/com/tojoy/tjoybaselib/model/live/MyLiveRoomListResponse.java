package com.tojoy.tjoybaselib.model.live;

import android.content.Context;
import android.text.SpannableString;

import com.tojoy.tjoybaselib.R;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.util.string.StringUtil;
import com.tojoy.tjoybaselib.util.sys.MathematicalUtils;

import java.util.List;

/**
 * Created by luuzhu on 2019/4/17.
 * 我的房间实体类
 */

public class MyLiveRoomListResponse extends OMBaseResponse {

    public DataObjBean data;

    public static class DataObjBean {
        public int size;
        public int total;
        public List<ListBean> list;

        public static class ListBean {
            public String coverOne,coverTwo;
            /** 直播标题 */
            public String title;
            public String userId;
            /** 标签 */
            public String tagNames;
            public String roomId;

            /** 替换roomId */
            public String roomCode;
            /** 类型，腾讯:tencent */
            public String type;
            /** 聊天室id */
            public String imRoomId;
            public String password;
            /** 直播-观看数 */
            public String liveWatchTotal;
            public String roomLiveId;
            public long startDate;
            /** 直播状态，1:未开始；2:直播中；3:暂停；4：回放状态 */
            public String status;
            /** 回放状态url ： 直播推流地址 */
            public String videoPullUrl;
            public String companyCode;
            public String companyName;
            public String getRoomIdText() {
                return "ID" + roomCode;
            }

            /** 是否可以分销：0：不可以；1：可以 */
            public int ifDistribution;
            /** 最大分销金额 */
            public String maxBudget;
            /** 一级分销员佣金 */
            public String level1Commission;
            /** 二级分销员佣金 */
            public String level2Commission;
            /** 一级员工分销额佣金 */
            public String level1CommissionEmployee;
            /** 二级员工分销额佣金 */
            public String level2CommissionEmployee;
            /** 最少参会人数 */
            public String minPerson;
            /** 最低时长 */
            public String minViewTime;
        }
    }
}
