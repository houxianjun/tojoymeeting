package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

public class AgentLiveModel extends OMBaseResponse {
    public AgentLiveBean data;
    public class AgentLiveBean {
        //是否开通商品代播 1 是 0 否
        public int agentLive;
    }

}
