package com.tojoy.tjoybaselib.model.enterprise;

import com.tojoy.tjoybaselib.model.intentionalorder.makesure.CommdityModel;
import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * @author qll
 * @date 2019/9/18
 */
public class ProductDetailResponse extends OMBaseResponse {
    public CommdityModel data;
}
