package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * 用户信息
 * Created by ${daibin}
 * on 2019/6/19.
 * function
 */

public class OnlineUserInfoResponse extends OMBaseResponse {
    public HomePageModel data;

    public class HomePageModel {
        public String trueName;
    }
}
