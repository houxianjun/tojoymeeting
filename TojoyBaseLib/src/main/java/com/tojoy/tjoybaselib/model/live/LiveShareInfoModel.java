package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * Created by luuzhu on 2019/4/28.
 */

public class LiveShareInfoModel extends OMBaseResponse {

    public DataObjBean data;

    public class DataObjBean {

        public ShareInfoBean shareInfo;
        public LiveInfo liveInfo;
        public WXMiniProgramInfo weChaAppShareLinkDto;
        public class ShareInfoBean {
            public String shareDesc;
            public String shareImg;
            public String shareUrl;
        }

        public class LiveInfo{
            public String title;
            public String userId;
        }

        public class WXMiniProgramInfo{
            public String path;
            public String userName;
        }
    }
}
