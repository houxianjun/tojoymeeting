package com.tojoy.tjoybaselib.model.intentionalorder.makesure;


public class AddressDetailModel{
    public String pkId;

    public String userId;

    public String provinceName;

    public String cityName;

    public String districtName;

    public String userName;

    public String tag;

    public String userMobile;

    public String remark;

}