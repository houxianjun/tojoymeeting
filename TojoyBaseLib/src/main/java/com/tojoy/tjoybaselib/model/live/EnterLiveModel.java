package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * Created by luuzhu on 2019/4/24.
 */

public class EnterLiveModel extends OMBaseResponse {

    public DataObjBean data;

    public class DataObjBean {
        public long curTime;
        /**
         * 1:未开始；2:直播中；3:暂停；4:结束
         */
        public String liveStatus;
        /**
         * 如果是暂离状态，主播选择的展示消息
         */
        public String pauseMsg;

        public LiveParams liveParam;

        /**
         * 是否支持回放 1:支持 0 不支持
         */
        public String isReplay;

        /**
         * 开播权限：1:有；0:无
         */
        public String isApply;

        /**
         * 客服电话
         */
        public String phone;

        /**
         * 主播间的公司号
         */
        public String companyCode;

        /**
         * 申请ID （数据统计二期增加）
         */
        public String applyId;

    }


    public class LiveParams {
        /**
         * 官方大会的拉流地址
         */
        public String pullFlvUrl;

        /**
         * 回放地址
         */
        public String videoPullUrl;

        /**
         * SDk新增
         */
        public String roomCode;
    }
}
