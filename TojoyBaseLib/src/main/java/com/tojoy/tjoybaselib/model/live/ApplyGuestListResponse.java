package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

/**
 * @author qll
 * @date 2019/5/3
 * 申请同台人员列表
 */
public class ApplyGuestListResponse extends OMBaseResponse{
    public AppLyGuestListModel data;

    public static class AppLyGuestListModel{
        public boolean isLastPage;
        public int size;
        public int total;
        public List<ApplyGuestInfo> list;
    }
}
