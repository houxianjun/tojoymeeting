package com.tojoy.tjoybaselib.model.home;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

public class MineMemberDistributionResponse extends OMBaseResponse {

    public  MyMemberInfoModel  data;

    public static class MyMemberInfoModel {

        public ArrayList<MyMemberRecordsModel> records;
        public int total;
        public int size;
        public int current;

    }

    public static class MyMemberRecordsModel {
        public String trueName;
        public String bindDateTime;
        public String avatar;
        public String currentDayPrice;
        public String totalPrice;
        public String userId;
        public String level;
        public String count;
        public String distributorId;
    }
}
