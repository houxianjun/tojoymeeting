package com.tojoy.tjoybaselib.model.user;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

/**
 * Created by luuzhu on 2019/9/9.
 */

public class LoginResponse extends OMBaseResponse {

    public DataBean data;

    public class DataBean {
        public String userId;
        public String mobile;
        public String trueName;
        public String avatar;
        public String companyName;
        public String roomId;
        //在修改SDk接口前就有这个字段
        public String roomCode;
        public String companyCode;
        public String job;

        // (1:超级管理员 2:管理员 3:财务)
        public String userBackRole;
        public String userFrontRole;

        // 0:没有权限;1:有权限
        public String checkAuth;
        public String token;
        public String token_type;
        public String access_token;
        public String license;
        public String user_id;
        public String username;

        //修改1.2.4权限版本新增
        public ArrayList<String> permissions;
        public ArrayList<String> roles;

        /**
         * 是否推广员：0-否；1-是
         */
        public int ifDistributor;
        /**
         * 推广员级别
         */
        public int distributorLevel;
    }
}
