package com.tojoy.tjoybaselib.model.home;

import android.text.TextUtils;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * @author qll
 * @date 2020/7/11
 */
public class MineDistributionResponse extends OMBaseResponse {
    public MineDistributionModel data;

    public static class MineDistributionModel{
        /**
         * 所有人数
         */
        public String allMembers;
        /**
         * 今日新增
         */
        public String todayIncrease;
        /**
         * 团队人数
         */
        public String teamMembers;
        /**
         * 专属邀请码
         */
        public String inviteUrl;
        /**
         * 本月收益
         */
        public String monthIncome;
        /**
         * 今日收益
         */
        public String todayIncome;
        /**
         * 累计收益
         */
        public String totalIncome;

        /**
         * 分销员等级
         */
        public int level;

        public String getAllMembers() {
            return TextUtils.isEmpty(allMembers) ? "0" : allMembers;
        }


        public String getTodayIncrease() {
            return TextUtils.isEmpty(todayIncrease) ? "0" : todayIncrease;
        }


        public String getTeamMembers() {
            return TextUtils.isEmpty(teamMembers) ? "0" : teamMembers;
        }


        public String getMonthIncome() {
            return TextUtils.isEmpty(monthIncome) ? "0" : monthIncome;
        }


        public String getTodayIncome() {
            return TextUtils.isEmpty(todayIncome) ? "0" :todayIncome;
        }


        public String getTotalIncome() {
            return TextUtils.isEmpty(totalIncome) ? "0" :totalIncome;
        }

        /**
         * 团队人数
         */
        public String teamTotal;
        /**
         * 成员人数
         */
        public String memberTotal;

        //个税费率
        public String rate;
        //公司员工是否共享
        public int isCompanyMemberShare;

    }
}
