package com.tojoy.tjoybaselib.model.home;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * @author qll
 * @date 2019/9/24
 */
public class HomeReadCountResponse extends OMBaseResponse {
    public HomeReadCountModel data;

    public class HomeReadCountModel{
        public int count;
    }
}
