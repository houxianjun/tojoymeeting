package com.tojoy.tjoybaselib.model.home;

public class MeetingInviteModel {
    public String createDate;
    public String messages;
    public String roomId;
    public String roomLiveId;
    public String status;
    public String userName;
}
