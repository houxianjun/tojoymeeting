package com.tojoy.tjoybaselib.model.meetingperson;

import java.io.Serializable;

/**
 * 参会人接口相关数据
 */
public class AttendeePersonModel implements Serializable {


    private String outsideMobils = "";
    private String outsideNames = "";

    //组织架构内部人员信息
    private String inuserIds = "";
    private String inmobiles = "";
    private String innameIds = "";

    public String getOutsideMobils() {
        if (outsideMobils == null) {
            outsideMobils = "";
        }
        return outsideMobils;
    }

    public void setOutsideMobils(String outsideMobils) {
        this.outsideMobils = outsideMobils;
    }

    public String getOutsideNames() {
        if (outsideNames == null) {
            outsideNames = "";
        }
        return outsideNames;
    }

    public void setOutsideNames(String outsideNames) {
        this.outsideNames = outsideNames;
    }

    public String getInuserIds() {
        if (inuserIds == null) {
            inuserIds = "";
        }
        return inuserIds;
    }

    public void setInuserIds(String inuserIds) {
        this.inuserIds = inuserIds;
    }

    public String getInmobiles() {
        if (inmobiles == null) {
            inmobiles = "";
        }
        return inmobiles;
    }

    public void setInmobiles(String inmobiles) {
        this.inmobiles = inmobiles;
    }

    public String getInnameIds() {

        if (innameIds == null) {
            innameIds = "";
        }
        return innameIds;
    }

    public void setInnameIds(String innameIds) {
        this.innameIds = innameIds;
    }

}
