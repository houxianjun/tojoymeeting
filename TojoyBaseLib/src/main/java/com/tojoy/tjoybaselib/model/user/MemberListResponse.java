package com.tojoy.tjoybaselib.model.user;

import android.text.TextUtils;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

/**
 * Created by luuzhu on 2019/4/22.
 * 在线用户列表
 */
public class MemberListResponse extends OMBaseResponse {
    public DataObjBean data;

    public class DataObjBean {
        public String size;
        public String total;
        /** 暂时代表直播间是否有助手，true：有；false：没有 */
        public boolean hasNextPage;
        public List<ListBean> list;

        public class ListBean {
            public String company;
            public long createDate;
            public String mobile;
            public String userAvatar;
            public String userId;
            public String userName;
            /** 用户类型，0:主播；1:助手；2：成员 */
            public String userType;
            /** 0代表能邀请 */
            public String invite;
            /** 0未注册 1已经注册 */
            public String isRegister;
            /** 1在直播间 2不在直播间 */
            public String online;

            public String getName() {
                return TextUtils.isEmpty(userName) ? "暂无" : userName;
            }

            public String getCompany() {
                return TextUtils.isEmpty(company) ? "暂无" : company;
            }
            /** SDk新增 */
            public String roomCode;

            public String fromWechat;
            public String getFromwechat() {
                return TextUtils.isEmpty(fromWechat) ? "0" : fromWechat;
            }

            public String fromApp;
            public String getFromapp() {
                return TextUtils.isEmpty(fromApp) ? "0" : fromApp;
            }

        }
    }
}
