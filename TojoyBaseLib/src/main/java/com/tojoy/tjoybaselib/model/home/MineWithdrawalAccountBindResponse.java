package com.tojoy.tjoybaselib.model.home;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * 提现账户绑定状态
 */
public class MineWithdrawalAccountBindResponse extends OMBaseResponse {

    public AccountBind data;

    public static class AccountBind{

        /**
         * 用户UserID
         */
        public String aliUserId;
        /**
         *
         */
        public String id;
        /**
         * 昵称
         */
        public String nickName;
        /**
         * 绑定状态
         */
        public int status;

        public boolean isBinding(){
            return status==1?true:false;
        }
    }

}
