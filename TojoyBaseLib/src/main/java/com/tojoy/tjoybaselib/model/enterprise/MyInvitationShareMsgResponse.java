package com.tojoy.tjoybaselib.model.enterprise;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * @author qll
 * @date 2019/12/2
 * 开播申请结果页面--我的邀请涵
 */
public class MyInvitationShareMsgResponse extends OMBaseResponse {
    public MyInvitaitonShareMsgModel data;
    public class MyInvitaitonShareMsgModel {

        public String avatar;
        public String company;
        public String name;
        public String ossUrl;
        public String roomId;
        public String roomLiveDesc;
        public String roomLiveId;
        public String roomName;
        public String invitationUrl;
        //SDk新增
        public String roomCode;
    }
}
