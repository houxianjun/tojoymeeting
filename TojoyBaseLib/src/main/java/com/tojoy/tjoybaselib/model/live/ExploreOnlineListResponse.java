package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

public class ExploreOnlineListResponse extends OMBaseResponse {

    public ExploreOnlineModelObj data;

    public class ExploreOnlineModelObj {
        public ArrayList<ExploreOnlineModel> list = new ArrayList<>();
    }
}
