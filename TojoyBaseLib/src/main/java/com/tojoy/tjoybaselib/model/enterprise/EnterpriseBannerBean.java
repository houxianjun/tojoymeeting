package com.tojoy.tjoybaselib.model.enterprise;

import java.util.List;

/**
 * @author qll
 * @date 2019/9/17
 */
public class EnterpriseBannerBean {
    public List<EnterpriseBannerModel> list;
}
