package com.tojoy.tjoybaselib.model.home;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

/**
 * @author houxianjun
 * @date 2020/8/15
 */
public class MineDistributionChannelRecordResponse extends OMBaseResponse {
    public DistributionChannelRecordModel data;

    public static class DistributionChannelRecordModel{
     public ArrayList<ChannelModel> records;
    }

    public class ChannelModel{
       public String withdrawalAccountNo;//":"12",
       public String  withdrawalName;//":null,
       public String  withdrawalChannel;//":"ALI_PAY"
    }
}
