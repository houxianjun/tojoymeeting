package com.tojoy.tjoybaselib.model.user;

/**
 * Created by chengyanfang on 2017/10/9.
 */

public class CountyModel extends AreaModel {
    public String disName;
    public String id;
    public String type;
    public boolean isChecked;

    public CountyModel(String disName, String id) {
        this.id = id;
        this.disName = disName;
    }

    @Override
    public String toString() {
        return disName;
    }

    @Override
    public String getTitle() {
        return disName;
    }
}
