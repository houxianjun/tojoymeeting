package com.tojoy.tjoybaselib.model.live;

/**
 * @author qll
 * @date 2019/5/8
 */
public class DoumentVideoSendMsg {
    // 视频地址
    public String url;
    // 已经播放的视频时间--秒
    public String playTime;
    // 0:开始播放视频， 1：暂停，2：退出播放视频
    public String status;

    // 获取时用到，用于接收上一次控制人的userid
    public String setUserId;
}
