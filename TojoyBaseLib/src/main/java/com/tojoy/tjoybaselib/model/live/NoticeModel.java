package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * Created by luuzhu on 2019/4/29.
 */

public class NoticeModel extends OMBaseResponse {

    public DataObjBean data;

    public class DataObjBean {
        /**
         * 显示时长
         */
        public String boardDuration;
        /**
         * 显示频率 多少分钟显示一次
         */
        public String boardFrequency;
        /**
         * 显示次数
         */
        public String boardTimesNum;
        public String isShow;
        public String publicBoardName;
    }
}
