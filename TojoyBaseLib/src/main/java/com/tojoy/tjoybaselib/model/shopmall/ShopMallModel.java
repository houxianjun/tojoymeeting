package com.tojoy.tjoybaselib.model.shopmall;

import com.tojoy.tjoybaselib.configs.OSSConfig;

/**
 * Created by daibin
 * on 2019/12/31.
 * function
 */
public class ShopMallModel {
    /**
     * 公司编码
     */
    public String companyCode;//公司编码
    public String companyName;//公司名称
    public String goodsId;//商品编号
    public String goodsImg1;//首图
    public String goodsName;//商品名称
    public String goodsPrice;//商品折扣前价格
    public String goodsRealPrice;//商品折扣后价格

    public String goodsType; // 1实物 2虚拟
    public boolean isChecked;
    public String isInterested;
    public String storeId; //店铺id
    public String isBuy; //0没买过，其他值都是买过
    public int isDeposit;   //定金：1 需要  0 不需要
    public String agentCompanyCode;//代播":"98821499",
    public String agentId;//代播":6,
    public String agentLive;//代播":1,
    public String getGoodsImg1() {
        return OSSConfig.getOSSURLedStr(goodsImg1);
    }
}
