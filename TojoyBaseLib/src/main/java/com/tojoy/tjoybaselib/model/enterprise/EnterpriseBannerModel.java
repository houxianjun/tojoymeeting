package com.tojoy.tjoybaselib.model.enterprise;

/**
 * @author qll
 * @date 2019/9/17
 */
public class EnterpriseBannerModel {
    public String bannerId;
    // 1:直播会议；2:商品；3:广告；4:企业宣传
    public int bannerType;
    // 房间号
    public String roomId;
    //  直播间id
    public String roomLiveId;
    public String id;
    public long createDate;
    public int companyCode;
    public String coverOne;
    public String userName;
    public String userId;
    public String bannerTitle;
    public String startDate;
    public int status;

}
