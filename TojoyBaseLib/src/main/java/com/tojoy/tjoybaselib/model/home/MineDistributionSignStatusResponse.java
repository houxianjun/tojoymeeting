package com.tojoy.tjoybaselib.model.home;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

public class MineDistributionSignStatusResponse extends OMBaseResponse {

    public  DistributionSignStatus  data;

    public static class DistributionSignStatus {
       //0:未签约;1:签约中;2:已签约;3:签约失败
       public int status;
       public String idCardNo;
       public String mobile;
       public String name;
    }

}
