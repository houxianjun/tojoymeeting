package com.tojoy.tjoybaselib.model.servicemanager;


import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.math.BigDecimal;
import java.util.List;

public class RechargeProductResponse extends OMBaseResponse {

    RechargeProductEntry data;

    public class RechargeProductEntry {
        /**
         * 产品列表
         */
        List<String> producttList;

        /**
         * 自定义金额上限
         */
        int moneyCeiling ;

        /**
         * 自定义金额下限
         */
        int moneyFloor ;

        /**
         * 单价
         */
        BigDecimal price;

        /**
         * 产品code
         */
        String  feeCode;

        /**
         * 产品名称
         */
        String feeName;


        public List<String> getProducttList() {
            return producttList;
        }

        public void setProducttList(List<String> producttList) {
            producttList = producttList;
        }

        public int getMoneyCeiling() {
            return moneyCeiling;
        }

        public void setMoneyCeiling(int moneyCeiling) {
            this.moneyCeiling = moneyCeiling;
        }

        public int getMoneyFloor() {
            return moneyFloor;
        }

        public void setMoneyFloor(int moneyFloor) {
            this.moneyFloor = moneyFloor;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public void setPrice(BigDecimal price) {
            this.price = price;
        }

        public String getFeeCode() {
            return feeCode;
        }

        public void setFeeCode(String feeCode) {
            this.feeCode = feeCode;
        }

        public String getFeeName() {
            return feeName;
        }

        public void setFeeName(String feeName) {
            this.feeName = feeName;
        }
    }


    public RechargeProductEntry getData() {
        return data;
    }

    public void setData(RechargeProductEntry data) {
        this.data = data;
    }
}
