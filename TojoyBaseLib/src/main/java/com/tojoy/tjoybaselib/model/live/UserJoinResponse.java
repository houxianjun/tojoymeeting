package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

/**
 * Created by luuzhu on 2019/9/11.
 */

public class UserJoinResponse extends OMBaseResponse {

    public DataBean data;
    
    public class DataBean {
        public String avatar;
        public Object companyCode;
        public String coverOne;
        public Object createDate;
        public String imRoomId;
        public String password;
        public String roomId;
        public String roomLiveId;
        public Object startDate;
        public int status;
        public String title;
        public int userId;
        public String userName;
        //SDk新增
        public String roomCode;
    }
}
