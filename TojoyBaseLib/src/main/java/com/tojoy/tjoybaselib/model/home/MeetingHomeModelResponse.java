package com.tojoy.tjoybaselib.model.home;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

public class MeetingHomeModelResponse extends OMBaseResponse {

    public MeetingHomeModel  data;

    public static class MeetingHomeModel{
        /**
         * 首页banner
         */
        public HomeModelResponse banner;
        /**
         * H5资源位
         */
        public HomeModelResponse topic;
        /**
         * 热门企业
         */
        public HomeModelResponse hot;
        /**
         * 招商推荐
         */
        public HomeModelResponse recommend;
    }

}
