package com.tojoy.tjoybaselib.model.home;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

public class MineProfitInfoDistributionResponse extends OMBaseResponse {
    public MineProfitInfoDistributionResponse.MineProfitDistributionModel data;

    public static class MineProfitDistributionModel{

        /**
         * 本月收益
         */
        public String monthIncome;
        /**
         * 今日收益
         */
        public String todayIncome;
        /**
         * 累计收益
         */
        public String totalIncome;
        /**
         * 待结算收益
         */
        public String preIncome;
        /**
         * 可提现收益
         */
        public String cashIncome;
        /**
         * 提现额度限制
         */
        public String minMoney;

        //已提现金额
        public String totalAmount;
        //到达扣税金额
        public String minAmount;
    }
}
