package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

public class ApplyExploreCountResponse extends OMBaseResponse {

    public ApplyExploreCount data;

    public class ApplyExploreCount {
        public String count;
    }
}
