package com.tojoy.tjoybaselib.model.user;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * Created by daibin
 * on 2019/9/17.
 * function
 */
public class CustomerServiceResponse extends OMBaseResponse {
    public String data;
}
