package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

public class EndLiveInfoResponse extends OMBaseResponse {

    public EndLiveInfoModel data;

    public class EndLiveInfoModel {
        public String enterNum;
        public String interestedKeyNum;
        public String seizeNum;
        public String durationTime;
        /**
         * 如果没有值，或者没有这个字段   立即购买数
         * 字段值为0   意向购买数
         * 字段值为1   立即购买数
         * 0--是企业未开通支付功能，1是企业开通支付功能
         */
        public String payment;
        /**
         * 开播申请设置是否生成回放：0：不生成；1：生成
         */
        public String isPlayback;

        public String getFormatNum(String value){
            long num = Long.valueOf(value);
            if (num >= 10000) {
                //把得到的值保留两位小数四舍五入
                double result = Math.round(num) / 10000d;
                String s1 = String.format("%.2f", result);
                return String.valueOf(s1) + "w";
            } else {
                return value;
            }
        }
    }

}
