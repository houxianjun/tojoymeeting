package com.tojoy.tjoybaselib.model.enterprise;

import android.text.TextUtils;

import com.tojoy.tjoybaselib.configs.OSSConfig;

import java.util.List;

/**
 * @author qll
 * @date 2019/9/16
 * 企业信息
 */
public class CorporationMsgModel {
    public String id;
    // 企业编码
    public String code;
    // 企业名称
    public String name;
    // 定制页状态：0-未开启；1-开启
    public int customState;
    public String phone;

    /**
     *  开播提醒结果页面--点击马上开播提示（欠费）
     */
    public String overdueFeesNotify;
    /**
     * 是否允许开播   0：不允许（企业最大欠费额度，当到达该数值后不可开播）， 1：允许
     */
    public String isAllowJoin;


    public List<CorporationNavsBean> corporationNavs;

    /**
     * 首页导航栏列表信息
     */
    public class CorporationNavsBean {
        public String companyCode;
        public String id;
        // code对应的栏目是固定的
        public String code;
        public String remark;
        // 0：不展示；1：展示
        public String state;
        public String title;

        public boolean getState() {
            if (!TextUtils.isEmpty(state) && "1".equals(state)) {
                return true;
            }
            return false;
        }
    }
}
