package com.tojoy.tjoybaselib.model.home;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

/**
 * @author houxianjun
 * @date 2020/8/15
 */
public class MineDistributionWithdarwalDetailResponse extends OMBaseResponse {
    public WithdarwalDetailModel data;

    public static class WithdarwalDetailModel{
     public String amount;
        public String createDate;
        public String failReason;
        public String orderNo;
        public String receivedAmount;
        public String serviceFee;
        public String status;
        public String successDate;
        public String withdrawalAccountNo;
        public String withdrawalChannel;
        public String withdrawalName;


    }


}
