package com.tojoy.tjoybaselib.model.OnlineMeeting;

import android.text.TextUtils;

import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.util.time.TimeUtility;

import java.util.List;

/**
 * @author qll
 * @date 2019/4/19
 */
public class ApprovalModel {
    /** 申请人名称 */
    public String trueName;
    public String company;
    public String job;
    public String roomName;
    /** 1：未操作；2：驳回；3：同意；4：后台管理员驳回；5：申请者自己撤回，不能操作 */
    public String status;
    public String createTime;
    public String playReason;
    public String headPicUrl;
    public String remark;
    public String checkId;
    public String applyId;

    public List<ApplyResultLeaderInfo> auditList;

    public String getHeadPicUrl() {
        return TextUtils.isEmpty(headPicUrl) ? " " : OSSConfig.getOSSURLedStr(headPicUrl);
    }

    public String getApplyName() {
        return TextUtils.isEmpty(trueName) ? "" : trueName;
    }

    public String getCompanyAndJob() {
        // 独立app不需要职位
        return (TextUtils.isEmpty(company) ? "暂无" : company);
    }

    public String getRoomName() {
        return TextUtils.isEmpty(roomName) ? "" : roomName;
    }

    public String getStartReason() {
        return TextUtils.isEmpty(playReason) ? "" : playReason;
    }

    public String getApplyTime() {
        return TimeUtility.getTimeFormat(Long.parseLong(createTime),"yyyy/MM/dd   HH:mm:ss");
    }

    public String getRejectMsg() {
        return TextUtils.isEmpty(remark) ? "" : remark;
    }

    public boolean isCheckenOver(){
        // 是否处理过此次请求(true:已处理；false：未处理)
        return !TextUtils.isEmpty(status) && !"1".equals(status);
    }

    public String getStatus(){
        return TextUtils.isEmpty(status)? "" : status;
    }
}
