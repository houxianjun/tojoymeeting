package com.tojoy.tjoybaselib.model.user;

import java.util.ArrayList;

/**
 * Created by chengyanfang on 2017/10/9.
 */

public class CityModel extends AreaModel {
    public String disName;
    public String id;
    public String type;
    public boolean isChecked;

    public CityModel(String disName, String id) {
        this.id = id;
        this.disName = disName;
    }

    public ArrayList<CountyModel> sonList = new ArrayList<>();

    @Override
    public String toString() {
        return disName;
    }

    @Override
    public String getTitle() {
        return disName;
    }
}
