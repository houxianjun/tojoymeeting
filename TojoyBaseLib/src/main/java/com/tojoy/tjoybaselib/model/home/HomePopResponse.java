package com.tojoy.tjoybaselib.model.home;

import android.text.TextUtils;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * Created by Administrator on 2018/8/21 0021.
 * 首页弹窗数据
 */

public class HomePopResponse extends OMBaseResponse {

    public DataObjBean data;

    public class DataObjBean {
        public String adLinkKey;
        public String adLinkType;
        public String adLinkUrl;
        public String adLinkId;//关联会议id 或关联公司主页的公司Id
        public String adLinkLiveId;//直播间id
        public String isPw;//是否需要密码  1是0否
        public String id;

        public String adUrl;
        public double aspectRatio;

        public boolean isPw() {
            if(!TextUtils.isEmpty(isPw) && "1".equals(isPw)){
                return true;
            }else{
                return false;
            }
        }
    }
}
