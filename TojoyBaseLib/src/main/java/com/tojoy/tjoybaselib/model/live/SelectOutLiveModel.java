package com.tojoy.tjoybaselib.model.live;

import android.os.Parcel;
import android.os.Parcelable;

public class SelectOutLiveModel implements Parcelable {
    public String closeId;
    public String pullFlvUrl;
    public String pullRtmpUrl;
    public String roomLiveId;
    public String status;
    public String streamId;
    public String liveId;
    public String audioStatus;
    public String roomTitle;
    public String type;
    public String pushStreamId;
    public String liveAcceptId;
    public String anchorName;
    public String audienceName;
    public String listType;

    public static final Creator<SelectOutLiveModel> CREATOR = new Creator<SelectOutLiveModel>() {
        @Override
        public SelectOutLiveModel createFromParcel(Parcel source) {
            SelectOutLiveModel classTerm = new SelectOutLiveModel();
            classTerm.closeId = source.readString();
            classTerm.pullFlvUrl = source.readString();
            classTerm.pullRtmpUrl = source.readString();
            classTerm.roomLiveId = source.readString();
            classTerm.status = source.readString();
            classTerm.streamId = source.readString();
            classTerm.liveId = source.readString();
            classTerm.audioStatus = source.readString();
            classTerm.roomTitle = source.readString();
            classTerm.type = source.readString();
            classTerm.pushStreamId = source.readString();
            return classTerm;
        }

        @Override
        public SelectOutLiveModel[] newArray(int size) {
            return new SelectOutLiveModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(closeId);
        dest.writeString(pullFlvUrl);
        dest.writeString(pullRtmpUrl);
        dest.writeString(roomLiveId);
        dest.writeString(status);
        dest.writeString(streamId);
        dest.writeString(liveId);
        dest.writeString(audioStatus);
        dest.writeString(roomTitle);
        dest.writeString(type);
        dest.writeString(pushStreamId);
    }
}