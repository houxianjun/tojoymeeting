package com.tojoy.tjoybaselib.model.meetingperson.SingleTonModel;

import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 会议联系人的单例类，存储组织架构内与外部的联系人数据
 */
public class MeetingPersonTon {

    /**
     * 是否点击确定按钮了
     */
    private boolean isClickSure = false;


    public boolean isClickSure() {
        return isClickSure;
    }

    public void setClickSure(boolean clickSure) {
        isClickSure = clickSure;
    }

    /**
     * 其他页面传过来的最大参会人数限制
     */
    public int maxInvitation;

    /**
     * 当前会议的类型
     * 1：内外部人员可参会
     * 2：仅内部人员可参会
     * 3：仅邀请人员可参会
     */
    public String meetingAuthType;

    /**
     * 组织架构的总人数
     */
    public int allPeopleCount;

    public List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> outSidePersonList;

    /**
     * *******************用户保存确定之后的数据data*******
     */

    private MeetingPersonDepartmentResponse.DepartmentModel sureDepartPersonData;

    public MeetingPersonDepartmentResponse.DepartmentModel getSureDepartPersonData() {
        return sureDepartPersonData;
    }

    public void setSureDepartPersonData(MeetingPersonDepartmentResponse.DepartmentModel sureDepartPersonData) {
        this.sureDepartPersonData = sureDepartPersonData;
    }

    private HashMap<String, String> allDepartHashMapString = new HashMap<>();

    public HashMap<String, String> getAllDepartHashMapString() {
        return allDepartHashMapString;
    }

    public void setAllDepartHashMapString(HashMap<String, String> allDepartHashMapString) {
        this.allDepartHashMapString = allDepartHashMapString;
    }

    //全部的部门数据对应表
    private HashMap<String, MeetingPersonDepartmentResponse.DepartmentModel> allDepartHashMap = new HashMap<>();

    //保存选中的简直对对象
    private HashMap<String, MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> mAllCheckedPersonModels = new HashMap<>();

    public void addCheckPersonModelToMap(String userid, MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel) {
        mAllCheckedPersonModels.put(userid, employeeModel);
    }

    public void removeCheckPersonModeToMap(String userid) {
        mAllCheckedPersonModels.remove(userid);
    }

    public void updateCheckPersonModelToMap(String userid, boolean isCheck) {
        mAllCheckedPersonModels.get(userid).isChecked = isCheck;
    }

    private MeetingPersonTon() {

    }

    public static synchronized MeetingPersonTon getInstance() {
        return SingletonHolder.instance;
    }

    private static class SingletonHolder {
        private static final MeetingPersonTon instance = new MeetingPersonTon();
    }

    public List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> getOutSidePersonList() {
        return outSidePersonList;
    }

    public void setOutSidePersonList(List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> outSidePersonList) {
        this.outSidePersonList = outSidePersonList;
    }

    public void addAllOutSidePersons(List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> outSidePersonList) {
        if (outSidePersonList == null || outSidePersonList.size() == 0) {
            return;
        }
        this.outSidePersonList.addAll(outSidePersonList);
    }


    private MeetingPersonDepartmentResponse.DepartmentModel mDepartmentData;

    public MeetingPersonDepartmentResponse.DepartmentModel getmDepartmentData() {
        return mDepartmentData;
    }

    public void setmDepartmentData(MeetingPersonDepartmentResponse.DepartmentModel mDepartmentData) {
        this.mDepartmentData = mDepartmentData;
    }

    /**
     * 初始化部门数据
     * 功能：将外部传入的选中的人员与本组织架构人员、外部人员遍历，
     * 从而初始化组织架构当前选中人员和外部人员中已选择的状态
     */
    public void initDepartmentData(int maxPersonCount, MeetingPersonDepartmentResponse.DepartmentModel mDepartmentData) {
        this.maxInvitation = maxPersonCount;
        allPeopleCount = 0;
        this.mDepartmentData = mDepartmentData;
        if (mDepartmentData != null) {
            // 从最顶层部门开始遍历人员
            initChildDepartMentData(mDepartmentData);
        }
    }

    /**
     * 遍历子部门的数据进行初始化选中状态
     *
     * @param childData
     */
    private void initChildDepartMentData(MeetingPersonDepartmentResponse.DepartmentModel childData) {
        if (childData.respUserForDepartmentDtoList != null && childData.respUserForDepartmentDtoList.size() > 0) {
            // 遍历人员
            // 计算总人数
            allPeopleCount = allPeopleCount + childData.respUserForDepartmentDtoList.size();
        }
        if (childData.corDepartmentList != null && childData.corDepartmentList.size() > 0) {
            // 继续遍历部门
            for (int i = 0; i < childData.corDepartmentList.size(); i++) {
                initChildDepartMentData(childData.corDepartmentList.get(i));
            }
        }
    }

    public void recDeptEmlopee(String itemDeptId, MeetingPersonDepartmentResponse.DepartmentModel departmentModel) {
        if (departmentModel != null) {
            allDepartHashMap.put(itemDeptId, departmentModel);
            if (departmentModel.corDepartmentList != null && !departmentModel.corDepartmentList.isEmpty()) {
                for (int i = 0; i < departmentModel.corDepartmentList.size(); i++) {
                    recDeptEmlopee(departmentModel.corDepartmentList.get(i).id, departmentModel.corDepartmentList.get(i));
                }
            }
        }
    }


    /**
     * 设置某一部门下面的选中或不选中
     */
    public void setSelectDempartStatus(boolean isCheck, MeetingPersonDepartmentResponse.DepartmentModel departmentModel) {
        if (departmentModel == null) {
            return;
        }
        departmentModel.isChecked = isCheck;
        if (departmentModel.corDepartmentList != null && departmentModel.corDepartmentList.size() > 0) {
            // 继续遍历部门
            for (int i = 0; i < departmentModel.corDepartmentList.size(); i++) {
                setSelectDempartStatus(isCheck, departmentModel.corDepartmentList.get(i));
            }
        }
        if (departmentModel.respUserForDepartmentDtoList != null && departmentModel.respUserForDepartmentDtoList.size() > 0) {
            // 遍历人员
            for (int j = 0; j < departmentModel.respUserForDepartmentDtoList.size(); j++) {
                MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel model = departmentModel.respUserForDepartmentDtoList.get(j);
                model.isChecked = isCheck;
            }
        }
    }

    /**
     * 遍历部门下的所有人
     */
    public List<String> recItemDeptEmployee(List<String> list, MeetingPersonDepartmentResponse.DepartmentModel departmentModel) {

        if (departmentModel != null) {
            if (departmentModel.respUserForDepartmentDtoList != null && !departmentModel.respUserForDepartmentDtoList.isEmpty()) {
                for (int i = 0; i < departmentModel.respUserForDepartmentDtoList.size(); i++) {
                    list.add(departmentModel.respUserForDepartmentDtoList.get(i).userId);
                }
            }
            if (departmentModel.corDepartmentList != null && !departmentModel.corDepartmentList.isEmpty()) {
                for (int i = 0; i < departmentModel.corDepartmentList.size(); i++) {
                    recItemDeptEmployee(list, departmentModel.corDepartmentList.get(i));
                }
            }
        }
        return list;
    }

    public boolean recRmoveByUseridDeptEmployee(boolean isHave, String userid, MeetingPersonDepartmentResponse.DepartmentModel departmentModel) {

        if (departmentModel != null) {
            if (departmentModel.respUserForDepartmentDtoList != null && !departmentModel.respUserForDepartmentDtoList.isEmpty()) {
                for (int i = 0; i < departmentModel.respUserForDepartmentDtoList.size(); i++) {
                    if (userid.equals(departmentModel.respUserForDepartmentDtoList.get(i).mobile)) {
                        isHave = true;
                    }
                }
            }
            if (departmentModel.corDepartmentList != null && !departmentModel.corDepartmentList.isEmpty()) {
                for (int i = 0; i < departmentModel.corDepartmentList.size(); i++) {
                    recRmoveByUseridDeptEmployee(isHave, userid, departmentModel.corDepartmentList.get(i));
                }
            }
            if (departmentModel.allowUserList != null && !departmentModel.allowUserList.isEmpty()) {
                for (int i = 0; i < departmentModel.allowUserList.size(); i++) {
                    if (userid.equals(departmentModel.respUserForDepartmentDtoList.get(i).userId)) {
                        departmentModel.respUserForDepartmentDtoList.get(i).isChecked = true;
                    }
                }
            }
        }
        return isHave;
    }

    /**
     * 遍历部门下的所有被选中的人
     */
    public List<String> recItemSelcetPersonEmployee(List<String> list, MeetingPersonDepartmentResponse.DepartmentModel departmentModel) {
        if (departmentModel != null) {
            //遍历员工
            if (departmentModel.respUserForDepartmentDtoList != null && !departmentModel.respUserForDepartmentDtoList.isEmpty()) {
                for (int i = 0; i < departmentModel.respUserForDepartmentDtoList.size(); i++) {
                    //被选中的
                    if (departmentModel.respUserForDepartmentDtoList.get(i).isChecked) {
                        list.add(departmentModel.respUserForDepartmentDtoList.get(i).userId);
                    }
                }
            }
            //遍历部门人员
            if (departmentModel.corDepartmentList != null && !departmentModel.corDepartmentList.isEmpty()) {
                for (int i = 0; i < departmentModel.corDepartmentList.size(); i++) {
                    recItemSelcetPersonEmployee(list, departmentModel.corDepartmentList.get(i));
                }
            }

            //遍历外部人员
            if (departmentModel.allowUserList != null && !departmentModel.allowUserList.isEmpty()) {
                for (int i = 0; i < departmentModel.allowUserList.size(); i++) {
                    //被选中的
                    if (departmentModel.allowUserList.get(i).isChecked) {
                        list.add(departmentModel.allowUserList.get(i).userId);
                    }
                }
            }
        }
        return list;
    }

    /**
     * 设置部门下所有人的选中／未选中状态
     */
    public void recSetDepartmentPseronStatus(boolean isCheck, MeetingPersonDepartmentResponse.DepartmentModel departmentModel) {
        if (departmentModel != null) {
            if (departmentModel.respUserForDepartmentDtoList != null && !departmentModel.respUserForDepartmentDtoList.isEmpty()) {
                for (int i = 0; i < departmentModel.respUserForDepartmentDtoList.size(); i++) {
                    departmentModel.respUserForDepartmentDtoList.get(i).isChecked = isCheck;
                }
            }
            if (departmentModel.corDepartmentList != null && !departmentModel.corDepartmentList.isEmpty()) {
                for (int i = 0; i < departmentModel.corDepartmentList.size(); i++) {
                    recSetDepartmentPseronStatus(isCheck, departmentModel.corDepartmentList.get(i));
                }
            }
        }
    }


    /**
     * 遍历外部联系人设置选中状态
     */
    public void recSetOutSidePersonStatus(boolean isCheck, List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> outSidePersonList) {
        //从新遍历一次当前外部人员已选择的人数
        // 遍历人员
        for (int j = 0; j < outSidePersonList.size(); j++) {
            MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel = outSidePersonList.get(j);
            employeeModel.isChecked = isCheck;
        }
    }

    public void recSetDepartItemPersonStatus(boolean isCheck, MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel) {
        employeeModel.isChecked = isCheck;
    }

    public int recCurDepartSelectPersonCount;

    /**
     * 遍历计算部门所有被选择人数
     */
    public int recCurDepartSelectPerson(MeetingPersonDepartmentResponse.DepartmentModel departmentModel) {
        if (departmentModel != null) {
            if (departmentModel.respUserForDepartmentDtoList != null && !departmentModel.respUserForDepartmentDtoList.isEmpty()) {
                for (int i = 0; i < departmentModel.respUserForDepartmentDtoList.size(); i++) {
                    if (departmentModel.respUserForDepartmentDtoList.get(i).isChecked) {
                        recCurDepartSelectPersonCount++;
                    }
                }
            }
            if (departmentModel.corDepartmentList != null && !departmentModel.corDepartmentList.isEmpty()) {
                for (int i = 0; i < departmentModel.corDepartmentList.size(); i++) {
                    recCurDepartSelectPerson(departmentModel.corDepartmentList.get(i));
                }
            }
        }

        return recCurDepartSelectPersonCount;
    }

    /**
     * 遍历部门下的所有人
     */
    public int recListDeptEmployee(int count, List<MeetingPersonDepartmentResponse.DepartmentModel> departmentModel) {
        for (MeetingPersonDepartmentResponse.DepartmentModel departModel : departmentModel) {
            List<String> list = new ArrayList<>();
            count += recItemDeptEmployee(list, departModel).size();
        }
        return count;
    }

    /**
     * 遍历部门下的所有被选中的人
     */
    public HashMap<String, String> recInitAllCheckedPersonCountMap(HashMap<String, String> map, MeetingPersonDepartmentResponse.DepartmentModel departmentModel) {
        if (departmentModel != null) {
            if (departmentModel.respUserForDepartmentDtoList != null && !departmentModel.respUserForDepartmentDtoList.isEmpty()) {
                for (int i = 0; i < departmentModel.respUserForDepartmentDtoList.size(); i++) {
                    //被选中的
                    if (departmentModel.respUserForDepartmentDtoList.get(i).isChecked) {
                        map.put(departmentModel.respUserForDepartmentDtoList.get(i).userId, departmentModel.respUserForDepartmentDtoList.get(i).mobile);
                    }
                }
            }
            if (departmentModel.corDepartmentList != null && !departmentModel.corDepartmentList.isEmpty()) {
                for (int i = 0; i < departmentModel.corDepartmentList.size(); i++) {
                    recInitAllCheckedPersonCountMap(map, departmentModel.corDepartmentList.get(i));
                }
            }
        }
        return map;
    }


    public void clear() {
        this.outSidePersonList = null;
        mDepartmentData = null;
        setSureDepartPersonData(null);
        this.allDepartHashMap.clear();
        //全部的部门数据对应表
        this.allDepartHashMap = new HashMap<>();
    }

}
