package com.tojoy.tjoybaselib.model.live;

import android.text.TextUtils;

import com.tojoy.tjoybaselib.configs.OSSConfig;

public class PublishQRListModel {
    public String id;
    public String name;
    public String picBig;
    public String picSmall;
    /**
     * -1 未发布过:'1：发布  2：隐藏',
     */
    public String status;

    public String openid;
    public String oprationStatus;
    public String picShare;
    public String shareRemark;
    public String sortNum;
    public String statusStr;
    public String type;
    public String url;
    public String wechatName;
    public String wechatUrl;

    public String getName() {
        return TextUtils.isEmpty(name) ? "" : name;
    }

    public String getPicBig() {
        return TextUtils.isEmpty(picBig) ? "" : OSSConfig.getOSSURLedStr(picBig);
    }

    /**
     * 获取当前活动的发布状态
     * -1：从未发布过（页面显示：隐藏-置灰&&不可点；发布-可点）
     * 1：已经发布过且不是隐藏状态（页面显示：隐藏-高亮&&可点；再次发布-可点）
     * 2：已发布过且当前为隐藏状态（页面显示：已隐藏-置灰&&不可点；再次发布-可点）
     * @return
     */
    public int getStatus(){
        if (TextUtils.isEmpty(status)) {
            return 0;
        } else {
            return Integer.valueOf(status);
        }
    }
}
