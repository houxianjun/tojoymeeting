package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

public class ExploreMemeberStatusResponse extends OMBaseResponse {

    public ArrayList<ExploreMemeberStatus> data = new ArrayList<>();

    public class ExploreMemeberStatus {
        public String audioStatus;
        public String videoStatus;
        public String selectUserId;
        // 电话的接挂状态：0：挂断/未操作；1：接听
        public String isCalling;
    }
}
