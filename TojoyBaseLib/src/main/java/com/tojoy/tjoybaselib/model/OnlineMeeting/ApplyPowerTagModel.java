package com.tojoy.tjoybaselib.model.OnlineMeeting;

import android.text.TextUtils;

import java.io.Serializable;

/**
 * @author qll
 * @date 2019/4/2
 */
public class ApplyPowerTagModel implements Serializable {
    public String tagName;
    public String tagId;
    public String isSelect;
    public boolean getIsSelect(){
        return !TextUtils.isEmpty(isSelect) && "1".equals(isSelect);
    }
}
