package com.tojoy.tjoybaselib.model.OnlineMeeting;

import android.text.TextUtils;

import com.tojoy.tjoybaselib.services.OnlineMeeting.ApplyStateCode;

import java.io.Serializable;
import java.util.List;

/**
 * @author qll
 * @date 2019/4/19
 */
public class ApplyJoinLiveModel implements Serializable {
    /**
     * 房间ID
     */
    public String roomId;
    /**
     * 房间ID：roomId替换为RoomCode（Sdk版本，云洽会app版本依旧使用roomId）
     */
    public String roomCode;
    /**
     * 提交领导
     */
    public String superName;
    /**
     * 观看人数列表
     */
    public List<LookPeopleNumModel> peopleNumList;
    /**
     * 标签列表
     */
    public List<ApplyPowerTagModel> tagList;

    /**
     * 审核中、审核通过、审核驳回相关信息
     */
    /**
     * 申请人员的公司信息
     */
    public String company;
    /**
     * 申请人员的职位
     */
    public String job;
    /**
     * 审核状态：1、申请中  2、被驳回  3 已通过  4已完成(本次申请通过并且直播完成)
     */
    public int status;
    /**
     * 申请人员姓名
     */
    public String userName;
    /**
     * 申请ID
     */
    public String applyId;
    public String imRoomId;
    public String liveId;
    /**
     * 1:是领导；0：非领导
     */
    public String leaderFlag;
    /**
     * 1:展示撤回按钮； 0 ：不展示撤回按钮（这个字段只针对于领导）
     */
    public String isShowRevert;
    public ApplyJoinLiveModel lastParams;

    /**
     * 邀请人员手机信息
     */
    public String mobiles;
    /**
     * 邀请人员userid
     */
    public String userIds;

    /**
     *  开播余额不足提醒，不为空时则显示
     */
    public String remainingTimeNotify;


    /**
     * 审批流程人员列表
     */
    public List<ApplyResultLeaderInfo> respLiveApplyCheckDtoList;

    public ApplyStateCode getApplyStatus() {
        return ApplyStateCode.getStateCode(status);
    }

    public String getCompany() {
        return TextUtils.isEmpty(company) ? "" : company;
    }
    /**
     * 查看详情、重新申请相关数据
     */
    public String playReason;
    public String roomName;
    public String roomPassword;
    // 可观看人数
    public String viewNum;
    // 理由
    public String remark;
    // 观看权限（1：内外部人员；2：仅内部；3：仅邀请人员(用户可登陆企业管理后台添加参会人员)）
    public String authType;
    // 视频引流权限
    public String drainageType;
    // 是否生成回放：0：不生成；1：生成
    public String isPlayback;
    // 邀请参会人数
    public List<ApplyPhotoInfo> docList;
    public List<ApplyPhotoInfo> picList;
    public List<ApplyPowerTagModel> selestTagList;

    public String getAuthType(){
        if (TextUtils.isEmpty(authType)){
            return "";
        } else {
            if ("1".equals(authType)){
                return "内外部人员可参会";
            } else if ("2".equals(authType)){
                return "仅内部人员可参会";
            } else if ("3".equals(authType)){
                return "仅邀请人员可参会";
            } else {
                return "";
            }
        }
    }

    public String getDrainageType(){
        if (TextUtils.isEmpty(drainageType)){
            return "";
        } else {
            if ("1".equals(drainageType)){
                return "可被其他会议引用本会议画面";
            } else {
                return "不可被其他会议引用本会议画面";
            }
        }
    }

    public String getInvitationPeoplesNum(){
        if (TextUtils.isEmpty(userIds)) {
            return "0人" ;
        } else {
            String[] invitations = userIds.split(",");
            return invitations.length + "人" ;
        }
    }

    public String getPlayBackType(){
        if (TextUtils.isEmpty(isPlayback)){
            return "不生成";
        } else {
            if ("1".equals(isPlayback)){
                return "生成";
            } else {
                return "不生成";
            }
        }
    }

}
