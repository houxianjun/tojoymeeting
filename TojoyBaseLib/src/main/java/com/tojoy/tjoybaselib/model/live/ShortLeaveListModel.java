package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

/**
 * Created by luuzhu on 2019/4/24.
 */

public class ShortLeaveListModel extends OMBaseResponse {

    public DataObjBean data;

    public class DataObjBean {

        public List<ListBean> list;

        public class ListBean {
            public long createDate;
            public int id;
            public int isShow;
            public String pauseMsg;
            public int weight;
        }
    }
}
