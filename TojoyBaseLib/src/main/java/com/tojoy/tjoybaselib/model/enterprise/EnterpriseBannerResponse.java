package com.tojoy.tjoybaselib.model.enterprise;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * @author fanxi
 * @date 2019/9/11.
 * description：
 */
public class EnterpriseBannerResponse extends OMBaseResponse {

    public EnterpriseBannerBean data;
}
