package com.tojoy.tjoybaselib.model.live;


import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

public class SelectOutLiveResponse extends OMBaseResponse {

    public SelectOutLiveModelList data;

    public class SelectOutLiveModelList {
        public ArrayList<SelectOutLiveModel> respStreamRoomDtoList = new ArrayList<>();

        public String count;

        public String delCount;
    }
}
