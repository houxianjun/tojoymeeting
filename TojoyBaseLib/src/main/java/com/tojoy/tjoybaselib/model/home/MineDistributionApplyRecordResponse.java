package com.tojoy.tjoybaselib.model.home;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

/**
 * @author houxianjun
 * @date 2020/8/15
 */
public class MineDistributionApplyRecordResponse extends OMBaseResponse {
    public DistributionApplyRecordModel data;

    public static class DistributionApplyRecordModel{
     public ArrayList<ApplyModel> records;
    }

    public class ApplyModel{
       public String id;//":"12",
       public String  withdrawalChannel;//":null,
       public String  amount;//":"ALI_PAY"
        public String createDate;
        public String status;

    }
}
