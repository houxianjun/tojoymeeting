package com.tojoy.tjoybaselib.model.meetingperson;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.io.Serializable;
import java.util.List;

/**
 * @author liuhm 修改 侯宪军
 * @date 2020/2/27 修改 2020.05.11
 * @desciption 选择参会人 修改云洽会1.2.3需求，数据结构增加外部联系人
 */
public class MeetingPersonDepartmentResponse extends OMBaseResponse implements Cloneable{

    public DepartmentModel data;

    public static class DepartmentModel implements Serializable ,Cloneable{
        public List<DepartmentModel> corDepartmentList;
        public String corporationCode;
        public String corporationId;
        public String id;
        public String name;
        public List<EmployeeModel> respUserForDepartmentDtoList;
        public boolean isChecked;

        //1.2.3新增外部联系人
        public List<EmployeeModel> allowUserList;
        public static class EmployeeModel implements Serializable,Cloneable{

            public String mobile;
            public String trueName;
            public String userId;
            public boolean isChecked;
            public String employeeId;
            //失败原因，只在添加外部联系人时用到
            public String failMsg;
            //该字段仅在本地使用，在使用添加外部联系人的时候，会根据此状态判断是否添加成功。
            public boolean isAddSucess;
            //是否是外部人员 true 为是外部， false 不是外部人员
            public boolean isOutSidePerson;
            @Override
            public Object clone() throws CloneNotSupportedException {
                // 实现clone方法
                return super.clone();
            }

            @Override
            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj == null) {
                    return false;
                }
                if (getClass() != obj.getClass()) {
                    return false;
                }
                EmployeeModel other = (EmployeeModel) obj;
                if (mobile != other.mobile) {
                    return false;
                }
                if (trueName == null) {
                    if (other.trueName != null)
                        return false;
                } else if (!trueName.equals(other.trueName))
                    return false;
                return true;
            }

        }

        @Override
        public Object clone() throws CloneNotSupportedException {
            // 实现clone方法
            return super.clone();
        }
    }
    @Override
    public Object clone() throws CloneNotSupportedException {
        // 实现clone方法
        return super.clone();
    }
}
