package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

public class InviteApplyResponse extends OMBaseResponse {

    public ArrayList<InviteApplyResModel> data = new ArrayList<>();

    public class InviteApplyResModel {
        public String applyForId;
        public String receiveId;
    }
}
