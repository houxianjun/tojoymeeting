package com.tojoy.tjoybaselib.model.user;

import java.util.ArrayList;

/**
 * Created by chengyanfang on 2017/10/9.
 */

public class ProvinceModel extends AreaModel {
    public String disName;
    public String id;
    public String type;
    public boolean isChecked;

    public ProvinceModel(String disName, String id) {
        this.disName = disName;
        this.id = id;
    }

    public ArrayList<CityModel> sonList = new ArrayList<>();


    @Override
    public String toString() {
        return disName;
    }

    @Override
    public String getTitle() {
        return disName;
    }
}
