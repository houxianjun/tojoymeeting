package com.tojoy.tjoybaselib.model.live;

import android.text.TextUtils;

/**
 * @author qll
 * @date 2019/5/3
 */
public class ApplyGuestInfo {

    public String applyForId;
    public String company;
    /** 0:未处理；1接受；-1：拒绝 */
    public int oprationStatus;
    public String postTime;
    public String trueName;
    public String userId;
    public String avatar;

    public String getAvatar() {
        return TextUtils.isEmpty(avatar) ? "" : avatar;
    }

    public String getTrueName() {
        return TextUtils.isEmpty(trueName) ? "" : trueName;
    }

    public String getCompany() {
        return TextUtils.isEmpty(company) ? "" : company;
    }
}
