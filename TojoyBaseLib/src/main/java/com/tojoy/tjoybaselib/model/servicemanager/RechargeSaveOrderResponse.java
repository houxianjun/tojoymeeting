package com.tojoy.tjoybaselib.model.servicemanager;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

public class RechargeSaveOrderResponse extends OMBaseResponse {
    RechargeSaveOrderEntry data;

    public class RechargeSaveOrderEntry {
        String orderId; // 67,
        String orderCode; //": "S202004130120275"

        public String getOrderId() {
            return orderId;
        }

        public String getOrderCode() {
            return orderCode;
        }
    }

    public RechargeSaveOrderEntry getData() {
        return data;
    }

    public void setData(RechargeSaveOrderEntry data) {
        this.data = data;
    }
}
