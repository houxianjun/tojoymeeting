package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

public class LiveSignResponse extends OMBaseResponse {

    public DataBean data;

    public class DataBean {

        public LoginParamsBean loginParams;

        public class LoginParamsBean {
            public String userSign;
        }
    }

}
