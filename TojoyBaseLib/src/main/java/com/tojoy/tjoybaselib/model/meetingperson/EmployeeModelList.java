package com.tojoy.tjoybaselib.model.meetingperson;

import java.io.Serializable;
import java.util.List;

public class EmployeeModelList implements Serializable {
    public EmployeeModelList(List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> list){
        this.list=list;
    }
    List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> list;


    public List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> getList() {
        return list;
    }

    public void setList(List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> list) {
        this.list = list;
    }
}
