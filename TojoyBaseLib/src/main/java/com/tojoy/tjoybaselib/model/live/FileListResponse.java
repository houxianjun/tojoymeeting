package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

public class FileListResponse extends OMBaseResponse {

    public FileListModel data;

    public class FileListModel {
        public ArrayList<FileModel> list = new ArrayList<>();
    }
}
