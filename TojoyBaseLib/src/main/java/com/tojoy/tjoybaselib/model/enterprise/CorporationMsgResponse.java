package com.tojoy.tjoybaselib.model.enterprise;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * @author qll
 * @date 2019/9/16
 */
public class CorporationMsgResponse extends OMBaseResponse {
    public CorporationMsgModel data;
}
