package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

public class GoodsResponse extends OMBaseResponse {

    public DataBean data;

    public class DataBean {

        public List<RecordsBean> records;
        public int total;
        public class RecordsBean {
            public String id;
            /** 商品详情图片 */
            public String imgurl;
            /** 直播间图片 */
            public String liveRoomCoverImgUrl;
            public String name;
            /** 副标题 */
            public String subhead;
            /**（1：真实商品、2：虚拟商品）*/
            public String attribute;
            public boolean isChecked;
            /** 店铺id */
            public String sellStoreId;
            /** 商品价格 */
            public String salePrice;
            /** 产品游览次数 */
            public String visitNum;


            /** 是否参与推广 1参与，0不参与 */
            public int ifDistribution;
            /** 本场次会议最大分销额预算 */
            public String maxBudget;
            /** 一级分销员佣金 */
            public String level1Commission;
            /** 二级分销员佣金 */
            public String level2Commission;
            /** 一级员工分销额佣金 */
            public String level1CommissionEmployee;
            /** 二级员工分销额佣金 */
            public String level2CommissionEmployee;
            /** 最少参会人数 */
            public String minPerson;
            /** 最低时长 */
            public String minViewTime;
            /** 此企业的公司码 */
            public String companyCode;

            /**
             * 代播商品新增参数
             */
            public String agentCompanyCode;
            public String agentId;
            public String agentLive;
        }
    }
}
