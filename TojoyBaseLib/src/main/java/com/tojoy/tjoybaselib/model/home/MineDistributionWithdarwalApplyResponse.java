package com.tojoy.tjoybaselib.model.home;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * @author houxianjun
 * @date 2020/8/15
 */
public class MineDistributionWithdarwalApplyResponse extends OMBaseResponse {
    public WithdarwalApplyModel data;

    public static class WithdarwalApplyModel{
     public String id;
        public String distributorId;
        public String distributorUserId;
        public String distributorMobile;
        public String amount;
        public String distributorTrueName;
        public String status;
        public String createDate;
        public String updateId;
        public String serviceFee;
        public String orderNo;


    }


}
