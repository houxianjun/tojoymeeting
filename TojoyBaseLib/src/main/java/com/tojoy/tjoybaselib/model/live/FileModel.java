package com.tojoy.tjoybaselib.model.live;

import android.text.TextUtils;

public class FileModel {
    /**
     * 类型1、图片 2、视频,3 ppt,4 pdf, 999 其他
     */
    public String type;
    public String url;
    public String documentId;
    public String fileName;
    /** 0否，1是，是否是点击我的文件夹 */
    public String isMe;
    public boolean isSelected;
    public String fileDir;
    /** 标识是否处于长按删除状态 */
    public boolean isDeleteChecked;
    /** pdf图片列表的真实的documentId（防止pdf为区分白板）*/
    public String documentIdPic;

    /**
     * pdf转码的参数
     */
    public int pages;
    public String resolution;
    public String resultUrl;
    public String title;

    public String getDocumentIdPic(){
        if (!TextUtils.isEmpty(documentIdPic)){
            return documentIdPic;
        } else {
            return documentId;
        }
    }
}
