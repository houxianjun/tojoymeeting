
package com.tojoy.tjoybaselib.model.enterprise;

import com.tojoy.tjoybaselib.model.live.OnlineMeetingHomeGridModel;
import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

/**
 * @author qululu
 * 网上天洽会首页：热门直播间列表、精选路演师列表数据共用一个
 */
public class EnterpriseHomeHotLiveListResponse extends OMBaseResponse {
    public EnterpriseHomeHotLiveBean  data;
    public class EnterpriseHomeHotLiveBean{
        public int total;
        public String extra;
        public List<OnlineMeetingHomeGridModel>  list;
    }

}
