package com.tojoy.tjoybaselib.model.enterprise;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

/**
 * @author fanxi
 * @date 2019/9/11.
 * description：
 */
public class BrowsedEnterpriseResponse extends OMBaseResponse {

    public BrowsedEnterpriseModel data;

    public class BrowsedEnterpriseModel {
        public List<EnterprisListBean> list;

        public List<EnterprisListBean> records;

        public class EnterprisListBean {
            public String companyCode;
            public String companyName;
            public String userName;
            public String company;
            public String roomLiveId;
            public String id;
            public String job;
            public String status;
            public String logoUrl;
        }
    }

}
