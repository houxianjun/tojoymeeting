package com.tojoy.tjoybaselib.model.shopmall;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

/**
 *商城列表列表返回实体类
 */
public class ShopMallListResponse extends OMBaseResponse {
    public ShopMallBean data;
    public  class ShopMallBean {
        public List<ShopMallModel> list;
    }
}
