package com.tojoy.tjoybaselib.model.OnlineMeeting;

import android.text.TextUtils;

import com.tojoy.tjoybaselib.configs.OSSConfig;

import java.io.Serializable;

public class ApplyPhotoInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    public String type;

    public String filePath;
    public String absolutePath;
    public long size;
    public boolean choose = false;

    /**
     * 查看详情、重新编辑相关
     */
    public String ossUrl;
    public String name;

    public int indexInfos;

    public String getGileUrl(){
        String glideUrl = "";
        if (!TextUtils.isEmpty(filePath)) {
            glideUrl = filePath;
        } else if (!TextUtils.isEmpty(ossUrl)) {
            glideUrl = OSSConfig.getOSSURLedStr(OSSConfig.getRemoveStylePath((String) ossUrl));
        }
        return glideUrl;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getAbsolutePath() {
        if (TextUtils.isEmpty(absolutePath)) {
            return filePath;
        }
        return absolutePath;
    }

    public boolean isChoose() {
        return choose;
    }

    public void setChoose(boolean choose) {
        this.choose = choose;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public ApplyPhotoInfo setType(String type) {
        this.type = type;
        return this;
    }
}
