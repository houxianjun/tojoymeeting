package com.tojoy.tjoybaselib.model.intentionalorder.makesure;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * Created by daibin
 * on 2020/1/2.
 * function
 */
public class NewOrderMakeOrderResponse extends OMBaseResponse {
    public DataBean data;

    public class DataBean {
        public String orderCode;
        public String orderId;
        public String orderPayPrice;
    }
}
