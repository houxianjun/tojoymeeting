package com.tojoy.tjoybaselib.model.OnlineMeeting;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * @author qll
 * @date 2019/4/19
 * 进入开会申请页面参数
 */
public class ApplyJoinLiveResponse extends OMBaseResponse {

    public ApplyJoinLiveModel data;
}
