package com.tojoy.tjoybaselib.model.user;

public class AddressResponseInfo {
    public String addAll;
    public String districtId;
    public String cityId;
    public String cityName;
    public String districtName;
    public String id;
    public String pkId;
    public String userMobile;
    public String remark;
    public String provinceId;
    public String provinceName;
    public String userName;
    public String state;
    public int isDefault;
}
