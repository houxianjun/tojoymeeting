package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * @author qll
 * @date 2019/4/26
 * 马上开播
 */
public class StartLiveResponse extends OMBaseResponse {
    public StartLiveModel data;
    public class StartLiveModel{
        // 房间-直播id
        public String roomLiveId;
        // 直播间标题
        public String title;
        // 开播时间戳，毫秒
        public long startTime;
        // 状态，直播状态，
        public String status;
    }
}
