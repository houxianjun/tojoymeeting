package com.tojoy.tjoybaselib.model.servicemanager;


import com.tojoy.tjoybaselib.net.OMBaseResponse;


public class RemainTimeResponse extends OMBaseResponse {

    public RemainTimeEntry data;

    public class RemainTimeEntry{
        String remainingTime;
        /**
         * 是否计费 0 免费 1计费
         */
        public int billing;
        /**
         * 服务截止时间说明
         */
        public String serveEndMessage;
        public String getRemainingTime() {
            return remainingTime;
        }

        public int getBilling() {
            return billing;
        }
        public String getServeEndMessage() {
            return serveEndMessage;
        }

    }

    public RemainTimeEntry getData() {
        return data;
    }

    public void setData(RemainTimeEntry data) {
        this.data = data;
    }
}
