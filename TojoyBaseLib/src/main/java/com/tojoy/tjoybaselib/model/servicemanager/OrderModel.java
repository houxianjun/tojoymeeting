package com.tojoy.tjoybaselib.model.servicemanager;

import android.text.TextUtils;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

public class OrderModel extends OMBaseResponse {

    public DataBean data;

    public class DataBean {
        public List<ListBean> list;

        public class ListBean {

            public int corporationCode;
            public String feeName;
            public String id;
            public String orderCode;
            public long orderDate;
            public String orderPrice;
            public int orderStatus;
            public String serviceTime;

            public String getServiceTime() {
                if(TextUtils.isEmpty(serviceTime)) {
                    return "0";
                }
                if(!serviceTime.contains(".")) {
                    return serviceTime;
                }

                if (serviceTime.endsWith("0")) {
                    serviceTime = serviceTime.substring(0, serviceTime.length()-1);
                }
                if (serviceTime.endsWith("0")) {
                    serviceTime = serviceTime.substring(0, serviceTime.length()-1);
                }
                if (serviceTime.endsWith(".")) {
                    serviceTime = serviceTime.substring(0, serviceTime.length()-1);
                }
                if(TextUtils.isEmpty(serviceTime)) {
                    return "0";
                }
                return serviceTime;
            }

        }
    }
}
