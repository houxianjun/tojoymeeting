package com.tojoy.tjoybaselib.model.user;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

public class MyAddressListResponse extends OMBaseResponse {
    public MyAddressListModel data;

    public class MyAddressListModel {
        public ArrayList<AddressResponseInfo> list = new ArrayList<>();
    }
}
