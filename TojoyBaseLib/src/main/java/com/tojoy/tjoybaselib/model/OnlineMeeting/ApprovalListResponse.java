package com.tojoy.tjoybaselib.model.OnlineMeeting;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

/**
 * @author qll
 * @date 2019/4/20
 */
public class ApprovalListResponse extends OMBaseResponse {
    public ApprovalResponse data;

    public static class ApprovalResponse {
        public List<ApprovalModel> list;
        public boolean isLastPage;
    }
}
