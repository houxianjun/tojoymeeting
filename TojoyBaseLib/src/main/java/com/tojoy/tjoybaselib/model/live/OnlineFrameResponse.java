package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

public class OnlineFrameResponse extends OMBaseResponse {
    public OnlineFrame data;

    public class OnlineFrame {
        public String userId;
    }
}
