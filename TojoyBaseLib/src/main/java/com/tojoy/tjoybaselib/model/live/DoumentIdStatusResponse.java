package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * @author qll
 * @date 2019/5/7
 * 获取白板的状态
 */
public class DoumentIdStatusResponse extends OMBaseResponse {
    public DoumentIdStatusModel data;

    public static class DoumentIdStatusModel{
        public String documentId;
        // 1：展示：0不展示
        public String isShow;
        public String liveId;
        // 获取时用到，用于接收上一次控制人的userid
        public String setUserId;
    }

}
