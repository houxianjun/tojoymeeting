package com.tojoy.tjoybaselib.model.OnlineMeeting;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * @author qll
 * @date 2019/4/23
 */
public class CheckUserInfoResponse extends OMBaseResponse {
    public CheckUserInfo data;
}
