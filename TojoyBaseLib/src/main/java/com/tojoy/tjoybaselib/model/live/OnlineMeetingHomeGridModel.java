package com.tojoy.tjoybaselib.model.live;

import android.text.TextUtils;
import java.util.List;

/**
 * @author qll
 * @date 2019/4/9
 */
public class OnlineMeetingHomeGridModel {
    /** 封面图片（竖直）*/
    public String verticalBanner;
    /** 封面图片（横屏）*/
    public String transverseBanner;
    /** 房间号 */
    public String roomId;
    /** 直播间SDK修改时新增房间号 */
    public String roomCode;
    /** 直播间id */
    public String roomLiveId;
    /**  房间名称 */
    public String title;

    /** 用户ID */
    public String userId;
    /** 0：没有密码；1：需要输入密码 */
    public String hasPassword;
    /** 直播间跳转需要 **/
    public String imRoomId;
    public String startDate;
    /** 是否已经被引流 : 1:已使用 2：未使用 */
    public String isUse;

    /** secretary:服务秘书；official:官方；recorder录演师 */
    public String roomType;

    /** 直播状态：1：未开始（预告中）；2：直播中；3：暂停（直播中）4：回放中 */
    public String status;

    /** 是否参与推广 1参与，0不参与 */
    public int ifDistribution;
    /** 本场次会议最大分销额预算 */
    public String maxBudget;
    /** 一级分销员佣金 */
    public String level1Commission;
    /** 二级分销员佣金 */
    public String level2Commission;
    /** 一级员工分销额佣金 */
    public String level1CommissionEmployee;
    /** 二级员工分销额佣金 */
    public String level2CommissionEmployee;
    /** 最少参会人数 */
    public String minPerson;
    /** 最低时长 */
    public String minViewTime;
    /** 此企业的公司码 */
    public String companyCode;

    public List<String> tags;

    /**
     * 热门直播间、路演师列表页面专用
     * @return
     */
    public String getListRoomIdText() {
        return "ID" + roomCode;
    }

    public String getTitle() {
        return TextUtils.isEmpty(title) ? "" : title;
    }

    public String coverOne;

    public String getOutLiveCover() {
        return coverOne;
    }

    public boolean isOfficialLive() {
        return "official".equals(roomType);
    }
}
