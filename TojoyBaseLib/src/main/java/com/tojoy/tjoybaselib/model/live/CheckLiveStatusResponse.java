package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

public class CheckLiveStatusResponse extends OMBaseResponse {

    public CheckLiveStatus data;

    public class CheckLiveStatus {
        public String status;
        public String statusText;
        public String havePassword;//1有密码；0无
        public String userId;//主播id
        public String title;//直播间标题
    }

}
