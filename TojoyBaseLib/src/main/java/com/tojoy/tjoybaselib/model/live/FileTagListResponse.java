package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

public class FileTagListResponse extends OMBaseResponse {
    public ArrayList<FileFolderModel> data = new ArrayList<>();
}
