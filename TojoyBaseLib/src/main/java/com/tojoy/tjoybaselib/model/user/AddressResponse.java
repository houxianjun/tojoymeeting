package com.tojoy.tjoybaselib.model.user;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

/**
 * Created by chengyanfang on 2017/10/9.
 */

public class AddressResponse extends OMBaseResponse {
    public ArrayList<ProvinceModel> data = new ArrayList<>();
}
