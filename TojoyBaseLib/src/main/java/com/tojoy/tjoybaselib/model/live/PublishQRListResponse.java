package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

public class PublishQRListResponse extends OMBaseResponse {
    public PublishQRListBean data;
    public class PublishQRListBean{
        public List<PublishQRListModel> list;
    }
}
