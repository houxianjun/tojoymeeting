package com.tojoy.tjoybaselib.model.home;

import android.text.TextUtils;

import com.tojoy.tjoybaselib.net.BaseResponse;

import java.util.ArrayList;

public class MeetingInviteNoticeResponse extends BaseResponse {

    public String code;
    public String msg;
    public MeetingInviteNoticeModel data;

    @Override
    public boolean isSuccess() {
        return !TextUtils.isEmpty(code) && "1".equals(code);
    }

    public class MeetingInviteNoticeModel {
        public ArrayList<MeetingInviteModel> list = new ArrayList<>();
    }
}
