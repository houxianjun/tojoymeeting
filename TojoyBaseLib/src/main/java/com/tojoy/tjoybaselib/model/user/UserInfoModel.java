package com.tojoy.tjoybaselib.model.user;

/**
 * Created by chengyanfang on 2018/1/3.
 */

import java.util.ArrayList;

/**
 * 平台基础用户Model
 */

public class UserInfoModel {
    public String id;
    public String userName;
    //头像
    public String avatar;

    //---------------- live ---------------------------------
    public String company;
    public String isRemove;
    public String job;
    public String mobile;
    public String liveId;
    public String userId;
    /**
     * 用户类型，0:主播；1:助手；2：成员
     */
    public String userType;

    public RoomLive roomLive;

    public static class RoomLive{

        public ArrayList<Tags> tagList;
        public String imRoomId;
        public String roomLiveId;
        public String title;
        public String roomId;
        public String roomCode;
        public long startDate;
        /**
         * 主播id
         */
        public String userId;
        public String userName;
        public String avatar;
        public String status;
        public String roomType;
        public static class Tags{
            public String tagName;
            public String tagId;

            public Tags(String tagName, String tagId) {
                this.tagName = tagName;
                this.tagId = tagId;
            }
        }

        public String companyCode;

        public String coverOne;

        public String allowViewTotal;//最大参会人
        public String authType;//会议权限类型
    }
    //---------------- live ---------------------------------

    // 是否隐藏分享按钮，0，隐藏 1不隐藏
    public String isHideShare;

    // 是否可以分销：0：不可以；1：可以
    public int ifDistribution;
    // 最大分销金额
    public String maxBudget;

    // 一级分销员佣金
    public String level1Commission;
    // 二级分销员佣金
    public String level2Commission;

    /** 一级员工分销额佣金 */
    public String level1CommissionEmployee;
    /** 二级员工分销额佣金 */
    public String level2CommissionEmployee;
    /** 最少参会人数 */
    public String minPerson;
    /** 最低时长 */
    public String minViewTime;
}
