package com.tojoy.tjoybaselib.model.user;

import android.text.TextUtils;

import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.util.string.StringUtil;

/**
 * Created by chengyanfang on 2018/1/3.
 */

public class UserProfileModel {

    public String id;
    public String mobile;
    public String trueName;
    public String company;
    public String title;
    public String liveId;
    public String information;
    public String prov;
    public String city;

    public String job;

    //头像
    public String avatar;

    public String getAddress() {
        if(!TextUtils.isEmpty(prov) && !TextUtils.isEmpty(city)) {
            return prov + " " + city;
        }else if(TextUtils.isEmpty(prov)) {
            return city;
        }else {
            return prov;
        }
    }

}
