package com.tojoy.tjoybaselib.model.enterprise;

import android.text.TextUtils;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

public class NewGoodsDetailResponse extends OMBaseResponse {

    public DataBean data;

    public class DataBean {
        public String attribute;
        public String content;
        public String salePrice;
        public String createDate;
        public String id;
        public String imgurl;
        public String limitNum;
        public String liveRoomCoverImgUrl;
        public String marketPrice;
        public String name;
        public String stockNum;
        public String subhead;

        //下单新增加
        public String companyCode;

        public String isDelivery;
        public String sellStoreId;
        public String sellStoreImg;
        public String sellStoreName;

        //企业名称
        public String companyName;


        public String isShelf; //0：否、1：是）
        public String checkStatus;//审核状态（0：待提交、1：待审核、2：审核通过、3：审核驳回）

        public int isIntention; //是否感兴趣：1 感兴趣   0 未感兴趣
        public int isDeposit;   //定金：1 需要  0 不需要

        /** 是否参与推广 1参与，0不参与 */
        public int ifDistribution;
        /** 本场次会议最大分销额预算 */
        public String maxBudget;
        /** 一级分销员佣金 */
        public String level1Commission;
        /** 二级分销员佣金 */
        public String level2Commission;
        /** 一级员工分销额佣金 */
        public String level1CommissionEmployee;
        /** 二级员工分销额佣金 */
        public String level2CommissionEmployee;
        /** 最少参会人数 */
        public String minPerson;
        /** 最低时长 */
        public String minViewTime;

        public String getCoverImg() {
            if (!TextUtils.isEmpty(imgurl)) {
                return imgurl;
            }

            if (!TextUtils.isEmpty(liveRoomCoverImgUrl)) {
                return liveRoomCoverImgUrl;
            }

            if (imgList.size() > 0) {
                return imgList.get(0).thumbnailImgUrl;
            }

            return "";
        }

        public List<NewGoodsDetailBannerModel> imgList;


        public boolean isShelf() {
            return "1".equals(isShelf);
        }

        public boolean isChackSuccess() {
            return "2".equals(checkStatus);
        }

        public boolean isSupperSendGoods() {
            boolean isSupperSendGoods;
            if (isIntentOder()) {
                isSupperSendGoods = true;
            } else {
                isSupperSendGoods = "1".equals(isDelivery);
            }
            return isSupperSendGoods;
        }

        public boolean isIntentOder() {
            return TextUtils.isEmpty(sellStoreId) || "null".equals(sellStoreId);
        }

        public String getOderType() {
            if (TextUtils.isEmpty(sellStoreId)) {
                return "2";
            } else {
                return "1";
            }
        }


        public long getStockNum() {
            long num = 1;
            if (isIntentOder()) {
                num = 99999999;
            } else {
                if ("null".equals(limitNum) || TextUtils.isEmpty(limitNum)) {//不作限购 处理
                    if ("null".equals(stockNum) || TextUtils.isEmpty(stockNum)) {//库存也没有
                        num = 1;
                    } else {//有库存
                        try {
                            num = Long.valueOf(stockNum);
                        } catch (Exception e) {
                            num = 1;
                        }
                    }
                } else {//有限购
                    if ("null".equals(stockNum) || TextUtils.isEmpty(stockNum)) {
                        num = 1;
                    } else {
                        long limt, stock;
                        try {
                            limt = Long.valueOf(limitNum);
                        } catch (Exception e) {
                            limt = 1;
                        }
                        try {
                            stock = Long.valueOf(stockNum);
                        } catch (Exception e) {
                            stock = 1;
                        }

                        if (limt > stock) {
                            num = stock;
                        } else if (limt == stock) {
                            num = limt;
                        } else if (limt < stock) {
                            num = limt;
                        }
                    }
                }
                if (num <= 0) {
                    num = 1;
                }
            }
            return num;
        }


        public String getLimtString() {
            String limtString = "";
            if ("null".equals(limitNum) || TextUtils.isEmpty(limitNum)) {//不作限购 处理
                limtString = "商品库存不足";
            } else {//有限购
                if ("null".equals(stockNum) || TextUtils.isEmpty(stockNum)) {
                    limtString = "商品库存不足";
                } else {
                    long limt, stock;
                    try {
                        limt = Long.valueOf(limitNum);
                    } catch (Exception e) {
                        limt = 1;
                    }
                    try {
                        stock = Long.valueOf(stockNum);
                    } catch (Exception e) {
                        stock = 1;
                    }
                    if (limt > stock) {
                        limtString = "商品库存不足";
                    } else if (limt == stock) {
                        limtString = "商品库存不足";
                    } else if (limt < stock) {
                        limtString = "超过限购数量";
                    }
                }
            }
            return limtString;
        }


        public String getSalePrice() {
            if (TextUtils.isEmpty(salePrice) || "null".equals(salePrice)) {
                return "0";
            } else {
                return salePrice;
            }
        }

        public boolean isCanGoMakeOrder() {
            boolean isCan;
            if (TextUtils.isEmpty(stockNum) || "null".equals(stockNum)) {
                isCan = false;
            } else {
                try {
                    isCan = Long.valueOf(stockNum) > 0;
                } catch (Exception e) {
                    isCan = false;
                }

            }
            return isCan;
        }
    }
}
