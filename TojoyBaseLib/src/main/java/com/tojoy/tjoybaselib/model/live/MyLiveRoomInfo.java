package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * Created by luuzhu on 2019/4/18.
 * 我的房间的信息   房间id 直播次数
 */

public class MyLiveRoomInfo extends OMBaseResponse {

    public DataObjBean data;

    public class DataObjBean {
        public long createDate;
        public String roomId;
        public String status;
        public String type;
        public String userId;
    }
}
