package com.tojoy.tjoybaselib.model.live;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

public class SelectOutliveStatusResponse extends OMBaseResponse {
    public ArrayList<SelectOutLiveModel> data = new ArrayList<>();
}
