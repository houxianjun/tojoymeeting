package com.tojoy.tjoybaselib.model.OnlineMeeting;

import android.text.TextUtils;

import com.tojoy.tjoybaselib.util.time.TimeUtility;

import java.io.Serializable;

/**
 * @author qll
 * @date 2019/4/16
 */
public class ApplyResultLeaderInfo implements Serializable {
    public String operateName;
    // 1:未审批；2已驳回；3：已同意；4：已完成；5也显示未审批
    public String status;
    // 审批时间（时间戳）
    public String auditDate;
    // 审批理由
    public String remark;

    public String applyId;
    public String userId;
    // 用户等级：大于等于6则为管理员，不显示领导两个字
    public String level;

    public String getName () {
        String hint = TextUtils.isEmpty(level) ? "领导" : (Integer.valueOf(level) >= 6 ? "" : "领导");
        String result = hint + operateName;
        if ("1".equals(status) || "4".equals(status) || "5".equals(status)) {
            return result + "未审批";
        } else if ("3".equals(status)) {
            return result + "已同意";
        } else if ("2".equals(status)) {
            return result + "已驳回";
        }
        return result;
    }

    public String getAuditDate(){
        if (TextUtils.isEmpty(auditDate)) {
            return "";
        }
        return TimeUtility.getTimeFormat(Long.parseLong(auditDate),"yyyy/MM/dd   HH:mm:ss") + "提交";
    }
}
