package com.tojoy.tjoybaselib.html;

import android.text.TextUtils;

import java.util.ArrayList;

public class HtmlContentProvider {

    public static ArrayList<HtmlItem> getHtmlItems(String content) {
        ArrayList<HtmlItem> contentList;

        contentList = new ArrayList<>();

        content = HTMLSpirit.delHTMLStrongTag(content);
        content = HTMLSpirit.delFontTag(content);
        content = HTMLSpirit.delHideTag(content);
        content = HTMLSpirit.delLinkTag(content);

        ArrayList<HtmlItem> aList = new HtmlParser().getAllHtmlTags(content);

        if (aList.size() > 0) {
            aList.remove(0);
        }

        for (HtmlItem aItem : aList) {
            if (aItem.isImageType()) {
                contentList.add(aItem);
                continue;
            } else if (aItem.isBlackQuote()) {
                continue;
            } else if (aItem.isFaceFontType()) {
                continue;
            } else if (aItem.isAllN()) {
                continue;
            } else {
                aItem.conVert2SpannableString();
                if (!TextUtils.isEmpty(aItem.mText)) {
                    contentList.add(aItem);
                }
                continue;
            }

        }

        return contentList;
    }

}
