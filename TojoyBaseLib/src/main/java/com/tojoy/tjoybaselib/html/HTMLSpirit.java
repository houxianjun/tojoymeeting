package com.tojoy.tjoybaselib.html;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class HTMLSpirit {

    static String delHTMLStrongTag(String htmlStr) {
        String regex = "<strong>|</strong>";
        Pattern p_script = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher m_script = p_script.matcher(htmlStr);
        htmlStr = m_script.replaceAll("");
        return htmlStr.trim();

    }

    static String delFontTag(String htmlStr) {
        String regex = "<font[^>]*?>|</font>";
        Pattern p_script = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher m_script = p_script.matcher(htmlStr);
        htmlStr = m_script.replaceAll("");
        return htmlStr.trim();

    }

    static String delLinkTag(String htmlStr) {
        String regex = "<a[^>]*?>|</a>";
        Pattern p_script = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher m_script = p_script.matcher(htmlStr);
        htmlStr = m_script.replaceAll("");
        return htmlStr.trim();

    }

    static String delHideTag(String htmlStr) {
        String regex = "\\[hide\\]|\\[/hide\\]";
        Pattern p_script = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher m_script = p_script.matcher(htmlStr);
        htmlStr = m_script.replaceAll("");
        return htmlStr.trim();
    }

}
