package com.tojoy.tjoybaselib.html;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.tojoy.tjoybaselib.R;
import com.tojoy.tjoybaselib.util.sys.AppUtils;

import java.util.ArrayList;

public class HtmlContainer {

    private View mView;
    private LinearLayout mContainerllv;

    private Context mContext;

    private ArrayList<HtmlItem> mItems;
    //默认字体大小
    private float fontSize = 14;

    //默认字体颜色
    private int textcolor;
    //父容器宽度
    private int mWidth;


    public HtmlContainer(Context mContext, ArrayList<HtmlItem> items, int width) {
        this.mContext = mContext;
        this.mItems = items;
        this.mWidth = width;
        Log.d(HtmlContainer.class.getName(),"mwidth"+width);
        textcolor = mContext.getResources().getColor(R.color.color_91969e);
    }

    public View getView() {
        if (mView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mView = aLayoutInflater.inflate(R.layout.llv_html_container, null);
            mContainerllv = mView.findViewById(R.id.llv_htmlcontainer);
        }
        initView();
        return mView;
    }

    public void initView() {
        mContainerllv.removeAllViews();

        for (HtmlItem item : mItems) {
            if (item.isImageType()) {
                //图片容器
                RelativeLayout imageContainer = new RelativeLayout(mContext);
                if (mWidth == 0) {
                    mWidth = ViewGroup.LayoutParams.MATCH_PARENT;
                }
                imageContainer.setLayoutParams(new RelativeLayout.LayoutParams(mWidth, ViewGroup.LayoutParams.WRAP_CONTENT));
                mContainerllv.addView(imageContainer);

                //图片
                ImageView imageView = new ImageView(mContext);
                imageView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageView.setAdjustViewBounds(true);
                imageView.setMaxWidth(mWidth);
                imageContainer.addView(imageView);

                RequestOptions requestOptions = new RequestOptions()
                        .disallowHardwareConfig()
                        .dontAnimate();
                Glide.with(mContext).load(item.mText).apply(requestOptions).into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        int imageWidth = resource.getIntrinsicWidth();
                        int imageHeight = resource.getIntrinsicHeight();
                        RelativeLayout.LayoutParams para = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
                        para.addRule(RelativeLayout.CENTER_HORIZONTAL);
                        //图片宽度小于容器宽度，居中显示
                        if (imageWidth < mWidth) {
//                             para.height = imageHeight;
//                             para.width = imageWidth;
                        } else { //宽度铺满，高度等比缩放
                            if (mWidth <= 0) {
                                para.height = imageHeight;
                                para.width = imageWidth;
                            } else {
                                int height = mWidth * imageHeight / imageWidth;
                                para.height = height;
                                para.width = mWidth;
                            }

                        }

                        para.setMargins(0, AppUtils.dip2px(mContext, 0), 0, 0);
                        imageView.setImageDrawable(resource);
                    }
                });
            } else {
                if (!TextUtils.isEmpty(item.getSpannableString().toString().trim())) {
                    TextView textView = new TextView(mContext);
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
                    textView.setText(item.getSpannableString());
                    textView.setLineSpacing(0, 1.3f);
                    textView.setTextColor(textcolor);
                    mContainerllv.addView(textView);
                    LinearLayout.LayoutParams textlps = (LinearLayout.LayoutParams) textView.getLayoutParams();
                    textlps.setMargins(0, AppUtils.dip2px(mContext, 8), 0, 0);
                }
            }
        }
    }
}
