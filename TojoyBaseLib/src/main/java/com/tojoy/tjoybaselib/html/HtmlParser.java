package com.tojoy.tjoybaselib.html;

import org.htmlcleaner.ContentNode;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import java.util.ArrayList;

class HtmlParser {

    private ArrayList<HtmlItem> mAllTags;

    private boolean isHaveQuote;

    HtmlParser() {
        mAllTags = new ArrayList<>();
        isHaveQuote = false;
    }

    ArrayList<HtmlItem> getAllHtmlTags(String aHtml) {

        if (aHtml != null) {
            HtmlCleaner cleaner = new HtmlCleaner();
            TagNode node = cleaner.clean(aHtml);
            node.traverse((tagNode, htmlNode) -> {
                if (htmlNode instanceof TagNode) {
                    TagNode tag = (TagNode) htmlNode;
                    String tagName = tag.getName();

                    if (HtmlTag.TAG_IMG.equalsIgnoreCase(tagName)) {
                        HtmlItem aItem = new HtmlItem(HtmlItem.HtmlType.HtmlTypeImage);
                        aItem.mText = tag.getAttributeByName("src");
                        if (aItem.mText.startsWith("http")) {
                            mAllTags.add(aItem);
                        }
                    } else if (tagName.equalsIgnoreCase(HtmlTag.TAG_A)) {
                        HtmlItem aItem = new HtmlItem(HtmlItem.HtmlType.HtmlTypeText);
                        aItem.mText = tag.getAttributeByName(HtmlTag.TAG_HREF);
                        mAllTags.add(aItem);
                        tag.getAllChildren().clear();
                    } else if (tagName.equalsIgnoreCase(HtmlTag.TAG_BLOCKQUOTE)) {
                        // 引用
                        isHaveQuote = true;
                        HtmlItem aItem = new HtmlItem(HtmlItem.HtmlType.HtmlTypeBLOCKQUOTE);
                        aItem.mText = tag.getText().toString();
                        mAllTags.add(aItem);
                        tag.getAllChildren().clear();
                    } else {
                        if (!isInQuote(tag)) {
                            if (tag.getAllElementsList(true).size() == 0) {
                                HtmlItem aItem = new HtmlItem(HtmlItem.HtmlType.HtmlTypeText);
                                aItem.mText = tag.getText().toString();
                                if (!(aItem.mText.isEmpty() && !mAllTags.isEmpty())) {
                                    mAllTags.add(aItem);
                                }
                                tag.getAllChildren().clear();
                            }
                        }

                    }
                } else if (htmlNode instanceof ContentNode) {
                    ContentNode contentNode = (ContentNode) htmlNode;
                    HtmlItem aItem = new HtmlItem(HtmlItem.HtmlType.HtmlTypeText);
                    aItem.mText = contentNode.getContent();
                    if (!(aItem.mText.isEmpty() && !mAllTags.isEmpty())) {
                        mAllTags.add(aItem);
                    }
                }
                return true;
            });
        }

        return mAllTags;
    }

    private boolean isInQuote(TagNode aNode) {
        if (!isHaveQuote) {
            return false;
        }

        if (aNode == null) {
            return false;
        }

        if (aNode.getParent() == null) {
            return false;
        }

        if (aNode.getParent().getName() == null) {
            return false;
        }

        boolean aIsIn = aNode.getParent().getName().equalsIgnoreCase(HtmlTag.TAG_BLOCKQUOTE);

        if (!aIsIn) {
            return this.isInQuote(aNode.getParent());
        }

        return aIsIn;

    }
}
