package com.tojoy.tjoybaselib.html;

import android.text.SpannableString;

import java.io.Serializable;

public class HtmlItem implements Serializable {

    private static final long serialVersionUID = -1509356710890667149L;

    String mText;

    private SpannableString mSpannableString;

    SpannableString getSpannableString() {
        return mSpannableString;
    }

    void conVert2SpannableString() {
        if (!this.isImageType()) {
            this.mSpannableString = SpannableString.valueOf(this.mText.replace("&nbsp;", " "));
        }
    }

    private HtmlType mHtmlType;

    HtmlItem(HtmlType aHtmlType) {
        this.mHtmlType = aHtmlType;
    }

    boolean isImageType() {
        return mHtmlType == HtmlType.HtmlTypeImage;
    }

    boolean isFaceFontType() {
        mText.trim();
        return (mText.startsWith("\n@font-face") || mText.startsWith("\np.MsoNormal") || mText.startsWith("\nspan.msoIns") || mText.startsWith("\nspan.msoDel")) && mText.endsWith("}");
    }

    boolean isAllN() {
        for (Character c : mText.toCharArray()) {
            if (c != '\n') {
                return false;
            }
        }

        return true;
    }

    // 是否是引用
    boolean isBlackQuote() {
        return mHtmlType == HtmlType.HtmlTypeBLOCKQUOTE;
    }

    public enum HtmlType {
        HtmlTypeText, HtmlTypeImage, HtmlTypeBLOCKQUOTE, // 引用
    }

}
