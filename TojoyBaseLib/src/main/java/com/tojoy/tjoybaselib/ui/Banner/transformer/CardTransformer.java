package com.tojoy.tjoybaselib.ui.Banner.transformer;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * @author qll
 * @date 2019/4/10
 * 网上天洽会banner特殊样式，中间大两边小
 */
public class CardTransformer implements ViewPager.PageTransformer {
    private static final float MIN_SCALE = 0.8f;
    private static final float MIN_ALPHA = 0.8f;


    private static final float MAX_SCALE = 1.0f;
    private static final float MAX_ALPHA = 1.0f;


    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @SuppressLint("NewApi")
    public void transformPage(View view, float position) {
        if (position < -1) {
            // 设置缩放比为0.8,透明度0.8
            view.setScaleY(MIN_SCALE);
            view.setAlpha(MIN_ALPHA);
        } else if (position == 0) {
            view.setScaleY(MAX_SCALE);
            view.setAlpha(MAX_ALPHA);
        } else if (position <= 1) {
            float scaleFactor = MIN_SCALE + (1 - MIN_SCALE) * (1 - Math.abs(position));
            float alphaFactor = MIN_ALPHA + (1 - MIN_ALPHA) * (1 - Math.abs(position));
            view.setScaleY(scaleFactor);
            view.setAlpha(alphaFactor);
        } else {
            view.setScaleY(MIN_SCALE);
            view.setAlpha(MIN_ALPHA);
        }
    }
}

