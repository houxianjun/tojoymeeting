package com.tojoy.tjoybaselib.ui.loadmoreView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.tojoy.tjoybaselib.R;

/**
 * @author chengyanfang
 * @version V 1.0.0
 * @function 列表加载更多控件
 */

public class LoadMoreView {

    private View mView;
    private TextView mTextView;

    @SuppressLint("InflateParams")
    public LoadMoreView(Context context) {
        mView = LayoutInflater.from(context).inflate(
                R.layout.widgt_loadmoreview_layout, null);
        mTextView = mView.findViewById(R.id.loadmore_text);
        mTextView.setText("加载中...");
    }

    public View getView() {
        return mView;
    }

}
