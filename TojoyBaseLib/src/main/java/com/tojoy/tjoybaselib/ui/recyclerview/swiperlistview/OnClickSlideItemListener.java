package com.tojoy.tjoybaselib.ui.recyclerview.swiperlistview;

import android.view.View;

/**
 * Created by chengyanfang on 2017/10/22.
 */

public interface OnClickSlideItemListener {
    void onItemClick(ISlide iSlideView, View v, int position);

    void onClick(ISlide iSlideView, View v, int position);
}
