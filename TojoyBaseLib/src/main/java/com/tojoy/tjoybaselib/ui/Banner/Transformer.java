package com.tojoy.tjoybaselib.ui.Banner;

import android.support.v4.view.ViewPager.PageTransformer;

import com.tojoy.tjoybaselib.ui.Banner.transformer.AccordionTransformer;
import com.tojoy.tjoybaselib.ui.Banner.transformer.BackgroundToForegroundTransformer;
import com.tojoy.tjoybaselib.ui.Banner.transformer.CubeInTransformer;
import com.tojoy.tjoybaselib.ui.Banner.transformer.CubeOutTransformer;
import com.tojoy.tjoybaselib.ui.Banner.transformer.DefaultTransformer;
import com.tojoy.tjoybaselib.ui.Banner.transformer.DepthPageTransformer;
import com.tojoy.tjoybaselib.ui.Banner.transformer.FlipHorizontalTransformer;
import com.tojoy.tjoybaselib.ui.Banner.transformer.FlipVerticalTransformer;
import com.tojoy.tjoybaselib.ui.Banner.transformer.ForegroundToBackgroundTransformer;
import com.tojoy.tjoybaselib.ui.Banner.transformer.RotateDownTransformer;
import com.tojoy.tjoybaselib.ui.Banner.transformer.RotateUpTransformer;
import com.tojoy.tjoybaselib.ui.Banner.transformer.ScaleInOutTransformer;
import com.tojoy.tjoybaselib.ui.Banner.transformer.StackTransformer;
import com.tojoy.tjoybaselib.ui.Banner.transformer.TabletTransformer;
import com.tojoy.tjoybaselib.ui.Banner.transformer.ZoomInTransformer;
import com.tojoy.tjoybaselib.ui.Banner.transformer.ZoomOutSlideTransformer;
import com.tojoy.tjoybaselib.ui.Banner.transformer.ZoomOutTranformer;


public class Transformer {
    public static Class<? extends PageTransformer> Default = DefaultTransformer.class;
    public static Class<? extends PageTransformer> Accordion = AccordionTransformer.class;
    public static Class<? extends PageTransformer> BackgroundToForeground = BackgroundToForegroundTransformer.class;
    public static Class<? extends PageTransformer> ForegroundToBackground = ForegroundToBackgroundTransformer.class;
    public static Class<? extends PageTransformer> CubeIn = CubeInTransformer.class;
    public static Class<? extends PageTransformer> CubeOut = CubeOutTransformer.class;
    public static Class<? extends PageTransformer> DepthPage = DepthPageTransformer.class;
    public static Class<? extends PageTransformer> FlipHorizontal = FlipHorizontalTransformer.class;
    public static Class<? extends PageTransformer> FlipVertical = FlipVerticalTransformer.class;
    public static Class<? extends PageTransformer> RotateDown = RotateDownTransformer.class;
    public static Class<? extends PageTransformer> RotateUp = RotateUpTransformer.class;
    public static Class<? extends PageTransformer> ScaleInOut = ScaleInOutTransformer.class;
    public static Class<? extends PageTransformer> Stack = StackTransformer.class;
    public static Class<? extends PageTransformer> Tablet = TabletTransformer.class;
    public static Class<? extends PageTransformer> ZoomIn = ZoomInTransformer.class;
    public static Class<? extends PageTransformer> ZoomOut = ZoomOutTranformer.class;
    public static Class<? extends PageTransformer> ZoomOutSlide = ZoomOutSlideTransformer.class;
}
