package com.tojoy.tjoybaselib.ui.tjrecyclerview;

import android.view.View;

/**
 * Created by chengyanfang on 2017/9/7.
 *
 * 用于recyclerview的header和footer的view fix info
 */

public class FixedViewInfo {

    /**
     * 不完美解决方法：添加一个header，则从-2开始减1
     * header:-2~-98
     */
    static final int ITEM_VIEW_TYPE_HEADER_START = -2;
    /**
     * 不完美解决方法：添加一个header，则从-99开始减1
     * footer:-99~-无穷
     */
    static final int ITEM_VIEW_TYPE_FOOTER_START = -99;


    /**
     * The view to add to the list
     */
    public final View view;
    /**
     * 因为onCreateViewHolder不包含位置信息，所以itemViewType需要包含位置信息
     * <p>
     * 位置信息方法：将位置添加到高位
     */
    final int itemViewType;

    FixedViewInfo(View view, int itemViewType) {
        this.view = view;
        this.itemViewType = itemViewType;
    }

}
