package com.tojoy.tjoybaselib.ui.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tojoy.tjoybaselib.R;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.KeyboardControlUtil;

public class BaseSearchAct extends UI {

    boolean isPreSearching;

    protected RelativeLayout mSearchLayout;

    protected RelativeLayout mPreseachLayout;

    protected EditText mSeachEditView;

    protected RelativeLayout mCancleLayout;

    protected TextView mCancleTitle;

    protected ViewPager mViewPager;

    FragmentManager mFragmentManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentid();
        setStatusBar();
        setSwipeBackEnable(false);
        initUI();
        initData();
        mFragmentManager = getSupportFragmentManager();
    }


    protected void setContentid() {
        setContentView(R.layout.activity_search_layout);
    }

    @SuppressLint("ResourceAsColor")
    protected void initUI() {
        setUpNavigationBar();
        mSearchLayout = (RelativeLayout) findViewById(R.id.rtl_search_title);
        mPreseachLayout = (RelativeLayout) findViewById(R.id.rlv_presearch);
        mSeachEditView = (EditText) findViewById(R.id.basequery);
        mCancleLayout = (RelativeLayout) findViewById(R.id.rlv_cancle);
        mCancleTitle = (TextView) findViewById(R.id.cancle_title);
        mViewPager = (ViewPager) findViewById(R.id.search_result_fragment);

        mSearchLayout.setVisibility(View.VISIBLE);
        int statusBarHeight = AppUtils.getStatusBarHeight(this);
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.dip2px(this, 48));
        lps.setMargins(0, statusBarHeight, 0, 0);
        mSearchLayout.setLayoutParams(lps);

        mCancleTitle.setTextColor(R.color.color_6c7f9f);
        mCancleLayout.setOnClickListener(v -> {
            if (getString(R.string.cancel).equals(mCancleTitle.getText().toString())) {
                stopSearch();
            } else {
                if (!TextUtils.isEmpty(mSeachEditView.getText().toString()) && mSeachEditView.getText().toString().trim().length() > 0){
                    doSearch();
                } else {
                    Toast.makeText(BaseSearchAct.this, "请输入有效的内容", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mPreseachLayout.setOnClickListener(v -> preSearch());

        mSeachEditView.setOnEditorActionListener((v, actionId, event) -> {
            //判断是否是“放大镜”键【简称搜索键】
            if(actionId == EditorInfo.IME_ACTION_SEARCH){
                //隐藏软键盘
                //对应逻辑操作
                if (mSeachEditView.getText().toString().length() > 0) {
                    if (mSeachEditView.getText().toString().trim().length() > 0){
                        showKeyboard(false);
                        doSearch();
                    } else {
                        Toast.makeText(BaseSearchAct.this, "请输入有效的内容", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(BaseSearchAct.this, "请输入搜索内容", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
            return false;
        });

        mSeachEditView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(s.toString())) {
                    mCancleTitle.setText(getString(R.string.cancel));
                } else {
                    mCancleTitle.setText(getString(R.string.search));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mHandler.postDelayed(() -> showSoftInputFromWindow(mSeachEditView), 400);
    }


    public void showSoftInputFromWindow(EditText editText) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        InputMethodManager inputManager =
                (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(editText, 0);
    }

    private void initData() {
        getHotsearchKey();
        getHistoryKey();
    }

    protected void getHotsearchKey() {

    }

    protected void getHistoryKey() {

    }

    protected void preSearch() {
        mPreseachLayout.setVisibility(View.GONE);
        mSeachEditView.setVisibility(View.VISIBLE);
        mCancleLayout.setVisibility(View.VISIBLE);
        mSeachEditView.requestFocus();
        isPreSearching = true;
    }

    protected void stopSearch() {
        showKeyboard(false);
        mPreseachLayout.setVisibility(View.VISIBLE);
        mSeachEditView.setVisibility(View.GONE);
        mCancleLayout.setVisibility(View.GONE);

        mViewPager.setVisibility(View.GONE);
        mSeachEditView.setText("");
        isPreSearching = false;
    }

    protected void setSearchHint(String searchHint) {
        mSeachEditView.setHint(searchHint);
    }

    protected void doSearch() {
        mViewPager.setVisibility(View.VISIBLE);
        showKeyboard(false);
        mCancleTitle.setText(getString(R.string.cancel));
    }

    @Override
    protected void inFromLeftOutToRight() {
        overridePendingTransition(0, R.anim.alpha_out);
    }

    @Override
    public void onBackPressed() {
        if (isPreSearching) {
            stopSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()) {
            KeyboardControlUtil.closeKeyboard(mSeachEditView, this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        showKeyboard(false);
    }
}

