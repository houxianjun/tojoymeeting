package com.tojoy.tjoybaselib.ui.recyclerview.swiperlistview;

import android.view.View;

/**
 * Created by chengyanfang on 2017/10/22.
 */

public interface SlideStateListener<T extends ISlide> {
    void onOpened(T v);

    void onClosed(T v);

    void beforeOpen(T v);

    void clamping(T v);

    void onForegroundViewClick(T slideView, View v);
}
