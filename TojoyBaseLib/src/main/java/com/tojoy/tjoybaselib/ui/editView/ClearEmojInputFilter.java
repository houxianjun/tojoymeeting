package com.tojoy.tjoybaselib.ui.editView;

import android.text.InputFilter;
import android.text.Spanned;
import android.widget.Toast;

import com.tojoy.tjoybaselib.BaseLibKit;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;

public class ClearEmojInputFilter implements InputFilter {
    private Pattern emoji = Pattern.compile("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]",
            Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
    private Pattern pattern = Pattern.compile("[^a-zA-Z0-9\\u4E00-\\u9FA5_,.?!:;…~_\\-\"\"/@*+'()<>{}/[/]()<>{}\\[\\]=%&$|\\/♀♂#¥£¢€\"^` ，。？！：；……～“”、“（）”、（——）‘’＠‘·’＆＊＃《》￥《〈〉》〈＄〉［］￡［］｛｝｛｝￠【】【】％〖〗〖〗／〔〕〔〕＼『』『』＾「」「」｜﹁﹂｀．]");

    @Override
    public CharSequence filter(CharSequence source, int i, int i1, Spanned spanned, int i2, int i3) {
        Matcher emojiMatcher = emoji.matcher(source);
        if (emojiMatcher.find()) {
            Toasty.normal(BaseLibKit.getContext(), "暂不支持输入表情").show();
            return "";
        }
        // 增加过滤颜文字
        Matcher matcher=  pattern.matcher(source);
        if(matcher.find()){
            if(!source.equals("\n")) {
                Toast.makeText(BaseLibKit.getContext(), "非法字符！", Toast.LENGTH_SHORT).show();
            }

            return "";
        }
        return null;
    }
}
