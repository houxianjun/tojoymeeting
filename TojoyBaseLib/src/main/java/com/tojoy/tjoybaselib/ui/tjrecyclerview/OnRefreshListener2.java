package com.tojoy.tjoybaselib.ui.tjrecyclerview;

/**
 * Created by chengyanfang on 2017/9/7.
 */

public interface OnRefreshListener2 {
    void onRefresh();
    void onLoadMore();
}
