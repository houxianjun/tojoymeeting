package com.tojoy.tjoybaselib.ui.adapter;

public interface IScrollStateListener {

    /**
     * move to scrap heap
     */
    void reclaim();


    /**
     * on idle
     */
    void onImmutable();
}
