package com.tojoy.tjoybaselib.ui.editView;

import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.tojoy.tjoybaselib.BaseLibKit;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;

public class LRNotiveEmojInputFilter implements InputFilter {
    private Pattern emoji = Pattern.compile("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]",
            Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
    private Pattern pattern = Pattern.compile("[^a-zA-Z0-9\\u4E00-\\u9FA5_,.?!:;…~_\\-\"\"/@*+'()<>{}/[/]()<>{}\\[\\]=%&$|\\/♀♂#¥£¢€\"^` ，。？！：；……～“”、“（）”、（——）‘’＠‘·’＆＊＃《》￥《〈〉》〈＄〉［］￡［］｛｝｛｝￠【】【】％〖〗〖〗／〔〕〔〕＼『』『』＾「」「」｜﹁﹂｀．]");
    private ArrayList<String> canShowEmoj;

    public LRNotiveEmojInputFilter() {
        canShowEmoj = new ArrayList<>();
        canShowEmoj.add("ude00");
        canShowEmoj.add("ude0a");
        canShowEmoj.add("ude18");
        canShowEmoj.add("ude42");
        canShowEmoj.add("ude1b");
        canShowEmoj.add("udd11");
        canShowEmoj.add("ude0e");
        canShowEmoj.add("udd17");
        canShowEmoj.add("ude4c");
        canShowEmoj.add("udc4f");
        canShowEmoj.add("udc4d");
        canShowEmoj.add("udc50");
        canShowEmoj.add("udc46");
        canShowEmoj.add("udc47");
        canShowEmoj.add("udc48");
        canShowEmoj.add("udc49");
        canShowEmoj.add("udd95");
        canShowEmoj.add("udc4c");
        canShowEmoj.add("udcaa");
        canShowEmoj.add("ude4f");
        canShowEmoj.add("ude2c");
        canShowEmoj.add("ude01");
        canShowEmoj.add("ude0c");
        canShowEmoj.add("udf39");
        canShowEmoj.add("udc84");
        canShowEmoj.add("udc8d");
        canShowEmoj.add("udc51");
        canShowEmoj.add("udc8e");
        canShowEmoj.add("udc96");
        canShowEmoj.add("u270a");
        canShowEmoj.add("u270c");
        canShowEmoj.add("u270d");
        canShowEmoj.add("u261d");
        canShowEmoj.add("ude0b");
        canShowEmoj.add("ude1a");
        canShowEmoj.add("ude33");
        canShowEmoj.add("udf1e");
        canShowEmoj.add("ude03");
    }

    @Override
    public CharSequence filter(CharSequence source, int i, int i1, Spanned spanned, int i2, int i3) {

        String cStr = "";

        if(!TextUtils.isEmpty(source)) {
            int c = source.charAt(source.length() - 1);
            if (c > 255) {
                cStr += "\\u" + Integer.toHexString(c);
            } else {
                cStr += String.valueOf(source.charAt(source.length() - 1));
            }

            if(!TextUtils.isEmpty(cStr) && cStr.contains("ud83d")) {
                source.subSequence(0, source.length() - 2);
                Toasty.normal(BaseLibKit.getContext(), "字数超过限制").show();
                return "";
            }

            Log.e("TAG1", "cStr = " + cStr);
        }


        Matcher emojiMatcher = emoji.matcher(source);
        if (emojiMatcher.find()) {

            int length = spanned.length();
            if(length >= 49) {
                Toasty.normal(BaseLibKit.getContext(), "字数超过限制").show();
                return "";
            }

            StringBuilder str = new StringBuilder();
            for (int p = 0; p < source.length(); p++) {
                int ch = (int) source.charAt(p);
                if (ch > 255) {
                    str.append("\\u").append(Integer.toHexString(ch));
                } else {
                    str.append(String.valueOf(source.charAt(p)));
                }
            }

            for(int k = 0; k < canShowEmoj.size(); k++) {

                Log.e("TAG", "source.toString() = " + str);
                Log.e("TAG", "canShowEmoj.get(k) = " + canShowEmoj.get(k));

                if(str.toString().contains(canShowEmoj.get(k))) {
                    return null;
                }
            }
            Toasty.normal(BaseLibKit.getContext(), "暂不支持输入该表情").show();
            return "";
        }
        // 增加过滤颜文字
        Matcher matcher=  pattern.matcher(source);
        if(matcher.find()){

            StringBuilder str = new StringBuilder();
            for (int p = 0; p < source.length(); p++) {
                int ch = (int) source.charAt(p);
                if (ch > 255) {
                    str.append("\\u").append(Integer.toHexString(ch));
                } else {
                    str.append(String.valueOf(source.charAt(p)));
                }
            }

            for(int k = 0; k < canShowEmoj.size(); k++) {

                Log.e("TAG", "source.toString() = " + str);
                Log.e("TAG", "pattern.get(k) = " + canShowEmoj.get(k));

                if(str.toString().contains(canShowEmoj.get(k))) {

                    if(i2 >= 49) {
                        Toasty.normal(BaseLibKit.getContext(), "字数超过限制").show();
                        return "";
                    }
                    return null;
                }
            }

            Toast.makeText(BaseLibKit.getContext(), "暂不支持输入该表情", Toast.LENGTH_SHORT).show();
            return "";
        }
        return null;
    }

}
