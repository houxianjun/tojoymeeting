package com.tojoy.tjoybaselib.ui.activity;

import android.Manifest;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.tojoy.tjoybaselib.services.permission.MPermission;


/**
 * Created by chengyanfang on 2017/12/5.
 */

public class BasePermissionAct extends FragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } catch (Exception e) {
        }
    }

    /**
     * //////////////////////////////
     * 1.Request Permissions
     * //////////////////////////////
     */
    //访问相机
    protected final String[] PREMISSIONS_TAKE_PHOTO = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
    };

    //内存的读写权限
    protected final String[] PREMISSIONS_EXTERNAL_STORAGE = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE
    };

    //只有内存的读写权限
    protected final String[] PREMISSIONS_EXTERNAL_STORAGE_ONLY = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
    };


    //访问通讯录
    protected final String[] PREMISSIONS_GET_ACCOUNTS = new String[]{
            Manifest.permission.GET_ACCOUNTS,
            Manifest.permission.READ_CONTACTS
    };


    //拨打电话
    protected final String[] PERMISSIONS_MAKECALL = new String[]{
            Manifest.permission.CALL_PHONE,
    };


    //访问定位
    protected final String[] PERMISSIONS_LOCATION = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
    };


    //访问录音机
    protected final String[] PERMISSIONS_RECORD = new String[]{
            Manifest.permission.RECORD_AUDIO
    };


    //开启音视频
    protected final String[] PERMISSIONS_AVCHAT = new String[]{
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
    };

    //获取手机信息权限
    protected final String[] PERMISSIONS_WEIHOU = new String[]{
            Manifest.permission.READ_PHONE_STATE
    };


    /**
     * //////////////////////////////
     * 2.Request Code
     * //////////////////////////////
     */
    //访问相机
    protected final int PREMISSIONS_TAKE_PHOTO_REQUEST_CODE = 2000;

    //访问通讯录
    protected final int PREMISSIONS_CONTACTS_REQUEST_CODE = 2001;

    //拨打电话
    protected final int PERMISSIONS_MAKECALL_REQUEST_CODE = 2002;

    //录音
    protected final int PERMISSIONS_RECORD_REQUEST_CODE = 2004;

    //音视频
    protected final int PERMISSIONS_AVCHAT_REQUEST_CODE = 2005;

    //读写权限
    protected final int PERMISSIONS_EXTERNAL_STORAGE_REQUEST_CODE = 2006;

    //只有读写权限
    protected final int PERMISSIONS_EXTERNAL_STORAGE_REQUEST_CODE_ONLY = 2019;


    /**
     * //////////////////////////////
     * 3.Start Check Permission
     * //////////////////////////////
     */
    protected void checkPersimiss(String[] permissions, int requestCode) {
        MPermission.with(this)
                .setRequestCode(requestCode)
                .permissions(permissions)
                .request();
    }


    /**
     * //////////////////////////////
     * 4.CallBack
     * //////////////////////////////
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        MPermission.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }
}
