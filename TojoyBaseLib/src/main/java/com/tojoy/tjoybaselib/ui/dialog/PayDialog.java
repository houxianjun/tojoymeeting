package com.tojoy.tjoybaselib.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tojoy.tjoybaselib.R;
import com.tojoy.tjoybaselib.ui.CountDownView.CountDownTextView;
import com.tojoy.tjoybaselib.util.sys.AppUtils;

import java.util.concurrent.TimeUnit;

public class PayDialog {
    private Context mContext;
    private View mView;
    private Dialog alertDialog;
    private CountDownTextView countdownView;

    public PayDialog(Context context) {
        mContext = context;
        initUI();
    }

    /**
     * 判断当前绑定的activity是否存在
     * @return
     */
    public  boolean isLiving() {
        Activity activity = (Activity) this.mContext;
        if (activity == null) {
            return false;
        }

        return !activity.isFinishing();
    }

    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.pop_pay_layout, null);
        countdownView = mView.findViewById(R.id.countdownView);
        countdownView
                .setCountDownText("您的订单在", "内未支付，将被取消，请尽快支付～")
                .setCloseKeepCountDown(false)
                .setCountDownClickable(false)
                .setShowFormatTime(true)
                .setFormatContent(false)
                .setIntervalUnit(TimeUnit.SECONDS)
                .setOnCountDownFinishListener(() -> dismiss());
    }


    public void initEvent(View.OnClickListener onSureClickListener, View.OnClickListener onCancleClickListener) {
        mView.findViewById(R.id.rlv_sure).setOnClickListener(v -> {
            if (onSureClickListener != null) {
                onSureClickListener.onClick(v);
            }
            alertDialog.dismiss();
        });
        mView.findViewById(R.id.rlv_cancle).setOnClickListener(v -> {
            if (onCancleClickListener != null) {
                onCancleClickListener.onClick(v);
            }
            countdownView.onDestroy();
            alertDialog.dismiss();
        });
    }


    public void show() {
        if (mView.getParent() != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            parent.removeAllViews();
        }

        alertDialog = new Dialog(mContext, R.style.tjoy_dialog_style);
        alertDialog.setContentView(mView, new ViewGroup.LayoutParams(AppUtils.getWidth(mContext) - AppUtils.dip2px(mContext, 120), ViewGroup.LayoutParams.WRAP_CONTENT));
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        alertDialog.setOnKeyListener((dialog, keyCode, event) -> true);
    }


    /**
     * ****************** UI定制化
     */
    public void setCountDownTime(long timeStamp) {
        countdownView.startCountDown(timeStamp);
    }


    /**
     * 判断是否在展示
     */
    public boolean isShowing() {
        if (alertDialog == null) {
            return false;
        }
        return alertDialog.isShowing();
    }

    public void dismiss() {
        if (isShowing()) {
            alertDialog.dismiss();
        }
    }


}
