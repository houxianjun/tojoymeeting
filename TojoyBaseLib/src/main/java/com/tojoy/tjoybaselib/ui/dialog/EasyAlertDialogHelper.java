package com.tojoy.tjoybaselib.ui.dialog;

import android.content.Context;
import android.text.TextUtils;
import android.view.View.OnClickListener;

public class EasyAlertDialogHelper {

    public static EasyAlertDialog createOkCancelDiolag(Context context, CharSequence title, CharSequence message,
                                                       boolean cancelable, final OnDialogActionListener listener) {
        return createOkCancelDiolag(context, title, message, null, null, cancelable, listener);
    }

    /**
     * 两个按钮的dialog
     */
    public static EasyAlertDialog createOkCancelDiolag(Context context, CharSequence title, CharSequence message,
                                                       CharSequence okStr, CharSequence cancelStr, boolean cancelable, final OnDialogActionListener listener) {
        final EasyAlertDialog dialog = new EasyAlertDialog(context);
        OnClickListener okListener = v -> {
            dialog.dismiss();
            listener.doOkAction();
        };
        OnClickListener cancelListener = v -> {
            dialog.dismiss();
            listener.doCancelAction();
        };
        if (TextUtils.isEmpty(title)) {
            dialog.setTitleVisible(false);
        } else {
            dialog.setTitle(title);
        }
        if (TextUtils.isEmpty(message)) {
            dialog.setMessageVisible(false);
        } else {
            dialog.setMessage(message);
        }
        dialog.addPositiveButton(okStr, okListener);
        dialog.addNegativeButton(cancelStr, cancelListener);
        dialog.setCancelable(cancelable);
        return dialog;
    }

    public interface OnDialogActionListener {
        void doCancelAction();

        void doOkAction();
    }
}
