package com.tojoy.tjoybaselib.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.tojoy.tjoybaselib.ui.loadmoreView.LoadMoreView;

import java.util.ArrayList;

/**
 * Created by chengyanfang on 2017/10/10.
 */

public abstract class BaseAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected BaseAdapter() {
    }

    public interface RecyclerAdapterListener {
        void OnLoadMore();
    }

    public interface RecyclerRefreshAdapterListener {
        void OnRefreshMore();
    }

    public interface OnRecyclerViewScroll {
        void onScroll(int positon);
    }

    protected Context mContext;
    protected ArrayList<T> mDatas = new ArrayList<T>();
    private LayoutInflater mInflater;
    private View mHeaderView;
    private View mFooterView;
    private LoadMoreView mLoadMoreView;
    private boolean mCanLoadMore;
    private boolean mRefreshMore;
    private RecyclerAdapterListener mListener;
    private RecyclerRefreshAdapterListener mRListener;
    private OnRecyclerViewScroll mOnRecyclerViewScroll;
    private static final int HEADER = -99;
    private static final int FOOTER = -1;

    public BaseAdapter(Context context, ArrayList<T> datas) {
        mContext = context;
        mDatas = datas;
        mInflater = LayoutInflater.from(context);
        mLoadMoreView = new LoadMoreView(context);
    }

    public void setData(ArrayList<T> datas) {
        mDatas = datas;
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (mHeaderView != null) {
            count++;
        }
        if (mFooterView != null) {
            count++;
        }
        if (mCanLoadMore) {
            count++;
        }
        return mDatas.size() + count;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        int viewType = getItemViewType(position);

        if (viewType == FOOTER) {
            if (mCanLoadMore && mListener != null) {
                mListener.OnLoadMore();
            }
        } else if (viewType == HEADER) {

        } else {
            onBindContentViewHolder(viewHolder, position - (mHeaderView == null ? 0 : 1), viewType);

            if (mOnRecyclerViewScroll != null) {
                mOnRecyclerViewScroll.onScroll(position);
            }

            if (mRefreshMore && this.mRListener != null && position == 0) {
                mRefreshMore = false;
                mRListener.OnRefreshMore();
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View convertView;
        if (viewType == HEADER) {
            return new RecyclerView.ViewHolder(mHeaderView) {
            };
        } else if (viewType == FOOTER) {
            if (mFooterView != null) {
                return new RecyclerView.ViewHolder(mFooterView) {
                };
            }
            return new RecyclerView.ViewHolder(mLoadMoreView.getView()) {
            };
        } else {
            convertView = mInflater.inflate(getResourceId(viewType), viewGroup, false);
            return onCreateItemHolderViewHolder(convertView, viewType);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && mHeaderView != null) {
            return HEADER;
        }
        if (position == getItemCount() - 1) {
            if (mCanLoadMore || mFooterView != null) {
                return FOOTER;
            }
        }
        return getContentViewType(mHeaderView != null ? position - 1 : position);
    }

    protected abstract int getContentViewType(int position);

    protected abstract int getResourceId(int viewType);

    protected abstract RecyclerView.ViewHolder onCreateItemHolderViewHolder(View arg0, int viewType);

    protected abstract void onBindContentViewHolder(RecyclerView.ViewHolder viewHolder, int position, int viewType);

    public void setFooterView(View footerView) {
        mFooterView = footerView;
    }

}