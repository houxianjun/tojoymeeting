package com.tojoy.tjoybaselib.ui.TJPickerView.listener;

/**
 * Created by chengyanfang on 2017/10/9.
 */

public interface OnItemSelectedListener {
    void onItemSelected(int index);
}
