package com.tojoy.tjoybaselib.ui.recyclerview.util;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hzxuwen on 2017/1/13.
 */

public class RecyclerViewUtil {

    public static void changeItemAnimation(RecyclerView recyclerView, boolean isOpen) {
        // 关闭viewholder动画效果。解决viewholder闪烁问题
        RecyclerView.ItemAnimator animator = recyclerView.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(isOpen);
        }
    }


    //存放item宽或高
    private static Map<Integer, Integer> mMapList = new HashMap<>();
    private static int iposition;

    public static int getScrollY(RecyclerView recyclerView) {
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        int itemWH = 0;
        int itemTR = 0;
        int distance = 0;
        int position = linearLayoutManager.findFirstVisibleItemPosition();
        View firstVisiableChildView = linearLayoutManager.findViewByPosition(position);
        itemWH = firstVisiableChildView.getHeight();

        //一层判断mMapList是否为空，若不为空则根据键判断保证不会重复存入
        if (mMapList.size() == 0) {
            mMapList.put(position, itemWH);
        } else {
            if (!mMapList.containsKey(position)) {
                mMapList.put(position, itemWH);
            }
        }
        itemTR = firstVisiableChildView.getTop();

        //position为动态获取，目前屏幕Item位置
        for (int i = 0; i < position; i++) {
            //iposition移出屏幕的距离

            Integer indexPos = mMapList.get(i);
            if (indexPos != null) {
                iposition = iposition + indexPos;
            }
        }
        //根据类型拿iposition减未移出Item部分距离，最后得出滑动距离
        distance = iposition - itemTR;
        //归零
        iposition = 0;
        return distance;
    }

    /**
     * 判断是否滑动到了底部
     *
     * @param recyclerView
     * @return
     */
    public static boolean isScrollToBottom(RecyclerView recyclerView) {

        if (recyclerView == null) return false;

        RecyclerView.LayoutManager layoutManager;
        int lastVisibleItemPosition;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            layoutManager = recyclerView.getLayoutManager();
            lastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
        } else {
            layoutManager = recyclerView.getLayoutManager();
            int[] lastPositions = new int[((StaggeredGridLayoutManager) layoutManager).getSpanCount()];
            ((StaggeredGridLayoutManager) layoutManager).findLastVisibleItemPositions(lastPositions);
            lastVisibleItemPosition = findMax(lastPositions);
        }

        //当前屏幕所看到的子项个数
        int visibleItemCount = layoutManager.getChildCount();
        //当前RecyclerView的所有子项个数
        int totalItemCount = layoutManager.getItemCount();

        if (totalItemCount <= 1) {
            return false;
        }
        //RecyclerView的滑动状态
        int state = recyclerView.getScrollState();
        if (visibleItemCount > 0 && lastVisibleItemPosition == totalItemCount - 1) {
            return true;
        } else {
            return false;
        }
    }

    private static int findMax(int[] lastPositions) {
        int max = lastPositions[0];
        for (int value : lastPositions) {
            if (value > max) {
                max = value;
            }
        }
        return max;
    }
}
