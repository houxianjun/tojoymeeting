package com.tojoy.tjoybaselib.ui.indicator;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.tojoy.tjoybaselib.R;

/**
 * 作者：jiangguoqiu on 2020/2/28 12:55
 */
public class CoustomIndicator extends View {
    private String tag = getClass().getSimpleName();

    private int indicationSize = -1;
    private int curIndex = -1;
    private Paint mPaint;

    private int radius = 0;
    private int bigColor = Color.GRAY;
    private int smallColor = Color.BLUE;

    public CoustomIndicator(Context context) {
        super(context);
        init(null);
    }

    public CoustomIndicator(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CoustomIndicator(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {

        mPaint = new Paint();

        if (attrs != null) {
            TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.CoustomIndicator);
            radius = ta.getDimensionPixelOffset(R.styleable.CoustomIndicator_coustom_indicator_radius, 0);
            bigColor = ta.getColor(R.styleable.CoustomIndicator_coustom_indicator_big_color, Color.GRAY);
            smallColor = ta.getColor(R.styleable.CoustomIndicator_coustom_indicator_small_color, Color.BLUE);
        }

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = canvas.getWidth();
        int hight = canvas.getHeight();
        Log.e(tag, "-----width------" + width);
        Log.e(tag, "-----hight------" + hight);
        //画圆角矩形
        mPaint.setStyle(Paint.Style.FILL);//充满
        mPaint.setColor(bigColor);
        mPaint.setAntiAlias(true);// 设置画笔的锯齿效果
        RectF oval3 = new RectF(0, 0, width, hight);// 设置个新的长方形
        canvas.drawRoundRect(oval3
                , radius
                , radius
                , mPaint);//第二个参数是x半径，第三个参数是y半径
        if (indicationSize == -1) {
            return;
        }
        if (curIndex == -1) {
            return;
        }
        int itemW = width / indicationSize;
        int index = curIndex ;

        mPaint.setColor(smallColor);
        int left = index * itemW;
        Log.e(tag, " index " + index + " left " + left);
        RectF rectF = new RectF(left, 0, left + itemW, hight);
        canvas.drawRoundRect(rectF
                , radius
                , radius
                , mPaint);//第二个参数是x半径，第三个参数是y半径

    }

    public void setMaxSize(int size) {
        if (size!=0) {
            indicationSize = size;
        }
    }

    public void setCurIndex(int index) {
        curIndex = index;
        postInvalidate();
    }


}