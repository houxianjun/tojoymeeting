package com.tojoy.tjoybaselib.ui.TJPickerView.listener;

/**
 * @author qll
 * tab监听事件
 */
public interface OnTabSelectListener {
    void onTabSelect(int position);
    void onTabReselect(int position);
}