package com.tojoy.tjoybaselib.ui.activity;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.R;
import com.tojoy.tjoybaselib.ui.fragment.TFragment;
import com.tojoy.tjoybaselib.ui.hud.ProgressHUD;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.StatusbarUtils;

import java.util.ArrayList;
import java.util.List;

public abstract class UI extends BaseAssembledAct {

    protected final int BASIC_PERMISSION_REQUEST_CODE = 110;

    /**
     * 处理分页请求
     */
    protected int mPage = 1;

    /**
     * 设置状态栏
     */
    protected void setStatusBar() {
        StatusbarUtils.enableTranslucentStatusbar(this);
        StatusbarUtils.setStatusBarDarkMode(this, StatusbarUtils.statusbarlightmode(this));
    }

    /**
     * 设置状态栏不沉浸深色主题
     */
    public void setStatusBarDarkMode() {
        StatusbarUtils.setStatusBarDarkMode(this, StatusbarUtils.statusbarlightmode(this));
    }

    /**
     * 设置状态栏不沉浸深色主题
     */
    protected void setStatusBarLightMode() {
        StatusbarUtils.enableTranslucentStatusbar(this);
        StatusbarUtils.setStatusBarLightMode(this, StatusbarUtils.statusbarlightmode(this));
    }


    /**
     * NavigationBar UI
     */
    protected View mBottomLine;
    protected ImageView mLeftArrow;
    protected ImageView mRightManagerIv;
    protected TextView mRightManagerTv;
    protected RelativeLayout mNormalTitleLayout;
    protected RelativeLayout mTitleContainer;
    protected TextView mTitle;
    protected TextView mleftCancleTv;
    protected TextView mLeftFinishTv;
    protected TextView mLeftBackAndTitle;


    protected void setUpNavigationBar() {
        mBottomLine = findViewById(com.tojoy.tjoybaselib.R.id.base_title_line);
        mTitleContainer = (RelativeLayout) findViewById(R.id.title_container);
        mNormalTitleLayout = (RelativeLayout) findViewById(R.id.rtl_normal_title);
        mLeftArrow = (ImageView) findViewById(R.id.iv_basetitle_leftimg);
        mRightManagerIv = (ImageView) findViewById(R.id.iv_right_manager_bt);
        mTitle = (TextView) findViewById(R.id.tv_basetitle_cetener);
        mRightManagerTv = (TextView) findViewById(R.id.tv_right_manager_tv);
        mleftCancleTv = (TextView) findViewById(R.id.tv_left_manager_tv);
        mLeftFinishTv = (TextView) findViewById(R.id.tv_left_finish_tv);

        int statusBarHeight = AppUtils.getStatusBarHeight(this);
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.dip2px(this, 48));
        lps.setMargins(0, statusBarHeight, 0, 0);
        if (mNormalTitleLayout != null) {
            mNormalTitleLayout.setLayoutParams(lps);
        }

        mLeftBackAndTitle = (TextView) findViewById(R.id.iv_title_back);
    }

    protected void hideStatusBar() {
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.dip2px(this, 48));
        lps.setMargins(0, 0, 0, 0);
        mNormalTitleLayout.setLayoutParams(lps);
    }

    /**
     * 隐藏Navgationbar
     */
    protected void alphaNavgationBar(View containerView) {
        mTitleContainer.setBackgroundColor(getResources().getColor(R.color.transparent));
        mTitle.setVisibility(View.GONE);
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.getHeight(this));
        containerView.setLayoutParams(lps);
        mLeftArrow.setImageResource(R.drawable.img_nav_left_white);
        findViewById(R.id.base_title_line).setVisibility(View.GONE);
    }

    protected void showBlackNavgationBar(View containerView) {
        mTitleContainer.setBackgroundColor(getResources().getColor(R.color.black));
        mTitle.setVisibility(View.VISIBLE);

        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.getHeight(this));
        int statusBarHeight = AppUtils.getStatusBarHeight(this);
        lps.setMargins(0, statusBarHeight + AppUtils.dip2px(this, 48), 0, 0);
        containerView.setLayoutParams(lps);

        mLeftArrow.setImageResource(R.drawable.img_nav_left_white);
        mTitle.setTextColor(getResources().getColor(R.color.color_ffffff));
        findViewById(R.id.base_title_line).setVisibility(View.GONE);
    }

    protected void showNavgationBar(View containerView) {
        mTitleContainer.setBackgroundColor(getResources().getColor(R.color.color_ffffff));
        mTitle.setVisibility(View.VISIBLE);

        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.getHeight(this));
        int statusBarHeight = AppUtils.getStatusBarHeight(this);
        lps.setMargins(0, statusBarHeight + AppUtils.dip2px(this, 48), 0, 0);
        containerView.setLayoutParams(lps);

        mLeftArrow.setImageResource(R.drawable.img_nav_left_back);
        findViewById(R.id.base_title_line).setVisibility(View.VISIBLE);
    }

    /**
     * 名医约见详情--标题、返回按钮均为白色，actionbar背景色为透明
     */
    protected void setAlphaBackground() {
        mTitleContainer.setBackgroundColor(getResources().getColor(R.color.transparent));
        findViewById(R.id.base_title_line).setVisibility(View.GONE);
        mLeftArrow.setImageResource(R.drawable.img_nav_left_white);
        mTitle.setTextColor(getResources().getColor(R.color.color_ffffff));
    }

    protected void setAlphaBackground(int resId) {
        mTitleContainer.setBackgroundColor(getResources().getColor(R.color.transparent));
        findViewById(R.id.base_title_line).setVisibility(View.GONE);
        mLeftArrow.setImageResource(resId);
        mTitle.setTextColor(getResources().getColor(R.color.color_ffffff));
    }

    public RelativeLayout getmTitleContainer() {
        return mTitleContainer;
    }

    protected void showBack() {
        if (mLeftArrow != null) {
            mLeftArrow.setVisibility(View.VISIBLE);
        }
        assert mLeftArrow != null;
        mLeftArrow.setOnClickListener(v -> {
            onLeftManagerImageClick();
            onBackPressed();
        });
    }

    protected void showBackNoFinish() {
        if (mLeftArrow != null) {
            mLeftArrow.setVisibility(View.VISIBLE);
        }
        mLeftArrow.setOnClickListener(v -> {
            onLeftManagerImageClick();
        });
    }

    protected void showBackCancelTitle() {
        if (mleftCancleTv != null) {
            mleftCancleTv.setVisibility(View.VISIBLE);
        }
        assert mleftCancleTv != null;
        mleftCancleTv.setOnClickListener(v -> finish());
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        if (mTitle != null) {
            mTitle.setText(title);
        }
    }

    /**
     * 设置中间标题文案色值
     * @param color
     */
    public void setModileTitleColor(int color) {

        if (mTitle != null) {
            mTitle.setTextColor(color);
        }
    }

    protected void hideBottomLine() {
        if (mBottomLine != null) {
            mBottomLine.setVisibility(View.GONE);
        }
    }

    /**
     * 修改状态
     *
     * @param clickable
     */
    protected void changeRightManagerTitleStatus(boolean clickable) {
        mRightManagerTv.setClickable(clickable);
        if (clickable) {
            mRightManagerTv.setTextColor(getResources().getColor(R.color.color_0f88eb));
        } else {
            mRightManagerTv.setTextColor(getResources().getColor(R.color.color_808080));
        }
    }

    protected void setRightManagerTitle(String title) {
        if (mRightManagerTv != null) {
            mRightManagerTv.setText(title);
            mRightManagerTv.setVisibility(View.VISIBLE);
            mRightManagerTv.setOnClickListener(v -> onRightManagerTitleClick());
        }
    }

    /**
     * 动态设置文字上方的图片的资源
     *
     * @param title
     * @param topDrowable
     */
    protected void setRightManagerTitleTopDrowable(String title, int topDrowable) {
        if (mRightManagerTv != null) {
            mRightManagerTv.setVisibility(View.VISIBLE);
            mRightManagerTv.setText(title);
            mRightManagerTv.setTextSize(10);
            mRightManagerTv.setTextColor(getResources().getColor(R.color.color_7B808F));
            Drawable drawable = getResources().getDrawable(topDrowable);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(),
                    drawable.getMinimumHeight());
            mRightManagerTv.setCompoundDrawables(null, drawable, null, null);
            mRightManagerTv.setOnClickListener(v -> {
                onRightManagerTitleClick();
            });

        }
    }

    protected void setRightManagerTitleColor(int titleColor) {
        if (mRightManagerTv != null) {
            mRightManagerTv.setTextColor(titleColor);
        }
    }

    protected void setRightManagerImg(int resId) {
        if (mRightManagerIv != null) {
            mRightManagerIv.setImageResource(resId);
            mRightManagerIv.setVisibility(View.VISIBLE);
            mRightManagerIv.setOnClickListener(v -> onRightManagerTitleClick());
        }
    }

    protected void onRightManagerTitleClick() {
    }

    protected void onLeftManagerImageClick() {
    }

    /**
     * ProgressHUD处理
     */
    protected ProgressHUD mProgressHUD;

    public void showProgressHUD(Context context, String showMessage) {
        if (this.isFinishing()) {
            return;
        }
        if (mProgressHUD != null && mProgressHUD.isShowing()) {
            mHandler.post(() -> mProgressHUD.setMessage(showMessage));
        } else {
            mProgressHUD = ProgressHUD.show(context, showMessage, true, null);
        }

    }


    public void showProgressHUD(Context context, String showMessage, boolean cancelable) {
        if (this.isFinishing())
            return;
        if (mProgressHUD != null && mProgressHUD.isShowing()) {
            mHandler.post(() -> mProgressHUD.setMessage(showMessage));
        } else {
            mProgressHUD = ProgressHUD.show(context, showMessage, cancelable, null);
        }

    }

    public boolean isShowProgressHUD() {
        try {
            return mProgressHUD != null && mProgressHUD.isShowing();
        } catch (Exception e) {
            return false;
        }

    }


    public void dismissProgressHUD() {
        try {
            if (mProgressHUD != null && !this.isFinishing()) {
                mProgressHUD.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * fragment management
     */
    public TFragment addFragment(TFragment fragment) {
        List<TFragment> fragments = new ArrayList<TFragment>(1);
        fragments.add(fragment);

        List<TFragment> fragments2 = addFragments(fragments);
        return fragments2.get(0);
    }

    public List<TFragment> addFragments(List<TFragment> fragments) {
        List<TFragment> fragments2 = new ArrayList<TFragment>(fragments.size());

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();

        boolean commit = false;
        for (int i = 0; i < fragments.size(); i++) {
            // install
            TFragment fragment = fragments.get(i);
            int id = fragment.getContainerId();

            // exists
            TFragment fragment2 = (TFragment) fm.findFragmentById(id);

            if (fragment2 == null) {
                fragment2 = fragment;
                transaction.add(id, fragment);
                commit = true;
            }

            fragments2.add(i, fragment2);
        }

        if (commit) {
            try {
                transaction.commitAllowingStateLoss();
            } catch (Exception e) {

            }
        }

        return fragments2;
    }

    public TFragment switchContent(TFragment fragment) {
        return switchContent(fragment, false);
    }

    protected TFragment switchContent(TFragment fragment, boolean needAddToBackStack) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(fragment.getContainerId(), fragment);
        if (needAddToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        try {
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {

        }

        return fragment;
    }

    protected <T extends View> T findView(int resId) {
        return (T) (findViewById(resId));
    }

    /**
     * 友盟统计
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d("resultActivity", getLocalClassName());
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    //设置app字体都不允许随系统调节而发生大小变化
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.fontScale != 1)//非默认值
            getResources();
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        if (res.getConfiguration().fontScale != 1) {//非默认值
            Configuration newConfig = new Configuration();
            newConfig.setToDefaults();//设置默认
            res.updateConfiguration(newConfig, res.getDisplayMetrics());
        }
        return res;
    }


    /**
     * 自定义左边图片的图片资源
     *
     * @param imageSrc
     */
    protected void setLeftImage(int imageSrc) {
        if (mLeftArrow != null) {
            mLeftArrow.setVisibility(View.VISIBLE);
        }
        mLeftArrow.setImageResource(imageSrc);
    }

    /**
     * 带返回图标和返回字体
     */
    protected void showBackImageAndTitle() {
        if (mLeftBackAndTitle != null) {
            mLeftBackAndTitle.setVisibility(View.VISIBLE);
            mLeftBackAndTitle.setOnClickListener(v -> {
                onLeftManagerImageClick();
                finish();
            });
        }

    }
}
