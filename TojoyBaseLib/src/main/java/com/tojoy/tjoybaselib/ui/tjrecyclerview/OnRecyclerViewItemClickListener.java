package com.tojoy.tjoybaselib.ui.tjrecyclerview;

import android.view.View;

/**
 * Created by chengyanfang on 2017/9/6.
 */

public interface OnRecyclerViewItemClickListener<T> {
    void onItemClick(View v, int position, T data);
}
