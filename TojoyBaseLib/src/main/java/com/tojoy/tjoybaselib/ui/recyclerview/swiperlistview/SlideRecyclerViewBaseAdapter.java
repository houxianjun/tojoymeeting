package com.tojoy.tjoybaselib.ui.recyclerview.swiperlistview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tojoy.tjoybaselib.ui.recyclerview.holder.BaseViewHolder;


/**
 * Created by chengyanfang on 2017/10/22.
 */

public abstract class SlideRecyclerViewBaseAdapter<K extends BaseViewHolder> extends RecyclerView.Adapter<K> implements ISlideAdapter {
    private SlideAdapterImpl mSlideAdapterImpl;

    protected SlideRecyclerViewBaseAdapter() {
        mSlideAdapterImpl = new SlideAdapterImpl(){
            @Override
            public int[] getBindOnClickViewsIds() {
                return SlideRecyclerViewBaseAdapter.this.getBindOnClickViewsIds();
            }
        };
    }

    @Override
    public void bindSlideState(View slideTouchView) {
        mSlideAdapterImpl.bindSlideState(slideTouchView);
    }

    @Override
    public void bindSlidePosition(View slideTouchView, int pos) {
        mSlideAdapterImpl.bindSlidePosition(slideTouchView, pos);
    }

    @Override
    public void setOnClickSlideItemListener(OnClickSlideItemListener listener) {
        mSlideAdapterImpl.setOnClickSlideItemListener(listener);
    }

    @Override
    public void closeAll() {
        mSlideAdapterImpl.openAll();
    }

    @Override
    public abstract int[] getBindOnClickViewsIds();


    @Override
    public void onViewAttachedToWindow(@NonNull K holder) {
        super.onViewAttachedToWindow(holder);
    }
}
