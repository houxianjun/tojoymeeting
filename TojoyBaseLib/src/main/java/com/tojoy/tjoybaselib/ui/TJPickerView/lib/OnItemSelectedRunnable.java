package com.tojoy.tjoybaselib.ui.TJPickerView.lib;

/**
 * Created by chengyanfang on 2017/10/9.
 */

final class OnItemSelectedRunnable implements Runnable {
    private final WheelView loopView;

    OnItemSelectedRunnable(WheelView loopview) {
        loopView = loopview;
    }

    @Override
    public final void run() {
        loopView.onItemSelectedListener.onItemSelected(loopView.getCurrentItem());
    }
}
