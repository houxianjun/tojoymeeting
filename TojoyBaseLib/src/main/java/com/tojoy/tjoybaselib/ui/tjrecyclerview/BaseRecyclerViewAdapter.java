package com.tojoy.tjoybaselib.ui.tjrecyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tojoy.tjoybaselib.R;
import com.tojoy.tjoybaselib.ui.recyclerview.swiperlistview.SlideRecyclerViewBaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chengyanfang on 2017/9/6.
 */

public abstract class BaseRecyclerViewAdapter<T> extends SlideRecyclerViewBaseAdapter<BaseRecyclerViewHolder<T>> {

    private static final String TAG = "BaseRecyclerViewAdapter";
    protected Context context;
    protected List<T> datas;
    protected LayoutInflater mInflater;

    public boolean hasHeader = false;
    public boolean hasTwoHeader = false;

    protected OnRecyclerViewItemClickListener<T> onRecyclerViewItemClickListener;

    public BaseRecyclerViewAdapter(@NonNull Context context, @NonNull List<T> datas) {
        this.context = context;
        this.datas = datas;
        this.datas = new ArrayList<>(datas);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getItemViewType(int position) {
        if(datas == null || datas.size() == 0) {
            return position;
        }
        return getViewType(position, datas.get(position));
    }

    @Override
    public BaseRecyclerViewHolder<T> onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseRecyclerViewHolder holder;
        if (getLayoutResId(viewType) != 0) {
            View rootView = mInflater.inflate(getLayoutResId(viewType), parent, false);
            holder = getViewHolder(parent, rootView, viewType);
        } else {
            holder = getViewHolder(parent, null, viewType);
        }
        setUpItemEvent(holder);
        return holder;
    }

    /**
     * 设置点击事件
     *
     * @param holder
     */
    protected void setUpItemEvent(final BaseRecyclerViewHolder holder) {
        if (onRecyclerViewItemClickListener != null) {
            holder.itemView.setOnClickListener(v -> {
                //这个获取位置的方法，防止添加删除导致位置不变
                int layoutPosition = holder.getAdapterPosition();

                if (hasHeader) layoutPosition = layoutPosition - 1;
                if (hasTwoHeader) layoutPosition = layoutPosition - 1;

                try {
                    onRecyclerViewItemClickListener.onItemClick(holder.itemView, layoutPosition, datas.get(layoutPosition));
                } catch (Exception e) {
                }
            });
        }
    }


    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder<T> holder, int position) {
        T data = datas.get(position);
        holder.itemView.setTag(R.id.recycler_view_tag, data);
        holder.onBindData(data, position);
        onBindData(holder, data, position);
    }

    public void addMore(List<T> datas) {
        if (!(datas == null || datas.size() <= 0)) {
            this.datas.addAll(datas);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return datas == null ? 0 : datas.size();
    }

    public void updateData(List<T> datas) {
        if (this.datas != null) {
            this.datas.clear();
            this.datas.addAll(datas);
        } else {
            this.datas = datas;
        }
        notifyDataSetChanged();
    }

    public List<T> getDatas() {
        return datas;
    }


    protected abstract int getViewType(int position, @NonNull T data);

    protected abstract int getLayoutResId(int viewType);

    protected abstract BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType);

    protected void onBindData(BaseRecyclerViewHolder<T> holder, T data, int position) {
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener<T> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @Override
    public int[] getBindOnClickViewsIds() {
        return new int[0];
    }
}
