package com.tojoy.tjoybaselib.ui.tjrecyclerview;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

public class TJLinerLayoutManager extends LinearLayoutManager {

    private boolean isScrollEnabled = true;

    TJLinerLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public void setScrollEnabled(boolean flag) {
        this.isScrollEnabled = flag;
    }

    @Override
    public boolean canScrollVertically() {
        return isScrollEnabled && super.canScrollVertically();
    }

}
