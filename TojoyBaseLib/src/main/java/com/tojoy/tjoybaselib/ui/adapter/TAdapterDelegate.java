package com.tojoy.tjoybaselib.ui.adapter;

public interface TAdapterDelegate {

    int getViewTypeCount();

    Class<? extends TViewHolder> viewHolderAtPosition(int position);

    boolean enabled(int position);
}