package com.tojoy.tjoybaselib.ui.tjrecyclerview;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;

import com.tojoy.tjoybaselib.R;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.ui.recyclerview.layoutmanager.FullyLinearLayoutManager;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import jp.wasabeef.recyclerview.animators.SlideInRightAnimator;

import static com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView.Mode.FROM_BOTTOM;
import static com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView.Mode.FROM_START;
import static com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView.Status.CANNOT_LOADMORE;
import static com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView.Status.DEFAULT;
import static com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView.Status.REFRESHING;

/**
 * Created by chengyanfang on 2017/9/7.
 *
 * @function: 增加HeaderView 和 FooterView
 * <p>
 * 配置下拉刷新及加载更多
 */

public class TJRecyclerView extends FrameLayout {
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({DEFAULT, REFRESHING, CANNOT_LOADMORE})
    @interface Status {
        int DEFAULT = 0;
        int REFRESHING = 1;
        int CANNOT_LOADMORE = 2;
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({FROM_START, FROM_BOTTOM})
    @interface Mode {
        int FROM_START = 0;
        int FROM_BOTTOM = 1;
    }

    @Status
    private int currentStatus;
    @Mode
    private int pullMode;

    //callback
    private OnRefreshListener2 onRefreshListener;
    private SwipeRefreshLayout swipeRefreshLayout;
    private GesturesRecyclerView recyclerView;
    public TJLinerLayoutManager linearLayoutManager;
    private PullRefreshFooter footerView;

    // 滚动到中间位置
    private boolean isMove = false;
    private int position = 0;

    public TJRecyclerView(Context context) {
        this(context, null);
    }

    public TJRecyclerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TJRecyclerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


    @Override
    protected void onFinishInflate() {
        if (getChildCount() > 1) {
            throw new IllegalStateException("咳咳，不能超过一个view哦");
        }
        super.onFinishInflate();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (onPreDispatchTouchListener != null) {
            onPreDispatchTouchListener.onPreTouch(ev);
        }
        return super.dispatchTouchEvent(ev);
    }


    private void init(Context context) {
        if (isInEditMode()) return;
        setRecyclerView(context);
    }

    /**
     * 设置RecyclerView
     */
    private void setRecyclerView(Context context) {
        if (recyclerView == null) {
            swipeRefreshLayout = (SwipeRefreshLayout) LayoutInflater.from(context).inflate(R.layout.view_recyclerview, this, false);
            swipeRefreshLayout.setColorSchemeResources(R.color.color_0f88eb);
            swipeRefreshLayout.setOnRefreshListener(() -> swipeRefreshLayout.postDelayed(() -> onRefreshListener.onRefresh(), 500));
            recyclerView = (GesturesRecyclerView) swipeRefreshLayout.findViewById(R.id.recyclerview);
            linearLayoutManager = new TJLinerLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setItemAnimator(null);
        }
        addView(swipeRefreshLayout, RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT);
        recyclerView.addOnScrollListener(onScrollListener);
    }

    boolean canHasNotFooter = false;

    public void setGridLayoutManager(Context context, int gridCnt) {
        canHasNotFooter = true;
        recyclerView.setLayoutManager(new GridLayoutManager(context, gridCnt));
    }

    public void setLayout(FullyLinearLayoutManager mLayoutManager) {
        recyclerView.setLayoutManager(mLayoutManager);
    }

    public void setItemAnimator() {
        recyclerView.setItemAnimator(new SlideInRightAnimator());
        recyclerView.getItemAnimator().setRemoveDuration(1250);
        SlideInLeftAnimator animator = new SlideInLeftAnimator();
        animator.setInterpolator(new OvershootInterpolator());
        recyclerView.setItemAnimator(animator);
    }

    public void removeItemAnimator() {
        recyclerView.setItemAnimator(null);
    }

    /**
     * recyclerView 滑动到中间位置
     *
     * @param position
     */
    public void moveToPosition(int position) {
        this.position = position;
        // 如果item可见
        if (isVisible(position)) {
            scrollToMiddle(position);
        } else {
            recyclerView.scrollToPosition(position);
            isMove = true;
        }
    }

    /**
     * 滑动到中间位置
     *
     * @param position
     */
    private void scrollToMiddle(int position) {
        if (recyclerView == null) return;

        RecyclerView.LayoutManager layoutManager;
        int firstVisibleItemPosition;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            layoutManager = recyclerView.getLayoutManager();
            firstVisibleItemPosition = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
            int top = recyclerView.getChildAt(position - firstVisibleItemPosition).getTop();
            int viewHeight = recyclerView.getChildAt(position - firstVisibleItemPosition).getHeight();
            Rect rect = new Rect();
            recyclerView.getGlobalVisibleRect(rect);
            int reHeight = rect.bottom - rect.top - viewHeight;
            int half = reHeight / 2;
            recyclerView.scrollBy(0, top - half);
            isMove = false;
        }
    }

    // 指定item是否可见
    private boolean isVisible(int position) {
        if (recyclerView == null) return true;
        RecyclerView.LayoutManager layoutManager;
        int firstVisibleItemPosition;
        int lastVisibleItemPosition;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            layoutManager = recyclerView.getLayoutManager();
            firstVisibleItemPosition = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
            lastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
            return position >= firstVisibleItemPosition && position <= lastVisibleItemPosition;
        }
        return true;
    }


    /**
     * 设置加载更多View
     */
    public void setLoadMoreView() {
        footerView = new PullRefreshFooter(getContext());
        addFooterView(footerView);
    }

    public void setLoadMoreViewWithHide() {
        footerView = new PullRefreshFooter(getContext());
        goneFooterView();
        addFooterView(footerView);
    }

    public void setEmptyImgAndTip(int drawable, String tip) {
        if (footerView != null) {
            footerView.setEmptyImgAndTip(drawable, tip);
        }
    }

    public void showEmptyView(){
        if (footerView != null) {
            footerView.showEmptyView();
        }
    }
    public void setEmptyTextViewStyle(String color, int size) {
        if (footerView != null) {
            footerView.setEmptyTextViewStyle(color, size);
        }
    }

    /**
     * 动态设置字体和图片的间距
     *
     * @param topMarginPx 单位px
     * @param botMarginPx 单位px
     */
    public void setEmptyTextSpaceMargin(int topMarginPx, int botMarginPx) {
        if (footerView != null) {
            footerView.setEmptyTextSpaceMargin(topMarginPx, botMarginPx);
        }
    }

    public void hideBottomLoadMoreTip() {
        if (footerView != null) {
            footerView.hideBottomLoadMoreTip();
        }
    }

    public void setEmptyViewLocationFail(int drawable, String tip) {
        if (footerView != null) {
            footerView.setEmptyViewLocationFail(drawable, tip);
        }
    }

    public void setRefreshable(boolean refreshable) {
        swipeRefreshLayout.setEnabled(refreshable);
    }

    /**
     * 隐藏底部view
     */
    public void goneFooterView() {
        if (footerView != null) {
            footerView.refresh(1, 0);
        }
    }

    public void hideFooterView() {
        if (footerView == null) return;
        footerView.setVisibility(GONE);
    }

    public void showFooterView() {
        footerView.setVisibility(VISIBLE);
    }

    public void autoRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.postDelayed(() -> onRefreshListener.onRefresh(), 500);
    }

    /**
     * 设置是否刷新
     */
    public void setRefreshing(boolean isRefreshing) {
        swipeRefreshLayout.setRefreshing(false);
    }

    /**
     * 停止刷新
     */
    public void stopRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }


    //------------------------------------------get/set-----------------------------------------------
    private void setCurrentStatus(@Status int status) {
        this.currentStatus = status;
    }


    public OnRefreshListener2 getOnRefreshListener() {
        return onRefreshListener;
    }

    public void setOnRefreshListener(OnRefreshListener2 onRefreshListener) {
        this.onRefreshListener = onRefreshListener;
    }

    public OnPreDispatchTouchListener getOnPreDispatchTouchListener() {
        return onPreDispatchTouchListener;
    }

    public void setOnPreDispatchTouchListener(OnPreDispatchTouchListener onPreDispatchTouchListener) {
        this.onPreDispatchTouchListener = onPreDispatchTouchListener;
    }

    /**
     * 控制与输入框的冲突
     */
    private OnPreDispatchTouchListener onPreDispatchTouchListener;

    public interface OnPreDispatchTouchListener {
        boolean onPreTouch(MotionEvent ev);
    }


    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        if (adapter != null) {
            if (mHeaderViewInfos.size() > 0 || mFooterViewInfos.size() > 0) {
                recyclerView.setAdapter(wrapHeaderRecyclerViewAdapterInternal(adapter, mHeaderViewInfos, mFooterViewInfos));
            } else {
                recyclerView.setAdapter(adapter);
            }
        }
    }


    /**
     * 判断recyclerview是否滑到底部
     * <p>
     * 原理：判断滑过的距离加上屏幕上的显示的区域是否比整个控件高度高
     *
     * @return
     */
    public boolean isScrollToBottom() {

        if (recyclerView == null) return false;

        RecyclerView.LayoutManager layoutManager;
        int lastVisibleItemPosition;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            layoutManager = recyclerView.getLayoutManager();
            lastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
        } else {
            layoutManager = recyclerView.getLayoutManager();
            int[] lastPositions = new int[((StaggeredGridLayoutManager) layoutManager).getSpanCount()];
            ((StaggeredGridLayoutManager) layoutManager).findLastVisibleItemPositions(lastPositions);
            lastVisibleItemPosition = findMax(lastPositions);
        }

        //当前屏幕所看到的子项个数
        int visibleItemCount = layoutManager.getChildCount();
        //当前RecyclerView的所有子项个数
        int totalItemCount = layoutManager.getItemCount();

        if (totalItemCount <= 1) {
            return false;
        }
        //RecyclerView的滑动状态
        int state = recyclerView.getScrollState();
        if (visibleItemCount > 0 && lastVisibleItemPosition == totalItemCount - 1) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * 添加recyclerView滑动事件监听
     * @param listener
     */
    public void addOnScrollListener(RecyclerView.OnScrollListener listener) {
        recyclerView.addOnScrollListener(listener);
    }

    /**
     * 计算RecyclerView滑动的距离
     * @param hasHead 是否有头部
     * @param headerHeight RecyclerView的头部高度
     * @return 滑动的距离
     */
    public int getScollYHeight(boolean hasHead, int headerHeight) {

        RecyclerView.LayoutManager layoutManager = null;


        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            layoutManager = recyclerView.getLayoutManager();
        }

        //获取到第一个可见的position,其添加的头部不算其position当中
        int position = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
        //通过position获取其管理器中的视图
        View firstVisiableChildView = ((LinearLayoutManager) layoutManager).findViewByPosition(position);
        //获取自身的高度
        int itemHeight = firstVisiableChildView.getHeight();
        //有头部
        if(hasHead) {
            return headerHeight + itemHeight*position - firstVisiableChildView.getTop();
        }else {
            return itemHeight*position - firstVisiableChildView.getTop();
        }
    }


    /**
     * scroll listener
     */
    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (canHasNotFooter) {
                if (isScrollToBottom() && currentStatus != REFRESHING && currentStatus != CANNOT_LOADMORE && onRefreshListener != null) {
                    onRefreshListener.onLoadMore();
                    pullMode = FROM_BOTTOM;
                    setCurrentStatus(REFRESHING);
                }
            } else {
                if (footerView != null && isScrollToBottom() && currentStatus != REFRESHING && currentStatus != CANNOT_LOADMORE && onRefreshListener != null) {
                    onRefreshListener.onLoadMore();
                    pullMode = FROM_BOTTOM;
                    setCurrentStatus(REFRESHING);
                    footerView.onRefreshing();
                }
            }

            // 滑动到中间位置
            if (isMove) {
                scrollToMiddle(position);
            }
        }
    };


    private int findMax(int[] lastPositions) {
        int max = lastPositions[0];
        for (int value : lastPositions) {
            if (value > max) {
                max = value;
            }
        }
        return max;
    }


    public void refreshLoadMoreView(int currentPage, int currentPageCount) {
        refreshLoadMoreView(currentPage, currentPageCount > 10 ? 10 : currentPageCount, AppConfig.PER_PAGE_COUNT);
    }

    public void hideScrollBar() {
        recyclerView.setScrollBarSize(0);
    }

    public void refreshLoadMoreView(int currentPage, int currentPageCount, int perPageSize) {
        setCurrentStatus(DEFAULT);
        swipeRefreshLayout.setRefreshing(false);

        //是否还可以继续加载
        if (currentPageCount == perPageSize) {
            currentStatus = DEFAULT;
        } else {
            currentStatus = CANNOT_LOADMORE;
        }

        if (footerView == null) return;

        footerView.refresh(currentPage, currentPageCount, perPageSize);

    }

    //------------------------------------------分割线-----------------------------------------------

    /**
     * 以下为recyclerview 的headeradapter实现方案
     * <p>
     * 以Listview的headerView和footerView为模板做出的recyclerview的header和footer
     */
    private ArrayList<FixedViewInfo> mHeaderViewInfos = new ArrayList<>();
    private ArrayList<FixedViewInfo> mFooterViewInfos = new ArrayList<>();

    /**
     * 不完美解决方法：添加一个header，则从-2开始减1
     * header:-2~-98
     */
    private static final int ITEM_VIEW_TYPE_HEADER_START = -2;
    /**
     * 不完美解决方法：添加一个header，则从-99开始减1
     * footer:-99~-无穷
     */
    private static final int ITEM_VIEW_TYPE_FOOTER_START = -99;

    public void addHeaderView(View headerView) {
        final FixedViewInfo info = new FixedViewInfo(headerView, ITEM_VIEW_TYPE_HEADER_START - mHeaderViewInfos.size());
        if (mHeaderViewInfos.size() == Math.abs(ITEM_VIEW_TYPE_FOOTER_START - ITEM_VIEW_TYPE_HEADER_START)) {
            mHeaderViewInfos.remove(mHeaderViewInfos.size() - 1);
        }
        if (checkFixedViewInfoNotAdded(info, mHeaderViewInfos)) {
            mHeaderViewInfos.add(info);
        }
        checkAndNotifyWrappedViewAdd(recyclerView.getAdapter(), info, true);

    }

    private void checkAndNotifyWrappedViewAdd(RecyclerView.Adapter adapter, FixedViewInfo info, boolean isHeader) {
        //header和footer只能再setAdapter前使用，如果是set了之后再用，为何不add普通的viewholder而非要Headr或者footer呢
        if (adapter != null) {
            if (!(adapter instanceof HeaderViewWrapperAdapter)) {
                adapter = wrapHeaderRecyclerViewAdapterInternal(adapter);
                if (isHeader) {
                    adapter.notifyItemInserted(((HeaderViewWrapperAdapter) adapter).findHeaderPosition(info.view));
                } else {
                    adapter.notifyItemInserted(((HeaderViewWrapperAdapter) adapter).findFooterPosition(info.view));
                }
            }
        }
    }

    public void addFooterView(View footerView) {
        if (mFooterViewInfos.size() > 0) {
            return;
        }
        final FixedViewInfo info = new FixedViewInfo(footerView, ITEM_VIEW_TYPE_FOOTER_START - mFooterViewInfos.size());
        if (checkFixedViewInfoNotAdded(info, mFooterViewInfos)) {
            mFooterViewInfos.add(info);
        }
        checkAndNotifyWrappedViewAdd(recyclerView.getAdapter(), info, false);
    }

    private boolean checkFixedViewInfoNotAdded(FixedViewInfo info, List<FixedViewInfo> infoList) {
        boolean result = true;
        if (AppUtils.isListEmpty(infoList) || info == null) {
            result = true;
        } else {
            for (FixedViewInfo fixedViewInfo : infoList) {
                if (fixedViewInfo.view == info.view) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    public int getHeaderViewCount() {
        return mHeaderViewInfos.size();
    }

    public int getFooterViewCount() {
        return mFooterViewInfos.size();
    }

    protected HeaderViewWrapperAdapter wrapHeaderRecyclerViewAdapterInternal(@NonNull RecyclerView.Adapter mWrappedAdapter,
                                                                             ArrayList<FixedViewInfo> mHeaderViewInfos,
                                                                             ArrayList<FixedViewInfo> mFooterViewInfos) {
        return new HeaderViewWrapperAdapter(recyclerView, mWrappedAdapter, mHeaderViewInfos, mFooterViewInfos);
    }

    protected HeaderViewWrapperAdapter wrapHeaderRecyclerViewAdapterInternal(@NonNull RecyclerView.Adapter mWrappedAdapter) {
        return wrapHeaderRecyclerViewAdapterInternal(mWrappedAdapter, mHeaderViewInfos, mFooterViewInfos);

    }
}
