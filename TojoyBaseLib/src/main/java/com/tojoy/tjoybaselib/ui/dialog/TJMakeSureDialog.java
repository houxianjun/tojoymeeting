package com.tojoy.tjoybaselib.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.R;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.configs.UrlProvider;
import com.tojoy.tjoybaselib.net.RetrofitSingleton;
import com.tojoy.tjoybaselib.util.sys.AppUtils;

/**
 * Created by chengyanfang on 2017/10/13.
 */

public class TJMakeSureDialog {

    private Context mContext;
    private View mView;
    private Dialog alertDialog;
    //取消监听
    private OnDialogCancelListener mCancelListener;

    //确定按钮
    private TextView mBtnSure, mBtnCancel;

    public TJMakeSureDialog(Context context) {
        mContext = context;
        initUI();
    }

    public TJMakeSureDialog(Context context, View.OnClickListener onClickListener) {
        mContext = context;
        initUI();
        initEvent(onClickListener, true);
    }

    public TJMakeSureDialog(Context context, boolean isAgreeFirst, View.OnClickListener onClickListener) {
        mContext = context;
        initAgreeUI(isAgreeFirst);
        initEvent(onClickListener, true);
    }

    /**
     * 适配android Q 的升级
     */
    public TJMakeSureDialog(Context context, View.OnClickListener onClickListener, boolean isCanDismiss) {
        mContext = context;
        initUI();
        initEvent(onClickListener, isCanDismiss);
    }

    /**
     * 抢占机会的布局
     */
    public TJMakeSureDialog(Context context, int type, View.OnClickListener onClickListener) {
        mContext = context;
        initGrabUI();
        initEvent(onClickListener, true);
    }

    public TJMakeSureDialog(Context context, String content, View.OnClickListener onClickListener) {
        mContext = context;
        initPswHintUI(content);
        initEvent(onClickListener, true);
    }

    public TJMakeSureDialog(Context context, SpannableStringBuilder content, View.OnClickListener onClickListener) {
        mContext = context;
        initPswHintUI(content);
        initEvent(onClickListener, true);
    }


    private void initEvent(View.OnClickListener onClickListener, boolean isCanDismiss) {
        mView.findViewById(R.id.rlv_sure).setOnClickListener(v -> {
            if (isCanDismiss) {
                alertDialog.dismiss();
            }
            if (onClickListener != null) {
                onClickListener.onClick(v);
            }
        });
        mView.findViewById(R.id.rlv_cancle).setOnClickListener(v -> {
            if (isCanDismiss) {
                alertDialog.dismiss();
            }
            if (mCancelListener != null) {
                mCancelListener.onCancelClick();
            }
        });
    }


    private void initGrabUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.popup_grab_makesure_layout, null);
        mBtnSure = mView.findViewById(R.id.button_sure);
        mBtnCancel = mView.findViewById(R.id.button_cancle);
    }

    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.popup_makesure_layout, null);
        mBtnSure = mView.findViewById(R.id.button_sure);
        mBtnCancel = mView.findViewById(R.id.button_cancle);
    }

    private void initAgreeUI(boolean isAgreeFirst) {
        mView = LayoutInflater.from(mContext).inflate(R.layout.popup_agree_first_makesure_layout, null);
        mBtnSure = mView.findViewById(R.id.button_sure);
        mBtnCancel = mView.findViewById(R.id.button_cancle);
        RelativeLayout mAgreeRl = mView.findViewById(R.id.rlv_agree);
        TextView mBtnUser = mView.findViewById(R.id.tv_login_agree);
        TextView mBtnprivacy = mView.findViewById(R.id.tvPrivacy);
        mBtnUser.setText(Html.fromHtml("<u>" + "《用户协议》" + "</u>"));
        mBtnprivacy.setText(Html.fromHtml("<u>" + "《隐私政策》" + "</u>"));

        if (isAgreeFirst) {
            mAgreeRl.setVisibility(View.VISIBLE);
        } else {
            mAgreeRl.setVisibility(View.GONE);
        }

        mBtnUser.setOnClickListener(v ->
                ARouter.getInstance().build(RouterPathProvider.WEBNew)
                .withString("url", UrlProvider.USERCENTER_RegistPolicy)
                .withString("title", "")
                .navigation());
        mBtnprivacy.setOnClickListener(v ->
                ARouter.getInstance().build(RouterPathProvider.WEBNew)
                .withString("url", UrlProvider.USERCENTER_Privacy)
                .withString("title", "")
                .navigation());
    }

    private void initPswHintUI(String content) {
        mView = LayoutInflater.from(mContext).inflate(R.layout.popup_psw_hint_layout, null);
        mBtnSure = mView.findViewById(R.id.button_sure);
        mBtnCancel = mView.findViewById(R.id.button_cancle);
        ((TextView) mView.findViewById(R.id.tv_content)).setText(content);
    }

    /**
     * 弹窗提示字体颜色可以自定义
     *
     * @param content
     */
    private void initPswHintUI(SpannableStringBuilder content) {
        mView = LayoutInflater.from(mContext).inflate(R.layout.popup_psw_hint_layout, null);
        mBtnSure = mView.findViewById(R.id.button_sure);
        mBtnCancel = mView.findViewById(R.id.button_cancle);
        ((TextView) mView.findViewById(R.id.tv_content)).setText(content);
    }

    public void show() {

        if (mView.getParent() != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            parent.removeAllViews();
        }
        alertDialog = new Dialog(mContext, R.style.tjoy_dialog_style);
        alertDialog.setContentView(mView, new ViewGroup.LayoutParams(AppUtils.getWidth(mContext) - AppUtils.dip2px(mContext, 120), ViewGroup.LayoutParams.WRAP_CONTENT));
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        alertDialog.setOnKeyListener((dialog, keyCode, event) -> true);
    }

    public void show(int marginWidth) {
        if (mView.getParent() != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            parent.removeAllViews();
        }

        alertDialog = new Dialog(mContext, R.style.tjoy_dialog_style);
        alertDialog.setContentView(mView, new ViewGroup.LayoutParams(AppUtils.getWidth(mContext) - AppUtils.dip2px(mContext, marginWidth), ViewGroup.LayoutParams.WRAP_CONTENT));
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        alertDialog.setOnKeyListener((dialog, keyCode, event) -> true);
    }

    /**
     * ****************** UI定制化
     */
    public TJMakeSureDialog setTitleAndCotent(String title, String content) {
        ((TextView) mView.findViewById(R.id.tv_title)).setText(title);
        ((TextView) mView.findViewById(R.id.tv_content)).setText(content);
        return this;
    }

    /**
     * 提示内容显示两行并居中
     *
     * @param content
     * @param hint
     * @return
     */
    public TJMakeSureDialog setcontent(String content, String hint) {
        mView.findViewById(R.id.rlv_contenthint_layout).setVisibility(View.VISIBLE);
        ((TextView) mView.findViewById(R.id.tv_content_meeting)).setText(content);
        ((TextView) mView.findViewById(R.id.tv_content_hint)).setText(hint);
        return this;
    }

    /**
     * 提示内容显示两行并居中
     *
     * @param content
     * @return
     */
    public TJMakeSureDialog setCallPhonecontent(String content) {
        hideTitle();
        mView.findViewById(R.id.rlv_contenthint_layout).setVisibility(View.VISIBLE);
        TextView contentTv = mView.findViewById(R.id.tv_content_meeting);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) contentTv.getLayoutParams();
        layoutParams.topMargin = AppUtils.dip2px(contentTv.getContext(), 40);
        contentTv.setLayoutParams(layoutParams);
        contentTv.setTextColor(contentTv.getContext().getResources().getColor(R.color.color_353f5d));
        contentTv.setText(content);
        mView.findViewById(R.id.tv_content_hint).setVisibility(View.GONE);
        return this;
    }

    /**
     * 显示提示框
     */
    public void showAlert() {
        mView.findViewById(R.id.rlv_cancle).setVisibility(View.GONE);
        mView.findViewById(R.id.bottom_vertical_divider).setVisibility(View.GONE);
        show();
    }

    //主播邀请后 观众的弹窗
    public void showSameTableDialog2(Activity context) {
        if (mView.getParent() != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            parent.removeAllViews();
        }
        alertDialog = new Dialog(context, R.style.tjoy_dialog_style);
        alertDialog.setContentView(mView, new ViewGroup.LayoutParams(AppUtils.getWidth(context) - AppUtils.dip2px(context, 120), ViewGroup.LayoutParams.WRAP_CONTENT));
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.setCanceledOnTouchOutside(false);
        if (!context.isFinishing()) {
            alertDialog.show();
        }
        alertDialog.setOnKeyListener((dialog, keyCode, event) -> true);
    }

    //观众申请 主播同意后 观众的弹窗
    public void showSameTableDialog(Activity context) {
        mView.findViewById(R.id.rlv_cancle).setVisibility(View.GONE);
        mView.findViewById(R.id.bottom_vertical_divider).setVisibility(View.GONE);
        if (mView.getParent() != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            parent.removeAllViews();
        }
        alertDialog = new Dialog(context, R.style.tjoy_dialog_style);
        alertDialog.setContentView(mView, new ViewGroup.LayoutParams(AppUtils.getWidth(context) - AppUtils.dip2px(context, 120), ViewGroup.LayoutParams.WRAP_CONTENT));
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.setCanceledOnTouchOutside(false);
        if (!context.isFinishing()) {
            alertDialog.show();
        }
        alertDialog.setOnKeyListener((dialog, keyCode, event) -> true);
    }


    public TJMakeSureDialog hideTitle() {
        mView.findViewById(R.id.rlv_title_layout).setVisibility(View.GONE);
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lps.setMargins(0, AppUtils.dip2px(mContext, 35), 0, AppUtils.dip2px(mContext, 35));
        lps.addRule(RelativeLayout.CENTER_HORIZONTAL);
        mView.findViewById(R.id.tv_content).setLayoutParams(lps);
        ((TextView) mView.findViewById(R.id.tv_content)).setTextSize(15);

        return this;
    }

    public void setSameColor() {
        TextView mTextView;
        mTextView = mView.findViewById(R.id.button_cancle);
        mTextView.setTextColor(mContext.getResources().getColor(R.color.color_0f88eb));
    }

    /**
     * 设置按钮文字
     */

    public TJMakeSureDialog setBtnText(String sure, String cancle) {
        ((TextView) mView.findViewById(R.id.button_sure)).setText(sure);
        ((TextView) mView.findViewById(R.id.button_cancle)).setText(cancle);
        return this;
    }

    public TJMakeSureDialog setBtnSureText(String text) {
        mBtnSure.setText(text);
        return this;
    }

    public TJMakeSureDialog setBtnCancelText(String text) {
        if (mBtnCancel != null) {
            mBtnCancel.setText(text);
            mBtnCancel.setVisibility(View.VISIBLE);
        }
        return this;
    }

    /**
     * 判断是否在展示
     */
    public boolean isShowing() {
        if (alertDialog == null) {
            return false;
        }
        return alertDialog.isShowing();
    }

    public void dismiss() {
        if (isShowing()) {
            alertDialog.dismiss();
        }
    }

    public void dismissStrongly() {
        try {
            alertDialog.dismiss();
        } catch (Exception e) {

        }
    }

    /**
     * 设置确定按钮的显示
     *
     * @param okText
     */
    public TJMakeSureDialog setOkText(String okText) {
        mBtnSure.setText(okText);
        return this;
    }

    //设置取消监听
    public TJMakeSureDialog setCancelListener(OnDialogCancelListener mCancelListener) {
        this.mCancelListener = mCancelListener;
        return this;
    }

    //点击取消监听
    public interface OnDialogCancelListener {
        void onCancelClick();
    }

    public void setEvent(View.OnClickListener onClickListener) {
        mView.findViewById(R.id.rlv_sure).setOnClickListener(onClickListener);

        mView.findViewById(R.id.rlv_cancle).setOnClickListener(v -> {
            if (mCancelListener != null) {
                mCancelListener.onCancelClick();
            }
            alertDialog.dismiss();
        });
    }


}
