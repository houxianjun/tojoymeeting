package com.tojoy.tjoybaselib.ui.tjrecyclerview;

import android.support.v7.widget.RecyclerView;

/**
 * Created by chengyanfang on 2017/9/7.
 */

public interface WrapperRecyclerAdapter {
    RecyclerView.Adapter getWrappedAdapter();
}
