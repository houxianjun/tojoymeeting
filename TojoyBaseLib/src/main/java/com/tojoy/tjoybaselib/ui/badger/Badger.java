package com.tojoy.tjoybaselib.ui.badger;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import com.tojoy.tjoybaselib.framework.Handlers;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * APP图标未读数红点更新接口
 * https://github.com/leolin310148/ShortcutBadger
 * <p>
 * Created by huangjun on 2017/7/25.
 */

public class Badger {
    private static final String TAG = "Badger";

    private static Handler handler;

    private static boolean support;

    static {
        support = Build.VERSION.SDK_INT < Build.VERSION_CODES.O;
    }

    public static void updateBadgerCount(Context context, final int unreadCount) {
        if (!support) {
            return; // O版本及以上不再支持
        }
        if (handler == null) {
            handler = Handlers.sharedInstance().newHandler("Badger");
        }
        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(() -> {
            int badgerCount = unreadCount;
            if (badgerCount < 0) {
                badgerCount = 0;
            } else if (badgerCount > 99) {
                badgerCount = 99;
            }

            boolean res = ShortcutBadger.applyCount(context, badgerCount);
            if (!res) {
                // 如果失败就不要再使用了!
                support = false;
            }
            Log.i(TAG, "update badger count " + (res ? "success" : "failed"));
        }, 200);
    }
}
