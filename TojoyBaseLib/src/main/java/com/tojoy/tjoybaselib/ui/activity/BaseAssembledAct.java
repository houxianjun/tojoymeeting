package com.tojoy.tjoybaselib.ui.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import com.tojoy.tjoybaselib.R;
import com.tojoy.tjoybaselib.util.sys.ActivityStack;
import com.tojoy.tjoybaselib.util.sys.ReflectionUtil;

import me.imid.swipebacklayout.lib.SwipeBackLayout;
import me.imid.swipebacklayout.lib.Utils;
import me.imid.swipebacklayout.lib.app.SwipeBackActivityBase;
import me.imid.swipebacklayout.lib.app.SwipeBackActivityHelper;

/**
 * Created by chengyanfang on 2017/8/28.
 */

public class BaseAssembledAct extends BasePermissionAct implements SwipeBackActivityBase {


    protected Handler mHandler;

    @SuppressLint("HandlerLeak")
    private void initHandler() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
            }
        };
    }

    /**
     * //////////////////////////////
     * LIFE CYCLER
     * //////////////////////////////
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mHelper = new SwipeBackActivityHelper(this);
        mHelper.onActivityCreate();
        initHandler();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
        ActivityStack.getActivityStack().pushActivity(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mHelper.onPostCreate();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        invokeFragmentManagerNoteStateNotSaved();
        super.onBackPressed();
    }

    private void invokeFragmentManagerNoteStateNotSaved() {
        FragmentManager fm = getSupportFragmentManager();
        ReflectionUtil.invokeMethod(fm, "noteStateNotSaved", null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        destroyed = true;
        ActivityStack.getActivityStack().popAcitivityAndReomve(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * //////////////////////////////
     * Handler 异步管理
     * //////////////////////////////
     */
    private static Handler handler;

    protected final Handler getHandler() {
        if (handler == null) {
            handler = new Handler(getMainLooper());
        }
        return handler;
    }


    /**
     * //////////////////////////////
     * SwipeBack 相关
     * //////////////////////////////
     */
    private SwipeBackActivityHelper mHelper;

    @Override
    public View findViewById(int id) {
        View v = super.findViewById(id);
        if (v == null && mHelper != null) {
            return mHelper.findViewById(id);
        }
        return v;
    }

    @Override
    public SwipeBackLayout getSwipeBackLayout() {
        return mHelper.getSwipeBackLayout();
    }

    @Override
    public void setSwipeBackEnable(boolean enable) {
        getSwipeBackLayout().setEnableGesture(enable);
    }

    @Override
    public void scrollToFinishActivity() {
        Utils.convertActivityToTranslucent(this);
        getSwipeBackLayout().scrollToFinishActivity();
    }


    /**
     * //////////////////////////////
     * 键盘操作 相关
     * //////////////////////////////
     */
    public void showKeyboard(boolean isShow) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        if (isShow) {
            if (getCurrentFocus() == null) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            } else {
                imm.showSoftInput(getCurrentFocus(), 0);
            }
        } else {
            if (getCurrentFocus() != null) {
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    /**
     * //////////////////////////////
     * 键盘操作 相关
     * //////////////////////////////
     */
    public void showKeyboard(boolean isShow,View view) {

        View foucusView = getCurrentFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        if (isShow) {
            if (foucusView == null) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            } else {
                if(view != null){
                    imm.showSoftInput(view, 0);
                }

            }
        } else {
            if (foucusView != null) {
                if(view != null){
                    imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }

            }
        }
    }

    protected void showKeyboardDelayed(View focus) {
        final View viewToFocus = focus;
        if (focus != null) {
            focus.requestFocus();
        }

        getHandler().postDelayed(() -> {
            if (viewToFocus == null || viewToFocus.isFocused()) {
                showKeyboard(true);
            }
        }, 200);
    }

    /**
     * //////////////////////////////
     * 退出系统
     * //////////////////////////////
     */
    protected boolean destroyed = false;

    public boolean isDestroyedCompatible() {
        if (Build.VERSION.SDK_INT >= 17) {
            return isDestroyedCompatible17();
        } else {
            return destroyed || super.isFinishing();
        }
    }

    @TargetApi(17)
    private boolean isDestroyedCompatible17() {
        return super.isDestroyed();
    }

    protected boolean mIsFirstAnim = false;

    @Override
    public void finish() {
        super.finish();
        if (mIsFirstAnim) {
            inFromRightOutToLeft();
            mIsFirstAnim = false;
        } else {
            inFromLeftOutToRight();
        }
    }

    protected void inFromRightOutToLeft() {
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }

    protected void inFromLeftOutToRight() {
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }

}


