package com.tojoy.tjoybaselib.ui.dotindicator;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.widget.LinearLayout;

import com.tojoy.tjoybaselib.util.sys.AppUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chengyanfang on 2017/9/7.
 *
 * banner指示器容器
 */

public class DotIndicator extends LinearLayout {

    private static final String TAG = "DotIndicator";
    private static final int DEFAULT_DOT_NUM = 0;
    private static final int DOT_SIZE = 5;

    List<DotView> mDotViews;

    private int currentSelection = 0;

    private int mDotsNum = DEFAULT_DOT_NUM;

    public DotIndicator(Context context) {
        this(context, null);
    }

    public DotIndicator(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DotIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public DotIndicator init(Context context, int dotNum) {
        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER);
        this.mDotsNum = dotNum == 0 ? DEFAULT_DOT_NUM : dotNum;
        buildDotView(context);
        if (mDotsNum <= 1) {
            setVisibility(GONE);
        } else {
            setVisibility(VISIBLE);
        }
        setCurrentSelection(0);
        return this;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (mDotViews.size() == 0) {
            removeAllViews();
            buildDotView(getContext());
            setCurrentSelection(currentSelection);
        }
    }


    /**
     * 初始化dotview
     */
    private void buildDotView(Context context) {
        mDotViews = new ArrayList<>();
        for (int i = 0; i < mDotsNum; i++) {
            DotView dotView = new DotView(context);
            dotView.setSelected(false);
            LinearLayout.LayoutParams params = new LayoutParams(AppUtils.dip2px(context,DOT_SIZE), AppUtils.dip2px(context,DOT_SIZE));
            if (i == 0) {
                params.leftMargin = 0;
            } else {
                params.leftMargin = AppUtils.dip2px(context,6f);
            }
            addView(dotView, params);
            mDotViews.add(dotView);
        }
    }

    /**
     * 当前选中的dotview
     */
    public void setCurrentSelection(int selection) {
        this.currentSelection = selection;
        for (DotView dotView : mDotViews) {
            dotView.setSelected(false);
        }
        if (selection >= 0 && selection < mDotViews.size()) {
            mDotViews.get(selection).setSelected(true);
        } else {
            Log.e(TAG, "the selection can not over dotViews size");
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mDotViews != null) {
            mDotViews.clear();
        }
    }

}
