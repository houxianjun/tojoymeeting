package com.tojoy.tjoybaselib.ui.adapter;

import android.view.View;

public interface IViewReclaimer {
    /**
     * reclaim view
     *
     * @param view
     */
    void reclaimView(View view);
}
