package com.tojoy.tjoybaselib.ui.vlayout;

import android.util.SparseIntArray;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author qll
 * @date 2019/4/26
 */
public class ViewNodeTypeHelper {
    private static final SparseIntArray VIEW_TYPE_MAPPING = new SparseIntArray(30);

    private static final SparseIntArray VIEW_TYPE_UN_MAPPING = new SparseIntArray(30);

    private static AtomicInteger ENCODED_VIEW_TYPE = new AtomicInteger(1);

    public static int encode(int viewType) {
        int encodedViewType = VIEW_TYPE_MAPPING.get(viewType);
        if (encodedViewType <= 0) {
            encodedViewType = ENCODED_VIEW_TYPE.getAndIncrement();
            VIEW_TYPE_MAPPING.put(viewType, encodedViewType);
            VIEW_TYPE_UN_MAPPING.put(encodedViewType, viewType);
        }
        return encodedViewType;
    }

    public static int decode(int encodedViewType) {
        return VIEW_TYPE_UN_MAPPING.get(encodedViewType);
    }

}
