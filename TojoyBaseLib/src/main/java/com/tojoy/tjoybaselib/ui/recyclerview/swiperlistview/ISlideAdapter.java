package com.tojoy.tjoybaselib.ui.recyclerview.swiperlistview;

import android.view.View;

/**
 * Created by chengyanfang on 2017/10/22.
 */

public interface ISlideAdapter {

    void bindSlideState(View slideTouchView);
    void bindSlidePosition(View slideTouchView , int pos);
    void setOnClickSlideItemListener(OnClickSlideItemListener listener);
    int[] getBindOnClickViewsIds();
    void closeAll();

}
