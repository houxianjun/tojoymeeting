package com.tojoy.tjoybaselib.ui.vlayout.extend;

public interface LayoutManagerCanScrollListener {
    boolean canScrollVertically();

    boolean canScrollHorizontally();
}
