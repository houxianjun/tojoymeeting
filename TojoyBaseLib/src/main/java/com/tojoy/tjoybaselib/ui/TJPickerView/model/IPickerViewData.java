package com.tojoy.tjoybaselib.ui.TJPickerView.model;

/**
 * Created by chengyanfang on 2017/10/9.
 */

public interface IPickerViewData {
    String getPickerViewText();
}