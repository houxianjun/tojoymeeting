package com.tojoy.tjoybaselib.ui.tjrecyclerview;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.R;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.util.log.LogUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;

/**
 * Created by chengyanfang on 2017/9/7.
 */

public class PullRefreshFooter extends FrameLayout {

    private ProgressBar loadingView;
    private TextView mTextView;
    private RelativeLayout mEmptyView;
    private RelativeLayout mLoadingContentView;
    private Context mContext;
    private ImageView mEmptyImageView;
    private TextView mEmptyTextView;

    public PullRefreshFooter(Context context) {
        this(context, null);
    }

    public PullRefreshFooter(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PullRefreshFooter(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView(context);
    }

    private void initView(Context context) {
        View.inflate(context, R.layout.view_ptr_footer, this);
        loadingView = findViewById(R.id.loading);
        mTextView = findViewById(R.id.loadmore_text);
        mEmptyView = findViewById(R.id.rlv_empty);
        mEmptyTextView = findViewById(R.id.empty_tip);
        mEmptyImageView = findViewById(R.id.iv_empty_logo);
        mLoadingContentView = findViewById(R.id.content_layout);
    }


    public void onRefreshing() {
        mTextView.setText(mContext.getResources().getString(R.string.refreshing));
        loadingView.setVisibility(VISIBLE);
    }

    public void setEmptyImgAndTip(int drawable, String tip) {
        mEmptyView.setVisibility(GONE);
        mEmptyTextView.setText(tip);
        mEmptyImageView.setBackgroundResource(drawable);
    }
    public void showEmptyView(){
        mEmptyView.setVisibility(VISIBLE);
    }

    public void hideBottomLoadMoreTip() {
        mTextView.setVisibility(GONE);
    }

    public void setEmptyImgAndTipHaveClickListener(int drawable, String tip) {
        mEmptyTextView.setText(tip);
        mEmptyImageView.setBackgroundResource(drawable);
    }

    //定位失败空界面 点击调到系统设置
    public void setEmptyViewLocationFail(int drawable, String htmlTip) {
        mEmptyTextView.setText(Html.fromHtml(htmlTip.replace("\n", "<br />")));
        mEmptyImageView.setBackgroundResource(drawable);
        mEmptyTextView.setOnClickListener(view -> {
//            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);

            Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
            String pkg = "com.android.settings";
            String cls = "com.android.settings.applications.InstalledAppDetails";
            intent.setComponent(new ComponentName(pkg, cls));
            intent.setData(Uri.parse("package:" + mContext.getPackageName()));
            try {
                mContext.startActivity(intent);
            } catch (Exception e) {
                LogUtil.e("LOC", "start ACTION_LOCATION_SOURCE_SETTINGS error");
            }
        });
    }

    public void refresh(int currentPage, int currentPageCount) {
        if (currentPageCount == AppConfig.PER_PAGE_COUNT) {//可以加载更多
            loadingView.setVisibility(VISIBLE);
            mTextView.setText(mContext.getResources().getString(R.string.refreshing));

            mLoadingContentView.setVisibility(VISIBLE);
            mEmptyView.setVisibility(GONE);
        } else {
            if (currentPage == 1 && currentPageCount == 0) {
                mLoadingContentView.setVisibility(GONE);
                mEmptyView.setVisibility(VISIBLE);
                loadingView.setVisibility(GONE);
            } else {
                mTextView.setText(mContext.getResources().getString(R.string.loadmore_complete));
                mLoadingContentView.setVisibility(VISIBLE);
                loadingView.setVisibility(GONE);
                mEmptyView.setVisibility(GONE);
            }
        }
    }

    public void refresh(int currentPage, int currentPageCount, int perPageSize) {
        if (currentPageCount == perPageSize) {//可以加载更多
            loadingView.setVisibility(VISIBLE);
            mTextView.setText(mContext.getResources().getString(R.string.refreshing));

            mLoadingContentView.setVisibility(VISIBLE);
            mEmptyView.setVisibility(GONE);
        } else {
            if (currentPage == 1 && currentPageCount == 0) {
                mLoadingContentView.setVisibility(GONE);
                mEmptyView.setVisibility(VISIBLE);
                loadingView.setVisibility(GONE);
            } else if (currentPage == 1 && currentPageCount < perPageSize) {
                mLoadingContentView.setVisibility(GONE);
                mEmptyView.setVisibility(GONE);
                loadingView.setVisibility(GONE);
            } else {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        mTextView.setText(mContext.getResources().getString(R.string.loadmore_complete));
                        mLoadingContentView.setVisibility(VISIBLE);
                        loadingView.setVisibility(GONE);
                        mEmptyView.setVisibility(GONE);
                    }
                });
            }
        }
    }


    public void setEmptyTextViewStyle(String color, int size) {
        mEmptyTextView.setTextColor(Color.parseColor(color));
        mEmptyTextView.setTextSize(size);
    }

    /**
     * 1`
     * 动态设置字体和图片的间距
     *
     * @param topMargin
     * @param botMargin
     */
    public void setEmptyTextSpaceMargin(int topMargin, int botMargin) {
        RelativeLayout.LayoutParams layoutParamsTop = (RelativeLayout.LayoutParams) mEmptyImageView.getLayoutParams();
        layoutParamsTop.setMargins(0, AppUtils.dip2px(mContext, topMargin), 0, 0);


        RelativeLayout.LayoutParams layoutParamsBot = (RelativeLayout.LayoutParams) mEmptyTextView.getLayoutParams();
        layoutParamsBot.setMargins(0, AppUtils.dip2px(mContext, botMargin), 0, 0);

    }
}
