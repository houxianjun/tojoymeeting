package com.netease.nim.uikit.greendao;


import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class ChatRoomUserInfoModel {

    @Id
    public String userId;
    public String roomId;
    public String avatar;
    public String userName;
    public String job;
    public String userRole;
    public String isQuit;
    public String isOnline;

    public String getJob() {
        return this.job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoomId() {
        return this.roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getUserRole() {
        return this.userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getIsQuit() {
        return this.isQuit;
    }

    public void setIsQuit(String isQuit) {
        this.isQuit = isQuit;
    }

    public String getIsOnline() {
        return this.isOnline;
    }

    public void setIsOnline(String isOnline) {
        this.isOnline = isOnline;
    }

    @Generated(hash = 1435946951)
    public ChatRoomUserInfoModel(String userId, String roomId, String avatar, String userName,
                                 String job, String userRole, String isQuit, String isOnline) {
        this.userId = userId;
        this.roomId = roomId;
        this.avatar = avatar;
        this.userName = userName;
        this.job = job;
        this.userRole = userRole;
        this.isQuit = isQuit;
        this.isOnline = isOnline;
    }

    @Generated(hash = 1133218068)
    public ChatRoomUserInfoModel() {
    }

    @Override
    public String toString() {
        return userName;
    }
}
