package com.netease.nim.uikit.business.p2p.extension;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLiveOverAttachment;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishDistributionAttachment;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishProjectAttachment;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishQRAttachment;
import com.netease.nim.uikit.business.chatroom.extension.ChatRoomMessageSenstiveAttachment;
import com.netease.nim.uikit.business.chatroom.extension.LRClosingMsgAttachment;
import com.netease.nim.uikit.business.chatroom.extension.LRDrainageAttachment;
import com.netease.nim.uikit.business.chatroom.extension.LRGuestTipsAttachment;
import com.netease.nim.uikit.business.chatroom.extension.LRHelperLeaveAttachment;
import com.netease.nim.uikit.business.chatroom.extension.LRRemoveMsgAttachment;
import com.netease.nim.uikit.business.chatroom.extension.LiveRoomCustomMessageAttachment;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachmentParser;

/**
 * Created by chengyanfang on 2017/8/23.
 * 解析自定义消息
 */

public class CustomAttachParser implements MsgAttachmentParser {

    private static final String KEY_TYPE = "type";
    private static final String KEY_DATA = "data";

    @Override
    public MsgAttachment parse(String json) {
        CustomAttachment attachment = null;

        try {
            JSONObject object = JSON.parseObject(json);
            int type = object.getInteger(KEY_TYPE);

            //V3.0.0兼容iOS端天九云信发送的json串不包含data问题
            JSONObject data;
            if (!object.containsKey(KEY_DATA)) {
                data = object;
            } else {
                data = object.getJSONObject(KEY_DATA);
            }
            switch (type) {

                case CustomAttachmentType.CHAT_ROOM_SENSTIVE://聊天室敏感词提示
                    attachment = new ChatRoomMessageSenstiveAttachment();
                    break;

                case CustomAttachmentType.LIVE_PUBLISH_PROJECT:
                    attachment = new CRMsgLivePublishProjectAttachment();
                    break;

                case CustomAttachmentType.LIVE_PUBLISH_QR:
                    attachment = new CRMsgLivePublishQRAttachment();
                    break;

                case CustomAttachmentType.LIVE_PUBLISH_DISTRUBTION:
                    attachment = new CRMsgLivePublishDistributionAttachment();
                    break;

                case CustomAttachmentType.LIVE_CUSTOM_MESSAGE:
                    attachment = new LiveRoomCustomMessageAttachment();
                    break;

                case CustomAttachmentType.LIVE_OVER_LIVE:
                    attachment = new CRMsgLiveOverAttachment();
                    break;

                case CustomAttachmentType.LIVE_HELPER_LEAVE:
                    attachment = new LRHelperLeaveAttachment();
                    break;

                case CustomAttachmentType.LIVE_DRAINAGE:
                    attachment = new LRDrainageAttachment();
                    break;

                case CustomAttachmentType.LIVE_REMOVE_MESSAGE:
                    attachment = new LRRemoveMsgAttachment();
                    break;

                case CustomAttachmentType.LIVE_GUEST_TIPS:
                    attachment = new LRGuestTipsAttachment();
                    break;

                case CustomAttachmentType.LIVE_CLOSING:
                    attachment = new LRClosingMsgAttachment();
                    break;

                default:
                    attachment = new DefaultCustomAttachment();
                    break;
            }
            attachment.fromJson(data);
        } catch (Exception e) {

        }

        return attachment;
    }

    public static String packData(int type, JSONObject data) {
        JSONObject object = new JSONObject();
        object.put(KEY_TYPE, type);
        if (data != null) {
            object.put(KEY_DATA, data);
        }

        return object.toJSONString();
    }
}
