package com.netease.nim.uikit.business.p2p.viewholder;

import com.netease.nim.uikit.business.session.viewholder.MsgViewHolderText;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.tojoy.tjoybaselib.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.business.p2p.extension.DefaultCustomAttachment;

/**
 * Created by chengyanfang on 2017/8/24.
 */

public class MsgViewHolderDefCustom extends MsgViewHolderText {

    public MsgViewHolderDefCustom(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected String getDisplayText() {
        MsgAttachment attachment = message.getAttachment();
        if(attachment instanceof DefaultCustomAttachment) {
            DefaultCustomAttachment customAttachment = (DefaultCustomAttachment) attachment;
            return "type: " + customAttachment.getType() + ", data: " + customAttachment.getContent();
        }
        return "";
    }
}
