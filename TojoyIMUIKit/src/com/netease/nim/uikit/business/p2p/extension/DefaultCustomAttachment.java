package com.netease.nim.uikit.business.p2p.extension;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by chengyanfang on 2017/8/24.
 */

public class DefaultCustomAttachment  extends CustomAttachment {

    private String content;

    DefaultCustomAttachment() {
        super(0);
    }

    @Override
    protected void parseData(JSONObject data) {
        content = data.toJSONString();
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = null;
        try {
            data = JSONObject.parseObject(content);
        } catch (Exception e) {

        }
        return data;
    }

    public String getContent() {
        return content;
    }
}