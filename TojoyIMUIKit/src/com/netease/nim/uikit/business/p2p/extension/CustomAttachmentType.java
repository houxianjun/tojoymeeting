package com.netease.nim.uikit.business.p2p.extension;

/**
 * Created by chengyanfang on 2017/8/23.
 */

public interface CustomAttachmentType {

    /**
     * 敏感词重新编辑
     */
    int CHAT_ROOM_SENSTIVE = 121;

    /**
     * 直播间发布项目
     */
    int LIVE_PUBLISH_PROJECT = 200;

    /**
     * 结束直播
     */
    int LIVE_OVER_LIVE = 201;

    /**
     * 助手离开直播间
     */
    int LIVE_HELPER_LEAVE = 202;

    /**
     * 直播间内自定义消息
     */
    int LIVE_CUSTOM_MESSAGE = 203;

    /**
     * 直播间被引流
     */
    int LIVE_DRAINAGE = 204;

    /**
     * 删除消息
     */
    int LIVE_REMOVE_MESSAGE = 209;

    /**
     * ***点击感兴趣，***要合作项目，***抢占项目区域
     */
    int LIVE_GUEST_TIPS = 208;

    /**
     * 发布活动（二维码）
     */
    int LIVE_PUBLISH_QR = 220;

    /**
     * 会议推广（被设置为可推广会议）
     */
    int LIVE_PUBLISH_DISTRUBTION = 221;

    /**
     * 企业已禁用，60s 后关闭直播
     */
    int LIVE_CLOSING = 308;
}