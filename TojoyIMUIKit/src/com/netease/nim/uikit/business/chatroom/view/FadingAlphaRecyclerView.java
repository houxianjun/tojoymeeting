package com.netease.nim.uikit.business.chatroom.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by luuzhu on 2019/4/25.
 * 重写getBottomFadingEdgeStrength，解决聊天列表下边渐透明的效果
 */

public class FadingAlphaRecyclerView extends RecyclerView {
    public FadingAlphaRecyclerView(Context context) {
        this(context, null);
    }

    public FadingAlphaRecyclerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FadingAlphaRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected float getBottomFadingEdgeStrength() {
        return 0;
    }
}
