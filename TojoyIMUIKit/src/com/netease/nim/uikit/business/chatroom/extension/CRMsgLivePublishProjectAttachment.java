package com.netease.nim.uikit.business.chatroom.extension;

import com.alibaba.fastjson.JSONObject;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachment;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachmentType;

/**
 * Created by luuzhu on 2019/3/21.
 * 自定义消息 直播间发布项目 普通成员弹个弹窗
 */

public class CRMsgLivePublishProjectAttachment extends CustomAttachment {

    private String isInterest;
    private String coverUrl;
    private String projectId;
    private String title;
    private String productType; //商品类别 1 商品 2广告
    private String createDate;
    private String storeId;// 店铺id
    private String isBuy;//0没买过，其他值都是买过
    private int isDeposit;   //定金：1 需要  0 不需要
    public String agentCompanyCode;//代播":"98821499",
    public String agentId;//代播":6,
    public String agentLive;//代播":1,
    private String KEY_ISINTEREST = "isInterested";
    private String KEY_COVERURL = "productImg1";
    private String KEY_PROJECTID = "productId";
    private String KEY_TITLE = "productName";
    private String KEY_TYPE = "productType";
    private String KEY_CREATEDATE = "createDate";
    private String KEY_SEOREID = "storeId";
    private String KEY_ISBUY = "isBuy";
    private String KEY_ISDEPOSIT = "isDeposit";
    public String KEY_AGENTCOMPANYCODE = "agentCompanyCode";//代播":"98821499",
    public String KEY_AGENTID = "agentId";//代播":6,
    public String KEY_AGENTLIVE = "agentLive";//代播":1,

    public CRMsgLivePublishProjectAttachment() {
        super(CustomAttachmentType.LIVE_PUBLISH_PROJECT);
    }

    @Override
    protected void parseData(JSONObject data) {
        isInterest = data.getString(KEY_ISINTEREST);
        coverUrl = data.getString(KEY_COVERURL);
        projectId = data.getString(KEY_PROJECTID);
        title = data.getString(KEY_TITLE);
        productType = data.getString(KEY_TYPE);
        createDate = data.getString(KEY_CREATEDATE);
        storeId = data.getString(KEY_SEOREID);
        isBuy = data.getString(KEY_ISBUY);
        isDeposit = data.getInteger(KEY_ISDEPOSIT);
        agentCompanyCode = data.getString(KEY_AGENTCOMPANYCODE);
        agentId = data.getString(KEY_AGENTID);
        agentLive = data.getString(KEY_AGENTLIVE);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_ISINTEREST, isInterest);
        data.put(KEY_COVERURL, coverUrl);
        data.put(KEY_PROJECTID, projectId);
        data.put(KEY_TITLE, title);
        data.put(KEY_TYPE, productType);
        data.put(KEY_CREATEDATE, createDate);
        data.put(KEY_SEOREID, storeId);
        data.put(KEY_ISBUY, isBuy);
        data.put(KEY_ISDEPOSIT,isDeposit);
        data.put(KEY_AGENTCOMPANYCODE,agentCompanyCode);
        data.put(KEY_AGENTID,agentId);
        data.put(KEY_AGENTLIVE,agentLive);
        return data;
    }

    @Override
    public String toString() {
        return "CRMsgLivePublishProjectAttachment:{projectId:" + projectId + ",type:" + type + ",title:" + title +
                "agentCompanyCode："+agentCompanyCode + "agentId："+agentId+"agentLive："+agentLive+"}";
    }

    public String getIsInterest() {
        return isInterest;
    }


    public String getCoverUrl() {
        return coverUrl;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getTitle() {
        return title;
    }

    public void setIsInterest(String isInterest) {
        this.isInterest = isInterest;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProductType() {
        if("2".equals(productType)){
            productType = "1";
        }
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getIsBuy() {
        return isBuy;
    }

    public void setIsBuy(String isBuy) {
        this.isBuy = isBuy;
    }

    public int getIsDeposit(){
        return isDeposit;
    }
    public void setIsDeposit(int isDeposit){
        this.isDeposit = isDeposit;
    }

    public String getAgentCompanyCode() {
        return agentCompanyCode;
    }

    public void setAgentCompanyCode(String agentCompanyCode) {
        this.agentCompanyCode = agentCompanyCode;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getAgentLive() {
        return agentLive;
    }

    public void setAgentLive(String agentLive) {
        this.agentLive = agentLive;
    }
}
