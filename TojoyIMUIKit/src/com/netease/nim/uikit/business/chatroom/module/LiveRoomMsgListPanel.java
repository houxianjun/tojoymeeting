package com.netease.nim.uikit.business.chatroom.module;

import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;

import com.netease.nim.uikit.R;
import com.netease.nim.uikit.api.NimUIKit;
import com.netease.nim.uikit.business.chatroom.adapter.ChatRoomMsgAdapter;
import com.netease.nim.uikit.business.chatroom.viewholder.ChatRoomMsgViewHolderBase;
import com.netease.nim.uikit.business.chatroom.viewholder.ChatRoomMsgViewHolderText;
import com.netease.nim.uikit.business.preference.UserPreferences;
import com.netease.nim.uikit.business.session.audio.MessageAudioControl;
import com.netease.nim.uikit.business.session.module.Container;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.ResponseCode;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.ChatRoomServiceObserver;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.msg.attachment.FileAttachment;
import com.netease.nimlib.sdk.msg.constant.MsgDirectionEnum;
import com.netease.nimlib.sdk.msg.constant.MsgStatusEnum;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.model.AttachmentProgress;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tojoy.tjoybaselib.ui.dialog.EasyAlertDialog;
import com.tojoy.tjoybaselib.ui.dialog.EasyAlertDialogHelper;
import com.tojoy.tjoybaselib.ui.recyclerview.adapter.BaseFetchLoadAdapter;
import com.tojoy.tjoybaselib.ui.recyclerview.adapter.IRecyclerView;
import com.tojoy.tjoybaselib.ui.recyclerview.listener.OnItemClickListener;
import com.tojoy.tjoybaselib.ui.recyclerview.loadmore.MsgListFetchLoadMoreView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by luuzhu on 2019/4/18.
 * 直播间消息收发
 */

public class LiveRoomMsgListPanel {

    private static final int MESSAGE_CAPACITY = 500;

    // container
    private Container container;
    private View rootView;
    private Handler uiHandler;

    // message list view
    private RecyclerView messageListView;
    private LinkedList<ChatRoomMessage> items;
    private ChatRoomMsgAdapter adapter;
    private FrameLayout messageListContainer;

    public LiveRoomMsgListPanel(Container container, View rootView) {
        this.container = container;
        this.rootView = rootView;

        init();
    }

    public void onResume() {
        setEarPhoneMode(UserPreferences.isEarPhoneModeEnable(), false);
    }

    public void onPause() {
        MessageAudioControl.getInstance(container.activity).stopAudio();
    }

    public void onDestroy() {
        registerObservers(false);
    }

    public boolean onBackPressed() {
        uiHandler.removeCallbacks(null);
        // 界面返回，停止语音播放
        MessageAudioControl.getInstance(container.activity).stopAudio();
        return false;
    }

    public void reload(Container container) {
        this.container = container;
        if (adapter != null) {
            adapter.clearData();
        }
    }

    private void init() {
        initListView();
        this.uiHandler = new Handler(NimUIKit.getContext().getMainLooper());
        registerObservers(true);
    }

    private void initListView() {
        // RecyclerView
        messageListContainer = rootView.findViewById(R.id.message_activity_list_view_container);
        messageListView = rootView.findViewById(R.id.messageListView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(container.activity);
        messageListView.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setStackFromEnd(true);
        messageListView.requestDisallowInterceptTouchEvent(true);
        messageListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState != RecyclerView.SCROLL_STATE_IDLE) {
                    container.proxy.shouldCollapseInputPanel();
                }
            }
        });
        messageListView.setOverScrollMode(View.OVER_SCROLL_NEVER);

        // adapter
        items = new LinkedList<>();
        adapter = new ChatRoomMsgAdapter(messageListView, items, container);
        adapter.closeLoadAnimation();
        adapter.setFetchMoreView(new MsgListFetchLoadMoreView());
        adapter.setLoadMoreView(new MsgListFetchLoadMoreView());
        adapter.setEventListener(new LiveRoomMsgListPanel.MsgItemEventListener());
        messageListView.setAdapter(adapter);

        messageListView.addOnItemTouchListener(listener);
    }

    public RecyclerView getMessageRecyclerView(){
        messageListContainer.removeAllViews();
        return messageListView;
    }

    /**
     * 新消息来了
     * @param messages
     */
    public void onIncomingMessage(List<ChatRoomMessage> messages, boolean isSmallList) {
        boolean needScrollToBottom = isLastMessageVisible();
        boolean needRefresh = false;
        List<ChatRoomMessage> addedListItems = new ArrayList<>(messages.size());
        for (ChatRoomMessage message : messages) {
            // 保证显示到界面上的消息，来自同一个聊天室
            if (isMyMessage(message)) {
                saveMessage(message);
                addedListItems.add(message);
                needRefresh = true;
            }
        }
        if (needRefresh) {
            adapter.notifyDataSetChanged();
        }

        // incoming messages tip
        ChatRoomMessage lastMsg = messages.get(messages.size() - 1);
        if ((isMyMessage(lastMsg) && needScrollToBottom) || isSmallList) {
            doScrollToBottom();
        }
    }

    private boolean isLastMessageVisible() {
        LinearLayoutManager layoutManager = (LinearLayoutManager) messageListView.getLayoutManager();
        int lastVisiblePosition = layoutManager.findLastCompletelyVisibleItemPosition();
        return lastVisiblePosition >= adapter.getBottomDataPosition();
    }


    // 发送消息后，更新本地消息列表
    public void onMsgSend(ChatRoomMessage message) {
        saveMessage(message);

        adapter.notifyDataSetChanged();
        doScrollToBottom();
    }

    private void saveMessage(final ChatRoomMessage message) {
        if (message == null) {
            return;
        }

        if (items.size() >= MESSAGE_CAPACITY) {
            items.poll();
        }

        items.add(message);
    }

    /**
     * ************************* 观察者 ********************************
     */

    private void registerObservers(boolean register) {
        ChatRoomServiceObserver service = NIMClient.getService(ChatRoomServiceObserver.class);
        service.observeMsgStatus(messageStatusObserver, register);
        service.observeAttachmentProgress(attachmentProgressObserver, register);
    }

    /**
     * 消息状态变化观察者
     */
    private Observer<ChatRoomMessage> messageStatusObserver = (Observer<ChatRoomMessage>) message -> {
        if (isMyMessage(message)) {
            onMessageStatusChange(message);
        }
    };

    /**
     * 消息附件上传/下载进度观察者
     */
    private Observer<AttachmentProgress> attachmentProgressObserver = (Observer<AttachmentProgress>) progress -> onAttachmentProgressChange(progress);

    private void onMessageStatusChange(IMMessage message) {
        int index = getItemIndex(message.getUuid());
        if (index >= 0 && index < items.size()) {
            IMMessage item = items.get(index);
            item.setStatus(message.getStatus());
            item.setAttachStatus(message.getAttachStatus());
            // 处理语音、音视频通话
            if (item.getMsgType() == MsgTypeEnum.audio || item.getMsgType() == MsgTypeEnum.avchat) {
                // 附件可能更新了
                item.setAttachment(message.getAttachment());
            }

            refreshViewHolderByIndex(index);
        }
    }

    private void onAttachmentProgressChange(AttachmentProgress progress) {
        int index = getItemIndex(progress.getUuid());
        if (index >= 0 && index < items.size()) {
            IMMessage item = items.get(index);
            float value = (float) progress.getTransferred() / (float) progress.getTotal();
            adapter.putProgress(item, value);
            refreshViewHolderByIndex(index);
        }
    }

    private boolean isMyMessage(ChatRoomMessage message) {
        return message.getSessionType() == container.sessionType
                && message.getSessionId() != null
                && message.getSessionId().equals(container.account);
    }

    public void refreshMsg() {
        refreshViewHolderByIndex(items.size() - 1);
        doScrollToBottom();
    }

    public void refreshMsgAll() {
        adapter.notifyDataSetChanged();
    }

    /**
     * 根据message id 刷新 把这条消息变成敏感
     * @param uuId
     */
    public void refreshMsgByUuId(String uuId) {
        int size = items.size();
        for (int i = size - 1; i >= 0; i--) {
            ChatRoomMessage chatRoomMessage = items.get(i);
            if (chatRoomMessage.getUuid().equals(uuId)) {

                String showBg = "";
                Map<String, Object> extension = chatRoomMessage.getLocalExtension();
                if (extension != null && !extension.isEmpty()) {
                    showBg = (String) extension.get(ChatRoomMsgViewHolderText.SHOW_BG);
                }

                Map<String, Object> msgMap = new HashMap<>();
                msgMap.put(ChatRoomMsgViewHolderText.SENSTIVE, ChatRoomMsgViewHolderText.SENSTIVE_1);
                msgMap.put(ChatRoomMsgViewHolderText.SHOW_BG, showBg);
                chatRoomMessage.setLocalExtension(msgMap);
                adapter.notifyDataItemChanged(i);
            }
        }
    }

    public void removeMessage(String id){
        int size = items.size();
        for(int i = size - 1; i >= 0 ; i--) {
            ChatRoomMessage chatRoomMessage = items.get(i);
            if(id.equals(chatRoomMessage.getUuid())) {
                adapter.notifyItemRemoved(i);
                items.remove(i);
            }
        }
    }

    public void refreshMsgBg(String state){
        int size = items.size();
        for (int i = size - 1; i >= 0; i--) {
            ChatRoomMessage chatRoomMessage = items.get(i);
            String senstive = "";
            Map<String, Object> extension = chatRoomMessage.getLocalExtension();
            if (extension != null && !extension.isEmpty()) {
                senstive = (String) extension.get(ChatRoomMsgViewHolderText.SENSTIVE);
            }
            Map<String, Object> msgMap = new HashMap<>();
            msgMap.put(ChatRoomMsgViewHolderText.SHOW_BG, state);
            msgMap.put(ChatRoomMsgViewHolderText.SENSTIVE, senstive);
            chatRoomMessage.setLocalExtension(msgMap);
        }
        adapter.notifyDataSetChanged();
    }

    /**
     * 刷新单条消息
     */
    private void refreshViewHolderByIndex(final int index) {
        container.activity.runOnUiThread(() -> {
            if (index < 0) {
                return;
            }
            adapter.notifyDataItemChanged(index);
        });
    }

    private int getItemIndex(String uuid) {
        for (int i = 0; i < items.size(); i++) {
            IMMessage message = items.get(i);
            if (TextUtils.equals(message.getUuid(), uuid)) {
                return i;
            }
        }

        return -1;
    }

    private OnItemClickListener listener = new OnItemClickListener() {
        @Override
        public void onItemClick(IRecyclerView adapter, View view, int position) {

        }

        @Override
        public void onItemLongClick(IRecyclerView adapter, View view, int position) {
        }

        @Override
        public void onItemChildClick(IRecyclerView adapter2, View view, int position) {

        }
    };

    private class MsgItemEventListener implements ChatRoomMsgAdapter.ViewHolderEventListener {

        @Override
        public void onFailedBtnClick(IMMessage message) {
            if (message.getDirect() == MsgDirectionEnum.Out) {
                // 发出的消息，如果是发送失败，直接重发，否则有可能是漫游到的多媒体消息，但文件下载
                if (message.getStatus() == MsgStatusEnum.fail) {
                    // 重发
                    resendMessage(message);
                } else {
                    if (message.getAttachment() instanceof FileAttachment) {
                        FileAttachment attachment = (FileAttachment) message.getAttachment();
                        if (TextUtils.isEmpty(attachment.getPath())
                                && TextUtils.isEmpty(attachment.getThumbPath())) {
                            showReDownloadConfirmDlg(message);
                        }
                    } else {
                        resendMessage(message);
                    }
                }
            } else {
                showReDownloadConfirmDlg(message);
            }
        }

        @Override
        public boolean onViewHolderLongClick(View clickView, View viewHolderView, IMMessage item) {
            return true;
        }

        @Override
        public void onFooterClick(ChatRoomMsgViewHolderBase viewHolderBase, IMMessage message) {
            // 与 robot 对话
            container.proxy.onItemFooterClick(message);
        }

        // 重新下载(对话框提示)
        private void showReDownloadConfirmDlg(final IMMessage message) {
            EasyAlertDialogHelper.OnDialogActionListener listener = new EasyAlertDialogHelper.OnDialogActionListener() {

                @Override
                public void doCancelAction() {
                }

                @Override
                public void doOkAction() {
                    // 正常情况收到消息后附件会自动下载。如果下载失败，可调用该接口重新下载
                    if (message.getAttachment() != null && message.getAttachment() instanceof FileAttachment)
                        NIMClient.getService(ChatRoomService.class).downloadAttachment((ChatRoomMessage) message, true);
                }
            };

            final EasyAlertDialog dialog = EasyAlertDialogHelper.createOkCancelDiolag(container.activity, null,
                    container.activity.getString(R.string.repeat_download_message), true, listener);
            dialog.show();
        }

        // 重发消息到服务器
        private void resendMessage(IMMessage message) {
            // 重置状态为unsent
            int index = getItemIndex(message.getUuid());
            if (index >= 0 && index < items.size()) {
                IMMessage item = items.get(index);
                item.setStatus(MsgStatusEnum.sending);
                refreshViewHolderByIndex(index);
            }

            NIMClient.getService(ChatRoomService.class).sendMessage((ChatRoomMessage) message, true);
        }
    }

    private void setEarPhoneMode(boolean earPhoneMode, boolean update) {
        if (update) {
            UserPreferences.setEarPhoneModeEnable(earPhoneMode);
        }
        MessageAudioControl.getInstance(container.activity).setEarPhoneModeEnable(earPhoneMode);
    }

    public void scrollToBottom() {
        uiHandler.postDelayed(() -> doScrollToBottom(), 200);
    }

    private void doScrollToBottom() {
        messageListView.scrollToPosition(adapter.getBottomDataPosition());
    }
}
