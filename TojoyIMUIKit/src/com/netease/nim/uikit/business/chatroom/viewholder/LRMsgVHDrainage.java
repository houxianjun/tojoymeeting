package com.netease.nim.uikit.business.chatroom.viewholder;

import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.R;
import com.netease.nim.uikit.business.chatroom.extension.LRDrainageAttachment;
import com.tojoy.tjoybaselib.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;

import java.util.Map;

/**
 * Created by luuzhu on 2019/7/10.
 * 自定义消息- 直播间被引流
 */

public class LRMsgVHDrainage extends ChatRoomMsgViewHolderBase {

    private TextView tvInfo;
    private RelativeLayout rootLayout;

    public LRMsgVHDrainage(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected int getContentResId() {
        return R.layout.live_room_message_helper_leave;
    }

    @Override
    protected void inflateContentView() {
        tvInfo = view.findViewById(R.id.tvInfo);
        rootLayout = view.findViewById(R.id.rootLayout);
    }

    //是否显示昵称
    @Override
    protected boolean shouldDisplayNick() {
        return false;
    }

    //是否居中
    @Override
    protected boolean isMiddleItem() {
        return false;
    }

    @Override
    protected boolean isShowBubble() {
        return false;
    }

    @Override
    protected boolean isShowHeadImage() {
        return false;
    }

    @Override
    protected void bindContentView() {
        LRDrainageAttachment attachment = (LRDrainageAttachment) message.getAttachment();
        String info = attachment.getInfo();
        tvInfo.setText(info);

        //处理直播间大小聊天列表 消息背景的显示隐藏
        Map<String, Object> localextension = message.getLocalExtension();
        if (localextension != null && !localextension.isEmpty()) {
            String showState = (String) localextension.get(ChatRoomMsgViewHolderText.SHOW_BG);
            if (ChatRoomMsgViewHolderText.SHOW_BG_STATE_SHOW.equals(showState)) {
                rootLayout.setBackgroundResource(R.drawable.bg_live_room_join_msg);
            } else if (ChatRoomMsgViewHolderText.SHOW_BG_STATE_GONE.equals(showState)) {
                rootLayout.setBackgroundResource(0);
            }
        }
    }

    @Override
    protected void onItemClick() {
    }

}
