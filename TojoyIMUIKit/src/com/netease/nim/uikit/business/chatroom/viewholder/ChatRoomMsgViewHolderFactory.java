package com.netease.nim.uikit.business.chatroom.viewholder;

import com.netease.nim.uikit.business.chatroom.extension.CRMsgLiveOverAttachment;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishProjectAttachment;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishQRAttachment;
import com.netease.nim.uikit.business.chatroom.extension.ChatRoomMessageSenstiveAttachment;
import com.netease.nim.uikit.business.chatroom.extension.LRClosingMsgAttachment;
import com.netease.nim.uikit.business.chatroom.extension.LRDrainageAttachment;
import com.netease.nim.uikit.business.chatroom.extension.LRGuestTipsAttachment;
import com.netease.nim.uikit.business.chatroom.extension.LRHelperLeaveAttachment;
import com.netease.nim.uikit.business.chatroom.extension.LRRemoveMsgAttachment;
import com.netease.nim.uikit.business.chatroom.extension.LiveRoomCustomMessageAttachment;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 聊天室消息项展示ViewHolder工厂类。
 */
public class ChatRoomMsgViewHolderFactory {

    private static HashMap<Class<? extends MsgAttachment>, Class<? extends ChatRoomMsgViewHolderBase>> viewHolders =
            new HashMap<>();

    static {

        //新增 聊天室自定义消息-敏感词提示
        register(ChatRoomMessageSenstiveAttachment.class, ChatRoomMsgviewHolderSenstive.class);

        //直播间发布项目
        register(CRMsgLivePublishProjectAttachment.class, null);
        //直播间发布活动
        register(CRMsgLivePublishQRAttachment.class, null);
        register(LiveRoomCustomMessageAttachment.class, null);
        //结束直播
        register(CRMsgLiveOverAttachment.class, null);
        //直播间助手离开
        register(LRHelperLeaveAttachment.class, LRMsgviewHolderHelperLeave.class);
        //直播间引流
        register(LRDrainageAttachment.class, LRMsgVHDrainage.class);


        //直播间删除某条消息
        register(LRRemoveMsgAttachment.class, null);

        //***点击感兴趣，***要合作项目，***抢占项目区域
        register(LRGuestTipsAttachment.class, null);

        //隐形自定义消息 企业已禁用，60s 后关闭直播
        register(LRClosingMsgAttachment.class, null);
    }

    public static void register(Class<? extends MsgAttachment> attach, Class<? extends ChatRoomMsgViewHolderBase> viewHolder) {
        viewHolders.put(attach, viewHolder);
    }

    public static Class<? extends ChatRoomMsgViewHolderBase> getViewHolderByType(ChatRoomMessage message) {
        if (message.getMsgType() == MsgTypeEnum.text) {
            return ChatRoomMsgViewHolderText.class;
        } else {
            Class<? extends ChatRoomMsgViewHolderBase> viewHolder = null;

            MsgAttachment attachment = message.getAttachment();

            if (attachment != null) {
                Class<? extends MsgAttachment> clazz = attachment.getClass();
                while (viewHolder == null && clazz != null) {
                    viewHolder = viewHolders.get(clazz);
                    if (viewHolder == null) {
                        clazz = getSuperClass(clazz);
                    }
                }
            }
            return viewHolder == null ? ChatRoomMsgViewHolderUnknown.class : viewHolder;
        }
    }

    private static Class<? extends MsgAttachment> getSuperClass(Class<? extends MsgAttachment> derived) {
        Class sup = derived.getSuperclass();
        if (sup != null && MsgAttachment.class.isAssignableFrom(sup)) {
            return sup;
        } else {
            for (Class itf : derived.getInterfaces()) {
                if (MsgAttachment.class.isAssignableFrom(itf)) {
                    return itf;
                }
            }
        }
        return null;
    }

    public static List<Class<? extends ChatRoomMsgViewHolderBase>> getAllViewHolders() {
        List<Class<? extends ChatRoomMsgViewHolderBase>> list = new ArrayList<>();
        list.addAll(viewHolders.values());
        list.add(ChatRoomMsgViewHolderUnknown.class);
        list.add(ChatRoomMsgViewHolderText.class);

        return list;
    }
}
