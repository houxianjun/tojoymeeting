package com.netease.nim.uikit.business.chatroom.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.netease.nim.uikit.R;
import com.netease.nim.uikit.business.chatroom.viewholder.ChatRoomMsgViewHolderBase;
import com.netease.nim.uikit.business.chatroom.viewholder.ChatRoomMsgViewHolderFactory;
import com.netease.nim.uikit.business.session.module.Container;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tojoy.tjoybaselib.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.tojoy.tjoybaselib.ui.recyclerview.holder.BaseViewHolder;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by huangjun on 2016/12/21.
 */
public class ChatRoomMsgAdapter extends BaseMultiItemFetchLoadAdapter<ChatRoomMessage, BaseViewHolder> {

    private Map<Class<? extends ChatRoomMsgViewHolderBase>, Integer> holder2ViewType;

    private ViewHolderEventListener eventListener;
    /** 有文件传输，需要显示进度条的消息ID map */
    private Map<String, Float> progresses;
    private String messageId;
    private Container container;

    public ChatRoomMsgAdapter(RecyclerView recyclerView, List<ChatRoomMessage> data, Container container) {
        super(recyclerView, data);

        timedItems = new HashSet<>();
        progresses = new HashMap<>();

        // view type, view holder
        holder2ViewType = new HashMap<>();
        List<Class<? extends ChatRoomMsgViewHolderBase>> holders = ChatRoomMsgViewHolderFactory.getAllViewHolders();
        int viewType = 0;
        for (Class<? extends ChatRoomMsgViewHolderBase> holder : holders) {
            viewType++;
            addItemType(viewType, R.layout.nim_message_item_chat_room, holder);
            holder2ViewType.put(holder, viewType);
        }

        this.container = container;
    }

    @Override
    protected int getViewType(ChatRoomMessage message) {
        return holder2ViewType.get(ChatRoomMsgViewHolderFactory.getViewHolderByType(message));
    }

    @Override
    protected String getItemKey(ChatRoomMessage item) {
        return item.getUuid();
    }

    public void setEventListener(ViewHolderEventListener eventListener) {
        this.eventListener = eventListener;
    }

    public ViewHolderEventListener getEventListener() {
        return eventListener;
    }

    public void putProgress(IMMessage message, float progress) {
        progresses.put(message.getUuid(), progress);
    }


    /** 需要显示消息时间的消息ID */
    private Set<String> timedItems;
    public boolean needShowTime(IMMessage message) {
        return timedItems.contains(message.getUuid());
    }

    public interface ViewHolderEventListener {
        // 长按事件响应处理
        boolean onViewHolderLongClick(View clickView, View viewHolderView, IMMessage item);

        // 发送失败或者多媒体文件下载失败指示按钮点击响应处理
        void onFailedBtnClick(IMMessage resendMessage);

        // viewholder footer按钮点击，如机器人继续会话
        void onFooterClick(ChatRoomMsgViewHolderBase holderBase, IMMessage message);
    }

    public String getUuid() {
        return messageId;
    }

    public Container getContainer() {
        return container;
    }
}
