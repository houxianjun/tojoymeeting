package com.netease.nim.uikit.business.chatroom.extension;

import com.alibaba.fastjson.JSONObject;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachment;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachmentType;

/**
 * Created by luuzhu on 2019/5/22.
 * 自定义消息-直播间助手退出聊天室
 */

public class LRHelperLeaveAttachment extends CustomAttachment {

    private String targetName;
    private String targetId;

    private String KEY_TARGET_NAME = "targetName";

    public LRHelperLeaveAttachment() {
        super(CustomAttachmentType.LIVE_HELPER_LEAVE);
    }

    @Override
    protected void parseData(JSONObject data) {
        targetName = data.getString(KEY_TARGET_NAME);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_TARGET_NAME, targetName);
        return data;
    }

    public String getTargetName() {
        return targetName;
    }

    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }
}
