package com.netease.nim.uikit.business.chatroom.view;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.alibaba.android.arouter.launcher.ARouter;
import com.netease.nim.uikit.R;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishProjectAttachment;
import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.enterprise.CorporationMsgResponse;
import com.tojoy.tjoybaselib.model.live.LiveShareInfoModel;
import com.tojoy.tjoybaselib.model.live.LiveUserInfoResponse;
import com.tojoy.tjoybaselib.model.shopmall.ShopMallListResponse;
import com.tojoy.tjoybaselib.model.shopmall.ShopMallModel;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.common.LiveRoom.LiveSharePopview;
import com.tojoy.common.LiveRoom.ProjectTJDialog;
import com.tojoy.common.Services.EventBus.LiveRoomEvent;
import com.tojoy.live.tenxunyun.event.VideoPlayerIsPlayingStateEvent;
import com.tojoy.live.tenxunyun.listener.OnTopVideoListener;
import com.tojoy.live.tenxunyun.TxVideoPlayer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 视屏播放页面
 * created by fanxi
 */
public class TJVideoPlayerAct extends UI {
    private TxVideoPlayer mVideoPlayer;
    /**
     * 判断当前的页面是否隐藏
     */
    private boolean isActivityShowState = true;
    /**
     * 判断当前视屏播放的状态是否是在播放
     * 1代表暂停中
     * 2代表播放中
     */
    private int curPlayerIsPlaying = -1;

    //直播回放的id
    private String mLiveId;
    //项目推荐弹窗
    private ProjectTJDialog mProjectTJDialog;
    private ArrayList<CRMsgLivePublishProjectAttachment> mProjects;
    private String mCompanyCode;

    //私密会议时使用判断是否显示分享按钮
    private String isHideShareBtn = "";

    public static void start(Context activity, String url, String liveId, String companyCode) {
        Intent intent = new Intent();
        intent.setClass(activity, TJVideoPlayerAct.class);
        intent.putExtra("url", url);
        intent.putExtra("liveId", liveId);
        intent.putExtra("companyCode", companyCode);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room_video);
        EventBus.getDefault().register(this);
        initData();
        initView();
        initTitle();

        EventBus.getDefault().post(new LiveRoomEvent(LiveRoomEvent.LIVE_CLOSE_ALL, ""));
    }

    private void initData() {
        if (getIntent().hasExtra("liveId")) {
            mLiveId = getIntent().getStringExtra("liveId");
            if (!TextUtils.isEmpty(mLiveId)) {
                getProjectTJ(true);
            }
        }
        if (getIntent().hasExtra("companyCode")) {
            mCompanyCode = getIntent().getStringExtra("companyCode");
        }
        judgeHomePage();
    }


    private void initView() {
        mVideoPlayer = (TxVideoPlayer) findViewById(R.id.video_player);
        // 聊天室视屏播放模块
        initChatRoomVideoPlayer();
        //网洽会直播回放功能模块
        if (mLiveId != null) {
            //获取直播间信息，用于判断分享按钮是否隐藏
            getLiveRoomInfo();
        }
    }

    /**
     * 聊天室视屏播放模块
     */
    private void initChatRoomVideoPlayer() {
        //设置视频地址和请求头部
        mVideoPlayer.setVideoUrl(getIntent().getStringExtra("url"), 2);
        //创建视频控制器
        mVideoPlayer.isShowFullScreenLayout(false);
        mVideoPlayer.isAlwaysShowTop(true);
        mVideoPlayer.autoStart();
    }

    /**
     * 网洽会直播回放功能模块
     */
    private void initLiveReplayVideoPLayer() {

        /**
         * 根据isHideShare字段判断是否显示顶部分享按钮
         */
        if (!TextUtils.isEmpty(isHideShareBtn) && "0".equals(isHideShareBtn)) {
            // 0 代表隐藏
            mVideoPlayer.setTopShareBtnVisibility(View.GONE);
        } else {
            //字段状态为null 或 ""
            mVideoPlayer.setTopShareBtnVisibility(View.VISIBLE);
        }

        mVideoPlayer.setOnTopVideoListener(new OnTopVideoListener() {
            @Override
            public void clickExitVideoBackIm() {
                finish();
            }

            @Override
            public void clickShareImg() {
                //分享功能
                if (mLiveId == null) {
                    return;
                }
                showProgressHUD(TJVideoPlayerAct.this, "处理中");
                OMAppApiProvider.getInstance().getShareInfo(mLiveId, new Observer<LiveShareInfoModel>() {
                    @Override
                    public void onCompleted() {
                        dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(TJVideoPlayerAct.this, "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(LiveShareInfoModel model) {
                        if (model.isSuccess()) {
                            // 允许分享
                            LiveShareInfoModel.DataObjBean.ShareInfoBean shareInfo = model.data.shareInfo;
                            LiveSharePopview wxPopView = new LiveSharePopview(TJVideoPlayerAct.this, LiveSharePopview.STYLE_BLACK);
                            wxPopView.setLiveShareData(OSSConfig.getOSSURLedStr(shareInfo.shareImg),
                                    shareInfo.shareUrl + ((!TextUtils.isEmpty(shareInfo.shareUrl) && shareInfo.shareUrl.contains("roomLiveId")) ? "" : "?roomLiveId=" + LiveRoomInfoProvider.getInstance().liveId),
                                    model.data.liveInfo.title, shareInfo.shareDesc, model.data);
                            wxPopView.show();

                        } else {
                            Toasty.normal(TJVideoPlayerAct.this, model.msg).show();
                        }
                    }
                });
            }

            @Override
            public void clickLeftList() {//点击列表
                if (mProjectTJDialog != null && mProjectTJDialog.isShowing()) {
                    return;
                }
                if (FastClickAvoidUtil.isDoubleClick()) {
                    return;
                }
                getProjectTJ(false);
            }

            @Override
            public void clickEnterpriseHome() {
                ARouter.getInstance().build(RouterPathProvider.OnlineMeetingCustomizedEnterprisePageAct)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .withString("companyCode", mCompanyCode)
                        .withString("sourceWay","1")
                        .navigation();

            }
        });
    }

    /**
     * 私密会需求时新增的方法：获取直播间信息
     */
    public void getLiveRoomInfo() {

        OMAppApiProvider.getInstance().getMemberInfo(BaseUserInfoCache.getUserId(this), mLiveId, new Observer<LiveUserInfoResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                isHideShareBtn = "";
                initLiveReplayVideoPLayer();
                Toasty.normal(TJVideoPlayerAct.this, "网络异常，请检查网络").show();
            }

            @Override
            public void onNext(LiveUserInfoResponse response) {

                final boolean next = response != null && response.isSuccess() &&
                        response.data != null && response.data.isHideShare != null;
                if (next) {
                    //新增保存是否显示分享按钮字段
                    isHideShareBtn = response.data.isHideShare;
                }
                initLiveReplayVideoPLayer();
            }
        });
    }


    /**
     * 项目推荐
     */
    private void getProjectTJ(boolean isFirst) {

        OMAppApiProvider.getInstance().getGoodsTJ(BaseUserInfoCache.getUserId(TJVideoPlayerAct.this), mLiveId, "", "1", new Observer<ShopMallListResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(ShopMallListResponse response) {
                if (response.isSuccess()) {
                    List<ShopMallModel> list = response.data.list;
                    int size = list.size();

                    if (size == 0) {
                        return;
                    }

                    mProjects = new ArrayList<>();
                    mVideoPlayer.mCickList.setVisibility(View.VISIBLE);
                    for (int i = 0; i < size; i++) {
                        ShopMallModel bean = list.get(i);
                        CRMsgLivePublishProjectAttachment attachment = new CRMsgLivePublishProjectAttachment();
                        attachment.setTitle(bean.goodsName);
                        attachment.setProjectId(bean.goodsId);
                        attachment.setCoverUrl(bean.goodsImg1);
                        attachment.setIsInterest(bean.isInterested);
                        attachment.setStoreId(bean.storeId);
                        attachment.setIsBuy(bean.isBuy);
                        attachment.setIsDeposit(bean.isDeposit);
                        attachment.setAgentCompanyCode(bean.agentCompanyCode);
                        attachment.setAgentId(bean.agentId);
                        attachment.setAgentId(bean.agentLive);
                        mProjects.add(attachment);
                    }

                    if (!isFirst) {
                        mProjectTJDialog = new ProjectTJDialog(TJVideoPlayerAct.this, mProjects);
                        mProjectTJDialog.show();
                    }
                }
            }
        });
    }

    /**
     * 判断是否有定制首页
     */
    private void judgeHomePage() {
        OMAppApiProvider.getInstance().mCorporationQuery(mCompanyCode, false, new rx.Observer<CorporationMsgResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(CorporationMsgResponse response) {
                if (response.isSuccess()) {
                    if (response.data.customState == 1) {
                        mVideoPlayer.mIvHome.setVisibility(View.VISIBLE);
                    } else {
                        mVideoPlayer.mIvHome.setVisibility(View.GONE);

                    }
                } else {
                    if (!TextUtils.isEmpty(response.code) && "-2".equals(response.code)) {
                        Toasty.normal(TJVideoPlayerAct.this, response.msg).show();
                    }
                }
            }
        });
    }


    private void initTitle() {
        setStatusBar();
        setUpNavigationBar();
        setStatusBarLightMode();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mVideoPlayer != null) {
            mVideoPlayer.releaseVideoSource();
        }
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mVideoPlayer != null) {
            mVideoPlayer.pauseVideo();
        }

        isActivityShowState = false;
        if (mVideoPlayer != null) {
            if (mVideoPlayer.isPlaying()) {
                mVideoPlayer.pauseVideo();
                curPlayerIsPlaying = 1;
            } else {
                curPlayerIsPlaying = 2;

            }
        }
        /**
         *后台需要时长统计
         */
        collectWatchReplayVideoTime();

    }

    private void collectWatchReplayVideoTime() {
        LiveRoomIOHelper.leaveLiveRoom(this, new IOLiveRoomListener() {
            @Override
            public void onIOError(String error, int errorCode) {
            }

            @Override
            public void onIOSuccess() {
            }
        }, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mVideoPlayer != null && !mVideoPlayer.isPlaying() && !isActivityShowState) {
            mVideoPlayer.restartVideo();
        }
        isActivityShowState = true;
    }

    /**
     * 判断当前视屏是否是在播放中
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getCurPlayerPlayingState(VideoPlayerIsPlayingStateEvent event) {
        if (event != null) {
            if (event.PlayingState == 1 && !isActivityShowState && curPlayerIsPlaying == 2) {
                mHandler.postDelayed(() -> {
                    if (mVideoPlayer != null) {
                        mVideoPlayer.pauseVideo();
                    }
                }, 10);
            }
        }
        curPlayerIsPlaying = -1;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPublicStringEvent(LiveRoomEvent event) {

        if (LiveRoomEvent.LIVE_CLOSE_GUEST.equals(event.getType())) {
            finish();
        }
    }


}
