package com.netease.nim.uikit.business.chatroom.extension;

import com.alibaba.fastjson.JSONObject;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachment;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachmentType;

/**
 * Created by qll on 2020/7/17.
 * 自定义消息 直播间 被设置为可分销会议
 */

public class CRMsgLivePublishDistributionAttachment extends CustomAttachment {

    /** 是否参与推广 1参与，0不参与 */
    private int ifDistribution;
    /** 本场次会议最大分销额预算 */
    private String maxBudget;
    /** 一级分销员佣金 */
    private String level1Commission;
    /** 二级分销员佣金 */
    private String level2Commission;
    /** 一级员工分销额佣金 */
    private String level1CommissionEmployee;
    /** 二级员工分销额佣金 */
    private String level2CommissionEmployee;
    /** 最少参会人数 */
    private String minPerson;
    /** 最低时长 */
    private String minViewTime;


    private String KEY_IF_DISTRIBUTION = "ifDistribution";
    private String KEY_MAX_BUDGET = "maxBudget";
    private String KEY_LEVEL_ONE_COMMISSION = "level1Commission";
    private String KEY_LEVEL_TWO_COMMISSION = "level2Commission";
    private String KEY_LEVEL_ONE_COMMISSION_EMPLOYEE = "level1CommissionEmployee";
    private String KEY_LEVEL_TWO_COMMISSION_EMPLOYEE = "level2CommissionEmployee";
    private String KEY_MIN_PERSION = "minPerson";
    private String KEY_MIN_VIEW_TIME = "minViewTime";

    public CRMsgLivePublishDistributionAttachment() {
        super(CustomAttachmentType.LIVE_PUBLISH_DISTRUBTION);
    }

    @Override
    protected void parseData(JSONObject data) {
        JSONObject entity = data.getJSONObject("entity");
        ifDistribution = entity.getInteger(KEY_IF_DISTRIBUTION);
        maxBudget = entity.getString(KEY_MAX_BUDGET);
        level1Commission = entity.getString(KEY_LEVEL_ONE_COMMISSION);
        level2Commission = entity.getString(KEY_LEVEL_TWO_COMMISSION);
        level1CommissionEmployee = entity.getString(KEY_LEVEL_ONE_COMMISSION_EMPLOYEE);
        level2CommissionEmployee = entity.getString(KEY_LEVEL_TWO_COMMISSION_EMPLOYEE);
        minPerson = entity.getString(KEY_MIN_PERSION);
        minViewTime = entity.getString(KEY_MIN_VIEW_TIME);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_IF_DISTRIBUTION, ifDistribution);
        data.put(KEY_MAX_BUDGET, maxBudget);
        data.put(KEY_LEVEL_ONE_COMMISSION, level1Commission);
        data.put(KEY_LEVEL_TWO_COMMISSION, level2Commission);
        data.put(KEY_LEVEL_ONE_COMMISSION_EMPLOYEE, level1CommissionEmployee);
        data.put(KEY_LEVEL_TWO_COMMISSION_EMPLOYEE, level2CommissionEmployee);
        data.put(KEY_MIN_PERSION, minPerson);
        data.put(KEY_MIN_VIEW_TIME, minViewTime);
        return data;
    }

    @Override
    public String toString() {
        return "CRMsgLivePublishDistributionAttachment:{ifDistribution:" + ifDistribution + ",type:" + type + ",maxBudget:" + maxBudget + ",level1Commission:" + level1Commission + ",level2Commission:" + level2Commission + "}";
    }

    public int getIfDistribution() {
        return ifDistribution;
    }

    public String getMaxBudget() {
        return maxBudget;
    }

    public String getLevel1Commission() {
        return level1Commission;
    }

    public String getLevel2Commission() {
        return level2Commission;
    }

    public String getLevel1CommissionEmployee() {
        return level1CommissionEmployee;
    }

    public String getLevel2CommissionEmployee() {
        return level2CommissionEmployee;
    }

    public String getMinPerson() {
        return minPerson;
    }

    public String getMinViewTime() {
        return minViewTime;
    }
}
