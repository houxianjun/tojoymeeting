package com.netease.nim.uikit.business.chatroom.extension;

import com.alibaba.fastjson.JSONObject;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachment;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachmentType;

public class LiveRoomCustomMessageAttachment extends CustomAttachment {

    private String ext;

    private String KEY_EXT = "ext";

    public LiveRoomCustomMessageAttachment() {
        super(CustomAttachmentType.LIVE_CUSTOM_MESSAGE);
    }

    @Override
    protected void parseData(JSONObject data) {
        ext = data.getString(KEY_EXT);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_EXT, ext);
        return data;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getExt() {
        return ext;
    }
}
