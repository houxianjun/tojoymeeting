package com.netease.nim.uikit.business.chatroom.module;

/**
 * Created by luuzhu on 2019/4/29.
 */

public class LiveKickedOutBean {
    public String template;
    public String action;
    public String type;
    public String content;
}
