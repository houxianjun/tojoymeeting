package com.netease.nim.uikit.business.chatroom.module;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.netease.nim.uikit.business.session.actions.BaseAction;
import com.netease.nim.uikit.business.session.module.Container;
import com.netease.nim.uikit.business.session.module.input.InputPanel;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.msg.model.IMMessage;

import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * Created by luuzhu on 2019/4/18.
 */

public class LiveRoomInputPanel extends InputPanel {

    public LiveRoomInputPanel(Activity context, Container container, View view, List<BaseAction> actions, boolean isTextAudioSwitchShow) {
        super(context, container, view, actions, isTextAudioSwitchShow);
    }

    public LiveRoomInputPanel(Activity context, Container container, View view, List<BaseAction> actions) {
        super(context, container, view, actions, false);
    }

    @Override
    protected IMMessage createTextMessage(String text) {
        return ChatRoomMessageBuilder.createChatRoomTextMessage(container.account, text);
    }

    @Override
    protected void onTextMessageSendButtonPressed(TextView tvBtn) {
        if ("发送".contains(sendMessageButtonInInputBar.getText().toString())) {
            if (LiveRoomInfoProvider.getInstance().isMute) {
                Toasty.normal(mContext, "你已被主播禁言").show();
                return;
            } else if(LiveRoomInfoProvider.getInstance().isMuteAll && !LiveRoomInfoProvider.getInstance().isHost()) {
//                LiveRoomHelper.getInstance().sendMuteAllMsg(true);
                Toasty.normal(mContext, "全员禁言已开启").show();
                return;
            }
        }
        super.onTextMessageSendButtonPressed(tvBtn);
    }
}

