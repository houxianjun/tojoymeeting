package com.netease.nim.uikit.business.chatroom.extension;

import com.alibaba.fastjson.JSONObject;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachment;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachmentType;

/**
 * Created by luuzhu on 2019/11/11.
 * 隐形自定义消息 删除某条消息
 */

public class LRRemoveMsgAttachment extends CustomAttachment {

    private String msgIdStr;

    private String KEY_MSG_ID = "msgIdStr";

    public LRRemoveMsgAttachment() {
        super(CustomAttachmentType.LIVE_REMOVE_MESSAGE);
    }

    @Override
    protected void parseData(JSONObject data) {
        msgIdStr = data.getString(KEY_MSG_ID);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_MSG_ID, msgIdStr);
        return data;
    }

    public String getMsgIdStr() {
        return msgIdStr;
    }
}
