package com.netease.nim.uikit.business.chatroom.viewholder;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.R;
import com.netease.nim.uikit.business.liveroom.LiveRoomHelper;
import com.netease.nim.uikit.business.session.emoji.MoonUtil;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessageExtension;
import com.tojoy.tjoybaselib.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.tojoy.tjoybaselib.util.sys.AppUtils;

import java.util.Map;

/**
 * Created by hzxuwen on 2016/1/18.
 */
public class ChatRoomMsgViewHolderText extends ChatRoomMsgViewHolderBase {

    /**
     * 固定成员（3）发送文本消息的扩展消息key，用于判断这条消息的状态
     */
    public static final String SENSTIVE = "senstive";
    /**
     * loding状态，转菊花
     */
    public static final String SENSTIVE_0 = "0";
    /**
     * 审核后被后台下发通知确认为敏感词的状态，显示黄色叹号，点击叹号插入一条重新编辑的本地消息，点击重新编辑把这条敏感的信息文本带入输入框
     */
    public static final String SENSTIVE_1 = "1";
    /**
     * 调后台接口失败，可重发状态，显示红色叹号，点击叹号重发，也就是重新调后台发送接口
     */
    public static final String SENSTIVE_2 = "2";
    /**
     * 调接口后后台审核通过，隐藏一切菊花和叹号
     */
    public static final String SENSTIVE_3 = "3";

    /**
     * 针对直播间大小聊天列表框消息背景的显示隐藏
     */
    public static final String SHOW_BG = "show_bg";
    public static final String SHOW_BG_STATE_SHOW = "1";
    public static final String SHOW_BG_STATE_GONE = "0";

    protected ImageView ivSensitiveWords;//敏感词的感叹号
    protected ImageView ivSendAgain;//重发感叹号
    protected ProgressBar progressBarLoading;
    private RelativeLayout chatRoomContainer;//聊天室文本
    private RelativeLayout rootView;

    private TextView bodyTextViewLive;//直播间文本
    private ViewGroup.LayoutParams layoutParams;

    public ChatRoomMsgViewHolderText(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected int getContentResId() {
        return R.layout.nim_text_item_layout;
    }

    @Override
    protected void inflateContentView() {
        ivSensitiveWords = findViewById(R.id.ivSensitiveWords);
        ivSendAgain = findViewById(R.id.ivSendAgain);
        progressBarLoading = findViewById(R.id.progressBarLoading);
        bodyTextViewLive = findViewById(R.id.nim_message__text_body_live);
        chatRoomContainer = findViewById(R.id.chatRoomContainer);
        rootView = findViewById(R.id.rootView);
    }

    protected String getDisplayText() {
        if(message == null) {
            return "";
        }
        return message.getContent();
    }

    @Override
    protected boolean shouldDisplayNick() {
        return false;
    }

    @Override
    protected boolean isShowBubble() {
        return false;
    }

    @Override
    protected boolean isMiddleItem() {
        return false;
    }

    @Override
    protected boolean isShowHeadImage() {
        return false;
    }

    @Override
    protected void bindContentView() {

        String name = "";
        ChatRoomMessageExtension chatRoomMessageExtension = message.getChatRoomMessageExtension();
        if (chatRoomMessageExtension != null) {
            Map<String, Object> senderExtension = chatRoomMessageExtension.getSenderExtension();
            if (senderExtension != null) {
                name = (String) senderExtension.get("nickName");
            }
        }

        if (TextUtils.isEmpty(name)) {
            name = ChatRoomViewHolderHelper.getNameText(message);
        }
        if (TextUtils.isEmpty(name)) {
            name = message.getFromNick();
        }
        if(TextUtils.isEmpty(name)) {
            name = getNameText();
        }

        Map<String, Object> extension = message.getLocalExtension();
        if (extension != null && !extension.isEmpty()) {
            String senstive = (String) extension.get(SENSTIVE);
            if (!TextUtils.isEmpty(senstive)) {
                switch (senstive) {

                    case SENSTIVE_0:
                        progressBarLoading.setVisibility(View.VISIBLE);
                        ivSensitiveWords.setVisibility(View.GONE);
                        ivSendAgain.setVisibility(View.GONE);
                        break;

                    case SENSTIVE_1:
                        progressBarLoading.setVisibility(View.GONE);
                        ivSensitiveWords.setVisibility(View.VISIBLE);
                        ivSendAgain.setVisibility(View.GONE);
                        break;

                    case SENSTIVE_2:
                        progressBarLoading.setVisibility(View.GONE);
                        ivSensitiveWords.setVisibility(View.GONE);
                        ivSendAgain.setVisibility(View.VISIBLE);
                        break;

                    case SENSTIVE_3:
                        progressBarLoading.setVisibility(View.GONE);
                        ivSensitiveWords.setVisibility(View.GONE);
                        ivSendAgain.setVisibility(View.GONE);
                        break;

                    default:
                        progressBarLoading.setVisibility(View.GONE);
                        ivSensitiveWords.setVisibility(View.GONE);
                        ivSendAgain.setVisibility(View.GONE);
                        break;
                }
            } else {
                progressBarLoading.setVisibility(View.GONE);
                ivSensitiveWords.setVisibility(View.GONE);
                ivSendAgain.setVisibility(View.GONE);
            }

            layoutParams =  rootView.getLayoutParams();

            //处理直播间 消息背景的显示隐藏
            String showState = (String) extension.get(SHOW_BG);
            if(SHOW_BG_STATE_SHOW.equals(showState)) {
                rootView.setBackgroundResource(R.drawable.bg_live_room_text_msg);
                layoutParams.width = AppUtils.getScreenWidth(context) - AppUtils.dip2px(context, 100);
                bodyTextViewLive.setMaxWidth(AppUtils.getScreenWidth(context) - AppUtils.dip2px(context, 126));

            }else if(SHOW_BG_STATE_GONE.equals(showState)) {
                rootView.setBackgroundResource(0);
                layoutParams.width = RelativeLayout.LayoutParams.WRAP_CONTENT;
                bodyTextViewLive.setMaxWidth(AppUtils.getScreenWidth(context) - AppUtils.dip2px(context, 34));
            }
            rootView.setLayoutParams(layoutParams);

        } else {
            progressBarLoading.setVisibility(View.GONE);
            ivSensitiveWords.setVisibility(View.GONE);
            ivSendAgain.setVisibility(View.GONE);
        }

        //区分是聊天室还是直播间（始终是直播间）
        chatRoomContainer.setVisibility(View.GONE);
        bodyTextViewLive.setVisibility(View.VISIBLE);
        avatarLeft.setVisibility(View.GONE);
        String content = name + "：" + getDisplayText();
        if(!TextUtils.isEmpty(content)) {
            setSpan(context, bodyTextViewLive, content, R.color.color_f9d354, 0, name.length() + 1, view -> {

            });
        }

        //敏感词叹号点击
        ivSensitiveWords.setOnClickListener(view -> {
            LiveRoomHelper.getInstance().sendSenstiveMsg(message.getContent());
        });

        //重发按钮点击
        ivSendAgain.setOnClickListener(view -> {
            LiveRoomHelper.getInstance().sendMsgToSys(context, message, true, false);
        });
    }


    private void setSpan(Context context, TextView tv, String content, @ColorRes final int color,
                         int start, int end, final View.OnClickListener listener) {

        tv.setHighlightColor(ContextCompat.getColor(context, android.R.color.transparent));
        String str = content;
        SpannableString info = MoonUtil.replaceEmoticons(context, str, 0.6f, ImageSpan.ALIGN_BOTTOM);

        int startNum = (start <= -1) ? 0 : start;
        int endNum = (end <= -1) ? str.length() : end;

        info.setSpan(new ClickableSpan() {

                         @Override
                         public void onClick(View widget) {
                             if (listener != null) {
                                 listener.onClick(widget);
                             }
                         }

                         @Override
                         public void updateDrawState(TextPaint ds) {
                             int colorRes = color;
                             if (color == -1) {
                                 colorRes = Color.BLUE;
                             }
                             ds.setColor(ContextCompat.getColor(context, colorRes));
                         }
                     }, startNum,
                endNum, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv.setText(info);
        tv.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
