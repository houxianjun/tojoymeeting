package com.netease.nim.uikit.business.chatroom.extension;

import com.alibaba.fastjson.JSONObject;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachment;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachmentType;

/**
 * Created by luuzhu on 2019/3/13.
 * 自定义消息-聊天室敏感词提示
 */

public class ChatRoomMessageSenstiveAttachment extends CustomAttachment {

    private String content;

    private String KEY_CONTENT = "content";

    public ChatRoomMessageSenstiveAttachment() {
        super(CustomAttachmentType.CHAT_ROOM_SENSTIVE);
    }

    @Override
    protected void parseData(JSONObject data) {
        content = data.getString(KEY_CONTENT);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_CONTENT, content);
        return data;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
