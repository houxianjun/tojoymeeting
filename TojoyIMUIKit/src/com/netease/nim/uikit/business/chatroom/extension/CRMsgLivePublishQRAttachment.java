package com.netease.nim.uikit.business.chatroom.extension;

import android.text.TextUtils;

import com.alibaba.fastjson.JSONObject;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachment;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachmentType;
import com.tojoy.tjoybaselib.configs.OSSConfig;

/**
 * Created by luuzhu on 2019/3/21.
 * 自定义消息 直播间发布项目 普通成员弹个弹窗
 */

public class CRMsgLivePublishQRAttachment extends CustomAttachment {
    private String id;
    private String name;
    private String picBig;
    private String picSmall;
    private String picShare;
    /**
     * -1 未发布过:'1：发布  2：隐藏',
     */
    private String status;

    private String openid;
    /**
     * 前端消息发送出去的状态：
     * 1:发布；2:隐藏
     */
    private String oprationStatus;
    private String shareRemark;
    private String sortNum;
    private String statusStr;
    private String qrType;
    private String url;
    private String wechatName;
    private String wechatUrl;

    public String getPicBig() {
        return TextUtils.isEmpty(picBig) ? "" : OSSConfig.getOSSURLedStr(picBig);
    }

    public String getPicSmall() {
        return TextUtils.isEmpty(picSmall) ? "" : OSSConfig.getOSSURLedStr(picSmall);
    }

    /**
     * 获取当前活动的发布状态
     * -1：从未发布过（页面显示：隐藏-置灰&&不可点；发布-可点）
     * 1：已经发布过且不是隐藏状态（页面显示：隐藏-高亮&&可点；再次发布-可点）
     * 2：已发布过且当前为隐藏状态（页面显示：已隐藏-置灰&&不可点；再次发布-可点）
     * @return
     */
    public int getStatus(){
        if (TextUtils.isEmpty(status)) {
            return 0;
        } else {
            return Integer.valueOf(status);
        }
    }


    private String KEY_ID = "id";
    private String KEY_NAME = "name";
    private String KEY_PIC_BIG = "picBig";
    private String KEY_PIC_SMALL = "picSmall";
    private String KEY_PIC_SHARE = "picShare";
    private String KEY_STATUS = "status";
    private String KEY_OPENID = "openid";
    private String KEY_OPRATION_STATUS = "oprationStatus";
    private String kEY_SHARE_REMARK = "shareRemark";
    private String kEY_SORT_NUM = "sortNum";
    private String KEY_STATUS_STR = "statusStr";
    private String KEY_TYPE = "type";
    private String KEY_URL = "url";
    private String KEY_WECHAT_NAME = "wechatName";
    private String KEY_WECHAT_URL = "wechatUrl";

    public CRMsgLivePublishQRAttachment() {
        super(CustomAttachmentType.LIVE_PUBLISH_QR);
    }

    @Override
    protected void parseData(JSONObject data) {
        JSONObject entity = data.getJSONObject("entity");
        id = entity.getString(KEY_ID);
        name = entity.getString(KEY_NAME);
        picBig = entity.getString(KEY_PIC_BIG);
        picSmall = entity.getString(KEY_PIC_SMALL);
        picShare = entity.getString(KEY_PIC_SHARE);
        status = entity.getString(KEY_STATUS);
        openid = entity.getString(KEY_OPENID);
        oprationStatus = entity.getString(KEY_OPRATION_STATUS);
        shareRemark = entity.getString(kEY_SHARE_REMARK);
        sortNum = entity.getString(kEY_SORT_NUM);
        statusStr = entity.getString(KEY_STATUS_STR);
        qrType = entity.getString(KEY_TYPE);
        url = entity.getString(KEY_URL);
        wechatName = entity.getString(KEY_WECHAT_NAME);
        wechatUrl = entity.getString(KEY_WECHAT_URL);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_ID,id);
        data.put(KEY_NAME,name);
        data.put(KEY_PIC_BIG,picBig);
        data.put(KEY_PIC_SMALL,picSmall);
        data.put(KEY_PIC_SHARE,picShare);
        data.put(KEY_STATUS,status);
        data.put(KEY_OPENID,openid);
        data.put(KEY_OPRATION_STATUS,oprationStatus);
        data.put(kEY_SHARE_REMARK,shareRemark);
        data.put(kEY_SORT_NUM,sortNum);
        data.put(KEY_STATUS_STR,statusStr);
        data.put(KEY_TYPE,qrType);
        data.put(KEY_URL,url);
        data.put(KEY_WECHAT_NAME,wechatName);
        data.put(KEY_WECHAT_URL,wechatUrl);
        return data;
    }

    @Override
    public String toString() {
        return "CRMsgLivePublishQRAttachment:{id:" + id + ",type:" + type + ",name:" + name + ",status:" + status + ",openid:" + openid + "}";
    }


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return TextUtils.isEmpty(name) ? "" : name;
    }

    public void setPicBig(String picBig) {
        this.picBig = picBig;
    }

    public void setPicSmall(String picSmall) {
        this.picSmall = picSmall;
    }

    public String getPicShare() {
        return picShare;
    }

    public void setPicShare(String picShare) {
        this.picShare = picShare;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    /**
     * 1:显示
     * 2：隐藏
     * @return
     */
    public int getOprationStatus() {
        if (TextUtils.isEmpty(oprationStatus)) {
            return 0;
        } else {
            return Integer.valueOf(oprationStatus);
        }

    }

    public void setOprationStatus(String oprationStatus) {
        this.oprationStatus = oprationStatus;
    }

    public String getShareRemark() {
        return shareRemark;
    }

    public void setShareRemark(String shareRemark) {
        this.shareRemark = shareRemark;
    }

    public String getSortNum() {
        return sortNum;
    }

    public void setSortNum(String sortNum) {
        this.sortNum = sortNum;
    }

    public String getStatusStr() {
        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWechatName() {
        return wechatName;
    }

    public void setWechatName(String wechatName) {
        this.wechatName = wechatName;
    }

    public String getWechatUrl() {
        return wechatUrl;
    }

    public void setWechatUrl(String wechatUrl) {
        this.wechatUrl = wechatUrl;
    }

    public String getQrType(){
        return qrType;
    }
    public void setQrType(String qrType){
        this.qrType = qrType;
    }
}
