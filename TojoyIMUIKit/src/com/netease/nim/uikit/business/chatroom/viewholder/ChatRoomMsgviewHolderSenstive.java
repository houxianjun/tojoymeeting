package com.netease.nim.uikit.business.chatroom.viewholder;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.netease.nim.uikit.R;
import com.netease.nim.uikit.business.chatroom.extension.ChatRoomMessageSenstiveAttachment;
import com.netease.nim.uikit.business.liveroom.LiveRoomHelper;
import com.tojoy.tjoybaselib.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;

import java.util.Map;

/**
 * Created by luuzhu on 2019/3/13.
 * 自定义消息-敏感词提示
 * 云洽会三期新增 全员禁言的提示
 */

public class ChatRoomMsgviewHolderSenstive extends ChatRoomMsgViewHolderBase {

    private TextView tvEdit;
    private View sensitiveLayout;

    private View liveMuteAll;
    private TextView tvMuteAll;

    public ChatRoomMsgviewHolderSenstive(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected int getContentResId() {
        return R.layout.chat_room_message_sensitive_words;
    }

    @Override
    protected void inflateContentView() {
        tvEdit = view.findViewById(R.id.tvEdit);
        sensitiveLayout = view.findViewById(R.id.sensitiveLayout);

        liveMuteAll = view.findViewById(R.id.liveMuteAll);
        tvMuteAll = view.findViewById(R.id.tvMuteAll);
    }

    //是否显示昵称
    @Override
    protected boolean shouldDisplayNick() {
        return false;
    }

    //是否居中
    @Override
    protected boolean isMiddleItem() {
        return true;
    }

    @Override
    protected boolean isShowBubble() {
        return false;
    }

    @Override
    protected boolean isShowHeadImage() {
        return false;
    }

    @Override
    protected void bindContentView() {

        ChatRoomMessageSenstiveAttachment attachment = (ChatRoomMessageSenstiveAttachment) message.getAttachment();
        String content = attachment.getContent();

        if (!TextUtils.isEmpty(content) && content.contains("全员禁言")) {
            sensitiveLayout.setVisibility(View.GONE);
            liveMuteAll.setVisibility(View.VISIBLE);
            tvMuteAll.setText(content);

            //处理直播间大小聊天列表 消息背景的显示隐藏
            Map<String, Object> localextension = message.getLocalExtension();
            if (localextension != null && !localextension.isEmpty()) {
                String showState = (String) localextension.get(ChatRoomMsgViewHolderText.SHOW_BG);
                if (ChatRoomMsgViewHolderText.SHOW_BG_STATE_SHOW.equals(showState)) {
                    liveMuteAll.setBackgroundResource(R.drawable.bg_live_room_join_msg);
                } else if (ChatRoomMsgViewHolderText.SHOW_BG_STATE_GONE.equals(showState)) {
                    liveMuteAll.setBackgroundResource(0);
                }
            }

        } else {
            sensitiveLayout.setVisibility(View.VISIBLE);
            liveMuteAll.setVisibility(View.GONE);
        }

        tvEdit.setOnClickListener(view -> {
            LiveRoomHelper.getInstance().inputPanel.messageEditText.setText(content);
            LiveRoomHelper.getInstance().inputPanel.messageEditText.setSelection(content.length());
            if (LiveRoomHelper.getInstance().inputMsgBtn != null) {
                LiveRoomHelper.getInstance().inputMsgBtn.performClick();
            }
        });

    }

    @Override
    protected void onItemClick() {
    }

}
