package com.netease.nim.uikit.business.chatroom.extension;

import com.alibaba.fastjson.JSONObject;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachment;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachmentType;

/**
 * Created by luuzhu on 2019/5/6.
 * 自定义消息 直播间 结束直播 / 管理员驳回主播的直播
 */

public class CRMsgLiveOverAttachment extends CustomAttachment {

    /**
     * 驳回直播 对主播的提示语
     */
    private String tipsMsg;
    /**
     * 驳回直播 对观众的提示语
     */
    private String tipsMsgViewer;

    private String KEY_TIPS = "tipsMsg";
    private String KEY_tipsMsgViewer = "tipsMsgViewer";

    public CRMsgLiveOverAttachment() {
        super(CustomAttachmentType.LIVE_OVER_LIVE);
    }

    @Override
    protected void parseData(JSONObject data) {
        tipsMsg = data.getString(KEY_TIPS);
        tipsMsgViewer = data.getString(KEY_tipsMsgViewer);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_TIPS, tipsMsg);
        data.put(KEY_tipsMsgViewer, tipsMsgViewer);
        return data;
    }

    @Override
    public String toString() {
        return "CRMsgLiveOverAttachment:{tipsMsg:" + tipsMsg + ",type:" + type + ",tipsMsgViewer:" + tipsMsgViewer + "}";
    }

    public String getTipsMsg() {
        return tipsMsg;
    }

    public String getTipsMsgViewer() {
        return tipsMsgViewer;
    }
}
