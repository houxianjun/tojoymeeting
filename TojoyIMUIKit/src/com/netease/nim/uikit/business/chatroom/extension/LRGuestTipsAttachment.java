package com.netease.nim.uikit.business.chatroom.extension;

import com.alibaba.fastjson.JSONObject;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachment;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachmentType;

/**
 * Created by luuzhu on 2019/11/13.
 * ***点击感兴趣，***要合作项目，***抢占项目区域
 */

public class LRGuestTipsAttachment extends CustomAttachment {

    private String info;

    private String KEY_INFO = "info";

    public LRGuestTipsAttachment() {
        super(CustomAttachmentType.LIVE_GUEST_TIPS);
    }

    @Override
    protected void parseData(JSONObject data) {
        info = data.getString(KEY_INFO);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_INFO, info);
        return data;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "LRGuestTipsAttachment:{,type:" + type + ",info:" + info + "}";
    }
}
