package com.netease.nim.uikit.business.chatroom.extension;

import com.alibaba.fastjson.JSONObject;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachment;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachmentType;

/**
 * Created by luuzhu on 2019/7/10.
 * 自定义消息 直播间 被引流通知
 */

public class LRDrainageAttachment extends CustomAttachment {

    private String info;

    private String KEY_INFO = "info";

    public LRDrainageAttachment() {
        super(CustomAttachmentType.LIVE_DRAINAGE);
    }

    @Override
    protected void parseData(JSONObject data) {
        info = data.getString(KEY_INFO);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_INFO, info);
        return data;
    }

    public String getInfo() {
        return info;
    }
}
