package com.netease.nim.uikit.business.chatroom.extension;

import com.alibaba.fastjson.JSONObject;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachment;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachmentType;

/**
 * Created by luuzhu on 2019/11/14.
 * 隐形自定义消息 企业已禁用，60s 后关闭直播
 */

public class LRClosingMsgAttachment extends CustomAttachment {

    private String content;

    private String KEY_CONTENT= "content";

    public LRClosingMsgAttachment() {
        super(CustomAttachmentType.LIVE_CLOSING);
    }

    @Override
    protected void parseData(JSONObject data) {
        content = data.getString(KEY_CONTENT);
    }

    @Override
    protected JSONObject packData() {
        JSONObject data = new JSONObject();
        data.put(KEY_CONTENT, content);
        return data;
    }

    public String getContent() {
        return content;
    }
}
