package com.netease.nim.uikit.business.liveroom;

import com.tojoy.tjoybaselib.model.live.DoumentVideoSendMsg;
import com.netease.nim.uikit.business.liveroom.FileShowCallback;
import com.netease.nim.uikit.business.liveroom.LiveCustomMessage;

/**
 * 直播页面操作回调
 */
public interface OperationCallback {
    //底部操作栏目
    void showBottomView(boolean show);

    //聊天室消息
    void showChatRoomMessage();

    //成员列表
    void showMemberList();

    //分享
    void showShareView();

    //互动直播操作栏
    void showOperationView();

    //关闭互动弹窗
    void dismissOperationView();

    //申请连麦
    void applyExporeMe();

    //收到申请连麦申请
    void receiveExporeApply(LiveCustomMessage customMessage);

    //邀请连麦
    void inviteExpore();

    //关闭连麦
    void disExploreMe();

    //开始文档演示--图片、文档
    void showFileShow(String boardId, boolean isClicked);

    // 结束文档演示--图片、文档
    void disShowFile();

    //开始文档演示--视频
    void showDoumentVideo(DoumentVideoSendMsg msg, boolean isInitJump);

    // 结束文档演示--视频
    void disDoumentVideo();

    //开始手绘板
    void showDrawPad(boolean isClick);

    //结束手绘板
    void disDrawPad(boolean isClick);

    //发起交易
    void startDeal();

    //发起活动（二维码）
    void startQR();

    //开始引流
    void selectOuterLive();

    //开始引流
    void showOuterLive(String outlinkUrl, String outlinkRtmp, String streamId, boolean showOuterLive);

    /**
     * 结束引流
     *
     * @param streamId
     * @param isDireact
     */
    void disOuterLive(String streamId, boolean isDireact, EndOuterLiveCallback endOuterLiveCallback);

    //暂离
    void onClickTempleteLeave();

    //结束暂离
    void finishTempleteLeave();

    //公告
    void showNotice();

    //举报
    void showReport();

    //结束直播
    void stopLive(boolean initiative, boolean isCloseOfficialLive);

    //退出直播间
    void exitRoom(int status);

    void controlApplyViewVisile(boolean needHide);

    // 退出文档演示
    void exitFileShow(FileShowCallback callback);

    // 统一清空隐藏未读消息数量（由于文档演示中更多功能里面的评论点击后只清了自己的，没有清除外部的，反之同理）
    void goneUnreadCount();

    void setNoticeViewShowStatus(int status);

    //反转摄像头
    void switchCamera();

    void changeVideoQst();

    //手绘板显示隐藏
    void onWacomShowHidden(boolean isShow);

    public interface EndOuterLiveCallback {
        void end();
    }

    //主播或者助手弹出引导提示

    /**
     * 提示一：
     * 主播端:第一次同台或引流，弹提示；当同台弹了引流就不弹了，反之亦然
     * 助手端：第一次被设为助手，若有小窗则弹提示；若当时无小窗，当第一次有同台或引流时弹提示；若同台弹了引流就不弹了，反之亦然
     * <p>
     * 提示二：
     * 主播或助手，首次操作将引流&同台画面切到大屏后，弹提示
     * l
     * 提示一文案：点这里 更多功能
     * 提示二文案：设置主讲，可将主画面切回小屏（暂定）
     */
    public void checkShowTip1(boolean needCheckOnLineMember);

    public void checkShowTip2();

    public void hideTip();

    void setBottomGraientHeight(boolean isScrolling);

    void callOffHook(String selectUserId);

    void callIdle(String selectUserId);
}
