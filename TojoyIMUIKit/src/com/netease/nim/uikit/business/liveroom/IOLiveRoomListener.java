package com.netease.nim.uikit.business.liveroom;

/**
 * @author qll
 * @date 2019/426
 * 进入直播间回调
 */
public interface IOLiveRoomListener {
    /**
     * 进入失败
     */
    void onIOError(String error, int errorCode);

    /**
     * 进入成功
     */
    void onIOSuccess();

}
