package com.netease.nim.uikit.business.liveroom;

/**
 * 直播间 自定义消息码
 */
public enum LiveCustomMessageCode {

    APPLY_EXPORE(1001), //观众同台直播申请
    AGREE_EXPORE(1002), //同意观众的同台申请
    REFUSE_EXPORE(1003),//拒绝观众的同台申请

    INVITE_EXPORE(1004),        //邀请观众同台直播
    AGREE_INVITE_EXPORE(1005),  //观众同意主播的同台邀请
    REFUSE_INVITE_EXPORE(1006), //观众拒绝主播的同台邀请

    CLOSE_EXPLORE(1007),    //关闭某个用户的同台
    CLOSE_AUDIO(1008),      //关闭某个用户的声音
    OPEN_AUDIO(1009),       //开启某个用户的声音
    CLOSE_VIDEO(1010),      //关闭某个用户的视频
    OPEN_VIDEO(1011),       //开启某个用户的视频
    HOST_VIDEO_UP(1012),    //主播屏幕上屏
    HOST_VIDEO_DOWN(1013),  //主播屏幕下屏

//    TEMP_EXIT_ROOM(2001),   //暂离房间
    RESTART_LIVE(2002),     //重新开播(取消暂离)
    TEMP_EXIT_ROOM_NEW(2003),   //暂离房间(新，增加传递暂离时间)

    START_DRAW_POP(3001),   //开始绘画版
    STOP_DRAW_POP(3002),    //结束绘画版

    OPEN_DOCUMENT_WB_ID(3003),     // 展示白板消息通知
    CLOSE_DOCUMENT_WB(3004),       // 关闭白板

    SHOWOUTLINK(3005),    //开始引流
    STOPOUTLINK(3006),    //结束引流

    OPEN_DOCUMENT_VIDEO(3015),     // 展示文档演示--视频（播放、暂停）
    CLOSE_DOCUMENT_VIDEO(3016),    // 关闭文档演示--视频

    REFRESH_ACTIVITY(4001), //刷新活动操作

    CALL_OFF_HOOK(3025), // 主播、同台人正在接听电话
    CALL_IDLE(3026), // 主播、同台人已挂断电话


    HAS_RECEIVE_TIP(10000); //主播收到观众的申请消息后给回馈


    LiveCustomMessageCode(int code) {
        this.code = code;
    }

    private int code;

    public int getCode() {
        return code;
    }
}
