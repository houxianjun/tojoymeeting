package com.netease.nim.uikit.business.liveroom;

/**
 * @author qll
 * @date 2019/6/20
 * 文档演示相关回调
 */
public interface FileShowCallback {

    void exitFileShowSuccess();
}
