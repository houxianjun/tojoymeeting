package com.netease.nim.uikit.business.liveroom;

/**
 * 自定义消息json传对应的内容
 */
public class LiveCustomMessage {
    //0:点对点 、 1：群发
    public int type = 0;

    public String fromUserId;
    public String fromUserName;
    public String fromUserJob;
    public String fromUserHeader;

    public String toUserId;
    public int msgCode;
    public String remark;

    //0:未解决、1：已同意、2：已拒绝
    public int applyAgreenStatus = 0;

    //切换屏幕的UserID
    public String extroId;
    public String outLinkUrlFlv;
    public String outLinkUrlRtmp;

    @Override
    public String toString() {
        return "msgCode:" + msgCode + "fromUserId:" + fromUserId + "toUserId:" + toUserId + "type:" + type + "extroId:" + extroId;
    }
}
