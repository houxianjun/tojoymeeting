package com.netease.nim.uikit.business.liveroom;

public interface JoinLiveRoomListener {
    /**
     * 进入失败（接到后可进行页面刷新等操作）
     */
    void onJoinError(String error, int errorCode);

    /**
     * 进入成功（收到后可以退出页面）
     */
    void onJoinSuccess();
}
