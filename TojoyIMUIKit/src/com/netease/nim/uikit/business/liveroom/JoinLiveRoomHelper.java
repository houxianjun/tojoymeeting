package com.netease.nim.uikit.business.liveroom;

import android.app.Activity;
import android.text.TextUtils;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.live.UserJoinResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * @author qll
 * 进入直播间流程
 * <p>
 * 异常返回码，为与IO进行区分
 * <p>
 * 1001         接口返回异常
 * 1002         接口正常，返回错误原因
 */
public class JoinLiveRoomHelper {

    /**
     * 只有roomid想要进入直播间入口
     */
    public static void joinLiveRoomForRoomId(Activity context, String roomId, JoinLiveRoomListener joinLiveRoomListener) {
        OMAppApiProvider.getInstance().joinMeeting(roomId, new Observer<UserJoinResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                if (context instanceof UI) {
                    ((UI) context).dismissProgressHUD();
                }
                Toasty.normal(context, "网络异常，请稍后再试").show();
                if (joinLiveRoomListener != null) {
                    joinLiveRoomListener.onJoinError("网络异常，请稍后再试", 1001);
                }
            }

            @Override
            public void onNext(UserJoinResponse response) {
                if (response.isSuccess()) {
                    if (TextUtils.isEmpty(response.data.password)) {
                        joinRoom(context, response.data.roomLiveId,joinLiveRoomListener);
                    } else {
                        goToLiveRoom(context, response.data.roomLiveId, response.data.title,joinLiveRoomListener);
                    }
                } else {
                    if (context instanceof UI) {
                        ((UI) context).dismissProgressHUD();
                    }
                    Toasty.normal(context, response.msg).show();
                    if (joinLiveRoomListener != null) {
                        joinLiveRoomListener.onJoinError("网络异常，请稍后再试", 1002);
                    }
                }
            }
        });
    }

    /**
     * 有roomliveID和直播间标题title，但不知道是否要输入密码，进入直播间入口
     */
    public static void goToLiveRoom(Activity context, String roomLiveId, String title,JoinLiveRoomListener joinLiveRoomListener) {
        LiveRoomIOHelper.checkEnterLiveRoom(context, roomLiveId, new IOLiveRoomListener() {
            @Override
            public void onIOError(String error, int errorCode) {
                if (context instanceof UI) {
                    ((UI) context).dismissProgressHUD();
                }
                if (errorCode == 301) {
                    ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_ENTER_ROOM_PSW)
                            .withString("roomName", title)
                            .withString("liveId", roomLiveId)
                            .navigation();
                } else {
                    Toasty.normal(context, error).show();
                    if (joinLiveRoomListener != null) {
                        joinLiveRoomListener.onJoinError(error, errorCode);
                    }
                }
            }

            @Override
            public void onIOSuccess() {
                // 不需要输入密码，直接进入直播间
                joinRoom(context, roomLiveId,joinLiveRoomListener);
            }
        });
    }

    /**
     * 已知不需要密码或输入密码成功后，有roomLiveId直接进入直播间
     */
    public static void joinRoom(Activity context, String roomLiveId,JoinLiveRoomListener joinLiveRoomListener) {
        LiveRoomInfoProvider.getInstance().joinPassword = "";
        LiveRoomInfoProvider.getInstance().liveId = roomLiveId;
        LiveRoomIOHelper.joinLiveRoom(context, new IOLiveRoomListener() {
            @Override
            public void onIOError(String error, int errorCode) {
                if (context instanceof UI) {
                    ((UI) context).dismissProgressHUD();
                }
                if (!TextUtils.isEmpty(error)) {
                    Toasty.normal(context, error).show();
                }
                if (joinLiveRoomListener != null) {
                    joinLiveRoomListener.onJoinError(error, errorCode);
                }
            }

            @Override
            public void onIOSuccess() {
                if (context instanceof UI) {
                    ((UI) context).dismissProgressHUD();
                }
                if (joinLiveRoomListener != null) {
                    joinLiveRoomListener.onJoinSuccess();
                }
            }
        });
    }
}
