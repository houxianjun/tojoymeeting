package com.netease.nim.uikit.business.liveroom;

import android.Manifest;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;

import com.alibaba.android.arouter.launcher.ARouter;
import com.netease.nim.uikit.api.NimUIKit;
import com.netease.nim.uikit.business.chatroom.view.TJVideoPlayerAct;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.tojoy.tjoybaselib.BaseLibKit;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.live.CheckLiveStatusResponse;
import com.tojoy.tjoybaselib.model.live.DoumentIdStatusResponse;
import com.tojoy.tjoybaselib.model.live.DoumentVideoResponse;
import com.tojoy.tjoybaselib.model.live.EnterLiveModel;
import com.tojoy.tjoybaselib.model.live.LiveUserInfoResponse;
import com.tojoy.tjoybaselib.model.live.StartLiveResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.common.Services.EventBus.LiveRoomEvent;
import com.yanzhenjie.permission.AndPermission;

import org.greenrobot.eventbus.EventBus;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * Created by luuzhu on 2019/4/26.
 * 直播间进出流程
 * 主播：开始直播 -> 进入直播间 -> 获取直播间信息 -> 离开直播间 -> 结束直播
 * 观众：检查密码 -> 进入直播间 -> 获取直播间信息 -> 离开直播间
 */


/**
 * IN ERROR CODE
 * 101:startLive                    接口网络异常
 * 102:startLive                    返回数据异常
 * 121:startLive                    由于后台驳回不能开播
 * <p>
 * 103:joinLiveRoom                 接口网络异常
 * 104:joinLiveRoom                 直播未开始 / 已结束
 * 105:joinLiveRoom                 密码错误
 * 106:joinLiveRoom                 返回数据异常
 * 111                              无权限进入（私密会议）
 * <p>
 * 107:getLiveRoomInfoBeforeJoin    接口网络异常
 * 108:getLiveRoomInfoBeforeJoin    被踢出房
 * 109:getLiveRoomInfoBeforeJoin    返回数据异常
 * 110:endLive                      直播结束
 * <p>
 * <p>
 * <p>
 * OUT ERROR CODE
 * 201:leaveLiveRoom                接口网络异常
 * 202:leaveLiveRoom                返回数据异常
 * 203:endLive                      接口网络异常
 * 204:endLive                      返回数据异常
 * <p>
 * 301:checkEnterLiveRoom           需要输入密码
 * 302:checkEnterLiveRoom           密码错误超过5次
 * 303:checkEnterLiveRoom           返回数据异常
 * <p>
 * 401:                             针对被踢出后在个人主页点击被提出的直播间，需要toast提示
 * <p>
 * 501:setDocument                    接口网络异常
 * 502:setDocument                    返回数据异常
 */


public class LiveRoomIOHelper {

    /**
     * 用户端进入直播间--判断是否需要密码
     */
    public static void checkEnterLiveRoom(Context context, String liveId, IOLiveRoomListener IOLiveRoomListener) {

        OMAppApiProvider.getInstance().checkEnterLiveRoom(new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(context, "网络异常，请稍后再试").show();
            }

            @Override
            public void onNext(OMBaseResponse baseResponse) {
                if (baseResponse != null && baseResponse.isSuccess()) {
                    IOLiveRoomListener.onIOSuccess();
                } else {
                    if (baseResponse != null && "2".equals(baseResponse.code)) {
                        IOLiveRoomListener.onIOError(baseResponse.msg, 301);
                    } else if (baseResponse != null && "5".equals(baseResponse.code)) {
                        IOLiveRoomListener.onIOError(baseResponse.msg, 302);
                    } else {
                        IOLiveRoomListener.onIOError(baseResponse != null && !TextUtils.isEmpty(baseResponse.msg) ? baseResponse.msg : "", 303);
                    }
                }
            }
        }, liveId);
    }


    /**
     * 开始直播
     */
    public static void startLive(Context context,IOLiveRoomListener IOLiveRoomListener) {
        OMAppApiProvider.getInstance().startLive(new Observer<StartLiveResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                IOLiveRoomListener.onIOError("网络异常，请检查网络", 101);
            }

            @Override
            public void onNext(StartLiveResponse baseResponse) {
                if (baseResponse.isSuccess()) {
                    LiveRoomInfoProvider.getInstance().liveId = baseResponse.data.roomLiveId;
                    LiveRoomInfoProvider.getInstance().startTime = baseResponse.data.startTime;
                    // 开播后调用发送push
                    startLivePushRemind(context, LiveRoomInfoProvider.getInstance().roomId, BaseUserInfoCache.getUserId(context), baseResponse.data.roomLiveId);

                    joinLiveRoom(context, IOLiveRoomListener);
                } else if ("-95".equals(baseResponse.code)) {
                    // 驳回状态不能开播
                    IOLiveRoomListener.onIOError(baseResponse.msg, 121);
                } else {
                    IOLiveRoomListener.onIOError(baseResponse.msg, 102);
                }
            }
        });
    }


    /**
     * 进入直播间
     */
    public static void joinLiveRoom(Context context, IOLiveRoomListener onIOLiveRoomListener) {
        if (!((UI) context).isShowProgressHUD()) {
            ((UI) context).showProgressHUD(context, "加载中");
        }
        OMAppApiProvider.getInstance().joinLiveRoom(LiveRoomInfoProvider.getInstance().liveId,
                BaseUserInfoCache.getUserName(context),
                BaseUserInfoCache.getUserHeaderPic(context),
                BaseUserInfoCache.getCompanyName(context),
                BaseUserInfoCache.getJobName(context),
                LiveRoomInfoProvider.getInstance().joinPassword,
                new Observer<EnterLiveModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        ((UI) context).dismissProgressHUD();
                        onIOLiveRoomListener.onIOError("网络异常，请检查网络", 103);
                    }

                    @Override
                    public void onNext(EnterLiveModel response) {
                        ((UI) context).dismissProgressHUD();
                        if (response.isSuccess()) {
                            LiveRoomInfoProvider.getInstance().sysCurTime = response.data.curTime;
                            LiveRoomInfoProvider.getInstance().liveStatus = response.data.liveStatus;
                            LiveRoomInfoProvider.getInstance().isReplay = response.data.isReplay;
                            LiveRoomInfoProvider.getInstance().isApply = response.data.isApply;
                            LiveRoomInfoProvider.getInstance().applyId=response.data.applyId;
                            if (response.data.liveParam != null) {
                                LiveRoomInfoProvider.getInstance().officalLiveUrl = response.data.liveParam.pullFlvUrl;
                                LiveRoomInfoProvider.getInstance().videoPullUrl = response.data.liveParam.videoPullUrl;
                            }

                            LiveRoomInfoProvider.getInstance().templateLeaveTip = "";
                            LiveRoomInfoProvider.getInstance().templateLeaveTime = "";

                            if (!TextUtils.isEmpty(response.data.pauseMsg)) {
                                LiveRoomInfoProvider.getInstance().templateLeaveTip = response.data.pauseMsg;
                            }

                            if ("1".equals(LiveRoomInfoProvider.getInstance().liveStatus)) {
                                onIOLiveRoomListener.onIOError("会议还未开始", 104);
                            } else {

                                getLiveRoomInfoBeforeJoin(context, onIOLiveRoomListener);
                            }

                        } else {
                            if ("5".equals(response.code)) {
                                // 密码错误，回调到输入密码页面，提示用户输入密码错误
                                onIOLiveRoomListener.onIOError("密码错误", 105);
                            } else if ("10".equals(response.code)) {
                                // 密码输错超过5次
                                onIOLiveRoomListener.onIOError(response.data.phone, 302);
                            }
                             else {
                                onIOLiveRoomListener.onIOError(response.msg, 111);
                            }
                        }
                    }
                }
        );
    }


    /**
     * 获取当前直播间状态
     */

    static boolean needRecheck = true;

    public static void checkLiveRoom(IOLiveRoomListener onIOLiveRoomListener) {
        OMAppApiProvider.getInstance().getLiveRoomStatus(LiveRoomInfoProvider.getInstance().liveId, new Observer<CheckLiveStatusResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                //如接口异常 或超时 延时再处理一次
                if (needRecheck) {
                    needRecheck = false;
                    new Handler().postDelayed(() -> checkLiveRoom(onIOLiveRoomListener), 2000);
                }
            }

            @Override
            public void onNext(CheckLiveStatusResponse response) {
                if (response.isSuccess()) {
                    if ("2".equals(response.data.status) || "3".equals(response.data.status)) {
                        onIOLiveRoomListener.onIOSuccess();
                    } else {
                        onIOLiveRoomListener.onIOError(response.data.statusText, -1);
                    }
                } else {
                    onIOLiveRoomListener.onIOError("获取直播间状态异常", -1);
                }
            }
        });
    }


    /**
     * 获取进入直播间的个人身份及直播间状态
     *
     * @param context
     * @param onIOLiveRoomListener
     */
    private static void getLiveRoomInfoBeforeJoin(Context context, IOLiveRoomListener onIOLiveRoomListener) {
        OMAppApiProvider.getInstance().getMemberInfo(BaseUserInfoCache.getUserId(context), LiveRoomInfoProvider.getInstance().liveId, new Observer<LiveUserInfoResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                onIOLiveRoomListener.onIOError("网络异常，请检查网络", 107);
            }

            @Override
            public void onNext(LiveUserInfoResponse response) {
                if (response.isSuccess()) {
                    LiveRoomInfoProvider.getInstance().hasTemplateLeave = false;
                    LiveRoomInfoProvider.getInstance().isMyselfOpenDocument = false;
                    LiveRoomInfoProvider.getInstance().clearRoomData();

                    if (response.data.roomLive.startDate == 0) {
                        LiveRoomInfoProvider.getInstance().startTime = System.currentTimeMillis();
                    } else {
                        LiveRoomInfoProvider.getInstance().startTime = response.data.roomLive.startDate;
                    }

                    if (!TextUtils.isEmpty(response.data.userName)) {
                        BaseUserInfoCache.setUserName(response.data.userName, context);
                    }

                    LiveRoomInfoProvider.getInstance().liveDuration = LiveRoomInfoProvider.getInstance().sysCurTime - LiveRoomInfoProvider.getInstance().startTime;
                    LiveRoomInfoProvider.getInstance().hostAvatar = response.data.roomLive.avatar;
                    LiveRoomInfoProvider.getInstance().hostName = response.data.roomLive.userName;
                    LiveRoomInfoProvider.getInstance().roomId = response.data.roomLive.roomId;
                    LiveRoomInfoProvider.getInstance().roomCode = response.data.roomLive.roomCode;
                    LiveRoomInfoProvider.getInstance().liveStatus = response.data.roomLive.status;
                    LiveRoomInfoProvider.getInstance().roomType = response.data.roomLive.roomType;
                    LiveRoomInfoProvider.getInstance().liveTitle = response.data.roomLive.title;

                    LiveRoomInfoProvider.getInstance().imRoomId = response.data.roomLive.imRoomId;
                    LiveRoomInfoProvider.getInstance().liveId = TextUtils.isEmpty(response.data.liveId) ? response.data.roomLive.roomLiveId : response.data.liveId;
                    LiveRoomInfoProvider.getInstance().myUserType = response.data.userType;
                    LiveRoomInfoProvider.getInstance().imCreatorid = response.data.roomLive.userId;
                    LiveRoomInfoProvider.getInstance().hostUserId = response.data.roomLive.userId;
                    LiveRoomInfoProvider.getInstance().mLiveTags.clear();
                    LiveRoomInfoProvider.getInstance().mLiveTags.addAll(response.data.roomLive.tagList);

                    LiveRoomInfoProvider.getInstance().coverOne = response.data.roomLive.coverOne;

                    LiveRoomInfoProvider.getInstance().otherCompanyCode = response.data.roomLive.companyCode;
                    LiveRoomInfoProvider.getInstance().comCode = response.data.roomLive.companyCode;

                    //新增保存是否显示分享按钮字段
                    LiveRoomInfoProvider.getInstance().isHideShare = response.data.isHideShare;
                    LiveRoomInfoProvider.getInstance().allowViewTotal = response.data.roomLive.allowViewTotal;
                    LiveRoomInfoProvider.getInstance().authType = response.data.roomLive.authType;

                    LiveRoomInfoProvider.getInstance().ifDistribution = response.data.ifDistribution;
                    LiveRoomInfoProvider.getInstance().maxBudget = response.data.maxBudget;
                    LiveRoomInfoProvider.getInstance().level1Commission = response.data.level1Commission;
                    LiveRoomInfoProvider.getInstance().level2Commission = response.data.level2Commission;
                    LiveRoomInfoProvider.getInstance().level1CommissionEmployee = response.data.level1CommissionEmployee;
                    LiveRoomInfoProvider.getInstance().level2CommissionEmployee = response.data.level2CommissionEmployee;
                    LiveRoomInfoProvider.getInstance().minPerson = response.data.minPerson;
                    LiveRoomInfoProvider.getInstance().minViewTime = response.data.minViewTime;


                    if ("1".equals(response.data.isRemove)) {
                        String timeStr = getTimeStr((int) (LiveRoomInfoProvider.getInstance().liveDuration / 1000));

                        String name = context.getClass().getName();
                        if (!TextUtils.isEmpty(name) && !name.contains("UserHomePageAct")) {
                            ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_USER_CENTER)
                                    .withString("type", "3")
                                    .withString("duration", timeStr)
                                    .withString("hostPic", LiveRoomInfoProvider.getInstance().hostAvatar)
                                    .withString("roomId", LiveRoomInfoProvider.getInstance().roomId)
                                    .withString("roomCode",LiveRoomInfoProvider.getInstance().roomCode)
                                    .withString("userName", LiveRoomInfoProvider.getInstance().hostName)
                                    .navigation();
                        }
                        onIOLiveRoomListener.onIOError("很抱歉！您已经移除直播间！", 401);
                        leaveLiveRoom(context, onIOLiveRoomListener, false);
                        return;
                    }

                    if ("4".equals(LiveRoomInfoProvider.getInstance().liveStatus)) {
                        if ("1".equals(LiveRoomInfoProvider.getInstance().isReplay)) {
                            //如果是用户进入回放页
                            if (!LiveRoomInfoProvider.getInstance().isHost()) {
                                //关闭之前打开的
                                EventBus.getDefault().post(new LiveRoomEvent(LiveRoomEvent.LIVE_CLOSE_GUEST, ""));
                            }

                            TJVideoPlayerAct.start(context, LiveRoomInfoProvider.getInstance().videoPullUrl, LiveRoomInfoProvider.getInstance().liveId, response.data.roomLive.companyCode);
                        } else {
                            if (BaseUserInfoCache.getUserId(context).equals(response.data.roomLive.userId)) {
                                //我是主播type传1
                                String timeStr = getTimeStr((int) (LiveRoomInfoProvider.getInstance().liveDuration / 1000));
                                ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_USER_CENTER)
                                        .withString("type", "1")
                                        .withString("duration", timeStr)
                                        .withString("hostPic", LiveRoomInfoProvider.getInstance().hostAvatar)
                                        .withString("roomId", LiveRoomInfoProvider.getInstance().roomId)
                                        .withString("roomCode",LiveRoomInfoProvider.getInstance().roomCode)
                                        .withString("userName", LiveRoomInfoProvider.getInstance().hostName)
                                        .navigation();
                            } else {
                                //我不是主播type传1
                                String timeStr = getTimeStr((int) (LiveRoomInfoProvider.getInstance().liveDuration / 1000));
                                ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_USER_CENTER)
                                        .withString("type", "2")
                                        .withString("duration", timeStr)
                                        .withString("hostPic", LiveRoomInfoProvider.getInstance().hostAvatar)
                                        .withString("roomId", LiveRoomInfoProvider.getInstance().roomId)
                                        .withString("roomCode",LiveRoomInfoProvider.getInstance().roomCode)
                                        .withString("userName", LiveRoomInfoProvider.getInstance().hostName)
                                        .navigation();
                            }
                            leaveLiveRoom(context, onIOLiveRoomListener, false);
                        }
                        onIOLiveRoomListener.onIOSuccess();
                        return;
                    }

                    onIOLiveRoomListener.onIOSuccess();

                    if (LiveRoomInfoProvider.getInstance().isHost()) {
                        ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_LIVE_ROOM).navigation();
                    } else {
                        // 如果是通过push已经进入了seeLive
                        if (!LiveRoomInfoProvider.getInstance().isFromPush) {
                            //关闭之前打开的
                            EventBus.getDefault().post(new LiveRoomEvent(LiveRoomEvent.LIVE_CLOSE_GUEST, ""));
                        }

                        ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_SEE_LIVE_ROOM).navigation();
                        LiveRoomInfoProvider.getInstance().isFromPush = false;
                    }
                } else {
                    onIOLiveRoomListener.onIOError(response.msg, 109);
                    leaveLiveRoom(context, onIOLiveRoomListener, false);
                }
            }
        });
    }

    public static String getTimeStr(int duration) {

        int h = duration / 3600;
        int leftSecond = duration % 3600;
        int m = leftSecond / 60;
        int s = leftSecond % 60;

        String hour, min, second;
        if (h < 10) {
            hour = "0" + h;
        } else {
            hour = "" + h;
        }
        if (m < 10) {
            min = "0" + m;
        } else {
            min = "" + m;
        }
        if (s < 10) {
            second = "0" + s;
        } else {
            second = "" + s;
        }
        return hour + ":" + min + ":" + second;
    }

    /**
     * 退出直播间
     * <p>
     * 5.22大会主播结束直播的时候 需要调用endLive，增加needCloseMeeting
     *
     * @param context
     */
    public static void leaveLiveRoom(Context context, IOLiveRoomListener ioLiveRoomListener, boolean needCloseMeeting) {
        OMAppApiProvider.getInstance().leaveLiveRoom(true,LiveRoomInfoProvider.getInstance().liveId, BaseUserInfoCache.getUserName(context), BaseUserInfoCache.getUserHeaderPic(context),
                BaseUserInfoCache.getCompanyName(context), BaseUserInfoCache.getJobName(context),BaseUserInfoCache.getUserId(context), new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        ioLiveRoomListener.onIOError("网络异常，请检查网络", 201);
                    }

                    @Override
                    public void onNext(OMBaseResponse baseResponse) {
                        if (baseResponse.isSuccess()) {
                            //主播结束官方大会
                            if (LiveRoomInfoProvider.getInstance().isOfficalLive() && needCloseMeeting && LiveRoomInfoProvider.getInstance().isHost()) {
                                endLive(context,ioLiveRoomListener);
                            } else if (LiveRoomInfoProvider.getInstance().isHost() && !LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                                //服务秘书主播结束直播
                                endLive(context,ioLiveRoomListener);
                            } else {
                                onExitLiveSuccess(ioLiveRoomListener);
                            }
                        } else {
                            ioLiveRoomListener.onIOError(baseResponse.msg, 202);
                        }
                    }
                });
        exitedChatRoom();
    }

    private static void exitedChatRoom() {
        NimUIKit.exitedChatRoom(LiveRoomInfoProvider.getInstance().imRoomId);
        NIMClient.getService(ChatRoomService.class).exitChatRoom(LiveRoomInfoProvider.getInstance().imRoomId);
    }

    /**
     * 退出直播间
     */
    public static void endLive(Context context,IOLiveRoomListener ioLiveRoomListener) {
        OMAppApiProvider.getInstance().endLive(true,LiveRoomInfoProvider.getInstance().liveId,BaseUserInfoCache.getUserId(context), new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                ioLiveRoomListener.onIOError("网络异常，请检查网络", 203);
            }

            @Override
            public void onNext(OMBaseResponse baseResponse) {

                if (baseResponse.isSuccess()) {
                    onExitLiveSuccess(ioLiveRoomListener);
                } else {
                    ioLiveRoomListener.onIOError("", 204);
                }
            }
        });
    }


    /**
     * 退出成功单例数据回收
     *
     * @param ioLiveRoomListener
     */
    private static void onExitLiveSuccess(IOLiveRoomListener ioLiveRoomListener) {
        ioLiveRoomListener.onIOSuccess();
    }

    /**
     * 继续直播
     *
     * @param context
     */
    public static void goOnLive(Context context) {
        OMAppApiProvider.getInstance().goOnLive(LiveRoomInfoProvider.getInstance().liveId, BaseUserInfoCache.getUserId(context),
                new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(OMBaseResponse baseResponse) {

                    }
                });
    }

    /**
     * 主播端--展示白板通知后台当前展示的白板ID
     *
     * @param isShow 1：展示；0：关闭
     */
    public static void setDocumentStatus(Context context, String isShow, String boardId, IOLiveRoomListener IOLiveRoomListener) {
        ((UI) context).showProgressHUD(context, "");
        OMAppApiProvider.getInstance().setDocumentStatus(LiveRoomInfoProvider.getInstance().liveId,
                isShow,
                boardId, new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        ((UI) context).dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ((UI) context).dismissProgressHUD();
                        IOLiveRoomListener.onIOError("网络错误，请稍后再试", 501);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        if (response.isSuccess()) {
                            IOLiveRoomListener.onIOSuccess();
                        } else {
                            IOLiveRoomListener.onIOError(response.msg, 502);
                        }
                    }
                });
    }

    /**
     * 用户端--获取当前直播间是否显示白板
     */
    public static void getDocumentStatus(Observer<DoumentIdStatusResponse> observer) {
        OMAppApiProvider.getInstance().getDocumentStatus(LiveRoomInfoProvider.getInstance().liveId, observer);
    }

    /**
     * 主播端--展示文档演示视频通知后台当前展示的视频状态
     *
     * @param status 0:开始播放视频， 1：暂停，2：退出播放视频
     */
    public static void setVideoStatus(String status, String videoUrl, String playTime) {
        OMAppApiProvider.getInstance().setVideoStatus(LiveRoomInfoProvider.getInstance().liveId,
                status,
                videoUrl,
                playTime, new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        if (response.isSuccess()) {

                        }
                    }
                });
    }

    /**
     * 用户端--获取当前直播间是否显示文档演示的视频
     */
    public static void getVideoStatus(Observer<DoumentVideoResponse> observer) {
        OMAppApiProvider.getInstance().getVideoStatus(LiveRoomInfoProvider.getInstance().liveId, observer);
    }

    public static void checkMediaPermission(Context mContext, PermissionCallback permissionCallback) {

        AndPermission.with(mContext)
                .permission(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                )
                .rationale((context, permissions, executor) -> {
                    // 此处可以选择显示提示弹窗
                    executor.execute();
                })
                // 用户给权限了
                .onGranted(permissions -> {
                            permissionCallback.onGranted();
                        }
                )
                // 用户拒绝权限，包括不再显示权限弹窗也在此列
                .onDenied(permissions -> {
                    // 判断用户是不是不再显示权限弹窗了，是的话进入权限设置页
                    if (AndPermission.hasAlwaysDeniedPermission(mContext, permissions)) {
                        // 打开权限设置页
                        AndPermission.permissionSetting(mContext).execute();
                    } else {
                        permissionCallback.onDenied();
                    }

                })
                .start();
    }

    public interface PermissionCallback {
        /**
         * 同意
         */
        void onGranted();

        /**
         * 拒绝
         */
        void onDenied();
    }

    /**
     * 开播提醒：主播马上开播调用startLive接口返回成功后（调用）
     *
     * @param context
     */
    public static void startLivePushRemind(Context context, String roomId, String userId, String roomLiveId) {

        OMAppApiProvider.getInstance().startLivePushRemind(roomId, userId, roomLiveId, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(context, "网络错误，请稍后再试").show();
            }

            @Override
            public void onNext(OMBaseResponse baseResponse) {
                if (baseResponse != null && !baseResponse.isSuccess() && !TextUtils.isEmpty(baseResponse.msg)) {
                    Toasty.normal(context, baseResponse.msg).show();
                }
            }
        });
    }

    /**
     * 开播提醒：直播间—互动弹窗—开播提醒按钮—发送（调用）
     *
     * @param context
     */
    public static void livePushRemind(Context context, String roomId, String userId, String roomLiveId) {

        OMAppApiProvider.getInstance().livePushRemind(roomId, userId, roomLiveId, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(context, "网络错误，请稍后再试").show();
            }

            @Override
            public void onNext(OMBaseResponse baseResponse) {
                if (baseResponse.isSuccess()) {
                    Toasty.normal(context, "操作成功！").show();
                } else if (baseResponse != null && !baseResponse.isSuccess() && !TextUtils.isEmpty(baseResponse.msg)) {
                    Toasty.normal(context, baseResponse.msg).show();
                }
            }
        });
    }

    /**
     * 针对用户从直播间点主页后跳另外的直播间
     */
    public static void exitOldUser(String userId) {

        if (!TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().lastLiveId) && !TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().lastIMRoomId)) {
            OMAppApiProvider.getInstance().leaveLiveRoom(true,LiveRoomInfoProvider.getInstance().lastLiveId, BaseUserInfoCache.getUserName(BaseLibKit.getContext()), BaseUserInfoCache.getUserHeaderPic(BaseLibKit.getContext()),
                    BaseUserInfoCache.getCompanyName(BaseLibKit.getContext()), BaseUserInfoCache.getJobName(BaseLibKit.getContext()),userId, new Observer<OMBaseResponse>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                        }

                        @Override
                        public void onNext(OMBaseResponse baseResponse) {
                            if (baseResponse.isSuccess()) {

                            }
                        }
                    });
        }
    }
}
