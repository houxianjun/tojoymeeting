package com.netease.nim.uikit.business.liveroom;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishProjectAttachment;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishQRAttachment;
import com.netease.nim.uikit.business.chatroom.extension.ChatRoomMessageSenstiveAttachment;
import com.netease.nim.uikit.business.chatroom.module.LiveRoomInputPanel;
import com.netease.nim.uikit.business.chatroom.module.LiveRoomMsgListPanel;
import com.netease.nim.uikit.business.chatroom.viewholder.ChatRoomMsgViewHolderText;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.util.net.NetworkUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * Created by luuzhu on 2019/4/20.
 */

public class LiveRoomHelper {

    public boolean loginReenterLiveRoom;

    private LiveRoomHelper() {

    }

    private static class SingletonInstance {
        @SuppressLint("StaticFieldLeak")
        private static final LiveRoomHelper INSTANCE = new LiveRoomHelper();
    }

    public static LiveRoomHelper getInstance() {
        return LiveRoomHelper.SingletonInstance.INSTANCE;
    }

    /**
     * 直播页的项目推荐按钮
     */
    public View productListBtn;
    /**
     * 用户在直播间时接受到的项目集合
     */
    public ArrayList<CRMsgLivePublishProjectAttachment> receivedProjects = new ArrayList<>();

    public LiveRoomInputPanel inputPanel;
    private LiveRoomMsgListPanel messageListPanel;


    /**
     * 直播页的接收活动的右下角轮播图
     */
    public View publishQRListSmallView;
    /**
     * 用户在直播间时接受到的活动（二维码）集合（右下脚）
     */
    public ArrayList<CRMsgLivePublishQRAttachment> receivedQrSmallList = new ArrayList<>();
    /**
     * 用户在直播间时接受到的活动（二维码）集合（中间存储未展示）
     */
    public ArrayList<CRMsgLivePublishQRAttachment> receivedQrBigList = new ArrayList<>();

    /**
     * 主要进行右下角轮播图列表数据的维护
     * 注意：添加的可能是要隐藏的消息数据
     */
    public void addQRDataSmall(CRMsgLivePublishQRAttachment attachment) {
        if (attachment == null) {
            return;
        }

        /**
         * 处理small：判断是要显示还是隐藏
         * 显示：判断小轮播图是否有数据，有则不做处理，无进行添加刷新
         * 隐藏：判断小轮播图是否有数据，有则删除刷新数据，无不做处理
         */
        boolean isSmallContains = false;
        for (int i = receivedQrSmallList.size()-1; i >= 0 ; i--) {
            if (attachment.getId().equals(receivedQrSmallList.get(i).getId())) {
                isSmallContains = true;
                if (attachment.getOprationStatus() == 2) {
                    // 要隐藏：如果展示列表中已包含则判断是否正在展示（正在展示不处理，未展示则从列表删除不进行展示），不包含则不处理
                    receivedQrSmallList.remove(receivedQrSmallList.get(i));
                } else if (attachment.getOprationStatus() == 1) {
                    // 要显示：如果展示列表中，重新对这条数据进行赋值，（防止后台更新该活动的数据了）
                    receivedQrSmallList.get(i).setId(attachment.getId());
                    receivedQrSmallList.get(i).setName(attachment.getName());
                    receivedQrSmallList.get(i).setPicBig(attachment.getPicBig());
                    receivedQrSmallList.get(i).setPicSmall(attachment.getPicSmall());
                    receivedQrSmallList.get(i).setPicShare(attachment.getPicShare());
                    receivedQrSmallList.get(i).setStatus(String.valueOf(attachment.getStatus()));
                    receivedQrSmallList.get(i).setOpenid(attachment.getOpenid());
                    receivedQrSmallList.get(i).setOprationStatus(String.valueOf(attachment.getOprationStatus()));
                    receivedQrSmallList.get(i).setShareRemark(attachment.getShareRemark());
                    receivedQrSmallList.get(i).setSortNum(attachment.getSortNum());
                    receivedQrSmallList.get(i).setStatusStr(attachment.getStatusStr());
                    receivedQrSmallList.get(i).setQrType(attachment.getQrType());
                    receivedQrSmallList.get(i).setUrl(attachment.getUrl());
                    receivedQrSmallList.get(i).setWechatName(attachment.getWechatName());
                    receivedQrSmallList.get(i).setWechatUrl(attachment.getWechatUrl());
                }
            }
        }

        if (attachment.getOprationStatus() == 1) {
            if (!isSmallContains) {
                receivedQrSmallList.add(attachment);
            }
        }
    }

    /**
     * 主要进行中间弹窗列表数据的维护
     * 注意：添加的可能是要隐藏的消息数据
     */
    public void addQRDataBig(CRMsgLivePublishQRAttachment attachment, boolean isShowCenterQR) {
        if (attachment == null) {
            return;
        }
        //处理Big：如果现有的集合(正在展示的中间弹窗)里没有这个项目,就添加，有的话排的同时判断是隐藏还是显示
        boolean isBigContains = false;
        for (int i = receivedQrBigList.size() - 1; i >= 0 ; i--) {
            if (attachment.getId().equals(receivedQrBigList.get(i).getId())) {
                isBigContains = true;
                if (attachment.getOprationStatus() == 2) {
                    // 要隐藏：如果展示列表中已包含则判断是否正在展示（正在展示不处理，未展示则从列表删除不进行展示），不包含则不处理
                    receivedQrBigList.remove(receivedQrBigList.get(i));
                } else if (attachment.getOprationStatus() == 1) {
                    // 要显示：如果展示列表中，重新对这条数据进行赋值，（防止后台更新该活动的数据了）
                    receivedQrBigList.get(i).setId(attachment.getId());
                    receivedQrBigList.get(i).setName(attachment.getName());
                    receivedQrBigList.get(i).setPicBig(attachment.getPicBig());
                    receivedQrBigList.get(i).setPicSmall(attachment.getPicSmall());
                    receivedQrBigList.get(i).setPicShare(attachment.getPicShare());
                    receivedQrBigList.get(i).setStatus(String.valueOf(attachment.getStatus()));
                    receivedQrBigList.get(i).setOpenid(attachment.getOpenid());
                    receivedQrBigList.get(i).setOprationStatus(String.valueOf(attachment.getOprationStatus()));
                    receivedQrBigList.get(i).setShareRemark(attachment.getShareRemark());
                    receivedQrBigList.get(i).setSortNum(attachment.getSortNum());
                    receivedQrBigList.get(i).setStatusStr(attachment.getStatusStr());
                    receivedQrBigList.get(i).setQrType(attachment.getQrType());
                    receivedQrBigList.get(i).setUrl(attachment.getUrl());
                    receivedQrBigList.get(i).setWechatName(attachment.getWechatName());
                    receivedQrBigList.get(i).setWechatUrl(attachment.getWechatUrl());
                }
//                break;
            }
        }

        if (attachment.getOprationStatus() == 1) {
            if (!isBigContains) {
                // 要展示：如果展示列表中已经包含则不需处理，若展示列表不包含则加入列表
                receivedQrBigList.add(attachment);
            }
        }
    }

    /**
     * 中间弹窗关闭时删除顶层数据
     *
     * @param attachment // 已经展示且刚关闭的数据
     */
    public void deleteQRDataBig(CRMsgLivePublishQRAttachment attachment) {
        if (attachment == null) {
            return;
        }
        if (receivedQrBigList != null && receivedQrBigList.size() > 0) {
            for (int i = 0; i < receivedQrBigList.size(); i++) {
                if (receivedQrBigList.get(i).getId().equals(attachment.getId())) {
                    receivedQrBigList.remove(receivedQrBigList.get(i));
                    break;
                }
            }
        }
    }

    /**
     * 聊天室消息弹窗的“请输入内容...”按钮
     */
    public TextView inputMsgBtn;

    public void setInputPanel(LiveRoomInputPanel inputPanel) {
        this.inputPanel = inputPanel;
    }

    public void setMessageListPanel(LiveRoomMsgListPanel messageListPanel) {
        this.messageListPanel = messageListPanel;
    }

    /**
     * 把用户的输入内容发送到后台审核
     *
     * @param mContext
     * @param message
     * @param fromLiveRoom 这条消息是不是点击发送按钮发出的，还有一种情况是点击重发叹号发的，以此确认本地是否再插入一条消息
     * @param sendAgain    是不是重发
     */
    public void sendMsgToSys(Context mContext, ChatRoomMessage message, boolean sendAgain, boolean fromLiveRoom) {

        Map<String, Object> msgMap = new HashMap<>();

        String showBg = "";
        Map<String, Object> extension = message.getLocalExtension();
        if (extension != null && !extension.isEmpty()) {
            showBg = (String) extension.get(ChatRoomMsgViewHolderText.SHOW_BG);
        }

        if (NetworkUtil.isNetAvailable(mContext)) {
            msgMap.put(ChatRoomMsgViewHolderText.SENSTIVE, ChatRoomMsgViewHolderText.SENSTIVE_0);
        } else {
            msgMap.put(ChatRoomMsgViewHolderText.SENSTIVE, ChatRoomMsgViewHolderText.SENSTIVE_2);
        }

        msgMap.put(ChatRoomMsgViewHolderText.SHOW_BG, showBg);

        message.setLocalExtension(msgMap);

        if (!fromLiveRoom) {
            if (!sendAgain) {
                sendLiveMessage(message);
            }
        }

        String userId = BaseUserInfoCache.getUserId(mContext);
        String finalShowBg = showBg;
        OMAppApiProvider.getInstance().sendMsg(
                BaseUserInfoCache.addIMHeader(mContext, userId),
                BaseUserInfoCache.getUserName(mContext),
                BaseUserInfoCache.getCompanyName(mContext),
                BaseUserInfoCache.getUserJob(mContext),
                LiveRoomInfoProvider.getInstance().imRoomId,
                message.getContent(),
                message.getUuid(),
                LiveRoomInfoProvider.getInstance().liveId,
                LiveRoomInfoProvider.getInstance().imCreatorid,
                LiveRoomInfoProvider.getInstance().roomId,
                new rx.Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }
                    @Override
                    public void onError(Throwable e) {
                        Map<String, Object> msgMap = new HashMap<>();
                        msgMap.put(ChatRoomMsgViewHolderText.SENSTIVE, ChatRoomMsgViewHolderText.SENSTIVE_2);
                        msgMap.put(ChatRoomMsgViewHolderText.SHOW_BG, finalShowBg);
                        message.setLocalExtension(msgMap);
                        messageListPanel.refreshMsgAll();
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {

                        if (response.isSuccess()) {
                            Map<String, Object> msgMap = new HashMap<>();
                            msgMap.put(ChatRoomMsgViewHolderText.SENSTIVE, ChatRoomMsgViewHolderText.SENSTIVE_3);
                            msgMap.put(ChatRoomMsgViewHolderText.SHOW_BG, finalShowBg);
                            message.setLocalExtension(msgMap);
                            messageListPanel.refreshMsgAll();
                        } else {
                            Map<String, Object> msgMap = new HashMap<>();
                            msgMap.put(ChatRoomMsgViewHolderText.SENSTIVE, ChatRoomMsgViewHolderText.SENSTIVE_2);
                            msgMap.put(ChatRoomMsgViewHolderText.SHOW_BG, finalShowBg);
                            message.setLocalExtension(msgMap);
                            messageListPanel.refreshMsgAll();
                        }
                    }
                });

    }

    /**
     * 发送敏感词提示消息
     *
     * @param content
     */
    public void sendSenstiveMsg(String content) {

        ChatRoomMessageSenstiveAttachment attachment = new ChatRoomMessageSenstiveAttachment();
        attachment.setContent(content);
        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(LiveRoomInfoProvider.getInstance().imRoomId, attachment);
        sendLiveMessage(message);
    }

    /**
     * 直播间发消息 - 只发本地
     */
    private void sendLiveMessage(ChatRoomMessage message) {
        new Handler().postDelayed(() -> {
            if (messageListPanel == null) {
                sendLiveMessage(message);
            } else {
                messageListPanel.onMsgSend(message);
            }
        }, 100);
    }

    /**
     * 发送全员禁言提示消息
     */
    public void sendMuteAllMsg(boolean is) {
        sendSenstiveMsg(is ? "全员禁言已开启" : "全员禁言已关闭");
    }

}
