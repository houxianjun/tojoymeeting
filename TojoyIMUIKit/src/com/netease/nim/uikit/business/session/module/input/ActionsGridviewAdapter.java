package com.netease.nim.uikit.business.session.module.input;


import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.netease.nim.uikit.R;
import com.netease.nim.uikit.business.session.actions.BaseAction;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ActionsGridviewAdapter extends BaseAdapter {

    private Context context;

    private List<BaseAction> baseActions;

    private boolean isBlack;

    ActionsGridviewAdapter(Context context, List<BaseAction> baseActions, boolean isBlack) {
        this.context = context;
        this.baseActions = baseActions;
        this.isBlack = isBlack;
    }

    @Override
    public int getCount() {
        return baseActions.size();
    }

    @Override
    public Object getItem(int position) {
        return baseActions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemlayout;
        if (convertView == null) {
            if (isBlack){
                itemlayout = LayoutInflater.from(context).inflate(R.layout.nim_actions_item_black_layout, null);
            }else{
                itemlayout = LayoutInflater.from(context).inflate(R.layout.nim_actions_item_layout, null);
            }
        } else {
            itemlayout = convertView;
        }

        BaseAction viewHolder = baseActions.get(position);
        (itemlayout.findViewById(R.id.imageView)).setBackgroundResource(viewHolder.getIconResId());

        if (!TextUtils.isEmpty(viewHolder.getTitle()) && viewHolder.getTitleId()==0) {
            ((TextView) itemlayout.findViewById(R.id.textView)).setText(viewHolder.getTitle());
        }else{
            ((TextView) itemlayout.findViewById(R.id.textView)).setText(context.getString(viewHolder.getTitleId()));
        }

        return itemlayout;
    }
}

