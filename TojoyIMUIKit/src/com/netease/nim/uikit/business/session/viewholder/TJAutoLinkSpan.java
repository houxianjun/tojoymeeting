package com.netease.nim.uikit.business.session.viewholder;

import android.text.TextPaint;
import android.text.style.URLSpan;

public class TJAutoLinkSpan extends URLSpan {

    private int textColor;

    TJAutoLinkSpan(String url, int color) {
        super(url);
        textColor = color;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        if (ds != null) {
            ds.setColor(textColor);
        }
    }

}