package com.netease.nim.uikit.business.session.viewholder;

import android.graphics.Color;
import android.text.Spannable;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ImageSpan;
import android.text.style.URLSpan;
import android.widget.TextView;

import com.netease.nim.uikit.R;
import com.netease.nim.uikit.api.NimUIKit;
import com.netease.nim.uikit.business.session.emoji.MoonUtil;
import com.tojoy.tjoybaselib.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.tojoy.tjoybaselib.util.sys.ScreenUtil;
import com.netease.nim.uikit.impl.NimUIKitImpl;

/**
 * Created by zhoujianghua on 2015/8/4.
 */
public class MsgViewHolderText extends MsgViewHolderBase {

    private TextView bodyTextView;

    public MsgViewHolderText(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected int getContentResId() {
        return R.layout.nim_message_item_text;
    }

    @Override
    protected void inflateContentView() {
        bodyTextView = findViewById(R.id.nim_message_item_text_body);
    }

    @Override
    protected void bindContentView() {
        layoutDirection();
        bodyTextView.setTextColor(isReceivedMessage() ? Color.BLACK : Color.WHITE);
        bodyTextView.setOnClickListener(v -> onItemClick());
        MoonUtil.identifyFaceExpression(NimUIKit.getContext(), bodyTextView, getDisplayText(), ImageSpan.ALIGN_BOTTOM);
        bodyTextView.setMovementMethod(LinkMovementMethod.getInstance());
        bodyTextView.setOnLongClickListener(longClickListener);

        if (bodyTextView.getText() instanceof Spannable) {
            URLSpan[] urlSpans = (((Spannable) bodyTextView.getText()).getSpans(0, bodyTextView.getText().length() - 1, URLSpan.class));
            for (URLSpan urlSpan : urlSpans) {
                String url = urlSpan.getURL();
                int start = ((Spannable) bodyTextView.getText()).getSpanStart(urlSpan);
                int end = ((Spannable) bodyTextView.getText()).getSpanEnd(urlSpan);
                TJAutoLinkSpan noUnderlineSpan = new TJAutoLinkSpan(url, isReceivedMessage() ? NimUIKit.getContext().getResources().getColor(R.color.color_0f88eb) : Color.WHITE);
                Spannable s = (Spannable) bodyTextView.getText();
                s.setSpan(noUnderlineSpan, start, end, Spanned.SPAN_POINT_MARK);
            }
        }
    }

    private void layoutDirection() {
        if (isReceivedMessage()) {
            bodyTextView.setBackgroundResource(NimUIKitImpl.getOptions().messageLeftBackground);
            bodyTextView.setTextColor(Color.BLACK);
            bodyTextView.setPadding(ScreenUtil.dip2px(15), ScreenUtil.dip2px(8), ScreenUtil.dip2px(10), ScreenUtil.dip2px(8));
        } else {
            bodyTextView.setBackgroundResource(NimUIKitImpl.getOptions().messageRightBackground);
            bodyTextView.setTextColor(Color.WHITE);
            bodyTextView.setPadding(ScreenUtil.dip2px(10), ScreenUtil.dip2px(8), ScreenUtil.dip2px(15), ScreenUtil.dip2px(8));
        }
    }

    @Override
    protected int leftBackground() {
        return 0;
    }

    @Override
    protected int rightBackground() {
        return 0;
    }

    protected String getDisplayText() {
        return message.getContent();
    }
}
