package com.netease.nim.uikit.business.uinfo;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.netease.nim.uikit.R;
import com.netease.nim.uikit.common.NIMCache;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.robot.model.RobotAttachment;
import com.tojoy.tjoybaselib.configs.OSSConfig;

public class HeadImageView extends SimpleDraweeView {

    public static final int DEFAULT_AVATAR_THUMB_SIZE = (int) NIMCache.getContext().getResources().getDimension(R.dimen.avatar_max_size);
    public static final int DEFAULT_AVATAR_NOTIFICATION_ICON_SIZE = (int) NIMCache.getContext().getResources().getDimension(R.dimen.avatar_notification_size);
    private static final int DEFAULT_AVATAR_RES_ID = R.drawable.icon_dorecord;

    public HeadImageView(Context context) {
        super(context);
    }

    public HeadImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HeadImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * 加载用户头像（默认大小的缩略图）
     *
     * @param account 用户账号
     */
    public void loadBuddyAvatar(String account) {
        if (TextUtils.isEmpty(account)) {
            doLoadImage("", DEFAULT_AVATAR_RES_ID, DEFAULT_AVATAR_THUMB_SIZE);
        }
    }


    /**
     * 加载用户头像（默认大小的缩略图）
     *
     * @param message 消息
     */
    public void loadBuddyAvatar(IMMessage message) {
        String account = message.getFromAccount();
        if (message.getMsgType() == MsgTypeEnum.robot) {
            RobotAttachment attachment = (RobotAttachment) message.getAttachment();
            if (attachment.isRobotSend()) {
                account = attachment.getFromRobotAccount();
            }
        }
        loadBuddyAvatar(account);
    }

    /**
     * ImageLoader异步加载
     */
    private void doLoadImage(final String url, final int defaultResId, final int thumbSize) {
        /*
         * 若使用网易云信云存储，这里可以设置下载图片的压缩尺寸，生成下载URL
         * 如果图片来源是非网易云信云存储，请不要使用NosThumbImageUtil
         */

        RoundingParams rp = new RoundingParams();
        rp.setRoundAsCircle(true);
        GenericDraweeHierarchy hierarchy = GenericDraweeHierarchyBuilder.newInstance(getResources()).
                setRoundingParams(rp).
                setFadeDuration(500).
                setPlaceholderImage(defaultResId).
                build();
        this.setHierarchy(hierarchy);


        final String thumbUrl = OSSConfig.getOSSURLedStr(OSSConfig.getStyledImagePath(url, OSSConfig.ResultFormattedMultiKey));

        Uri uri = Uri.parse(thumbUrl);
        setImageURI(uri);
//        ImageLoaderManager.INSTANCE.loadCircleImage(getContext(), this, thumbUrl, R.drawable.icon_dorecord);
    }

}
