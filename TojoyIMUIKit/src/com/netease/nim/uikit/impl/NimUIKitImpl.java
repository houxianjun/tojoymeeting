package com.netease.nim.uikit.impl;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.netease.nim.uikit.api.UIKitOptions;
import com.netease.nim.uikit.api.model.chatroom.ChatRoomMemberChangedObservable;
import com.netease.nim.uikit.api.model.chatroom.ChatRoomProvider;
import com.netease.nim.uikit.api.model.contact.ContactProvider;
import com.netease.nim.uikit.api.model.main.LoginSyncDataStatusObserver;
import com.netease.nim.uikit.api.model.main.OnlineStateChangeObservable;
import com.netease.nim.uikit.api.model.session.SessionEventListener;
import com.netease.nim.uikit.api.model.team.TeamChangedObservable;
import com.netease.nim.uikit.api.model.team.TeamProvider;
import com.netease.nim.uikit.api.model.user.IUserInfoProvider;
import com.netease.nim.uikit.api.model.user.UserInfoObservable;
import com.netease.nim.uikit.business.session.emoji.StickerManager;
import com.netease.nim.uikit.business.session.viewholder.MsgViewHolderBase;
import com.netease.nim.uikit.business.session.viewholder.MsgViewHolderFactory;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nim.uikit.impl.cache.ChatRoomCacheManager;
import com.netease.nim.uikit.impl.cache.DataCacheManager;
import com.netease.nim.uikit.impl.provider.DefaultChatRoomProvider;
import com.netease.nim.uikit.impl.provider.DefaultContactProvider;
import com.netease.nim.uikit.impl.provider.DefaultTeamProvider;
import com.netease.nim.uikit.impl.provider.DefaultUserInfoProvider;
import com.netease.nim.uikit.support.glide.ImageLoaderKit;
import com.netease.nimlib.sdk.AbortableFuture;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.auth.AuthService;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomInfo;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.tojoy.tjoybaselib.util.storage.StorageType;
import com.tojoy.tjoybaselib.util.storage.StorageUtil;
import com.tojoy.tjoybaselib.util.sys.ScreenUtil;
import com.tojoy.file.browser.IMFileProvider;

/**
 * UIKit能力实现类。
 */
public final class NimUIKitImpl {

    // context
    private static Context context;

    // 自己的用户帐号
    private static String account;

    private static UIKitOptions options;

    // 用户信息提供者
    private static IUserInfoProvider userInfoProvider;

    // 通讯录信息提供者
    private static ContactProvider contactProvider;


    private static IMFileProvider fileProvider;

    // 图片加载、缓存与管理组件
    private static ImageLoaderKit imageLoaderKit;

    // 会话窗口消息列表一些点击事件的响应处理函数
    private static SessionEventListener sessionListener;

    // 在线状态变化监听
    private static OnlineStateChangeObservable onlineStateChangeObservable;

    // userInfo 变更监听
    private static UserInfoObservable userInfoObservable;

    //群、群成员信息提供者
    private static TeamProvider teamProvider;

    //群、群成员变化监听
    private static TeamChangedObservable teamChangedObservable;

    // 聊天室提供者
    private static ChatRoomProvider chatRoomProvider;

    // 聊天室成员变更通知
    private static ChatRoomMemberChangedObservable chatRoomMemberChangedObservable;

    /*
     * ****************************** 初始化 ******************************
     */
    public static void init(Context context) {
        init(context, new UIKitOptions(), null, null);
    }

    public static void init(Context context, UIKitOptions options) {
        init(context, options, null, null);
    }

    public static void init(Context context, IUserInfoProvider userInfoProvider, ContactProvider contactProvider) {
        init(context, new UIKitOptions(), userInfoProvider, contactProvider);
    }

    public static void init(Context context, UIKitOptions options, IUserInfoProvider userInfoProvider, ContactProvider contactProvider) {
        NimUIKitImpl.context = context.getApplicationContext();
        NimUIKitImpl.options = options;
        // init tools
        StorageUtil.init(context, options.appCacheDir);
        ScreenUtil.init(context);

        if (options.loadSticker) {
            StickerManager.getInstance().init();
        }

        // init log
        String path = StorageUtil.getDirectoryByDirType(StorageType.TYPE_LOG);
        LogUtil.init(path, Log.DEBUG);

        NimUIKitImpl.imageLoaderKit = new ImageLoaderKit(context);

        if (!options.independentChatRoom) {
            initUserInfoProvider(userInfoProvider);
            initContactProvider(contactProvider);
            // init data cache
            LoginSyncDataStatusObserver.getInstance().registerLoginSyncDataStatus(true);  // 监听登录同步数据完成通知
            DataCacheManager.observeSDKDataChanged(true);
        }

        ChatRoomCacheManager.initCache();
        if (!TextUtils.isEmpty(getAccount())) {
            if (options.initAsync) {
                DataCacheManager.buildDataCacheAsync(); // build data cache on auto login
            } else {
                DataCacheManager.buildDataCache(); // build data cache on auto login
            }
            getImageLoaderKit().buildImageCache(); // build image cache on auto login
        }
    }

    /*
     * ****************************** 登录登出 ******************************
     */
    public static AbortableFuture<LoginInfo> login(LoginInfo loginInfo, final RequestCallback<LoginInfo> callback) {

        AbortableFuture<LoginInfo> loginRequest = NIMClient.getService(AuthService.class).login(loginInfo);
        loginRequest.setCallback(new RequestCallback<LoginInfo>() {
            @Override
            public void onSuccess(LoginInfo loginInfo) {
                loginSuccess(loginInfo.getAccount());
                callback.onSuccess(loginInfo);
            }

            @Override
            public void onFailed(int code) {
                callback.onFailed(code);
            }

            @Override
            public void onException(Throwable exception) {
                callback.onException(exception);
            }
        });
        return loginRequest;
    }

    public static void loginSuccess(String account) {
        setAccount(account);
        DataCacheManager.buildDataCache();
        getImageLoaderKit().buildImageCache();
    }

    public static void logout() {
        DataCacheManager.clearDataCache();
        ChatRoomCacheManager.clearCache();
        getImageLoaderKit().clear();
        LoginSyncDataStatusObserver.getInstance().reset();
        NIMClient.getService(AuthService.class).logout();
    }

    public static void enterChatRoomSuccess(EnterChatRoomResultData data, boolean independent) {
        ChatRoomInfo roomInfo = data.getRoomInfo();

        if (independent) {
            setAccount(data.getAccount());
            DataCacheManager.buildRobotCacheIndependent(roomInfo.getRoomId());
        }

        //存储 member
        ChatRoomMember member = data.getMember();
        member.setRoomId(roomInfo.getRoomId());
        ChatRoomCacheManager.saveMyMember(member);
    }

    public static void exitedChatRoom(String roomId) {
        ChatRoomCacheManager.clearRoomCache(roomId);
    }

    public static UIKitOptions getOptions() {
        return options;
    }

    // 初始化用户信息提供者
    private static void initUserInfoProvider(IUserInfoProvider userInfoProvider) {

        if (userInfoProvider == null) {
            userInfoProvider = new DefaultUserInfoProvider();
        }

        NimUIKitImpl.userInfoProvider = userInfoProvider;
    }

    // 初始化联系人信息提供者
    private static void initContactProvider(ContactProvider contactProvider) {
        if (contactProvider == null) {
            contactProvider = new DefaultContactProvider();
        }

        NimUIKitImpl.contactProvider = contactProvider;
    }

    public static IUserInfoProvider getUserInfoProvider() {
        return userInfoProvider;
    }

    public static UserInfoObservable getUserInfoObservable() {
        if (userInfoObservable == null) {
            userInfoObservable = new UserInfoObservable(context);
        }
        return userInfoObservable;
    }

    public static ContactProvider getContactProvider() {
        return contactProvider;
    }

    public static TeamProvider getTeamProvider() {
        if (teamProvider == null) {
            teamProvider = new DefaultTeamProvider();
        }
        return teamProvider;
    }

    public static TeamChangedObservable getTeamChangedObservable() {
        if (teamChangedObservable == null) {
            teamChangedObservable = new TeamChangedObservable(context);
        }
        return teamChangedObservable;
    }

    public static ChatRoomProvider getChatRoomProvider() {
        if (chatRoomProvider == null) {
            chatRoomProvider = new DefaultChatRoomProvider();
        }
        return chatRoomProvider;
    }

    public static ChatRoomMemberChangedObservable getChatRoomMemberChangedObservable() {
        if (chatRoomMemberChangedObservable == null) {
            chatRoomMemberChangedObservable = new ChatRoomMemberChangedObservable(context);
        }
        return chatRoomMemberChangedObservable;
    }

    public static void setFileProvider(IMFileProvider fileProvider) {
        NimUIKitImpl.fileProvider = fileProvider;
    }

    public static ImageLoaderKit getImageLoaderKit() {
        return imageLoaderKit;
    }

    public static void registerMsgItemViewHolder(Class<? extends MsgAttachment> attach, Class<? extends MsgViewHolderBase> viewHolder) {
        MsgViewHolderFactory.register(attach, viewHolder);
    }


    public static void registerTipMsgViewHolder(Class<? extends MsgViewHolderBase> viewHolder) {
        MsgViewHolderFactory.registerTipMsgViewHolder(viewHolder);
    }

    public static void setAccount(String account) {
        NimUIKitImpl.account = account;
    }

    public static SessionEventListener getSessionListener() {
        return sessionListener;
    }

    public static void setSessionListener(SessionEventListener sessionListener) {
        NimUIKitImpl.sessionListener = sessionListener;
    }
    /*
     * ****************************** 在线状态 ******************************
     */

    public static OnlineStateChangeObservable getOnlineStateChangeObservable() {
        if (onlineStateChangeObservable == null) {
            onlineStateChangeObservable = new OnlineStateChangeObservable(context);
        }
        return onlineStateChangeObservable;
    }

    /*
     * ****************************** basic ******************************
     */
    public static Context getContext() {
        return context;
    }

    public static String getAccount() {
        return account;
    }
}
