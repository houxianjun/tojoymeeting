package com.netease.nim.uikit.impl.cache;

/**
 * Created by huangjun on 2015/10/13.
 */
class UIKitLogTag {
    static final String FRIEND_CACHE = "FRIEND_CACHE";
    static final String USER_CACHE = "USER_CACHE";
    static final String TEAM_CACHE = "TEAM_CACHE";
    static final String ROBOT_CACHE = "ROBOT_CACHE";
}
