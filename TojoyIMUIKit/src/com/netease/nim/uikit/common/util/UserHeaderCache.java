package com.netease.nim.uikit.common.util;

import android.annotation.SuppressLint;
import android.content.Context;

import com.netease.nim.uikit.business.uinfo.UserInfoHelper;
import com.tojoy.tjoybaselib.util.storage.PreferencesUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chengyanfang on 2018/5/15.
 */

public class UserHeaderCache {

    private static final String USER_INFO = "nim_userinfo_propertiesInfo";
    @SuppressLint("StaticFieldLeak")
    private static PreferencesUtil mInfoUtil = null;
    /**
     * Preferences文件
     */
    private static PreferencesUtil getPreferencesUtil(Context context) {
        if (mInfoUtil == null) {
            mInfoUtil = new PreferencesUtil(context, USER_INFO);
        }
        return mInfoUtil;
    }

    private static String getHeaderByUserId(Context context, String userId) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("headerPic:" + userId);
    }

    private static void saveUserId(Context context, String userId, String headerPic) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putString("headerPic:" + userId, headerPic);
    }

    public static void saveUserHeaderToCache(Context context, String userId, String header, boolean checkFriend) {
        if (!header.equals(getHeaderByUserId(context, userId))) {
            saveUserId(context, userId, header);
            List<String> accouts = new ArrayList<>();
            accouts.add(userId);
            UserInfoHelper.notifyChanged(accouts);
        }
    }
}
