package com.netease.nim.uikit.common;

import android.content.Context;

import com.netease.nim.uikit.api.NimUIKit;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.StatusBarNotificationConfig;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.model.user.IMTokenResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * Created by chengyanfang on 2017/10/20.
 */

public class IMLoginManager {

    private static IMLoginManager mInterface;

    private boolean reTry = false;

    public static IMLoginManager getInterface() {
        if (mInterface == null) {
            synchronized (IMLoginManager.class) {
                mInterface = new IMLoginManager();
            }
        }
        return mInterface;
    }

    //2019-06-04 新增是否是获取新token的标记
    public void requestLoginIM(Context context, String isNewToken, LoginSuccessCallback loginSuccessCallback) {
        String userId = BaseUserInfoCache.getUserId(context);
        OMAppApiProvider.getInstance().getIMToken(userId, isNewToken, new Observer<IMTokenResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                loginSuccessCallback.onLoginError(1000);
                if (AppConfig.isCanToast()) {
                    Toasty.normal(context, "登录-获取token接口错误 e = " + e.getMessage()).show();
                }
            }

            @Override
            public void onNext(IMTokenResponse imToken) {
                if (imToken.isSuccess()) {
                    BaseUserInfoCache.setIMAccountHeader(context, imToken.data.env);
                    BaseUserInfoCache.saveIMToken(context, imToken.data.token);
                    NimUIKit.setIMHeaderStr(imToken.data.env);
                } else {
                    BaseUserInfoCache.saveIMToken(context, "fail");
                    if (AppConfig.isCanToast()) {
                        Toasty.normal(context, "登录-获取token异常 code= " + imToken.code).show();
                    }
                }
                NIMLogin(context, loginSuccessCallback);
            }
        });
    }

    private void NIMLogin(Context context, LoginSuccessCallback loginSuccessCallback) {
        final String account = BaseUserInfoCache.getIMHeaderedAccount(context);
        final String token = BaseUserInfoCache.getIMToken(context);

        NimUIKit.login(new LoginInfo(account, token), new RequestCallback<LoginInfo>() {
            @Override
            public void onSuccess(LoginInfo param) {
                NIMCache.setAccount(account);
                initNotificationConfig();

                loginSuccessCallback.onLoginSuccess();
            }

            @Override
            public void onFailed(int code) {

                if (AppConfig.isCanToast()) {
                    Toasty.normal(context, "登录-登录IM失败 code = " + code).show();
                }

                if (code == 302 && !reTry) {
                    reTry = true;
                    requestLoginIM(context, "1", loginSuccessCallback);
                } else {
                    loginSuccessCallback.onLoginError(-1);
                }
            }

            @Override
            public void onException(Throwable exception) {
                loginSuccessCallback.onLoginError(-1);
                if (AppConfig.isCanToast()) {
                    Toasty.normal(context, "登录-登录IM异常 e = " + exception.getMessage()).show();
                }
            }
        });
    }


    private void initNotificationConfig() {
        // 初始化消息提醒
        NIMClient.toggleNotification(UserPreferences.getNoticeContentToggle());

        // 加载状态栏配置
        StatusBarNotificationConfig statusBarNotificationConfig = UserPreferences.getStatusConfig();
        if (statusBarNotificationConfig == null) {
            statusBarNotificationConfig = NIMCache.getNotificationConfig();
            UserPreferences.setStatusConfig(statusBarNotificationConfig);
        }
        // 更
        // 新配置
        NIMClient.updateStatusBarNotificationConfig(statusBarNotificationConfig);
    }


    public interface LoginSuccessCallback {
        void onLoginSuccess();

        void onLoginError(int code);
    }

}
