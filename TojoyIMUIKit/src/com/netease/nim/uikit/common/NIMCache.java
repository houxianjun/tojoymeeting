package com.netease.nim.uikit.common;

import android.content.Context;

import com.netease.nim.uikit.api.NimUIKit;
import com.netease.nimlib.sdk.StatusBarNotificationConfig;

/**
 * @author chengyanfang
 * @version 1.0.0
 * @function:提供静态变量及静态方法，做数据应用级的缓存
 */

public class NIMCache {

    //上下文
    private static Context context;

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        NIMCache.context = context.getApplicationContext();
//        AVChatKit.setContext(context);
    }


    //账户
    private static String account;

    public static String getAccount() {
        return account;
    }

    public static void setAccount(String account) {
        NIMCache.account = account;
        NimUIKit.setAccount(account);
    }

    public static void clear() {
        account = null;
    }


    //状态栏通知配置
    private static StatusBarNotificationConfig notificationConfig;

    public static void setNotificationConfig(StatusBarNotificationConfig notificationConfig) {
        NIMCache.notificationConfig = notificationConfig;
    }

    static StatusBarNotificationConfig getNotificationConfig() {
        return notificationConfig;
    }

}
