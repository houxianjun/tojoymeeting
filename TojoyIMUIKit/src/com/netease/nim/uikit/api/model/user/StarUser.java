package com.netease.nim.uikit.api.model.user;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * Created by chengyanfang on 2018/5/15.
 */


@Entity
public class StarUser {
    @Id
    public String id;
    public String friend_user;
    public String star_flag;
    public String user_id;
    public String getUser_id() {
        return this.user_id;
    }
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
    public String getStar_flag() {
        return this.star_flag;
    }
    public void setStar_flag(String star_flag) {
        this.star_flag = star_flag;
    }
    public String getFriend_user() {
        return this.friend_user;
    }
    public void setFriend_user(String friend_user) {
        this.friend_user = friend_user;
    }
    public String getId() {
        return this.id;
    }
    public void setId(String id) {
        this.id = id;
    }
    @Generated(hash = 1383687676)
    public StarUser(String id, String friend_user, String star_flag, String user_id) {
        this.id = id;
        this.friend_user = friend_user;
        this.star_flag = star_flag;
        this.user_id = user_id;
    }
    @Generated(hash = 1488264047)
    public StarUser() {
    }
}
