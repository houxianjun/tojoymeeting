package com.netease.nim.uikit.api.model.contact;

/**
 * 通讯录（联系人）数据源提供者
 */
public interface ContactProvider {

    /**
     * 获取备注
     *
     * @param account 账号
     * @return 备注
     */
    String getAlias(String account);

    /**
     * 是否是自己的好友
     *
     * @param account 账号
     * @return 结果
     */
    boolean isMyFriend(String account);
}
