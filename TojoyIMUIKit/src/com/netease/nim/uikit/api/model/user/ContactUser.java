package com.netease.nim.uikit.api.model.user;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class ContactUser {
    @Id
    public String id;
    public String name;
    public String number;
    public String getNumber() {
        return this.number;
    }
    public void setNumber(String number) {
        this.number = number;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getId() {
        return this.id;
    }
    public void setId(String id) {
        this.id = id;
    }
    @Generated(hash = 396755238)
    public ContactUser(String id, String name, String number) {
        this.id = id;
        this.name = name;
        this.number = number;
    }
    @Generated(hash = 390741652)
    public ContactUser() {
    }
}
