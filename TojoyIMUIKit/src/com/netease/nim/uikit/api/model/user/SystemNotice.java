package com.netease.nim.uikit.api.model.user;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * Created by chengyanfang on 2018/5/15.
 */


@Entity
public class SystemNotice {

    @Id
    public String id;
    public String userId;
    public String noticeId;
    public String title;
    public String content;
    public String meetId;
    public String listType;
    public String type;
    public String template;
    public String timestamp;
    public String extra;
    public boolean isRead;
    public String getExtra() {
        return this.extra;
    }
    public void setExtra(String extra) {
        this.extra = extra;
    }
    public String getTimestamp() {
        return this.timestamp;
    }
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getContent() {
        return this.content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getId() {
        return this.id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public boolean getIsRead() {
        return this.isRead;
    }
    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }
    public String getNoticeId() {
        return this.noticeId;
    }
    public void setNoticeId(String noticeId) {
        this.noticeId = noticeId;
    }
    public String getUserId() {
        return this.userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getTemplate() {
        return this.template;
    }
    public void setTemplate(String template) {
        this.template = template;
    }
    public String getListType() {
        return this.listType;
    }
    public void setListType(String listType) {
        this.listType = listType;
    }
    public String getMeetId() {
        return this.meetId;
    }
    public void setMeetId(String meetId) {
        this.meetId = meetId;
    }
    @Generated(hash = 1516691485)
    public SystemNotice(String id, String userId, String noticeId, String title,
            String content, String meetId, String listType, String type, String template,
            String timestamp, String extra, boolean isRead) {
        this.id = id;
        this.userId = userId;
        this.noticeId = noticeId;
        this.title = title;
        this.content = content;
        this.meetId = meetId;
        this.listType = listType;
        this.type = type;
        this.template = template;
        this.timestamp = timestamp;
        this.extra = extra;
        this.isRead = isRead;
    }
    @Generated(hash = 1233077117)
    public SystemNotice() {
    }


//    public class Extra {
//        public String from;
//        public String to;
//        public String content;
//        public String status;
//    }
}
