package com.netease.nim.uikit.api.model.user;

/**
 * Created by chengyanfang on 2018/5/15.
 */

public class SystenMsgExtra {
    public String from;
    public String to;
    public String content;
    public String title;
    public String pic;
    public String status;
    public String module;
    public String moduleDetail;

}
