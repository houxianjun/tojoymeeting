package com.netease.nim.uikit.api.model.user;

import org.json.JSONObject;

/**
 * Created by chengyanfang on 2017/10/17.
 */

public class PushMsgParser {

    /**
     * 执行JSON解析的对象
     */
    private JSONObject jsonObject = null;

    public PushMsgParser(byte[] aDatas) {
        this.initJsonObject(aDatas);
    }

    private void initJsonObject(byte[] datas) {
        try {
            this.jsonObject = new JSONObject(new String(datas, "utf-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public SystemNotice doParser() {
        try {
            SystemNotice aSystemNotice = new SystemNotice();
            SystenMsgExtra extra = new SystenMsgExtra();
            if (jsonObject.has("extra")) {
                aSystemNotice.extra = jsonObject.getString("extra");
                extra = new SystemMsgExtraPraser(aSystemNotice.extra.getBytes()).prase();
                aSystemNotice.userId = extra.to;
            }
            if (jsonObject.has("id")) {
                aSystemNotice.id = jsonObject.getString("id") + extra.to;
                aSystemNotice.noticeId = jsonObject.getString("id");
            }
            if (jsonObject.has("title")) {
                aSystemNotice.title = jsonObject.getString("title");
            } else {
                aSystemNotice.title = "直播招商云";
            }
            if (jsonObject.has("content")) {
                aSystemNotice.content = jsonObject.getString("content");
            }
            if (jsonObject.has("listType")) {
                aSystemNotice.listType = jsonObject.getString("listType");
            }
            if (jsonObject.has("meetId")) {
                aSystemNotice.meetId = jsonObject.getString("meetId");
            }
            if (jsonObject.has("type")) {
                aSystemNotice.type = jsonObject.getString("type");
            }
            if (jsonObject.has("template")) {
                aSystemNotice.template = jsonObject.getString("template");
            }
            if (jsonObject.has("timestamp")) {
                aSystemNotice.timestamp = jsonObject.getString("timestamp");
            }
            return aSystemNotice;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
