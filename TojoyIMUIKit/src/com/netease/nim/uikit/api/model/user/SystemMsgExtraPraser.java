package com.netease.nim.uikit.api.model.user;

import org.json.JSONObject;

/**
 * Created by chengyanfang on 2018/5/15.
 */

public class SystemMsgExtraPraser {

    /**
     * 执行JSON解析的对象
     */
    private JSONObject jsonObject = null;

    public SystemMsgExtraPraser(byte[] aDatas) {
        this.initJsonObject(aDatas);
    }

    private void initJsonObject(byte[] datas) {
        try {
            this.jsonObject = new JSONObject(new String(datas, "utf-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public SystenMsgExtra prase() {
        SystenMsgExtra extra = new SystenMsgExtra();
        try {
            if (jsonObject.has("from")) {
                extra.from = jsonObject.getString("from");
            }

            if (jsonObject.has("to")) {
                extra.to = jsonObject.getString("to");
            }

            if (jsonObject.has("status")) {
                extra.status = jsonObject.getString("status");
            }

            if (jsonObject.has("content")) {
                extra.content = jsonObject.getString("content");
            }

            if (jsonObject.has("title")) {
                extra.title = jsonObject.getString("title");
            }

            if (jsonObject.has("pic")) {
                extra.pic = jsonObject.getString("pic");
            }

            if (jsonObject.has("module")) {
                extra.module = jsonObject.getString("module");
            }

            if (jsonObject.has("moduleDetailId")) {
                extra.moduleDetail = jsonObject.getString("moduleDetailId");
            }
        } catch (Exception e) {
        }

        return extra;
    }

}
