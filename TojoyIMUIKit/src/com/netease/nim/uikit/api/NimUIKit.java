package com.netease.nim.uikit.api;

import android.content.Context;

import com.netease.nim.uikit.api.model.chatroom.ChatRoomMemberChangedObservable;
import com.netease.nim.uikit.api.model.chatroom.ChatRoomProvider;
import com.netease.nim.uikit.api.model.contact.ContactProvider;
import com.netease.nim.uikit.api.model.main.OnlineStateChangeObservable;
import com.netease.nim.uikit.api.model.session.SessionEventListener;
import com.netease.nim.uikit.api.model.team.TeamChangedObservable;
import com.netease.nim.uikit.api.model.team.TeamProvider;
import com.netease.nim.uikit.api.model.user.IUserInfoProvider;
import com.netease.nim.uikit.business.session.viewholder.MsgViewHolderBase;
import com.netease.nim.uikit.impl.NimUIKitImpl;
import com.netease.nim.uikit.impl.provider.DefaultContactProvider;
import com.netease.nim.uikit.impl.provider.DefaultUserInfoProvider;
import com.netease.nim.uikit.support.glide.ImageLoaderKit;
import com.netease.nimlib.sdk.AbortableFuture;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomInfo;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.tojoy.file.browser.IMFileProvider;

import java.util.Set;

/**
 * 云信UI组件接口/定制化入口
 * Created by huangjun on 2017/9/29.
 */

public class NimUIKit {

    /**
     * 初始化UIKit, 用户信息、联系人信息使用 {@link DefaultUserInfoProvider}，{@link DefaultContactProvider}
     * 若用户自行提供 userInfoProvider，contactProvider，请使用 {@link NimUIKitImpl#init(Context, IUserInfoProvider, ContactProvider)}
     *
     * @param context 上下文
     */
    public static void init(Context context) {
        NimUIKitImpl.init(context);
    }

    /**
     * 初始化UIKit, 用户信息、联系人信息使用 {@link DefaultUserInfoProvider}，{@link DefaultContactProvider}
     * 若用户自行提供 userInfoProvider，contactProvider，请使用 {@link NimUIKitImpl#init(Context, IUserInfoProvider, ContactProvider)}
     *
     * @param context 上下文
     * @param option  自定义选项
     */
    public static void init(Context context, UIKitOptions option) {
        NimUIKitImpl.init(context, option);
    }

    /**
     * 初始化UIKit，须传入context以及用户信息提供者
     *
     * @param context          上下文
     * @param userInfoProvider 用户信息提供者
     * @param contactProvider  通讯录信息提供者
     */
    public static void init(Context context, IUserInfoProvider userInfoProvider, ContactProvider contactProvider) {
        NimUIKitImpl.init(context, userInfoProvider, contactProvider);
    }

    /**
     * 初始化UIKit，须传入context以及用户信息提供者
     *
     * @param context          上下文
     * @param option           自定义选项
     * @param userInfoProvider 用户信息提供者
     * @param contactProvider  通讯录信息提供者
     */
    public static void init(Context context, UIKitOptions option, IUserInfoProvider userInfoProvider, ContactProvider contactProvider) {
        NimUIKitImpl.init(context, option, userInfoProvider, contactProvider);
    }

    /**
     * 获取配置项
     *
     * @return UIKitOptions
     */
    public static UIKitOptions getOptions() {
        return NimUIKitImpl.getOptions();
    }


    /**
     * 设置聊天界面的事件监听器
     *
     * @param sessionListener 事件监听器
     */
    public static void setSessionListener(SessionEventListener sessionListener) {
        NimUIKitImpl.setSessionListener(sessionListener);
    }

    /**
     * 设置文件信息提供者
     *
     * @param imFileProvider 文件信息提供者
     */
    public static void setIMFileProvider(IMFileProvider imFileProvider) {
        NimUIKitImpl.setFileProvider(imFileProvider);
    }

    /**
     * 根据IM消息附件类型注册对应的消息项展示ViewHolder
     *
     * @param attach     附件类型
     * @param viewHolder 消息ViewHolder
     */
    public static void registerMsgItemViewHolder(Class<? extends MsgAttachment> attach, Class<? extends MsgViewHolderBase> viewHolder) {
        NimUIKitImpl.registerMsgItemViewHolder(attach, viewHolder);
    }

    /**
     * 注册Tip类型消息项展示ViewHolder
     *
     * @param viewHolder Tip消息ViewHolder
     */
    public static void registerTipMsgViewHolder(Class<? extends MsgViewHolderBase> viewHolder) {
        NimUIKitImpl.registerTipMsgViewHolder(viewHolder);
    }

    /**
     * 手动登陆，由于手动登陆完成之后，UIKit 需要设置账号、构建缓存等，使用此方法登陆 UIKit 会将这部分逻辑处理好，开发者只需要处理自己的逻辑即可
     *
     * @param loginInfo 登陆账号信息
     * @param callback  登陆结果回调
     */
    public static AbortableFuture<LoginInfo> login(LoginInfo loginInfo, final RequestCallback<LoginInfo> callback) {
        return NimUIKitImpl.login(loginInfo, callback);
    }

    /**
     * 释放缓存，一般在注销时调用
     */
    public static void logout() {
        NimUIKitImpl.logout();
    }

    /**
     * 独立模式进入聊天室成功之后，调用此方法
     *
     * @param data        EnterChatRoomResultData
     * @param independent 是否独立模式
     */
    public static void enterChatRoomSuccess(EnterChatRoomResultData data, boolean independent) {
        NimUIKitImpl.enterChatRoomSuccess(data, independent);
    }

    /**
     * 退出聊天室之后，调用此方法
     *
     * @param roomId 聊天室id
     */
    public static void exitedChatRoom(String roomId) {
        NimUIKitImpl.exitedChatRoom(roomId);
    }

    /**
     * 获取上下文
     *
     * @return 必须初始化后才有值
     */
    public static Context getContext() {
        return NimUIKitImpl.getContext();
    }


    /**
     * 设置当前登录用户的帐号
     *
     * @param account 帐号
     */
    public static void setAccount(String account) {
        NimUIKitImpl.setAccount(account);
//        AVChatKit.setAccount(account);
    }

    /**
     * 获取当前登录的账号
     *
     * @return 必须登录成功后才有值
     */
    public static String getAccount() {
        return NimUIKitImpl.getAccount();
    }


    /**
     * 获取 “用户资料” 提供者
     *
     * @return 必须在初始化后获取
     */
    public static IUserInfoProvider getUserInfoProvider() {
        return NimUIKitImpl.getUserInfoProvider();
    }

    /**
     * 获取 “用户关系” 提供者
     *
     * @return 必须在初始化后获取
     */
    public static ContactProvider getContactProvider() {
        return NimUIKitImpl.getContactProvider();
    }

    /**
     * 获取群、群成员信息提供者
     *
     * @return TeamProvider
     */
    public static TeamProvider getTeamProvider() {
        return NimUIKitImpl.getTeamProvider();
    }

    /**
     * 获取群成员变化通知
     *
     * @return TeamChangedObservable
     */
    public static TeamChangedObservable getTeamChangedObservable() {
        return NimUIKitImpl.getTeamChangedObservable();
    }

    /**
     * 获取聊天室信息提供者
     *
     * @return ChatRoomProvider
     */
    public static ChatRoomProvider getChatRoomProvider() {
        return NimUIKitImpl.getChatRoomProvider();
    }

    /**
     * 获取聊天室成员变更监听接口
     *
     * @return ChatRoomMemberChangedObservable
     */
    public static ChatRoomMemberChangedObservable getChatRoomMemberChangedObservable() {
        return NimUIKitImpl.getChatRoomMemberChangedObservable();
    }

    /**
     * 获取图片缓存
     *
     * @return Glide图片缓存
     */
    public static ImageLoaderKit getImageLoaderKit() {
        return NimUIKitImpl.getImageLoaderKit();
    }

    /**
     * 通知在线状态发生变化
     *
     * @param accounts 状态变化的账号
     */
    @Deprecated
    public static void notifyOnlineStateChange(Set<String> accounts) {
        getOnlineStateChangeObservable().notifyOnlineStateChange(accounts);
    }

    /**
     * 获取在线状态变更通知接口
     *
     * @return
     */
    public static OnlineStateChangeObservable getOnlineStateChangeObservable() {
        return NimUIKitImpl.getOnlineStateChangeObservable();
    }


    /**
     * IMHeader
     */
    private static String IMHeaderStr = "";

    public static void setIMHeaderStr(String IMHeaderStr) {
        NimUIKit.IMHeaderStr = IMHeaderStr;
    }


    /**
     * 获取聊天室信息
     * @param imRoomId
     * @param callback
     */
    public static void getChatRoomInfo(String imRoomId, RequestCallback<ChatRoomInfo> callback){
        NIMClient.getService(ChatRoomService.class).fetchRoomInfo(imRoomId).setCallback(callback);
    }

}
