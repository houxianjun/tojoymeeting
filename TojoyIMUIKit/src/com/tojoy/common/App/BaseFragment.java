package com.tojoy.common.App;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.R;
import com.tojoy.tjoybaselib.ui.hud.ProgressHUD;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.KeyboardControlUtil;

public class BaseFragment extends Fragment {

    protected View mView;

    protected boolean isCreateView = false;

    private static final Handler handler = new Handler();

    /**
     * view创建完毕后开始配置View控件
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
    }

    /**
     * 初始化View
     */
    protected void initUI() {
    }

    /**
     * 初始化数据
     */
    protected void initData() {
    }


    /**
     * 加载数据操作,在视图创建之前初始化
     */
    protected void lazyLoad() {
        initData();
    }

    /**
     * 此方法在控件初始化前调用，所以不能在此方法中直接操作控件会出现空指针
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isCreateView) {
            isCreateView = false;
            lazyLoad();
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getUserVisibleHint()) {
            lazyLoad();
        }
    }

    /**
     * NavigationBar UI
     */
    protected View mBottomLine;
    protected ImageView mLeftArrow;
    protected ImageView mRightManagerIv;
    protected TextView mTitle;
    protected RelativeLayout mNormalTitleLayout, mTitleContainer;

    protected void setUpNavigationBar(View view) {
        mBottomLine = view.findViewById(R.id.base_title_line);
        mTitleContainer = view.findViewById(R.id.title_container);
        mNormalTitleLayout = view.findViewById(R.id.rtl_normal_title);
        mLeftArrow = view.findViewById(R.id.iv_basetitle_leftimg);
        mRightManagerIv = view.findViewById(R.id.iv_right_manager_bt);
        mTitle = view.findViewById(R.id.tv_basetitle_cetener);

        int statusBarHeight = AppUtils.getStatusBarHeight(getContext());
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.dip2px(getContext(), 48));
        lps.setMargins(0, statusBarHeight, 0, 0);
        mNormalTitleLayout.setLayoutParams(lps);
    }


    protected void setNavTitle(String title) {
        if (mTitle != null) {
            mTitle.setText(title);
        }
    }

    protected void hideBottomLine() {
        if (mBottomLine != null) {
            mBottomLine.setVisibility(View.GONE);
        }
    }

    protected void showBack() {
        if (mLeftArrow != null) {
            mLeftArrow.setVisibility(View.VISIBLE);
            mLeftArrow.setOnClickListener(v -> getActivity().finish());
        }
    }

    protected void setRightManagerImg(int resId) {
        if (mRightManagerIv != null) {
            mRightManagerIv.setImageResource(resId);
            mRightManagerIv.setVisibility(View.VISIBLE);
            mRightManagerIv.setOnClickListener(v -> onRightManagerTitleClick());
        }
    }

    protected void onRightManagerTitleClick() {

    }

    /**
     * Handler
     */
    protected final Handler getHandler() {
        return handler;
    }

    protected final void postDelayed(final Runnable runnable, long delay) {
        handler.postDelayed(() -> {
            // validate
            if (!isAdded()) {
                return;
            }
            runnable.run();
        }, delay);
    }

    /**
     * ProgressHUD
     */
    protected ProgressHUD mProgressHUD;

    public void showProgressHUD(Context context, String showMessage) {
        if (this.isDetached()) {
            return;
        }
        mProgressHUD = ProgressHUD.show(context, showMessage, true, null);
    }

    public void dismissProgressHUD() {
        try {
            if (mProgressHUD != null) {
                mProgressHUD.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected boolean isVisible;

    @Override
    public void onResume() {
        super.onResume();
        isVisible = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isVisible = false;
        KeyboardControlUtil.hideKeyboard(mLeftArrow);
    }
}
