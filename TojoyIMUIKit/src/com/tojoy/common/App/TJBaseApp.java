package com.tojoy.common.App;

import android.annotation.SuppressLint;
import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.netease.nim.uikit.greendao.DaoMaster;
import com.netease.nim.uikit.greendao.DaoSession;

public class TJBaseApp extends Application {

    @SuppressLint("StaticFieldLeak")
    public static Application mApp;

    public static Application getInstance() {
        return mApp;
    }

    /**
     * 数据库配置
     */
    protected DaoMaster.DevOpenHelper mHelper;
    protected DaoMaster mDaoMaster;
    protected SQLiteDatabase db;
    protected DaoSession mDaoSession;

    /**
     * 通过 DaoMaster 的内部类 DevOpenHelper，可以得到一个便利的 SQLiteOpenHelper 对象。
     * 注意：默认的 DaoMaster.DevOpenHelper 会在数据库升级时，删除所有的表，意味着这将导致数据的丢失。
     * 所以，还应该做一层封装，来实现数据库的安全升级。
     */
    protected void setupDatabase() {
        mHelper = new DaoMaster.DevOpenHelper(this, "notes-db", null);
        db = mHelper.getWritableDatabase();
        // 注意：该数据库连接属于 DaoMaster，所以多个 Session 指的是相同的数据库连接。
        mDaoMaster = new DaoMaster(db);
        mDaoSession = mDaoMaster.newSession();
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }

    public SQLiteDatabase getDataBase() {
        return db;
    }
}
