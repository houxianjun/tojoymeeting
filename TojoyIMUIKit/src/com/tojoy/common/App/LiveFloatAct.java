package com.tojoy.common.App;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;

import com.tencent.rtmp.TXLiveConstants;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.live.floatpalyer.MyPlayView;
import com.tojoy.live.floatpalyer.SuperPlayerConst;
import com.tojoy.live.floatpalyer.SuperPlayerGlobalConfig;
import com.tojoy.live.floatpalyer.SuperPlayerModel;

/**
 * 直播间出来的悬浮窗处理
 */
public class LiveFloatAct extends UI implements MyPlayView.OnSuperPlayerViewCallback {
    // 是否正在播放
    private boolean isStartFloatOnme = false;
    // 是否跳到开启悬浮窗权限页面回来的
    private boolean isSkipToFloatPermission = false;

    protected MyPlayView mSuperPlayerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void initFloatLive() {
        if (mSuperPlayerView != null) {
            mSuperPlayerView.setPlayerViewCallback(this);
        }

    }

    protected void showFloatLive(boolean isShowFloat){
        if (isShowFloat) {
            if (!MyPlayView.isFloating) {
                MyPlayView.isFloating = true;
                isStartFloatOnme = true;
                initSuperVodGlobalSetting();
                initLiveWindow();
                
            }
        } else {
            // 停止小窗口播放
            if (isStartFloatOnme && mSuperPlayerView.mControllerCallback != null) {
                MyPlayView.isFloating = false;
                // 切换到窗口播放
                mSuperPlayerView.requestPlayMode(SuperPlayerConst.PLAYMODE_WINDOW);

//                mSuperPlayerView.mControllerCallback.onBackPressed(SuperPlayerConst.PLAYMODE_FLOAT);
                mSuperPlayerView.resetPlayer();
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
//        if (isSkipToFloatPermission) {
//            // 从开启悬浮窗权限页面回来，再次判断是否开启权限
//            isSkipToFloatPermission = false;
//            initLiveWindow();
//        }
    }

    private void initLiveWindow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(getApplicationContext())) {
                //启动Activity让用户授权
                isSkipToFloatPermission = true;
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                intent.setData(Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, 1001);
            } else {
                playVideoModel();
                showFloatWindow();
            }
        }
    }

    private void playVideoModel() {
        final SuperPlayerModel superPlayerModelV3 = new SuperPlayerModel();
        superPlayerModelV3.title = "";
        superPlayerModelV3.url = "http://200024424.vod.myqcloud.com/200024424_709ae516bdf811e6ad39991f76a4df69.f20.mp4";
        superPlayerModelV3.qualityName = "原画";
        mSuperPlayerView.playWithModel(superPlayerModelV3);
    }

    private void showFloatWindow() {
        if (mSuperPlayerView.getPlayState() == SuperPlayerConst.PLAYSTATE_PLAYING) {
            // 切换到悬浮窗播放
            mSuperPlayerView.requestPlayMode(SuperPlayerConst.PLAYMODE_FLOAT);
        } else {
            mSuperPlayerView.resetPlayer();
            finish();
        }
    }


    private void initSuperVodGlobalSetting() {
        SuperPlayerGlobalConfig prefs = SuperPlayerGlobalConfig.getInstance();
        // 开启悬浮窗播放
        prefs.enableFloatWindow = true;
        // 设置悬浮窗的初始位置和宽高
        SuperPlayerGlobalConfig.TXRect rect = new SuperPlayerGlobalConfig.TXRect();
        rect.x = AppUtils.getWidth(this) - AppUtils.dip2px(this, 112);
        rect.y = (AppUtils.getHeight(this) - AppUtils.dip2px(this, 178)) / 2;
        rect.width = AppUtils.dip2px(this, 100);
        rect.height = AppUtils.dip2px(this, 178);
        prefs.floatViewRect = rect;
        // 播放器默认缓存个数
        prefs.maxCacheItem = 5;
        // 设置播放器渲染模式
        prefs.enableHWAcceleration = true;
        prefs.renderMode = TXLiveConstants.RENDER_MODE_FULL_FILL_SCREEN;
    }

    @Override
    public void onStartFullScreenPlay() {

    }

    @Override
    public void onStopFullScreenPlay() {

    }

    @Override
    public void onClickFloatCloseBtn() {

    }

    @Override
    public void onClickSmallReturnBtn() {

    }

    @Override
    public void onStartFloatWindowPlay() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        if (mSuperPlayerView != null) {
//            // 当不需要播放器时，调用resetPlayer清理播放器内部状态，释放内存。
//            mSuperPlayerView.mControllerCallback.onBackPressed(SuperPlayerConst.PLAYMODE_FLOAT);
//            mSuperPlayerView.resetPlayer();
//        }
    }
}
