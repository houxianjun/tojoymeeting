package com.tojoy.common.Model.Router;

/**
 * @author qll
 * @date 2019/4/29
 * 路由跳转解析参数（由站内信、个推推过来的）
 */
public class AppRouterJumpModel {
    // 头像
    public String avatar;
    // 名字
    public String userName;
    // 用户ID
    public String userId;
    // 直播间ID
    public String roomId;

    public String roomLiveId;
    public String title;
}
