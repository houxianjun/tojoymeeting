package com.tojoy.common.Services.EventBus;

/**
 * 直播间结束：主要用于助手进入引流列表页面、邀请同台页面时，主播结束直播，助手跳转结束页面返回后有回到引流、同台页面
 * @author qll
 */
public class EndLiveEventBus {
    private String endRoomId;
    public boolean isEnd;
    public EndLiveEventBus(String roomId,boolean end){
        this.endRoomId = roomId;
        this.isEnd = end;
    }
}
