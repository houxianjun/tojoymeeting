package com.tojoy.common.Services.EventBus;

/**
 * Created by luuzhu on 2019/9/18.
 */

public class LiveRoomEvent {

    /**
     * 关闭观众直播页
     */
    public static final String LIVE_CLOSE_GUEST = "LIVE_CLOSE_GUEST_LIVE";

    /**
     * 关闭直播页面
     */
    public static final String LIVE_CLOSE_ALL = "LIVE_CLOSE";


    private String type;
    /**
     * 如果回直播间有后续操作，传对应的key id position等
     */
    private String router;

    public LiveRoomEvent(String type, String router) {
        this.type = type;
        this.router = router;
    }

    public String getType() {
        return type;
    }

    public String getRouter() {
        return router;
    }

}
