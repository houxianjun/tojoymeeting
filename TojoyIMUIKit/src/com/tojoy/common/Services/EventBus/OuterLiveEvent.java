package com.tojoy.common.Services.EventBus;

import com.tojoy.tjoybaselib.model.live.SelectOutLiveModel;

import java.util.ArrayList;

/**
 * 引流事件，为了防止在引流列表选择后由于接口没结束直接返回上一页面后导致引流数据没有同步到
 * @author qll
 */
public class OuterLiveEvent {
    public ArrayList<SelectOutLiveModel> respStreamRoomDtoList;
    public OuterLiveEvent(ArrayList<SelectOutLiveModel> liveModels){
        this.respStreamRoomDtoList = liveModels;
    }
}
