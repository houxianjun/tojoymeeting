package com.tojoy.common.Services.EventBus;

/**
 * 支付结果
 * Created by daibin on 2020/1/3.
 */

public class OrderPayResultEvent {

    public boolean isSuccess;

    private String orderId;

    public OrderPayResultEvent(boolean isSuccess, String orderId) {
        this.isSuccess = isSuccess;
        this.orderId = orderId;
    }
}
