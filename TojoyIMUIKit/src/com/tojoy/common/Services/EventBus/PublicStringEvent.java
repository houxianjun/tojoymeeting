package com.tojoy.common.Services.EventBus;

public class PublicStringEvent {

    /**
     * 发布合作成功
     */
    public static final String JUMP_OWN = "JUMP_OWN";
    public static final String EDIT_ADDRESS_SUCCESS = "EDIT_ADDRESS_SUCCESS";
    public static final String ADD_ADDRESS_SUCCESS = "ADD_ADDRESS_SUCCESS";

    /**
     * 退出登录
     */
    public static final String LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
    public static final String LOGIN_SUCCESS = "LOGIN_SUCCESS";


    /**
     * 直播间身份变更处理
     */
    public static final String LIVE_GENGLE_CHANGE = "LIVE_GENGLE_CHANGE";


    /**
     * 聊天室切换
     */
    public static final String CHAT_ROOM_LIVE_REENTER = "CHAT_ROOM_LIVE_REENTER";

    /**
     * 收到会议邀请
     */
    public static final String INVITE_JOIN_MEETING = "INVITE_JOIN_MEETING";


    /**
     * 角色变更
     */
    public static final String ROLE_CHANGE = "ROLE_CHANGE";

    /**
     * 以前不是分销客现在变成分销客了，直播间根据判断是否需要显示分销按钮
     */
    public static final String DISTRIBUTOR_CHANGE = "DISTRIBUTOR_CHANGE";

    public String eventTitle;

    public PublicStringEvent(String eventStr) {
        eventTitle = eventStr;
    }
}
