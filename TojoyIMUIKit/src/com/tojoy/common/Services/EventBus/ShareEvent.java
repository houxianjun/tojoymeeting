package com.tojoy.common.Services.EventBus;

/**
 * Created by chengyanfang on 2017/4/22.
 */

public class ShareEvent {

    public boolean isSuccess;

    public String failurReason;

    public String shareData;

    public ShareEvent(boolean isSuccess, String failurReason) {
        this.isSuccess = isSuccess;
        this.failurReason = failurReason;
    }
}
