package com.tojoy.common.Services.ParamsPicker;

import com.tojoy.image.compressor.UpLoadImageBaseAct;

import java.util.ArrayList;

public class BasePickerAct extends UpLoadImageBaseAct {

    /**
     * 图片控制器
     */
    @Override
    public void uploadImgToOSS() {

    }

    @Override
    public ArrayList<String> getImgsUriList() {
        return new ArrayList<>();
    }

}
