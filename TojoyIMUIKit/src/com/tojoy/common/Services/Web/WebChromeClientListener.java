package com.tojoy.common.Services.Web;

import android.webkit.WebView;

public interface WebChromeClientListener {
    void onReceivedTitle (WebView view, String title);
}
