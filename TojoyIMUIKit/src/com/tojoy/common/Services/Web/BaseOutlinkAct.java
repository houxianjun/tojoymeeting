package com.tojoy.common.Services.Web;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.ValueCallback;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebHistoryItem;
import android.webkit.WebSettings;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.hjhrq1991.library.BridgeWebView;
import com.hjhrq1991.library.BridgeWebViewClient;
import com.hjhrq1991.library.DefaultHandler;
import com.netease.nim.uikit.R;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.net.NetWorkUtils;
import com.tojoy.tjoybaselib.util.storage.CacheUtils;
import com.tojoy.tjoybaselib.util.sys.SoftHideKeyBoardUtil;
import com.tojoy.common.Services.EventBus.ShareEvent;
import com.tojoy.image.photopicker.Extras;
import com.tojoy.image.photopicker.PickImageHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;

/**
 * Created by chengyanfang on 2017/9/19.
 * <p>
 * JS - Natvie  BridgeWebView
 * <p>
 * see detail : https://github.com/lzyzsd/JsBridge
 */

abstract public class BaseOutlinkAct extends UI implements ReWebChomeClient.OpenFileChooserCallBack {
    protected BridgeWebView mWebView;
    protected String mWebUrl;
    protected String title;
    protected BridgeWebViewClient mWebViewClient;
    protected ProgressBar mLoaderBar;
    protected FrameLayout mVideoContainer;
    protected RelativeLayout mRlvContainer;
    protected RelativeLayout mEmptyErrorRlv;
    /**
     * 此参数为页面调用finish而不会立即走onDestory释放资源，所以在onPause时判断是否结束，结束则释放掉资源
     * 例如：横竖屏切换对其他页面造成的影响
     */
    protected boolean isWantFinish;
    protected String meetingId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_web_act_layout);
        setStatusBar();
        CacheUtils.clearAllCache();
        SoftHideKeyBoardUtil.assistActivity(BaseOutlinkAct.this);
        NetWorkUtils.checkNet(BaseOutlinkAct.this);

        mWebUrl = getIntent().getStringExtra("url");
        title = getIntent().getStringExtra("title");
        // 摇奖机特有
        meetingId = getIntent().getStringExtra("meetingId");

        initUI();
        loadWebView();
        initWebView();
        EventBus.getDefault().register(this);
        loadData();
    }


    protected void loadData() {
    }

    protected void initUI() {
        setUpNavigationBar();
        setTitle(title);
        showBack();
        mLeftArrow.setOnClickListener(v -> {
            if (FastClickAvoidUtil.isFastDoubleClick(mLeftArrow.getId())) {
                return;
            }
            onBackPressed();
        });
        setSwipeBackEnable(false);

        mWebView = (BridgeWebView) findViewById(R.id.my_webview);
        mVideoContainer = (FrameLayout) findViewById(R.id.videoContainer);
        mLoaderBar = (ProgressBar) findViewById(R.id.loadbar);
        mRlvContainer = (RelativeLayout) findViewById(R.id.rlv_webcontainer);
        mEmptyErrorRlv = (RelativeLayout) findViewById(R.id.rlv_empty);
        mEmptyErrorRlv.setVisibility(View.GONE);

    }

    private void initWebView() {
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setDatabaseEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setSupportZoom(true);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setTextZoom(100);
        mWebView.setWebChromeClient(new BaseWebChromeClient());
        mWebViewClient = new BridgeWebViewClient(mWebView);
        mWebView.setWebViewClient(mWebViewClient);

        //字体大小不受系统设置字体影响
        mWebView.getSettings().setTextZoom(100);
        mWebView.setDefaultHandler(new DefaultHandler());
        /**
         *  Webview在安卓5.0之前默认允许其加载混合网络协议内容
         *  在安卓5.0之后，默认不允许加载http与https混合内容，需要设置webview允许其加载混合网络协议内容
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
    }

    private void loadWebView() {
        try {
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setDomStorageEnabled(true);
            webSettings.setUseWideViewPort(true);
            webSettings.setLoadWithOverviewMode(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        if (mWebView != null && mWebView.canGoBack()) {
            mWebView.goBack();
            getWebTitle();
        } else {
            super.onBackPressed();
            clearWebView();
        }
    }

    public void clearWebView() {
        mWebView.clearCache(true);
        mWebView.clearHistory();
        mWebView.clearFormData();
        mWebView = null;
    }


    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (customView != null) {
                    hideCustomView();
                } else if (mWebView != null && mWebView.canGoBack()) {
                    mWebView.goBack();
                    getWebTitle();
                } else {
                    isWantFinish = true;
                    finish();
                }
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    public void getWebTitle() {
        if (mWebView == null) {
            return;
        }
        WebBackForwardList forwardList = mWebView.copyBackForwardList();
        WebHistoryItem item = forwardList.getCurrentItem();
        if (item != null) {
            if (TextUtils.isEmpty(title)) {
                setTitle(item.getTitle());
            }
        }
    }


    //微信分享成功通过EventBus回调
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPublicStringEvent(ShareEvent event) {
        if ("回调请求成功".equals(event.failurReason)) {
            mWebView.loadUrl(event.shareData);
        } else if ("回调请求失败".equals(event.failurReason)) {

        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mWebView != null) {
            mWebView.destroy();
            mWebView = null;
        }
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mWebView != null) {
            mWebView.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mWebView != null) {
            mWebView.onResume();
        }
    }

    /**
     * ============================ 文件上传 ================================
     */
    private static final int REQUEST_PICK_ICON = 104;

    private ValueCallback<Uri> mUploadMsg;
    private ValueCallback<Uri[]> mUploadMsg5Plus;

    @Override
    public void openFileChooserCallBack(ValueCallback<Uri> uploadMsg, String acceptType) {
        mUploadMsg = uploadMsg;
        showOptions();
    }

    @Override
    public void showFileChooserCallBack(ValueCallback<Uri[]> filePathCallback) {
        mUploadMsg5Plus = filePathCallback;
        showOptions();
    }

    public void showOptions() {
        PickImageHelper.PickImageOption option = new PickImageHelper.PickImageOption();
        option.titleResId = R.string.up_image;
        option.multiSelect = false;
        option.crop = true;
        option.cropOutputImageWidth = 720;
        option.cropOutputImageHeight = 720;
        PickImageHelper.pickImage(BaseOutlinkAct.this, REQUEST_PICK_ICON, option, dialogInterface -> {
            if (mUploadMsg != null) {
                mUploadMsg.onReceiveValue(null);
            } else {
                mUploadMsg5Plus.onReceiveValue(null);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) {
            if (mUploadMsg != null) {
                mUploadMsg.onReceiveValue(null);
            } else {
                mUploadMsg5Plus.onReceiveValue(null);
            }
            return;
        }
        if (data == null) {
            return;
        }
        switch (requestCode) {
            case REQUEST_PICK_ICON:
                String path = data.getStringExtra(Extras.EXTRA_FILE_PATH);
                Uri uri = Uri.fromFile(new File(path));
                if (mUploadMsg != null) {
                    mUploadMsg.onReceiveValue(uri);
                    mUploadMsg = null;
                } else {
                    mUploadMsg5Plus.onReceiveValue(new Uri[]{uri});
                    mUploadMsg5Plus = null;
                }
                break;
            default:
                break;
        }
    }


    /**
     * =============================  视频控制 ============================
     */
    /**
     * 视频全屏参数
     */
    protected static final FrameLayout.LayoutParams COVER_SCREEN_PARAMS = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    private View customView;
    private WebChromeClient.CustomViewCallback mCallBack;

    public class CustomWebViewChromeClient extends ReWebChomeClient {


        public CustomWebViewChromeClient(OpenFileChooserCallBack openFileChooserCallBack, ProgressBar loaderBar,WebChromeClientListener listener) {
            super(openFileChooserCallBack, loaderBar,listener);
        }

        @Override
        public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
            showCustomView(view, callback);
        }

        @Override
        public void onHideCustomView() {
            hideCustomView();
        }

        @Nullable
        @Override
        public View getVideoLoadingProgressView() {
            FrameLayout frameLayout = new FrameLayout(BaseOutlinkAct.this);
            frameLayout.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
            return frameLayout;
        }
    }


    /**
     * 视频播放全屏
     *
     * @param view
     * @param callback
     */
    private void showCustomView(View view, WebChromeClient.CustomViewCallback callback) {
        // if a view already exists then immediately terminate the new one
        if (customView != null) {
            callback.onCustomViewHidden();
            return;
        }

        this.getWindow().getDecorView();
        fullScreen(true);
        FrameLayout decor = (FrameLayout) getWindow().getDecorView();
        mVideoContainer = new FullscreenHolder(BaseOutlinkAct.this);
        mVideoContainer.addView(view, COVER_SCREEN_PARAMS);
        decor.addView(mVideoContainer, COVER_SCREEN_PARAMS);
        customView = view;
        setStatusBarVisibility(false);
        mCallBack = callback;
    }

    /**
     * 隐藏视频全屏
     */
    private void hideCustomView() {
        if (customView == null) {
            return;
        }

        setStatusBarVisibility(true);
        fullScreen(false);
        FrameLayout decor = (FrameLayout) getWindow().getDecorView();
        decor.removeView(mVideoContainer);
        mVideoContainer = null;
        customView = null;
        mCallBack.onCustomViewHidden();
        mWebView.setVisibility(View.VISIBLE);
    }

    static class FullscreenHolder extends FrameLayout {

        public FullscreenHolder(Context ctx) {
            super(ctx);
            setBackgroundColor(ctx.getResources().getColor(android.R.color.black));
        }

        @Override
        public boolean onTouchEvent(MotionEvent evt) {
            return true;
        }
    }

    private void setStatusBarVisibility(boolean visible) {
        int flag = visible ? 0 : WindowManager.LayoutParams.FLAG_FULLSCREEN;
        getWindow().setFlags(flag, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public void fullScreen(boolean isFull) {
        if (isFull) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }
}