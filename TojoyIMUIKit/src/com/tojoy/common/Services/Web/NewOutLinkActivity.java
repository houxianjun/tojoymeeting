package com.tojoy.common.Services.Web;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebHistoryItem;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.hjhrq1991.library.BridgeWebView;
import com.hjhrq1991.library.BridgeWebViewClient;
import com.hjhrq1991.library.DefaultHandler;
import com.netease.nim.uikit.R;
import com.tojoy.common.Services.EventBus.ShareEvent;
import com.tojoy.common.Services.Router.AppRouter;
import com.tojoy.image.photopicker.Extras;
import com.tojoy.image.photopicker.PickImageHelper;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.net.NetWorkUtils;
import com.tojoy.tjoybaselib.util.net.NetworkUtil;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.storage.CacheUtils;
import com.tojoy.tjoybaselib.util.string.StringUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;

import es.dmoral.toasty.Toasty;

@Route(path = RouterPathProvider.WEBNew)
public class NewOutLinkActivity extends UI implements ReWebChomeClient.OpenFileChooserCallBack{
    protected BridgeWebView mWebView;
    protected String mWebUrl;
    protected String title;
    protected BridgeWebViewClient mWebViewClient;
    protected ProgressBar mLoaderBar;
    protected FrameLayout mVideoContainer;
    protected RelativeLayout mRlvContainer;
    protected RelativeLayout mEmptyErrorRlv;

    /**
     * 此参数为页面调用finish而不会立即走onDestory释放资源，所以在onPause时判断是否结束，结束则释放掉资源
     * 例如：横竖屏切换对其他页面造成的影响
     */
    protected boolean isWantFinish;
    boolean isError = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_out_link);
        setStatusBar();
        CacheUtils.clearAllCache();
        NetWorkUtils.checkNet(NewOutLinkActivity.this);
        EventBus.getDefault().register(this);
        mWebUrl = getIntent().getStringExtra("url");
        title = getIntent().getStringExtra("title");
        initUI();
        initWebView();
        loadData();
    }

    private void initUI() {
        setUpNavigationBar();
        setTitle(title);
        showBackNoFinish();
        setSwipeBackEnable(false);

        mWebView = (BridgeWebView) findViewById(R.id.webview);
        mVideoContainer = (FrameLayout) findViewById(R.id.videoContainer);
        mLoaderBar = (ProgressBar) findViewById(R.id.loadbar);
        mRlvContainer = (RelativeLayout) findViewById(R.id.rlv_webcontainer);
        mEmptyErrorRlv = (RelativeLayout) findViewById(R.id.rlv_empty);
        mEmptyErrorRlv.setVisibility(View.GONE);
    }

    private void initWebView() {
        try {
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setDomStorageEnabled(true);
            webSettings.setUseWideViewPort(true);
            webSettings.setLoadWithOverviewMode(true);

            webSettings.setJavaScriptEnabled(true);
            webSettings.setDomStorageEnabled(true);
            webSettings.setDatabaseEnabled(true);
            webSettings.setBuiltInZoomControls(true);
            webSettings.setDisplayZoomControls(false);
            webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
            webSettings.setAllowFileAccess(true);
            webSettings.setSupportZoom(true);
            webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
            mWebView.getSettings().setTextZoom(100);
            mWebView.setWebChromeClient(new BaseWebChromeClient());
            mWebViewClient = new BridgeWebViewClient(mWebView);
            mWebView.setWebViewClient(mWebViewClient);

            //字体大小不受系统设置字体影响
            mWebView.getSettings().setTextZoom(100);
            mWebView.setDefaultHandler(new DefaultHandler());
            /**
             *  Webview在安卓5.0之前默认允许其加载混合网络协议内容
             *  在安卓5.0之后，默认不允许加载http与https混合内容，需要设置webview允许其加载混合网络协议内容
             */
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadData() {
        showProgressHUD(this, "");
        mWebView.setWebChromeClient(new CustomWebViewChromeClient(NewOutLinkActivity.this, mLoaderBar, (view, title) -> getWebTitle()));
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);

                if (!NetWorkUtils.isNetworkConnected(NewOutLinkActivity.this)) {
                    Toasty.normal(NewOutLinkActivity.this, "网络错误,请检查网络").show();
                }
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);

                if (!NetWorkUtils.isNetworkConnected(NewOutLinkActivity.this)) {
                    Toasty.normal(NewOutLinkActivity.this, "网络错误,请检查网络").show();
                }
                //6.0以上执行
                isError = true;
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                //6.0以下执行
                isError = true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                dismissProgressHUD();
                showErrorPage(isError);
                isError = false;
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed(); // 接受所有证书
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                try {
                    if (url.startsWith("weixin://") || url.startsWith("alipays://") ||
                            url.startsWith("mailto://") || url.startsWith("tel://")
                        //其他自定义的scheme
                    ) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(intent);
                        return true;
                    }
                } catch (Exception e) { //防止crash (如果手机上没有安装处理某个scheme开头的url的APP, 会导致crash)
                    return false;
                }

                return checkSkip(url);
            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                String url = request.getUrl().toString();
                try {
                    if (url.startsWith("weixin://") || url.startsWith("alipays://") ||
                            url.startsWith("mailto://") || url.startsWith("tel://")
                        //其他自定义的scheme
                    ) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(intent);
                        return true;
                    }
                } catch (Exception e) { //防止crash (如果手机上没有安装处理某个scheme开头的url的APP, 会导致crash)
                    return false;
                }

                return checkSkip(url);
            }

        });

        mWebView.loadUrl(mWebUrl);
    }


    /**
     * 显示自定义错误提示页面，用一个View覆盖在WebView
     */
    private void showErrorPage(boolean isError) {
        if (mEmptyErrorRlv != null) {
            mEmptyErrorRlv.setVisibility(isError ? View.VISIBLE : View.GONE);
        }

        if (mWebView != null) {
            mWebView.setVisibility(isError ? View.GONE : View.VISIBLE);
        }

    }

    /**
     * ====================================  跳转控制
     */
    protected boolean checkSkip(String url) {
        Log.d("OutLinkAct:checkSkip:", url);

        if (url.contains("&clearNav")) {
            // 防止重复跳转多次
            if (!FastClickAvoidUtil.isFastDoubleClick(FastClickAvoidUtil.H5_JUMP_ID)) {
                ARouter.getInstance().build(RouterPathProvider.WEBNew)
                        .withString("url", url)
                        .withString("title", "")
                        .navigation();
            }
            return true;
        }
        if (StringUtil.isEmpty(url)) {
            return false;
        }

        if (!url.startsWith("jsb")) {
            return false;
        }

        if (!BaseUserInfoCache.isLogined(NewOutLinkActivity.this)) {
            RouterJumpUtil.startLoginActivity();
            isWantFinish = true;
            finish();
            return true;
        }
        //返回到原生
        if (url.contains("action=goBackNative")) {
            if (url.contains("goBack")) {
                isWantFinish = true;
                finish();
                return true;
            }
        }

        //适配成功埋点 不做处理
        if (url.contains("vrHomeEnter")) {
            return true;
        }
        //检测跳转
        try {
            String[] actionAndKeyArr = getKeyAndId(url);
            String action = actionAndKeyArr[0];
            String key = actionAndKeyArr[1];
            String id = actionAndKeyArr[2];
            if (!NetworkUtil.isNetAvailable(this)) {
                return true;
            }

            if ("toNativePage".equals(action)) {
                if ("H5".equals(key)) {
                    AppRouter.doJump(this, "h5", key, id, id, "");
                } else {
                    AppRouter.doJump(this, "native", key, id, url, "");
                    needFinish(key);
                }

                if (mWebUrl.contains("disAppear=1")) {
                    // 跳转原生后需要关闭当前页面。
                    finish();
                }
                return true;
            } else if ("setNativeHead".equals(action)) {
                if ("1".equals(key)) {
                    setStatusBarDarkMode();
                } else {
                    setStatusBarLightMode();
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    /**
     * @param key
     */
    private void needFinish(String key) {
        if ("mine".equals(key)) {
            finish();
        }
    }

    /***.
     * 获取h5点击跳转的 action 和 key
     * @return
     */
    public static String[] getKeyAndId(String url) {
        String[] resultParams = new String[4];

        try {
            //获取Action
            resultParams[0] = url.substring(url.indexOf("action=") + 7, url.indexOf("?"));
            String keyValueStr = url.substring(url.indexOf("?") + 1, url.length());
            String[] params = keyValueStr.split("&");

            for (int i = 0; i < params.length; i++) {
                if (params[i].contains("key")) {
                    resultParams[1] = params[i].split("=")[1];
                }

                if (params[i].contains("detailId")) {
                    resultParams[2] = params[i].split("=")[1];
                }

                if (params[i].contains("colorType")) {
                    resultParams[1] = params[i].split("=")[1];
                }
            }
        } catch (Exception e) {

        }
        return resultParams;
    }



    public void getWebTitle() {
        if (mWebView == null) {
            return;
        }
        WebBackForwardList forwardList = mWebView.copyBackForwardList();
        WebHistoryItem item = forwardList.getCurrentItem();
        if (item != null) {
            if (TextUtils.isEmpty(title)) {
                setTitle(item.getTitle());
            }
        }
    }

    public void clearWebView() {
        mWebView.clearCache(true);
        mWebView.clearHistory();
        mWebView.clearFormData();
        mWebView = null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mWebView != null) {
            mWebView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mWebView != null) {
            mWebView.onPause();
        }
    }

    @Override
    protected void onLeftManagerImageClick() {
        super.onLeftManagerImageClick();
        if (FastClickAvoidUtil.isFastDoubleClick(mLeftArrow.getId())) {
            return;
        }
        onBackPressed();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (customView != null) {
                    hideCustomView();
                } else if (mWebView != null && mWebView.canGoBack()) {
                    mWebView.goBack();
                    getWebTitle();
                } else {
                    isWantFinish = true;
                    finish();
                }
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    @Override
    public void onBackPressed() {
        if (mWebView != null && mWebView.canGoBack()) {
            mWebView.goBack();
            getWebTitle();
        } else {
            super.onBackPressed();
            clearWebView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mWebView != null) {
            mWebView.destroy();
            mWebView = null;
        }
        EventBus.getDefault().unregister(this);
    }

    //微信分享成功通过EventBus回调
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPublicStringEvent(ShareEvent event) {
        if ("回调请求成功".equals(event.failurReason)) {
            mWebView.loadUrl(event.shareData);
        } else if ("回调请求失败".equals(event.failurReason)) {

        }
    }

    /**
     * ============================ 文件上传 ================================
     */
    private static final int REQUEST_PICK_ICON = 104;

    private ValueCallback<Uri> mUploadMsg;
    private ValueCallback<Uri[]> mUploadMsg5Plus;

    @Override
    public void openFileChooserCallBack(ValueCallback<Uri> uploadMsg, String acceptType) {
        mUploadMsg = uploadMsg;
        showOptions();
    }

    @Override
    public void showFileChooserCallBack(ValueCallback<Uri[]> filePathCallback) {
        mUploadMsg5Plus = filePathCallback;
        showOptions();
    }

    public void showOptions() {
        PickImageHelper.PickImageOption option = new PickImageHelper.PickImageOption();
        option.titleResId = R.string.up_image;
        option.multiSelect = false;
        option.crop = true;
        option.cropOutputImageWidth = 720;
        option.cropOutputImageHeight = 720;
        PickImageHelper.pickImage(NewOutLinkActivity.this, REQUEST_PICK_ICON, option, dialogInterface -> {
            if (mUploadMsg != null) {
                mUploadMsg.onReceiveValue(null);
            } else {
                mUploadMsg5Plus.onReceiveValue(null);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) {
            if (mUploadMsg != null) {
                mUploadMsg.onReceiveValue(null);
            } else {
                mUploadMsg5Plus.onReceiveValue(null);
            }
            return;
        }
        if (data == null) {
            return;
        }
        switch (requestCode) {
            case REQUEST_PICK_ICON:
                String path = data.getStringExtra(Extras.EXTRA_FILE_PATH);
                Uri uri = Uri.fromFile(new File(path));
                if (mUploadMsg != null) {
                    mUploadMsg.onReceiveValue(uri);
                    mUploadMsg = null;
                } else {
                    mUploadMsg5Plus.onReceiveValue(new Uri[]{uri});
                    mUploadMsg5Plus = null;
                }
                break;
            default:
                break;
        }
    }


    /**
     * =============================  视频控制 ============================
     */
    /**
     * 视频全屏参数
     */
    protected static final FrameLayout.LayoutParams COVER_SCREEN_PARAMS = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    private View customView;
    private WebChromeClient.CustomViewCallback mCallBack;
    public class CustomWebViewChromeClient extends ReWebChomeClient {


        public CustomWebViewChromeClient(OpenFileChooserCallBack openFileChooserCallBack, ProgressBar loaderBar,WebChromeClientListener listener) {
            super(openFileChooserCallBack, loaderBar,listener);
        }

        @Override
        public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
            showCustomView(view, callback);
        }

        @Override
        public void onHideCustomView() {
            hideCustomView();
        }

        @Nullable
        @Override
        public View getVideoLoadingProgressView() {
            FrameLayout frameLayout = new FrameLayout(NewOutLinkActivity.this);
            frameLayout.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
            return frameLayout;
        }
    }


    /**
     * 视频播放全屏
     *
     * @param view
     * @param callback
     */
    private void showCustomView(View view, WebChromeClient.CustomViewCallback callback) {
        // if a view already exists then immediately terminate the new one
        if (customView != null) {
            callback.onCustomViewHidden();
            return;
        }

        this.getWindow().getDecorView();
        fullScreen(true);
        FrameLayout decor = (FrameLayout) getWindow().getDecorView();
        mVideoContainer = new BaseOutlinkAct.FullscreenHolder(NewOutLinkActivity.this);
        mVideoContainer.addView(view, COVER_SCREEN_PARAMS);
        decor.addView(mVideoContainer, COVER_SCREEN_PARAMS);
        customView = view;
        setStatusBarVisibility(false);
        mCallBack = callback;
    }

    /**
     * 隐藏视频全屏
     */
    private void hideCustomView() {
        if (customView == null) {
            return;
        }

        setStatusBarVisibility(true);
        fullScreen(false);
        FrameLayout decor = (FrameLayout) getWindow().getDecorView();
        decor.removeView(mVideoContainer);
        mVideoContainer = null;
        customView = null;
        mCallBack.onCustomViewHidden();
        mWebView.setVisibility(View.VISIBLE);
    }

    static class FullscreenHolder extends FrameLayout {

        public FullscreenHolder(Context ctx) {
            super(ctx);
            setBackgroundColor(ctx.getResources().getColor(android.R.color.black));
        }

        @Override
        public boolean onTouchEvent(MotionEvent evt) {
            return true;
        }
    }

    private void setStatusBarVisibility(boolean visible) {
        int flag = visible ? 0 : WindowManager.LayoutParams.FLAG_FULLSCREEN;
        getWindow().setFlags(flag, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public void fullScreen(boolean isFull) {
        if (isFull) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }
}