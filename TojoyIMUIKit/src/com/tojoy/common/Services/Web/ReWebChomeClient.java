package com.tojoy.common.Services.Web;

import android.net.Uri;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

/**
 * Created by chengyanfang on 2017/12/4.
 */

public class ReWebChomeClient extends WebChromeClient {

    private ProgressBar mLoaderBar;

    private OpenFileChooserCallBack mOpenFileChooserCallBack;

    private WebChromeClientListener webChromeClientListener;

    ReWebChomeClient(OpenFileChooserCallBack openFileChooserCallBack, ProgressBar loaderBar, WebChromeClientListener listener) {
        mLoaderBar = loaderBar;
        mOpenFileChooserCallBack = openFileChooserCallBack;
        webChromeClientListener = listener;
    }


    // For Android  > 4.1.1
    public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
        openFileChooser(uploadMsg, acceptType, capture);
    }

    // For Android 5.0+
    @Override
    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
        mOpenFileChooserCallBack.showFileChooserCallBack(filePathCallback);
        return true;
    }


    public interface OpenFileChooserCallBack {
        void openFileChooserCallBack(ValueCallback<Uri> uploadMsg, String acceptType);

        void showFileChooserCallBack(ValueCallback<Uri[]> filePathCallback);
    }


    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        if (mLoaderBar != null) {
            if (newProgress == 100) {
                mLoaderBar.setProgress(0);
                mLoaderBar.setVisibility(View.GONE);
            } else {
                mLoaderBar.setVisibility(View.VISIBLE);
                mLoaderBar.setProgress(newProgress);
            }
        }
        super.onProgressChanged(view, newProgress);
    }

    @Override
    public void onReceivedTitle(WebView view, String title) {
        super.onReceivedTitle(view, title);
        if (webChromeClientListener != null) {
            webChromeClientListener.onReceivedTitle(view,title);
        }
    }

}
