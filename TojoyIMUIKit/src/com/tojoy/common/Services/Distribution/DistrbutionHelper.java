package com.tojoy.common.Services.Distribution;

import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;

import com.netease.nim.uikit.R;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.util.string.StringUtil;
import com.tojoy.tjoybaselib.util.sys.MathematicalUtils;

/**
 * 推广控制类
 * 规则：
 * 企业员工 && 分销客：（自己的企业，若员工佣金配置了佣金，展示具体分销金额，下一步进入海报页面。若员工佣金没有配置金额，列表不展示具体分销金额，点击推广直接进入海报页面）
 * 1、可共享	：可推广其他企业的会议/商品（显示入口）
 * 2、不可共享：只能分享自己企业的，不能分享其他企业的（只有自己企业的会议、商品显示入口，其他企业的不显示入口）
 *
 * @author qll
 * @date 2020/8/10
 */
public class DistrbutionHelper {

    /**
     * 是否展示推广按钮（可推广（会议/商品） && 自己是分销客 && （自己没有企业归属 | 有企业归属&&设置共享 | 有企业归属&&未设置共享&&自己企业的商品/会议））
     *
     * @param ifDistribution   // 该会议/商品是否设置为可推广
     * @param otherCompanyCode // 该会议/商品所在的企业码
     * @return // true：展示；false：不展示
     */
    public static boolean isShowDistribution(Context context, int ifDistribution, String otherCompanyCode) {
        if (ifDistribution == 1 && BaseUserInfoCache.getiSDistributor(context)) {
            // 商品/会议可推广 && 自己是分销客
            if (BaseUserInfoCache.isHasCompany(context)) {
                // 自己有企业
                if (BaseUserInfoCache.getCompanyCode(context).equals(otherCompanyCode)) {
                    // 自己企业都显示推广布局（无论是否设置共享）
                    return true;
                } else {
                    // 不是自己企业的，判断是否设置了共享（共享可显示，不共享不显示）
                    return BaseUserInfoCache.getIsShareStaff(context);
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * 根据该用户自己的分销等级返回相应等级的分销金额（需要判断员工一二级金额）
     *
     * @param level1Commission         // 普通分销客一级金额
     * @param level2Commission         // 普通分销客户二级金额
     * @param level1CommissionEmployee // 分销客 && 员工 一级金额（自己的企业 && 员工佣金配置了佣金 =  展示具体分销金额）
     * @param level2CommissionEmployee // 分销客 && 员工 二级金额（自己企业 && 没有配置员工佣金 = 不展示具体分销金额）
     * @param otherCompanyCode         // 该会议/商品所在企业
     * @return // 具体金额或者""
     */
    public static String getDistributionMoney(Context context, String level1Commission, String level2Commission, String level1CommissionEmployee, String level2CommissionEmployee, String otherCompanyCode) {
        if (BaseUserInfoCache.isHasCompany(context) && BaseUserInfoCache.getCompanyCode(context).equals(otherCompanyCode)) {
            // 有企业归属 && 自己企业
            if (BaseUserInfoCache.getDistributorLevel(context) > 1) {
                return (!TextUtils.isEmpty(level2CommissionEmployee) && Double.parseDouble(level2CommissionEmployee) > 0) ? level2CommissionEmployee : "";
            } else {
                return (!TextUtils.isEmpty(level1CommissionEmployee) && Double.parseDouble(level1CommissionEmployee) > 0) ? level1CommissionEmployee : "";
            }
        } else {
            // 普通分销客，无企业归属（肯定有佣金，区分销客等级的钱即可）
            if (BaseUserInfoCache.getDistributorLevel(context) > 1) {
                return level2Commission;
            } else {
                return level1Commission;
            }
        }
    }

    /**
     * 获取要显示的分销金额文案（金额字体需放大）
     * ⚠️：此方法只有在需要展示时才调用
     *
     * @param context
     */
    public static SpannableString getMaxBudgetText(Context context, String level1Commission, String level2Commission, String level1CommissionEmployee, String level2CommissionEmployee, String otherCompanyCode, boolean isLive) {
        String commission = getDistributionMoney(context, level1Commission, level2Commission, level1CommissionEmployee, level2CommissionEmployee, otherCompanyCode);
        if (isLive) {
            String msg = String.format(context.getResources().getString(R.string.distribution_money), TextUtils.isEmpty(commission) ? "0" : commission);
            return StringUtil.setDifferentTextSize(msg, (float) MathematicalUtils.div(30, 24, 2), 2, msg.length() - 1);
        } else {
            String msggoods = String.format(context.getResources().getString(R.string.distribution_money_goods), TextUtils.isEmpty(commission) ? "0" : commission);
            return StringUtil.setDifferentTextSize(msggoods, (float) MathematicalUtils.div(30, 24, 2), 4, msggoods.length() - 1);
        }
    }

    /**
     * 用于判断是否需要跳转佣金弹窗提示问题
     * ⚠️：正常只有展示推广位置只有才会判断，但是不排除中途修改身份或者配置(不考虑异常，默认调用的都是符合前置条件的)
     *
     * @return true：需要；false：不需要
     */
    public static boolean isNeedJumpPop(Context context, String level1CommissionEmployee, String level2CommissionEmployee, String otherCompanyCode) {
        if (BaseUserInfoCache.isHasCompany(context) && BaseUserInfoCache.getCompanyCode(context).equals(otherCompanyCode)) {
            // 有企业归属 && 自己企业
            if (BaseUserInfoCache.getDistributorLevel(context) > 1) {
                return !TextUtils.isEmpty(level2CommissionEmployee) && Double.parseDouble(level2CommissionEmployee) > 0;
            } else {
                return !TextUtils.isEmpty(level1CommissionEmployee) && Double.parseDouble(level1CommissionEmployee) > 0;
            }
        } else {
            // 普通分销客，无企业归属（肯定有佣金）
            return true;
        }
    }
}
