package com.tojoy.common.Services.Push;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.netease.nim.uikit.R;
import com.netease.nim.uikit.api.model.user.SystemNotice;
import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.common.Model.Router.AppRouterJumpModel;

import es.dmoral.toasty.Toasty;

/**
 * Created by chengyanfang on 2017/10/17.
 */

public class NotificationUtil {

    private static NotificationUtil mNotificationUtil;

    private NotificationManager mNotifyManager;
    private PendingIntent mPendingIntent;

    private Context mContext;
    public static int pushId = 10;

    public static NotificationUtil getNotificationUtils(Context context) {
        if (mNotificationUtil == null)
            mNotificationUtil = new NotificationUtil(context);
        return mNotificationUtil;
    }

    private NotificationUtil(Context context) {
        this.mContext = context;

        mNotifyManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // 通知渠道的id
            String id = "tojoyPushChannel";
            // 用户可以看到的通知渠道的名字.
            CharSequence name = "系统通知";
            // 用户可以看到的通知渠道的描述
            String description = "系统通知";
            int importance = NotificationManager.IMPORTANCE_HIGH;

            NotificationChannel mChannel = new NotificationChannel(id, name, importance);
            // 配置通知渠道的属性
            mChannel.setDescription(description);
            // 设置通知出现时的闪灯（如果 android 设备支持的话）
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.GREEN);
            // 设置通知出现时的震动（如果 android 设备支持的话）
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            //最后在notificationmanager中创建该通知渠道
            //
            mNotifyManager.createNotificationChannel(mChannel);
        }
    }

    /**
     * 个推 透传
     *
     * @param message
     * @param intent
     */
    public void showPushNotice(SystemNotice message, Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setAction(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mPendingIntent = PendingIntent.getActivity(mContext.getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification;
        notification = getNotification(mContext.getString(R.string.app_name), message.content, 0, message.id);
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        notification.contentView = NotifyView.getPushRemoteViews(mContext, message.title, message.content);

//        int pushId = 10;
//        try {
//            pushId = Integer.parseInt(message.id);
//        } catch (Exception e) {
//        }

        mNotifyManager.notify(pushId++, notification);
    }

    private Notification getNotification(String title, String text, int num, String channelId) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification notification = new Notification.Builder(this.mContext)
                    .setSmallIcon(R.mipmap.icon_launcher)
                    .setContentIntent(mPendingIntent)
                    .setNumber(num)
                    .setDefaults(Notification.DEFAULT_VIBRATE)
                    .setTicker(text)
                    .setContentTitle(title)
                    .setChannelId("tojoyPushChannel")
                    .build();
            return notification;
        } else {
            Notification notification = new Notification();
            notification.icon = R.mipmap.icon_launcher;
            notification.tickerText = text;
            notification.defaults = Notification.DEFAULT_VIBRATE;
            notification.contentIntent = mPendingIntent;
            notification.flags = Notification.FLAG_AUTO_CANCEL;
            notification.number = num;
            return notification;
        }
    }

    public void clearAllNotification(Context context){
        this.mContext = context;
        if (mNotifyManager == null) {
            mNotifyManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        }
        mNotifyManager.cancelAll();
    }
}
