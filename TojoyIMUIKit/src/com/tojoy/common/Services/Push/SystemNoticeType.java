package com.tojoy.common.Services.Push;

/**
 * 图同通知
 */
public class SystemNoticeType {

    //系统通知
    public static String SYSTEM_NOTICE = "3";
    //活动通知
    public static String ACTIVIT_NOTICE = "4";
    public static String ACTIVIT_INVITE = "77";
    public static String CHANGE_ROLE = "900";
    //踢人下线
    public static String SYSTEM_KICK_OUT = "8";

    /**
     * 聊天室敏感词手动过滤
     */
    public static final String SENSTIVE = "100";

    /**
     * 聊天室身份变更
     */
    public static final String CR_MEMBER_TYPE_CHANGED = "101";
    /**
     * 订单支付结果
     */
    public static final String ORDER_PAY_SUC_RESULT = "313";

    public static final String CLOSE_LIVE_STREAM = "201";
    public static final String CLOSE_ENTERPRISE_FORBID = "308";

    /**
     * 引流的房间流增加
     */
    public static final String OUT_LIVE_ROOM_LIVE_ADD = "292";

    /**
     * 引流的房间流减少
     */
    public static final String OUT_LIVE_ROOM_LIVE_DECREASE = "291";
}
