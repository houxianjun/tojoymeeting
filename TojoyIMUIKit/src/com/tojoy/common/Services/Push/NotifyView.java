package com.tojoy.common.Services.Push;

import android.content.Context;
import android.widget.RemoteViews;

import com.netease.nim.uikit.R;

/**
 * Created by chengyanfang on 2017/10/17.
 */

public class NotifyView {
    public static RemoteViews getPushRemoteViews(Context mContext, String title, String content) {
        RemoteViews aRemoteViews = new RemoteViews(mContext.getPackageName(), R.layout.view_push_notice_layout);

        aRemoteViews.setImageViewResource(R.id.push_icon, R.mipmap.icon_launcher);

        aRemoteViews.setTextViewText(R.id.push_tile, title);

        aRemoteViews.setTextViewText(R.id.push_content, content);

        return aRemoteViews;
    }
}
