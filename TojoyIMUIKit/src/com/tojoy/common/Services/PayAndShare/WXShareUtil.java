package com.tojoy.common.Services.PayAndShare;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.text.TextUtils;

import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXImageObject;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXMiniProgramObject;
import com.tencent.mm.opensdk.modelmsg.WXTextObject;
import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.EnvironmentInstance;

import es.dmoral.toasty.Toasty;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author qll
 * 微信分享相关功能：
 * 1、分享微信好友（链接、图片、小程序）
 * 2、分享微信朋友圈（链接、图片）
 * 3、打开微信小程序
 */
public class WXShareUtil {

    /**
     * 分享到微信
     *
     * @param cover  封面图片
     * @param target 链接地址
     * @param title  标题
     * @param desc   内容
     * @param isWxFriend 是否分享微信好友：true（微信好友）；false（微信朋友圈）
     */
    public static void shareToWeixin(Context context, String cover, String target, String title, String desc,boolean isWxFriend) {
        if (WXCheckUtils.checkWeixinApp(context)) {
            return;
        }
        if (WXCheckUtils.checkShareFiled(context, cover, target, title, desc)) {
            return;
        }
        IWXAPI msgApi = WXAPIFactory.createWXAPI(context, null);
        msgApi.registerApp(AppConfig.WXPAY_APP_ID);
        Observable<SendMessageToWX.Req> observable = Observable.create(subscriber -> {
            if (subscriber.isUnsubscribed()) {
                return;
            }

            if ("分享文本".equals(desc)) {
                try {
                    WXTextObject textObj = new WXTextObject();
                    textObj.text = title;

                    //用 WXTextObject 对象初始化一个 WXMediaMessage 对象
                    WXMediaMessage msg = new WXMediaMessage();
                    msg.mediaObject = textObj;
                    msg.description = title;

                    SendMessageToWX.Req req = new SendMessageToWX.Req();
                    req.transaction = buildTransaction("text");
                    req.message = msg;
                    req.scene = isWxFriend ? SendMessageToWX.Req.WXSceneSession : SendMessageToWX.Req.WXSceneTimeline;

                    subscriber.onNext(req);
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            } else {
                try {
                    WXWebpageObject webObj = new WXWebpageObject();
                    webObj.webpageUrl = target;

                    WXMediaMessage msg = new WXMediaMessage(webObj);
                    msg.title = title;
                    msg.description = TextUtils.isEmpty(desc) ? " " : desc;
                    Bitmap bmp = WXCheckUtils.loadBitmap(cover);
                    if (bmp == null) {
                        // 由于分享时H5、后台传递的imgUrl地址有问题导致分享失败
                        new Handler().post(() -> {
                            Toasty.normal(context, "封面图片错误").show();
                        });
                        return;
                    }
                    msg.thumbData = WXCheckUtils.getBitmapBytes(bmp);
                    bmp.recycle();

                    SendMessageToWX.Req req = new SendMessageToWX.Req();
                    req.transaction = buildTransaction("webpage");
                    req.message = msg;
                    req.scene = isWxFriend ? SendMessageToWX.Req.WXSceneSession : SendMessageToWX.Req.WXSceneTimeline;

                    subscriber.onNext(req);
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        });
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(req -> msgApi.sendReq(req));
    }

    /**
     * 分享到微信（小程序的形式）
     * @param context
     * @param cover
     * @param target
     * @param title
     * @param desc
     */
    public static void shareToWeixinMinProgram(Context context, String cover, String target, String title, String desc, String userName, String path) {
        if (WXCheckUtils.checkWeixinApp(context)) {
            return;
        }
        if (WXCheckUtils.checkShareFiled(context, cover, target, title, desc)) {
            return;
        }
        requestShareMiniProgram(context, cover, target, title, desc, true,userName,path);
    }

    /**
     * 打开微信小程序
     */
    public static void openToWeixinMini(Context context, String userName, String path) {
        if (WXCheckUtils.checkWeixinApp(context)) {
            return;
        }
        openMiniProgram(context,userName,path);
    }

    /**
     * 单纯分享图片
     * @param context
     * @param isWeixin
     */
    public static void shareImage(Context context,Bitmap imageBitmap, boolean isWeixin){
        if (WXCheckUtils.checkWeixinApp(context)) {
            return;
        }

        if (imageBitmap == null) {
            Toasty.normal(context, "图片不能为空").show();
            return;
        }

        IWXAPI msgApi = WXAPIFactory.createWXAPI(context, null);
        msgApi.registerApp(AppConfig.WXPAY_APP_ID);

        //初始化 WXImageObject 和 WXMediaMessage 对象
        WXImageObject imgObj = new WXImageObject(imageBitmap);
        WXMediaMessage msg = new WXMediaMessage();
        msg.mediaObject = imgObj;

        //设置缩略图
        msg.thumbData = WXCheckUtils.getBitmapBytes(imageBitmap);
        imageBitmap.recycle();
        //构造一个Req
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("img");
        req.message = msg;
        req.scene = isWeixin ? SendMessageToWX.Req.WXSceneSession : SendMessageToWX.Req.WXSceneTimeline;
        //调用api接口，发送数据到微信
        msgApi.sendReq(req);
    }

    /**
     * 分享小程序
     * @param context
     * @param cover     封面图片
     * @param target    链接
     * @param title     标题
     * @param desc      描述
     * @param isWeixin  是否分享到微信好友：true：微信好友；false：朋友圈
     */
    private static void requestShareMiniProgram(Context context, String cover, String target, String title, String desc, boolean isWeixin,String userName,String path){
        IWXAPI msgApi = WXAPIFactory.createWXAPI(context, null);
        msgApi.registerApp(AppConfig.WXPAY_APP_ID);

        WXMiniProgramObject miniProgramObj = new WXMiniProgramObject();
        // 兼容低版本的网页链接
        miniProgramObj.webpageUrl = target;
        // 正式版:0，测试版:1，体验版:2
        miniProgramObj.miniprogramType = (AppConfig.environmentInstance == EnvironmentInstance.FORMAL) ? WXMiniProgramObject.MINIPTOGRAM_TYPE_RELEASE : WXMiniProgramObject.MINIPROGRAM_TYPE_PREVIEW;
        // 小程序原始id
        miniProgramObj.userName = userName;
        //小程序页面路径
        miniProgramObj.path = path;
        final WXMediaMessage msg = new WXMediaMessage(miniProgramObj);
        // 小程序消息title
        msg.title = title;
        // 小程序消息desc
        msg.description = desc;
        Bitmap bmp = WXCheckUtils.loadBitmap(cover);
        if (bmp == null) {
            // 由于分享时H5、后台传递的imgUrl地址有问题导致分享失败
            Toasty.normal(context, "封面图片错误").show();
            return;
        }
        // 小程序消息封面图片，小于128k(必传)
        msg.thumbData = WXCheckUtils.getBitmapBytes(bmp);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        // transaction字段用于唯一标识一个请求
        req.transaction = buildTransaction("webpage");
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneSession;
        msgApi.sendReq(req);
    }

    /**
     * 拉起微信小程序
     * @param context
     * @param userName
     * @param path
     */
    public static void openMiniProgram(Context context,String userName,String path) {
        IWXAPI msgApi = WXAPIFactory.createWXAPI(context, null);
        msgApi.registerApp(AppConfig.WXPAY_APP_ID);
        WXLaunchMiniProgram.Req req = new WXLaunchMiniProgram.Req();
        // 填小程序原始id
        req.userName = userName;
        req.path = path;
        req.miniprogramType = (AppConfig.environmentInstance == EnvironmentInstance.FORMAL) ? WXLaunchMiniProgram.Req.MINIPTOGRAM_TYPE_RELEASE : WXLaunchMiniProgram.Req.MINIPROGRAM_TYPE_PREVIEW;
        msgApi.sendReq(req);
    }

    /**
     * 创建事务描述
     * @param type
     * @return
     */
    private static String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
    }
}
