package com.tojoy.common.Services.PayAndShare;

import android.content.Context;

import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.EnvironmentInstance;

import org.json.JSONObject;

import es.dmoral.toasty.Toasty;

/**
 * Created by hanxing on 2017/4/20.
 */
public class AllPayRequestUtils {

    /**
     * 拉起小程序，appid用接口返回的
     */
    public static void doMiniPay(Context context, String oderData, String totalMoney, long num) {
        if (WXCheckUtils.checkWeixinApp(context)) {
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject(oderData);
            String appId = jsonObject.getString("appId");
            String userName = jsonObject.getString("userName");
            String path = jsonObject.getString("path");
            path = path + "&from=android&price=" + totalMoney + "&time=" + num;
            IWXAPI api = WXAPIFactory.createWXAPI(context, appId);
            boolean b = api.openWXApp();
            WXLaunchMiniProgram.Req req = new WXLaunchMiniProgram.Req();
            req.userName = userName;
            req.path = path;
            req.miniprogramType = (AppConfig.environmentInstance == EnvironmentInstance.FORMAL) ? WXLaunchMiniProgram.Req.MINIPTOGRAM_TYPE_RELEASE : WXLaunchMiniProgram.Req.MINIPROGRAM_TYPE_PREVIEW;
            api.sendReq(req);
        } catch (Exception e) {
            Toasty.normal(context, "支付参数异常").show();
        }
    }
}
