package com.tojoy.common.Services.PayAndShare.callback;

public interface PayChannelStatusCallBack<T> {

    public void onFail(String msg);
    public void onSuccess(T o);
}
