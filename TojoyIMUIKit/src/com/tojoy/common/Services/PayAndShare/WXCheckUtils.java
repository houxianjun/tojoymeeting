package com.tojoy.common.Services.PayAndShare;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tojoy.common.App.TJBaseApp;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.util.sys.AppUtils;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import es.dmoral.toasty.Toasty;

/**
 * @author qll
 * 微信相关判断的工具类
 */
public class WXCheckUtils {

    /**
     * 检查微信App
     * @return true：没有安装；false：已安装
     */
    public static boolean checkWeixinApp(Context context) {
        IWXAPI msgApi = WXAPIFactory.createWXAPI(context, null);
        msgApi.registerApp(AppConfig.WXPAY_APP_ID);
        if (!msgApi.isWXAppInstalled()) {
            Toasty.normal(context, "没有安装微信客户端").show();
            return true;
        }
        return false;
    }

    /**
     * 检查分享（链接）的必填写字段是否为空
     * @param cover
     * @param target
     * @param title
     * @param desc
     * @return      true：必填字段有为空的；false：检验无问题
     */
    public static boolean checkShareFiled(Context context, String cover, String target, String title, String desc) {
        if ("分享文本".equals(desc)) {
            return false;
        }

        if (TextUtils.isEmpty(cover)) {
            Toasty.normal(context, "预览图不能为空").show();
            return true;
        }
        if (TextUtils.isEmpty(target)) {
            Toasty.normal(context, "链接地址不能为空").show();
            return true;
        }
        if (TextUtils.isEmpty(title)) {
            Toasty.normal(context, "分享标题不能为空").show();
            return true;
        }
        return false;
    }

    /**
     * 根据图片的url路径获得Bitmap对象
     * @param url
     * @return
     */
    public static Bitmap loadBitmap(String url) {
        URL fileUrl = null;
        Bitmap bitmap = null;
        try {
            fileUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) fileUrl.openConnection();
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            bitmap = BitmapFactory.decodeStream(is);
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * Bitmap -> byte[]
     * @param sourceBitmap
     * @return
     */
    public static byte[] getBitmapBytes(Bitmap sourceBitmap) {
        Bitmap bitmap = Bitmap.createScaledBitmap(sourceBitmap, sourceBitmap.getWidth(), sourceBitmap.getHeight(), true);
        byte[] bytes = compressBitmap(sourceBitmap, 30);
        sourceBitmap.recycle();
        bitmap.recycle();
        return bytes;
    }

    /**
     * 压缩图片到指定大小（微信分享图片，要传入二进制数据过去，限制32kb之内）
     * @param image
     * @param maxSize
     * @return
     */
    private static byte[] compressBitmap(Bitmap image, int maxSize) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        // 质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);
        int options = 100;
        // 循环判断如果压缩后图片是否大于maxSize kb,大于继续压缩
        while (baos.toByteArray().length / 1024 > maxSize) {
            // 重置baos即清空baos
            baos.reset();
            // 这里压缩options%，把压缩后的数据存放到baos中
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);
            // 每次都减少10
            options -= 10;
        }
        return baos.toByteArray();
    }
}
