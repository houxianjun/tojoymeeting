package com.tojoy.common.Services.PayAndShare.channel;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.alipay.sdk.app.OpenAuthTask;
import com.tojoy.common.Services.PayAndShare.callback.PayChannelStatusCallBack;
import com.tojoy.common.Services.PayAndShare.channel.IPayChannel;
import com.tojoy.tjoybaselib.model.home.MineWithdrawalAccountBindResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;

import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 支付宝支付渠道
 */
public class PayAliChannel implements IPayChannel {

    String appid="2021001188689261";
    public  PayAliChannel(){

    }

    @Override
    public void doPay() {

    }

    @Override
    public void bingPayChannel() {

    }


    public void bingPayChannel(Activity activity, PayChannelStatusCallBack openAuthCallback){

        if(checkAliPayInstalled(activity)){


            openAuthScheme(activity,openAuthCallback);
        }
        else{
            Toasty.normal(activity,"没有安装支付宝客户端").show();
        }
    }

    @Override
    public void unbingPayChannel() {

    }

    /**
     * 通用跳转授权业务 Demo
     */
    private void openAuthScheme(Activity activity,PayChannelStatusCallBack openAuthCallback) {

        // 传递给支付宝应用的业务参数
        final Map<String, String> bizParams = new HashMap<>();
        bizParams.put("url", "https://authweb.alipay.com/auth?auth_type=PURE_OAUTH_SDK&app_id="+appid+"&scope=auth_user&state=init");
        // 支付宝回跳到你的应用时使用的 Intent Scheme。请设置为不和其它应用冲突的值。
        // 如果不设置，将无法使用 H5 中间页的方法(OpenAuthTask.execute() 的最后一个参数)回跳至
        // 你的应用。
        //
        // 参见 AndroidManifest 中 <AlipayResultActivity> 的 android:scheme，此两处
        // 必须设置为相同的值。
        final String scheme = "__alipaysdkdemo__";

        // 唤起授权业务
        final OpenAuthTask task = new OpenAuthTask(activity);
        task.execute(
                scheme,    // Intent Scheme
                OpenAuthTask.BizType.AccountAuth, // 业务类型
                bizParams, // 业务参数
                new OpenAuthTask.Callback() {
                    @Override
                    public void onResult(int i, String s, Bundle bundle) {
                        //授权成功
                        if(i==9000) {
                            //授权拒绝/未成功
                            if(bundleToString(bundle).equals("")){
                                openAuthCallback.onFail(s);
                            }
                            //授权成功
                            else{

                            openAuthCallback.onSuccess(bundleToString(bundle));
                            }
                        }
                        else{
                            openAuthCallback.onFail(s);
                        }

                    }
                }, // 业务结果回调。注意：此回调必须被你的应用保持强引用
                true); // 是否需要在用户未安装支付宝 App 时，使用 H5 中间页中转

    }

    @Override
    public void isChannelBind(PayChannelStatusCallBack callBack) {

        OMAppApiProvider.getInstance().queryAliAccountBindingStatus(new Observer<MineWithdrawalAccountBindResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(MineWithdrawalAccountBindResponse response) {

                if(response.isSuccess()){
                    callBack.onSuccess(response);
                }
                else{
                    callBack.onFail(response.msg);
                }
            }
        });
    }

    /**
     * 解析返回授权返回的值
     * @param bundle
     * @return
     */
    public  String bundleToString(Bundle bundle) {
        if (bundle == null) {
            return "null";
        }
        String auth_code="";
        final StringBuilder sb = new StringBuilder();
        for (String key: bundle.keySet()) {
            if(key.equals("auth_code")){
                auth_code=bundle.get(key).toString();
            }
            // sb.append(key).append("=>").append(bundle.get(key)).append("\n");
        }
        return auth_code;
    }
    public static boolean checkAliPayInstalled(Context context) {

        Uri uri = Uri.parse("alipays://platformapi/startApp");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        ComponentName componentName = intent.resolveActivity(context.getPackageManager());
        return componentName != null;
    }
}
