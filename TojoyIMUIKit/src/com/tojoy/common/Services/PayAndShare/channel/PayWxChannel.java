package com.tojoy.common.Services.PayAndShare.channel;

import android.content.Context;

import com.tojoy.common.Services.PayAndShare.AllPayRequestUtils;
import com.tojoy.common.Services.PayAndShare.callback.PayChannelStatusCallBack;
import com.tojoy.common.Services.PayAndShare.channel.IPayChannel;

/**
 * 微信支付渠道
 */
public class PayWxChannel implements IPayChannel {


    public PayWxChannel(){

    }

    @Override
    public void doPay() {

    }

    public void doPay(Context context, String oderData, String totalMoney, long num){
        AllPayRequestUtils.doMiniPay(context,oderData,totalMoney,num);
    }

    @Override
    public void bingPayChannel() {

    }

    @Override
    public void unbingPayChannel() {

    }

    @Override
    public void isChannelBind(PayChannelStatusCallBack callBack) {

    }
}
