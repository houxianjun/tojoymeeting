package com.tojoy.common.Services.PayAndShare.channel;

import com.tojoy.common.Services.PayAndShare.callback.PayChannelStatusCallBack;

public interface IPayChannel {


    //支付方法
    public void  doPay();

    //绑定渠道方法
    public void  bingPayChannel();

    //解除绑定渠道方法
    public void  unbingPayChannel();

    //渠道是否已经绑定/授权
    public void isChannelBind(PayChannelStatusCallBack callBack);
}
