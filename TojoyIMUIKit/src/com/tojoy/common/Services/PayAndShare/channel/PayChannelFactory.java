package com.tojoy.common.Services.PayAndShare.channel;

/**
 *  返回不同支付渠道的工厂类
 */
public class PayChannelFactory  {

    public static IPayChannel getPayChannel(String channelName){
        if(channelName.equals("ali")){

            return new PayAliChannel();
        }
        else if(channelName.equals("wx")){

            return new PayWxChannel();
        }
        return null;

    }
}
