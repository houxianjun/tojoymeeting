package com.tojoy.common.Services.PayAndShare;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import com.netease.nim.uikit.R;
import com.tojoy.tjoybaselib.ui.dialog.PickerViewAnimateUtil;
public class ShareWxPopView {

    private Context context;
    protected ViewGroup contentContainer;
    private Animation outAnim;
    private Animation inAnim;
    private boolean isAnim = true;
    private boolean isShowing;
    private int gravity = Gravity.BOTTOM;
    public ViewGroup decorView;
    private ViewGroup rootView;
    private boolean dismissing;

    private final String SHARE_TOWX = "SHARE_TOWX";
    private final String SHARE_TOWXFRIENDS = "SHARE_TOWXFRIENDS";

    private String mLiveCover, mLiveTarget, mLiveTitle, mLiveDesc;

    //邀请好友 分享
    public ShareWxPopView(Context context) {
        this.context = context;
        init();
        initViews();
    }

    private void initViews() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        //decorView是activity的根View
        if (decorView == null) {
            decorView = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        }
        //将控件添加到decorView中
        rootView = (ViewGroup) layoutInflater.inflate(R.layout.share_wx_popview, decorView, false);
        rootView.setLayoutParams(new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        ));
        rootView.setBackgroundColor(context.getResources().getColor(R.color.c66000000));
        contentContainer = rootView.findViewById(R.id.content_container);
        setKeyBackCancelable(true);

        contentContainer.findViewById(R.id.rlv_yuejian).setOnClickListener(view -> {
            inviteProductDetails(context, SHARE_TOWX);
            dismiss();
        });
        contentContainer.findViewById(R.id.llv_share_friends).setOnClickListener(view -> {
            inviteProductDetails(context, SHARE_TOWXFRIENDS);
            dismiss();
        });

        contentContainer.findViewById(R.id.tv_cancel_share).setOnClickListener(v -> {
            dismiss();
        });
    }

    protected void init() {
        inAnim = getInAnimation();
        outAnim = getOutAnimation();
    }

    /**
     * @param v      (是通过哪个View弹出的)
     * @param isAnim 是否显示动画效果
     */
    public void show(View v, boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(View v) {
        show();
    }

    /**
     * 添加View到根视图
     */
    public void show() {
        if (isShowing()) {
            return;
        }
        isShowing = true;
        onAttached(rootView);
        rootView.requestFocus();
    }

    /**
     * show的时候调用
     *
     * @param view 这个View
     */
    private void onAttached(View view) {
        decorView.addView(view);
        if (isAnim) {
            contentContainer.startAnimation(inAnim);
        }
    }

    /**
     * 检测该View是不是已经添加到根视图
     *
     * @return 如果视图已经存在该View返回true
     */
    public boolean isShowing() {
        return rootView.getParent() != null || isShowing;
    }

    public void dismiss() {
        if (dismissing) {
            return;
        }

        if (isAnim) {
            //消失动画
            outAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    dismissImmediately();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            contentContainer.startAnimation(outAnim);
        } else {
            dismissImmediately();
        }
        dismissing = true;
    }

    public void dismissImmediately() {

        decorView.post(() -> {
            //从根视图移除
            decorView.removeView(rootView);
            isShowing = false;
            dismissing = false;
        });

    }

    public Animation getInAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, true);
        return AnimationUtils.loadAnimation(context, res);
    }

    public Animation getOutAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, false);
        return AnimationUtils.loadAnimation(context, res);
    }

    public void setKeyBackCancelable(boolean isCancelable) {
        ViewGroup View;
        View = rootView;
        View.setFocusable(isCancelable);
        View.setFocusableInTouchMode(isCancelable);
        if (isCancelable) {
            View.setOnKeyListener(onKeyBackListener);
        } else {
            View.setOnKeyListener(null);
        }
    }

    private View.OnKeyListener onKeyBackListener = (v, keyCode, event) -> {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == MotionEvent.ACTION_DOWN
                && isShowing()) {
            dismiss();
            return true;
        }
        return false;
    };

    public ShareWxPopView setOutSideCancelable(boolean isCancelable) {
        if (rootView != null) {
            View view = rootView.findViewById(R.id.outmost_container);

            if (isCancelable) {
                view.setOnTouchListener(onCancelableTouchListener);
            } else {
                view.setOnTouchListener(null);
            }
        }
        return this;
    }

    /**
     * Called when the user touch on black overlay in order to dismiss the dialog
     */
    @SuppressLint("ClickableViewAccessibility")
    private final View.OnTouchListener onCancelableTouchListener = (v, event) -> {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            dismiss();
        }
        return false;
    };


    //=================================================处理 分享事件===========================
    //分享产品和企业详情
    private void inviteProductDetails(Context context, String toWhere) {
        if (SHARE_TOWX.equals(toWhere)) {
            //分享到好友
            WXShareUtil.shareToWeixin(context, mLiveCover, mLiveTarget, mLiveTitle, mLiveDesc,true);
        } else if (SHARE_TOWXFRIENDS.equals(toWhere)) {
            //分享到好友
            WXShareUtil.shareToWeixin(context, mLiveCover, mLiveTarget, mLiveTitle, mLiveDesc,false);
        }
    }

    public void setLiveShareData(String liveCover, String liveTarget, String liveTitle, String liveDesc) {
        this.mLiveCover = liveCover;
        this.mLiveTarget = liveTarget;
        this.mLiveTitle = liveTitle;
        this.mLiveDesc = liveDesc;
    }
}
