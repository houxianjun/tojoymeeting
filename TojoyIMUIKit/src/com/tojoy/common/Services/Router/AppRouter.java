package com.tojoy.common.Services.Router;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.alibaba.android.arouter.launcher.ARouter;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.netease.nim.uikit.api.NimUIKit;
import com.netease.nim.uikit.business.chatroom.helper.ChatRoomHelper;
import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.netease.nim.uikit.common.NIMCache;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.enterprise.CorporationMsgResponse;
import com.tojoy.tjoybaselib.model.live.CheckLiveStatusResponse;
import com.tojoy.tjoybaselib.model.user.IMTokenResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.util.string.StringUtil;
import com.tojoy.common.App.TJBaseApp;
import com.tojoy.common.Model.Router.AppRouterJumpModel;
import com.tojoy.common.Services.EventBus.PublicStringEvent;

import org.greenrobot.eventbus.EventBus;

import es.dmoral.toasty.Toasty;
import rx.Observer;

public class AppRouter {

    public static final String SkipType_H5 = "h5";
    public static final String SkipType_Native = "native";
    public static final String PushType_Native = "nativepage";

    /**
     * Usercenter
     */
    private static final String KEY_MINE = "mine";


    // 热门直播间列表
    static final String KEY_NET_MEETING_HOT_LIVE = "netMeetingHotLive";
    // 直播间（moudelDetailId为房间号）
    static final String KEY_NET_MEETING_ROOM = "netMeetingRoom";
    // 直接往直播间里跳
    static final String KEY_NET_MEETING_LIVE_ROOM = "netMeetingLiveRoom";
    // 开会申请页面（申请过）跳的是一个页面
    static final String KEY_NET_MEETING_APPLY = "netMeetingApply";
    // 审批列表
    static final String KEY_NET_MEETING_CHECK = "netMeetingCheck";
    //公司首页
    private static final String KEY_COMPANY_HOME_PAG = "company_home_page";
    // 企业入驻页面
    public static final String KEY_APPLY_ENTERPRISE = "applyEnterprise";


    // TODO: 新增context参数 ，认证弹窗需要当前页面的context（2019/4/23）
    public static void doJump(Context context, String jumpType, String key, String id, String jumpUrl, String urlTitle) {

        //0是无跳转
        if ("0".equals(jumpType)) {
            return;
        }

        //外链
        if ("h5".equals(jumpType) || "1".equals(jumpType)) {
            skipToH5(jumpType, key, id, jumpUrl, urlTitle);
            return;
        }
        //根据具体的Key进行跳转
        switch (key) {
            //========= App模块
            case KEY_NET_MEETING_HOT_LIVE:
                // 热门直播间列表
                ARouter.getInstance().build(RouterPathProvider.ENTERPRISE_ROAD_OR_HOT_LIVE_LIST)
                        .withString("pageTitle", "热门直播间")
                        .navigation();
                break;
            case KEY_NET_MEETING_APPLY:
                // 开会申请页面（申请过）跳的是一个页面
                ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_START_LIVE_BROADCAST_APPLY)
                        .withString("source", "menu")
                        .navigation();
                break;
            case KEY_NET_MEETING_CHECK:
                // 审批列表页面
                ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_MY_APPROVAL_LIST)
                        .navigation();
                break;
            case KEY_NET_MEETING_LIVE_ROOM:
                // 直播间（moudelDetailId为房间号）
                if (!TextUtils.isEmpty(id)) {
                    if (id.startsWith("{")) {
                        AppRouterJumpModel model = null;
                        try {
                            model = new Gson().fromJson(id, AppRouterJumpModel.class);
                        } catch (JsonSyntaxException e) {

                        }

                        if (model == null) {
                            return;
                        }

                        if (TextUtils.isEmpty(model.roomLiveId)) {
                            ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_USER_CENTER)
                                    .withString("hostPic", model.avatar)
                                    .withString("roomId", model.roomId)
                                    .withString("userName", model.userName)
                                    .withString("type", "-1")
                                    .navigation();
                        } else {
                            AppRouterJumpModel finalModel = model;
                            LiveRoomIOHelper.checkEnterLiveRoom(context, model.roomLiveId, new IOLiveRoomListener() {
                                @Override
                                public void onIOError(String error, int errorCode) {

                                    if (errorCode == 301) {
                                        ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_ENTER_ROOM_PSW)
                                                .withString("roomName", finalModel.title)
                                                .withString("liveId", finalModel.roomLiveId)
                                                .navigation();
                                    } else {
                                        Toasty.normal(context, error).show();
                                    }
                                }

                                @Override
                                public void onIOSuccess() {
                                    if (BaseUserInfoCache.getUserId(context).equals(finalModel.userId)) {
                                        ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_LIVE_ROOM)
                                                .withString("liveJson", id)
                                                .navigation();
                                    } else {
                                        ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_SEE_LIVE_ROOM)
                                                .withString("liveJson", id)
                                                .navigation();
                                    }
                                }
                            });
                        }
                    } else {
                        jumpLiveRoom(context, id);
                    }
                }
                break;
            case KEY_COMPANY_HOME_PAG:
                //公司首页
                OMAppApiProvider.getInstance().mCorporationQuery(id, false, new rx.Observer<CorporationMsgResponse>() {
                    @Override
                    public void onCompleted() {
                        //dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(context, "网络错误，请检查网络").show();
                        //dismissProgressHUD();
                    }

                    @Override
                    public void onNext(CorporationMsgResponse omBaseResponse) {
                        if (omBaseResponse != null && omBaseResponse.isSuccess()) {
                            if ("1".equals(String.valueOf(omBaseResponse.data.customState)) && id != null) {
                                ARouter.getInstance().build(RouterPathProvider.OnlineMeetingCustomizedEnterprisePageAct)
                                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                                        .withString("companyCode", id)
                                        .withString("sourceWay","2")
                                        .navigation();
                            } else {
                                Toasty.normal(context, "抱歉，该企业未配置企业主页").show();
                            }
                        } else {
                            Toasty.normal(context, omBaseResponse.msg).show();
                        }

                    }
                });

                break;
            case KEY_APPLY_ENTERPRISE:
                // 企业入驻
                ARouter.getInstance().build(RouterPathProvider.ApplyEnterprise)
                        .navigation();
                break;
            case KEY_MINE:
                // 跳转到我的
                EventBus.getDefault().postSticky(new PublicStringEvent(PublicStringEvent.JUMP_OWN));
                break;
            default:
                break;

        }
//
//        //跳转到我的
//        if (KEY_MINE.equals(key)) {
//            EventBus.getDefault().postSticky(new PublicStringEvent(PublicStringEvent.JUMP_OWN));
//            return;
//        }
    }

    /**
     * @param context
     * @param id      直播间roomLiveId
     */
    public static void jumpLiveRoom(Context context, String id) {
        if (context instanceof UI) {
            ((UI) context).showProgressHUD(context, "处理中");
        }

        OMAppApiProvider.getInstance().getLiveRoomStatus(id, new Observer<CheckLiveStatusResponse>() {
            @Override
            public void onCompleted() {
                if (context instanceof UI) {
                    ((UI) context).dismissProgressHUD();
                }
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(CheckLiveStatusResponse response) {
                if (response.isSuccess()) {

                    if (LiveRoomInfoProvider.getInstance().isHost() && ChatRoomHelper.isFromLiveRoom) {
                        //如果自己正在开播
                        ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_LIVE_ROOM)
                                .withString("liveJson", id)
                                .navigation();
                    } else {

                        if ("2".equals(response.data.status) || "3".equals(response.data.status) || "4".equals(response.data.status)) {
                            if ("1".equals(response.data.havePassword)) {
                                LiveRoomIOHelper.checkEnterLiveRoom(context, id, new IOLiveRoomListener() {
                                    @Override
                                    public void onIOError(String error, int errorCode) {
                                        if (errorCode == 301) {
                                            ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_ENTER_ROOM_PSW)
                                                    .withString("roomName", response.data.title)
                                                    .withString("liveId", id)
                                                    .navigation();

                                        } else {
                                            Toasty.normal(context, error).show();
                                        }
                                        if (context instanceof UI) {
                                            ((UI) context).dismissProgressHUD();
                                        }
                                    }

                                    @Override
                                    public void onIOSuccess() {

                                        joinDirect(context, id);
                                    }
                                });
                            } else {

                                joinDirect(context, id);
                            }
                        } else if ("1".equals(response.data.status)) {
                            if (context instanceof UI) {
                                Toasty.normal(context, "会议未开始").show();
                                ((UI) context).dismissProgressHUD();
                            }
                        } else {
                            if (context instanceof UI) {

                                joinDirect(context, id);

                                ((UI) context).dismissProgressHUD();
                            }
                        }
                    }

                } else {
                    if (response.data != null && !TextUtils.isEmpty(response.data.havePassword) && response.data.havePassword.equals("1")) {
                        ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_ENTER_ROOM_PSW)
                                .withString("liveId", id)
                                .navigation();
                    }
                    if (context instanceof UI) {
                        ((UI) context).dismissProgressHUD();
                    }
                }
            }
        });
    }

    /**
     * @param context
     * @param id      直播间roomLiveId
     */
    public static void jumpLiveRoomDirectet(Context context, String id) {
        if (context instanceof UI) {
            ((UI) context).showProgressHUD(context, "处理中");
        }

        OMAppApiProvider.getInstance().getLiveRoomStatus(id, new Observer<CheckLiveStatusResponse>() {
            @Override
            public void onCompleted() {
                if (context instanceof UI) {
                    ((UI) context).dismissProgressHUD();
                }
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(CheckLiveStatusResponse response) {
                if (response.isSuccess()) {
                    if (LiveRoomInfoProvider.getInstance().isHost() && ChatRoomHelper.isFromLiveRoom) { ////如果自己正在开播
                        ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_LIVE_ROOM)
                                .withString("liveJson", id)
                                .navigation();
                    } else {

                        if ("2".equals(response.data.status) || "3".equals(response.data.status) || "4".equals(response.data.status)) {
                            if ("1".equals(response.data.havePassword)) {
                                LiveRoomIOHelper.checkEnterLiveRoom(context, id, new IOLiveRoomListener() {
                                    @Override
                                    public void onIOError(String error, int errorCode) {
                                        if (errorCode == 301) {
                                            ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_ENTER_ROOM_PSW)
                                                    .withString("roomName", response.data.title)
                                                    .withString("liveId", id)
                                                    .navigation();

                                        } else {
                                            Toasty.normal(context, error).show();
                                        }
                                        if (context instanceof UI) {
                                            ((UI) context).dismissProgressHUD();
                                        }
                                    }

                                    @Override
                                    public void onIOSuccess() {
                                        joinDirect(context, id);
                                    }
                                });
                            } else {
                                joinDirect(context, id);
                            }
                        } else if ("1".equals(response.data.status)) {
                            if (context instanceof UI) {
                                Toasty.normal(context, "会议未开始").show();
                                ((UI) context).dismissProgressHUD();
                            }
                        } else {
                            if (context instanceof UI) {
                                joinDirect(context, id);

                                ((UI) context).dismissProgressHUD();
                            }
                        }
                    }

                } else {
                    if (response.data != null && !TextUtils.isEmpty(response.data.havePassword) && response.data.havePassword.equals("1")) {
                        ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_ENTER_ROOM_PSW)
                                .withString("liveId", id)
                                .navigation();
                    } else {
                        Toasty.normal(context, response.msg).show();
                    }
                    if (context instanceof UI) {
                        ((UI) context).dismissProgressHUD();
                    }
                }
            }
        });
    }


    private static void loginIMDirect(Context mContext, String id) {
        OMAppApiProvider.getInstance().getIMToken(BaseUserInfoCache.getUserId(mContext), "0", new Observer<IMTokenResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                statitiscIMLogin("0");
                if (mContext instanceof UI) {
                    ((UI) mContext).dismissProgressHUD();
                }
                if (AppConfig.isCanToast()) {
                    Toasty.normal(mContext, "getIMToken error = " + e.getMessage()).show();
                } else {
                    Toasty.normal(mContext, "网络错误，请稍后再试").show();
                }
            }

            @Override
            public void onNext(IMTokenResponse imToken) {
                if (imToken.isSuccess()) {
                    BaseUserInfoCache.setIMAccountHeaderBase(mContext, imToken.data.env);
                    NimUIKit.setIMHeaderStr(imToken.data.env);
                    BaseUserInfoCache.saveIMTokenBase(mContext, imToken.data.token);

                    String account = BaseUserInfoCache.getIMHeaderedAccount(mContext);
                    NimUIKit.login(new LoginInfo(account, imToken.data.token), new RequestCallback<LoginInfo>() {
                        @Override
                        public void onSuccess(LoginInfo param) {
                            statitiscIMLogin("1");
                            NIMCache.setAccount(account);
                            joinLiveRoomDirect(mContext, id);
                        }

                        @Override
                        public void onFailed(int code) {
                            statitiscIMLogin("0");
                            if (mContext instanceof UI) {
                                ((UI) mContext).dismissProgressHUD();
                            }
                            if (AppConfig.isCanToast()) {
                                Toasty.normal(mContext, "登录IM 失败 code = " + code).show();
                            } else {
                                Toasty.normal(mContext, "网络错误，请稍后再试").show();
                            }
                        }

                        @Override
                        public void onException(Throwable exception) {
                            statitiscIMLogin("0");
                            if (mContext instanceof UI) {
                                ((UI) mContext).dismissProgressHUD();
                            }
                            if (AppConfig.isCanToast()) {
                                Toasty.normal(mContext, "登录IM 异常 e = " + exception.getMessage()).show();
                            } else {
                                Toasty.normal(mContext, "网络错误，请稍后再试").show();
                            }
                        }
                    });

                } else {
                    statitiscIMLogin("0");
                    if (mContext instanceof UI) {
                        ((UI) mContext).dismissProgressHUD();
                    }
                    if (AppConfig.isCanToast()) {
                        Toasty.normal(mContext, "getIMToken 异常 = " + imToken.msg).show();
                    } else {
                        Toasty.normal(mContext, "网络错误，请稍后再试").show();
                    }
                }

            }
        });
    }


    public static void joinDirect(Context context, String id) {
        if (AppConfig.isLoginIM) {
            joinLiveRoomDirect(context, id);
        } else {
            loginIMDirect(context, id);
        }
    }

    private static void joinLiveRoomDirect(Context context, String id) {
        LiveRoomInfoProvider.getInstance().joinPassword = "";
        LiveRoomInfoProvider.getInstance().liveId = id;
        LiveRoomIOHelper.joinLiveRoom(context, new IOLiveRoomListener() {
            @Override
            public void onIOError(String error, int errorCode) {
                if (!TextUtils.isEmpty(error)) {
                    Toasty.normal(context, error).show();
                }
            }

            @Override
            public void onIOSuccess() {
                LiveRoomIOHelper.exitOldUser(BaseUserInfoCache.getUserId(context));
            }
        });
    }

    private static void statitiscIMLogin(String loginIMState) {
        OMAppApiProvider.getInstance().statitiscIMLogin(loginIMState, "enterLivePage", new rx.Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(OMBaseResponse omBaseResponse) {
            }
        });
    }

    // TODO: 新增context参数 ，认证弹窗需要当前页面的context（2019/4/23）
    public static void doJumpAd(Context context, String jumpType, String key, String id, String jumpUrl, String urlTitle, String adLinkId, String adLinkLiveId, boolean isPw) {

        //0是无跳转
        if ("1".equals(jumpType)) {
            return;
        }

        //外链
        if ("h5".equals(jumpType) || "2".equals(jumpType)) {
            skipToH5(jumpType, key, id, jumpUrl, urlTitle);
            return;
        }
        //根据具体的Key进行跳转
        switch (key) {
            //========= App模块
            case KEY_NET_MEETING_LIVE_ROOM://会议
                AppRouter.jumpLiveRoomDirectet(context, adLinkLiveId);
                break;
            case KEY_COMPANY_HOME_PAG://公司首页

                OMAppApiProvider.getInstance().mCorporationQuery(adLinkId, false, new rx.Observer<CorporationMsgResponse>() {
                    @Override
                    public void onCompleted() {
                        //dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(context, "网络错误，请检查网络").show();
                        //dismissProgressHUD();
                    }

                    @Override
                    public void onNext(CorporationMsgResponse omBaseResponse) {
                        if (omBaseResponse != null && omBaseResponse.isSuccess()) {
                            if ("1".equals(String.valueOf(omBaseResponse.data.customState)) && id != null) {
                                ARouter.getInstance().build(RouterPathProvider.OnlineMeetingCustomizedEnterprisePageAct)
                                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                                        .withString("companyCode", adLinkId)
                                        .withString("sourceWay","2")
                                        .navigation();
                            } else {
                                Toasty.normal(context, "抱歉，该企业未配置企业主页").show();
                            }
                        } else {
                            Toasty.normal(context, omBaseResponse.msg).show();
                        }

                    }
                });
                break;
            default:
                break;

        }
    }


    /**
     * 跳转到H5页面
     */
    private static void skipToH5(String jumpType, String key, String id, String jumpUrl, String urlTitle) {
        if (StringUtil.isEmpty(jumpUrl)) {
            return;
        }

        if (!jumpUrl.contains("baidu.com")) {
            if (!jumpUrl.contains("userId")) {
                if (jumpUrl.contains("?")) {
                    jumpUrl = jumpUrl + "&userId=" + BaseUserInfoCache.getUserId(TJBaseApp.getInstance()) + "&mobile=" + BaseUserInfoCache.getUserMobile(TJBaseApp.getInstance());
                } else {
                    jumpUrl = jumpUrl + "?userId=" + BaseUserInfoCache.getUserId(TJBaseApp.getInstance()) + "&mobile=" + BaseUserInfoCache.getUserMobile(TJBaseApp.getInstance());
                }
                if (BaseUserInfoCache.isHasCompany(TJBaseApp.getInstance())){
                    jumpUrl = jumpUrl + "&CompanyCode=" + BaseUserInfoCache.getCompanyCode(TJBaseApp.getInstance());
                }
            }
        }

        ARouter.getInstance().build(RouterPathProvider.WEBNew)
                .withString("url", jumpUrl)
                .withString("title", urlTitle)
                .navigation();
    }


    // TODO: 新增context参数 ，认证弹窗需要当前页面的context（2019/4/23）
    public static void doHomeAttractJump(Context context, String jumpType, String key, String id, String jumpUrl, String urlTitle) {

        //0是无跳转
        if ("0".equals(jumpType)) {
            return;
        }

        //外链
        if ("h5".equals(jumpType) || "1".equals(jumpType)) {
            skipToH5(jumpType, key, id, jumpUrl, urlTitle);
            return;
        }
        //根据具体的Key进行跳转
        switch (key) {
            //========= App模块
            case KEY_NET_MEETING_HOT_LIVE:
                // 热门直播间列表
                ARouter.getInstance().build(RouterPathProvider.ENTERPRISE_ROAD_OR_HOT_LIVE_LIST)
                        .withString("pageTitle", "热门直播间")
                        .navigation();
                break;
            case KEY_NET_MEETING_APPLY:
                // 开会申请页面（申请过）跳的是一个页面
                ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_START_LIVE_BROADCAST_APPLY)
                        .withString("source", "menu")
                        .navigation();
                break;
            case KEY_NET_MEETING_LIVE_ROOM:
                jumpLiveRoom(context, id);

                break;
            case KEY_COMPANY_HOME_PAG://公司首页
                OMAppApiProvider.getInstance().mCorporationQuery(id, false, new rx.Observer<CorporationMsgResponse>() {
                    @Override
                    public void onCompleted() {
                        //dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(context, "网络错误，请检查网络").show();
                        //dismissProgressHUD();
                    }

                    @Override
                    public void onNext(CorporationMsgResponse omBaseResponse) {
                        if (omBaseResponse != null && omBaseResponse.isSuccess()) {
                            if ("1".equals(String.valueOf(omBaseResponse.data.customState)) && id != null) {
                                ARouter.getInstance().build(RouterPathProvider.OnlineMeetingCustomizedEnterprisePageAct)
                                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                                        .withString("companyCode", id)
                                        .withString("sourceWay","2")
                                        .navigation();
                            } else {
                                Toasty.normal(context, "抱歉，该企业未配置企业主页").show();
                            }
                        } else {
                            Toasty.normal(context, omBaseResponse.msg).show();
                        }

                    }
                });
                break;
            default:
                break;

        }
    }
}
