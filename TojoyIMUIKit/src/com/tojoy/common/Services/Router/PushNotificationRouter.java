package com.tojoy.common.Services.Router;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.alibaba.android.arouter.launcher.ARouter;
import com.netease.nim.uikit.api.NimUIKit;
import com.netease.nim.uikit.business.chatroom.helper.ChatRoomHelper;
import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.netease.nim.uikit.common.NIMCache;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.tojoy.common.App.TJBaseApp;
import com.tojoy.common.Services.EventBus.PublicStringEvent;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.live.CheckLiveStatusResponse;
import com.tojoy.tjoybaselib.model.user.IMTokenResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.string.StringUtil;

import org.greenrobot.eventbus.EventBus;

import es.dmoral.toasty.Toasty;
import rx.Observer;

import static com.tojoy.common.Services.Router.AppRouter.KEY_NET_MEETING_APPLY;
import static com.tojoy.common.Services.Router.AppRouter.KEY_NET_MEETING_CHECK;
import static com.tojoy.common.Services.Router.AppRouter.KEY_NET_MEETING_HOT_LIVE;
import static com.tojoy.common.Services.Router.AppRouter.KEY_NET_MEETING_LIVE_ROOM;
import static com.tojoy.common.Services.Router.AppRouter.KEY_NET_MEETING_ROOM;

public class PushNotificationRouter {
    /**
     * 个推 - 离线跳转
     *
     * @param context
     * @param model
     * @param detail
     */
    public static void doPushJump(Context context, String model, String detail, String action) {
        if (!BaseUserInfoCache.isLogined(context)) {
            RouterJumpUtil.startLoginActivity();
            return;
        }
        if (AppRouter.PushType_Native.equals(action)) {
            AppRouter.doJump(context, AppRouter.SkipType_Native, model, detail, "", "");
        } else if (AppRouter.SkipType_H5.equals(action)) {

            if (!detail.contains("userId")) {
                if (detail.contains("?")) {
                    detail = detail + "&userId=" + BaseUserInfoCache.getUserId(TJBaseApp.getInstance()) + "&mobile=" + BaseUserInfoCache.getUserMobile(TJBaseApp.getInstance());
                } else {
                    detail = detail + "?userId=" + BaseUserInfoCache.getUserId(TJBaseApp.getInstance()) + "&mobile=" + BaseUserInfoCache.getUserMobile(TJBaseApp.getInstance());
                }
            }
            ARouter.getInstance().build(RouterPathProvider.WEBNew)
                    .withString("url", detail)
                    .withString("title", TextUtils.isEmpty(model) ? "" : model)
                    .navigation();
        }
    }


    /**
     * 个推 - 透传消息处理
     *
     * @param context
     * @param module
     * @param id
     */
    private static Intent mainIntent = null;

    public static void doPushJumpOnline(Context context, String title, String module, String id, String content, String jumpType) throws ClassNotFoundException {
        mainIntent = null;
        if (TextUtils.isEmpty(module)) {
            module = "";
        }

        if (!BaseUserInfoCache.isLogined(context)) {
            RouterJumpUtil.startLoginActivity();
            return;
        }

        if (TextUtils.isEmpty(id)) {
            id = "";
        }

        if (TextUtils.isEmpty(content)) {
            content = "";
        }

        if (AppRouter.SkipType_H5.equals(jumpType)) {
            if (StringUtil.isEmpty(id)) {
                return;
            }

            if (!id.contains("userId")) {
                if (id.contains("?")) {
                    id = id + "&userId=" + BaseUserInfoCache.getUserId(TJBaseApp.getInstance()) + "&mobile=" + BaseUserInfoCache.getUserMobile(TJBaseApp.getInstance());
                } else {
                    id = id + "?userId=" + BaseUserInfoCache.getUserId(TJBaseApp.getInstance()) + "&mobile=" + BaseUserInfoCache.getUserMobile(TJBaseApp.getInstance());
                }
            }

            mainIntent = new Intent(context, Class.forName("com.tojoy.common.Services.Web.NewOutLinkActivity"));
            mainIntent.putExtra("url", id);
            mainIntent.putExtra("title", "");

        } else if(AppRouter.PushType_Native.equals(jumpType)) {
            switch (module) {
                // =======================网上天洽会
                case KEY_NET_MEETING_HOT_LIVE:
                    // 热门直播间
                    mainIntent = new Intent(context, Class.forName("com.tojoy.cloud.online_meeting.App.Home.activity.RoadOrHotLiveListAct"));
                    mainIntent.putExtra("pageTitle", "热门直播间");
                    break;
                case KEY_NET_MEETING_APPLY:
                    // 开会申请
                    mainIntent = new Intent(context, Class.forName("com.tojoy.cloud.online_meeting.App.Apply.activity.ApplyAct"));
                    mainIntent.putExtra("source", "menu");
                    break;
                case KEY_NET_MEETING_CHECK:
                    // 审批列表
                    mainIntent = new Intent(context, Class.forName("com.tojoy.cloud.online_meeting.App.Approval.activity.ApprovalListAct"));
                    break;
                case KEY_NET_MEETING_ROOM:
                    // 直播间个人主页
                    mainIntent = context.getPackageManager().getLaunchIntentForPackage("com.tojoy.onlinemeeting");//获取启动Activity
                    mainIntent.putExtra("isFinish",true);
                    break;

                case KEY_NET_MEETING_LIVE_ROOM:
                    // 直播间
                    if (context instanceof UI) {
                        ((UI) context).showProgressHUD(context, "加载中");
                    }

                    EventBus.getDefault().post(new PublicStringEvent(PublicStringEvent.INVITE_JOIN_MEETING));

                    if (AppConfig.isLoginIM) {
                        join(context, id, content, title);
                    } else {
                        loginIM(context, id, content, title);
                    }
                    break;
                default:
                    break;
            }
        } else {
            //获取启动Activity
            mainIntent = context.getPackageManager().getLaunchIntentForPackage("com.tojoy.onlinemeeting");
            mainIntent.putExtra("isFinish",true);
        }

        if (!module.equals(KEY_NET_MEETING_LIVE_ROOM)) {
            if (mainIntent == null) {
                mainIntent = new Intent(context, Class.forName("com.tojoy.onlinemeeting.App.Home.Controller.HomeTabAct"));
            }

            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            mainIntent.setAction(Intent.ACTION_VIEW);
            mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(mainIntent);
    }

    private static void loginIM(Context mContext, String id, String content, String title) {

        OMAppApiProvider.getInstance().getIMToken(BaseUserInfoCache.getUserId(mContext), "0", new Observer<IMTokenResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                statitiscIMLogin("0");
                if (mContext instanceof UI) {
                    ((UI) mContext).dismissProgressHUD();
                }
                if (AppConfig.isCanToast()) {
                    Toasty.normal(mContext, "getIMToken error = " + e.getMessage()).show();
                } else {
                    Toasty.normal(mContext, "网络错误，请稍后再试").show();
                }
            }

            @Override
            public void onNext(IMTokenResponse imToken) {
                if (imToken.isSuccess()) {
                    BaseUserInfoCache.setIMAccountHeaderBase(mContext, imToken.data.env);
                    NimUIKit.setIMHeaderStr(imToken.data.env);
                    BaseUserInfoCache.saveIMTokenBase(mContext, imToken.data.token);

                    String account = BaseUserInfoCache.getIMHeaderedAccount(mContext);
                    NimUIKit.login(new LoginInfo(account, imToken.data.token), new RequestCallback<LoginInfo>() {
                        @Override
                        public void onSuccess(LoginInfo param) {
                            NIMCache.setAccount(account);
                            join(mContext, id, content, title);
                            statitiscIMLogin("1");
                        }

                        @Override
                        public void onFailed(int code) {
                            statitiscIMLogin("0");
                            if (mContext instanceof UI) {
                                ((UI) mContext).dismissProgressHUD();
                            }
                            if (AppConfig.isCanToast()) {
                                Toasty.normal(mContext, "登录IM 失败 code = " + code).show();
                            } else {
                                Toasty.normal(mContext, "网络错误，请稍后再试").show();
                            }
                        }

                        @Override
                        public void onException(Throwable exception) {
                            statitiscIMLogin("0");
                            if (mContext instanceof UI) {
                                ((UI) mContext).dismissProgressHUD();
                            }
                            if (AppConfig.isCanToast()) {
                                Toasty.normal(mContext, "登录IM 异常 e = " + exception.getMessage()).show();
                            } else {
                                Toasty.normal(mContext, "网络错误，请稍后再试").show();
                            }
                        }
                    });

                } else {
                    statitiscIMLogin("0");
                    if (mContext instanceof UI) {
                        ((UI) mContext).dismissProgressHUD();
                    }
                    if (AppConfig.isCanToast()) {
                        Toasty.normal(mContext, "getIMToken 异常 = " + imToken.msg).show();
                    } else {
                        Toasty.normal(mContext, "网络错误，请稍后再试").show();
                    }
                }

            }
        });
    }

    private static void statitiscIMLogin(String loginIMState) {
        OMAppApiProvider.getInstance().statitiscIMLogin(loginIMState, "enterLivePage", new rx.Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(OMBaseResponse omBaseResponse) {
            }
        });
    }

    private static void join(Context context, String id, String content, String title) {
        OMAppApiProvider.getInstance().getLiveRoomStatus(id, new Observer<CheckLiveStatusResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(CheckLiveStatusResponse response) {
                if (response.isSuccess()) {


                    if (LiveRoomInfoProvider.getInstance().isHost() && ChatRoomHelper.isFromLiveRoom) { //如果自己正在开播

                        Class<?> aClass = null;
                        try {
                            aClass = Class.forName("com.tojoy.cloud.online_meeting.App.Live.Live.Activity.LiveAct");
                        } catch (ClassNotFoundException e) {

                        }
                        pushLive(context, aClass, id, response.data.title, id, title, content);

                    } else {


                        if ("2".equals(response.data.status) || "3".equals(response.data.status) || "4".equals(response.data.status)) {
                            if (!TextUtils.isEmpty(response.data.havePassword) && response.data.havePassword.equals("1")) {
                                LiveRoomIOHelper.checkEnterLiveRoom(context, id, new IOLiveRoomListener() {
                                    @Override
                                    public void onIOError(String error, int errorCode) {
                                        if (errorCode == 301) {
                                            Class<?> aClass = null;
                                            try {
                                                aClass = Class.forName("com.tojoy.cloud.online_meeting.App.Apply.activity.EnterRoomPswAct");
                                                pushLive(context, aClass, id, response.data.title, id, title, content);
                                            } catch (ClassNotFoundException e) {
                                            }

                                        }
                                    }

                                    @Override
                                    public void onIOSuccess() {
                                        try {
                                            if (BaseUserInfoCache.getUserId(context).equals(response.data.userId)) {
                                                Class<?> aClass = Class.forName("com.tojoy.cloud.online_meeting.App.Live.Live.Activity.LiveAct");
                                                pushLive(context, aClass, id, response.data.title, id, title, content);
                                            } else {
                                                Class<?> aClass = Class.forName("com.tojoy.cloud.online_meeting.App.Live.Live.Activity.SeeLiveAct");
                                                pushLive(context, aClass, id, response.data.title, id, title, content);
                                            }
                                        } catch (ClassNotFoundException e) {
                                        }
                                    }
                                });
                            } else {

                                try {
                                    if (BaseUserInfoCache.getUserId(context).equals(response.data.userId)) {
                                        Class<?> aClass = Class.forName("com.tojoy.cloud.online_meeting.App.Live.Live.Activity.LiveAct");
                                        pushLive(context, aClass, id, response.data.title, id, title, content);
                                    } else {
                                        Class<?> aClass = Class.forName("com.tojoy.cloud.online_meeting.App.Live.Live.Activity.SeeLiveAct");
                                        pushLive(context, aClass, id, response.data.title, id, title, content);
                                    }
                                } catch (ClassNotFoundException e) {
                                }
                            }
                        } else if ("1".equals(response.data.status)) {
                            try {
                                if (BaseUserInfoCache.getUserId(context).equals(response.data.userId)) {
                                    Class<?> aClass = Class.forName("com.tojoy.cloud.online_meeting.App.Live.Live.Activity.LiveAct");
                                    pushLive(context, aClass, id, response.data.title, id, title, content);
                                } else {
                                    Class<?> aClass = Class.forName("com.tojoy.cloud.online_meeting.App.Live.Live.Activity.SeeLiveAct");
                                    pushLive(context, aClass, id, response.data.title, id, title, content);
                                }
                            } catch (ClassNotFoundException e) {
                            }
                        } else {
                            try {
                                if (BaseUserInfoCache.getUserId(context).equals(response.data.userId)) {
                                    Class<?> aClass = Class.forName("com.tojoy.cloud.online_meeting.App.Live.Live.Activity.LiveAct");
                                    pushLive(context, aClass, id, response.data.title, id, title, content);
                                } else {
                                    Class<?> aClass = Class.forName("com.tojoy.cloud.online_meeting.App.Live.Live.Activity.SeeLiveAct");
                                    pushLive(context, aClass, id, response.data.title, id, title, content);
                                }
                            } catch (ClassNotFoundException e) {
                            }
                        }

                    }


                } else {
                    if (response.data != null && !TextUtils.isEmpty(response.data.havePassword) && response.data.havePassword.equals("1")) {
                        Class<?> aClass = null;
                        try {
                            aClass = Class.forName("com.tojoy.cloud.online_meeting.App.Apply.activity.EnterRoomPswAct");
                            pushLive(context, aClass, id, response.data.title, id, title, content);
                        } catch (ClassNotFoundException e) {
                        }
                    }
                }
            }
        });
    }

    private static void pushLive(Context context, Class aClass, String liveJson, String roomTitle, String roomLiveId, String title, String content) {

        Intent intent = new Intent(context, aClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setAction(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("liveJson", liveJson);
        intent.putExtra("roomName", roomTitle);
        intent.putExtra("liveId", roomLiveId);
        context.startActivity(intent);
    }
}
