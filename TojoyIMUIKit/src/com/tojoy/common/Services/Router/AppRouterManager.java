package com.tojoy.common.Services.Router;

import android.content.Context;
import android.text.TextUtils;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.common.App.TJBaseApp;
import com.tojoy.common.Services.EventBus.PublicStringEvent;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;

import org.greenrobot.eventbus.EventBus;

/**
 * 页面跳转封装
 *
 * @author qll
 */
public class AppRouterManager {
    /**
     * 跳转类型
     * JUMP_H5：H5页面
     * JUMP_NATIVE：原生页面
     */
    public static final String JUMP_H5 = "h5";
    public static final String JUMP_NATIVE = "native";

    /**
     * 协商后的统一的页面key
     */
    public static final String KEY_MODULE_MINE = "mine";
    // 热门直播间列表
    public static final String KEY_MODULE_NET_MEETING_HOT_LIVE = "netMeetingHotLive";
    // 直接往直播间里跳
    public static final String KEY_MODULE_NET_MEETING_LIVE_ROOM = "netMeetingLiveRoom";
    // 开会申请页面（申请过）跳的是一个页面
    public static final String KEY_MODULE_NET_MEETING_APPLY = "netMeetingApply";
    // 审批列表
    public static final String KEY_MODULE_NET_MEETING_CHECK = "netMeetingCheck";
    //公司首页
    public static final String KEY_MODULE_COMPANY_HOME_PAG = "company_home_page";
    // 企业入驻页面
    public static final String KEY_MODULE_APPLY_ENTERPRISE = "applyEnterprise";





    /**
     * 页面跳转
     */
    public void jumpPage(Context mContext, String jumpType, String jumpUrl, String webTitle, String moduleName){
        // 0是无跳转
        if ("0".equals(jumpType)) {
            return;
        }
        // 跳转
        if (JUMP_H5.equals(jumpType) || "1".equals(jumpType)) {
            jumpToWeb(jumpUrl, webTitle);
            return;
        }
        // 跳转原生
        if (JUMP_NATIVE.equals(jumpType)) {
            jumpToNative(moduleName);
        }
        // TODO: 2020/8/4 跳转小程序

    }

    private void jumpToWeb(String jumpUrl, String webTitle){
        if (TextUtils.isEmpty(jumpUrl)) {
            return;
        }
        if (!jumpUrl.contains("baidu.com")) {
            if (!jumpUrl.contains("userId")) {
                if (jumpUrl.contains("?")) {
                    jumpUrl = jumpUrl + "&userId=" + BaseUserInfoCache.getUserId(TJBaseApp.getInstance()) + "&mobile=" + BaseUserInfoCache.getUserMobile(TJBaseApp.getInstance());
                } else {
                    jumpUrl = jumpUrl + "?userId=" + BaseUserInfoCache.getUserId(TJBaseApp.getInstance()) + "&mobile=" + BaseUserInfoCache.getUserMobile(TJBaseApp.getInstance());
                }
            }
        }

        ARouter.getInstance().build(RouterPathProvider.WEBNew)
                .withString("url", jumpUrl)
                .withString("title", webTitle)
                .navigation();
    }

    private void jumpToNative(String moduleName){
        if (TextUtils.isEmpty(moduleName)) {
            return;
        }
        String routerPath = getModuleRouterPath(moduleName);
        if (KEY_MODULE_MINE.equals(moduleName)) {
            // 切换到：首页--我的
            EventBus.getDefault().postSticky(new PublicStringEvent(PublicStringEvent.JUMP_OWN));
        } else {
            // 无特殊传参直接走这个
//            Postcard postcard = ARouter.getInstance()
//                    .build(routerPath);
//
//            postcard.navigation();
            ARouter.getInstance().build(routerPath)
                    .navigation();
        }
    }

    /**
     *  根据module名称获取要跳转的路由路径
     */
    private String getModuleRouterPath(String moduleName){
        String routerPath = "";
        switch (moduleName) {
            case KEY_MODULE_NET_MEETING_HOT_LIVE :
                // 热门直播间列表
                routerPath = RouterPathProvider.ENTERPRISE_ROAD_OR_HOT_LIVE_LIST;
                break;
            case KEY_MODULE_NET_MEETING_APPLY:
                // 开播申请
                routerPath = RouterPathProvider.ONLINE_MEETING_START_LIVE_BROADCAST_APPLY;
                break;
            case KEY_MODULE_NET_MEETING_CHECK:
                // 审批列表页面
                routerPath = RouterPathProvider.ONLINE_MEETING_MY_APPROVAL_LIST;
                break;
            case KEY_MODULE_COMPANY_HOME_PAG:
                //公司首页
                routerPath = RouterPathProvider.OnlineMeetingCustomizedEnterprisePageAct;
                break;
            case KEY_MODULE_APPLY_ENTERPRISE:
                // 企业入驻
                routerPath = RouterPathProvider.ApplyEnterprise;
                break;
            case KEY_MODULE_NET_MEETING_LIVE_ROOM:
                // 直播间
                // TODO: 2020/8/4 根据主播/观众区分跳转
                break;
            default:
                break;
        }
        return routerPath;
    }

}
