package com.tojoy.common.LiveRoom;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.R;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.tojoy.tjoybaselib.model.live.LiveShareInfoModel;
import com.tojoy.tjoybaselib.ui.dialog.PickerViewAnimateUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.common.Services.PayAndShare.WXShareUtil;

public class LiveSharePopview {
    public final static String STYLE_WHITE = "white";
    public final static String STYLE_BLACK = "black";

    private final String SHARE_TOWX = "SHARE_TOWX";
    private final String SHARE_TOWXFRIENDS = "SHARE_TOWXFRIENDS";

    private Context context;
    protected ViewGroup contentContainer;
    private Animation outAnim;
    private Animation inAnim;
    private boolean isAnim = true;
    private boolean isShowing;
    private int gravity = Gravity.BOTTOM;
    public ViewGroup decorView;
    public ViewGroup rootView;
    private boolean dismissing;

    private OperationCallback mOperationCallback;

    private String styleType;


    public LiveSharePopview(Context context,String type) {
        this.context = context;
        this.styleType = type;
        init();
        initViews();
        setOutSideCancelable(true);
    }

    public LiveSharePopview(Context context, OperationCallback operationCallback,String type) {
        this.context = context;
        this.mOperationCallback = operationCallback;
        this.styleType = type;
        init();
        initViews();
        setOutSideCancelable(true);
    }

    protected void init() {
        inAnim = getInAnimation();
        outAnim = getOutAnimation();
    }

    private void initViews() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        //decorView是activity的根View
        if (decorView == null) {
            decorView = (ViewGroup) ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        }

        //将控件添加到decorView中
        rootView = (ViewGroup) layoutInflater.inflate(R.layout.live_share_popview, decorView, false);
        rootView.setLayoutParams(new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        ));
        rootView.findViewById(R.id.iv_close).setOnClickListener(v -> {
            dismiss();
        });
        rootView.findViewById(R.id.tv_cancel_share).setOnClickListener(v -> {
            dismiss();
        });

        if (STYLE_BLACK.equals(styleType)){
            int height = AppUtils.dip2px(context, 45) + AppUtils.dip2px(context, 140);
            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height);
            rootView.findViewById(R.id.container_base).setLayoutParams(lps);
            rootView.findViewById(R.id.container_base).setBackgroundResource(R.drawable.live_pop_bg);
//            rootView.findViewById(R.id.container_base).setBackground(context.getResources().getDrawable(R.drawable.live_pop_bg));
            ((TextView)rootView.findViewById(R.id.tv_share_wx)).setTextColor(context.getResources().getColor(R.color.color_ffffff));
            ((TextView)rootView.findViewById(R.id.tv_share_wxfriends)).setTextColor(context.getResources().getColor(R.color.color_ffffff));
            rootView.findViewById(R.id.rlv_top_layout).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.share_line).setVisibility(View.GONE);
            rootView.findViewById(R.id.tv_cancel_share).setVisibility(View.GONE);
            rootView.setBackgroundColor(context.getResources().getColor(R.color.colorTranslucent));
        } else if (STYLE_WHITE.equals(styleType)){
            int height = AppUtils.dip2px(context, 55) + AppUtils.dip2px(context, 140);
            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height);
            rootView.findViewById(R.id.container_base).setLayoutParams(lps);
            rootView.findViewById(R.id.container_base).setBackgroundColor(context.getResources().getColor(R.color.white));
            ((TextView)rootView.findViewById(R.id.tv_share_wx)).setTextColor(context.getResources().getColor(R.color.color_808080));
            ((TextView)rootView.findViewById(R.id.tv_share_wxfriends)).setTextColor(context.getResources().getColor(R.color.color_808080));
            rootView.findViewById(R.id.rlv_top_layout).setVisibility(View.GONE);
            rootView.findViewById(R.id.share_line).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.tv_cancel_share).setVisibility(View.VISIBLE);
            rootView.setBackgroundColor(context.getResources().getColor(R.color.c66000000));
        }

        setKeyBackCancelable(true);

        contentContainer = rootView.findViewById(R.id.content_container);

        contentContainer.findViewById(R.id.rlv_yuejian).setOnClickListener(view -> {
            initLiveRoomShare(context, SHARE_TOWX);
            dismiss();
        });
        contentContainer.findViewById(R.id.llv_share_friends).setOnClickListener(view -> {
            initLiveRoomShare(context, SHARE_TOWXFRIENDS);
            dismiss();
        });
    }

    /**
     * @param v      (是通过哪个View弹出的)
     * @param isAnim 是否显示动画效果
     */
    public void show(View v, boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(View v) {
        show();
    }

    public void show() {
        if (isShowing()) {
            return;
        }
        isShowing = true;
        onAttached(rootView);
        rootView.requestFocus();
    }

    private void onAttached(View view) {
        decorView.addView(view);
        if (isAnim) {
            contentContainer.startAnimation(inAnim);
        }
    }

    public boolean isShowing() {
        return rootView.getParent() != null || isShowing;
    }


    public void dismiss() {

        if (dismissing) {
            dismissImmediately();
            contentContainer.clearAnimation();
            return;
        }
        if (isAnim) {
            //消失动画
            outAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    dismissImmediately();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            contentContainer.startAnimation(outAnim);
        } else {
            dismissImmediately();
        }
        dismissing = true;

        if (mOperationCallback != null) {
            new Handler().postDelayed(() -> mOperationCallback.showBottomView(true), 400);
        }
    }

    public void dismissImmediately() {
        decorView.post(() -> {
            //从根视图移除
            decorView.removeView(rootView);
            isShowing = false;
            dismissing = false;
        });

    }

    public Animation getInAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, true);
        return AnimationUtils.loadAnimation(context, res);
    }

    public Animation getOutAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, false);
        return AnimationUtils.loadAnimation(context, res);
    }

    public void setKeyBackCancelable(boolean isCancelable) {
        ViewGroup View;
        View = rootView;
        View.setFocusable(isCancelable);
        View.setFocusableInTouchMode(isCancelable);
        if (isCancelable) {
            View.setOnKeyListener(onKeyBackListener);
        } else {
            View.setOnKeyListener(null);
        }
    }

    private View.OnKeyListener onKeyBackListener = (v, keyCode, event) -> {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == MotionEvent.ACTION_DOWN
                && isShowing()) {
            dismiss();
            if (mOperationCallback != null) {
                mOperationCallback.showBottomView(true);
            }
            return true;
        }
        return false;
    };

    /**
     * Called when the user touch on black overlay in order to dismiss the dialog
     */
    @SuppressLint("ClickableViewAccessibility")
    private final View.OnTouchListener onCancelableTouchListener = (v, event) -> {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            dismiss();
        }
        return false;
    };

    private void setOutSideCancelable(boolean isCancelable) {
        if (rootView != null) {
            View view = rootView.findViewById(com.netease.nim.uikit.R.id.outmost_container);

            if (isCancelable) {
                view.setOnTouchListener(onCancelableTouchListener);
            } else {
                view.setOnTouchListener(null);
            }
        }
    }


    private String mLiveCover, mLiveTarget, mLiveTitle, mLiveDesc;
    private LiveShareInfoModel.DataObjBean mShareInfo;

    public void setLiveShareData(String liveCover, String liveTarget, String liveTitle, String liveDesc,LiveShareInfoModel.DataObjBean liveInfoBean) {
        this.mLiveCover = liveCover;
        this.mLiveTarget = liveTarget;
        this.mLiveTitle = liveTitle;
        this.mLiveDesc = liveDesc;
        this.mShareInfo = liveInfoBean;
    }

    //直播分享
    private void initLiveRoomShare(Context context, String toWhere) {
        if (SHARE_TOWX.equals(toWhere)) {
            // 微信好友
            if (mShareInfo != null && mShareInfo.weChaAppShareLinkDto != null ) {
                // 小程序
                WXShareUtil.shareToWeixinMinProgram(context, mLiveCover, mLiveTarget, mLiveTitle, mLiveDesc,
                        mShareInfo.weChaAppShareLinkDto.userName,
                        mShareInfo.weChaAppShareLinkDto.path);
            } else {
                // 链接
                WXShareUtil.shareToWeixin(context, mLiveCover, mLiveTarget, mLiveTitle, mLiveDesc,true);
            }

        } else if (SHARE_TOWXFRIENDS.equals(toWhere)) {
            // 微信朋友圈
            WXShareUtil.shareToWeixin(context, mLiveCover, mLiveTarget, mLiveTitle, mLiveDesc,false);
        }
    }

}
