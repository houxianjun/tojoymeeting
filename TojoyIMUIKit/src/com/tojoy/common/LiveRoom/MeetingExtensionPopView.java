package com.tojoy.common.LiveRoom;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.netease.nim.uikit.R;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.dialog.PickerViewAnimateUtil;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.string.StringUtil;
import com.tojoy.tjoybaselib.util.sys.MathematicalUtils;

import es.dmoral.toasty.Toasty;

public class MeetingExtensionPopView {

    private Context context;
    private ViewGroup contentContainer;
    private Animation outAnim;
    private Animation inAnim;
    private boolean isAnim = true;
    private boolean isShowing;
    private int gravity = Gravity.BOTTOM;
    private ViewGroup decorView;
    public ViewGroup rootView;
    private boolean dismissing;

    private TextView mExtensionMoneyTv;
    private TextView mExtensionPopHintTv;

    public MeetingExtensionPopView(Context context) {
        this.context = context;
        init();
        initViews();
        setOutSideCancelable(true);
    }

    protected void init() {
        inAnim = getInAnimation();
        outAnim = getOutAnimation();
    }

    private void initViews() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        if (decorView == null) {
            decorView = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        }

        //将控件添加到decorView中
        rootView = (ViewGroup) layoutInflater.inflate(R.layout.dialog_meeting_extension, decorView, false);
        rootView.setLayoutParams(new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        ));

        setKeyBackCancelable(true);
        contentContainer = rootView.findViewById(R.id.content_container);
        mExtensionMoneyTv = rootView.findViewById(R.id.tv_extension_money);
        mExtensionPopHintTv = rootView.findViewById(R.id.tv_extension_pop_hint);

        rootView.findViewById(R.id.iv_close).setOnClickListener(view -> dismiss());
        rootView.findViewById(R.id.rlv_poster).setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                // 判断二维码内容是否为空，为空生成海报没有意义
                if (!TextUtils.isEmpty(BaseUserInfoCache.getUserInviteUrl(context))) {
                    ARouter.getInstance()
                            .build(RouterPathProvider.ONLINE_MEETING_EXTEN_SION_POSTER)
                            .withInt("fromType", type)
                            .withString("mLiveTitle", mLiveTitle)
                            .withString("mLiveId", mLiveId)
                            .withString("mLiveCoverUrl", mLiveCoverUrl)
                            .withString("mRoomId", mRoomId)
                            .withString("goodsId",goodsId)
                            .withString("goodsTitle",goodsTitle)
                            .withString("goodsPrice",goodsMoney)
                            .navigation();

                } else {
                    Toasty.normal(context, context.getResources().getString(R.string.extension_qr_error)).show();
                }
                dismiss();
            }
        });

        rootView.findViewById(R.id.iv_close).setOnClickListener(v -> dismiss());
    }

    private String mRoomId;
    private String mLiveId;
    private String mLiveTitle;
    private String mLiveCoverUrl;
    private String mMoney;
    private String mMinPerson;
    private String mMinViewTime;

    private String goodsId;
    private String goodsTitle;
    private String goodsMoney;
    /**
     * 类型：
     * 1、会议
     * 2、商品
     */
    private int type;

    public void setMeetingData(String roomId, String liveId, String title, String cover, String money, String minPerson, String minViewTime) {
        this.mRoomId = roomId;
        this.mLiveId = liveId;
        this.mMoney = money;
        this.mLiveTitle = title;
        this.mLiveCoverUrl = cover;
        this.mMinPerson = minPerson;
        this.mMinViewTime = minViewTime;
        type = 1;
        setData();
    }

    public void setGoodsData(String goodsId, String title, String cover, String money, String minPerson, String minViewTime,String goodsMoney) {
        this.goodsId = goodsId;
        this.goodsMoney = goodsMoney;
        this.mMoney = money;
        this.goodsTitle = title;
        this.mLiveCoverUrl = cover;
        this.mMinPerson = minPerson;
        this.mMinViewTime = minViewTime;
        type = 2;
        setData();
    }

    private void setData() {
        String msg = String.format(context.getResources().getString(R.string.extension_pop_money), mMoney);
        mExtensionMoneyTv.setText(StringUtil.setDifferentTextColor(msg, Color.parseColor("#F8E71C"), 3, msg.length() - 1));
        if (type == 1) {
            String extensionMeetingMinPerson = String.format(context.getResources().getString(R.string.distribution_pop_meeting_minperson),mMinPerson);
            String extensionMeetingMinViewTime = String.format(context.getResources().getString(R.string.distribution_pop_meeting_minTime),mMinViewTime);
            String extensionPopHint = context.getResources().getString(R.string.extension_pop_meeting_money_des);

            String str = extensionMeetingMinPerson + " " + extensionMeetingMinViewTime + extensionPopHint;
            SpannableString spanStr = new SpannableString(str);
            spanStr.setSpan(new RelativeSizeSpan((float) MathematicalUtils.div(20, 15, 2))
                    , 3, str.indexOf("人"), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            spanStr.setSpan(new RelativeSizeSpan((float) MathematicalUtils.div(20, 15, 2))
                    , str.indexOf("于") + 1, str.indexOf("分"), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            mExtensionPopHintTv.setTextColor(Color.parseColor("#FFF9F6"));
            mExtensionPopHintTv.setText(spanStr);
        } else {
            mExtensionPopHintTv.setTextColor(Color.parseColor("#ffffff"));
            mExtensionPopHintTv.setText(context.getResources().getString(R.string.extension_pop_money_des));
        }
    }

    public void show() {
        if (isShowing()) {
            return;
        }
        isShowing = true;
        onAttached(rootView);
        rootView.requestFocus();
    }

    private void onAttached(View view) {
        decorView.addView(view);
        if (isAnim) {
            contentContainer.startAnimation(inAnim);
        }
    }

    public boolean isShowing() {
        return rootView.getParent() != null || isShowing;
    }

    public void dismiss() {

        if (dismissing) {
            dismissImmediately();
            contentContainer.clearAnimation();
            return;
        }
        if (isAnim) {
            //消失动画
            outAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    dismissImmediately();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            contentContainer.startAnimation(outAnim);
        } else {
            dismissImmediately();
        }
        dismissing = true;

    }

    private void dismissImmediately() {
        decorView.post(() -> {
            //从根视图移除
            decorView.removeView(rootView);
            isShowing = false;
            dismissing = false;
        });

    }

    private Animation getInAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, true);
        return AnimationUtils.loadAnimation(context, res);
    }

    private Animation getOutAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, false);
        return AnimationUtils.loadAnimation(context, res);
    }

    private void setKeyBackCancelable(boolean isCancelable) {
        ViewGroup view;
        view = rootView;
        view.setFocusable(isCancelable);
        view.setFocusableInTouchMode(isCancelable);
        if (isCancelable) {
            view.setOnKeyListener(onKeyBackListener);
        } else {
            view.setOnKeyListener(null);
        }
    }

    private View.OnKeyListener onKeyBackListener = (v, keyCode, event) -> {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == MotionEvent.ACTION_DOWN
                && isShowing()) {
            dismiss();
            return true;
        }
        return false;
    };

    /**
     * Called when the user touch on black overlay in order to dismiss the dialog
     */
    @SuppressLint("ClickableViewAccessibility")
    private final View.OnTouchListener onCancelableTouchListener = (v, event) -> {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            dismiss();
        }
        return false;
    };

    private void setOutSideCancelable(boolean isCancelable) {
        if (rootView != null) {
            View view = rootView.findViewById(R.id.outmost_container);

            if (isCancelable) {
                view.setOnTouchListener(onCancelableTouchListener);
            } else {
                view.setOnTouchListener(null);
            }
        }
    }

}
