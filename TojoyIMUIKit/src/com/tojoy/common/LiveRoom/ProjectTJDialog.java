package com.tojoy.common.LiveRoom;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.R;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishProjectAttachment;
import com.netease.nim.uikit.business.liveroom.LiveRoomHelper;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigCacheManager;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigConstantKey;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.net.NetworkUtil;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * Created by luuzhu on 2019/4/24.
 * 项目推荐list
 */

public class ProjectTJDialog {

    private Dialog mDialog;
    private Activity mContext;
    private View mView;
    private ArrayList<CRMsgLivePublishProjectAttachment> mDatas;

    public ProjectListAdapter adapter;
    public String sourceWay;
    public ProjectTJDialog(Context context) {
        mContext = (Activity) context;
        this.sourceWay="0";
        initUI();
    }


    public ProjectTJDialog(Context context, ArrayList<CRMsgLivePublishProjectAttachment> mProjects) {
        mContext = (Activity) context;
        this.mDatas = mProjects;
        this.sourceWay="1";
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_live_project_list, null);
        initView();
    }

    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_live_project_list, null);
        this.mDatas = LiveRoomHelper.getInstance().receivedProjects;
        initView();
    }

    public void show() {
        mDialog = new Dialog(mContext, R.style.LiveDialogFragmentEnterExitAnim);
        mDialog.setContentView(mView);

        Window win = mDialog.getWindow();
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.windowAnimations = R.style.BottomInAndOutStyle;
        lp.gravity = Gravity.BOTTOM;
        win.setAttributes(lp);

        mDialog.setCanceledOnTouchOutside(true);
        try {
            mDialog.show();
            mDialog.setOnKeyListener((dialog, keyCode, event) -> true);
        } catch (Exception e) {

        }
    }

    private void initView() {
        ((TextView) mView.findViewById(R.id.tv_title)).setText("猜您喜欢");
        RecyclerView mRecyclerView = mView.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        adapter = new ProjectListAdapter();
        mRecyclerView.setAdapter(adapter);

        View tvEmpty = mView.findViewById(R.id.tvEmpty);
        if (mDatas == null || mDatas.isEmpty()) {
            mRecyclerView.setVisibility(View.GONE);
            tvEmpty.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            tvEmpty.setVisibility(View.GONE);
        }

        mView.findViewById(R.id.iv_close).setOnClickListener(view -> mDialog.dismiss());
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener listener) {
        mDialog.setOnDismissListener(listener);
    }

    public boolean isShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }


    public class ProjectListAdapter extends RecyclerView.Adapter {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = null;
            itemView = LayoutInflater.from(mContext).inflate(R.layout.item_project_tj, parent, false);
            return new ProjectVH(itemView);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ProjectVH projectVH = (ProjectVH) holder;
            projectVH.bindData(position);
        }

        @Override
        public int getItemCount() {
            return mDatas == null ? 0 : mDatas.size();
        }
    }

    private class ProjectVH extends RecyclerView.ViewHolder {

        private ImageView ivCover;
        private TextView tvTitle;
        private TextView tvInterested;
        private TextView tvSeize;
        private ImageView ivLike;
        private ImageView ivBuy;

        private int width, height;

        ProjectVH(View itemView) {
            super(itemView);
            ivCover = itemView.findViewById(R.id.ivCover);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvInterested = itemView.findViewById(R.id.tvInterested);
            tvSeize = itemView.findViewById(R.id.tvSeize);
            ivLike = itemView.findViewById(R.id.iv_like);
            ivBuy = itemView.findViewById(R.id.iv_shop);

            width = (AppUtils.getWidth(mContext) - AppUtils.dip2px(mContext, 32) - AppUtils.dip2px(mContext, 2 * 4 + 2 * 8)) / 2;
            height = (int) (width * (90.0f / 160.f));

            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(width, height);
            lps.setMargins(AppUtils.dip2px(mContext, 4), AppUtils.dip2px(mContext, 14), 0, 0);
            ivCover.setLayoutParams(lps);
        }

        void bindData(int position) {
            CRMsgLivePublishProjectAttachment data = mDatas.get(position);
            tvTitle.setText(data.getTitle());
            ImageLoaderManager.INSTANCE.loadImage(mContext, ivCover, OSSConfig.getOSSURLedStr(data.getCoverUrl()), 0, 5);

            if ("1".equals(data.getIsInterest())) {
                tvInterested.setText("已提交");
                ivLike.setBackground(mContext.getResources().getDrawable(R.drawable.icon_like_red));
            } else {
                tvInterested.setText("感兴趣");
                ivLike.setBackground(mContext.getResources().getDrawable(R.drawable.icon_like));
            }

            String type = data.getProductType();

            String wantBuytitle = TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.PRODUCT3);
            String title = TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.PRODUCT2);

            if (TextUtils.isEmpty(data.getStoreId())) {
                tvSeize.setText(TextUtils.isEmpty(title) ? "意向购买" : title);
                ivBuy.setImageResource(R.drawable.icon_buy);
            } else {
                if (data.getIsDeposit() == 1) {
                    // 支付定金
                    tvSeize.setText("预付定金");
                    ivBuy.setImageResource(R.drawable.icon_project_list_deposit);
                } else {
                    // 全款
                    tvSeize.setText(TextUtils.isEmpty(wantBuytitle) ? "立即购买" : wantBuytitle);
                    ivBuy.setImageResource(R.drawable.icon_buy);
                }
            }

            ivCover.setOnClickListener(view -> {
                if (FastClickAvoidUtil.isFastDoubleClick(ivCover.getId())) {
                    return;
                }

                //直播间进入商品详情
                RouterJumpUtil.startGoodsDetailFromLiveRoom(data.getProjectId(), LiveRoomInfoProvider.getInstance().roomId, LiveRoomInfoProvider.getInstance().liveId,sourceWay,data.getAgentCompanyCode(),data.getAgentId(),data.getAgentLive());
            });

            itemView.findViewById(R.id.op_left).setOnClickListener(view -> {

                if (FastClickAvoidUtil.isDoubleClick()) {
                    return;
                }

                if ("已提交".equals(tvInterested.getText().toString())) {
                    return;
                }

                OMAppApiProvider.getInstance().interestedV2(data.getProjectId(),
                        LiveRoomInfoProvider.getInstance().roomId, LiveRoomInfoProvider.getInstance().liveId, data.getAgentCompanyCode(),data.getAgentId(),data.getAgentLive(),new Observer<OMBaseResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                Toasty.normal(mContext, "网络错误，请检查网络").show();
                            }

                            @Override
                            public void onNext(OMBaseResponse response) {
                                if (response.isSuccess()) {
                                    tvInterested.setText("已提交");
                                    ivLike.setBackground(mContext.getResources().getDrawable(R.drawable.icon_like_red));
                                    if (LiveRoomHelper.getInstance().receivedProjects != null && !LiveRoomHelper.getInstance().receivedProjects.isEmpty()) {
                                        if (LiveRoomHelper.getInstance().receivedProjects.get(position) != null) {
                                            LiveRoomHelper.getInstance().receivedProjects.get(position).setIsInterest("1");
                                        }
                                    }
                                } else {
                                    Toasty.normal(mContext, response.msg).show();
                                }
                            }
                        });
            });

            //意向购买
            itemView.findViewById(R.id.op_right).setOnClickListener(view -> {

                if (FastClickAvoidUtil.isFastDoubleClick(itemView.getId())) {
                    return;
                }

                if (!NetworkUtil.isNetAvailable(mContext)) {
                    Toasty.normal(mContext, "网络错误，请检查网络").show();
                    return;
                }

                RouterJumpUtil.startOderSureActivity(data.getProjectId(), LiveRoomInfoProvider.getInstance().liveId,sourceWay, data.getAgentCompanyCode(),data.getAgentId(),data.getAgentLive());
            });
        }
    }
}
