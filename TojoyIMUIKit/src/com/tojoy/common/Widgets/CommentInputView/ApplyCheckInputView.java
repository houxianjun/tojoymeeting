package com.tojoy.common.Widgets.CommentInputView;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Rect;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.netease.nim.uikit.R;
import com.tojoy.tjoybaselib.ui.editView.ClearEmojInputFilter;
import com.tojoy.tjoybaselib.util.sys.KeyboardControlUtil;
import com.tojoy.tjoybaselib.util.sys.WindowParamsUtil;

import es.dmoral.toasty.Toasty;

public class ApplyCheckInputView {

    private View contentView;
    private EditText etContent;
    private Context context;
    private Activity activity;
    private RelativeLayout mSendLayout;

    private boolean mIsFirstShowToast = true;

    /**
     * 虚拟键盘高度
     *
     * @param clickCallback
     */
    private int softkeyHeight;

    private int keyboardheightPortrait;

    //发送点击回调
    public interface SendMsgClickListener {
        void onSendClick(String commentContent);
    }

    private SendMsgClickListener onSendClickListener;

    public void setOnSendClickListener(SendMsgClickListener onSendClickListener) {
        this.onSendClickListener = onSendClickListener;
    }

    //键盘高度获取回调
    public interface KeyboardHeightListener {
        void onHeightReceived(int screenOri, int height);
    }

    private KeyboardHeightListener onHeightReceivedListener;

    public void setOnHeightReceivedListener(KeyboardHeightListener onHeightReceivedListener) {
        this.onHeightReceivedListener = onHeightReceivedListener;
    }

    public ApplyCheckInputView(Context context, int protraitHeight) {
        this.context = context;
        keyboardheightPortrait = protraitHeight;
        softkeyHeight = WindowParamsUtil.getNavigationBarHeight(context);
        initView();
    }

    public void initView() {
        contentView = View.inflate(context, R.layout.layout_apply_check_input, null);
        etContent = contentView.findViewById(R.id.et_content);
        mSendLayout = contentView.findViewById(R.id.rlv_send_layout);
        mSendLayout.setOnClickListener(view -> {
            if (TextUtils.isEmpty(etContent.getText().toString())) {
                return;
            }
            onSendClickListener.onSendClick(etContent.getText().toString().trim());
            etContent.setText("");
            dismiss();
        });
        contentView.setVisibility(View.GONE);
        contentView.setOnClickListener(view -> {
        });
        etContent.setOnClickListener(v -> show(keyboardheightPortrait));
        etContent.setOnEditorActionListener((arg0, arg1, arg2) -> {
            if (arg1 == EditorInfo.IME_ACTION_SEND || (arg2 != null && arg2.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                mSendLayout.performClick();
                return true;
            }
            return false;
        });

        InputFilter[] emojiFilters = {new ClearEmojInputFilter()};
        etContent.setFilters(emojiFilters);

        etContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.toString().trim().length() > 0) {
                    mSendLayout.setBackgroundResource(R.drawable.bg_seize_btn);
                    mSendLayout.setEnabled(true);
                } else {
                    mSendLayout.setBackgroundResource(R.drawable.bg_send_normal_layout);
                    mSendLayout.setEnabled(false);
                }

                int length = charSequence.length();
                if (length > 200) {
                    etContent.setText(etContent.getText().subSequence(0, 200));
                    etContent.setSelection(200);
                    if (mIsFirstShowToast) {
                        Toasty.normal(context, "已达最大字数限制").show();
                        mIsFirstShowToast = false;
                    }
                } else if (length < 200) {
                    mIsFirstShowToast = true;
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void add2Window(Activity activity) {
        this.activity = activity;
        FrameLayout layout = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.BOTTOM;
        layout.addView(contentView, params);
        observeSoftKeyboard(activity);
    }

    public void show(int keyboardHeight) {
        keyboardheightPortrait = keyboardHeight;
        etContent.requestFocus();
        showKeyboard();
        etContent.postDelayed(() -> etContent.requestFocus(), 500);
    }

    private void showKeyboard() {
        final FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) contentView.getLayoutParams();
        if (keyboardheightPortrait == 0) {
            params.setMargins(0, 0, 0, 800);
            contentView.setLayoutParams(params);
            KeyboardControlUtil.openKeyboard(etContent, activity);
            new Handler().postDelayed(() -> {
                params.setMargins(0, 0, 0, keyboardheightPortrait);
                contentView.setLayoutParams(params);
                contentView.setVisibility(View.VISIBLE);
            }, 300);
            return;
        }

        params.setMargins(0, 0, 0, keyboardheightPortrait);
        if (contentView.getVisibility() == View.VISIBLE) {
            contentView.setLayoutParams(params);
            contentView.setVisibility(View.VISIBLE);
            KeyboardControlUtil.openKeyboard(etContent, activity);
        } else {
            KeyboardControlUtil.openKeyboard(etContent, activity);
            new Handler().postDelayed(() -> {
                contentView.setLayoutParams(params);
                contentView.setVisibility(View.VISIBLE);
            }, 300);
        }
    }

    public boolean isShow() {
        return contentView.getVisibility() == View.VISIBLE;
    }

    public void dismiss() {
        if (contentView == null || contentView.getVisibility() == View.GONE) {
            return;
        }

        ObjectAnimator animator = ObjectAnimator.ofFloat(contentView, "alpha", 1f, 0);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) contentView.getLayoutParams();
                params.setMargins(0, 0, 0, 0);
                contentView.setLayoutParams(params);
                contentView.setVisibility(View.GONE);
                contentView.setAlpha(1);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animator.setDuration(50);
        animator.start();
        etContent.setText("");
        KeyboardControlUtil.closeKeyboard(etContent, activity);
    }

    private void observeSoftKeyboard(final Activity activity) {
        final View decorView = activity.getWindow().getDecorView();
        decorView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            int previousKeyboardHeight = -1;

            @Override
            public void onGlobalLayout() {
                Rect rect = new Rect();
                decorView.getWindowVisibleDisplayFrame(rect);
                int displayHeight = rect.bottom - rect.top;
                int height = decorView.getHeight();
                int keyboardHeight = height - displayHeight - rect.top - softkeyHeight;
                if (previousKeyboardHeight != keyboardHeight) {
                    boolean hide = (double) displayHeight / height > 0.8;
                    if (hide) {
                        dismiss();
                    } else {
                        final FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) contentView.getLayoutParams();
                        if (activity.getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                            keyboardheightPortrait = keyboardHeight - previousKeyboardHeight;
                            if (onHeightReceivedListener != null)
                                onHeightReceivedListener.onHeightReceived(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT, keyboardheightPortrait);
                            params.setMargins(0, 0, 0, keyboardheightPortrait);
                        }
                        contentView.setLayoutParams(params);
                    }
                }
                previousKeyboardHeight = keyboardHeight;
            }
        });
    }

}