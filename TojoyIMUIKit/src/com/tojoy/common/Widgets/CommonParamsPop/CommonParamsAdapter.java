package com.tojoy.common.Widgets.CommonParamsPop;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.R;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.sys.AppUtils;

import java.util.List;

public class CommonParamsAdapter extends BaseRecyclerViewAdapter<CommonParam> {

    private int mColumnCount;

    CommonParamsAdapter(@NonNull Context context, @NonNull List<CommonParam> datas, int columnCount) {
        super(context, datas);
        this.mColumnCount = columnCount;
    }

    @Override
    protected int getViewType(int position, @NonNull CommonParam data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_common_params_layout;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new ParamViewHolder(context, parent, getLayoutResId(viewType));
    }


    /**
     * 正常条目的item的ViewHolder
     */
    class ParamViewHolder extends BaseRecyclerViewHolder<CommonParam> {

        RelativeLayout mContainer;
        TextView mParamsTitle;

        ParamViewHolder(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            mContainer = (RelativeLayout) findViewById(R.id.rlv_item_container);
            mParamsTitle = (TextView) findViewById(R.id.tv_item_title);
            if (mColumnCount == 1) {
                RelativeLayout.LayoutParams containnerlps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, AppUtils.dip2px(context, 48));
                containnerlps.setMargins(AppUtils.dip2px(context, 4), AppUtils.dip2px(context, 4), AppUtils.dip2px(context, 4), AppUtils.dip2px(context, 4));
                mContainer.setLayoutParams(containnerlps);
            }
        }

        @Override
        public void onBindData(CommonParam data, int position) {
            mParamsTitle.setText(data.name);
            if (data.name.length() < 10) {
                mParamsTitle.setTextSize(14);
            } else {
                mParamsTitle.setTextSize(13);
            }
            if (data.isSelected) {
                mContainer.setBackgroundResource(R.drawable.bg_common_s_bg);
                mParamsTitle.setTextColor(context.getResources().getColor(R.color.white));
            } else {
                mContainer.setBackgroundResource(R.drawable.bg_common_n_bg);
                mParamsTitle.setTextColor(context.getResources().getColor(R.color.color_353f5c));
            }
        }
    }
}