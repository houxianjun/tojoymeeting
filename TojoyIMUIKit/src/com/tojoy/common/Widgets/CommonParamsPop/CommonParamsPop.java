package com.tojoy.common.Widgets.CommonParamsPop;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.R;
import com.tojoy.tjoybaselib.ui.dialog.PickerViewAnimateUtil;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRecyclerViewItemClickListener;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.sys.AppUtils;


import java.util.ArrayList;

/**
 * @author chengyanfang
 * 开播申请、企业人员导入通用弹窗
 */
public class CommonParamsPop {

    private Context context;
    protected ViewGroup contentContainer;
    private Animation outAnim;
    private Animation inAnim;
    private boolean isAnim = true;
    private boolean isShowing;
    private int gravity = Gravity.BOTTOM;
    public ViewGroup decorView;
    private ViewGroup rootView;
    private View clickView;
    private boolean dismissing;

    private int mColumnCount;
    private String mTitle;
    private String mSubTitle;
    private String mFooterTip;
    // 根据不同选项显示不同提示文案
    private String mFooterTipTwo;
    // 根据位置显示特殊文案
    private int mFooterTwoPosion;
    private TextView mFooterTipView;
    private TJRecyclerView mRecyclerView;
    private ArrayList<CommonParam> mParams;
    private CommonParamsAdapter mAdapter;
    private OnRecyclerViewItemClickListener<CommonParam> mOnRecyclerViewItemClickListener;

    public CommonParamsPop(Context context, ArrayList<CommonParam> params, int columnCount, String title, String subTitle, String footerTip, OnRecyclerViewItemClickListener<CommonParam> onRecyclerViewItemClickListener) {
        this.context = context;
        this.mColumnCount = columnCount;
        this.mTitle = title;
        this.mFooterTip = footerTip;
        this.mSubTitle = subTitle;
        this.mParams = params;
        this.mOnRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
        init();
        initViews();
    }


    public CommonParamsPop(Context context, ArrayList<CommonParam> params, int columnCount, String title, String subTitle, String footerTip, String footerTipTwo,int footerTwoPosion, OnRecyclerViewItemClickListener<CommonParam> onRecyclerViewItemClickListener) {
        this.context = context;
        this.mColumnCount = columnCount;
        this.mTitle = title;
        this.mFooterTip = footerTip;
        this.mFooterTipTwo = footerTipTwo;
        this.mFooterTwoPosion = footerTwoPosion;
        this.mSubTitle = subTitle;
        this.mParams = params;
        this.mOnRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
        init();
        initViews();
    }


    private void initViews() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        //decorView是activity的根View
        if (decorView == null) {
            decorView = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        }
        //将控件添加到decorView中
        rootView = (ViewGroup) layoutInflater.inflate(R.layout.view_common_params_layout, decorView, false);
        rootView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        rootView.setBackgroundColor(context.getResources().getColor(R.color.c66000000));
        contentContainer = rootView.findViewById(R.id.content_container);
        setKeyBackCancelable(true);
        setOutSideCancelable(true);

        mRecyclerView = rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setRefreshable(false);
        mRecyclerView.setGridLayoutManager(context, mColumnCount);


        setRecyclerParams();
        mAdapter = new CommonParamsAdapter(context, mParams, mColumnCount);
        mRecyclerView.setAdapter(mAdapter);

        TextView mTitleTv = rootView.findViewById(R.id.tv_title);
        mTitleTv.setText(mTitle);

        mFooterTipView = rootView.findViewById(R.id.footer_tip);
        if (!TextUtils.isEmpty(mFooterTip) && !TextUtils.isEmpty(mFooterTipTwo)) {
            // 需特殊处理
            mFooterTipView.setVisibility(View.VISIBLE);
            int position = -1;
            for (int i = 0; i < mParams.size(); i++) {
                 if (mParams.get(i).isSelected) {
                     position = i;
                     break;
                 }
            }
            if (position == mFooterTwoPosion){
                mFooterTipView.setText(mFooterTipTwo);
            } else {
                mFooterTipView.setText(mFooterTip);
            }
        } else if (!TextUtils.isEmpty(mFooterTip)) {
            mFooterTipView.setVisibility(View.VISIBLE);
            mFooterTipView.setText(mFooterTip);
        }

        if (!TextUtils.isEmpty(mSubTitle)) {
            TextView mSubTitleTv = rootView.findViewById(R.id.tv_subtitle);
            mSubTitleTv.setVisibility(View.VISIBLE);
            mSubTitleTv.setText(mSubTitle);
        }

        mAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
            for (CommonParam commonParam : mParams) {
                commonParam.isSelected = false;
            }
            mParams.get(position).isSelected = true;
            mAdapter.notifyDataSetChanged();
            if (!TextUtils.isEmpty(mFooterTip) && !TextUtils.isEmpty(mFooterTipTwo)) {
                // 需特殊处理
                mFooterTipView.setVisibility(View.VISIBLE);
                if (position == mFooterTwoPosion){
                    mFooterTipView.setText(mFooterTipTwo);
                } else {
                    mFooterTipView.setText(mFooterTip);
                }
            }
            mOnRecyclerViewItemClickListener.onItemClick(v, position, data);
            dismiss();
        });
    }

    protected void init() {
        inAnim = getInAnimation();
        outAnim = getOutAnimation();
    }

    private void setRecyclerParams() {
        int columnLineCnt = mParams.size() / mColumnCount + (mParams.size() % mColumnCount > 0 ? 1 : 0);

        if (columnLineCnt > 4) {
            columnLineCnt = 4;
        }

        int lineHeight = columnLineCnt * AppUtils.dip2px(context, 56) + AppUtils.dip2px(context, 16);
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, lineHeight);
        lps.setMargins(AppUtils.dip2px(context, 10), 0, AppUtils.dip2px(context, 10), 0);
        mRecyclerView.setLayoutParams(lps);

        if (lineHeight < AppUtils.dip2px(context, 168)) {
            RelativeLayout.LayoutParams containnerlps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.dip2px(context, 250));
            containnerlps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            contentContainer.setLayoutParams(containnerlps);
        }
    }

    /**
     * @param v      (是通过哪个View弹出的)
     * @param isAnim 是否显示动画效果
     */
    public void show(View v, boolean isAnim) {
        this.clickView = v;
        this.isAnim = isAnim;
        show();
    }

    public void show(boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(View v) {
        this.clickView = v;
        show();
    }

    /**
     * 添加View到根视图
     */
    public void show() {
        if (isShowing()) {
            return;
        }
        isShowing = true;
        onAttached(rootView);
        rootView.requestFocus();
    }

    /**
     * show的时候调用
     *
     * @param view 这个View
     */
    private void onAttached(View view) {
        decorView.addView(view);
        if (isAnim) {
            contentContainer.startAnimation(inAnim);
        }
    }


    /**
     * 检测该View是不是已经添加到根视图
     *
     * @return 如果视图已经存在该View返回true
     */
    public boolean isShowing() {
        return rootView.getParent() != null || isShowing;
    }


    public void dismiss() {
        if (dismissing) {
            return;
        }

        if (isAnim) {
            //消失动画
            outAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    dismissImmediately();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            contentContainer.startAnimation(outAnim);
        } else {
            dismissImmediately();
        }
        dismissing = true;
    }

    public void dismissImmediately() {

        decorView.post(() -> {
            //从根视图移除
            decorView.removeView(rootView);
            isShowing = false;
            dismissing = false;
        });

    }

    public Animation getInAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, true);
        return AnimationUtils.loadAnimation(context, res);
    }

    public Animation getOutAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, false);
        return AnimationUtils.loadAnimation(context, res);
    }

    public void setKeyBackCancelable(boolean isCancelable) {
        ViewGroup View;
        View = rootView;
        View.setFocusable(isCancelable);
        View.setFocusableInTouchMode(isCancelable);
        if (isCancelable) {
            View.setOnKeyListener(onKeyBackListener);
        } else {
            View.setOnKeyListener(null);
        }
    }

    private View.OnKeyListener onKeyBackListener = (v, keyCode, event) -> {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == MotionEvent.ACTION_DOWN
                && isShowing()) {
            dismiss();
            return true;
        }
        return false;
    };

    public CommonParamsPop setOutSideCancelable(boolean isCancelable) {
        if (rootView != null) {
            View view = rootView.findViewById(R.id.outmost_container);

            if (isCancelable) {
                view.setOnTouchListener(onCancelableTouchListener);
            } else {
                view.setOnTouchListener(null);
            }
        }
        return this;
    }

    /**
     * Called when the user touch on black overlay in order to dismiss the dialog
     */
    @SuppressLint("ClickableViewAccessibility")
    private final View.OnTouchListener onCancelableTouchListener = (v, event) -> {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            dismiss();
        }
        return false;
    };
}
