package com.tojoy.common.Widgets.AddressPicker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.netease.nim.uikit.R;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRecyclerViewItemClickListener;
import com.tojoy.common.App.BaseFragment;
import com.tojoy.tjoybaselib.model.user.CountyModel;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("ValidFragment")
public class CountyFragment extends BaseFragment implements OnRecyclerViewItemClickListener<CountyModel> {

    private RecyclerView mRecyclerView;
    private CoutryAdapter areaListAdapter;
    private List<CountyModel> mOrderList;

    OnSelectAreaCallback mOnSelectAreaCallback;

    public CountyFragment(Context context, List<CountyModel> orderList) {
        this.mOrderList = orderList;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.pay_fragment_address, container, false);
        }
        isCreateView = true;
        return mView;
    }


    @Override
    protected void lazyLoad() {
        super.lazyLoad();
        loadData();
    }

    @Override
    protected void initUI() {
        super.initUI();
    }

    private void loadData() {
        mRecyclerView = mView.findViewById(R.id.container_layout);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        areaListAdapter = new CoutryAdapter(getContext(), mOrderList);
        mRecyclerView.setAdapter(areaListAdapter);
        areaListAdapter.notifyDataSetChanged();

        areaListAdapter.setOnRecyclerViewItemClickListener(this);
    }

    public void setmOnSelectAreaCallback(OnSelectAreaCallback mOnSelectAreaCallback) {
        this.mOnSelectAreaCallback = mOnSelectAreaCallback;
    }

    public void setmOrderList(List<CountyModel> mOrderList) {
        for (CountyModel cityModel : mOrderList) {
            cityModel.isChecked = false;
        }

        this.mOrderList.clear();
        this.mOrderList.addAll(mOrderList);
        areaListAdapter = new CoutryAdapter(getContext(), mOrderList);
        mRecyclerView.setAdapter(areaListAdapter);
        areaListAdapter.setOnRecyclerViewItemClickListener(this);
    }


    @Override
    public void onItemClick(View v, int position, CountyModel data) {
        for (CountyModel provinceModel : mOrderList) {
            provinceModel.isChecked = false;
        }
        mOnSelectAreaCallback.onSelectAt(data.disName, position);
        mOrderList.get(position).isChecked = true;
        areaListAdapter.notifyDataSetChanged();
    }

    public void setSelectIndex(int index) {
        for (CountyModel provinceModel : mOrderList) {
            provinceModel.isChecked = false;
        }
        mOrderList.get(index).isChecked = true;
        areaListAdapter.notifyDataSetChanged();
    }
}
