package com.tojoy.common.Widgets.AddressPicker;

public interface OnSelectAreaCallback {
    void onSelectAt(String name, int pos);
}
