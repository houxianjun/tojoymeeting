package com.tojoy.common.Widgets.AddressPicker;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.netease.nim.uikit.R;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.model.user.ProvinceModel;

import java.util.List;

public class AreaListAdapter extends BaseRecyclerViewAdapter<ProvinceModel> {
    private Context mContext;

    AreaListAdapter(@NonNull Context context, @NonNull List<ProvinceModel> datas) {
        super(context, datas);
        this.mContext = context;
    }

    @Override
    protected int getViewType(int position, @NonNull ProvinceModel data) {
        return position;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_area_pick_item_layout;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new AreaViewHoder(context, getLayoutResId(viewType));
    }

    public class AreaViewHoder extends BaseRecyclerViewHolder<ProvinceModel> {
        TextView tvAreaName;
        ImageView ivChecked;

        @Override
        public void onBindData(ProvinceModel data, int position) {
            if (data.isChecked) {
                ivChecked.setVisibility(View.VISIBLE);
                tvAreaName.setTextColor(mContext.getResources().getColor(R.color.color_3A83F7));
            } else {
                ivChecked.setVisibility(View.GONE);
                tvAreaName.setTextColor(mContext.getResources().getColor(R.color.color_222222));
            }
            tvAreaName.setText(data.getTitle());
        }

        AreaViewHoder(Context context, int layoutResId) {
            super(context, layoutResId);
            tvAreaName = (TextView) findViewById(R.id.tv_area_name);
            ivChecked = (ImageView) findViewById(R.id.iv_checked);
        }
    }
}
