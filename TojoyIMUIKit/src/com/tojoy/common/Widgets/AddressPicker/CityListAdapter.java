package com.tojoy.common.Widgets.AddressPicker;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.netease.nim.uikit.R;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.model.user.CityModel;

import java.util.List;

public class CityListAdapter extends BaseRecyclerViewAdapter<CityModel> {
    private Context mContext;

    CityListAdapter(@NonNull Context context, @NonNull List<CityModel> datas) {
        super(context, datas);
        this.mContext = context;
    }

    @Override
    protected int getViewType(int position, @NonNull CityModel data) {
        return position;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_area_pick_item_layout;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new CityViewHoder(context, getLayoutResId(viewType));
    }

    public class CityViewHoder extends BaseRecyclerViewHolder<CityModel> {
        TextView tvAreaName;
        ImageView ivChecked;

        @Override
        public void onBindData(CityModel data, int position) {
            if (data.isChecked) {
                ivChecked.setVisibility(View.VISIBLE);
                tvAreaName.setTextColor(mContext.getResources().getColor(R.color.color_3A83F7));
            } else {
                ivChecked.setVisibility(View.GONE);
                tvAreaName.setTextColor(mContext.getResources().getColor(R.color.color_222222));
            }
            tvAreaName.setText(data.getTitle());
        }

        CityViewHoder(Context context, int layoutResId) {
            super(context, layoutResId);
            tvAreaName = (TextView) findViewById(R.id.tv_area_name);
            ivChecked = (ImageView) findViewById(R.id.iv_checked);
        }
    }
}
