package com.tojoy.common.Widgets.AddressPicker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.netease.nim.uikit.R;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.common.App.BaseFragment;
import com.tojoy.tjoybaselib.model.user.AddressResponse;
import com.tojoy.tjoybaselib.model.user.ProvinceModel;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("ValidFragment")
public class AddressFragment extends BaseFragment {

    private AreaListAdapter areaListAdapter;
    private List<ProvinceModel> mOrderList = new ArrayList<>();
    AddressResponse addressResponse;

    OnSelectAreaCallback mOnSelectAreaCallback;

    public AddressFragment(Context context) {
        Context mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.pay_fragment_address, container, false);
        }
        isCreateView = true;
        return mView;
    }


    @Override
    protected void lazyLoad() {
        super.lazyLoad();
        loadData();
    }

    @Override
    protected void initUI() {
        super.initUI();
    }

    private void loadData() {
        if (addressResponse != null) {
            return;
        }

        String addressJson = BaseUserInfoCache.getAddress(getContext());
        addressResponse = new Gson().fromJson(addressJson, AddressResponse.class);
        RecyclerView mRecyclerView = mView.findViewById(R.id.container_layout);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mOrderList = addressResponse.data;
        areaListAdapter = new AreaListAdapter(getContext(), mOrderList);
        mRecyclerView.setAdapter(areaListAdapter);
        areaListAdapter.notifyDataSetChanged();

        areaListAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
            for (ProvinceModel provinceModel : mOrderList) {
                provinceModel.isChecked = false;
            }
            mOnSelectAreaCallback.onSelectAt(addressResponse.data.get(position).disName, position);
            mOrderList.get(position).isChecked = true;
            areaListAdapter.notifyDataSetChanged();
        });
    }

    public void setmOnSelectAreaCallback(OnSelectAreaCallback mOnSelectAreaCallback) {
        this.mOnSelectAreaCallback = mOnSelectAreaCallback;
    }

    public void setSelectIndex(int index) {
        for (ProvinceModel provinceModel : mOrderList) {
            provinceModel.isChecked = false;
        }
        mOrderList.get(index).isChecked = true;
        areaListAdapter.notifyDataSetChanged();
    }
}
