package com.tojoy.common.Widgets.AddressPicker;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.netease.nim.uikit.R;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.dialog.PickerViewAnimateUtil;
import com.tojoy.tjoybaselib.util.string.StringUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.common.App.BaseFragment;
import com.tojoy.tjoybaselib.model.user.AddressResponse;
import com.tojoy.tjoybaselib.model.user.CityModel;
import com.tojoy.tjoybaselib.model.user.CountyModel;
import com.tojoy.tjoybaselib.model.user.ProvinceModel;

import java.util.ArrayList;
import java.util.List;


public class NewAddreassPickview {

    private Context context;
    protected ViewGroup contentContainer;
    private Animation outAnim;
    private Animation inAnim;
    private boolean isAnim = true;
    private boolean isShowing;
    private int gravity = Gravity.BOTTOM;
    public ViewGroup decorView;
    private ViewGroup rootView;
    private boolean dismissing;

    private int blueColor, blackColor;
    private int indicatorLeftMargin1;
    private int indicatorTopMargin;
    private int cellWidth1;

    private TextView mTabTitle1, mTabTitle2, mTabTitle3;

    private AddressFragment mAddressFragment;
    private AreaFragment mCityFragment;
    private CountyFragment mCountyFragment;
    private int currentProvinceIndex = -1;
    private int currentCityIndex = -1;
    private int currentCountryIndex = -1;

    private ViewPager mViewPager;
    private FragmentPagerAdapter mAdapter;
    private ArrayList mFragments = new ArrayList<BaseFragment>();

    private List<ProvinceModel> mOrderList = new ArrayList<>();

    public boolean isSelectCityOnly = false;

    private View mIndicate;

    public NewAddreassPickview(Context context) {
        this.context = context;
        init();
        initViews();
        initData();
    }


    private void initViews() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        if (decorView == null) {
            decorView = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        }
        //将控件添加到decorView中
        rootView = (ViewGroup) layoutInflater.inflate(R.layout.layout_area_pick_layout, decorView, false);
        rootView.setLayoutParams(new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        ));
        rootView.setBackgroundColor(context.getResources().getColor(R.color.c66000000));
        contentContainer = rootView.findViewById(R.id.content_container);
        setKeyBackCancelable(true);

        mViewPager = rootView.findViewById(R.id.viewpager);

        mTabTitle1 = rootView.findViewById(R.id.tv_1);
        mTabTitle2 = rootView.findViewById(R.id.tv_2);
        mTabTitle3 = rootView.findViewById(R.id.tv_3);

        cellWidth1 = getCellWidth(mTabTitle1.getText().toString());
        indicatorLeftMargin1 = (cellWidth1 - AppUtils.dip2px(context, 30)) / 2;
        indicatorTopMargin = AppUtils.dip2px(context, 33);

        mIndicate = rootView.findViewById(R.id.indicator);
        ((RelativeLayout.LayoutParams) mIndicate.getLayoutParams()).setMargins(indicatorLeftMargin1, indicatorTopMargin, 0, 0);

        blackColor = context.getResources().getColor(R.color.color_000000);
        blueColor = context.getResources().getColor(R.color.color_3A83F7);

        rootView.findViewById(R.id.iv_close).setOnClickListener(v -> dismiss());

        mTabTitle1.setOnClickListener(v -> mViewPager.setCurrentItem(0));
        mTabTitle2.setOnClickListener(v -> mViewPager.setCurrentItem(1));
        mTabTitle3.setOnClickListener(v -> mViewPager.setCurrentItem(2));
    }

    public void setTitle(String titleStr) {
        setOutSideCancelable(true);
        rootView.findViewById(R.id.iv_close).setVisibility(View.GONE);
        ((TextView) rootView.findViewById(R.id.title)).setText(titleStr);
    }

    protected void init() {
        inAnim = getInAnimation();
        outAnim = getOutAnimation();
    }

    /**
     * @param v      (是通过哪个View弹出的)
     * @param isAnim 是否显示动画效果
     */
    public void show(View v, boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(View v) {
        show();
    }

    /**
     * 添加View到根视图
     */
    public void show() {
        if (isShowing()) {
            return;
        }
        isShowing = true;
        onAttached(rootView);
        rootView.requestFocus();
    }

    /**
     * show的时候调用
     *
     * @param view 这个View
     */
    private void onAttached(View view) {
        decorView.addView(view);
        if (isAnim) {
            contentContainer.startAnimation(inAnim);
        }
    }


    /**
     * 检测该View是不是已经添加到根视图
     *
     * @return 如果视图已经存在该View返回true
     */
    public boolean isShowing() {
        return rootView.getParent() != null || isShowing;
    }


    public void dismiss() {
        if (dismissing) {
            return;
        }

        if (isAnim) {
            //消失动画
            outAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    dismissImmediately();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            contentContainer.startAnimation(outAnim);
        } else {
            dismissImmediately();
        }
        dismissing = true;
    }

    public void dismissImmediately() {
        decorView.post(() -> {
            //从根视图移除
            decorView.removeView(rootView);
            isShowing = false;
            dismissing = false;
        });
    }

    public Animation getInAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, true);
        return AnimationUtils.loadAnimation(context, res);
    }

    public Animation getOutAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, false);
        return AnimationUtils.loadAnimation(context, res);
    }

    public void setKeyBackCancelable(boolean isCancelable) {
        ViewGroup View;
        View = rootView;
        View.setFocusable(isCancelable);
        View.setFocusableInTouchMode(isCancelable);
        if (isCancelable) {
            View.setOnKeyListener(onKeyBackListener);
        } else {
            View.setOnKeyListener(null);
        }
    }

    private View.OnKeyListener onKeyBackListener = (v, keyCode, event) -> {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == MotionEvent.ACTION_DOWN
                && isShowing()) {
            dismiss();
            return true;
        }
        return false;
    };

    public NewAddreassPickview setOutSideCancelable(boolean isCancelable) {
        if (rootView != null) {
            View view = rootView.findViewById(R.id.outmost_container);

            if (isCancelable) {
                view.setOnTouchListener(onCancelableTouchListener);
            } else {
                view.setOnTouchListener(null);
            }
        }
        return this;
    }

    /**
     * Called when the user touch on black overlay in order to dismiss the dialog
     */
    @SuppressLint("ClickableViewAccessibility")
    private final View.OnTouchListener onCancelableTouchListener = (v, event) -> {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            dismiss();
        }
        return false;
    };

    private void initData() {
        String addressJson = BaseUserInfoCache.getAddress(context);
        AddressResponse addressResponse = new Gson().fromJson(addressJson, AddressResponse.class);
        mOrderList = addressResponse.data;

        mAddressFragment = new AddressFragment(context);
        mAddressFragment.setmOnSelectAreaCallback((name, pos) -> onSelectArea(1, pos, name, false));
        mFragments.add(mAddressFragment);

        //set adapter
        mAdapter = new FragmentPagerAdapter(((FragmentActivity) context).getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return mFragments.size();
            }

            @Override
            public Fragment getItem(int position) {
                return (Fragment) mFragments.get(position);
            }

        };
        mViewPager.setAdapter(mAdapter);
        mViewPager.setCurrentItem(0);
        mViewPager.setOffscreenPageLimit(mFragments.size());

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                changeIndicatorUI(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private int getCellWidth(String title) {
        return (int) AppUtils.sp2px(context, title.length() * 14);
    }

    private float getDividerCellWidth1(float precent) {
        int firstTitle = getCellWidth(mTabTitle1.getText().toString());
        int secondTitle = getCellWidth(mTabTitle2.getText().toString());

        return precent * ((firstTitle + secondTitle) / 2 + AppUtils.dip2px(context, 8));
    }

    private float getDividerCellWidth2(float precent) {
        int secondTitle = getCellWidth(mTabTitle2.getText().toString());
        int thirdTitle = getCellWidth(mTabTitle3.getText().toString());

        return precent * ((thirdTitle + secondTitle) / 2 + AppUtils.dip2px(context, 8));
    }

    private void changeIndicatorUI(int position, float positionOffset, int positionOffsetPixels) {
        if (position == 0) {
            mTabTitle1.setTextColor(StringUtil.getCurrentColor(positionOffset, blueColor, blackColor));
            mTabTitle2.setTextColor(StringUtil.getCurrentColor(positionOffset, blackColor, blueColor));
            mTabTitle3.setTextColor(StringUtil.getCurrentColor(positionOffset, blackColor, blackColor));

            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(AppUtils.dip2px(context, 30), AppUtils.dip2px(context, 2));
            lps.setMargins((int) (indicatorLeftMargin1 + getDividerCellWidth1(positionOffset)), indicatorTopMargin, 0, 0);
            mIndicate.setLayoutParams(lps);
        } else if (position == 1) {
            mTabTitle1.setTextColor(StringUtil.getCurrentColor(positionOffset, blackColor, blackColor));
            mTabTitle2.setTextColor(StringUtil.getCurrentColor(positionOffset, blueColor, blackColor));
            mTabTitle3.setTextColor(StringUtil.getCurrentColor(positionOffset, blackColor, blueColor));

            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(AppUtils.dip2px(context, 30), AppUtils.dip2px(context, 2));
            lps.setMargins((int) ((indicatorLeftMargin1 + getDividerCellWidth2(positionOffset)) + getDividerCellWidth1(1.0f)), indicatorTopMargin, 0, 0);
            mIndicate.setLayoutParams(lps);

        }
    }

    private void onSelectArea(int type, int pos, String name, boolean needSetCheck) {
        if (type == 1) {
            mTabTitle1.setText(name);
            mTabTitle2.setVisibility(View.VISIBLE);
            mTabTitle2.setText("请选择");
            mTabTitle3.setVisibility(View.GONE);
            currentCityIndex = -1;
            currentCountryIndex = -1;

            cellWidth1 = getCellWidth(mTabTitle1.getText().toString());
            indicatorLeftMargin1 = (cellWidth1 - AppUtils.dip2px(context, 30)) / 2;

            currentProvinceIndex = pos;

            if (mFragments.size() == 1) {
                ArrayList<CityModel> cityModels = new ArrayList<>();
                cityModels.addAll(mOrderList.get(pos).sonList);
                mCityFragment = new AreaFragment(context, cityModels);
                mFragments.add(mCityFragment);

                mAdapter.notifyDataSetChanged();
                mViewPager.setOffscreenPageLimit(mFragments.size());
                mCityFragment.setmOnSelectAreaCallback((name12, pos12) -> {
                    currentCityIndex = pos12;
                    onSelectArea(2, pos12, name12, false);
                });
            } else {
                ArrayList<CityModel> cityModels = new ArrayList<>();
                cityModels.addAll(mOrderList.get(pos).sonList);
                ((AreaFragment) mFragments.get(1)).setmOrderList(cityModels);
            }

            if (needSetCheck) {
                mAddressFragment.setSelectIndex(pos);
            }

            mViewPager.setCurrentItem(1);
        } else if (type == 2) {
            if (isSelectCityOnly) {
                mTabTitle2.setText(name);
                dismiss();
            } else {
                mTabTitle2.setText(name);
                mTabTitle3.setVisibility(View.VISIBLE);
                mTabTitle3.setText("请选择");
                currentCountryIndex = -1;

                changeIndicatorUI(0, 1, 0);

                if (mFragments.size() == 2) {
                    ArrayList<CountyModel> cityModels = new ArrayList<>();
                    cityModels.addAll(mOrderList.get(currentProvinceIndex).sonList.get(pos).sonList);
                    mCountyFragment = new CountyFragment(context, cityModels);
                    mFragments.add(mCountyFragment);

                    mAdapter.notifyDataSetChanged();
                    mViewPager.setOffscreenPageLimit(mFragments.size());
                    mCountyFragment.setmOnSelectAreaCallback((name1, pos1) -> {
                        onSelectArea(3, pos1, name1, false);
                        dismiss();
                    });
                } else {
                    ArrayList<CountyModel> cityModels = new ArrayList<>();
                    cityModels.addAll(mOrderList.get(currentProvinceIndex).sonList.get(pos).sonList);
                    ((CountyFragment) mFragments.get(2)).setmOrderList(cityModels);
                }

                if (needSetCheck) {
                    mCityFragment.setSelectIndex(pos);
                }

                mViewPager.setCurrentItem(2);
            }
        } else {
            currentCountryIndex = pos;
            mTabTitle3.setText(name);
            changeIndicatorUI(1, 1, 0);

            if (needSetCheck) {
                mCountyFragment.setSelectIndex(pos);
            }

        }

        if (selectAreaCallback != null) {

            ProvinceModel provinceModel = mOrderList.get(currentProvinceIndex);
            CityModel cityModel = currentCityIndex >= 0 ? mOrderList.get(currentProvinceIndex).sonList.get(currentCityIndex) : null;
            CountyModel countyModel = (currentCountryIndex >= 0 && currentCityIndex >= 0) ? mOrderList.get(currentProvinceIndex).sonList.get(currentCityIndex).sonList.get(currentCountryIndex) : null;

            selectAreaCallback.onSelectArea(provinceModel,
                    cityModel,
                    countyModel);
        }
    }

    public String setSelected(boolean getName, String pro, String city, String county) {
        int pos1 = -1;
        int pos2 = -1;
        int pos3 = -1;
        for (int i = 0; i < mOrderList.size(); i++) {
            if (mOrderList.get(i).id.equals(pro)) {
                pos1 = i;
                break;
            }
        }

        if (pos1 >= 0) {
            for (int i = 0; i < mOrderList.get(pos1).sonList.size(); i++) {
                if (mOrderList.get(pos1).sonList.get(i).id.equals(city)) {
                    pos2 = i;
                    break;
                }
            }
        } else {
            return "";
        }


        if (pos2 >= 0) {
            ArrayList<CountyModel> list = mOrderList.get(pos1).sonList.get(pos2).sonList;
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).id.equals(county)) {
                    pos3 = i;
                    break;
                }
            }
        } else {
            return "";
        }


        if (pos3 >= 0) {

            int finalPos1 = pos1;
            int finalPos2 = pos2;
            int finalPos3 = pos3;

            if (getName) {
                selectAreaCallback.onSelectArea(mOrderList.get(pos1), mOrderList.get(finalPos1).sonList.get(finalPos2), mOrderList.get(finalPos1).sonList.get(finalPos2).sonList.get(finalPos3));
                return mOrderList.get(pos1).disName + " " + mOrderList.get(finalPos1).sonList.get(finalPos2).disName + " " + mOrderList.get(finalPos1).sonList.get(finalPos2).sonList.get(finalPos3).disName;
            }
            onSelectArea(1, pos1, mOrderList.get(pos1).disName, true);

            new android.os.Handler().postDelayed(() -> {
                onSelectArea(2, finalPos2, mOrderList.get(finalPos1).sonList.get(finalPos2).disName, true);
                new android.os.Handler().postDelayed(() -> onSelectArea(3, finalPos3, mOrderList.get(finalPos1).sonList.get(finalPos2).sonList.get(finalPos3).disName, true), 100);
            }, 100);
        }

        return "";

    }


    public interface SelectAreaCallback {
        void onSelectArea(ProvinceModel provinceModel, CityModel cityModel, CountyModel countyModel);
    }

    private SelectAreaCallback selectAreaCallback;

    public void setSelectAreaCallback(SelectAreaCallback selectAreaCallback) {
        this.selectAreaCallback = selectAreaCallback;
    }
}
