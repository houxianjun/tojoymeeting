package com.tojoy.common.Widgets.AnimationIndicator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.netease.nim.uikit.R;
import com.tojoy.tjoybaselib.util.sys.AppUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chengyanfang on 2017/12/11.
 */

public class MeetingHomeAnimationIndicator {
    private Context mContext;
    private View mView;
    private LinearLayout indicator;

    private int mIndicatorSelectedResId;
    private int mIndicatorUnselectedResId;
    private LinearLayout.LayoutParams mNormalParams, mSelectedParams;
    private int count;
    private int lastPosition;
    private List<ImageView> indicatorImages = new ArrayList<>();


    public MeetingHomeAnimationIndicator(Context mContext) {
        this.mContext = mContext;
        mIndicatorSelectedResId = R.drawable.animation_meeting_home_banner_indicator_selected;
        mIndicatorUnselectedResId = R.drawable.animation_meeting_home_banner_indicator_noemal;

        mNormalParams = new LinearLayout.LayoutParams(AppUtils.dip2px(mContext, 6.5f), AppUtils.dip2px(mContext, 2));
        mNormalParams.leftMargin = AppUtils.dip2px(mContext, 2);
        mNormalParams.rightMargin = AppUtils.dip2px(mContext, 2);

        mSelectedParams = new LinearLayout.LayoutParams(AppUtils.dip2px(mContext, 6.5f), AppUtils.dip2px(mContext, 2));
        mSelectedParams.leftMargin = AppUtils.dip2px(mContext, 2);
        mSelectedParams.rightMargin = AppUtils.dip2px(mContext, 2);
    }

    public void setIndicatorCoolor() {
        mIndicatorSelectedResId = R.drawable.animation_meeting_home_banner_indicator_selected;
        mIndicatorUnselectedResId = R.drawable.animation_meeting_home_banner_indicator_noemal;
    }

    public View getView() {
        if (mView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mView = aLayoutInflater.inflate(R.layout.view_animator_indicator, null);
        }
        initUI();

        return mView;
    }

    private void initUI() {
        indicator = mView.findViewById(R.id.indicator_container);
    }


    public void createIndicator(int count) {
        this.count = count;
        indicatorImages.clear();
        indicator.removeAllViews();

        //一个的时候不做显示
        if (count <= 1) {
            return;
        }

        for (int i = 0; i < count; i++) {
            ImageView imageView = new ImageView(mContext);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            indicatorImages.add(imageView);

            if (i == 0) {
                imageView.setImageResource(mIndicatorSelectedResId);
                indicator.addView(imageView, mSelectedParams);
            } else {
                imageView.setImageResource(mIndicatorUnselectedResId);
                indicator.addView(imageView, mNormalParams);
            }
        }
    }

    public void selectPos(int position) {
        for (int i = 0; i < count; i++) {
            if (position < indicatorImages.size()) {
                if (i == position) {
                    indicatorImages.get(position).setImageResource(mIndicatorSelectedResId);
                } else {
                    if (i < indicatorImages.size()) {
                        indicatorImages.get(i).setImageResource(mIndicatorUnselectedResId);
                    }
                }
            }
        }

        if (indicatorImages != null && indicatorImages.size() > 0 && lastPosition < indicatorImages.size() && position < indicatorImages.size()) {
            indicatorImages.get(lastPosition).setLayoutParams(mNormalParams);
            indicatorImages.get(position).setLayoutParams(mSelectedParams);
        }

        if (lastPosition != position && indicatorImages != null && indicatorImages.size() > position) {
            lastPosition = position;
        }

    }

}
