package com.tojoy.common.Config;

/**
 * @author qll
 * @date 2019/4/16
 * 用户角色信息
 * （1：客户；2：服务秘书 ；3： 领导；4：路演师）
 */
public enum UserEmployeeFlagCode {
    /**
     * 未知
     */
    Unkown(0),
    /**
     * 客户（非公司员工）
     */
    Customer(1),
    /**
     * 服务秘书
     */
    ServiceSecretary(2),
    /**
     * 领导
     */
    Leader(3),
    /**
     * 路演师
     */
    Roadshow(4),;

    private int value;

    UserEmployeeFlagCode(int netState) {
        this.value = netState;
    }

    public int getValue() {
        return value;
    }

    /**
     * 1、申请中  2、被驳回  3 已通过  4已完成(本次申请通过并且直播完成)
     * @param value
     * @return
     */
    public static UserEmployeeFlagCode getStateCode(int value) {
        switch (value) {
            case 0:
                return Unkown;
            case 1:
                return Customer;
            case 2:
                return ServiceSecretary;
            case 3:
                return Leader;
            case 4:
                return Roadshow;
            default:
                return Unkown;
        }
    }
}
