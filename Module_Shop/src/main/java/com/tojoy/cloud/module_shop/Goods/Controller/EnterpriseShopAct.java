package com.tojoy.cloud.module_shop.Goods.Controller;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.util.sys.StatusbarUtils;
import com.tojoy.cloud.module_shop.R;
import com.tojoy.common.App.BaseFragment;

/**
 * 企业店铺主页
 * @author daibin
 */
@Route(path = RouterPathProvider.Enterprise_Shop)
public class EnterpriseShopAct extends UI {
    BaseFragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterprise_shop);
        StatusbarUtils.setStatusBarDarkMode(this, StatusbarUtils.statusbarlightmode(this));
        initUI();
        initTitle();
        setSwipeBackEnable(false);
    }

    private void initUI() {
        String enterCompanyCode = getIntent().getStringExtra("companyCode");
        String mCompaynUrl = getIntent().getStringExtra("mCompaynUrl");
        String mCompanyName = getIntent().getStringExtra("mCompanyName");

        mFragment = new ShopFragment(enterCompanyCode, mCompaynUrl, mCompanyName);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        if (mFragment != null) {
            transaction.add(R.id.flv_root, mFragment).commit();
        }
    }

    private void initTitle() {

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
