package com.tojoy.cloud.module_shop.Api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.net.BaseApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.net.RetrofitSingleton;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.util.net.RxUtils;
import com.tojoy.tjoybaselib.util.router.RouterConstants;
import com.tojoy.cloud.module_shop.Enterprise.Apply.Model.EnterParamsResponse;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.DepartRespone;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.RoleResponse;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.UploadStaffModel;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Home.Model.EnterStructResponse;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Home.Model.EnterStructSearchResponse;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.NewOderDetailModel;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.OrderPayDetailTimeResponse;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.OrderPayDetailWXPayResponse;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.RechargeOrderStatusResponse;
import com.tojoy.cloud.module_shop.Order.OrderSegment.Model.OrderManagementResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;
import rx.Subscriber;

public class ShopModuleApiProvider extends BaseApiProvider {
	private static ShopModuleApiInterface app_retrofit = null;

	private static final String PAYKEY = "TojoyPaymentKey";


	/**
	 * //////////////////////////////
	 * 构造单例
	 * //////////////////////////////
	 */
	private ShopModuleApiProvider() {
		init();
	}

	private static class SingletonHolder {
		private static final ShopModuleApiProvider INSTANCE = new ShopModuleApiProvider();
	}

	public static ShopModuleApiProvider getInstance() {
		return ShopModuleApiProvider.SingletonHolder.INSTANCE;
	}

	public static ShopModuleApiProvider initialize(Context context) {
		if (mContext == null) {
			mContext = context;
		}
		return ShopModuleApiProvider.SingletonHolder.INSTANCE;
	}

	public void init() {
		app_retrofit = RetrofitSingleton.getInstance(mContext).app_retrofit.create(ShopModuleApiInterface.class);
	}


	/**
	 * @param pageNum
	 * @param searchkeyword 订单号/商品名称
	 * @param status        订单状态
	 * @param observer
	 */
	public void mOrderManagementList(String userId, int pageNum, String searchkeyword, int status, Observer<OrderManagementResponse> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			if (searchkeyword != null) {
				dataJson.put("searchkeyword", searchkeyword);
			}
			dataJson.put("customerId", userId);
			dataJson.put("orderStatus", status);
			dataJson.put("pageNum", String.valueOf(pageNum));
			dataJson.put("pageSize", String.valueOf(AppConfig.PER_PAGE_COUNT));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.mOrderManagementList(getCommandSign(dataJson, "mOrderManagementList"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<OrderManagementResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();

			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(OrderManagementResponse preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});
	}

	/**
	 * @param mOrderId 订单号/商品名称
	 * @param observer
	 */
	public void mOrderDelete(String mOrderId, Observer<OMBaseResponse> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("id", mOrderId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.mOrderDelete(getCommandSign(dataJson, "mOrderDelete"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<OMBaseResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();

			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(OMBaseResponse preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});
	}

	/**
	 * 订单详情
	 *
	 * @param id
	 * @param observer
	 */
	public void getOrderPayParams(String id, Observer<OrderPayDetailWXPayResponse> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("userOrderNo", id);
			dataJson.put("paySource", "3");
		} catch (JSONException e) {
			e.printStackTrace();
		}
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.getOrderPayParams(getCommandSign(dataJson, "getOrderDetail"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<OrderPayDetailWXPayResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();
			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);
			}

			@Override
			public void onNext(OrderPayDetailWXPayResponse preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});
	}

	/**
	 * 收银台倒计时详情
	 *
	 * @param id
	 * @param observer
	 */
	public void getOrderTimeDetail(String id, Observer<OrderPayDetailTimeResponse> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("userOrderNo", id);
		} catch (JSONException e) {
			e.printStackTrace();
		}
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.orderTimeDetail(getCommandSign(dataJson, "getOrderTimeDetail"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<OrderPayDetailTimeResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();
			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);
			}

			@Override
			public void onNext(OrderPayDetailTimeResponse preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});
	}

	/**
	 * 新获取订单详情口
	 *
	 * @param orderId
	 * @param observer
	 */
	public void getNewOrderDetail(String orderId, Observer<NewOderDetailModel> observer) {
		getNewOrderDetail(orderId, RouterConstants.ORDER_TYPE_DEFAULT,observer);
	}

	public void getNewOrderDetail(String orderId, int orderType, Observer<NewOderDetailModel> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("customerId", BaseUserInfoCache.getUserId(mContext));
			dataJson.put("orderId", orderId);
			dataJson.put("orderType", orderType);
		} catch (JSONException e) {
			e.printStackTrace();
		}
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.newOderDetail(getCommandSign(dataJson, "getNewOrderDetail"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<NewOderDetailModel>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();

			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(NewOderDetailModel preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});
	}

	/**
	 * 确认订单
	 *
	 * @param orderId 订单id
	 */
	public void comfirmOrder(String orderId, Observer<OMBaseResponse> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("id", orderId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.mComfirmOrder(getCommandSign(dataJson, "mComfirmOrder"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<OMBaseResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();

			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(OMBaseResponse preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});

	}

	/**
	 * 取消订单
	 *
	 * @param orderId 订单号
	 */
	public void cancelOrder(String orderId, Observer<OMBaseResponse> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("id", orderId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.mCancelOrder(getCommandSign(dataJson, "mCancelOrder"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<OMBaseResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();

			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(OMBaseResponse preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});
	}

	/**
	 * 删除订单
	 *
	 * @param orderId 订单号
	 */
	public void deletOrder(String orderId, Observer<OMBaseResponse> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("id", orderId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.mDeleteOrder(getCommandSign(dataJson, "mCancelOrder"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<OMBaseResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();

			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(OMBaseResponse preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});
	}

	/**
	 * 新增下级部门
	 */
	public void addDepartment(String departName, String parentId, Observer<OMBaseResponse> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("name", departName);
			dataJson.put("pid", parentId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.addDepart(getCommandSign(dataJson, "addDepartment"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<OMBaseResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();

			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(OMBaseResponse response) {
				observer.onNext(response);
			}
		});
	}

	/**
	 * 获取部门
	 */
	public void getDepartments( String parentId, Observer<DepartRespone> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("pid", parentId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.getDepartList(getCommandSign(dataJson, "getDepartments"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<DepartRespone>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();
			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(DepartRespone response) {
				observer.onNext(response);
			}
		});
	}


	/**
	 * 获取企业规模
	 */
	public void enterSize(Observer<EnterParamsResponse> observer) {
		JSONObject dataJson = new JSONObject();
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.enterSize(getCommandSign(dataJson, "enterSize"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<EnterParamsResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();

			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(EnterParamsResponse preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});

	}


	/**
	 * 获取企业职位
	 */
	public void enterPos(Observer<EnterParamsResponse> observer) {
		JSONObject dataJson = new JSONObject();

        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.enterPos(getCommandSign(dataJson, "enterSize"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<EnterParamsResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();

			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(EnterParamsResponse preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});

	}

	/**
	 * 提交企业入驻申请
	 */
	public void submitApply(String name, String cityCode, String scale, String email, String applicantTitle, Observer<OMBaseResponse> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("name", name);
			dataJson.put("cityCode", cityCode);
			dataJson.put("scale", scale);
			dataJson.put("email", email);
			dataJson.put("applicantTitle", applicantTitle);
		} catch (JSONException e) {
			e.printStackTrace();
		}

        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.submitApply(getCommandSign(dataJson, "submitApply"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<OMBaseResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();

			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(OMBaseResponse preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});
	}


	/**
	 * 查询企业通讯录
	 */
	public void getEnterpriseUsers(String corporationCode, Observer<EnterStructResponse> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("companyCode", corporationCode);
		} catch (JSONException e) {
			e.printStackTrace();
		}

        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.getEnterpriseUsers(getCommandSign(dataJson, "getEnterpriseUsers"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<EnterStructResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();

			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(EnterStructResponse preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});
	}


	/**
	 * 查询子部门及用户
	 */
	public void getSubDepartUsers(String corporationCode, String deptId, Observer<EnterStructResponse> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("companyCode", corporationCode);
			dataJson.put("deptId", deptId);
		} catch (JSONException e) {
			e.printStackTrace();
		}

        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.getSubDepartUsers(getCommandSign(dataJson, "getSubDepartUsers"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<EnterStructResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();

			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(EnterStructResponse preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});
	}

	/**
	 * 编辑员工信息
	 */
	public void editStaffInfo( String deptId, String mobile, String trueName, String userId, ArrayList<Integer> roleIds, Observer<OMBaseResponse> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("departmentCode", deptId);
			dataJson.put("mobile", mobile);
			dataJson.put("name", trueName);
			JSONArray jsonArray = new JSONArray(roleIds);
			dataJson.put("roleIds",jsonArray);
			dataJson.put("id", userId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.editStaffInfo(getCommandSign(dataJson, "editStaffInfo"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<OMBaseResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();
			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(OMBaseResponse response) {
				observer.onNext(response);
			}
		});
	}

	/**
	 * 获取角色
	 */
	public void getRoleList(String userId, Observer<RoleResponse> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("userId", userId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.getRoleList(getCommandSign(dataJson, "getRoleList"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<RoleResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();
			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(RoleResponse response) {
				observer.onNext(response);
			}
		});
	}

	/**
	 * 查询子部门及用户
	 */
	public void searchEnterStruct(String corporationCode, String key, Observer<EnterStructSearchResponse> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("companyCode", corporationCode);
			dataJson.put("key", key);
		} catch (JSONException e) {
			e.printStackTrace();
		}

        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.searchEnterStruct(getCommandSign(dataJson, "searchEnterStruct"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<EnterStructSearchResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();

			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(EnterStructSearchResponse preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});
	}

	/**
	 * 删除员工
	 */
	public void deleteStaff(String corporationCode, String moblie,String employeeId, Observer<OMBaseResponse> observer) {
		JSONObject dataJson = new JSONObject();
		try {
			//新接口入参
			dataJson.put("employeeId", employeeId);
			dataJson.put("corporationCode", corporationCode);
			dataJson.put("mobile", moblie);

		} catch (JSONException e) {
			e.printStackTrace();
		}
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.deleteStaff(getCommandSign(dataJson, "deleteStaff"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<OMBaseResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();

			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(OMBaseResponse preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});
	}

	/**
	 *  新增员工
	 * @param mobile
	 * @param trueName
	 * @param deptId
	 * @param roleIds [4,5,6]
	 * @param observer
	 */
	public void addStaff( String mobile, String trueName,  String deptId,ArrayList<Integer> roleIds ,Observer<OMBaseResponse> observer) {

		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("mobile", mobile);
			//替换truename
			dataJson.put("name", trueName);
			dataJson.put("departmentCode", deptId);
			JSONArray jsonArray = new JSONArray(roleIds);
			dataJson.put("roleIds",jsonArray);

		} catch (JSONException e) {
			e.printStackTrace();
		}
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.addStaff(getCommandSign(dataJson, "addStaff"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<OMBaseResponse>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();

			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(OMBaseResponse preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});
	}

	/**
	 * 批量新增员工
	 */
	public void addStaffs( ArrayList<Integer> roleIds, String deptId, String userInfoList, Observer<UploadStaffModel> observer) {

		List<UploadStaffModel.Staff> list = new Gson().fromJson(userInfoList, new TypeToken<List<UploadStaffModel.Staff>>() {}.getType());

		List<JSONObject> jlist = new ArrayList<>();
		try {
			for (UploadStaffModel.Staff staff : list) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("mobile", staff.mobile);
				jsonObject.put("name", staff.trueName);
				jsonObject.put("employeeCode", staff.employeeId);
				jlist.add(jsonObject);
			}
		} catch (Exception e) {

		}
		JSONArray jsonArray = new JSONArray(jlist);

		JSONArray roleIdsArray = new JSONArray(roleIds);

		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("companyCode", BaseUserInfoCache.getCompanyCode(mContext));
			dataJson.put("departmentCode", deptId);
			dataJson.put("userInfoList", jsonArray);
			dataJson.put("roleIds",roleIdsArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.addStaffs(getCommandSign(dataJson, "addStaff"), getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {
				}).subscribe(new Subscriber<UploadStaffModel>() {
			@Override
			public void onCompleted() {
				observer.onCompleted();

			}

			@Override
			public void onError(Throwable e) {
				observer.onError(e);

			}

			@Override
			public void onNext(UploadStaffModel preBindBankResponse) {
				observer.onNext(preBindBankResponse);
			}
		});
	}

	/**
	 * 会议服务管理：充值订单剩余有效时间
	 * @param userOrderNo
	 * @param observer
	 */
	public void queryRechargeUserPaySurplusTime(String userOrderNo,Observer<OrderPayDetailTimeResponse> observer){
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("userOrderNo", userOrderNo);
		} catch (JSONException e) {
			e.printStackTrace();
		}
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.rechargeUserPaySurplusTime(getCommandSign(dataJson, "userPaySurplusTime"),getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {

				})
				.subscribe(new Subscriber<OrderPayDetailTimeResponse>() {
					@Override
					public void onCompleted() {
						observer.onCompleted();
					}

					@Override
					public void onError(Throwable e) {
						observer.onError(e);
					}

					@Override
					public void onNext(OrderPayDetailTimeResponse response) {
						observer.onNext(response);
					}
				});
	}

	/**
	 * 会议服务管理：充值支付接口
	 * @param userOrderNo
	 * @param paySource
	 * @param observer
	 */
	public void queryRechargePlatformAliWxPay(String userOrderNo,String paySource,Observer<OrderPayDetailWXPayResponse> observer){
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("userOrderNo", userOrderNo);
			dataJson.put("paySource", paySource);

		} catch (JSONException e) {
			e.printStackTrace();
		}
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.rechargePlatformAliWxPay(getCommandSign(dataJson, "platformAliWxPay"),getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {

				})
				.subscribe(new Subscriber<OrderPayDetailWXPayResponse>() {
					@Override
					public void onCompleted() {
						observer.onCompleted();
					}

					@Override
					public void onError(Throwable e) {
						observer.onError(e);
					}

					@Override
					public void onNext(OrderPayDetailWXPayResponse response) {
						observer.onNext(response);
					}
				});
	}


	/**
	 * 会议服务管理：充值订单详情接口，返回订单状态
	 * @param id
	 * @param observer
	 */
	public void queryRechargeOrderDetail(String id,Observer<RechargeOrderStatusResponse> observer){
		JSONObject dataJson = new JSONObject();
		try {
			dataJson.put("id", id);

		} catch (JSONException e) {
			e.printStackTrace();
		}
        AppConfig.isAppendHeaderAuthorization = true;
		app_retrofit.rechargeOrderDetail(getCommandSign(dataJson, "detail"),getCommandRequestBody(dataJson))
				.compose(RxUtils.rxSchedulerHelper())
				.doOnError(throwable -> {

				})
				.subscribe(new Subscriber<RechargeOrderStatusResponse>() {
					@Override
					public void onCompleted() {
						observer.onCompleted();
					}

					@Override
					public void onError(Throwable e) {
						observer.onError(e);
					}

					@Override
					public void onNext(RechargeOrderStatusResponse response) {
						observer.onNext(response);
					}
				});
	}
}
