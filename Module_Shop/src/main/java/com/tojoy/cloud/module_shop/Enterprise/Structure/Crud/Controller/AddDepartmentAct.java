package com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.editView.ClearEmojInputFilter;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.cloud.module_shop.Api.ShopModuleApiProvider;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.DepartRespone;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.DepartStaffEvent;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Model.DepartModel;
import com.tojoy.cloud.module_shop.R;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 新增部门
 * create by luzhu on 2020/2/27
 */

public class AddDepartmentAct extends UI {

    public static final int Request_Code = 888;
    public static final int Result_Code = 889;

    private EditText mEtDepartment;
    private TextView mTvAdminiDepartment;
    private String corporationCode;
    private String parentId;
    private String selectParentId;
    private String departId;
    private String departName;
    private List<DepartModel> comData;
    private DepartModel comModel;
    private String departsJsonStr;


    public static void start(String corporationCode, String parentId, String departId, String departName, Context context) {
        Intent intent = new Intent();
        intent.putExtra("corporationCode", corporationCode);
        intent.putExtra("parentId", parentId);
        intent.putExtra("departId", departId);
        intent.putExtra("deptName", departName);
        intent.setClass(context, AddDepartmentAct.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_department);

        initUI();
        getData();
    }

    private void getData() {
        corporationCode = getIntent().getStringExtra("corporationCode");
        //上级部门
        parentId = getIntent().getStringExtra("parentId");
        //当前部门
        departId = getIntent().getStringExtra("departId");
        //部门名称
        departName = getIntent().getStringExtra("deptName");
        //被选择的上级部门
        selectParentId = departId;
        mTvAdminiDepartment.setText(departName == null ? "请选择部门" : departName);
        //父子部门都没有，说明是公司根部
        comData = new ArrayList<>();
        comModel = new DepartModel();
        comModel.name = BaseUserInfoCache.getCompanyName(AddDepartmentAct.this);
        comModel.id = "";
        comModel.parentId = "";
        comModel.corporationCode = corporationCode;
        if (TextUtils.isEmpty(parentId) && TextUtils.isEmpty(departId)) {
            comModel.isChecked = true;
        }
        comData.add(comModel);

        ShopModuleApiProvider.getInstance().getDepartments(parentId, new Observer<DepartRespone>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(DepartRespone respone) {

                if (respone.isSuccess()) {
                    List<DepartModel> data = respone.data;
                    for (int i = 0; i < data.size(); i++) {
                        DepartModel model = data.get(i);
                        if (!TextUtils.isEmpty(departId) && departId.equals(model.id)) {
                            mTvAdminiDepartment.setText(model.name);
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Request_Code && data != null) {
            corporationCode = data.getStringExtra("corporationCode");
            selectParentId = data.getStringExtra("parentId");
            departName = data.getStringExtra("departName");
            mTvAdminiDepartment.setText(data.getStringExtra("departName"));
        }

    }

    private void initUI() {
        setStatusBar();
        setUpNavigationBar();
        showBack();
        setTitle("新增部门");
        mEtDepartment = (EditText) findViewById(R.id.etDepartment);
        mTvAdminiDepartment = (TextView) findViewById(R.id.tvAdminiDepartment);
        TextView mTvSave = (TextView) findViewById(R.id.tvSave);

        InputFilter[] emojiFilters = {new ClearEmojInputFilter()};
        mEtDepartment.setFilters(emojiFilters);

        mEtDepartment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String content = mEtDepartment.getText().toString();
                if (content.length() > 20) {
                    Toasty.normal(AddDepartmentAct.this, "字数超过限制").show();
                    mEtDepartment.setText(content.substring(0, 20));
                    mEtDepartment.setSelection(20);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mTvAdminiDepartment.setOnClickListener(view -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }
            showProgressHUD(AddDepartmentAct.this, "加载中");
            ShopModuleApiProvider.getInstance().getDepartments(parentId, new Observer<DepartRespone>() {
                @Override
                public void onCompleted() {
                    dismissProgressHUD();
                }

                @Override
                public void onError(Throwable e) {
                    dismissProgressHUD();
                    Toasty.normal(AddDepartmentAct.this, "网络错误，请检查网络").show();
                }

                @Override
                public void onNext(DepartRespone respone) {

                    if (respone.isSuccess()) {
                        List<DepartModel> data = respone.data;
                        comModel.existSubDept = !data.isEmpty();
                        departsJsonStr = new Gson().toJson(respone.data);

                        for (int i = 0; i < data.size(); i++) {
                            DepartModel model = data.get(i);
                            if (!TextUtils.isEmpty(departId) && departId.equals(model.id)) {
                                mTvAdminiDepartment.setText(model.name);
                            }
                        }

                    } else {
                        comModel.existSubDept = false;
                        departsJsonStr = new Gson().toJson(comData);
                    }
                    Intent intent = new Intent(AddDepartmentAct.this, SelectDepartAct.class);
                    intent.putExtra("departsJsonStr", departsJsonStr);
                    startActivityForResult(intent, Request_Code);
                }
            });
        });

        mTvSave.setOnClickListener(view -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }
            String departName = mEtDepartment.getText().toString().trim();
            if (TextUtils.isEmpty(departName)) {
                Toasty.normal(AddDepartmentAct.this, "请完善必填项").show();
                return;
            }
            String updepartMent = mTvAdminiDepartment.getText().toString().trim();
            if ("请选择部门".equals(updepartMent)) {
                Toasty.normal(AddDepartmentAct.this, "请选择上级部门").show();
                return;
            }
            showProgressHUD(AddDepartmentAct.this, "加载中");
            ShopModuleApiProvider.getInstance().addDepartment(departName, selectParentId, new Observer<OMBaseResponse>() {
                @Override
                public void onCompleted() {
                    dismissProgressHUD();
                }

                @Override
                public void onError(Throwable e) {
                    dismissProgressHUD();
                    Toasty.normal(AddDepartmentAct.this, "网络错误，请检查网络").show();
                }

                @Override
                public void onNext(OMBaseResponse response) {
                    if (response.isSuccess()) {
                        Toasty.normal(AddDepartmentAct.this, "保存成功").show();
                        EventBus.getDefault().post(new DepartStaffEvent("3", parentId));
                        finish();
                    } else {
                        Toasty.normal(AddDepartmentAct.this, response.msg).show();
                    }
                }
            });
        });
    }
}
