package com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.View;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Controller.HandAddStaffAct;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Controller.MailListAct;
import com.tojoy.cloud.module_shop.R;
import com.yanzhenjie.permission.AndPermission;

import es.dmoral.toasty.Toasty;

/**
 * 新增员工
 * Created by luuzhu on 2020/2/25.
 */

public class AddStaffDialog {

    private String departId;
    private String corporationCode;
    private String parentId;
    private Dialog mDialog;
    private Activity mContext;
    private View mView;

    public AddStaffDialog(String corporationCode, String parentId, String departId, Context context) {
        mContext = (Activity) context;
        this.corporationCode = corporationCode;
        this.parentId = parentId;
        this.departId = departId;
        initUI();
    }

    @SuppressLint("WrongConstant")
    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_add_staff, null);
        initView(mView);
    }

    public void show() {

        mDialog = new Dialog(mContext, R.style.LiveDialogStopLive);
        mDialog.setContentView(mView);

        Window win = mDialog.getWindow();
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = AppUtils.dip2px(mContext, 168);
        lp.windowAnimations = R.style.BottomInAndOutStyle;
        lp.gravity = Gravity.BOTTOM;
        win.setAttributes(lp);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.show();
    }

    public boolean isShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    private void initView(View view) {

        mView.findViewById(R.id.tvHandAdd).setOnClickListener(view12 -> {
            if (mDialog != null) {
                mDialog.dismiss();
            }
            HandAddStaffAct.start(mContext, "1", corporationCode, parentId, departId);
        });

        //有通讯录权限再跳
        mView.findViewById(R.id.tvMailAdd).setOnClickListener(view13 -> {
            if (mDialog != null) {
                mDialog.dismiss();
            }

            AndPermission.with(mContext)
                    .permission(
                            Manifest.permission.GET_ACCOUNTS,
                            Manifest.permission.READ_CONTACTS
                    )
                    .rationale((context, permissions, executor) -> {
                        // 此处可以选择显示提示弹窗
                        executor.execute();
                    })
                    // 用户给权限了
                    .onGranted(permissions -> {
                                MailListAct.start(mContext, corporationCode, parentId, departId);
                            }
                    )
                    // 用户拒绝权限，包括不再显示权限弹窗也在此列
                    .onDenied(permissions -> {
                        // 判断用户是不是不再显示权限弹窗了，是的话进入权限设置页
                        if (AndPermission.hasAlwaysDeniedPermission(mContext, permissions)) {
                            // 打开权限设置页
                            AndPermission.permissionSetting(mContext).execute();
                        } else {
                            Toasty.normal(mContext, "获取权限失败，请重试").show();
                        }

                    })
                    .start();
        });

        mView.findViewById(R.id.tvCancle).setOnClickListener(view1 -> {
            if (mDialog != null) {
                mDialog.dismiss();
            }
        });
    }
}
