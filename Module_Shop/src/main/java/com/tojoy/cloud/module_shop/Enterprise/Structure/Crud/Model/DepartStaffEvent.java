package com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model;

public class DepartStaffEvent {

    public String type;
    public String departId;

    /**
     * @param type 1员工新增 2员工删除   3部门新增
     * @param departId type为1和2时，departId是员工所在部门的id； 3时，departId是这个部门的上级部门id
     */
    public DepartStaffEvent(String type, String departId) {
        this.type = type;
        this.departId = departId;
    }
}
