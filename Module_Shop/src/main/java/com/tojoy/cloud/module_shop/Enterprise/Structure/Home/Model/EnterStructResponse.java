package com.tojoy.cloud.module_shop.Enterprise.Structure.Home.Model;

import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Model.StaffModel;

import java.util.ArrayList;

public class EnterStructResponse extends OMBaseResponse {

    public EnterStructResponseModel data;

    public static class EnterStructResponseModel {
        //企业数据
        public EnterStructAdminModel admin;
        public EnterStructDepartmentModel subDepartment;
        public EnterStructUserModel users;

        //部门数据
        public ArrayList<StaffModel> userList = new ArrayList<>();
        public ArrayList<StaffModel> subDepartmentList = new ArrayList<>();
    }

    public class EnterStructAdminModel {
        public int count;
        public ArrayList<StaffModel> userList = new ArrayList<>();
    }

    public static class EnterStructDepartmentModel {
        public int count;
        public ArrayList<StaffModel> subDepartmentList = new ArrayList<>();
    }

    public class EnterStructUserModel {
        public int count;
        public ArrayList<StaffModel> userList = new ArrayList<>();
    }

}
