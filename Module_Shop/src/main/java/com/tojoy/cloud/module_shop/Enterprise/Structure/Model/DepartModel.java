package com.tojoy.cloud.module_shop.Enterprise.Structure.Model;

import java.io.Serializable;

/**
 * 部门数据
 */

public class DepartModel implements Serializable {

    public boolean isChecked;
    public String name;
    public String parentId;
    public String id;
    public String corporationCode;
    public boolean existSubDept;

    public String code;
    public String companyCode;

}
