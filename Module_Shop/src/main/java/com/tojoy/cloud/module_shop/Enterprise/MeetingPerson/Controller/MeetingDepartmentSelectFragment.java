package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Controller;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.model.meetingperson.SingleTonModel.MeetingPersonTon;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Adapter.MeetingDepartmentSelectAdapter;
import com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Adapter.MeetingPersonSelectAdapter;
import com.tojoy.cloud.module_shop.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author liuhm
 * @date 2020/2/26
 * @desciption 选择参会部门
 */
public class MeetingDepartmentSelectFragment extends MeetingPersonBaseFragment implements MeetingDepartmentSelectAdapter.OnDeptCheckBoxListener, MeetingPersonSelectAdapter.OnPersonCheckBoxListener {

    public static final String DEPT_DATA = "DEPT_DATA";
    public static final String PERSON_DATA = "PERSON_DATA";
    private static final String CHECKED_PERSON = "CHECKED_PERSON";

    private RecyclerView tjRvDept, tjRvPerson;
    private MeetingDepartmentSelectAdapter mDeptAdapter;
    private MeetingPersonSelectAdapter mPersonAdapter;
    private List<MeetingPersonDepartmentResponse.DepartmentModel> mDepartments;
    private List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> mPersons;
    private List<String> mCheckedPerson;
    private List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> mCurDeptAllPerson;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_meeting_department_select, container, false);
        }
        return mView;
    }

    @Override
    protected void initUI() {
        super.initUI();
        tjRvDept = mView.findViewById(R.id.tjRvDept);
        tjRvPerson = mView.findViewById(R.id.tjRvPerson);
    }

    public static MeetingDepartmentSelectFragment getInstance(Serializable serDept, Serializable serPerson, ArrayList<String> checkedList, String curTitleDepartmentId) {
        MeetingDepartmentSelectFragment fragment = new MeetingDepartmentSelectFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(DEPT_DATA, serDept);
        bundle.putSerializable(PERSON_DATA, serPerson);
        bundle.putStringArrayList(CHECKED_PERSON, checkedList);
        bundle.putString("curTitleDepartmentId", curTitleDepartmentId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mDepartments = (List<MeetingPersonDepartmentResponse.DepartmentModel>) getArguments().getSerializable(DEPT_DATA);
            mPersons = (List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel>) getArguments().getSerializable(PERSON_DATA);
            mCheckedPerson = getArguments().getStringArrayList(CHECKED_PERSON);
            mCurDeptAllPerson = new ArrayList<>();
            mCurDeptAllPerson.addAll(mPersons);
            recCurDeptAllPerson(mDepartments);
            mCurDeptAllPerson = removeDuplicateEmployee();
            setCheckedPersonStatus();
            setCheckedDeptStatus();
        }
    }

    @Override
    protected void initData() {
        super.initData();
        if (mDepartments != null) {
            mDeptAdapter = new MeetingDepartmentSelectAdapter(getActivity(), mDepartments);
            mDeptAdapter.setOnCheckListener(this);
            tjRvDept.setLayoutManager(new LinearLayoutManager(getActivity()));
            tjRvDept.setAdapter(mDeptAdapter);
            mDeptAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
                if (!FastClickAvoidUtil.isDoubleClick()) {
                    if (getActivity() instanceof MeetingPersonSelectAct) {
                        if (data.corDepartmentList != null && !data.corDepartmentList.isEmpty()) {
                            ((MeetingPersonSelectAct) getActivity()).addDepartmentFragment(data.corDepartmentList, data.respUserForDepartmentDtoList, data.corporationId);
                        } else {
                            ((MeetingPersonSelectAct) getActivity()).addPersonFragment(data.respUserForDepartmentDtoList, data.id);
                        }
                    }
                }
            });
        }

        if (mPersons != null) {
            mPersonAdapter = new MeetingPersonSelectAdapter(getActivity(), mPersons);
            mPersonAdapter.setOnCheckListener(this);
            tjRvPerson.setLayoutManager(new LinearLayoutManager(getActivity()));
            tjRvPerson.setAdapter(mPersonAdapter);
            mPersonAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
                if (!FastClickAvoidUtil.isDoubleClick()) {
                    CheckBox checkBox = v.findViewById(R.id.cb);
                    if (checkBox.isChecked()) {
                        checkBox.setChecked(false);
                    } else {
                        checkBox.setChecked(true);
                    }
                    onPersonCheckListener(mPersons.get(position), checkBox.isChecked());
                }
            });
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tjRvDept.setNestedScrollingEnabled(false);
            tjRvPerson.setNestedScrollingEnabled(false);
        }

        updateActBottomCheckAllStatus();
    }

    /**
     * 回退时，更新上一Fragment数据
     */
    public void setPopBackListener(List<String> checkedPerson) {
        if (mCheckedPerson != null) {
            mCheckedPerson.clear();
            mCheckedPerson.addAll(checkedPerson);
        }
        setCheckedPersonStatus();
        setCheckedDeptStatus();
        tjRvDept.post(() -> {
            if (mDeptAdapter != null) {
                mDeptAdapter.notifyDataSetChanged();
            }
        });
        tjRvPerson.post(() -> {
            if (mPersonAdapter != null) {
                mPersonAdapter.notifyDataSetChanged();
            }
        });
        updateActBottomCheckAllStatus();
    }

    /**
     * 去重
     *
     * @return
     */
    private List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> removeDuplicateEmployee() {
        Set<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> set = new TreeSet<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel>((a, b) -> {
            // 字符串则按照asicc码升序排列
            return a.userId.compareTo(b.userId);
        });
        set.addAll(mCurDeptAllPerson);
        return new ArrayList<>(set);
    }

    /**
     * 全选时，状态变化
     *
     * @param isChecked
     */
    public void notifyDataChange(boolean isChecked) {
        if (isChecked) {
            //部门架构内人数
            int inSindeCount = 0;
            inSindeCount = MeetingPersonTon.getInstance().recListDeptEmployee(inSindeCount, mDepartments) + mPersons.size();
            //部门下已选的人数包含部门+员工
            int departSelectCount = 0;
            //遍历部门下所有被选中的人
            for (int i = 0; i < mDepartments.size(); i++) {
                departSelectCount += MeetingPersonTon.getInstance().recInitAllCheckedPersonCountMap(new HashMap<String, String>(), mDepartments.get(i)).size();
            }
            for (int k = 0; k < mPersons.size(); k++) {
                if (mPersons.get(k).isChecked) {
                    departSelectCount++;
                }
            }
            //********************************************************

            //当前总共邀请人数
            int totalInvitatePerson = ((MeetingPersonSelectAct) getActivity()).getmAllCheckedPersonCountMap().size();
            //内部会议 5。25 5点修改将这块隐藏，因为直接获取了当前选择移除的size了，就不在计算
            //邀请人最大数限制
            if (totalInvitatePerson - departSelectCount + inSindeCount > MeetingPersonTon.getInstance().maxInvitation) {
                ((MeetingPersonSelectAct) getActivity()).updateBottomCheckAllStatus(false);
                ((MeetingPersonSelectAct) getActivity()).showMaxPerson();
                return;
            }
        }

        if (mDepartments != null && !mDepartments.isEmpty()) {
            for (MeetingPersonDepartmentResponse.DepartmentModel departmentModel : mDepartments) {
                MeetingPersonTon.getInstance().recSetDepartmentPseronStatus(isChecked, departmentModel);
                departmentModel.isChecked = isChecked;
                ((MeetingPersonSelectAct) getActivity()).updateDeptCheckStatus(departmentModel, isChecked);
            }
            tjRvDept.post(() -> {
                if (mDeptAdapter != null) {
                    mDeptAdapter.notifyDataSetChanged();
                }
            });
        }
        if (mPersons != null && !mPersons.isEmpty()) {
            for (MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel : mPersons) {
                employeeModel.isChecked = isChecked;
                //设置个人的选中状态
                MeetingPersonTon.getInstance().recSetDepartItemPersonStatus(isChecked, employeeModel);
                ((MeetingPersonSelectAct) getActivity()).updatePersonCheckStatus(employeeModel, isChecked);
            }
            tjRvPerson.post(() -> {
                if (mPersonAdapter != null) {
                    mPersonAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    /**
     * 点击部门选项
     */
    @Override
    public void onDeptCheckListener(MeetingPersonDepartmentResponse.DepartmentModel data, boolean isChecked) {
        if (getActivity() instanceof MeetingPersonSelectAct) {
            if (isChecked) {
                //判断最大限制规则：（当前已选人数-  部门内所有已选人数）+部门总共人数<MaxPerson
                //当前已选人数
                int currentSelectCount = ((MeetingPersonSelectAct) getActivity()).getmAllCheckedPersonCountMap().size();
                //部门内所有已选人数
                MeetingPersonTon.getInstance().recCurDepartSelectPersonCount = 0;
                int currentDepartSelectCount = MeetingPersonTon.getInstance().recCurDepartSelectPerson(data);
                int currentDepartTotalCount = MeetingPersonTon.getInstance().recItemDeptEmployee(new ArrayList<>(), data).size();
                if ((currentSelectCount - currentDepartSelectCount + currentDepartTotalCount) > MeetingPersonTon.getInstance().maxInvitation) {
                    data.isChecked = false;
                    ((MeetingPersonSelectAct) getActivity()).showMaxPerson();
                    MeetingPersonTon.getInstance().recSetDepartmentPseronStatus(false, data);
                    tjRvDept.post(() -> {
                        if (mDeptAdapter != null) {
                            mDeptAdapter.notifyDataSetChanged();
                        }
                    });
                    return;
                }
            }
            data.isChecked = isChecked;
            MeetingPersonTon.getInstance().recSetDepartmentPseronStatus(isChecked, data);
            ((MeetingPersonSelectAct) getActivity()).updateDeptCheckStatus(data, isChecked);
        }
    }

    /**
     * 点击员工选项
     */
    @Override
    public void onPersonCheckListener(MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel data, boolean isChecked) {
        if (getActivity() instanceof MeetingPersonSelectAct) {
            if (isChecked) {
                // 选中单个的时候判断是否超出添加人数最大限制
                if (!((MeetingPersonSelectAct) getActivity()).checkMaxPerson(1, true)) {
                    data.isChecked = false;
                    mPersonAdapter.notifyDataSetChanged();
                    return;
                }
            }
            data.isChecked = isChecked;
            ((MeetingPersonSelectAct) getActivity()).updatePersonCheckStatus(data, isChecked);
        }
    }

    /**
     * 递归当前部门所有人
     */
    private void recCurDeptAllPerson(List<MeetingPersonDepartmentResponse.DepartmentModel> departmentModels) {
        if (departmentModels != null && !departmentModels.isEmpty()) {
            for (int i = 0; i < departmentModels.size(); i++) {
                mCurDeptAllPerson.addAll(departmentModels.get(i).respUserForDepartmentDtoList);
                recCurDeptAllPerson(departmentModels.get(i).corDepartmentList);
            }
        }
    }

    /**
     * 设置默认选中状态-部门
     */
    private void setCheckedDeptStatus() {
        if (mDepartments != null && !mDepartments.isEmpty()) {
            // 设置选中状态显示，递归遍历如果总人数，与已选择人数一致则 isChecked=true
            for (MeetingPersonDepartmentResponse.DepartmentModel departmentModel : mDepartments) {
                List<String> list = new ArrayList<>();
                boolean isChecked = true;
                if (departmentModel != null) {
                    List<String> lis = new ArrayList<>();
                    lis = MeetingPersonTon.getInstance().recItemDeptEmployee(lis, departmentModel);
                    if (departmentModel.respUserForDepartmentDtoList.size() + lis.size() > 0) {
                        int total = MeetingPersonTon.getInstance().recItemDeptEmployee(list, departmentModel).size();
                        List<String> selectList = new ArrayList<>();
                        int selecttotal = MeetingPersonTon.getInstance().recItemSelcetPersonEmployee(selectList, departmentModel).size();
                        isChecked = total == selecttotal;
                    } else {
                        isChecked = false;
                    }
                    departmentModel.isChecked = isChecked;
                }
            }
        }
    }

    /**
     * 设置默认选中状态-人员
     */
    private void setCheckedPersonStatus() {

    }

    /**
     * 用户点击了所有Checkbox
     */
    public void onItemCheckedAllListener(List<String> checkedPerson) {
        if (mCheckedPerson != null) {
            mCheckedPerson.clear();
            mCheckedPerson.addAll(checkedPerson);
        }
        updateActBottomCheckAllStatus();
    }

    public void notifyDataChange() {
        setCheckedPersonStatus();
        setCheckedDeptStatus();

        tjRvDept.post(() -> {
            if (mDeptAdapter != null) {
                mDeptAdapter.notifyDataSetChanged();
            }
        });
        tjRvPerson.post(() -> {
            if (mPersonAdapter != null) {
                mPersonAdapter.notifyDataSetChanged();
            }
        });
    }

    /**
     * 更新Activity底部checkbox选中状态
     */
    public void updateActBottomCheckAllStatus() {

        //返回false时标示未全部选中
        boolean isChecked = true;
        if (getActivity() instanceof MeetingPersonSelectAct) {
            //员工和部门都是空
            if (mDepartments == null || mDepartments.size() == 0) {
                isChecked = false;
            } else {
                for (int i = 0; i < mDepartments.size(); i++) {
                    List<String> lis = new ArrayList<>();
                    lis = MeetingPersonTon.getInstance().recItemDeptEmployee(lis, mDepartments.get(i));
                    //部门是空的情况
                    if ((mDepartments.get(i).respUserForDepartmentDtoList.size() + lis.size()) != 0) {
                        if (!mDepartments.get(i).isChecked) {
                            isChecked = false;
                        }
                    }
                }
            }

            if (mPersons == null || mPersons.size() == 0) {
                isChecked = false;
            } else {
                for (int i = 0; i < mPersons.size(); i++) {
                    if (!mPersons.get(i).isChecked) {
                        isChecked = false;
                    }
                }
            }
            //更新底部按钮
            ((MeetingPersonSelectAct) getActivity()).updateBottomCheckAllStatus(isChecked);
        }
    }

}
