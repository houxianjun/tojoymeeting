package com.tojoy.cloud.module_shop.Order.OrderSegment.Controller;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.tojoy.tjoybaselib.model.enterprise.CorporationMsgResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.net.NetWorkUtils;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.cloud.module_shop.Api.ShopModuleApiProvider;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Controller.NewOrderDetailAct;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.OrderChangeEvent;
import com.tojoy.cloud.module_shop.Order.OrderSegment.Model.OrderManagementModel;
import com.tojoy.cloud.module_shop.Order.OrderSegment.Model.OrderManagementResponse;
import com.tojoy.cloud.module_shop.Order.OrderSegment.adapter.OrderManagerListAdapter;
import com.tojoy.cloud.module_shop.Order.utils.OrderConstants;
import com.tojoy.cloud.module_shop.R;
import com.tojoy.common.App.BaseFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import rx.Observer;

import static com.tojoy.cloud.module_shop.Order.utils.OrderConstants.ORDER_STATUS_ALL;
import static com.tojoy.cloud.module_shop.Order.utils.OrderConstants.ORDER_STATUS_DONE;
import static com.tojoy.cloud.module_shop.Order.utils.OrderConstants.ORDER_STATUS_WAIT_EMAIL;
import static com.tojoy.cloud.module_shop.Order.utils.OrderConstants.ORDER_STATUS_WAIT_PAY;
import static com.tojoy.cloud.module_shop.Order.utils.OrderConstants.ORDER_STATUS_WAIT_RECEIVING;


/**
 * @author yongchaowang
 */
@SuppressLint("ValidFragment")
public class OrderManagementListFragment extends BaseFragment {
    private Context mContext;
    private TJRecyclerView mRecyclerView;
    private OrderManagerListAdapter mOrderAdapter;
    private int mOrderType;
    private int mPageNum = 1;
    private List<OrderManagementModel> mOrderList = new ArrayList<>();

    public OrderManagementListFragment(Context context, int type) {
        this.mOrderType = type;
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_order_list, container, false);
        }
        isCreateView = true;
        return mView;
    }

    @Override
    protected void lazyLoad() {
        super.lazyLoad();
        requestDataList(false);
    }

    @Override
    protected void initUI() {
        super.initUI();
        RelativeLayout mContainerLayout = mView.findViewById(R.id.container_layout);
        mRecyclerView = new TJRecyclerView(getContext());
        mContainerLayout.addView(mRecyclerView);
        mRecyclerView.setLoadMoreView();
        mRecyclerView.goneFooterView();
        mOrderAdapter = new OrderManagerListAdapter(mOrderList);
        mRecyclerView.setAdapter(mOrderAdapter);
        mRecyclerView.autoRefresh();
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPageNum = 1;
                requestDataList(false);
            }

            @Override
            public void onLoadMore() {
                mPageNum++;
                requestDataList(false);
            }
        });
        mOrderAdapter.setOnItemClickListener((v, position, data) -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                NewOrderDetailAct.start(mContext, mOrderList.get(data).getId(),mOrderList.get(data).getOrderType());
            }

        });
        mOrderAdapter.setOnItemChildClickListener((adapter, view, position) -> {
            if (FastClickAvoidUtil.isFastDoubleClick(view.getId())) {
                return;
            }
            if (view.getId() == R.id.order_cancel_tv) {

                showCancelDialog(mOrderList.get(position).getId());
            } else if (view.getId() == R.id.order_pay_tv) {
                RouterJumpUtil.startOrderPayDetailAct(mOrderList.get(position).getId(),
                        mOrderList.get(position).getOrderCode(),
                        mOrderList.get(position).getOrderPrice(),
                        "");
            } else if (view.getId() == R.id.tv_order_delete) {
                showDeleteDialog(mOrderList.get(position).getId());
            } else if (view.getId() == R.id.img_shop || view.getId() == R.id.tvCommdityOrderTitle || view.getId() == R.id.rlv_image_shop_in) {
                chatCanJump(mOrderList.get(position).getEnterpriseId());
            }
        });
        mRecyclerView.setEmptyImgAndTip(R.drawable.icon_empty_order, "暂无订单");
    }

    private void showDeleteDialog(String mOrderId) {
        TJMakeSureDialog dialog = new TJMakeSureDialog(getActivity(), v1 -> deleteOrder(mOrderId)).setTitleAndCotent("删除订单", getSourceString(R.string.order_cdelete_true)).hideTitle();
        dialog.show();
    }

    private void showCancelDialog(String mOrderId) {
        TJMakeSureDialog dialog = new TJMakeSureDialog(getActivity(), v1 -> cancelOrder(mOrderId)).setTitleAndCotent("取消订单", getSourceString(R.string.order_cancel_true)).hideTitle();
        dialog.show();
    }

    private String getSourceString(int id) {
        return getResources().getString(id);
    }

    /**
     * 取消订单
     */
    private void cancelOrder(String orderSn) {
        showProgressHUD(getActivity(), "处理中...");
        ShopModuleApiProvider.getInstance().cancelOrder(orderSn, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {
                dismissProgressHUD();
                showMessage(e.getMessage());
            }

            @Override
            public void onNext(OMBaseResponse response) {
                if (response.isSuccess()) {
                    EventBus.getDefault().post(new OrderChangeEvent(OrderConstants.PAY_STATUS_CANCEL));
                }
                showMessage(response.msg);
            }
        });

    }

    @Override
    protected void initData() {
        super.initData();
    }

    private void requestDataList(boolean isRefresh) {
        if (!NetWorkUtils.isNetworkConnected(getActivity())) {
            showMessage("网络断开，请检查网络");
            return;
        }
        mPageNum = isRefresh ? 1 : mPageNum;
        ShopModuleApiProvider.getInstance().mOrderManagementList(BaseUserInfoCache.getUserId(getActivity()), mPageNum, null, mOrderType, new Observer<OrderManagementResponse>() {
            @Override
            public void onCompleted() {
                mRecyclerView.setRefreshing(false);
            }

            @Override
            public void onError(Throwable e) {
                mRecyclerView.setRefreshing(false);
            }

            @Override
            public void onNext(OrderManagementResponse orderListResponse) {
                if (orderListResponse.isSuccess()) {
                    if (mPageNum == 1) {
                        mOrderList.clear();
                        mOrderList.addAll(orderListResponse.data.records);
                        mOrderAdapter.setNewData(mOrderList);
                    } else {
                        mOrderList.addAll(orderListResponse.data.records);
                        mOrderAdapter.notifyDataSetChanged();
                    }
                    mRecyclerView.showFooterView();
                    mRecyclerView.refreshLoadMoreView(mPageNum, orderListResponse.data.records.size());
                } else {
                    showMessage(orderListResponse.msg);
                }
            }
        });
    }

    private void deleteOrder(String mOrderId) {
        if (!NetWorkUtils.isNetworkConnected(getActivity())) {
            showMessage("网络断开，请检查网络");
            return;
        }
        showProgressHUD(getActivity(), "处理中...");
        ShopModuleApiProvider.getInstance().mOrderDelete(mOrderId, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {
                showMessage(e.getMessage());
                dismissProgressHUD();
            }

            @Override
            public void onNext(OMBaseResponse orderListResponse) {
                if (orderListResponse.isSuccess()) {
                    EventBus.getDefault().post(new OrderChangeEvent(OrderConstants.PAY_STATUS_CLOSE));
                }
                showMessage(orderListResponse.msg);
            }
        });

    }

    private void showMessage(String msg) {
        try {
            Toasty.normal(Objects.requireNonNull(getActivity()), msg).show();
        } catch (Exception e) {

        }

    }

    private void chatCanJump(String enterpriseId) {
        showProgressHUD(getActivity(), "");
        OMAppApiProvider.getInstance().mCorporationQuery(enterpriseId, false, new rx.Observer<CorporationMsgResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {
                dismissProgressHUD();
                Toasty.normal(getActivity(), "查询商家信息失败");
            }

            @Override
            public void onNext(CorporationMsgResponse response) {
                dismissProgressHUD();
                if (response.isSuccess()) {
                    if (response.data.customState == 1) {//开启
                        RouterJumpUtil.startEnterPrise(enterpriseId);
                    } else {
                        TJMakeSureDialog tjMakeSureDialog = new TJMakeSureDialog(getActivity()).setTitleAndCotent("提示", " 店铺已关闭，请联系卖家").setBtnSureText("确认");
                        tjMakeSureDialog.showAlert();
                        tjMakeSureDialog.setEvent(v -> tjMakeSureDialog.dismiss());
                    }
                } else {
                    showMessage(response.msg);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private boolean isOrderStatusWaitPay() {
        return mOrderType == ORDER_STATUS_WAIT_PAY;
    }

    private boolean isOrderStatusAll() {
        return mOrderType == ORDER_STATUS_ALL;
    }

    private boolean isOrderStatusWaitEmil() {
        return mOrderType == ORDER_STATUS_WAIT_EMAIL;
    }

    private boolean isOrderStatusWaitReceiving() {
        return mOrderType == ORDER_STATUS_WAIT_RECEIVING;
    }

    private boolean isOrderStatusDone() {
        return mOrderType == ORDER_STATUS_DONE;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderChange(OrderChangeEvent event) {
        switch (event.changeType) {
            case OrderConstants.PAY_STATUS_SUCCESS:
                if (isOrderStatusWaitPay() || isOrderStatusAll() || isOrderStatusWaitEmil()) {
                    requestDataList(true);
                }
                break;
            case OrderConstants.PAY_STATUS_CLOSE:
            case OrderConstants.PAY_STATUS_CANCEL:
                if (isOrderStatusWaitPay() || isOrderStatusAll()) {
                    requestDataList(true);
                }
                break;
            case OrderConstants.PAY_STATUS_CONFIRM:
                if (isOrderStatusWaitReceiving() || isOrderStatusDone() || isOrderStatusAll()) {
                    requestDataList(true);
                }
                break;
            default:
                requestDataList(true);
                break;
        }
    }
}
