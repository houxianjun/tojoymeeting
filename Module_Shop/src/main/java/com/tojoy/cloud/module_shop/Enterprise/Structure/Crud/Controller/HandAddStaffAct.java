package com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.editView.ClearEmojInputFilter;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.cloud.module_shop.Api.ShopModuleApiProvider;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Helper.SelectedContactHelper;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.DepartRespone;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.DepartStaffEvent;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.UploadStaffModel;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.View.UploadStaffDialog;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Model.DepartModel;
import com.tojoy.cloud.module_shop.R;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 手动新增员工 / 为员工设置信息
 * create by luzhu on 2020/2/26
 */

public class HandAddStaffAct extends UI {

    private EditText mEtName, mEtPhone;
    private TextView mTvDepartment;
    private TextView mTvPost;
    private String parentId;
    private String selectParentId;
    private String corporationCode;
    private String departsJsonStr;
    private final int Request_Code = 333;
    private String frontRole;
    private String backRole;
    private String departId;
    //角色id列表
    private ArrayList<Integer> roleIds = new ArrayList<>();
    private List<DepartModel> comData;
    private DepartModel comModel;
    private boolean isUpLoadTipsShow = false; //批量上传的时候出错是否已提示
    private int uploadSucsessTimes = 0; //批量上传的时候成功次数
    private int uploadNeedTimes; //批量上传全部成功需要的次数
    private int successCount; //批量上传每次的成功数
    private int failCount; //批量上传每次的失败数

    /**
     * @param type 1：手动新增员工  2：为员工设置信息
     */
    public static void start(Context context, String type, String corporationCode, String parentId, String departId) {
        Intent intent = new Intent();
        intent.putExtra("type", type);
        intent.putExtra("corporationCode", corporationCode);
        intent.putExtra("parentId", parentId);
        intent.putExtra("departId", departId);
        intent.setClass(context, HandAddStaffAct.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hand_add_staff);

        initTitle();
        initUI();
        getData();
    }

    private void getData() {
        parentId = getIntent().getStringExtra("parentId");
        departId = getIntent().getStringExtra("departId");
        corporationCode = getIntent().getStringExtra("corporationCode");

        comData = new ArrayList<>();
        comModel = new DepartModel();
        comModel.name = BaseUserInfoCache.getCompanyName(HandAddStaffAct.this);
        comModel.id = "";
        comModel.parentId = "";
        comModel.corporationCode = corporationCode;
        if (TextUtils.isEmpty(parentId) && TextUtils.isEmpty(departId)) {
            comModel.isChecked = true;
        }
        comData.add(comModel);
        //默认添加员工橘色
        roleIds.add(5);

        ShopModuleApiProvider.getInstance().getDepartments(parentId, new Observer<DepartRespone>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(DepartRespone respone) {
                if (respone.isSuccess()) {
                    List<DepartModel> data = respone.data;
                    for (int i = 0; i < data.size(); i++) {
                        DepartModel model = data.get(i);
                        if (!TextUtils.isEmpty(departId) && departId.equals(model.id)) {
                            mTvDepartment.setText(model.name);
                            selectParentId = model.code;
                        }
                    }
                }
            }
        });
    }

    private void initTitle() {
        setStatusBar();
        setUpNavigationBar();
        showBack();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (requestCode == AddDepartmentAct.Request_Code) {
                //部门回传
                corporationCode = data.getStringExtra("corporationCode");
                selectParentId = data.getStringExtra("parentId");
                mTvDepartment.setText(data.getStringExtra("departName"));
            } else if (requestCode == Request_Code) {
                //角色回传
                frontRole = data.getStringExtra("frontRole");
                backRole = data.getStringExtra("backRole");
                roleIds = (ArrayList<Integer>) data.getSerializableExtra("roleIds");
                String roleStr = data.getStringExtra("roleStr");
                mTvPost.setText(roleStr);
            }
        }
    }

    private void initUI() {
        RelativeLayout mNameLayout = (RelativeLayout) findViewById(R.id.nameLayout);
        mEtName = (EditText) findViewById(R.id.etName);
        RelativeLayout mPhoneLayout = (RelativeLayout) findViewById(R.id.phoneLayout);
        mEtPhone = (EditText) findViewById(R.id.etPhone);
        mTvDepartment = (TextView) findViewById(R.id.tvDepartment);
        mTvPost = (TextView) findViewById(R.id.tvPost);
        LinearLayout mBtnLayout = (LinearLayout) findViewById(R.id.btnLayout);
        TextView mTvSave1 = (TextView) findViewById(R.id.tvSave1);
        TextView mTvKeep = (TextView) findViewById(R.id.tvKeep);
        TextView mTvSave2 = (TextView) findViewById(R.id.tvSave2);
        String type = getIntent().getStringExtra("type");

        if ("2".equals(type)) {
            mNameLayout.setVisibility(View.GONE);
            mPhoneLayout.setVisibility(View.GONE);
            mBtnLayout.setVisibility(View.GONE);
            mTvSave2.setVisibility(View.VISIBLE);
            setTitle("为" + SelectedContactHelper.getInstance().getSelectedContactsCount() + "人设置员工信息");

            uploadNeedTimes = SelectedContactHelper.getInstance().getSelectedContactsCount() % SelectedContactHelper.getInstance().mPerPageCount == 0 ?
                    SelectedContactHelper.getInstance().getSelectedContactsCount() / SelectedContactHelper.getInstance().mPerPageCount :
                    SelectedContactHelper.getInstance().getSelectedContactsCount() / SelectedContactHelper.getInstance().mPerPageCount + 1;

        } else {
            setTitle("手动新增员工");
        }

        InputFilter[] emojiFilters = {new ClearEmojInputFilter()};
        mEtName.setFilters(emojiFilters);
        mEtPhone.setFilters(emojiFilters);

        mEtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String content = mEtName.getText().toString();
                if (content.length() > 10) {
                    Toasty.normal(HandAddStaffAct.this, "字数超过限制").show();
                    mEtName.setText(content.substring(0, 10));
                    mEtName.setSelection(10);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mEtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String content = mEtPhone.getText().toString();
                if (content.length() > 11) {
                    Toasty.normal(HandAddStaffAct.this, "字数超过限制").show();
                    mEtPhone.setText(content.substring(0, 11));
                    mEtPhone.setSelection(11);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mTvSave1.setOnClickListener(view -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }
            String name = mEtName.getText().toString().trim();
            String phone = mEtPhone.getText().toString().trim();
            if (phone.length() != 11) {
                Toasty.normal(HandAddStaffAct.this, "手机号格式不正确").show();
                return;
            }
            if (TextUtils.isEmpty(name)) {
                Toasty.normal(HandAddStaffAct.this, "请输入姓名").show();
                return;
            }
            if ("请选择部门".equals(mTvDepartment.getText().toString())) {
                Toasty.normal(HandAddStaffAct.this, "请选择部门").show();
                return;
            }

            addStaff(1);
        });

        mTvKeep.setOnClickListener(view -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }

            String name = mEtName.getText().toString().trim();
            String phone = mEtPhone.getText().toString().trim();
            if (phone.length() != 11) {
                Toasty.normal(HandAddStaffAct.this, "手机号格式不正确").show();
                return;
            }
            if (TextUtils.isEmpty(name)) {
                Toasty.normal(HandAddStaffAct.this, "请输入姓名").show();
                return;
            }
            if ("请选择部门".equals(mTvDepartment.getText().toString())) {
                Toasty.normal(HandAddStaffAct.this, "请选择部门").show();
                return;
            }
            addStaff(2);
        });

        mTvDepartment.setOnClickListener(view -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }
            showProgressHUD(HandAddStaffAct.this, "加载中");
            ShopModuleApiProvider.getInstance().getDepartments(parentId, new Observer<DepartRespone>() {
                @Override
                public void onCompleted() {
                    dismissProgressHUD();
                }

                @Override
                public void onError(Throwable e) {
                    dismissProgressHUD();
                    Toasty.normal(HandAddStaffAct.this, "网络错误，请检查网络").show();
                }

                @Override
                public void onNext(DepartRespone respone) {
                    if (respone.isSuccess()) {
                        List<DepartModel> data = respone.data;

                        comModel.existSubDept = !data.isEmpty();
                        departsJsonStr = new Gson().toJson(data);

                        for (int i = 0; i < data.size(); i++) {
                            DepartModel model = data.get(i);
                            if (!TextUtils.isEmpty(departId) && departId.equals(model.id)) {
                                mTvDepartment.setText(model.name);
                            }
                        }

                    } else {
                        comModel.existSubDept = false;
                        departsJsonStr = new Gson().toJson(comData);
                    }
                    Intent intent = new Intent(HandAddStaffAct.this, SelectDepartAct.class);
                    intent.putExtra("departsJsonStr", departsJsonStr);
                    intent.putExtra("type", type);
                    startActivityForResult(intent, AddDepartmentAct.Request_Code);
                }
            });
        });

        mTvPost.setOnClickListener(view -> {
            Intent intent = new Intent(HandAddStaffAct.this, SelectPostAct.class);
            //当前角色列表
            intent.putExtra("currentRoleIds", roleIds);
            startActivityForResult(intent, Request_Code);
        });

        mTvSave2.setOnClickListener(v -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }

            if ("请选择部门".equals(mTvDepartment.getText().toString())) {
                Toasty.normal(HandAddStaffAct.this, "请选择部门").show();
                return;
            }

            showProgressHUD(HandAddStaffAct.this, "加载中");
            if (TextUtils.isEmpty(frontRole)) {
                frontRole = "5";
            }
            if (TextUtils.isEmpty(backRole) || "0".equals(backRole)) {
                backRole = "";
            }

            successCount = 0;
            failCount = 0;
            uploadSucsessTimes = 0;
            int page = 1;
            while (page <= uploadNeedTimes) {
                ShopModuleApiProvider.getInstance().addStaffs(roleIds,
                        TextUtils.isEmpty(selectParentId) ? departId : selectParentId,
                        SelectedContactHelper.getInstance().getDataJsonByPage(page),
                        new Observer<UploadStaffModel>() {
                            @Override
                            public void onCompleted() {
                            }

                            @Override
                            public void onError(Throwable e) {
                                dismissProgressHUD();
                                if (!isUpLoadTipsShow) {
                                    Toasty.normal(HandAddStaffAct.this, "网络错误，请检查网络").show();
                                    isUpLoadTipsShow = true;
                                }
                            }

                            @Override
                            public void onNext(UploadStaffModel response) {
                                if (response.isSuccess()) {

                                    uploadSucsessTimes++;

                                    successCount += response.data.successCount;
                                    failCount += response.data.failCount;

                                    if (uploadSucsessTimes == uploadNeedTimes) {
                                        dismissProgressHUD();
                                        EventBus.getDefault().post(new DepartStaffEvent("1", TextUtils.isEmpty(selectParentId) ? departId : selectParentId));

                                        if (failCount > 0) {
                                            new UploadStaffDialog("成功" + successCount + "人，已存在" + failCount + "人", HandAddStaffAct.this).show();
                                        } else {
                                            Toasty.normal(HandAddStaffAct.this, "保存成功").show();
                                            finish();
                                        }
                                    }

                                } else {
                                    if (!isUpLoadTipsShow) {
                                        dismissProgressHUD();
                                        Toasty.normal(HandAddStaffAct.this, response.msg).show();
                                        isUpLoadTipsShow = true;
                                    }
                                }
                            }
                        });
                page++;
                if (isUpLoadTipsShow) {
                    break;
                }
            }
        });
    }

    private void addStaff(int type) {
        showProgressHUD(HandAddStaffAct.this, "加载中");

        ShopModuleApiProvider.getInstance().addStaff(mEtPhone.getText().toString(), mEtName.getText().toString(),

                TextUtils.isEmpty(selectParentId) ? departId : selectParentId, roleIds, new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismissProgressHUD();
                        Toasty.normal(HandAddStaffAct.this, "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        if (response.isSuccess()) {
                            Toasty.normal(HandAddStaffAct.this, "保存成功").show();

                            EventBus.getDefault().post(new DepartStaffEvent("1", TextUtils.isEmpty(selectParentId) ? departId : selectParentId));

                            if (type == 1) {
                                finish();
                            } else {
                                mEtName.setText("");
                                mEtPhone.setText("");
                                mTvDepartment.setText("请选择部门");
                                mTvPost.setText("员工");
                                selectParentId = "";
                            }
                        } else {
                            Toasty.normal(HandAddStaffAct.this, response.msg).show();
                        }
                    }
                });
    }
}