package com.tojoy.cloud.module_shop.Enterprise.Apply.Model;

import com.tojoy.tjoybaselib.net.BaseResponse;
import com.tojoy.common.Widgets.CommonParamsPop.CommonParam;

import java.util.ArrayList;

public class EnterParamsResponse extends BaseResponse {
    public ArrayList<CommonParam> data ;
}
