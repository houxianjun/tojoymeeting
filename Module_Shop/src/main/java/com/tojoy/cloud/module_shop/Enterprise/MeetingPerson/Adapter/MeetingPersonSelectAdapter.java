package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.cloud.module_shop.R;

import java.util.List;

/**
 * @author liuhm
 * @date 2020/2/26
 * @desciption 参会人Adapter
 */
public class MeetingPersonSelectAdapter extends BaseRecyclerViewAdapter<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> {

    public MeetingPersonSelectAdapter(@NonNull Context context, @NonNull List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_meeting_person;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new MeetingPersonSelectViewHolder(context, parent, getLayoutResId(viewType));
    }

    public class MeetingPersonSelectViewHolder extends BaseRecyclerViewHolder<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> {

        private TextView tvName, tvPhone;
        private CheckBox checkBox;


        MeetingPersonSelectViewHolder(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            checkBox = (CheckBox) findViewById(R.id.cb);
            tvName = (TextView) findViewById(R.id.tvName);
            tvPhone = (TextView) findViewById(R.id.tvPhone);
        }

        @Override
        public void onBindData(MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel data, int position) {
            checkBox.setChecked(data.isChecked);
            tvName.setText(data.trueName);
            tvPhone.setText(data.mobile);

            checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
                if(compoundButton.isPressed() && onCheckBoxListener != null){
                    onCheckBoxListener.onPersonCheckListener(data, b);
                }
            });
        }
    }

    private OnPersonCheckBoxListener onCheckBoxListener;

    public void setOnCheckListener(OnPersonCheckBoxListener checkListener) {
        this.onCheckBoxListener = checkListener;
    }

    public interface OnPersonCheckBoxListener {
        void onPersonCheckListener(MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel data, boolean isChecked);
    }
}

