package com.tojoy.cloud.module_shop.Api;

import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.cloud.module_shop.Enterprise.Apply.Model.EnterParamsResponse;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.DepartRespone;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.RoleResponse;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.UploadStaffModel;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Home.Model.EnterStructResponse;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Home.Model.EnterStructSearchResponse;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.NewOderDetailModel;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.OrderPayDetailTimeResponse;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.OrderPayDetailWXPayResponse;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.RechargeOrderStatusResponse;
import com.tojoy.cloud.module_shop.Order.OrderSegment.Model.OrderManagementResponse;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import rx.Observable;

public interface ShopModuleApiInterface {

    /**
     * 订单管理列表
     */
    @POST("/cloud-meeting-api/api/liveorder/customerorders")
    Observable<OrderManagementResponse> mOrderManagementList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 订单删除
     */
    @POST("/cloud-meeting-api/api/liveorder/deleteOrder")
    Observable<OMBaseResponse> mOrderDelete(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 确认订单
     */
    @POST("/cloud-meeting-api/api/liveorder/receiptOrder")
    Observable<OMBaseResponse> mComfirmOrder(@Header("sign") String sign, @Body RequestBody body);


    /**
     * 取消订单
     */
    @POST("/cloud-meeting-api/api/liveorder/cancelOrder")
    Observable<OMBaseResponse> mCancelOrder(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 删除订单
     */
    @POST("/cloud-meeting-api/api/liveorder/deleteOrder")
    Observable<OMBaseResponse> mDeleteOrder(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 订单详情
     */
    @POST("/cloud-meeting-api/api/livePay/wxPay/userAliWxPay")
    Observable<OrderPayDetailWXPayResponse> getOrderPayParams(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 订单倒计时详情
     */
    @POST("/cloud-meeting-api/api/livePay/wxPay/userPaySurplusTime")
    Observable<OrderPayDetailTimeResponse> orderTimeDetail(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 新订单详情接口
     */
    @POST("/cloud-meeting-api/api/liveorder/orderdetail")
    Observable<NewOderDetailModel> newOderDetail(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 新增部门
     */
    @POST("/cloud-meeting-upms/api/company/org/add")
    Observable<OMBaseResponse> addDepart(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取部门
     */
    @POST("/cloud-meeting-upms/api/company/org/list")
    Observable<DepartRespone> getDepartList(@Header("sign") String sign, @Body RequestBody body);


    /**
     * 获取企业规模
     */
    @POST("/cloud-meeting-api/api/corporation/join/apply/scales")
    Observable<EnterParamsResponse> enterSize(@Header("sign") String sign, @Body RequestBody body);


    /**
     * 获取企业职位
     */
    @POST("/cloud-meeting-api/api/corporation/join/apply/titles")
    Observable<EnterParamsResponse> enterPos(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 提交企业入驻申请
     */
    @POST("/cloud-meeting-api/api/corporation/join/apply/add")
    Observable<OMBaseResponse> submitApply(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 查询企业通讯录
     */
    @POST("/cloud-meeting-upms/api/company/enterprise/users")
    Observable<EnterStructResponse> getEnterpriseUsers(@Header("sign") String sign, @Body RequestBody body);


    /**
     * 查询子部门及用户
     */
    @POST("/cloud-meeting-upms/api/company/sub/orgemployee")
    Observable<EnterStructResponse> getSubDepartUsers(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 修改员工信息
     */
    @POST("/cloud-meeting-upms/api/company/employee/edit")
    Observable<OMBaseResponse> editStaffInfo(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 获取角色
     */
    @POST("/cloud-meeting-upms/api/company/role/list")
    Observable<RoleResponse> getRoleList(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 删除员工
     * 新接口：/cloud-meeting-upms/api/company/employee/delete
     */
    @POST("/cloud-meeting-api/api/userManager/deleteUser")
    Observable<OMBaseResponse> deleteStaff(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 新增员工
     */
    @POST("/cloud-meeting-upms/api/company/employee/add")
    Observable<OMBaseResponse> addStaff(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 批量新增员工
     */
    @POST("/cloud-meeting-upms/api/company/employee/import")
    Observable<UploadStaffModel> addStaffs(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 关键字搜索用户
     */
    @POST("/cloud-meeting-upms/api/company/employee/list")
    Observable<EnterStructSearchResponse> searchEnterStruct(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 会议服务管理：充值订单剩余有效时间
     */
    @POST("/cloud-meeting-api/api/thirdpath/pay/userPaySurplusTime")
    Observable<OrderPayDetailTimeResponse> rechargeUserPaySurplusTime(@Header("sign") String sign, @Body RequestBody body);

    /**
     * 会议服务管理：充值支付接口
     */
    @POST("/cloud-meeting-api/api/thirdpath/pay/platformAliWxPay")
    Observable<OrderPayDetailWXPayResponse> rechargePlatformAliWxPay(@Header("sign") String sign, @Body RequestBody body);

     /**
     * 会议服务管理：充值订单详情接口，返回订单状态
     */
    @POST("/cloud-meeting-api/api/platservices/orderStatus")
    Observable<RechargeOrderStatusResponse> rechargeOrderDetail(@Header("sign") String sign, @Body RequestBody body);
}
