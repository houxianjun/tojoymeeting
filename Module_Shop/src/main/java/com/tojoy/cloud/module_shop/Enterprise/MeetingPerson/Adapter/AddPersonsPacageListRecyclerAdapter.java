package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.ui.editView.ClearEmojInputFilter;
import com.tojoy.tjoybaselib.util.string.RegixUtil;
import com.tojoy.cloud.module_shop.R;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class AddPersonsPacageListRecyclerAdapter extends RecyclerView.Adapter<AddPersonsPacageListRecyclerAdapter.MyViewHolder> {
    //这两个静态变量来区分是normalView还是footView。
    private static final int NORMAL_VIEW = 0;
    private static final int FOOT_VIEW = 1;
    private Context context;
    private List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> datas;

    public AddPersonsPacageListRecyclerAdapter(Context context, List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> tmpdatas) {
        this.context = context;
        this.datas = tmpdatas;
    }

    public List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> getDatas() {
        return datas;
    }

    @Override
    public AddPersonsPacageListRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == FOOT_VIEW) {
            View inflate = View.inflate(context, R.layout.item_outside_footview, null);
            MyViewHolder myViewHolder = new MyViewHolder(inflate, viewType);
            myViewHolder.setIsRecyclable(false);

            return myViewHolder;
        } else {
            View inflate = View.inflate(context, R.layout.item_add_outside_person, null);
            MyViewHolder myViewHolder = new MyViewHolder(inflate, viewType);
            myViewHolder.setIsRecyclable(false);
            return myViewHolder;
        }
    }

    @Override
    public void onBindViewHolder(final AddPersonsPacageListRecyclerAdapter.MyViewHolder myViewHolder, int postion) {

        if (getItemViewType(postion) == NORMAL_VIEW) {
            //解决Recyclerview复用的问题
            myViewHolder.mNameEt.setText(this.datas.get(postion).trueName);
            myViewHolder.mPhoneEt.setText(this.datas.get(postion).mobile);
            addEdittextListener(myViewHolder.mNameEt, postion, this.datas.get(postion), "name");
            addEdittextListener(myViewHolder.mPhoneEt, postion, this.datas.get(postion), "moblie");
        } else {
            if (mOnClickListener != null) {
                myViewHolder.linearLayout.setOnClickListener(v -> {
                    int pos = myViewHolder.getLayoutPosition();
                    mOnClickListener.onItemClickListener(myViewHolder.itemView, pos);
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return datas.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1) {
            return FOOT_VIEW;
        }
        return NORMAL_VIEW;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout linearLayout;

        private EditText mNameEt;
        private EditText mPhoneEt;

        MyViewHolder(View itemView, int viewType) {
            super(itemView);
            if (viewType != FOOT_VIEW) {
                mNameEt = itemView.findViewById(R.id.ed_name);
                mPhoneEt = itemView.findViewById(R.id.ed_phone);
                InputFilter[] emojiFilters = {new ClearEmojInputFilter()};
                mNameEt.setFilters(emojiFilters);
            } else {
                linearLayout = itemView.findViewById(R.id.ll_add_persons);
            }
        }
    }

    //定义接口  条目点击事件  跟  长按条目点击事件
    public interface OnClickListenerr {
        void onItemClickListener(View view, int possition);

        void onLongItemClickListener(View view, int possition);
    }

    private OnClickListenerr mOnClickListener;

    public void setOnItemClinckListener(OnClickListenerr onClickListener) {
        this.mOnClickListener = onClickListener;
    }

    /**
     * 迭代列表集合，在添加时检查数据格式正确性
     * 规则：内容不能为空，手机号格式是否正确
     *
     * @return true 时可以添加， false 不能添加。说明数据输入有误
     */
    public boolean checkItemModel() {
        //是否通过检测
        boolean isCheck = datas != null && datas.size() > 0;
        //数据数量大于0且不为空
        if (isCheck) {
            //单挑数据
            if (datas.size() == 1) {
                //姓名非空，手机号为空
                if (!datas.get(0).trueName.trim().equals("") && datas.get(0).mobile.equals("")) {
                    Toasty.normal(context, "手机号或姓名不能为空").show();
                    isCheck = false;
                } else if (datas.get(0).trueName.trim().equals("") && datas.get(0).mobile.equals("")) {
                    //姓名收假后均为空
                    Toasty.normal(context, "手机号或姓名不能为空").show();
                    isCheck = false;
                } else if (datas.get(0).trueName.trim().equals("") && !datas.get(0).mobile.equals("")) {
                    Toasty.normal(context, "手机号或姓名不能为空").show();
                    isCheck = false;
                } else if (!datas.get(0).trueName.trim().equals("") && !RegixUtil.checkMobile(datas.get(0).mobile)) {
                    Toasty.normal(context, "手机号填写错误").show();
                    isCheck = false;
                }

            } else {
                //多条数据
                for (int i = 0; i < datas.size(); i++) {
                    //检测用户名和手机号是否已填写，都没有输入
                    if (datas.get(i).trueName.equals("") || datas.get(i).mobile.equals("")) {
                        //提示请检查输入内容格式是否正确
                        Toasty.normal(context, "手机号或姓名不能为空").show();

                        isCheck = false;
                        //退出整个循环
                        break;
                    }

                    if (!RegixUtil.checkMobile(datas.get(i).mobile)) {

                        Toasty.normal(context, "手机号填写错误").show();

                        isCheck = false;
                        //退出整个循环
                        break;
                    }
                }
            }
        }
        return isCheck;
    }

    public void setMdatas(MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel mdatas) {
        this.datas.add(mdatas);
        notifyItemRangeInserted(getItemCount() - 1, getItemCount());
    }

    /**
     * 输入框组件输入内容赋值
     *
     * @param editText
     * @param postion
     * @param employeeModel
     * @param tips
     */
    private void addEdittextListener(EditText editText, int postion, MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel, String tips) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if ("name".equals(tips)) {
                    if (" ".equals(s.toString())) {
                        editText.setText("");
                    }

                    int length = s.length();
                    if (length > 15) {
                        editText.setText(editText.getText().subSequence(0, 15));
                        editText.setSelection(15);
                        Toasty.normal(context, "已达最大字数限制").show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if ("name".equals(tips)) {
                    employeeModel.trueName = s.toString().trim();
                } else {
                    employeeModel.mobile = s.toString();
                    employeeModel.userId = s.toString();
                }
            }
        });
    }
}

