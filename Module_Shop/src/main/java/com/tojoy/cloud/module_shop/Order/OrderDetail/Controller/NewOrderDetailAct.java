package com.tojoy.cloud.module_shop.Order.OrderDetail.Controller;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.enterprise.CorporationMsgResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.permission.annotation.OnMPermissionGranted;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.router.RouterConstants;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.time.CountDownTextView;
import com.tojoy.tjoybaselib.util.time.DateTimeUtil;
import com.tojoy.cloud.module_shop.Api.ShopModuleApiProvider;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.NewOderDetailModel;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.OrderChangeEvent;
import com.tojoy.cloud.module_shop.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import rx.Observer;

import static com.tojoy.tjoybaselib.util.router.RouterJumpUtil.SKIP_FROM_DETAIL;


@Route(path = RouterPathProvider.Pay_OrderDetail)
public class NewOrderDetailAct extends UI {
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ImageView mImageGoodsCover;
    private TextView mTvEmailName;
    private TextView mTvEmailNumber;
    private TextView mTvAddressName;
    private TextView mTvAddressPhone;
    private TextView mTvAddress;
    private TextView mTvShopName;
    private TextView mTvGoodsName;
    private TextView mTvGoodsCount;
    private TextView mTvGoodsPrice;
    private TextView mTvRealyNeedPay;
    private TextView mTvOrderAccount;
    private TextView mTvShopPhone;
    private TextView mTvPiceHeJi;
    private RelativeLayout mRlShopPhone;
    private RelativeLayout mRlvEmail;
    private RelativeLayout mRlGoodsDetail;

    private String orderId;

    private NewOderDetailModel mNewOderDetailModel;
    private int mOrderType;

    public static void start(Context context, String orderId, int orderType) {
        Intent intent = new Intent();
        intent.setClass(context, NewOrderDetailAct.class);
        intent.putExtra("orderId", orderId);
        intent.putExtra(RouterConstants.ORDER_TYPE, orderType);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order_detail);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        setStatusBar();
        initUI();
        try {
            orderId = getIntent().getStringExtra("orderId");
            mOrderType = getIntent().getIntExtra(RouterConstants.ORDER_TYPE, RouterConstants.ORDER_TYPE_DEFAULT);
        } catch (Exception e) {
        }
        getData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initUI() {
        initHeadStatusView();
        setUpNavigationBar();
        showBack();
        setTitle(getResources().getText(R.string.order_detail_act_title));
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiprefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.color_0f88eb);
        mTvPiceHeJi = (TextView) findViewById(R.id.tvCommodityTotal);
        mRlvEmail = (RelativeLayout) findViewById(R.id.rlv_orde_oddnumber_info);
        mTvEmailName = (TextView) findViewById(R.id.tv_odd_company);
        mTvEmailNumber = (TextView) findViewById(R.id.tv_odd_number);
        mTvAddressName = (TextView) findViewById(R.id.tvBuyerName);
        mTvAddressPhone = (TextView) findViewById(R.id.tvBuyerPhoneNum);
        mTvAddress = (TextView) findViewById(R.id.tvBuyerAdress);
        mTvShopName = (TextView) findViewById(R.id.tvCommdityOrderTitle);
        mImageGoodsCover = (ImageView) findViewById(R.id.ivCommdityIcon);
        mTvGoodsName = (TextView) findViewById(R.id.tvCommodityName);
        mTvGoodsCount = (TextView) findViewById(R.id.tvCommodityNum);
        mTvGoodsPrice = (TextView) findViewById(R.id.tvCommodityReactPrice);
        mTvRealyNeedPay = (TextView) findViewById(R.id.tv_need_pay);
        mTvOrderAccount = (TextView) findViewById(R.id.odd_auucont);
        mTvShopPhone = (TextView) findViewById(R.id.tvShopPhone);
        mRlShopPhone = (RelativeLayout) findViewById(R.id.rlShopPhone);
        mRlGoodsDetail = (RelativeLayout) findViewById(R.id.rlGoodsDetail);
        initListener();
    }

    private void initListener() {
        mSwipeRefreshLayout.setEnabled(true);
        mSwipeRefreshLayout.setOnRefreshListener(this::getData);

        mTvShopName.setOnClickListener(view -> checkCanJump());

        mRlGoodsDetail.setOnClickListener(view -> {
            if (FastClickAvoidUtil.isFastDoubleClick(mRlGoodsDetail.getId())) {
                return;
            }
            if (mNewOderDetailModel != null && mNewOderDetailModel.data != null) {
                RouterJumpUtil.startGoodsDetails(mNewOderDetailModel.data.goodsId);
            }
        });

        mTvShopPhone.setOnClickListener(view -> callSectoryPhone());
        mShopHeadCover.setOnClickListener(v -> checkCanJump());

    }

    private void getData() {
        ShopModuleApiProvider.getInstance().getNewOrderDetail(orderId, mOrderType, new Observer<NewOderDetailModel>() {
            @Override
            public void onCompleted() {
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onError(Throwable e) {
                mSwipeRefreshLayout.setRefreshing(false);
                Toasty.normal(NewOrderDetailAct.this, "网络错误，请检查网络").show();
            }

            @Override
            public void onNext(NewOderDetailModel oderDetailModel) {
                if (oderDetailModel.isSuccess()) {
                    mNewOderDetailModel = oderDetailModel;
                    if (mNewOderDetailModel.data == null) {
                        return;
                    }
                    setOderHeadStatus();
                    setAddresseeInfo();
                    setMailInfo();
                    setGoodsInfo();
                    setPiceDetails();
                    setOderBottomStatus();
                    setOderAccountInfo();
                } else {
                    Toasty.normal(NewOrderDetailAct.this, oderDetailModel.msg).show();
                }

            }
        });
    }

    private void checkCanJump() {
        if (FastClickAvoidUtil.isDoubleClick()) {
            return;
        }
        if (mNewOderDetailModel == null || mNewOderDetailModel.data == null) {
            return;
        }
        showProgressHUD(this, "");
        OMAppApiProvider.getInstance().mCorporationQuery(mNewOderDetailModel.data.enterpriseId, false, new rx.Observer<CorporationMsgResponse>() {
            @Override
            public void onCompleted() {
            }

            @SuppressLint("CheckResult")
            @Override
            public void onError(Throwable e) {
                dismissProgressHUD();
                Toasty.normal(NewOrderDetailAct.this, "查询商家信息失败");
            }

            @Override
            public void onNext(CorporationMsgResponse response) {
                dismissProgressHUD();
                if (response.isSuccess()) {
                    if (response.data.customState == 1) {
                        //开启
                        RouterJumpUtil.startEnterPrise(mNewOderDetailModel.data.enterpriseId);
                    } else {
                        TJMakeSureDialog tjMakeSureDialog = new TJMakeSureDialog(NewOrderDetailAct.this).setTitleAndCotent("提示", " 店铺已关闭，请联系卖家").setBtnSureText("确认");
                        tjMakeSureDialog.showAlert();
                        tjMakeSureDialog.setEvent(v -> tjMakeSureDialog.dismiss());
                    }
                } else {
                    Toasty.normal(NewOrderDetailAct.this, response.msg).show();
                }
            }
        });
    }

    public void callSectoryPhone() {
        checkPersimiss(PERMISSIONS_MAKECALL, PERMISSIONS_MAKECALL_REQUEST_CODE);
    }

    //拨打电话
    @OnMPermissionGranted(PERMISSIONS_MAKECALL_REQUEST_CODE)
    public void onBasicPermissionSuccessMobile() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mNewOderDetailModel.data.storeBusiContactPhone.replaceAll("-", "")));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private ImageView mOrderWaitPayIv;
    private TextView mOderPayWaitTitleTv;
    private TextView mOrderPriceTipsTV;
    //支付订单状态提示
    private TextView mOrderPayTipsTv;
//    private RelativeLayout mRlvOderClose;
//    private RelativeLayout mRlvOderFinsh;
//    private RelativeLayout mRlvOderSend;
//    private RelativeLayout mRlvOderWaitSend;
    private CountDownTextView mTvOderPayTimeLate;
    private RelativeLayout mRlvOderWaitpay;
    private RelativeLayout mRlvAddress;
    private RelativeLayout mAddressNoSupper;
    private TextView mTvEmailNumberCopy, mTvOderAccountCopy;
    private ImageView mShopHeadCover;
    private EditText mEdTextOderReMaks;
    private TextView mTvGoodsNumberAll;
    private LinearLayout mRlvOderPiceDetails;
    private TextView mTvGoodsPriceAll, mTvEmailPice, mTvOderPiceAll;
    private TextView mTvOderLiuShuiAccount, mTvOderCreadTime, mTvOderPayTime, mTvOderSendGoodsTime, mTvOderFinshTime;
    private LinearLayout mOderAcitivityButtomLayout;
    private TextView mTvButtomCancle;
    private TextView mTvPayTitle, mTvPay;
    private RelativeLayout mRlvCancle;
    private RelativeLayout mRlvPay;
    private RelativeLayout mRlvEditTip;


    private void initHeadStatusView() {
        mOrderWaitPayIv = (ImageView) findViewById(R.id.image_oder_waitpay);
        mOderPayWaitTitleTv = (TextView) findViewById(R.id.tv_oder_pay_wait_title);
        mOrderPriceTipsTV = (TextView) findViewById(R.id.tv_order_pice_tip);
        mOrderPayTipsTv = (TextView) findViewById(R.id.tv_oder_pay_tips);
//        mRlvOderClose = (RelativeLayout) findViewById(R.id.rlv_oder_close);
//        mRlvOderFinsh = (RelativeLayout) findViewById(R.id.rlv_oder_finsh);
//        mRlvOderSend = (RelativeLayout) findViewById(R.id.rlv_oder_send);
//        mRlvOderWaitSend = (RelativeLayout) findViewById(R.id.rlv_oder_wait_send);
        mTvOderPayTimeLate = (CountDownTextView) findViewById(R.id.tv_oder_pay_time);
        mRlvOderWaitpay = (RelativeLayout) findViewById(R.id.rlv_oder_waitpay);

        mRlvAddress = (RelativeLayout) findViewById(R.id.rlv_oder_address);
        mAddressNoSupper = (RelativeLayout) findViewById(R.id.rlv_no_supper_address);
        mTvEmailNumberCopy = (TextView) findViewById(R.id.tv_odd_copy_number);
        mShopHeadCover = (ImageView) findViewById(R.id.img_shop);
        mEdTextOderReMaks = (EditText) findViewById(R.id.edit_remarks);
        mTvGoodsNumberAll = (TextView) findViewById(R.id.tvTotoalNum);
        mRlvOderPiceDetails = (LinearLayout) findViewById(R.id.rlv_oder_pice_detail);
        mTvGoodsPriceAll = (TextView) findViewById(R.id.tv_goods_all_pice);
        mTvEmailPice = (TextView) findViewById(R.id.tv_odd_pice);
        mTvOderPiceAll = (TextView) findViewById(R.id.tv_oder_all_pice);
        mTvOderAccountCopy = (TextView) findViewById(R.id.tv_odd_copy_account);
        mTvOderLiuShuiAccount = (TextView) findViewById(R.id.odd_auucont_liushui);
        mTvOderCreadTime = (TextView) findViewById(R.id.odd_auucont_cread_time);
        mTvOderPayTime = (TextView) findViewById(R.id.odd_pay_time);
        mTvOderSendGoodsTime = (TextView) findViewById(R.id.odd_send_good_time);
        mTvOderFinshTime = (TextView) findViewById(R.id.odd_finsh_time);
        mOderAcitivityButtomLayout = (LinearLayout) findViewById(R.id.rlBottomInfo);
        mTvButtomCancle = (TextView) findViewById(R.id.rlv_oder_details_gary_but);
        mTvPayTitle = (TextView) findViewById(R.id.tv_sum_title2);
        mTvPay = (TextView) findViewById(R.id.tv_order_pay);
        mRlvCancle = (RelativeLayout) findViewById(R.id.rlv_oder_details_gray_but);
        mRlvPay = (RelativeLayout) findViewById(R.id.rlv_oder_details_red_but);
        mRlvEditTip = (RelativeLayout) findViewById(R.id.rlv_oder_tip);

    }

    /**
     * 设置订单详情头部显示状态
     * <p>
     * (10：待付款、20：待发货、30：待收货、40：已收货、50：已完成、60：已取消、70：已超时）
     */
    private void setOderHeadStatus() {

            switch (mNewOderDetailModel.data.orderStatus) {
            case "10":
                mOderPayWaitTitleTv.setText("等待买家付款");
                mOrderWaitPayIv.setBackgroundResource(R.drawable.order_head_payment);
                break;
            case "20":
                mOderPayWaitTitleTv.setText("等待卖家发货");
                mOrderWaitPayIv.setBackgroundResource(R.drawable.order_head_unshipped);
                break;
            case "30":
                mOderPayWaitTitleTv.setText("卖家已发货");

                mOrderWaitPayIv.setBackgroundResource(R.drawable.order_head_shipped);
                break;
            case "40":
            case "50":
                mOderPayWaitTitleTv.setText("交易完成");
                mOrderWaitPayIv.setBackgroundResource(R.drawable.order_head_finish);
                break;
            case "60":
            case "70":
                mOderPayWaitTitleTv.setText("交易关闭");
                mOrderWaitPayIv.setBackgroundResource(R.drawable.order_head_close);
                break;
            default:

        }

        if(mNewOderDetailModel.data.orderStatus.equals("10")){
            mTvOderPayTimeLate.setVisibility(View.VISIBLE);
            startCountDown();
        }
        else{
            mTvOderPayTimeLate.setVisibility(View.GONE);
            mTvOderPayTimeLate.cancelCountDown();

        }

        if(mNewOderDetailModel.data.getIsDeposit()){
            mOrderPriceTipsTV.setText("定金金额");
            //是否是意向订单
            if(mNewOderDetailModel.data.isIntentOder()){
                mOrderPayTipsTv.setVisibility(View.GONE);
                mOrderPayTipsTv.setText("");
            }
            else if(mNewOderDetailModel.data.orderStatus.equals("10")||mNewOderDetailModel.data.orderStatus.equals("60")
                    ||mNewOderDetailModel.data.orderStatus.equals("70")){

                mOrderPayTipsTv.setVisibility(View.GONE);
                mOrderPayTipsTv.setText("");
            }
            else{
                mOrderPayTipsTv.setVisibility(View.VISIBLE);

                mOrderPayTipsTv.setText(mNewOderDetailModel.data.text);
            }
        }
        else{
            mOrderPriceTipsTV.setText("商品总价");
            mOrderPayTipsTv.setVisibility(View.GONE);
            mOrderPayTipsTv.setText("");
        }
    }

    private void startCountDown() {
        long seconds = 0;
        try {
            seconds = Long.valueOf(mNewOderDetailModel.data.expireTimes);
        } catch (Exception e) {

        }
        if (seconds < 0) {
            seconds = 0;
        }
        if (seconds>0){
            mRlvPay.setBackgroundResource(R.drawable.oder_detail_bt_stoken_red_);
            mTvPay.setTextColor(getResources().getColor(R.color.color_fff4372d));
            mRlvPay.setClickable(true);
        }

        mTvOderPayTimeLate
                .setCountDownText("支付剩余时间", "")
                .setCloseKeepCountDown(false)//关闭页面保持倒计时开关
                .setCountDownClickable(false)//倒计时期间点击事件是否生效开关
                .setShowFormatTime(true)//是否格式化时间
                .setIntervalUnit(TimeUnit.SECONDS)
                .setOnCountDownStartListener(() -> {
                })
                .setOnCountDownTickListener(untilFinished -> {
                })
                .setOnCountDownFinishListener(() -> {
                    Toasty.normal(NewOrderDetailAct.this, "订单已过期").show();
                    mTvOderPayTimeLate.setText("支付剩余时间 0 时 0 分 0 秒");
                    mRlvPay.setBackgroundResource(R.drawable.oder_detail_bt_stoken_gray);
                    mTvPay.setTextColor(getResources().getColor(R.color.color_353f5c));
                    mRlvPay.setClickable(false);
                })
                .startCountDown(seconds);
    }

    /**
     * 设置收件人信息
     */
    private void setAddresseeInfo() {
        mTvAddressName.setText(mNewOderDetailModel.data.receiverName);
        mTvAddressPhone.setText(mNewOderDetailModel.data.receiverMobile);
        mTvAddress.setText(mNewOderDetailModel.data.receiverAddress);
        if (!mNewOderDetailModel.data.isDelivery()) {
            mRlvAddress.setVisibility(View.GONE);
            mAddressNoSupper.setVisibility(View.VISIBLE);
        } else {
            mRlvAddress.setVisibility(View.VISIBLE);
            mAddressNoSupper.setVisibility(View.GONE);
            findViewById(R.id.arrow).setVisibility(View.GONE);
        }
    }

    /**
     * 设置物流信息
     */
    private void setMailInfo() {
        if (mNewOderDetailModel.data.isIntentOder()) {
            return;
        }

        if (mNewOderDetailModel.data.isIntentOder()) {
            mRlvEmail.setVisibility(View.GONE);
        } else {
            if (mNewOderDetailModel.data.isDelivery()) {
                mRlvEmail.setVisibility(mNewOderDetailModel.data.isShowEmalInfoLayout() ? View.VISIBLE : View.GONE);
            } else {
                mRlvEmail.setVisibility(View.GONE);
            }
        }
        if (mRlvEmail.getVisibility() == View.VISIBLE) {
            mTvEmailName.setText("物流公司：" + mNewOderDetailModel.data.logisticsName);
            mTvEmailNumber.setText("物流单号：" + mNewOderDetailModel.data.logisticsNum);
            mTvEmailNumberCopy.setOnClickListener(v -> {
                ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData mClipData = ClipData.newPlainText("Label", mNewOderDetailModel.data.logisticsNum);
                assert cm != null;
                cm.setPrimaryClip(mClipData);
                Toasty.normal(NewOrderDetailAct.this, "复制成功").show();
            });
        }
    }

    /**
     * 设置商品信息
     */
    private void setGoodsInfo() {
        ImageLoaderManager.INSTANCE.loadCircleImage(this, mShopHeadCover, mNewOderDetailModel.data.storeAvatar, R.drawable.order_shop);
        mTvShopName.setText(mNewOderDetailModel.data.storeName);
        findViewById(R.id.rlv_image_shop_in).setVisibility(View.VISIBLE);
        ImageLoaderManager.INSTANCE.loadImage(this, mImageGoodsCover, mNewOderDetailModel.data.goodsImgurl, R.drawable.nim_image_default, 0);
        mTvGoodsName.setText(mNewOderDetailModel.data.goodsName);
        mTvGoodsPrice.setText(mNewOderDetailModel.data.goodsSellerPrice);
        mTvGoodsCount.setText("x" + mNewOderDetailModel.data.goodsNum);
        if (TextUtils.isEmpty(mNewOderDetailModel.data.getRemark())) {
            mRlvEditTip.setVisibility(View.GONE);
        } else {
            mRlvEditTip.setVisibility(View.VISIBLE);
            mEdTextOderReMaks.setFocusable(false);
            mEdTextOderReMaks.setEnabled(false);
            mEdTextOderReMaks.setText(mNewOderDetailModel.data.getRemark());
        }
        mTvGoodsNumberAll.setText("共" + mNewOderDetailModel.data.goodsNum + "件");
        mTvPiceHeJi.setText(mNewOderDetailModel.data.orderGoodsPrice);
        findViewById(R.id.rlv_image_shop_in).setOnClickListener(v -> checkCanJump());
    }

    /**
     * 设置商品价格明细
     */
    private void setPiceDetails() {
        mRlvOderPiceDetails.setVisibility(mNewOderDetailModel.data.isIntentOder() ? View.GONE : View.VISIBLE);
        mTvGoodsPriceAll.setText(getResources().getString(R.string.order_yuan) + mNewOderDetailModel.data.orderGoodsPrice);
        mTvEmailPice.setText(getResources().getString(R.string.order_yuan) + mNewOderDetailModel.data.getPostagePrice());
        mTvOderPiceAll.setText(getResources().getString(R.string.order_yuan) + mNewOderDetailModel.data.orderPrice);
        mTvRealyNeedPay.setText(mNewOderDetailModel.data.orderPayPrice);
        if (mNewOderDetailModel.data.isSetNeedPayTv()) {
            mTvPayTitle.setText("需付款");
        } else {
            mTvPayTitle.setText("实付款");
        }

    }

    /**
     * 设置订单 时间详情
     */
    private void setOderAccountInfo() {
        //待付款、20：待发货、30：待收货、40：已收货、50：已完成、60：已取消、70：已超时）
        mTvOrderAccount.setText("订单编号：" + mNewOderDetailModel.data.orderCode);
        mTvOderCreadTime.setText("创建时间：" + DateTimeUtil.getFormatCurrentTime(mNewOderDetailModel.data.createDate, DateTimeUtil.PATTERN_CURRENT_TIME));

        mTvOderAccountCopy.setOnClickListener(v -> {
            ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData mClipData = ClipData.newPlainText("Label", mNewOderDetailModel.data.orderCode);
            assert cm != null;
            cm.setPrimaryClip(mClipData);
            Toasty.normal(NewOrderDetailAct.this, "复制成功").show();
        });
        if (mNewOderDetailModel.data.isWiatShopSendGoods()) {
            mTvOderLiuShuiAccount.setVisibility(View.VISIBLE);
            mTvOderPayTime.setVisibility(View.VISIBLE);
            mTvOderLiuShuiAccount.setText("交易流水号：" + mNewOderDetailModel.data.orderTradeNo);
            mTvOderPayTime.setText("付款时间：" + DateTimeUtil.getFormatCurrentTime(mNewOderDetailModel.data.payDate, DateTimeUtil.PATTERN_CURRENT_TIME));
        }
        if (mNewOderDetailModel.data.isShopGoodsSended()) {
            mTvOderSendGoodsTime.setVisibility(View.VISIBLE);
            mTvOderLiuShuiAccount.setVisibility(View.VISIBLE);
            mTvOderPayTime.setVisibility(View.VISIBLE);
            mTvOderLiuShuiAccount.setText("交易流水号：" + mNewOderDetailModel.data.orderTradeNo);
            mTvOderPayTime.setText("付款时间：" + DateTimeUtil.getFormatCurrentTime(mNewOderDetailModel.data.payDate, DateTimeUtil.PATTERN_CURRENT_TIME));
            mTvOderSendGoodsTime.setText("发货时间：" + DateTimeUtil.getFormatCurrentTime(mNewOderDetailModel.data.deliveryDate, DateTimeUtil.PATTERN_CURRENT_TIME));
        }
        if (mNewOderDetailModel.data.isFinshOder() && !mNewOderDetailModel.data.isIntentOder()) {
            mTvOderFinshTime.setVisibility(View.VISIBLE);
            mTvOderSendGoodsTime.setVisibility(View.VISIBLE);
            mTvOderLiuShuiAccount.setVisibility(View.VISIBLE);
            mTvOderPayTime.setVisibility(View.VISIBLE);
            mTvOderLiuShuiAccount.setText("交易流水号：" + mNewOderDetailModel.data.orderTradeNo);
            mTvOderPayTime.setText("付款时间：" + DateTimeUtil.getFormatCurrentTime(mNewOderDetailModel.data.payDate, DateTimeUtil.PATTERN_CURRENT_TIME));
            mTvOderSendGoodsTime.setText("发货时间：" + DateTimeUtil.getFormatCurrentTime(mNewOderDetailModel.data.deliveryDate, DateTimeUtil.PATTERN_CURRENT_TIME));
            mTvOderFinshTime.setText("成交时间：" + DateTimeUtil.getFormatCurrentTime(mNewOderDetailModel.data.completeDate, DateTimeUtil.PATTERN_CURRENT_TIME));
            if (!mNewOderDetailModel.data.isDelivery()) {
                mTvOderSendGoodsTime.setVisibility(View.GONE);
            }
        }
        mTvShopPhone.setText(mNewOderDetailModel.data.storeBusiContactPhone);
    }

    /**
     * 更新底部按钮状态
     */
    private void setOderBottomStatus() {
        if (mNewOderDetailModel.data.isIntentOder() || mNewOderDetailModel.data.isWiatShopSendGoods() || mNewOderDetailModel.data.isFinshOder()) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, AppUtils.dip2px(NewOrderDetailAct.this, 48) + AppUtils.getStatusBarHeight(NewOrderDetailAct.this), 0, 0);
            mSwipeRefreshLayout.setLayoutParams(layoutParams);
            mOderAcitivityButtomLayout.setVisibility(View.GONE);
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, AppUtils.dip2px(NewOrderDetailAct.this, 48) + AppUtils.getStatusBarHeight(NewOrderDetailAct.this), 0, AppUtils.dip2px(NewOrderDetailAct.this, 54));
            mSwipeRefreshLayout.setLayoutParams(layoutParams);
            mOderAcitivityButtomLayout.setVisibility(View.VISIBLE);
        }

        if (mNewOderDetailModel.data.isOderColse()) {
            mRlvPay.setVisibility(View.GONE);
            mRlvCancle.setVisibility(View.VISIBLE);
            mTvButtomCancle.setText("删除订单");
            mRlvCancle.setOnClickListener(v -> {
                TJMakeSureDialog dialog = new TJMakeSureDialog(NewOrderDetailAct.this, v1 -> delectOrder()).setTitleAndCotent("删除订单", getSourceString(R.string.order_cdelete_true)).hideTitle();
                dialog.show();
            });
        }

        if (mNewOderDetailModel.data.canEdOderRemaksEdText()) {
            mRlvPay.setVisibility(View.VISIBLE);
            mRlvCancle.setVisibility(View.VISIBLE);
            mTvButtomCancle.setText("取消订单");
            mRlvPay.setOnClickListener(v -> {
                if (!FastClickAvoidUtil.isFastDoubleClick(mRlvPay.getId())) {
                    RouterJumpUtil.startOrderPayDetailAct(mNewOderDetailModel.data.id, mNewOderDetailModel.data.orderCode, mTvRealyNeedPay.getText().toString(), SKIP_FROM_DETAIL);
                }
            });
            mRlvCancle.setOnClickListener(v -> {
                if (!FastClickAvoidUtil.isFastDoubleClick(mRlvCancle.getId())) {
                   //取消计时器
                    mTvOderPayTimeLate.cancelCountDown();

                    TJMakeSureDialog dialog = new TJMakeSureDialog(NewOrderDetailAct.this, v1 -> cancleOrder()).setTitleAndCotent("", getSourceString(R.string.order_cancel_true)).hideTitle();
                    dialog.show();
                }
            });
        }

        if (mNewOderDetailModel.data.isShopGoodsSended()) {
            mRlvPay.setVisibility(View.GONE);
            mRlvCancle.setVisibility(View.VISIBLE);
            mTvButtomCancle.setText("确认收货");
            mRlvCancle.setOnClickListener(v -> {
                TJMakeSureDialog dialog = new TJMakeSureDialog(NewOrderDetailAct.this, v1 -> makeSureOrder()).setTitleAndCotent("", "是否确认收货").hideTitle();
                dialog.show();
            });
        }
    }

    private void makeSureOrder() {
        ShopModuleApiProvider.getInstance().comfirmOrder(orderId, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(NewOrderDetailAct.this, e.getMessage()).show();
            }

            @Override
            public void onNext(OMBaseResponse response) {
                if (response.isSuccess()) {
                    Toasty.normal(NewOrderDetailAct.this, "确认成功").show();
                    EventBus.getDefault().post(new OrderChangeEvent("4"));
                } else {
                    Toasty.normal(NewOrderDetailAct.this, response.msg).show();
                }
            }
        });

    }

    private void delectOrder() {
        ShopModuleApiProvider.getInstance().deletOrder(orderId, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(NewOrderDetailAct.this, "网络错误，请检查网络").show();
            }

            @Override
            public void onNext(OMBaseResponse response) {
                if (response.isSuccess()) {
                    Toasty.normal(NewOrderDetailAct.this, "删除订单成功").show();
                    EventBus.getDefault().post(new OrderChangeEvent("2"));
                    onBackPressed();
                } else {
                    Toasty.normal(NewOrderDetailAct.this, response.msg).show();
                }
            }
        });
    }


    private void cancleOrder() {
        ShopModuleApiProvider.getInstance().cancelOrder(orderId, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(NewOrderDetailAct.this, "网络错误，请检查网络").show();
            }

            @Override
            public void onNext(OMBaseResponse response) {
                if (response.isSuccess()) {
                    Toasty.normal(NewOrderDetailAct.this, "取消订单成功").show();
                    EventBus.getDefault().post(new OrderChangeEvent("3"));
                } else {
                    Toasty.normal(NewOrderDetailAct.this, response.msg).show();
                }
            }
        });
    }


    private String getSourceString(int id) {
        return getResources().getString(id);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderChange(OrderChangeEvent event) {
        getData();
    }
}




















