package com.tojoy.cloud.module_shop.Order.OrderDetail.Model;

import android.text.TextUtils;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

public class RechargeOrderStatusResponse extends OMBaseResponse {

    private RechargeOrderStatusModel data;

    public class RechargeOrderStatusModel {
        /**
         * 该字段仅用于充值分钟数支付场景
         * 支付状态明细（0：待支付、1：支付成功、2：支付超时、4：取消支付（取消订单） 5、支付失败）
         * <p>
         * 订单状态：1：待付款、2：交易关闭、3：交易完成；
         */
        private String orderStatus;

        public boolean isPayStatusSuccess() {
            return !TextUtils.isEmpty(orderStatus) && "3".equals(orderStatus);
        }

        public boolean isPayStatusWait() {
            return !TextUtils.isEmpty(orderStatus) && "1".equals(orderStatus);
        }

        public boolean isPayStatusCancle() {
            return !TextUtils.isEmpty(orderStatus) && "2".equals(orderStatus);
        }
    }

    public RechargeOrderStatusModel getData() {
        return data;
    }

    public void setData(RechargeOrderStatusModel data) {
        this.data = data;
    }
}
