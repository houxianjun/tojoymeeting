package com.tojoy.cloud.module_shop.Enterprise.Structure.Home.View;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Model.StaffModel;
import com.tojoy.cloud.module_shop.R;

import java.util.List;

public class EnterStructItemAdapter extends BaseRecyclerViewAdapter<StaffModel> {

    private int mType;

    public EnterStructItemAdapter(@NonNull Context context, @NonNull List<StaffModel> datas, int type) {
        super(context, datas);
        mType = type;
    }

    @Override
    protected int getViewType(int position, @NonNull StaffModel data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_stuct_item_layout;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new EnterStructItemViewHolder(context, parent, getLayoutResId(viewType));
    }

    /**
     * ViewHolder
     */
    public class EnterStructItemViewHolder extends BaseRecyclerViewHolder<StaffModel> {

        ImageView mStypeIcon;
        TextView mTitle;
        TextView mSubTitle;

        EnterStructItemViewHolder(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            mStypeIcon = itemView.findViewById(R.id.iv_type_icon);
            mTitle = itemView.findViewById(R.id.iv_type_tv);
            mSubTitle = itemView.findViewById(R.id.iv_subtype_tv);
        }

        @Override
        public void onBindData(StaffModel meetingModel, int position) {
            itemView.setOnClickListener(v -> mStaffDepartClickListener.onClickAt(meetingModel, mType));

            if (mType == 0 || mType == 1) {
                mStypeIcon.setBackgroundResource(R.drawable.icon_manage_ry_levelic);
                mTitle.setText(meetingModel.name);
                mSubTitle.setVisibility(View.VISIBLE);
                mSubTitle.setText(meetingModel.mobile);
            } else {
                mStypeIcon.setBackgroundResource(R.drawable.icon_manage_ry_levelic2);
                mTitle.setText(meetingModel.deptName);
                mSubTitle.setVisibility(View.GONE);
            }
        }
    }

    public interface StaffDepartClickListener {
        void onClickAt(StaffModel meetingModel, int type);
    }

    public StaffDepartClickListener mStaffDepartClickListener;

}