package com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.cloud.module_shop.Api.ShopModuleApiProvider;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.DepartRespone;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Model.DepartModel;
import com.tojoy.cloud.module_shop.R;

import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 选择部门
 * create by luzhu on 2020/2/27
 */

public class SelectDepartAct extends UI {

    private List<DepartModel> mDatas;
    private String newJson;
    //是添加员工，还是部门
    private String type = "";

    /**
     * @param departsJsonStr 部门集合转成的json字符串
     */
    public static void start(String departsJsonStr, Context context) {
        Intent intent = new Intent();
        intent.putExtra("departsJsonStr", departsJsonStr);
        intent.setClass(context, SelectDepartAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_admini_department);

        initData(getIntent().getStringExtra("departsJsonStr"), getIntent().getStringExtra("type"));
        initUI();
    }

    private void initData(String jsonData, String type) {
        mDatas = new Gson().fromJson(jsonData, new TypeToken<List<DepartModel>>() {}.getType());
        this.type = type;
    }

    private void initUI() {
        setStatusBar();
        setUpNavigationBar();
        showBack();
        setTitle("选择部门");
        ListView mListView = (ListView) findViewById(R.id.listView);
        TextView mTvSure = (TextView) findViewById(R.id.tvSure);

        SelectDepartAdapter mAdapter = new SelectDepartAdapter();
        mListView.setAdapter(mAdapter);

        mTvSure.setOnClickListener(view -> {
            int size = mDatas.size();
            for (int i = 0; i < size; i++) {
                DepartModel model = mDatas.get(i);
                if (model.isChecked) {
                    Intent intent = getIntent();
                    intent.putExtra("corporationCode", model.corporationCode == null ? BaseUserInfoCache.getCompanyCode(SelectDepartAct.this) : model.corporationCode);

                    //添加员工时返回code
                    if (type != null) {
                        if ("1".equals(type) || "2".equals(type)) {
                            intent.putExtra("parentId", model.code == null ? "" : model.code);
                        } else {
                            //部门返回id
                            intent.putExtra("parentId", model.id == null ? "" : model.id);
                        }
                    } else {
                        //部门返回id
                        intent.putExtra("parentId", model.id == null ? "" : model.id);
                    }

                    intent.putExtra("departName", model.name);
                    intent.putExtra("existSubDept", model.existSubDept ? "1" : "0");
                    setResult(AddDepartmentAct.Request_Code, intent);
                    finish();
                    return;
                }
            }
            Toasty.normal(SelectDepartAct.this, "请选择部门").show();
        });
    }

    private class SelectDepartAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return mDatas == null ? 0 : mDatas.size();
        }

        @Override
        public Object getItem(int i) {
            return mDatas.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            SelectDepartVH vh;
            if (view == null) {
                view = LayoutInflater.from(SelectDepartAct.this).inflate(R.layout.item_select_depart, viewGroup, false);
                vh = new SelectDepartVH();
                vh.checkLayout = view.findViewById(R.id.checkLayout);
                vh.checkBox = view.findViewById(R.id.checkbox);
                vh.tvName = view.findViewById(R.id.tvName);
                vh.ivNext = view.findViewById(R.id.ivNext);

                view.setTag(vh);
            } else {
                vh = (SelectDepartVH) view.getTag();
            }

            DepartModel model = mDatas.get(i);

            vh.checkBox.setChecked(model.isChecked);
            vh.tvName.setText(model.name);
            vh.ivNext.setVisibility(model.existSubDept ? View.VISIBLE : View.GONE);

            vh.checkLayout.setOnClickListener(view1 -> {
                model.isChecked = !model.isChecked;
                if (model.isChecked) {
                    int size = mDatas.size();
                    for (int k = 0; k < size; k++) {
                        if (k != i) {
                            mDatas.get(k).isChecked = false;
                        }
                    }
                }
                notifyDataSetChanged();
            });

            view.setOnClickListener(v -> {
                if (!model.existSubDept) {
                    return;
                }
                showProgressHUD(SelectDepartAct.this, "加载中");
                ShopModuleApiProvider.getInstance().getDepartments(model.id, new Observer<DepartRespone>() {
                    @Override
                    public void onCompleted() {
                        dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismissProgressHUD();
                        Toasty.normal(SelectDepartAct.this, "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(DepartRespone respone) {
                        if (respone.isSuccess()) {
                            List<DepartModel> data = respone.data;
                            newJson = new Gson().toJson(data);
                            initData(newJson, type);
                            notifyDataSetChanged();
                        } else {
                            Toasty.normal(SelectDepartAct.this, respone.msg).show();
                        }
                    }
                });
            });

            return view;
        }
    }

    private class SelectDepartVH {
        View checkLayout;
        CheckBox checkBox;
        TextView tvName;
        ImageView ivNext;
    }

}
