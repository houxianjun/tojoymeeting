package com.tojoy.cloud.module_shop.Enterprise.Structure.Model;


import android.text.TextUtils;

import com.tojoy.tjoybaselib.configs.UserRoles;
import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.ArrayList;

/**
 * 员工数据
 */

public class StaffModel extends OMBaseResponse {
    public String id;
    public String userId;

    //手机号
    public String mobile;

    public String companyCode;
    public String deptName;
    public String trueName;
    public int userBackRole;
    public int userFrontRole;
    public String email;
    public String cityName;
    public String deptId;
    //新增
    public String name;
    public String roleNames;
    public String departmentCode;
    public String employeeCode;
    private String roleCodes;
    public ArrayList<Integer> roleIds;


    /**
     * 判断用户是否有某种角色
     *
     * @param roles 角色类型对应的code
     * @return true：有；false：无
     */
    public boolean getUserIsHaveRoles(UserRoles roles) {
        String rolesStr = roleCodes;
        if (TextUtils.isEmpty(rolesStr)) {
            return false;
        } else {
            String[] roleList = rolesStr.split("、");
            boolean isHavePermiss = false;
            for (String aRoleList : roleList) {
                if (!TextUtils.isEmpty(aRoleList) && roles.getValue().equals(aRoleList)) {
                    isHavePermiss = true;
                    break;
                }
            }
            return isHavePermiss;
        }
    }


}
