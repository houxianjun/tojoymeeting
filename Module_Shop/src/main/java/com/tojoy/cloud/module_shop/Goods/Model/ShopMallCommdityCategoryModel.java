package com.tojoy.cloud.module_shop.Goods.Model;

/**
 * Created by daibin
 * on 2019/12/25.
 * function
 */
public class ShopMallCommdityCategoryModel {
    /**
     * "":全部用户 1：实体 2：虚拟
     */
    public String type;
    public String title;
    public boolean isSelected;

    public ShopMallCommdityCategoryModel(String type, String title, boolean isSelected) {
        this.type = type;
        this.title = title;
        this.isSelected = isSelected;
    }
}
