package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Controller;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.meetingperson.EmployeeModelList;
import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.model.meetingperson.SingleTonModel.MeetingPersonTon;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Adapter.MeetingDepartmentSelectAdapter;
import com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Adapter.MeetingShowOutSidePersonAdapter;
import com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.rxbus.AttendeeRxBus;
import com.tojoy.cloud.module_shop.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import es.dmoral.toasty.Toasty;

/**
 * @author houxianjun
 * @date 2020/5/11.
 * 云洽会1.2.3需求  添加参会人页面首页展示替换 MeetingDepartmentSelectFragment
 * 1 新增外部联系人选项
 * 2 部门层级变为组织架构
 */
public class MeetingAttendeeSelectFragment extends MeetingPersonBaseFragment implements MeetingDepartmentSelectAdapter.OnDeptCheckBoxListener, MeetingShowOutSidePersonAdapter.OnPersonCheckBoxListener {

    public static final String DEPT_DATA = "DEPT_DATA";
    public static final String PERSON_DATA = "PERSON_DATA";
    private static final String CHECKED_PERSON = "CHECKED_PERSON";
    private static final String AUTH_TYPE = "meeting_auth_type";
    //会议邀请权限
    private String meeting_auth_type;
    //外部联系人标题
    private RelativeLayout rvOutsidePersonTitle;
    private RecyclerView rvCompnay, rvOutsidePerson;

    //添加外部联系人
    private TextView tvPersonAdd;

    /**
     * 组织架构公司列表适配器
     */
    private MeetingDepartmentSelectAdapter mDeptAdapter;
    /**
     * 外部联系人列表适配器
     */
    private MeetingShowOutSidePersonAdapter mPersonAdapter;
    /**
     * 组织架构部门集合
     */
    private List<MeetingPersonDepartmentResponse.DepartmentModel> mDepartments;
    /**
     * 外部联系人集合
     */
    private List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> mOutSidePersonsList;
    /**
     * 被选择人数集合
     */
    private List<String> mCheckedPerson;
    /**
     * 当前总人数，包括组织架构内人员以及外部人员
     */
    private List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> mCurDeptAllPerson;

    /**
     * 返回当前外部联系人集合
     */
    public List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> getmPersons() {
        if (mPersonAdapter != null) {

            mOutSidePersonsList = mPersonAdapter.getDatas();

        }
        return mOutSidePersonsList;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_meeting_attendee_select, container, false);
        }
        return mView;
    }

    @Override
    protected void initUI() {
        super.initUI();
        rvOutsidePersonTitle = mView.findViewById(R.id.rv_outside_person_title);
        rvCompnay = mView.findViewById(R.id.rv_compnay);
        rvOutsidePerson = mView.findViewById(R.id.rv_outside_person);
        tvPersonAdd = mView.findViewById(R.id.tv_person_add);
    }

    public static MeetingAttendeeSelectFragment getInstance(Serializable serDept, Serializable serPerson, ArrayList<String> checkedList, String curTitleDepartmentId, String meeting_auth_type) {
        MeetingAttendeeSelectFragment fragment = new MeetingAttendeeSelectFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(DEPT_DATA, serDept);
        bundle.putSerializable(PERSON_DATA, serPerson);
        bundle.putStringArrayList(CHECKED_PERSON, checkedList);
        bundle.putString(AUTH_TYPE, meeting_auth_type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            meeting_auth_type = getArguments().getString(AUTH_TYPE);
            mDepartments = (List<MeetingPersonDepartmentResponse.DepartmentModel>) getArguments().getSerializable(DEPT_DATA);
            mOutSidePersonsList = (List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel>) getArguments().getSerializable(PERSON_DATA);
            ((MeetingPersonSelectAct) getActivity()).setmOutSidePersonEmoloyModels(mOutSidePersonsList);
            mCheckedPerson = getArguments().getStringArrayList(CHECKED_PERSON);
            mCurDeptAllPerson = new ArrayList<>();
            if (mOutSidePersonsList != null) {
                mCurDeptAllPerson.addAll(mOutSidePersonsList);
            }
            recCurDeptAllPerson(mDepartments);
            mCurDeptAllPerson = removeDuplicateEmployee();
            setCheckedPersonStatus(mOutSidePersonsList);
            setCheckedDeptStatus();

            //计算总人数
            ((MeetingPersonSelectAct) getActivity()).calAllPersonCount();
            //将所有员工Map转List
            ((MeetingPersonSelectAct) getActivity()).saveAllPersonToList();
            //更新底部已选参会人数据ui
            ((MeetingPersonSelectAct) getActivity()).updateBottomTextViewStatus();

        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        AttendeeRxBus.getInstance().unSubscribe(this);
    }

    @Override
    protected void initData() {
        super.initData();
        if (mDepartments != null) {
            mDeptAdapter = new MeetingDepartmentSelectAdapter(getActivity(), mDepartments);
            mDeptAdapter.setOnCheckListener(this);
            rvCompnay.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvCompnay.setAdapter(mDeptAdapter);
            mDeptAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
                if (!FastClickAvoidUtil.isDoubleClick()) {
                    if (getActivity() instanceof MeetingPersonSelectAct) {
                        if (data.corDepartmentList != null && !data.corDepartmentList.isEmpty()) {
                            ((MeetingPersonSelectAct) getActivity()).addDepartmentFragment(mDepartments.get(0).corDepartmentList, mDepartments.get(0).respUserForDepartmentDtoList, mDepartments.get(0).id);
                        } else {
                            ((MeetingPersonSelectAct) getActivity()).addPersonFragment(mDepartments.get(0).respUserForDepartmentDtoList, mDepartments.get(0).id);
                        }
                    }
                }
            });
        }
        if (meeting_auth_type == null || "2".equals(meeting_auth_type)) {
            rvOutsidePersonTitle.setVisibility(View.GONE);
            rvOutsidePerson.setVisibility(View.GONE);
        } else {
            rvOutsidePersonTitle.setVisibility(View.VISIBLE);
            rvOutsidePerson.setVisibility(View.VISIBLE);
        }

        if (mOutSidePersonsList != null) {
            mPersonAdapter = new MeetingShowOutSidePersonAdapter(getActivity(), mOutSidePersonsList);
            mPersonAdapter.setOnCheckListener(this);
            rvOutsidePerson.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvOutsidePerson.setAdapter(mPersonAdapter);
            mPersonAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
                if (!FastClickAvoidUtil.isDoubleClick()) {
                    CheckBox checkBox = v.findViewById(R.id.cb);
                    if (checkBox.isChecked()) {
                        checkBox.setChecked(false);
                    } else {
                        checkBox.setChecked(true);
                    }
                    onPersonCheckListener(mOutSidePersonsList.get(position), checkBox.isChecked());
                }
            });
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rvCompnay.setNestedScrollingEnabled(false);
            rvOutsidePerson.setNestedScrollingEnabled(false);
        }

        // 1.2.3 版本 新增添加按钮时间监听
        tvPersonAdd.setOnClickListener(v -> {
            List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> tmpList = mCurDeptAllPerson;
            tmpList.addAll(getmPersons());
            ((MeetingPersonSelectAct) getActivity()).sendToAddOutSide(tmpList);
        });
        updateActBottomCheckAllStatus();
    }

    /**
     * 回退时，更新上一Fragment数据
     */
    public void setPopBackListener(List<String> checkedPerson) {
        if (mCheckedPerson != null) {
            mCheckedPerson.clear();
            mCheckedPerson.addAll(checkedPerson);
        }
        setCheckedPersonStatus(getmPersons());
        setCheckedDeptStatus();
        rvCompnay.post(() -> {
            if (mDeptAdapter != null) {
                mDeptAdapter.notifyDataSetChanged();
            }
        });
        rvOutsidePerson.post(() -> {
            if (mPersonAdapter != null) {
                mPersonAdapter.notifyDataSetChanged();
            }
        });
        updateActBottomCheckAllStatus();
    }

    /**
     * 去重
     *
     * @return
     */
    private List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> removeDuplicateEmployee() {
        Set<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> set = new TreeSet<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel>((a, b) -> {
            // 字符串则按照asicc码升序排列
            return a.userId.compareTo(b.userId);
        });
        set.addAll(mCurDeptAllPerson);
        return new ArrayList<>(set);
    }

    /**
     * 全选时，状态变化
     */
    public void notifyDataChange(boolean isChecked) {
        if (isChecked) {
            //外部联系人总数
            int oustSideCount = mOutSidePersonsList.size();
            //组织架构内人数
            int inSindeCount = MeetingPersonTon.getInstance().allPeopleCount;
            int totalInvitatePerson;
            //内部会议
            if ("2".equals(((MeetingPersonSelectAct) getActivity()).meeting_auth_type)) {
                totalInvitatePerson = inSindeCount;
            } else {
                totalInvitatePerson = inSindeCount + oustSideCount;
            }

            //邀请人最大数限制
            if (totalInvitatePerson > MeetingPersonTon.getInstance().maxInvitation) {
                ((MeetingPersonSelectAct) getActivity()).updateBottomCheckAllStatus(false);
                Toasty.normal(getActivity(), String.format(getString(R.string.t_max_meeting_person), MeetingPersonTon.getInstance().maxInvitation)).show();
                return;
            }
        }

        if (mDepartments != null && !mDepartments.isEmpty()) {
            MeetingPersonTon.getInstance().recSetDepartmentPseronStatus(isChecked, MeetingPersonTon.getInstance().getmDepartmentData());
            for (MeetingPersonDepartmentResponse.DepartmentModel departmentModel : mDepartments) {
                departmentModel.isChecked = isChecked;
                MeetingPersonTon.getInstance().recSetDepartmentPseronStatus(isChecked, departmentModel);
                ((MeetingPersonSelectAct) getActivity()).updateDeptCheckStatus(departmentModel, isChecked);
            }
            rvCompnay.post(() -> {
                if (mDeptAdapter != null) {
                    mDeptAdapter.notifyDataSetChanged();
                }
            });

        }
        if (!"2".equals(MeetingPersonTon.getInstance().meetingAuthType)) {
            if (mOutSidePersonsList != null && !mOutSidePersonsList.isEmpty()) {
                MeetingPersonTon.getInstance().recSetOutSidePersonStatus(isChecked, mOutSidePersonsList);
                for (MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel : mOutSidePersonsList) {
                    employeeModel.isChecked = isChecked;
                    ((MeetingPersonSelectAct) getActivity()).updatePersonCheckStatus(employeeModel, isChecked);
                }
                rvOutsidePerson.post(() -> {
                    if (mPersonAdapter != null) {
                        mPersonAdapter.notifyDataSetChanged();
                    }
                });
            }
        }
    }

    /**
     * 组织架构item checkbox选择事件监听
     */
    @Override
    public void onDeptCheckListener(MeetingPersonDepartmentResponse.DepartmentModel data, boolean isChecked) {
        if (getActivity() instanceof MeetingPersonSelectAct) {
            int outsideSelectCount = 0;
            if (mOutSidePersonsList != null && mOutSidePersonsList.size() > 0) {
                for (int i = 0; i < mOutSidePersonsList.size(); i++) {
                    if (mOutSidePersonsList.get(i).isChecked) {
                        outsideSelectCount++;
                    }
                }
            }

            if (isChecked) {
                //判断最大限制规则：（当前已选人数-  部门内所有已选人数）+部门总共人数<MaxPerson
                //当前已选人数
                MeetingPersonTon.getInstance().recCurDepartSelectPersonCount = 0;
                int currentSelectCount = MeetingPersonTon.getInstance().recCurDepartSelectPerson(MeetingPersonTon.getInstance().getmDepartmentData()) + outsideSelectCount;
                //部门内所有已选人数
                MeetingPersonTon.getInstance().recCurDepartSelectPersonCount = 0;
                int currentDepartSelectCount = MeetingPersonTon.getInstance().recCurDepartSelectPerson(data);
                int currentDepartTotalCount = MeetingPersonTon.getInstance().allPeopleCount;
                if ((currentSelectCount - currentDepartSelectCount + currentDepartTotalCount) > MeetingPersonTon.getInstance().maxInvitation) {
                    data.isChecked = false;
                    ((MeetingPersonSelectAct) getActivity()).showMaxPerson();
                    rvCompnay.post(() -> {
                        if (mDeptAdapter != null) {
                            mDeptAdapter.notifyDataSetChanged();
                        }
                    });
                    return;
                } else {
                    isChecked = true;
                }
            }
            data.isChecked = isChecked;
            //设置部门的状态
            MeetingPersonTon.getInstance().recSetDepartmentPseronStatus(isChecked, data);
            ((MeetingPersonSelectAct) getActivity()).updateDeptCheckStatus(data, isChecked);
        }
    }

    /**
     * 外部联系人item checkbox选择事件监听
     *
     * @param data
     * @param isChecked
     */
    @Override
    public void onPersonCheckListener(MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel data, boolean isChecked) {
        if (getActivity() instanceof MeetingPersonSelectAct) {
            if (isChecked && !((MeetingPersonSelectAct) getActivity()).checkMaxPerson(1, true)) {
                // 选中单个的时候判断是否超出添加人数最大限制
                data.isChecked = false;
                mPersonAdapter.notifyDataSetChanged();
                return;
            }
            data.isChecked = isChecked;
            //更新选中员工状态
            ((MeetingPersonSelectAct) getActivity()).updatePersonCheckStatus(data, isChecked);
        }
    }


    private void recCurDeptAllPerson(List<MeetingPersonDepartmentResponse.DepartmentModel> departmentModels) {
        if (departmentModels != null && !departmentModels.isEmpty()) {
            for (int i = 0; i < departmentModels.size(); i++) {
                if (departmentModels.get(i).respUserForDepartmentDtoList != null) {
                    mCurDeptAllPerson.addAll(departmentModels.get(i).respUserForDepartmentDtoList);
                }
                recCurDeptAllPerson(departmentModels.get(i).corDepartmentList);
            }
        }
    }

    /**
     * 设置默认选中状态-部门
     */
    private void setCheckedDeptStatus() {
        List<String> list = new ArrayList<>();

        for (MeetingPersonDepartmentResponse.DepartmentModel departmentModel : mDepartments) {
            int total = MeetingPersonTon.getInstance().recItemDeptEmployee(list, departmentModel).size();
            //组织架构一个人都没有
            if (total == 0) {
                departmentModel.isChecked = false;
            } else {
                // 有多个人
                List<String> selectList = new ArrayList<>();
                int selecttotal = MeetingPersonTon.getInstance().recItemSelcetPersonEmployee(selectList, departmentModel).size();
                departmentModel.isChecked = total == selecttotal;
            }
        }
        list.clear();
    }


    /**
     * 设置默认选中状态-人员
     */
    private void setCheckedPersonStatus(List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> tmpList) {
        if (tmpList != null && !tmpList.isEmpty()) {
            HashMap<String, String> map = ((MeetingPersonSelectAct) getActivity()).getmAllCheckedPersonCountMap();
            if (map.size() == 0) {
                for (int i = 0; i < tmpList.size(); i++) {
                    tmpList.get(i).isChecked = false;
                }
            }
        }
    }

    /**
     * 用户点击了所有Checkbox
     */
    public void onItemCheckedAllListener(List<String> checkedPerson) {
        if (mCheckedPerson != null) {
            mCheckedPerson.clear();
            mCheckedPerson.addAll(checkedPerson);
        }
        updateActBottomCheckAllStatus();
    }

    /**
     * 通知数据选择状态改变
     */
    public void notifyDataChange() {
        setCheckedPersonStatus(getmPersons());
        setCheckedDeptStatus();

        rvCompnay.post(() -> {
            if (mDeptAdapter != null) {
                mDeptAdapter.notifyDataSetChanged();
            }
        });
        rvOutsidePerson.post(() -> {
            if (mPersonAdapter != null) {
                mPersonAdapter.notifyDataSetChanged();
            }
        });
    }

    /**
     * 更新Activity底部checkbox选中状态
     */
    public void updateActBottomCheckAllStatus() {
        if (getActivity() instanceof MeetingPersonSelectAct) {
            boolean isChecked;
            List<String> list = new ArrayList<>();
            int total = 0;
            int selecttotal = 0;
            for (MeetingPersonDepartmentResponse.DepartmentModel departmentModel : mDepartments) {
                total = MeetingPersonTon.getInstance().recItemDeptEmployee(list, departmentModel).size();
                List<String> selectList = new ArrayList<>();
                selecttotal = MeetingPersonTon.getInstance().recItemSelcetPersonEmployee(selectList, departmentModel).size();
            }
            if (!"2".equals(MeetingPersonTon.getInstance().meetingAuthType) && getmPersons() != null && getmPersons().size() > 0) {
                //外部联系人数量
                for (MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel : getmPersons()) {
                    total++;
                    if (employeeModel.isChecked) {
                        selecttotal++;
                    }
                }
            }
            //部门没有人员默认不能被选中
            if (total == 0) {
                isChecked = false;
            } else {
                isChecked = total == selecttotal;
            }
            // 更新Activity页面底部显示数据
            ((MeetingPersonSelectAct) getActivity()).updateBottomCheckAllStatus(isChecked);
        }
    }

    /**
     * 设置新添加外部联系人选中状态
     */
    public void setNewAddOutSidePersonCheckStatus(List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> model) {
        for (MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel : model) {
            mCheckedPerson.add(employeeModel.userId);
            ((MeetingPersonSelectAct) getActivity()).updatePersonCheckStatus(employeeModel, employeeModel.isChecked);
        }
    }

    public void updateOutSidePerson(EmployeeModelList employeeModelList) {
        List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> tmplist = employeeModelList.getList();
        if (tmplist != null && tmplist.size() > 0) {
            //增加是否吐司提示
            boolean isCanAdd = ((MeetingPersonSelectAct) getActivity()).checkMaxPerson(tmplist.size(), false);
            if (!isCanAdd) {
                for (MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel mo : tmplist) {
                    mo.isChecked = false;
                }
            }

            //是否可以继续添加外部联系人
            mOutSidePersonsList.addAll(0, tmplist);
            //保存外部联系人
            mPersonAdapter.updateData(mOutSidePersonsList);

            ((MeetingPersonSelectAct) getActivity()).setmOutSidePersonEmoloyModels(mPersonAdapter.getDatas());

            //从添加外部联系人页面返回需要动态改变当前总共可参会人数量
            mCurDeptAllPerson.addAll(employeeModelList.getList());
            mCurDeptAllPerson = removeDuplicateEmployee();

            if (isCanAdd) {
                setNewAddOutSidePersonCheckStatus(tmplist);
                //通知Activity页面底部数据更新总人数
                ((MeetingPersonSelectAct) getActivity()).updateAllPersonCount(tmplist);
            } else {
                updateActBottomCheckAllStatus();
            }
        }
    }
}
