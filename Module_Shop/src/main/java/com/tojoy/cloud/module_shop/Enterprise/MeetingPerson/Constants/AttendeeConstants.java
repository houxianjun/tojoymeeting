package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Constants;

/**
 * 添加参会人相关常量
 */
public final class AttendeeConstants {

    //会议权限类型
    public static final String MEETING_AUTHTYPE = "meeting_authtype";
    //进入添加参会人来源页面标示
    public static final String MEETING_FROM = "meeting_from";

    public static final String ATTENDEE_REMOVE_PERSON_MAP= "remove_attendee_person_map";

    public static final String ATTENDEE_REMOVE_PERSON_DATA= "remove_attendee_person_data";

    public static final String  ATTENDEE_ADD_PERSON = "ADD_PERSON";
    public static final String  ATTENDEE_CHECKED_PERSON = "CHECKED_PERSON";

}
