package com.tojoy.cloud.module_shop.Enterprise.Structure.Home.View;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.configs.UserRoles;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Home.Model.EnterStructModel;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Model.StaffModel;
import com.tojoy.cloud.module_shop.R;

import java.util.ArrayList;
import java.util.List;

public class EnterStructHomeAdapter extends BaseRecyclerViewAdapter<EnterStructModel> {

    public EnterStructHomeAdapter(@NonNull Context context, @NonNull List<EnterStructModel> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull EnterStructModel data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_struct_layout;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new EnterStructViewHolder(context, parent, getLayoutResId(viewType));
    }

    /**
     * ViewHolder
     */
    public class EnterStructViewHolder extends BaseRecyclerViewHolder<EnterStructModel> {
        ImageView mTypeIcon;
        TextView mTypeTitle;
        RelativeLayout mAddLayout;

        CustomListView recyclerView;
        ContactsAdapter mAdapter;

        RelativeLayout mSwitchLayout;
        TextView mOpenTv;
        Drawable mOpen, mClose;

        RelativeLayout mEmptyTipLayout;
        TextView mEmptyTip;

        EnterStructViewHolder(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            mTypeIcon = itemView.findViewById(R.id.iv_type_icon);
            mTypeTitle = itemView.findViewById(R.id.iv_type_tv);
            mOpenTv = itemView.findViewById(R.id.iv_switch);
            recyclerView = itemView.findViewById(R.id.recyclerView);
            mSwitchLayout = itemView.findViewById(R.id.rlv_manager_footer);
            mAddLayout = itemView.findViewById(R.id.rlv_add);
            mEmptyTipLayout = itemView.findViewById(R.id.rlv_empty);
            mEmptyTip = itemView.findViewById(R.id.tv_empty_tip);

            mOpen = context.getResources().getDrawable(R.drawable.icon_more_switch_o);
            mOpen.setBounds(0, 0,
                    mOpen.getMinimumWidth(),
                    mOpen.getMinimumHeight());

            mClose = context.getResources().getDrawable(R.drawable.icon_more_switch_c);
            mClose.setBounds(0, 0,
                    mClose.getMinimumWidth(),
                    mClose.getMinimumHeight());
        }

        @Override
        public void onBindData(EnterStructModel enterStructModel, int position) {
            mTypeIcon.setBackgroundResource(enterStructModel.getTypeDrawable());
            mTypeTitle.setText(enterStructModel.getHeaderTitle());

            boolean needShowAdd = enterStructModel.type != 0 && BaseUserInfoCache.getUserIsHaveRoles(context, UserRoles.SUPERADMIN);

            mAddLayout.setVisibility(needShowAdd ? View.VISIBLE : View.GONE);
            mAddLayout.setOnClickListener(v -> mOperationCallback.doAdd(enterStructModel.type));

            mAdapter = new ContactsAdapter(enterStructModel.getStaffList(), enterStructModel.type);
            recyclerView.setAdapter(mAdapter);
            setListViewHeight(recyclerView);

            mSwitchLayout.setVisibility(enterStructModel.hasSwitch() ? View.VISIBLE : View.GONE);
            mSwitchLayout.setOnClickListener(v -> mOperationCallback.doSwitch(position));
            mOpenTv.setText(enterStructModel.isOpen ? "收起" : "更多");
            mOpenTv.setCompoundDrawables(null, null, enterStructModel.isOpen ? mClose : mOpen, null);

            //空提示
            if (enterStructModel.staffList != null && enterStructModel.staffList.size() > 0) {
                mEmptyTipLayout.setVisibility(View.GONE);
            } else {
                mEmptyTipLayout.setVisibility(View.VISIBLE);
            }
            if (enterStructModel.type == 1) {
                mEmptyTip.setText("还未添加员工");
            } else {
                mEmptyTip.setText("还未添加下级部门");
            }
        }

        void setListViewHeight(ListView listView) {
            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter == null) {
                return;
            }
            int totalHeight = 0;
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, listView);
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
        }
    }


    public interface OperationCallback {
        void doSwitch(int pos);

        void doAdd(int type);
    }

    public OperationCallback mOperationCallback;
    public EnterStructItemAdapter.StaffDepartClickListener mStaffDepartClickListener;


    private class ContactsAdapter extends BaseAdapter {

        private int mType;
        private List<StaffModel> mDatas;

        ContactsAdapter(List<StaffModel> datas, int type) {
            this.mDatas = datas;
            this.mType = type;
        }

        @Override
        public int getCount() {
            return mDatas == null ? 0 : mDatas.size();
        }

        @Override
        public Object getItem(int i) {
            return mDatas.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            ContactsVH vh;
            if (view == null) {
                view = LayoutInflater.from(context).inflate(R.layout.item_stuct_item_layout, viewGroup, false);
                vh = new ContactsVH();
                vh.mStypeIcon = view.findViewById(R.id.iv_type_icon);
                vh.mTitle = view.findViewById(R.id.iv_type_tv);
                vh.mSubTitle = view.findViewById(R.id.iv_subtype_tv);
                view.setTag(vh);
            } else {
                vh = (ContactsVH) view.getTag();
            }

            StaffModel staffModel = mDatas.get(i);
            if (mType == 0 || mType == 1) {
                vh.mStypeIcon.setBackgroundResource(R.drawable.icon_manage_ry_levelic);
                //vh.mTitle.setText(staffModel.trueName);
                vh.mTitle.setText(staffModel.name);
                vh.mSubTitle.setVisibility(View.VISIBLE);
                vh.mSubTitle.setText(staffModel.mobile);
            } else {
                vh.mStypeIcon.setBackgroundResource(R.drawable.icon_manage_ry_levelic2);
                vh.mTitle.setText(staffModel.deptName);
                vh.mSubTitle.setVisibility(View.GONE);
            }
            view.setOnClickListener(view1 -> mStaffDepartClickListener.onClickAt(staffModel, mType));

            return view;
        }
    }

    private class ContactsVH {
        ImageView mStypeIcon;
        TextView mTitle;
        TextView mSubTitle;
    }

}
