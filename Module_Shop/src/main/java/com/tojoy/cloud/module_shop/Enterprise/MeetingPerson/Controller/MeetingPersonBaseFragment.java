package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Controller;

import com.tojoy.common.App.BaseFragment;

/**
 * @author liuhm
 * @date 2020/2/27
 * @desciption
 */
public class MeetingPersonBaseFragment extends BaseFragment {

    private boolean visible = false;

    public void onVisible() {

    }
    public void onHidden() {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint() && !visible) {
            //确保在一个可见周期中只调用一次onVisible()
            visible = true;
            onVisible();
        } else if (visible) {
            visible = false;
            onHidden();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        //和activity的onResume绑定，Fragment初始化的时候必调用，但切换fragment的hide和visible的时候可能不会调用！
        if (isAdded() && !isHidden()) {
            //用isVisible此时为false，因为mView.getWindowToken为null
            onVisible();
            visible = true;
        }
    }

    @Override
    public void onPause() {
        if (isVisible() || visible) {
            onHidden();
            visible = false;
        }
        super.onPause();
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            onVisible();
            visible = true;
        } else {
            onHidden();
            visible = false;
        }
    }

}
