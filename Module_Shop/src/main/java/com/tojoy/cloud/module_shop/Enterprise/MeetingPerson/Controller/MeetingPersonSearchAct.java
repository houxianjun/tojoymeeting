package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.ui.activity.BaseSearchAct;
import com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Constants.AttendeeConstants;
import com.tojoy.cloud.module_shop.R;
import com.tojoy.common.App.BaseFragment;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liuhm
 * @date 2020/3/1
 * @desciption 搜索参会员工
 */
public class MeetingPersonSearchAct extends BaseSearchAct {
    MeetingPersonSearchAFragment mMeetingPersonSearchAFragment;
    private BaseFragment[] mFragments;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preSearch();
    }

    @Override
    protected void initUI() {
        super.initUI();
        setSearchHint(getString(R.string.name_id_phone));
        hideBottomLine();
    }

    private void initViewPager() {
        HashMap<String,MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> checkedModelList = (HashMap<String,MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel>) getIntent().getSerializableExtra(AttendeeConstants.ATTENDEE_REMOVE_PERSON_DATA);

        List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> employeeModelList = (List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel>) getIntent().getSerializableExtra(MeetingPersonSearchAFragment.DATA);
        Map<String, String> checkedEmployeeIdList = (Map<String, String>) getIntent().getSerializableExtra(MeetingPersonSearchAFragment.CHECKED_PERSON);
        mFragments = new BaseFragment[1];
        mMeetingPersonSearchAFragment = new MeetingPersonSearchAFragment();
        mMeetingPersonSearchAFragment.mAllEmployeeModel = employeeModelList;
        mMeetingPersonSearchAFragment.mCheckedPersonMap = checkedEmployeeIdList;
        mMeetingPersonSearchAFragment.mTmpCheckedPersonLists = checkedModelList;
        mFragments[0] = mMeetingPersonSearchAFragment;

        FragmentPagerAdapter mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments[position];
            }

            @Override
            public int getCount() {
                return 1;
            }
        };
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(1);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void doSearch() {
        super.doSearch();
        if (mMeetingPersonSearchAFragment == null) {
            initViewPager();
        }
        String searchKey = mSeachEditView.getText().toString().trim();
        ((MeetingPersonSearchAFragment) mFragments[0]).doSearch(searchKey);
    }

    public void savePersonCheckedList(HashMap<String, String> checkedList, HashMap<String,MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> tmpdatas) {
        Intent itn = new Intent();
        itn.putExtra(MeetingPersonSearchAFragment.CHECKED_PERSON, checkedList);
        itn.putExtra(AttendeeConstants.ATTENDEE_REMOVE_PERSON_DATA, tmpdatas);
        setResult(RESULT_OK, itn);
        finish();
    }

    @Override
    protected void stopSearch() {
        finish();
    }

}
