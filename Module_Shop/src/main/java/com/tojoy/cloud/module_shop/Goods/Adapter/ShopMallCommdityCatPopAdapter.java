package com.tojoy.cloud.module_shop.Goods.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.cloud.module_shop.Goods.Model.ShopMallCommdityCategoryModel;
import com.tojoy.cloud.module_shop.R;

import java.util.List;

public class ShopMallCommdityCatPopAdapter extends BaseRecyclerViewAdapter<ShopMallCommdityCategoryModel> {

    public ShopMallCommdityCatPopAdapter(@NonNull Context context, @NonNull List<ShopMallCommdityCategoryModel> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull ShopMallCommdityCategoryModel data) {
        return position;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_shop_mall_commdity_category;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new GrabCityViewHolder(context, parent, getLayoutResId(viewType));
    }

    public class GrabCityViewHolder extends BaseRecyclerViewHolder<ShopMallCommdityCategoryModel> {
        TextView tvProvinceName;
        ImageView ivSelected;

        GrabCityViewHolder(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            tvProvinceName = (TextView) findViewById(R.id.tv_commdity_category_name);
            ivSelected = (ImageView) findViewById(R.id.iv_commdity_category_selected);
        }

        @Override
        public void onBindData(ShopMallCommdityCategoryModel data, int position) {
            tvProvinceName.setText(data.title);

            if (data.isSelected) {
                tvProvinceName.setTextColor(tvProvinceName.getContext().getResources().getColor(R.color.color_FF2671E9));
                ivSelected.setVisibility(View.VISIBLE);
            } else {
                tvProvinceName.setTextColor(tvProvinceName.getContext().getResources().getColor(R.color.color_222222));
                ivSelected.setVisibility(View.GONE);
            }
        }
    }

}
