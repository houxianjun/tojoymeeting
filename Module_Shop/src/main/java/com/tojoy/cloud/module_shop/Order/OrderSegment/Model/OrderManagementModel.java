package com.tojoy.cloud.module_shop.Order.OrderSegment.Model;

/**
 * description：云洽会3期订单数据模型类
 */
public class OrderManagementModel {
    private String goodsImgurl;
    private String goodsName;
    private int goodsNum;
    private String id;
    private String orderPrice;
    private String orderState;
    private int orderStatus;
    private String storeName;
    private String goodsSellerPrice;
    private String storeAvatar;
    private String enterpriseId;
    private int orderType;
    private String orderCode;
    private String orderGoodsPrice;

    public String getEnterpriseId() {
        return enterpriseId;
    }

    public int getOrderType() {
        return orderType;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public boolean isNormalOrder() {
        return getOrderType() == 1;
    }

    public String getStoreAvatar() {
        return storeAvatar;
    }

    public String getGoodsSellerPrice() {
        return goodsSellerPrice == null ? "" : goodsSellerPrice;
    }

    public String getGoodsImgurl() {
        return goodsImgurl;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public int getGoodsNum() {
        return goodsNum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderPrice() {
        return orderGoodsPrice == null ? "" : orderGoodsPrice;
    }

    public String getOrderState() {
        return orderState;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public String getStoreName() {
        return storeName;
    }
}
