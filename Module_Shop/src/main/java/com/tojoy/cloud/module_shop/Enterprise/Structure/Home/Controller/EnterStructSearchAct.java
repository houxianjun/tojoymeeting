package com.tojoy.cloud.module_shop.Enterprise.Structure.Home.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import com.tojoy.tjoybaselib.ui.activity.BaseSearchAct;
import com.tojoy.common.App.BaseFragment;

public class EnterStructSearchAct extends BaseSearchAct {

    EnterStructSearchFragment mEnterStructSearchFragment;

    private BaseFragment[] mFragments;

    public static void start(Context context, String departId, String departPId) {
        Intent intent = new Intent();
        intent.setClass(context, EnterStructSearchAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("departId", departId);
        intent.putExtra("departPId", departPId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preSearch();
    }

    @Override
    protected void initUI() {
        super.initUI();
        setSearchHint("姓名、工号、手机号");
        hideBottomLine();
    }

    private void initViewPager() {
        String mDepartId = getIntent().getStringExtra("departId");
        String mDepartPId = getIntent().getStringExtra("departPId");
        mFragments = new BaseFragment[1];
        mEnterStructSearchFragment = new EnterStructSearchFragment();
        mEnterStructSearchFragment.mDepartId = mDepartId;
        mEnterStructSearchFragment.mDepartPId = mDepartPId;
        mFragments[0] = mEnterStructSearchFragment;

        FragmentPagerAdapter mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments[position];
            }

            @Override
            public int getCount() {
                return 1;
            }
        };
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(1);
    }

    @Override
    protected void doSearch() {
        super.doSearch();
        if (mEnterStructSearchFragment == null) {
            initViewPager();
        }
        String searchKey = mSeachEditView.getText().toString().trim();
        ((EnterStructSearchFragment) mFragments[0]).doSearch(searchKey);
    }

    @Override
    protected void stopSearch() {
        finish();
    }

}
