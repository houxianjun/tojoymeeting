package com.tojoy.cloud.module_shop.Order.OrderSegment.Controller;


import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.cloud.module_shop.Order.OrderSegment.View.OrderFragmentPagerAdapter;
import com.tojoy.cloud.module_shop.Order.utils.OrderConstants;
import com.tojoy.cloud.module_shop.Order.utils.OrderUtils;
import com.tojoy.cloud.module_shop.R;

import net.lucode.hackware.magicindicator.MagicIndicator;

/**
 * 订单管理页面
 *
 * @author yongchaowang
 */
@Route(path = RouterPathProvider.Pay_OrderManagementListAct)
public class OrderManagementListAct extends UI {
	private int[] mType = {OrderConstants.ORDER_STATUS_ALL, OrderConstants.ORDER_STATUS_WAIT_PAY, OrderConstants.ORDER_STATUS_WAIT_EMAIL, OrderConstants.ORDER_STATUS_WAIT_RECEIVING, OrderConstants.ORDER_STATUS_DONE};
	private String[] mTitles = {"全部", "待付款", "待发货", "待收货", "已完成"};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_order_segment2);
		initTitle();
		initUI();
		initData();
	}

	private void initTitle() {
		setStatusBar();
		setUpNavigationBar();
		showBack();
		mTitle.setText(getResources().getString(R.string.order_mine));
		mTitle.setTextColor(ContextCompat.getColor(this, R.color.color_353f5c));
	}

	private void initUI() {
		MagicIndicator mMagicIndicator = (MagicIndicator) findViewById(R.id.order_magic_indicator);
		ViewPager mViewPager = (ViewPager) findViewById(R.id.viewpager);
		mViewPager.setOffscreenPageLimit(5);
		OrderFragmentPagerAdapter mAdapter = new OrderFragmentPagerAdapter(this, getSupportFragmentManager(), mType);
		mViewPager.setAdapter(mAdapter);
		mViewPager.setCurrentItem(getIntent().getIntExtra("index", 0));
		OrderUtils.initIndicator(this, mTitles, mViewPager, mMagicIndicator);

	}

	private void initData() {
	}

}
