package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.cloud.module_shop.R;

import java.util.List;

/**
 * @author houxianjun
 * @date 2020/5/11
 * @desciption 外部联系人列表
 */
public class MeetingShowOutSidePersonAdapter extends BaseRecyclerViewAdapter<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> {
    /**
     * viewType--分别为item以及空view
     */
    private static final int VIEW_TYPE_ITEM = 1;
    private static final int VIEW_TYPE_EMPTY = 0;

    public MeetingShowOutSidePersonAdapter(@NonNull Context context, @NonNull List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> datas) {
        super(context, datas);
    }

    //如果增加空view需要重写
    @Override
    public int getItemViewType(int position) {

        return getViewType(position, null);
    }

    @Override
    protected int getViewType(int position, @NonNull MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel data) {
        if (datas.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        //如果不为0，按正常的流程跑
        return VIEW_TYPE_ITEM;

    }

    @Override
    public int getItemCount() {
        //添加判断，如果mData.size()为0的话，只引入一个布局，就是emptyView
        // 那么，这个recyclerView的itemCount为1
        if (datas.size() == 0) {
            return 1;
        }
        //如果不为0，按正常的流程跑
        return datas.size();
    }

    @Override
    protected int getLayoutResId(int viewType) {
        int layoutId = R.layout.item_outside_attendee_person_empty;
        switch (viewType) {
            case VIEW_TYPE_ITEM:
                layoutId = R.layout.item_meeting_person;
                break;
            case VIEW_TYPE_EMPTY:
                layoutId = R.layout.item_outside_attendee_person_empty;
                break;
            default:
                break;
        }
        return layoutId;
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> holder, int position) {
        //数据为空时不重写父类方法
        if (datas != null && datas.size() != 0) {
            super.onBindViewHolder(holder, position);
        }
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        //在这里根据不同的viewType进行引入不同的布局
        if (viewType == VIEW_TYPE_EMPTY) {
            MeetingEmptyPersonSelectViewHolder emptyHolder = new MeetingEmptyPersonSelectViewHolder(context, parent, getLayoutResId(viewType));
            return emptyHolder;
        }
        return new MeetingPersonSelectViewHolder(context, parent, getLayoutResId(viewType));
    }

    /**
     * 空数据时显示
     */
    public class MeetingEmptyPersonSelectViewHolder extends BaseRecyclerViewHolder<String> {
        private TextView content;

        MeetingEmptyPersonSelectViewHolder(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            content = (TextView) findViewById(R.id.tv_empty_person);
        }

        @Override
        public void onBindData(String data, int position) {
            content.setText("还未添加外部联系人");
        }
    }

    public class MeetingPersonSelectViewHolder extends BaseRecyclerViewHolder<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> {

        private TextView tvName, tvPhone;
        private CheckBox checkBox;


        MeetingPersonSelectViewHolder(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            checkBox = (CheckBox) findViewById(R.id.cb);
            tvName = (TextView) findViewById(R.id.tvName);
            tvPhone = (TextView) findViewById(R.id.tvPhone);
        }

        @Override
        public void onBindData(MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel data, int position) {
            checkBox.setChecked(data.isChecked);
            tvName.setText(data.trueName);
            tvPhone.setText(data.mobile);
            checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
                if (compoundButton.isPressed() && onCheckBoxListener != null) {
                    onCheckBoxListener.onPersonCheckListener(data, b);
                }
            });
        }
    }

    /**
     * 重写父类更新数据方法
     *
     * @param tmpdatas
     */
    @Override
    public void updateData(List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> tmpdatas) {
        this.datas = tmpdatas;
        notifyDataSetChanged();
    }

    private OnPersonCheckBoxListener onCheckBoxListener;

    public void setOnCheckListener(OnPersonCheckBoxListener checkListener) {
        this.onCheckBoxListener = checkListener;
    }

    public interface OnPersonCheckBoxListener {
        void onPersonCheckListener(MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel data, boolean isChecked);
    }
}

