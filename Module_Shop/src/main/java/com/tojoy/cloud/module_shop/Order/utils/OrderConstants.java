package com.tojoy.cloud.module_shop.Order.utils;

/**
 * Created by yongchaowang
 * on 2020-03-01.
 */
public class OrderConstants {
	//*订单状态（10：待付款、20：待发货、30：待收货、40：已收货、50：已完成、60：已取消、70：已超时）
	public final static int ORDER_STATUS_ALL = 0;
	public final static int ORDER_STATUS_WAIT_PAY = 10;
	public final static int ORDER_STATUS_WAIT_EMAIL = 20;
	public final static int ORDER_STATUS_WAIT_RECEIVING = 30;
	public final static int ORDER_STATUS_WAIT_RECEIVED = 40;
	public final static int ORDER_STATUS_DONE = 50;
	public final static int ORDER_STATUS_CANCELED = 60;
	public final static int ORDER_STATUS_OVER_TIME = 70;

	/**
	 * 1:支付成功：刷新待支付、全部、待发货
	 * 2:关闭:刷新待支付、全部
	 * 3.取消:刷新待支付、全部
	 * 4.确认收货：刷新待收货、已完成
	 */
	public final static String PAY_STATUS_SUCCESS = "1";
	public final static String PAY_STATUS_CLOSE = "2";
	public final static String PAY_STATUS_CANCEL = "3";
	public final static String PAY_STATUS_CONFIRM = "4";
}
