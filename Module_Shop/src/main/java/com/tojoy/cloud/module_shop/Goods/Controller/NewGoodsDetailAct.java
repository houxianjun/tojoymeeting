package com.tojoy.cloud.module_shop.Goods.Controller;

import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.common.LiveRoom.MeetingExtensionPopView;
import com.tojoy.common.Services.Distribution.DistrbutionHelper;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.html.HtmlContainer;
import com.tojoy.tjoybaselib.html.HtmlContentProvider;
import com.tojoy.tjoybaselib.html.HtmlItem;
import com.tojoy.tjoybaselib.model.enterprise.NewGoodsDetailResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigCacheManager;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigConstantKey;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.vlayout.VpSwipeRefreshLayout;
import com.tojoy.tjoybaselib.util.log.LogUtil;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.string.StringUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.ShopActAtack;
import com.tojoy.tjoybaselib.util.sys.StatusbarUtils;
import com.tojoy.cloud.module_shop.Goods.View.GoodsDetailBanner;
import com.tojoy.cloud.module_shop.Goods.View.MyScrollView;
import com.tojoy.cloud.module_shop.R;
import com.tojoy.live.floatpalyer.FloatEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;


/**
 * @author qululu
 * 独立App----商品详情（新版：商城跳转）
 */
@Route(path = RouterPathProvider.GoodsDetailNew)
public class NewGoodsDetailAct extends UI {
    private RelativeLayout mBannerRlv;
    private VpSwipeRefreshLayout mSwipeRefreshLayout;
    private GoodsDetailBanner mGoodsDetailBanner;

    private TextView mNowAmountTv;
    private TextView mNowAmountFloatTv;
    private TextView mOldAmountTv;
    private TextView mGoodsTitleTv;
    private TextView mGoodsSubTitleTv;

    float changeHeight;
    private NewGoodsDetailResponse mGoodsDetailData;
    private String mDetailId;
    private List<String> mBannerList = new ArrayList<>();

    private RelativeLayout mRlvGoodsWebDetail;
    private TextView mTvWantBuy;
    private ImageView mInterestedIv;
    private TextView mInterestedTv;

    private TextView mDistributionMoneyTv;
    private LinearLayout mDistributionLlv;

    private String mRoomLiveId;
    private String mRoomId;
    //数据统计二期新增页面来源 0 直播间 1 回放 2 企业主页
    private String sourceWay,agentCompanyCode,agentId,agentLive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods_detail_new);
        setStatusBar();
        initUI();
        initTitle();
        initListener();
        requestData();
        changeHeight = (AppUtils.dip2px(this, 48) + AppUtils.getStatusBarHeight(this)) * 1.0f;
        ShopActAtack.getActivityStack().pushActivity(this);
    }

    private void initUI() {
        mBannerRlv = (RelativeLayout) findViewById(R.id.rlv_banner_container);
        mSwipeRefreshLayout = (VpSwipeRefreshLayout) findViewById(R.id.swiprefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.color_0f88eb);
        mRlvGoodsWebDetail = (RelativeLayout) findViewById(R.id.rlv_goods_web_detail);
        mNowAmountTv = (TextView) findViewById(R.id.tv_money_int);
        mNowAmountFloatTv = (TextView) findViewById(R.id.tv_money_float);
        mOldAmountTv = (TextView) findViewById(R.id.tv_goods_money_old);
        mGoodsTitleTv = (TextView) findViewById(R.id.tv_goods_name);
        mGoodsSubTitleTv = (TextView) findViewById(R.id.tv_goods_subtitle);
        mTvWantBuy = (TextView) findViewById(R.id.tv_want_buy);
        mInterestedIv = (ImageView) findViewById(R.id.iv_interested);
        mInterestedTv = (TextView) findViewById(R.id.tv_interested);
        mDistributionMoneyTv = (TextView) findViewById(R.id.tv_distribution_money);
        mDistributionLlv = (LinearLayout) findViewById(R.id.llv_distribution_goods);

        mGoodsDetailBanner = new GoodsDetailBanner(this);
        mBannerRlv.addView(mGoodsDetailBanner.getView(), mGoodsDetailBanner.getLayoutParams());

        mDetailId = getIntent().getStringExtra("detailId");
        mRoomLiveId = getIntent().getStringExtra("roomLiveId");
        mRoomId = getIntent().getStringExtra("roomId");
        sourceWay = getIntent().getStringExtra("sourceWay");
        agentCompanyCode= getIntent().getStringExtra("agentCompanyCode");
        agentId= getIntent().getStringExtra("agentId");
        agentLive= getIntent().getStringExtra("agentLive");
        LogUtil.d("代播商品","商品详情 agentCompanyCode=" +agentCompanyCode +"agentId="+agentId
       +"agentLive="+agentLive +"sourceWay:"+sourceWay);
        ((MyScrollView) findViewById(R.id.rlv_scroll)).setOnScrollListener(scrollY -> {
            if (scrollY > changeHeight) {
                findViewById(R.id.rlv_bg_title).setAlpha(1);
                findViewById(R.id.rlv_back_title).setAlpha(0);
            } else {
                findViewById(R.id.rlv_bg_title).setAlpha(scrollY / changeHeight);
                findViewById(R.id.rlv_back_title).setAlpha(1 - (scrollY / changeHeight));
            }

            if (scrollY < 50) {
                setStatusBarLightMode();
            } else {
                setStatusBarDarkMode();
            }
        });

        String isFrom = getIntent().getStringExtra("isFromLive");
        if (!TextUtils.isEmpty(isFrom) && "1".equals(isFrom)) {
            // 从直播间跳过来的，打开小窗口播放
            EventBus.getDefault().post(new FloatEvent(true));
        }

    }

    protected void initTitle() {
        StatusbarUtils.enableTranslucentStatusbar(this);
        StatusbarUtils.setStatusBarLightMode(this, StatusbarUtils.statusbarlightmode(this));
        int statusBarHeight = AppUtils.getStatusBarHeight(this);
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.dip2px(this, 48));
        lps.setMargins(0, statusBarHeight, 0, 0);
        findViewById(R.id.rlv_back_title).setLayoutParams(lps);

        findViewById(R.id.rlv_title_sub).setLayoutParams(lps);

        RelativeLayout.LayoutParams lps2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, statusBarHeight + AppUtils.dip2px(this, 48));
        findViewById(R.id.rlv_bg_title).setLayoutParams(lps2);
    }

    private void initListener() {
        mSwipeRefreshLayout.setOnRefreshListener(this::requestData);

        findViewById(R.id.iv_goods_detail_leftimg).setOnClickListener(v -> {
            finish();
        });

        findViewById(R.id.rlv_pay).setOnClickListener(v -> {
            if (mGoodsDetailData != null && mGoodsDetailData.data != null) {
                if (!mGoodsDetailData.data.isShelf()) {
                    Toasty.normal(this, "该商品已下架").show();
                    return;
                }
                if (!mGoodsDetailData.data.isChackSuccess()) {
                    Toasty.normal(this, "该商品审核未通过").show();
                    return;
                }
                if (!mGoodsDetailData.data.isCanGoMakeOrder()) {
                    Toasty.normal(this, "该商品库存不足").show();
                    return;
                }
            }
            RouterJumpUtil.startOderSureActivity(mDetailId, TextUtils.isEmpty(mRoomLiveId) ? "" : mRoomLiveId,sourceWay!= null ? sourceWay : "2",agentCompanyCode,agentId,agentLive);
        });

        findViewById(R.id.rlv_interested).setOnClickListener(v -> {
            if (FastClickAvoidUtil.isFastDoubleClick(R.id.rlv_interested)) {
                return;
            }

            if (mGoodsDetailData.data.isIntention == 1) {
                return;
            }
            OMAppApiProvider.getInstance().interestedV2(mDetailId, mRoomId, mRoomLiveId, agentCompanyCode,agentId,agentLive,new Observer<OMBaseResponse>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    Toasty.normal(NewGoodsDetailAct.this, "网络错误，请检查网络").show();
                }

                @Override
                public void onNext(OMBaseResponse response) {
                    if (response.isSuccess()) {
                        mInterestedIv.setImageResource(R.drawable.icon_goods_interested_selected);
                        mInterestedTv.setTextColor(getResources().getColor(R.color.color_ff535d));
                        mGoodsDetailData.data.isIntention = 1;
                    } else {
                        Toasty.normal(NewGoodsDetailAct.this, response.msg).show();
                    }
                }
            });
        });
    }

    /**
     * 设置Banner数据
     *
     * @param bannerDataList
     */
    public void setBannerData(List<String> bannerDataList) {
        if (mGoodsDetailBanner == null) {
            return;
        }
        if (mGoodsDetailBanner.getView().getVisibility() != View.VISIBLE) {
            mGoodsDetailBanner.getView().setVisibility(View.VISIBLE);
        }
        mBannerRlv.setVisibility(View.VISIBLE);
        mGoodsDetailBanner.setViewData(bannerDataList);
    }

    private void requestData() {
        showProgressHUD(this, "");
        OMAppApiProvider.getInstance().mNewGoodsDetail(mDetailId, new Observer<NewGoodsDetailResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
                mSwipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onError(Throwable e) {
                dismissProgressHUD();
                mSwipeRefreshLayout.setRefreshing(false);
                mBannerList.clear();
                mBannerList.add("");
                setBannerData(mBannerList);
                Toasty.normal(NewGoodsDetailAct.this, "网络异常，稍后再试").show();
            }

            @Override
            public void onNext(NewGoodsDetailResponse omBaseResponse) {
                dismissProgressHUD();
                if (omBaseResponse.isSuccess()) {
                    if (omBaseResponse.data == null) {
                        return;
                    }
                    mGoodsDetailData = omBaseResponse;
                    mBannerList.clear();
                    if (omBaseResponse.data.imgList != null && omBaseResponse.data.imgList.size() > 0) {
                        for (int i = 0; i < omBaseResponse.data.imgList.size(); i++) {
                            mBannerList.add(omBaseResponse.data.imgList.get(i).largeImgUrl);
                        }
                    } else {
                        mBannerList.add("");
                    }
                    setBannerData(mBannerList);
                    setGoodsBasicMsg(omBaseResponse);
                    setGoodsWebDetails(omBaseResponse.data.content);

                } else {
                    Toasty.normal(NewGoodsDetailAct.this, TextUtils.isEmpty(omBaseResponse.msg) ? "网络异常，稍后再试" : omBaseResponse.msg).show();
                    if ("-2".equals(omBaseResponse.code)) {
                        finish();
                    }
                }

            }
        });
        addGoodsVisit();
    }


    /**
     * 记录商品游览量
     */
    private void addGoodsVisit() {
        OMAppApiProvider.getInstance().mAddVisit(mDetailId, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(OMBaseResponse omBaseResponse) {

            }
        });
    }

    /**
     * 设置商品基本信息：价格、历史价格、标题
     */
    public void setGoodsBasicMsg(NewGoodsDetailResponse response) {
        if (response == null || response.data == null) {
            return;
        }
        TextView moneyNowSymbolTv = (TextView) findViewById(R.id.tv_money_now_symbol);
        moneyNowSymbolTv.setVisibility(View.VISIBLE);
        findViewById(R.id.rlv_goods_detail_bottom_root).setVisibility(View.VISIBLE);

        if (response.data.isIntentOder()) {
            mTvWantBuy.setText("意向购买");
        } else {
            if (response.data.isDeposit == 1) {
                mTvWantBuy.setText("预付定金");
            } else {
                String wantBuytitle = TextConfigCacheManager.getInstance(this).getWidgetTextByKey(TextConfigConstantKey.PRODUCT3);
                mTvWantBuy.setText(TextUtils.isEmpty(wantBuytitle) ? "立即购买" : wantBuytitle);
            }
        }

        if (response.data.isIntention == 1) {
            // 已感兴趣
            mInterestedIv.setImageResource(R.drawable.icon_goods_interested_selected);
            mInterestedTv.setTextColor(getResources().getColor(R.color.color_ff535d));
        } else {
            // 未感兴趣
            mInterestedIv.setImageResource(R.drawable.icon_goods_interested_normal);
            mInterestedTv.setTextColor(getResources().getColor(R.color.color_91969e));
        }

        mNowAmountTv.setText(StringUtil.getFormatedIntString(response.data.getSalePrice()));
        mNowAmountFloatTv.setText(StringUtil.getFormatedMinString(response.data.getSalePrice()));
        // 不区分是否是意向订单
        if (response.data.isDeposit == 1) {
            moneyNowSymbolTv.setText("定金 " + getResources().getString(R.string.order_yuan));
        } else {
            moneyNowSymbolTv.setText(getResources().getString(R.string.order_yuan));
        }

        if (!TextUtils.isEmpty(response.data.marketPrice)) {
            mOldAmountTv.setVisibility(View.VISIBLE);
            mOldAmountTv.setText(getResources().getString(R.string.order_yuan) + StringUtil.getInsertedString(response.data.marketPrice, 1f));
            mOldAmountTv.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            mOldAmountTv.setVisibility(View.GONE);
        }
        mGoodsTitleTv.setText(response.data.name);
        ((TextView) findViewById(R.id.tv_pn_cetener)).setText(response.data.name);

        if (!TextUtils.isEmpty(response.data.subhead)) {
            mGoodsSubTitleTv.setVisibility(View.VISIBLE);
            mGoodsSubTitleTv.setText(response.data.subhead);
        }


        if (DistrbutionHelper.isShowDistribution(this, response.data.ifDistribution, response.data.companyCode)) {
            mDistributionLlv.setVisibility(View.VISIBLE);
            mDistributionMoneyTv.setText(DistrbutionHelper.getMaxBudgetText(this
                    , response.data.level1Commission
                    , response.data.level2Commission
                    , response.data.level1CommissionEmployee
                    , response.data.level2CommissionEmployee
                    , response.data.companyCode,false));
        } else {
            mDistributionLlv.setVisibility(View.GONE);
        }
        mDistributionLlv.setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isFastDoubleClick(mDistributionLlv.getId())) {
                if (DistrbutionHelper.isNeedJumpPop(this, response.data.level1CommissionEmployee, response.data.level2CommissionEmployee, response.data.companyCode)) {
                    MeetingExtensionPopView popView = new MeetingExtensionPopView(this);
                    popView.setGoodsData(response.data.id
                            , response.data.name
                            , !TextUtils.isEmpty(response.data.imgurl) ? response.data.imgurl : response.data.liveRoomCoverImgUrl
                            , DistrbutionHelper.getDistributionMoney(this
                                    , response.data.level1Commission
                                    , response.data.level2Commission
                                    , response.data.level1CommissionEmployee
                                    , response.data.level2CommissionEmployee
                                    , response.data.companyCode)
                            , response.data.minPerson
                            , response.data.minViewTime
                            , response.data.salePrice);
                    popView.show();
                } else {
                    // 判断二维码内容是否为空，为空生成海报没有意义
                    if (!TextUtils.isEmpty(BaseUserInfoCache.getUserInviteUrl(this))) {
                        ARouter.getInstance()
                                .build(RouterPathProvider.ONLINE_MEETING_EXTEN_SION_POSTER)
                                .withInt("fromType", 2)
                                .withString("goodsId", response.data.id)
                                .withString("goodsTitle", response.data.name)
                                .withString("goodsPrice", response.data.salePrice)
                                .withString("mLiveCoverUrl", !TextUtils.isEmpty(response.data.imgurl) ? response.data.imgurl : response.data.liveRoomCoverImgUrl)
                                .navigation();
                    } else {
                        Toasty.normal(this, this.getResources().getString(R.string.extension_qr_error)).show();
                    }
                }
            }
        });

    }

    public void setGoodsWebDetails(String content) {
        if (!TextUtils.isEmpty(content)) {
            mRlvGoodsWebDetail.removeAllViews();
            mRlvGoodsWebDetail.addView(new HtmlContainer(this, getHtmlItems(content), AppUtils.getScreenWidth(this)).getView());
        }

    }

    public ArrayList<HtmlItem> mContent;

    public ArrayList<HtmlItem> getHtmlItems(String content) {
        if (mContent == null) {
            mContent = HtmlContentProvider.getHtmlItems(content);
        }
        return mContent;
    }
}
