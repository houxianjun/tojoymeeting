package com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.View;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.module_shop.R;

/**
 * 批量新增员工已有提示
 * Created by luuzhu on 2020/3/12.
 */

public class UploadStaffDialog {

    private String info;
    private Dialog mDialog;

    private Activity mContext;
    private View mView;

    public UploadStaffDialog(String info, Context context) {
        mContext = (Activity) context;
        this.info = info;
        initUI();
    }

    @SuppressLint("WrongConstant")
    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_upload_staff, null);
        initView(mView);
    }

    public void show() {

        mDialog = new Dialog(mContext, R.style.LiveDialogStopLive);
        mDialog.setContentView(mView);

        Window win = mDialog.getWindow();
        WindowManager.LayoutParams lp = win.getAttributes();
//        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = AppUtils.getWidth(mContext) - AppUtils.dip2px(mContext, 112);
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = AppUtils.dip2px(mContext, 160);
        lp.windowAnimations = R.style.CenterInAndOutStyle;
        lp.gravity = Gravity.CENTER;
        win.setAttributes(lp);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.show();
    }

    public boolean isShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    private void initView(View view) {

        ((TextView) (mView.findViewById(R.id.tvInfo))).setText(info);

        mView.findViewById(R.id.tvSure).setOnClickListener(view1 -> {
            if (mDialog != null) {
                mDialog.dismiss();
            }
        });
    }
}
