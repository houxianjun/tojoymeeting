package com.tojoy.cloud.module_shop.Enterprise.Home.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.model.enterprise.BrowsedEnterpriseResponse;
import com.tojoy.tjoybaselib.model.enterprise.CorporationMsgResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.cloud.module_shop.Enterprise.Home.View.BrowsedEnterpriseAdapter;
import com.tojoy.cloud.module_shop.R;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 浏览过的企业
 */
@Route(path = RouterPathProvider.BrowsedEnterprise)
public class BrowsedEnterpriseActivity extends UI {
    private BrowsedEnterpriseAdapter mAdapter;
    private TJRecyclerView mRecyclerView;
    private List<BrowsedEnterpriseResponse.BrowsedEnterpriseModel.EnterprisListBean> mData = new ArrayList<>();
    //页面来源
    @Autowired(name = "pagefrom")
    String pagefrom;
    @Autowired(name = "title")
    String mtitle;

    String mPageSize = 20 + "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browsed_enterprise);
        ARouter.getInstance().inject(this);
        pagefrom = getIntent().getStringExtra("pagefrom");
        initTitle();
        initView();
    }

    private void initTitle() {
        setStatusBar();
        setUpNavigationBar();
        //来源是首页
        if ("MeetingFragment".equals(pagefrom)) {
            setTitle(mtitle);
        } else {
            setTitle(R.string.saw_enterprise);
        }

        showBack();
        findViewById(R.id.base_title_line).setVisibility(View.GONE);
    }

    private void initView() {
        RelativeLayout mContainerLayout = (RelativeLayout) findViewById(R.id.container_layout);
        mRecyclerView = new TJRecyclerView(this);
        mContainerLayout.addView(mRecyclerView);
        mAdapter = new BrowsedEnterpriseAdapter(this, mData);
        mRecyclerView.setLoadMoreViewWithHide();
        mRecyclerView.setEmptyImgAndTip(R.drawable.icon_empty_online_logo, getString(R.string.instant_no_content));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                initData();
            }

            @Override
            public void onLoadMore() {
                if (mData.isEmpty()) {
                    return;
                }
                mPage++;
                initData();
            }
        });
        mAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                getCompanyStatus(data.companyCode);
            }
        });

        findViewById(R.id.rlv_search_enterprise).setOnClickListener(v -> ARouter.getInstance().build(RouterPathProvider.BrowsedEnterpriseSearchAct)
                .withString("pagefrom", pagefrom)
                .navigation());

        mRecyclerView.autoRefresh();
    }

    private void initData() {
        if ("MeetingFragment".equals(pagefrom)) {
            getHotCommpanyList();
        } else {
            getBrowsedEnterprise();
        }
    }

    /**
     * 获取公司是否有定制企业状态
     */
    private void getCompanyStatus(String companyCode) {
        showProgressHUD(this, "处理中...");
        OMAppApiProvider.getInstance().mCorporationQuery(companyCode, false, new rx.Observer<CorporationMsgResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(BrowsedEnterpriseActivity.this, "网络错误，请检查网络").show();
                dismissProgressHUD();
            }

            @Override
            public void onNext(CorporationMsgResponse omBaseResponse) {
                if (omBaseResponse != null && omBaseResponse.isSuccess()) {
                    if ("1".equals(String.valueOf(omBaseResponse.data.customState)) && companyCode != null) {
                        ARouter.getInstance().build(RouterPathProvider.OnlineMeetingCustomizedEnterprisePageAct)
                                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                                .withString("companyCode", companyCode)
                                .withString("sourceWay","2")
                                .navigation();
                    } else {
                        Toasty.normal(BrowsedEnterpriseActivity.this, "抱歉，该企业未配置企业主页").show();
                    }
                } else {
                    Toasty.normal(BrowsedEnterpriseActivity.this, omBaseResponse.msg).show();
                }

            }
        });
    }


    //获取更多热门企业
    public void getHotCommpanyList() {

        OMAppApiProvider.getInstance().getHotCompanyMoreList("", mPage + "", mPageSize, new Observer<BrowsedEnterpriseResponse>() {
            @Override
            public void onCompleted() {
                mRecyclerView.setRefreshing(false);
            }

            @Override
            public void onError(Throwable e) {
                mRecyclerView.setRefreshing(false);
                Toasty.normal(BrowsedEnterpriseActivity.this, "网络异常，稍后再试").show();
            }

            @Override
            public void onNext(BrowsedEnterpriseResponse response) {
                if (response.isSuccess()) {
                    if (mPage == 1) {
                        mData.clear();
                        mData.addAll(response.data.records);
                        mAdapter.updateData(mData);
                    } else {
                        mData.addAll(response.data.records);
                        mAdapter.addMore(response.data.records);
                    }
                    mRecyclerView.showFooterView();
                    mRecyclerView.refreshLoadMoreView(mPage, response.data.records.size());
                } else {
                    Toasty.normal(BrowsedEnterpriseActivity.this, response.msg).show();
                }
            }
        });

    }

    //获取浏览过的企业
    public void getBrowsedEnterprise() {
        OMAppApiProvider.getInstance().browsedEnterprise(null, mPage, new Observer<BrowsedEnterpriseResponse>() {
            @Override
            public void onCompleted() {
                mRecyclerView.setRefreshing(false);
            }

            @Override
            public void onError(Throwable e) {
                mRecyclerView.setRefreshing(false);
                Toasty.normal(BrowsedEnterpriseActivity.this, "网络异常，稍后再试").show();
            }

            @Override
            public void onNext(BrowsedEnterpriseResponse response) {
                if (response.isSuccess()) {
                    if (mPage == 1) {
                        mData.clear();
                        mData.addAll(response.data.list);
                        mAdapter.updateData(mData);
                    } else {
                        mData.addAll(response.data.list);
                        mAdapter.addMore(response.data.list);
                    }
                    mRecyclerView.showFooterView();
                    mRecyclerView.refreshLoadMoreView(mPage, response.data.list.size());
                } else {
                    Toasty.normal(BrowsedEnterpriseActivity.this, response.msg).show();
                }
            }
        });

    }
}
