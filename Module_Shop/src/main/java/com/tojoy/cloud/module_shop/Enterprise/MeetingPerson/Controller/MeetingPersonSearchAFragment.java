package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Controller;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.model.meetingperson.SingleTonModel.MeetingPersonTon;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.string.StringUtil;
import com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Adapter.MeetingPersonSelectAdapter;
import com.tojoy.cloud.module_shop.R;
import com.tojoy.common.App.TJBaseApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.dmoral.toasty.Toasty;

/**
 * @author liuhm
 * @date 2020/3/1
 * @desciption 搜索参会员工
 */
public class MeetingPersonSearchAFragment extends MeetingPersonBaseFragment implements MeetingPersonSelectAdapter.OnPersonCheckBoxListener {
    public static final String DATA = "DATA";
    public static final String CHECKED_PERSON = "CHECKED_PERSON";
    private RelativeLayout rlContainTip;
    private RecyclerView tjRecyclerView;
    private MeetingPersonSelectAdapter mAdapter;
    //所有已被选择员工集合Map
    public Map<String, String> mCheckedPersonMap;
    private HashMap<String, String> mTmpCheckedPersonMap = new HashMap<>();
    //已选参会人
    public HashMap<String, MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> mTmpCheckedPersonLists = new HashMap<>();

    private List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> mSearchEmployeeModel = new ArrayList<>();
    //所有员工集合
    public List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> mAllEmployeeModel;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_meeting_person_searcha, container, false);
        }
        return mView;
    }

    @Override
    protected void initUI() {
        super.initUI();
        TextView tvConfirm = mView.findViewById(R.id.tvConfirm);
        tjRecyclerView = mView.findViewById(R.id.tjRecyclerView);
        rlContainTip = mView.findViewById(R.id.rlContainTip);
        tjRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        tvConfirm.setOnClickListener(view -> {
            if (getActivity() instanceof MeetingPersonSearchAct) {
                if (mTmpCheckedPersonMap.size() > MeetingPersonTon.getInstance().maxInvitation) {
                    Toasty.normal(getActivity(), String.format(getString(R.string.t_max_meeting_person), MeetingPersonTon.getInstance().maxInvitation)).show();
                    return;
                }
                for (MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel model : mAllEmployeeModel) {
                    model.isChecked = mTmpCheckedPersonMap.containsKey(model.userId);
                }
                List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> lis = MeetingPersonTon.getInstance().outSidePersonList;
                for (MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel model : lis) {
                    model.isChecked = mTmpCheckedPersonMap.containsKey(model.userId);
                }
                ((MeetingPersonSearchAct) getActivity()).savePersonCheckedList(mTmpCheckedPersonMap, mTmpCheckedPersonLists);
            }
        });
    }

    public void doSearch(String content) {
        if (StringUtil.noEmpty(content)) {
            mSearchEmployeeModel.clear();
            mTmpCheckedPersonMap.clear();
            for (int i = 0; i < mAllEmployeeModel.size(); i++) {
                if (mAllEmployeeModel.get(i).trueName.contains(content) || mAllEmployeeModel.get(i).mobile.contains(content) || ((mAllEmployeeModel.get(i).employeeId != null) && mAllEmployeeModel.get(i).employeeId.contains(content))) {
                    mSearchEmployeeModel.add(mAllEmployeeModel.get(i));
                }
            }
            setCheckedStatus();
            mTmpCheckedPersonMap.putAll(mCheckedPersonMap);
            new Handler().postDelayed(() -> tjRecyclerView.post(() -> {
                if (mSearchEmployeeModel != null && !mSearchEmployeeModel.isEmpty()) {
                    mAdapter = new MeetingPersonSelectAdapter(TJBaseApp.getInstance(), mSearchEmployeeModel);
                    mAdapter.setOnCheckListener(MeetingPersonSearchAFragment.this);
                    tjRecyclerView.setAdapter(mAdapter);
                    mAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
                        if (!FastClickAvoidUtil.isDoubleClick()) {
                            CheckBox checkBox = v.findViewById(R.id.cb);
                            if (checkBox.isChecked()) {
                                checkBox.setChecked(false);
                            } else {
                                checkBox.setChecked(true);
                            }
                            onPersonCheckListener(mSearchEmployeeModel.get(position), checkBox.isChecked());
                        }
                    });
                    visibleListData(true);
                } else {
                    visibleListData(false);
                }
            }), 300);

        } else {
            Toast.makeText(getActivity(), R.string.t_name_id_phone, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Checkbox选中状态监听
     *
     * @param data
     * @param isChecked
     */
    @Override
    public void onPersonCheckListener(MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel data, boolean isChecked) {
        data.isChecked = isChecked;
        if (isChecked) {
            mTmpCheckedPersonMap.put(data.userId, data.mobile);
            //不存在就添加新的
            if (!mTmpCheckedPersonLists.containsKey(data.userId)) {
                mTmpCheckedPersonLists.put(data.userId, data);
            }
        } else {
            mTmpCheckedPersonMap.remove(data.userId);
            mTmpCheckedPersonLists.remove(data.userId);
        }
    }

    /**
     * 设置列表中Checkbox默认选中状态
     */
    private void setCheckedStatus() {
        Set<String> checkedSet = mCheckedPersonMap.keySet();
        if (mSearchEmployeeModel != null && !mSearchEmployeeModel.isEmpty()) {
            for (int i = 0; i < mSearchEmployeeModel.size(); i++) {
                if (checkedSet.isEmpty()) {
                    mSearchEmployeeModel.get(i).isChecked = false;
                } else {
                    mSearchEmployeeModel.get(i).isChecked = checkedSet.contains(mSearchEmployeeModel.get(i).userId);
                }
            }
        }
    }

    private void visibleListData(boolean visible) {
        if (visible) {
            rlContainTip.setVisibility(View.GONE);
            tjRecyclerView.setVisibility(View.VISIBLE);
        } else {
            rlContainTip.setVisibility(View.VISIBLE);
            tjRecyclerView.setVisibility(View.GONE);
        }
    }

}
