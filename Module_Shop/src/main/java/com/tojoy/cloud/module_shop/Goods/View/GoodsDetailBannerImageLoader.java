package com.tojoy.cloud.module_shop.Goods.View;

import android.content.Context;
import android.widget.ImageView;

import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.ui.Banner.loader.ImageLoader;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.module_shop.R;


public class GoodsDetailBannerImageLoader extends ImageLoader {
    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {
        int widht = AppUtils.getWidth(context);
        int height = (int) (widht * 334.0f / 375.0f);
        ImageLoaderManager.INSTANCE.loadSizedImage(context, imageView, OSSConfig.getOSSURLedStr((String) path), 0, 0, widht, height);
    }
}
