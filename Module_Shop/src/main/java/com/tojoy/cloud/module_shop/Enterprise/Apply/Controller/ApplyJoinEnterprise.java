package com.tojoy.cloud.module_shop.Enterprise.Apply.Controller;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.user.CityModel;
import com.tojoy.tjoybaselib.model.user.CountyModel;
import com.tojoy.tjoybaselib.model.user.ProvinceModel;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.util.string.RegixUtil;
import com.tojoy.tjoybaselib.util.sys.KeyboardControlUtil;
import com.tojoy.cloud.module_shop.Api.ShopModuleApiProvider;
import com.tojoy.cloud.module_shop.Enterprise.Apply.Model.EnterParamsResponse;
import com.tojoy.cloud.module_shop.R;
import com.tojoy.common.Widgets.AddressPicker.NewAddreassPickview;
import com.tojoy.common.Widgets.CommonParamsPop.CommonParam;
import com.tojoy.common.Widgets.CommonParamsPop.CommonParamsPop;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import rx.Observer;

@Route(path = RouterPathProvider.ApplyEnterprise)
public class ApplyJoinEnterprise extends UI {

    EditText mEdAddName;
    EditText mEdAddContact;

    //城市
    TextView mEdAddAddress;
    ProvinceModel provinceModel;
    CityModel cityModel;
    CountyModel countyModel;
    NewAddreassPickview addreassPickview;

    //规模
    TextView mEdAddSize;
    String mCurrentSizeCode;
    ArrayList<CommonParam> mSizeParams = new ArrayList<>();

    //职位
    TextView mEdAddPos;
    String mCurrentPosCode;
    ArrayList<CommonParam> mPosParams = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_enterprise);
        setStatusBar();
        initView();
        initData();
        initListener();
        setSwipeBackEnable(false);
    }

    private void initView() {
        setUpNavigationBar();
        showBack();
        setTitle("企业入驻申请");

        mEdAddName = (EditText) findViewById(R.id.ed_add_name);
        mEdAddContact = (EditText) findViewById(R.id.ed_add_contact);
        mEdAddAddress = (TextView) findViewById(R.id.ed_add_address);
        mEdAddSize = (TextView) findViewById(R.id.ed_add_size);
        mEdAddPos = (TextView) findViewById(R.id.ed_add_pos);
    }

    private void initData() {
        showProgressHUD(this, "");
        getEnterpriseInfo();
        getEnterpriseSize();
    }


    @SuppressLint("SetTextI18n")
    private void initListener() {

        //地址
        if (addreassPickview == null) {
            if (TextUtils.isEmpty(BaseUserInfoCache.getAddress(this))) {
                return;
            }
            addreassPickview = new NewAddreassPickview(this);
            addreassPickview.setTitle("所在城市");
            addreassPickview.isSelectCityOnly = true;
            addreassPickview.setSelectAreaCallback((provinceModel, cityModel, countyModel) -> {
                this.provinceModel = provinceModel;
                this.cityModel = cityModel;
                this.countyModel = countyModel;
                this.mEdAddAddress.setText(provinceModel.disName + " " + (cityModel == null ? "" : cityModel.disName) + " " + (countyModel == null ? "" : countyModel.disName));
            });
        }

        findViewById(R.id.rlv_add_address).setOnClickListener(view -> {
            KeyboardControlUtil.hideKeyboard(mEdAddName);
            KeyboardControlUtil.hideKeyboard(mEdAddContact);
            addreassPickview.show();
        });

        //规模
        findViewById(R.id.rlv_size).setOnClickListener(v -> {
            if (mSizeParams == null || mSizeParams.isEmpty()) {
                return;
            }

            new CommonParamsPop(ApplyJoinEnterprise.this, mSizeParams, 3, "选择您的企业规模", "（单位/人）", "", (v2, position, data) -> {
                for (CommonParam commonParam : mSizeParams) {
                    commonParam.isSelected = false;
                }
                mSizeParams.get(position).isSelected = true;
                mCurrentSizeCode = mSizeParams.get(position).code;
                mEdAddSize.setText(mSizeParams.get(position).name);
            }).show();
            KeyboardControlUtil.hideKeyboard(mEdAddName);
            KeyboardControlUtil.hideKeyboard(mEdAddContact);
        });


        //联系人职位
        findViewById(R.id.rlv_pos).setOnClickListener(v -> {
            if (mPosParams == null || mPosParams.isEmpty()) {
                return;
            }

            new CommonParamsPop(ApplyJoinEnterprise.this, mPosParams, 2, "选择您的职位", "", "", (v2, position, data) -> {
                for (CommonParam commonParam : mPosParams) {
                    commonParam.isSelected = false;
                }
                mPosParams.get(position).isSelected = true;
                mCurrentPosCode = mPosParams.get(position).code;
                mEdAddPos.setText(mPosParams.get(position).name);
            }).show();
            KeyboardControlUtil.hideKeyboard(mEdAddName);
            KeyboardControlUtil.hideKeyboard(mEdAddContact);
        });


        findViewById(R.id.rlv_add_btn).setOnClickListener(view -> {
            if (TextUtils.isEmpty(mEdAddName.getText().toString().trim())) {
                Toasty.normal(this, "企业名称").show();
                return;
            }

            if (TextUtils.isEmpty(mEdAddAddress.getText().toString().trim())) {
                Toasty.normal(this, "请选择所在城市").show();
                return;
            }

            if (cityModel == null) {
                Toasty.normal(this, "请选择所在城市").show();
                return;
            }

            if (TextUtils.isEmpty(mEdAddSize.getText().toString().trim())) {
                Toasty.normal(this, "请选择企业规模").show();
                return;
            }

            if (TextUtils.isEmpty(mEdAddContact.getText().toString().trim())) {
                Toasty.normal(this, "请输入企业邮箱").show();
                return;
            }

            if (!RegixUtil.checkEmail(mEdAddContact.getText().toString().trim())) {
                Toasty.normal(this, "输入正确的邮箱").show();
                return;
            }

            if (TextUtils.isEmpty(mEdAddPos.getText().toString().trim())) {
                Toasty.normal(this, "请选择联系人职位").show();
                return;
            }

            showProgressHUD(ApplyJoinEnterprise.this, "");

            ShopModuleApiProvider.getInstance().submitApply(mEdAddName.getText().toString().trim(), cityModel.id, mCurrentSizeCode, mEdAddContact.getText().toString().trim(), mCurrentPosCode, new Observer<OMBaseResponse>() {
                @Override
                public void onCompleted() {
                    dismissProgressHUD();
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onNext(OMBaseResponse newResponse) {
                    if (newResponse.isSuccess()) {
                        Toasty.normal(ApplyJoinEnterprise.this, "提交成功").show();
                        finish();
                    } else {
                        Toasty.normal(ApplyJoinEnterprise.this, newResponse.msg).show();
                    }
                }
            });
        });
    }


    int finishCount = 0;

    /**
     * 获取企业规模和职位
     */
    private void getEnterpriseSize() {
        ShopModuleApiProvider.getInstance().enterPos(new Observer<EnterParamsResponse>() {
            @Override
            public void onCompleted() {
                if (finishCount == 2) {
                    dismissProgressHUD();
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(EnterParamsResponse enterParamsResponse) {
                finishCount = finishCount + 1;
                mPosParams = enterParamsResponse.data;
            }
        });
    }

    private void getEnterpriseInfo() {
        ShopModuleApiProvider.getInstance().enterSize(new Observer<EnterParamsResponse>() {
            @Override
            public void onCompleted() {
                if (finishCount == 2) {
                    dismissProgressHUD();
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(EnterParamsResponse enterParamsResponse) {
                finishCount = finishCount + 1;
                mSizeParams = enterParamsResponse.data;
            }
        });
    }

}
