package com.tojoy.cloud.module_shop.Goods.Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.shopmall.ShopMallModel;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.string.StringUtil;
import com.tojoy.cloud.module_shop.R;

import java.util.List;

/**
 * Created by daibin
 * on 2019/9/9.
 * function
 */
public class ShopMallCommdityListAdapter extends BaseRecyclerViewAdapter<ShopMallModel> {
    private Context mContext;

    public ShopMallCommdityListAdapter(@NonNull Context context, @NonNull List<ShopMallModel> datas) {
        super(context, datas);
        this.mContext = context;
    }

    @Override
    protected int getViewType(int position, @NonNull ShopMallModel data) {
        return position;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_shopmall_commdity_list_layout;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new ShopMallCommdityListViewHolder(mContext,getLayoutResId(viewType));
    }

    public class ShopMallCommdityListViewHolder extends BaseRecyclerViewHolder<ShopMallModel>{
        private TextView mCommodityNameTv,
                 mCommodityPriceTv,mOrPrice,mCompanyNameTv;
        private ImageView  mCommdityIconIv;

        private Context mContext;

        ShopMallCommdityListViewHolder(Context context, int layoutResId) {
            super(context, layoutResId);
            mContext = context;
            mCommodityNameTv = (TextView) findViewById(R.id.tvCommodityName);
            mOrPrice = (TextView) findViewById(R.id.tvCommodityPrice);
            mCommodityPriceTv = (TextView) findViewById(R.id.tvCommodityReactPrice);
            mCompanyNameTv = (TextView) findViewById(R.id.tvCommodityCompany);
            mCommdityIconIv = (ImageView) findViewById(R.id.ivCommdityIcon);
        }

        @Override
        public void onBindData(ShopMallModel data, int position) {
            mCommodityNameTv.setText(data.goodsName);
            mOrPrice.setText(StringUtil.getInsertedString(data.goodsPrice,1f));
            mCommodityPriceTv.setText(StringUtil.getInsertedString(data.goodsRealPrice,1f));
            mCompanyNameTv.setText(data.companyName);
            mOrPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG );
            ImageLoaderManager.INSTANCE.loadHeadImage(mContext, mCommdityIconIv, data.getGoodsImg1(),
                    R.drawable.nim_image_default);
        }
    }
}
