package com.tojoy.cloud.module_shop.Order.utils;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.module_shop.Order.OrderSegment.View.WycSimplePagerTitleView;
import com.tojoy.cloud.module_shop.R;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;

/**
 * Created by yongchaowang
 * on 2020-02-29.
 *
 * @author yongchaowang
 */
public class OrderUtils {

    public static void initIndicator(Context context, String[] mTitleDatas, ViewPager mViewPage,
                                     MagicIndicator mIndicator) {
        CommonNavigator commonNavigator = new CommonNavigator(context);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mTitleDatas == null ? 0 : mTitleDatas.length;
            }

            @Override
            public IPagerTitleView getTitleView(Context context, int index) {
                WycSimplePagerTitleView simplePagerTitleView = new WycSimplePagerTitleView(context);
                simplePagerTitleView.setText(mTitleDatas[index]);
                simplePagerTitleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                simplePagerTitleView.setNormalColor(Color.parseColor("#91969E"));
                simplePagerTitleView.setSelectedColor(Color.parseColor("#353F5C"));
                simplePagerTitleView.setOnClickListener(v -> mViewPage.setCurrentItem(index));
                return simplePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setMode(LinePagerIndicator.MODE_EXACTLY);
                indicator.setXOffset(AppUtils.dip2px(context, 12));
                indicator.setLineHeight(AppUtils.dip2px(context, 2));
                indicator.setLineWidth(AppUtils.dip2px(context, 12));
                indicator.setStartInterpolator(new AccelerateInterpolator());
                indicator.setEndInterpolator(new DecelerateInterpolator(2.0f));
                indicator.setRoundRadius(5);
                indicator.setColors(ContextCompat.getColor(context, R.color.color_353f5c));
                return indicator;
            }
        });
        mIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(mIndicator, mViewPage);
    }
}
