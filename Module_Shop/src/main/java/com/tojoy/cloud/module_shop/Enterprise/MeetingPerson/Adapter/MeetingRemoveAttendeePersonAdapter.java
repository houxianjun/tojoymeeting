package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.cloud.module_shop.R;

import java.util.List;

/**
 * @author houxianjun
 * @date 2020.05.11
 * 移除已选择参会人适配器
 */
public class MeetingRemoveAttendeePersonAdapter extends BaseRecyclerViewAdapter<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> {

    public MeetingRemoveAttendeePersonAdapter(@NonNull Context context, @NonNull List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> datas) {
        super(context, datas);

    }

    @Override
    protected int getViewType(int position, @NonNull MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel data) {
        return position;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_remove_attendee_person;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new MeetingRemovePersonViewHolder(context, parent, getLayoutResId(viewType));
    }

    public class MeetingRemovePersonViewHolder extends BaseRecyclerViewHolder<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> {

        private TextView tvName, tvPersonPhone, tvRemove;


        MeetingRemovePersonViewHolder(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            tvName = (TextView) findViewById(R.id.tv_person_name);
            tvPersonPhone = (TextView) findViewById(R.id.tv_person_phone);
            tvRemove = (TextView) findViewById(R.id.tv_person_remove);
        }

        @Override
        public void onBindData(MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel data, int position) {
            tvName.setText(data.trueName);
            tvPersonPhone.setText(data.mobile);
            tvRemove.setOnClickListener(v -> onRemoveListener.onRemoveListener(position));
        }
    }

    /**
     * 移除已选择参会人后，更新列表数据。在用户退出页面后，需要将剩下的数据返回。数据会有空的情况
     *
     * @param position 选中位置
     */
    public void updateRemoveData(int position) {
        this.datas.remove(position);
        notifyDataSetChanged();
    }

    //移除时间监听对象
    private OnRemovePersonListener onRemoveListener;

    public void setOnRemoveListener(OnRemovePersonListener removeListener) {
        this.onRemoveListener = removeListener;
    }

    /**
     * 移除参会人回调接口,点击移除，实时更新选择参会人个数
     */
    public interface OnRemovePersonListener {
        void onRemoveListener(int posion);
    }

}
