package com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tojoy.tjoybaselib.configs.UserRoles;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.cloud.module_shop.Api.ShopModuleApiProvider;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.DepartRespone;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.DepartStaffEvent;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Model.DepartModel;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Model.StaffModel;
import com.tojoy.cloud.module_shop.R;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

public class StaffInfoAct extends UI {

    private LinearLayout mInfoLayout, mBtnLayout;
    private TextView mTvNameLeft, mTvPhoneLeft, mTvPhoneRight, mTvJobNumber, mTvEmail, mTvCity;
    private TextView mTvDepartment, mTvSelectDepartment, mTvPost, mTvSelectPost, mTvInfo, mTvEdit, mTvDelete, mTvSave, tvpostContent;
    private EditText mTvNameRight;

    private String corporationCode;
    private String parentId;
    private String selectParentId;
    private String departsJsonStr;
    private StaffModel staffModel;
    private final int Request_Code = 666;
    private String frontRole;
    private String backRole;
    private String departId;
    private List<DepartModel> comData;
    private DepartModel comModel;
    private String beforeEditDepartName;
    private String role;
    private ArrayList<Integer> oldRoleIds;
    private ArrayList<Integer> roleIds;

    public static void start(Context context, String corporationCode, String parentId, String departId, String StaffModerJson) {
        Intent intent = new Intent();
        intent.putExtra("corporationCode", corporationCode);
        intent.putExtra("parentId", parentId);
        intent.putExtra("departId", departId);
        intent.putExtra("StaffModerJson", StaffModerJson);
        intent.setClass(context, StaffInfoAct.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_info);

        initTitle();
        initUI();
        getData();
    }

    private void getData() {
        corporationCode = getIntent().getStringExtra("corporationCode");
        parentId = getIntent().getStringExtra("parentId");
        departId = getIntent().getStringExtra("departId");
        String staffModerJson = getIntent().getStringExtra("StaffModerJson");

        comData = new ArrayList<>();
        comModel = new DepartModel();
        comModel.name = BaseUserInfoCache.getCompanyName(StaffInfoAct.this);

        comModel.parentId = "";
        comModel.corporationCode = corporationCode;
        if (TextUtils.isEmpty(parentId) && TextUtils.isEmpty(departId)) {
            comModel.isChecked = true;
        }


        staffModel = new Gson().fromJson(staffModerJson, StaffModel.class);
        //权限接口调试时增加的重新赋值
        departId =staffModel.departmentCode;
        selectParentId = staffModel.departmentCode;
        comModel.code="";
        comModel.companyCode = staffModel.companyCode;
        comData.add(comModel);
        mTvNameLeft.setText("姓名：" + staffModel.name);
        mTvPhoneLeft.setText("手机号：" + staffModel.mobile);
        String workNo = TextUtils.isEmpty(staffModel.employeeCode) ? "暂无" : staffModel.employeeCode;
        mTvJobNumber.setText("工号：" + workNo);
        String email = TextUtils.isEmpty(staffModel.email) ? "暂无" : staffModel.email;
        mTvEmail.setText("邮箱：" + email);
        String city = TextUtils.isEmpty(staffModel.cityName) ? "暂无" : staffModel.cityName;
        mTvCity.setText("常驻城市：" + city);
        String departName = TextUtils.isEmpty(staffModel.deptName) ? BaseUserInfoCache.getCompanyName(StaffInfoAct.this) : staffModel.deptName;
        mTvDepartment.setText("部门：" + departName);

        frontRole = staffModel.userFrontRole + "";
        backRole = staffModel.userBackRole + "";
        //角色列表
        roleIds = staffModel.roleIds;
        oldRoleIds= staffModel.roleIds;
        if ((staffModel.getUserIsHaveRoles(UserRoles.SUPERADMIN)&&staffModel.mobile.equals(BaseUserInfoCache.getUserMobile(StaffInfoAct.this)))
               || !BaseUserInfoCache.getUserIsHaveRoles(StaffInfoAct.this,UserRoles.SUPERADMIN)) {
            mTvEdit.setVisibility(View.GONE);
            mTvDelete.setVisibility(View.GONE);
            findViewById(R.id.line).setVisibility(View.GONE);
        }

        //修改权限涉及的接口时注释 使用roleNames代替
        role = "";
        role=staffModel.roleNames;
        tvpostContent.setVisibility(View.VISIBLE);
        tvpostContent.setText( staffModel.roleNames);
        mTvSelectPost.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {

            if (requestCode == AddDepartmentAct.Request_Code) {
                //部门回传
                corporationCode = data.getStringExtra("corporationCode");
                selectParentId = data.getStringExtra("parentId");
                mTvSelectDepartment.setText(data.getStringExtra("departName"));
            } else if (requestCode == Request_Code) {
                //角色回传
                frontRole = data.getStringExtra("frontRole");
                backRole = data.getStringExtra("backRole");
                roleIds= (ArrayList<Integer>) data.getSerializableExtra("roleIds");
                String roleStr = data.getStringExtra("roleStr");
                tvpostContent.setVisibility(View.GONE);
                mTvSelectPost.setVisibility(View.VISIBLE);
                mTvSelectPost.setText(roleStr);
            }
        }

    }

    private void initTitle() {
        setStatusBar();
        setUpNavigationBar();
        showBack();
        setTitle("员工详情");
    }

    private void initUI() {
        mTvNameLeft = (TextView) findViewById(R.id.tvNameLeft);
        mTvNameRight = (EditText) findViewById(R.id.tvNameRight);
        mTvPhoneLeft = (TextView) findViewById(R.id.tvPhoneLeft);
        mTvPhoneRight = (TextView) findViewById(R.id.tvPhoneRight);
        mInfoLayout = (LinearLayout) findViewById(R.id.infoLayout);
        mTvJobNumber = (TextView) findViewById(R.id.tvJobNumber);
        mTvEmail = (TextView) findViewById(R.id.tvEmail);
        mTvCity = (TextView) findViewById(R.id.tvCity);
        mTvDepartment = (TextView) findViewById(R.id.tvDepartment);
        mTvSelectDepartment = (TextView) findViewById(R.id.tvSelectDepartment);
        mTvPost = (TextView) findViewById(R.id.tvPost);
        tvpostContent = (TextView) findViewById(R.id.tvPost_content);
        mTvSelectPost = (TextView) findViewById(R.id.tvSelectPost);
        mTvInfo = (TextView) findViewById(R.id.tvInfo);
        mBtnLayout = (LinearLayout) findViewById(R.id.btnLayout);
        mTvEdit = (TextView) findViewById(R.id.tvEdit);
        mTvDelete = (TextView) findViewById(R.id.tvDelete);
        mTvSave = (TextView) findViewById(R.id.tvSave);

        mLeftArrow.setOnClickListener(v -> {
            if (mTvSave.getVisibility() == View.GONE) {
                //员工详情
                finish();
            } else {
                //编辑员工信息
                setTitle("员工详情");
                mTvNameLeft.setText("姓名：" + staffModel.name);
                mTvPhoneLeft.setText("手机号：" + staffModel.mobile);
                mTvNameRight.setVisibility(View.GONE);
                mTvPhoneRight.setVisibility(View.GONE);
                mInfoLayout.setVisibility(View.VISIBLE);
                mTvDepartment.setText(beforeEditDepartName);
                mTvPost.setText("角色：" );
                tvpostContent.setVisibility(View.VISIBLE);
                tvpostContent.setText( role);
                mTvSelectDepartment.setVisibility(View.GONE);
                mTvSelectPost.setVisibility(View.GONE);
                mTvInfo.setVisibility(View.GONE);
                mBtnLayout.setVisibility(View.VISIBLE);
                mTvSave.setVisibility(View.GONE);
                roleIds=oldRoleIds;
            }
        });

        mTvEdit.setOnClickListener(view -> {
            setTitle("编辑员工信息");
            mTvNameLeft.setText("姓名");
            mTvNameRight.setVisibility(View.VISIBLE);
            mTvNameRight.setText(staffModel.name);
            mTvNameRight.setEnabled(true);
            mTvPhoneLeft.setText("手机号");
            mTvPhoneRight.setText(staffModel.mobile);
            mTvPhoneRight.setVisibility(View.VISIBLE);
            mInfoLayout.setVisibility(View.GONE);
            beforeEditDepartName = mTvDepartment.getText().toString();

            String departStr = mTvDepartment.getText().toString();
            mTvDepartment.setText("部门");
            mTvSelectDepartment.setVisibility(View.VISIBLE);
            mTvSelectDepartment.setText(departStr.substring(3));

            String s = tvpostContent.getText().toString();
            mTvPost.setText("角色");
            tvpostContent.setVisibility(View.GONE);
            mTvSelectPost.setVisibility(View.VISIBLE);
            mTvSelectPost.setText(s);

            mTvInfo.setVisibility(View.VISIBLE);
            mBtnLayout.setVisibility(View.GONE);
            mTvSave.setVisibility(View.VISIBLE);
        });

        mTvSave.setOnClickListener(view -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }
            showProgressHUD(StaffInfoAct.this, "加载中");
            ShopModuleApiProvider.getInstance().editStaffInfo(
                    TextUtils.isEmpty(selectParentId) ? parentId : selectParentId,
                    staffModel.mobile,
                    mTvNameRight.getText().toString().trim(),
                    staffModel.id,roleIds, new Observer<OMBaseResponse>() {
                        @Override
                        public void onCompleted() {
                            dismissProgressHUD();
                        }

                        @Override
                        public void onError(Throwable e) {
                            dismissProgressHUD();
                            Toasty.normal(StaffInfoAct.this, "网络错误，请检查网络").show();
                        }

                        @Override
                        public void onNext(OMBaseResponse response) {
                            if (response.isSuccess()) {
                                setTitle("员工详情");
                                staffModel.trueName = mTvNameRight.getText().toString().trim();
                                mTvNameLeft.setText("姓名：" + staffModel.trueName);
                                mTvPhoneLeft.setText("手机号：" + staffModel.mobile);
                                mTvNameRight.setVisibility(View.GONE);
                                mTvPhoneRight.setVisibility(View.GONE);
                                mInfoLayout.setVisibility(View.VISIBLE);
                                mTvDepartment.setText("部门：" + mTvSelectDepartment.getText().toString());
                                mTvPost.setText("角色：");
                                role=mTvSelectPost.getText().toString();
                                oldRoleIds=roleIds;
                                tvpostContent.setVisibility(View.VISIBLE);
                                tvpostContent.setText( mTvSelectPost.getText().toString());
                                mTvSelectDepartment.setVisibility(View.GONE);
                                mTvSelectPost.setVisibility(View.GONE);
                                mTvInfo.setVisibility(View.GONE);
                                mBtnLayout.setVisibility(View.VISIBLE);
                                mTvSave.setVisibility(View.GONE);
                                if (!TextUtils.isEmpty(selectParentId)) {
                                    departId = selectParentId;
                                }
                                EventBus.getDefault().post(new DepartStaffEvent("2", staffModel.deptId));
                            } else {
                                Toasty.normal(StaffInfoAct.this, response.msg).show();
                            }
                        }
                    });
        });

        //员工信息编辑中跳转至员工角色选择
        mTvSelectPost.setOnClickListener(view -> {
            Intent intent = new Intent(StaffInfoAct.this, SelectPostAct.class);
            intent.putExtra("frontRole", frontRole + "");
            intent.putExtra("backRole", backRole + "");
            //当前角色列表
            intent.putExtra("currentRoleIds",roleIds);
            startActivityForResult(intent, Request_Code);
        });
        //选择公司部门
        mTvSelectDepartment.setOnClickListener(v -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }
            ShopModuleApiProvider.getInstance().getDepartments( "", new Observer<DepartRespone>() {
                @Override
                public void onCompleted() {
                    dismissProgressHUD();
                }

                @Override
                public void onError(Throwable e) {
                    dismissProgressHUD();
                    Toasty.normal(StaffInfoAct.this, "网络错误，请检查网络").show();
                }

                @Override
                public void onNext(DepartRespone respone) {
                    if (respone.isSuccess()) {
                        List<DepartModel> data = respone.data;

                        comModel.existSubDept = !data.isEmpty();
                        departsJsonStr = new Gson().toJson(data);
                    } else {
                        comModel.existSubDept = false;
                        departsJsonStr = new Gson().toJson(comData);
                    }
                    Intent intent = new Intent(StaffInfoAct.this, SelectDepartAct.class);
                    intent.putExtra("departsJsonStr", departsJsonStr);
                    intent.putExtra("type", "1");
                    startActivityForResult(intent, AddDepartmentAct.Request_Code);
                }
            });
        });

        mTvDelete.setOnClickListener(v -> {
            new TJMakeSureDialog(StaffInfoAct.this, "确认删除该员工", v1 -> {
                ShopModuleApiProvider.getInstance().deleteStaff(corporationCode, staffModel.mobile,staffModel.id, new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(StaffInfoAct.this, "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        if (response.isSuccess()) {
                            EventBus.getDefault().post(new DepartStaffEvent("2", staffModel.deptId));
                            finish();
                        } else {
                            Toasty.normal(StaffInfoAct.this, response.msg).show();
                        }
                    }
                });
            }).setBtnText("确认", "取消").show();
        });
    }
}
