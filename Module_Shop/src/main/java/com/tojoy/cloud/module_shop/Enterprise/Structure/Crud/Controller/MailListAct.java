package com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.tojoy.tjoybaselib.services.mobilecontacts.ContactInfo;
import com.tojoy.tjoybaselib.services.mobilecontacts.MobileContactsProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.editView.ClearEmojInputFilter;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Helper.SelectedContactHelper;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.UploadStaffModel;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.View.SideBar;
import com.tojoy.cloud.module_shop.R;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;

/**
 * 通讯录导入
 * create by luzhu on 2020/2/25
 */
public class MailListAct extends UI {

    private ArrayList<ContactInfo> mContacts; //本地查到的数据
    private ArrayList<ContactInfo> mDatas;    //展示的数据，可能是全部也可能是搜到的
    private boolean isSearchFlag = false;     //是不是搜索的标记
    private ListView mListView;
    private CheckBox mCbAll;
    private ContactsAdapter mAdapter;
    private TextView mTvStaffCount;
    private EditText mEtSearch;
    private String corporationCode;
    private String parentId;
    private String departId;

    public static void start(Context context, String corporationCode, String parentId, String departId) {
        Intent intent = new Intent();
        intent.putExtra("corporationCode", corporationCode);
        intent.putExtra("parentId", parentId);
        intent.putExtra("departId", departId);
        intent.setClass(context, MailListAct.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_list);

        initData();
        initTitle();
        initUI();
    }

    private void initData() {
        corporationCode = getIntent().getStringExtra("corporationCode");
        parentId = getIntent().getStringExtra("parentId");
        departId = getIntent().getStringExtra("departId");
        //获取联系人
        mContacts = MobileContactsProvider.getInstance().getPhoneContacts(this);
        mDatas = new ArrayList<>();
        mDatas.addAll(mContacts);
    }

    private void initTitle() {
        setStatusBar();
        setUpNavigationBar();
        setTitle("通讯录导入");
        showBack();
    }

    private void initUI() {
        mListView = (ListView) findViewById(R.id.listView);
        SideBar mSideBar = (SideBar) findViewById(R.id.sideBar);
        mCbAll = (CheckBox) findViewById(R.id.cbAll);
        mTvStaffCount = (TextView) findViewById(R.id.tvStaffCount);
        mEtSearch = (EditText) findViewById(R.id.etSearch);
        mSideBar.setScaleItemCount(0);
        mSideBar.setOnStrSelectCallBack((index, selectStr) -> {
            for (int i = 0; i < mContacts.size(); i++) {
                if (selectStr.equalsIgnoreCase(mContacts.get(i).getFirstLetter())) {
                    // 选择到首字母出现的位置
                    mListView.setSelection(i);
                    return;
                }
            }
        });

        InputFilter[] emojiFilters = {new ClearEmojInputFilter()};
        mEtSearch.setFilters(emojiFilters);

        mAdapter = new ContactsAdapter();
        mListView.setAdapter(mAdapter);

        findViewById(R.id.checkedAll).setOnClickListener(view -> {
            boolean checked = mCbAll.isChecked();
            mCbAll.setChecked(!checked);
            int size = mDatas.size();
            for (int i = 0; i < size; i++) {
                mDatas.get(i).isChecked = !checked;
            }
            mAdapter.notifyDataSetChanged();
            refreshCheckedCount();
        });

        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String key = mEtSearch.getText().toString().trim();
                if (!TextUtils.isEmpty(key)) {
                    isSearchFlag = true;
                    doSearch(key);
                } else {
                    isSearchFlag = false;
                    mDatas.clear();
                    mDatas.addAll(mContacts);
                    mAdapter.notifyDataSetChanged();
                }
                refreshCheckedCount();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        findViewById(R.id.tvImport).setOnClickListener(view -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }

            List<UploadStaffModel.Staff> list = new ArrayList<>();

            int size = mDatas.size();
            for (int i = 0; i < size; i++) {
                ContactInfo info = mDatas.get(i);
                if (info.isChecked) {
                    UploadStaffModel.Staff staff = new UploadStaffModel.Staff();
                    staff.trueName = info.trueName;
                    staff.mobile = info.mobile;
                    list.add(staff);
                }
            }

            if (list.isEmpty()) {
                Toasty.normal(MailListAct.this, "请选择至少一位员工").show();
                return;
            }

            SelectedContactHelper.getInstance().initData(list);
            HandAddStaffAct.start(MailListAct.this, "2", corporationCode, parentId, departId);
            finish();
        });
    }

    private void doSearch(String key) {
        mDatas.clear();
        int size = mContacts.size();

        //1、按手机号  2、按姓名
        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(key);
        if (!isNum.matches()) {
            //按姓名
            for (int i = 0; i < size; i++) {
                ContactInfo info = mContacts.get(i);
                if (info.trueName.contains(key)) {
                    mDatas.add(info);
                }
            }
        } else {
            //按手机号
            for (int i = 0; i < size; i++) {
                ContactInfo info = mContacts.get(i);
                if (info.mobile.contains(key)) {
                    mDatas.add(info);
                }
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    private void refreshCheckedCount() {
        int count = 0;
        int size = mContacts.size();
        for (int i = 0; i < size; i++) {
            boolean isChecked = mContacts.get(i).isChecked;
            if (isChecked) {
                count++;
            }
        }
        mTvStaffCount.setText(count + "");
        if (isSearchFlag) {
            int len = mDatas.size();
            boolean isCheckAllCheck = true;
            for (int i = 0; i < len; i++) {
                boolean isChecked = mDatas.get(i).isChecked;
                if (!isChecked) {
                    isCheckAllCheck = false;
                    break;
                }
            }
            mCbAll.setChecked(isCheckAllCheck && mDatas.size() != 0);
        } else {
            mCbAll.setChecked(count == size && size != 0);
        }
    }

    private class ContactsAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return mDatas == null ? 0 : mDatas.size();
        }

        @Override
        public Object getItem(int i) {
            return mDatas.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            ContactsVH vh = null;
            if (view == null) {
                view = LayoutInflater.from(MailListAct.this).inflate(R.layout.item_contacts, viewGroup, false);
                vh = new ContactsVH();
                vh.tvName = view.findViewById(R.id.tvName);
                vh.tvPhone = view.findViewById(R.id.tvPhone);
                vh.checkBox = view.findViewById(R.id.cb);
                view.setTag(vh);
            } else {
                vh = (ContactsVH) view.getTag();
            }

            ContactInfo contactInfo = mDatas.get(i);
            vh.tvName.setText(contactInfo.trueName);
            vh.tvPhone.setText(contactInfo.mobile);

            vh.checkBox.setChecked(contactInfo.isChecked);

            view.setOnClickListener(view1 -> {
                contactInfo.isChecked = !contactInfo.isChecked;
                notifyDataSetChanged();

                refreshCheckedCount();
            });

            return view;
        }
    }

    private class ContactsVH {
        TextView tvName;
        TextView tvPhone;
        CheckBox checkBox;
    }
}
