package com.tojoy.cloud.module_shop.Goods.View;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.tojoy.tjoybaselib.ui.Banner.Banner;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.net.NetWorkUtils;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.module_shop.R;
import com.tojoy.common.Widgets.AnimationIndicator.AnimationIndicator;

import java.util.ArrayList;
import java.util.List;

public class GoodsDetailBanner {

    private Context mContext;
    private View mView;
    private RelativeLayout mIndicatorContainer;

    private List<String> mArticles = new ArrayList<>();

    // 顶部广告栏控件
    private Banner convenientBanner;

    private AnimationIndicator mAnimationIndicator;

    public GoodsDetailBanner(Context mContext) {
        this.mContext = mContext;
    }

    public View getView() {
        if (mView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mView = aLayoutInflater.inflate(R.layout.view_goods_detail_banner_layout, null);
        }
        initUI();
        return mView;
    }

    @SuppressWarnings("rawtypes")
    private void initUI() {
        mIndicatorContainer = mView.findViewById(R.id.indicator_container);
        mIndicatorContainer.setVisibility(View.VISIBLE);
        convenientBanner = mView.findViewById(R.id.convenientBanner);
        int height = AppUtils.getWidth(mContext);
        RelativeLayout.LayoutParams bannerLps = new RelativeLayout.LayoutParams(AppUtils.getWidth(mContext), height);
        bannerLps.setMargins(0, 0, 0, 0);
        convenientBanner.setLayoutParams(bannerLps);
        convenientBanner.setDelayTime(5000);
        if (mAnimationIndicator == null) {
            mAnimationIndicator = new AnimationIndicator(mContext);
            mAnimationIndicator.setIndicatorCoolor();
            RelativeLayout.LayoutParams indicatorLps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.dip2px(mContext, 10));
            indicatorLps.setMargins(0, 0, 0, 0);
            mIndicatorContainer.addView(mAnimationIndicator.getView(), indicatorLps);
        }
    }

    @SuppressWarnings("unchecked")
    public void setViewData(List<String> articles) {
        this.mArticles = articles;
        //设置图片加载器
        convenientBanner.setImageLoader(new GoodsDetailBannerImageLoader());
        //设置图片集合
        convenientBanner.setImages(articles);
       // convenientBanner.setBannerStyle(BannerConfig.NUM_INDICATOR);
        //banner设置方法全部调用完毕时最后调用
        convenientBanner.start();

        //设置点击事件
        convenientBanner.setOnBannerListener(position -> {
            if (FastClickAvoidUtil.isFastDoubleClick(convenientBanner.getId())) {
                return;
            }
            NetWorkUtils.checkNet(mContext);

            if (position >= articles.size()) {
                return;
            }

            if (articles.size() <= 0) {
                return;
            }
        });

        mAnimationIndicator.createIndicator(articles.size());

        convenientBanner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mAnimationIndicator.selectPos(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public RelativeLayout.LayoutParams getLayoutParams() {

        int width = AppUtils.getWidth(mContext);
        int height = (int) (width * 334.0f / 375.0f);

        return new RelativeLayout.LayoutParams(width, height);
    }
}
