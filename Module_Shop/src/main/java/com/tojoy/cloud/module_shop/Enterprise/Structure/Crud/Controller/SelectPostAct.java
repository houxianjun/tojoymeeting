package com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.cloud.module_shop.Api.ShopModuleApiProvider;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.RoleResponse;
import com.tojoy.cloud.module_shop.R;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 角色（选择职位）
 * create by luzhu on 2020/2/26
 */

public class SelectPostAct extends UI {

    private List<RoleResponse.DataBean.RoleListBean> frontRoleList;
    private PostAdapter mAdapter;
    private String frontRole = "";
    private ArrayList<Integer> roleIds;

    public static void start(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, SelectPostAct.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_post);

        initUI();
        initData();
    }

    private void initUI() {
        setStatusBar();
        setUpNavigationBar();
        showBack();
        setTitle("角色");
        ListView mListView = (ListView) findViewById(R.id.listView);
        TextView mTvSure = (TextView) findViewById(R.id.tvSure);

        mAdapter = new PostAdapter();
        mListView.setAdapter(mAdapter);

        mTvSure.setOnClickListener(view -> {
            String backRole = "";
            StringBuilder roleStr = new StringBuilder();
            ArrayList<Integer> roleIdsString = new ArrayList<>();
            for (int i = 0; i < frontRoleList.size(); i++) {
                if (frontRoleList.get(i).isChecked) {
                    frontRole = frontRoleList.get(i).id;
                    roleStr.append(frontRoleList.get(i).roleName).append("、");
                    roleIdsString.add(Integer.valueOf(frontRoleList.get(i).id));
                }
            }
            if (roleStr.toString().length() > 0) {
                roleStr = roleStr.deleteCharAt(roleStr.length() - 1);
            }
            roleIds = roleIdsString;

            Intent intent = getIntent();
            intent.putExtra("frontRole", frontRole);
            intent.putExtra("backRole", backRole);
            intent.putExtra("roleStr", roleStr.toString());
            intent.putExtra("roleIds", roleIds);
            setResult(AddDepartmentAct.Result_Code, intent);
            finish();
        });
    }

    private void initData() {
        roleIds = (ArrayList<Integer>) getIntent().getSerializableExtra("currentRoleIds");
        ShopModuleApiProvider.getInstance().getRoleList(BaseUserInfoCache.getUserId(SelectPostAct.this), new Observer<RoleResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(SelectPostAct.this, "网络错误，请检查网络").show();
            }

            @Override
            public void onNext(RoleResponse response) {
                if (response.isSuccess()) {
                    frontRoleList = response.data.roleList;
                    for (int i = 0; i < frontRoleList.size(); i++) {
                        RoleResponse.DataBean.RoleListBean bean = frontRoleList.get(i);
                        //默认员工是被选中状态
                        if ("5".equals(bean.id)) {
                            bean.isChecked = true;
                        }
                        if (roleIds != null) {
                            for (int j = 0; j < roleIds.size(); j++) {
                                if (roleIds.get(j).equals(Integer.valueOf(bean.id))) {
                                    bean.isChecked = true;
                                }
                            }
                        }

                    }
                    mAdapter.notifyDataSetChanged();
                    for (int i = 0; i < frontRoleList.size(); i++) {
                        if ("员工".equals(frontRoleList.get(i).roleName)) {
                            SelectPostAct.this.frontRole = frontRoleList.get(i).id;
                        }
                    }
                } else {
                    Toasty.normal(SelectPostAct.this, response.msg).show();
                }
            }
        });
    }

    private class PostAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            try {
                return frontRoleList.size();
            } catch (Exception e) {
                return 0;
            }
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            PostVH vh = null;
            if (view == null) {
                view = LayoutInflater.from(SelectPostAct.this).inflate(R.layout.item_post, viewGroup, false);
                vh = new PostVH();
                vh.tv = view.findViewById(R.id.tv);
                vh.checkBox = view.findViewById(R.id.checkbox);
                view.setTag(vh);
            } else {
                vh = (PostVH) view.getTag();
            }

            RoleResponse.DataBean.RoleListBean bean;
            bean = frontRoleList.get(i);
            //第一个是员工默认被选中
            if ("5".equals(bean.id)) {
                vh.checkBox.setChecked(true);
            }
            vh.checkBox.setChecked(bean.isChecked);
            //新增
            vh.tv.setText(bean.roleName);
            view.setOnClickListener(view1 -> {
                //不是员工
                if (!"5".equals(bean.id)) {
                    bean.isChecked = !bean.isChecked;
                    notifyDataSetChanged();
                }
            });
            return view;
        }
    }

    private class PostVH {
        TextView tv;
        CheckBox checkBox;
    }
}
