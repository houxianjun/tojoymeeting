package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.meetingperson.EmployeeModelList;
import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.string.RegixUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.StatusbarUtils;
import com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Adapter.AddPersonsPacageListRecyclerAdapter;
import com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Constants.AttendeeConstants;
import com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Dialog.TJAddPeresonDialog;
import com.tojoy.cloud.module_shop.R;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import es.dmoral.toasty.Toasty;

/**
 * @author houxianjun
 * @Date 2020.05.11
 * 云洽会1.2.3需求 新增添加外部联系人页面
 */
@Route(path = RouterPathProvider.MeetingAddOutSidePersonEnterprise)
public class MeetingNewAddOutSidePersonAct extends Activity {

    /**
     * 标题栏返回组件
     */
    ImageView ivLeftimg;
    /**
     * 标题栏保存添加外部联系人组件
     */
    TextView tvSave;
    /**
     * 添加外部联系人按钮组件
     */
    private TextView tvAddPerson;

    /**
     * 展示外部联系人列表
     */
    private RecyclerView rvAddOutsidePerson;
    /**
     * 展示外部联系人列表适配器
     */
    AddPersonsPacageListRecyclerAdapter adapter;

    private List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> addList = new ArrayList<>();

    /**
     * 已添加外部联系人和公司内部联系人，用于排重
     */
    private List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> addedList = new ArrayList<>();


    /**
     * 重复数据集合
     */
    private List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> repeatList = new ArrayList<>();

    /**
     * 确认保存弹框提示
     */
    private TJMakeSureDialog mTjMakeSureDialog;

    /**
     * 成功的集合
     */
    List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> surcessList = new ArrayList<>();

    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_new_meeting_add_outside_person);

        initTitle();
        initUi();
        intiData();
        initListener();
    }

    private void initTitle() {
        int statusBarHeight = AppUtils.getStatusBarHeight(this);
        LinearLayout.LayoutParams lps = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AppUtils.dip2px(this, 48));
        lps.setMargins(0, statusBarHeight, 0, 0);
        setStatusBar();
        setTitle();
    }

    /**
     * 设置状态栏
     */
    protected void setStatusBar() {
        StatusbarUtils.enableTranslucentStatusbar(this);
        StatusbarUtils.setStatusBarDarkMode(this, StatusbarUtils.statusbarlightmode(this));
    }

    private void setTitle() {
        tvSave = findViewById(R.id.tv_save);
        ivLeftimg = findViewById(R.id.iv_leftimg);
        ivLeftimg.setOnClickListener(v -> {
            hideKeyboard(MeetingNewAddOutSidePersonAct.this);
            finish();
        });
        // 保存事件监听
        tvSave.setOnClickListener(v -> {
            if (FastClickAvoidUtil.isFastDoubleClick(v.getId())) {
                return;
            }
            //当前列表数据集合
            List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> adapterCurDatas = adapter.getDatas();

            hideKeyboard(MeetingNewAddOutSidePersonAct.this);
            new Handler().postDelayed(() -> {
                //单条数据点击保存情况
                if (adapterCurDatas != null && adapterCurDatas.size() > 0 && adapterCurDatas.size() == 1) {
                    //保存前先判断单挑数据
                    singleDataCheck(adapterCurDatas);
                } else {
                    showMakeSureDialog();
                }
            }, 500);
        });
    }


    private void initUi() {
        tvAddPerson = findViewById(R.id.tv_add_person);
        rvAddOutsidePerson = findViewById(R.id.rv_add_outside_person);
    }

    private void intiData() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        addedList = (List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel>) bundle.getSerializable("AttendeePerson");
        if (addedList == null) {
            addedList = new ArrayList<>();
        }
        //初始化页面需默认添加外部联系人项
        adapter = new AddPersonsPacageListRecyclerAdapter(MeetingNewAddOutSidePersonAct.this, addList);

        adapter.setOnItemClinckListener(new AddPersonsPacageListRecyclerAdapter.OnClickListenerr() {
            @Override
            public void onItemClickListener(View view, int possition) {
                addItem(false);

            }

            @Override
            public void onLongItemClickListener(View view, int possition) {

            }
        });
        linearLayoutManager = new LinearLayoutManager(MeetingNewAddOutSidePersonAct.this);
        rvAddOutsidePerson.setLayoutManager(linearLayoutManager);

        //数据更新监听
        rvAddOutsidePerson.setAdapter(adapter);
        addItem(true);
    }

    /**
     * 增加列表项
     */
    private void addItem(boolean isFirstAdd) {
        addItemModel(isFirstAdd);
        rvAddOutsidePerson.smoothScrollToPosition(adapter.getItemCount() - 1);
    }

    /**
     * 增加列表项
     *
     * @param isFirstAdd true 首次进入页面 false 使用添加外部联系人按钮添加
     */
    public void addItemModel(boolean isFirstAdd) {
        //是否是初始化时调用
        if (isFirstAdd) {
            MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel =
                    new MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel();
            employeeModel.trueName = "";
            employeeModel.mobile = "";
            employeeModel.userId = "0";
            employeeModel.isAddSucess = true;
            employeeModel.isOutSidePerson = true;
            employeeModel.isChecked = true;
            //适配器更新集合数据
            adapter.setMdatas(employeeModel);
        } else {
            //添加外部联系人时的检测函数
            if (adapter.checkItemModel()) {
                MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel =
                        new MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel();
                employeeModel.trueName = "";
                employeeModel.mobile = "";
                employeeModel.userId = "0";
                employeeModel.isAddSucess = true;
                employeeModel.isOutSidePerson = true;
                employeeModel.isChecked = true;
                //适配器更新集合数据
                adapter.setMdatas(employeeModel);
            }
        }
    }

    public void initListener() {
        tvAddPerson.setOnClickListener(v -> {
            if (FastClickAvoidUtil.isFastDoubleClick(v.getId())) {
                return;
            }
            //当前列表数据集合
            List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> adapterCurDatas = adapter.getDatas();
            hideKeyboard(MeetingNewAddOutSidePersonAct.this);
            new Handler().postDelayed(() -> {
                //单条数据点击保存情况
                if (adapterCurDatas != null && adapterCurDatas.size() > 0 && adapterCurDatas.size() == 1) {
                    //保存前先判断单挑数据
                    singleDataCheck(adapterCurDatas);
                } else {
                    showMakeSureDialog();
                }
            }, 500);
        });
    }

    /**
     * 确认保存弹框
     */
    public void showMakeSureDialog() {
        //当前列表数据集合
        List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> adapterCurDatas = adapter.getDatas();
        //是否是全空
        boolean isAllNull = true;
        for (int i = 0; i < adapterCurDatas.size(); i++) {
            if (!TextUtils.isEmpty(adapterCurDatas.get(i).trueName.trim()) || !TextUtils.isEmpty(adapterCurDatas.get(i).mobile.trim())) {
                isAllNull = false;
            }
        }
        if (isAllNull) {
            Toasty.normal(MeetingNewAddOutSidePersonAct.this, "手机号或姓名不能为空").show();
            return;
        }
        //保存确认弹框提示
        if (mTjMakeSureDialog == null || !mTjMakeSureDialog.isShowing()) {
            mTjMakeSureDialog = new TJMakeSureDialog(MeetingNewAddOutSidePersonAct.this, "保存已添加的信息吗？",
                    view -> {
                        //保存数据函数
                        saveData();
                        mTjMakeSureDialog.dismiss();
                    }).setBtnText(getApplicationContext().getResources().getString(R.string.meeting_add_outside_person_save),
                    getApplicationContext().getResources().getString(R.string.meeting_add_outside_person_nosave));
            mTjMakeSureDialog.setCancelListener(() -> {
                repeatList.clear();
                surcessList.clear();
                repeatList = new ArrayList<>();
                surcessList = new ArrayList<>();

            });
            mTjMakeSureDialog.show();
        }
    }

    TJAddPeresonDialog dialog;

    /**
     * 保存数据函数
     */
    private void saveData() {
        //检测数据是否可以提交,返回剩下可提交数据集合
        List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> tmpList = SavaCheckItemModel();

        if (tmpList != null) {
            dialog = new TJAddPeresonDialog(this, tmpList.size() + "", repeatList);
            dialog.setEvent(v -> {
                EmployeeModelList employeeModelList = new EmployeeModelList(tmpList);
                Intent itn = new Intent();
                itn.putExtra(AttendeeConstants.ATTENDEE_ADD_PERSON, employeeModelList);
                setResult(RESULT_OK, itn);
                dialog.dismiss();
                finish();
            });
            dialog.show();
        }
    }


    /**
     * 点击保存时,检测列表数据格式正确性
     * 规则：1 手机号是否已存在从组织架构人员和已添加外部联系人中比对
     * 2 数据为空提示补充数据
     * 3 item项都没有填写数据默认不提交该项
     * 4 手机号格式不正确
     * 5 外部联系人手机号排除主播自己的
     */
    public List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> SavaCheckItemModel() {
        //当前列表数据集合
        List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> adapterCurDatas = adapter.getDatas();
        repeatList = new ArrayList<>();

        //第一步过滤当前添加外部联系人列表数据
        Iterator<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> iterator = adapterCurDatas.iterator();

        while (iterator.hasNext()) {
            MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel = iterator.next();
            employeeModel.isAddSucess = true;
            // 手机号与姓名都为空则将该项数据从集合移除，不提交
            if (TextUtils.isEmpty(employeeModel.mobile) && TextUtils.isEmpty(employeeModel.trueName)) {
                iterator.remove();
            } else if (TextUtils.isEmpty(employeeModel.mobile.trim()) || TextUtils.isEmpty(employeeModel.trueName.trim())) {
                //信息不全需要提示用户完善用户信息
                if (TextUtils.isEmpty(employeeModel.mobile.trim())) {
                    employeeModel.failMsg = "手机号不能为空";
                    employeeModel.isAddSucess = false;
                    repeatList.add(employeeModel);
                } else {
                    if (!RegixUtil.checkMobile(employeeModel.mobile)) {
                        //保存成功，姓名显示用户手机号
                        employeeModel.trueName = employeeModel.mobile;
                        employeeModel.isAddSucess = true;
                    } else {
                        employeeModel.trueName = phoneEncrypt(employeeModel.mobile);
                        employeeModel.failMsg = "手机号填写错误";
                        employeeModel.isAddSucess = true;
                    }
                }
            } else if (!RegixUtil.checkMobile(employeeModel.mobile)) {
                employeeModel.failMsg = "手机号填写错误";
                employeeModel.isAddSucess = false;
                repeatList.add(employeeModel);
                iterator.remove();
            } else if (employeeModel.mobile.equals(BaseUserInfoCache.getUserMobile(MeetingNewAddOutSidePersonAct.this))) {
                //邀请用户手机号是自己的提示
                iterator.remove();
                employeeModel.isAddSucess = false;
                employeeModel.failMsg = "该手机号已存在";
                repeatList.add(employeeModel);
            }
        }

        List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> tmplist1;
        tmplist1 = adapterCurDatas;

        //排除正在添加的联系人是否有相同的手机号
        for (int i = 0; i < tmplist1.size(); i++) {
            for (int j = i + 1; j < tmplist1.size(); j++) {
                if (tmplist1.get(i).mobile.equals(tmplist1.get(j).mobile)) {
                    //如果增加之前添加到过失败集合就不在继续添加
                    if (!repeatList.contains(tmplist1.get(i))) {
                        tmplist1.get(j).isAddSucess = false;
                        tmplist1.get(j).failMsg = "该手机号已存在";
                        tmplist1.get(j).mobile = phoneEncrypt(tmplist1.get(j).mobile);
                        repeatList.add(tmplist1.get(j));
                    }
                }
            }
        }

        iterator = adapterCurDatas.iterator();

        //第二步过滤组织架构和已添加外部联系人数据比对

        while (iterator.hasNext()) {
            MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel = iterator.next();
            //临时迭代器，使用迭代器遍历在添加方法是效率高
            //从总共的联系人集合中遍历对比，是否有重复的手机号
            for (MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel tmpemployeeModel : addedList) {
                //查找到手机号已存在
                if (employeeModel.mobile.equals(tmpemployeeModel.mobile)) {
                    //如果增加之前添加到过失败集合就不在继续添加
                    if (!repeatList.contains(employeeModel)) {
                        //此处没有从集合中移除，是因为移除后会报错。所以使用状态判断
                        employeeModel.isAddSucess = false;
                        employeeModel.failMsg = "该手机号已存在";
                        employeeModel.mobile = phoneEncrypt(employeeModel.mobile);
                        repeatList.add(employeeModel);
                    }
                }
            }
        }

        iterator = adapterCurDatas.iterator();
        //去除不成功的数据
        while (iterator.hasNext()) {
            MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel tmpModel1 = iterator.next();
            if (!tmpModel1.isAddSucess) {
                iterator.remove();
            }
        }

        return adapterCurDatas;
    }

    int total = 0;

    //单挑数据
    private void singleDataCheck(List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> adapterCurDatas) {
        boolean isShow = true;
        repeatList = new ArrayList<>();
        total = adapterCurDatas.size();
        for (MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel : adapterCurDatas) {
            //姓名和手机号均为空 吐司提示：手机号或姓名不能为空
            if (TextUtils.isEmpty(employeeModel.trueName.trim()) && TextUtils.isEmpty(employeeModel.mobile.trim())) {
                Toasty.normal(MeetingNewAddOutSidePersonAct.this, "手机号或姓名不能为空").show();
                isShow = false;
            } else if (!TextUtils.isEmpty(employeeModel.trueName.trim()) && TextUtils.isEmpty(employeeModel.mobile.trim())) {
                //姓名非空，手机号为空 吐司提示：手机号或姓名不能为空
                Toasty.normal(MeetingNewAddOutSidePersonAct.this, "手机号或姓名不能为空").show();
                isShow = false;
            } else if (TextUtils.isEmpty(employeeModel.trueName.trim()) && !TextUtils.isEmpty(employeeModel.mobile.trim())) {
                //手机号输入不正确
                if (!RegixUtil.checkMobile(employeeModel.mobile)) {
                    employeeModel.failMsg = "手机号填写错误";
                    employeeModel.isAddSucess = false;
                    employeeModel.trueName = employeeModel.mobile;
                    repeatList.add(employeeModel);
                    total--;
                } else {
                    //手机号输入正确
                    if (!isHave(employeeModel)) {
                        employeeModel.isAddSucess = true;
                        employeeModel.trueName = phoneEncrypt(employeeModel.mobile);
                        surcessList.add(employeeModel);
                    } else {
                        employeeModel.isAddSucess = false;
                        employeeModel.failMsg = "该手机号已存在";
                        employeeModel.trueName = phoneEncrypt(employeeModel.mobile);
                        repeatList.add(employeeModel);
                        repeatList = removeDuplicateEmployee(repeatList);
                    }
                }
            } else {
                if (!RegixUtil.checkMobile(employeeModel.mobile)) {
                    employeeModel.failMsg = "手机号填写错误";
                    employeeModel.isAddSucess = false;
                    repeatList.add(employeeModel);
                    total--;
                } else {
                    if (!isHave(employeeModel)) {
                        surcessList.add(employeeModel);
                    } else {
                        employeeModel.isAddSucess = false;
                        employeeModel.failMsg = "该手机号已存在";
                        repeatList.add(employeeModel);
                        repeatList = removeDuplicateEmployee(repeatList);
                    }
                }
            }
        }
        if (!isShow) {
            return;
        }
        //保存确认弹框提示
        if (mTjMakeSureDialog == null || !mTjMakeSureDialog.isShowing()) {
            mTjMakeSureDialog = new TJMakeSureDialog(MeetingNewAddOutSidePersonAct.this, "保存已添加的信息吗？", view -> {
                if (adapterCurDatas != null) {
                    dialog = new TJAddPeresonDialog(MeetingNewAddOutSidePersonAct.this, surcessList.size() + "", repeatList);
                    dialog.setEvent(v -> {
                        EmployeeModelList employeeModelList = new EmployeeModelList(surcessList);
                        Intent itn = new Intent();
                        itn.putExtra(AttendeeConstants.ATTENDEE_ADD_PERSON, employeeModelList);
                        setResult(RESULT_OK, itn);
                        dialog.dismiss();
                        finish();
                    });
                    dialog.show();
                }
                mTjMakeSureDialog.dismiss();
            }).setBtnText(getApplicationContext().getResources().getString(R.string.meeting_add_outside_person_save), getApplicationContext().getResources().getString(R.string.meeting_add_outside_person_nosave));
            mTjMakeSureDialog.setCancelListener(() -> {
                repeatList.clear();
                surcessList.clear();
                repeatList = new ArrayList<>();
                surcessList = new ArrayList<>();
            });
            mTjMakeSureDialog.show();
        }
    }

    public boolean isHave(MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel) {
        boolean isHave = false;
        //从总共的联系人集合中遍历对比，是否有重复的手机号
        for (MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel tmpemployeeModel : addedList) {
            //查找到手机号已存在
            if (employeeModel.mobile.equals(tmpemployeeModel.mobile)) {
                isHave = true;
            }
        }
        //增加手机号是否是自己的判断
        if (employeeModel.mobile.equals(BaseUserInfoCache.getUserMobile(this))) {
            isHave = true;
        }
        return isHave;
    }

    /**
     * 去重
     */
    private List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> removeDuplicateEmployee(List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> adapterCurDatas) {
        Set<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> set = new TreeSet<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel>((a, b) -> {
            // 字符串则按照asicc码升序排列
            return a.userId.compareTo(b.userId);
        });
        set.addAll(adapterCurDatas);
        return new ArrayList<>(set);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        repeatList = null;
        adapter = null;
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * 隐藏键盘的方法
     *
     * @param context
     */
    public static void hideKeyboard(Activity context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        // 隐藏软键盘
        assert imm != null;
        imm.hideSoftInputFromWindow(context.getWindow().getDecorView().getWindowToken(), 0);
    }

    /**
     * 手机号脱敏筛选正则
     */
    public static final String PHONE_BLUR_REGEX = "(\\d{3})\\d{4}(\\d{4})";

    /**
     * 手机号脱敏替换正则
     */
    public static final String PHONE_BLUR_REPLACE_REGEX = "$1****$2";

    /**
     * 手机号脱敏处理
     *
     * @param phone
     * @return
     */
    public static String phoneEncrypt(String phone) {
        return phone.replaceAll(PHONE_BLUR_REGEX, PHONE_BLUR_REPLACE_REGEX);
    }

}


