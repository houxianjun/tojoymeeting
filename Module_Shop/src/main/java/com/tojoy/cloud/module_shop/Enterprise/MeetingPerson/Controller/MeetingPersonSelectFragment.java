package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Controller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Adapter.MeetingPersonSelectAdapter;
import com.tojoy.tjoybaselib.model.meetingperson.SingleTonModel.MeetingPersonTon;
import com.tojoy.cloud.module_shop.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liuhm
 * @date 2020/2/26
 * @desciption 选择参会员工
 */
public class MeetingPersonSelectFragment extends MeetingPersonBaseFragment implements MeetingPersonSelectAdapter.OnPersonCheckBoxListener {

    public static final String DATA = "DATA";
    private static final String CHECKED_PERSON = "CHECKED_PERSON";

    private RecyclerView tjRecyclerView;
    private RelativeLayout rlContainTip;
    private MeetingPersonSelectAdapter mAdapter;
    private List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> mPersons;
    private List<String> mCheckedPerson;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_meeting_person_select, container, false);
        }
        return mView;
    }

    @Override
    protected void initUI() {
        super.initUI();
        tjRecyclerView = mView.findViewById(R.id.tjRecyclerView);
        rlContainTip = mView.findViewById(R.id.rlContainTip);
    }

    public static MeetingPersonSelectFragment getInstance(Serializable serializable, ArrayList<String> checkedList) {
        MeetingPersonSelectFragment fragment = new MeetingPersonSelectFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(DATA, serializable);
        bundle.putStringArrayList(CHECKED_PERSON, checkedList);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPersons = (List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel>) getArguments().getSerializable(DATA);
            mCheckedPerson = getArguments().getStringArrayList(CHECKED_PERSON);
            setCheckedStatus();
        }
    }

    @Override
    protected void initData() {
        super.initData();
        if (mPersons != null && !mPersons.isEmpty()) {
            visibleListData(true);
            mAdapter = new MeetingPersonSelectAdapter(getActivity(), mPersons);
            mAdapter.setOnCheckListener(this);
            tjRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            tjRecyclerView.setAdapter(mAdapter);
            mAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
                if (!FastClickAvoidUtil.isDoubleClick()) {
                    CheckBox checkBox = v.findViewById(R.id.cb);
                    if (checkBox.isChecked()) {
                        checkBox.setChecked(false);
                    } else {
                        checkBox.setChecked(true);
                    }
                    onPersonCheckListener(mPersons.get(position), checkBox.isChecked());
                }
            });
        } else {
            visibleListData(false);
        }

        updateActBottomCheckAllStatus();
    }

    /**
     * 回退时，更新上一Fragment数据
     */
    public void setPopBackListener(List<String> checkedPerson) {
        if (mCheckedPerson != null) {
            mCheckedPerson.clear();
            mCheckedPerson.addAll(checkedPerson);
        }
        setCheckedStatus();
        tjRecyclerView.post(() -> {
            if (mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        });
        updateActBottomCheckAllStatus();
    }

    /**
     * 全选时，状态变化 Activity中调用
     */
    public void notifyDataChange(boolean isChecked) {
        if (isChecked) {
            //部门架构内人数
            int totalInvitatePerson = ((MeetingPersonSelectAct) getActivity()).getmAllCheckedPersonCountMap().size();
            int selectCount = 0;
            for (int i = 0; i < mPersons.size(); i++) {
                if (mPersons.get(i).isChecked) {
                    selectCount++;
                }
            }
            //邀请人最大数限制
            if ((totalInvitatePerson - selectCount + mPersons.size()) > MeetingPersonTon.getInstance().maxInvitation) {
                ((MeetingPersonSelectAct) getActivity()).updateBottomCheckAllStatus(false);
                ((MeetingPersonSelectAct) getActivity()).showMaxPerson();
                return;
            }
        }

        if (mPersons != null && !mPersons.isEmpty()) {
            for (MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel : mPersons) {
                employeeModel.isChecked = isChecked;
                //设置个人的选中状态
                MeetingPersonTon.getInstance().recSetDepartItemPersonStatus(isChecked, employeeModel);
                ((MeetingPersonSelectAct) getActivity()).updatePersonCheckStatus(employeeModel, isChecked);
            }
            tjRecyclerView.post(() -> {
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    /**
     * Checkbox选中状态监听
     */
    @Override
    public void onPersonCheckListener(MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel data, boolean isChecked) {
        if (getActivity() instanceof MeetingPersonSelectAct) {
            if (isChecked) {
                //选中单个的时候判断是否超出添加人数最大限制
                if (!((MeetingPersonSelectAct) getActivity()).checkMaxPerson(1, true)) {
                    data.isChecked = false;
                    mAdapter.notifyDataSetChanged();
                    return;
                }
            }
            data.isChecked = isChecked;
            //更新人员状态
            ((MeetingPersonSelectAct) getActivity()).updatePersonCheckStatus(data, isChecked);
        }
    }


    /**
     * 设置列表中Checkbox默认选中状态
     */
    private void setCheckedStatus() {
        if (mPersons != null && !mPersons.isEmpty()) {
            for (int i = 0; i < mPersons.size(); i++) {
                if (mCheckedPerson == null || mCheckedPerson.isEmpty()) {
                    mPersons.get(i).isChecked = false;
                } else {
                    mPersons.get(i).isChecked = mCheckedPerson.contains(mPersons.get(i).userId);
                }
            }
        }
    }


    /**
     * 用户点击了所有Checkbox
     */
    public void onItemCheckedAllListener(List<String> checkedPerson) {
        if (mCheckedPerson != null) {
            mCheckedPerson.clear();
            mCheckedPerson.addAll(checkedPerson);
        }
        updateActBottomCheckAllStatus();
    }

    /**
     * 更新Activity底部checkbox选中状态
     */
    public void updateActBottomCheckAllStatus() {
        //true为全选
        boolean isChecked = true;
        int count = 0;
        if (mPersons == null || mPersons.isEmpty()) {
            isChecked = false;
        } else {
            for (int i = 0; i < mPersons.size(); i++) {
                if (mPersons.get(i).isChecked) {
                    count++;
                }
            }
            if (count < mPersons.size()) {
                isChecked = false;
            }
        }
        ((MeetingPersonSelectAct) getActivity()).updateBottomCheckAllStatus(isChecked);
    }


    private void visibleListData(boolean visible) {
        if (visible) {
            rlContainTip.setVisibility(View.GONE);
            tjRecyclerView.setVisibility(View.VISIBLE);
        } else {
            rlContainTip.setVisibility(View.VISIBLE);
            tjRecyclerView.setVisibility(View.GONE);
        }
    }

}
