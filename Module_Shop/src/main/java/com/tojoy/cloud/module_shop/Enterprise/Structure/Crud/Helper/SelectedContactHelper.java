package com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Helper;

import com.google.gson.Gson;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.UploadStaffModel;

import java.util.ArrayList;
import java.util.List;

/**
 * 通讯录导入数据临时寄存处
 */
public class SelectedContactHelper {
    private static SelectedContactHelper instance;

    public synchronized static SelectedContactHelper getInstance() {
        if (instance == null) {
            instance = new SelectedContactHelper();
        }
        return instance;
    }

    public final int mPerPageCount = 200;
    private ArrayList<UploadStaffModel.Staff> mSelectedContacts = new ArrayList<>();

    public void initData(List<UploadStaffModel.Staff> staffList) {
        mSelectedContacts.clear();
        mSelectedContacts.addAll(staffList);
    }

    /**
     * 分页获取数据
     *
     * @param page
     * @return
     */
    public String getDataJsonByPage(int page) {

        List<UploadStaffModel.Staff> subList;

        if (page * mPerPageCount > mSelectedContacts.size()) {
            subList = mSelectedContacts.subList((page - 1) * mPerPageCount, mSelectedContacts.size());
        } else {
            subList = mSelectedContacts.subList((page - 1) * mPerPageCount, page * mPerPageCount);
        }

        return new Gson().toJson(subList);
    }

    public void clear() {
        mSelectedContacts.clear();
    }

    public int getSelectedContactsCount(){
        return mSelectedContacts.size();
    }
}
