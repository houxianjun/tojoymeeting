package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.cloud.module_shop.R;

import java.util.List;

/**
 * @author houxianjun
 * @date 2020.05.11
 * 添加外部联系人适配器
 */
public class MeetingAddPersonWrongAdapter extends BaseRecyclerViewAdapter<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> {

    public MeetingAddPersonWrongAdapter(@NonNull Context context, @NonNull List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel data) {
        return position;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_wrong_add_person;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new MeetingAddOutsidePersonViewHolder(context, parent, getLayoutResId(viewType));
    }

    public class MeetingAddOutsidePersonViewHolder extends BaseRecyclerViewHolder<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> {

        private TextView mNameTipsTv;

        private TextView mPhoneTipsTv;

        MeetingAddOutsidePersonViewHolder(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            mNameTipsTv = (TextView) findViewById(R.id.tv_wrong_name);
            mPhoneTipsTv = (TextView) findViewById(R.id.tv_wrong_msg);
        }

        @Override
        public void onBindData(MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel data, int position) {
            if (data.trueName != null) {
                mNameTipsTv.setText(data.trueName);
            }
            mPhoneTipsTv.setText(data.failMsg);
        }
    }
}
