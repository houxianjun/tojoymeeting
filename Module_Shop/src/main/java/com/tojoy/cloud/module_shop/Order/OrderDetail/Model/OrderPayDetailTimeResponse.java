package com.tojoy.cloud.module_shop.Order.OrderDetail.Model;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * Created by yongchaowang
 * on 2020-03-03.
 */
public class OrderPayDetailTimeResponse extends OMBaseResponse {
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        public int orderSurplusTime;
        public String orderPayPrice;
    }
}
