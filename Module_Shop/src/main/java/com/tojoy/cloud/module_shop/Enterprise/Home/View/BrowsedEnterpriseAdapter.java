package com.tojoy.cloud.module_shop.Enterprise.Home.View;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.enterprise.BrowsedEnterpriseResponse;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.cloud.module_shop.R;

import java.util.List;

/**
 * @author fanxi
 * @date 2019/9/7.
 * description：
 */
public class BrowsedEnterpriseAdapter extends BaseRecyclerViewAdapter<BrowsedEnterpriseResponse.BrowsedEnterpriseModel.EnterprisListBean> {

    public BrowsedEnterpriseAdapter(@NonNull Context context, @NonNull List<BrowsedEnterpriseResponse.BrowsedEnterpriseModel.EnterprisListBean> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull BrowsedEnterpriseResponse.BrowsedEnterpriseModel.EnterprisListBean data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_enterprise_layout;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new BrowsedEnterpriseViewHolder(context, getLayoutResId(viewType));
    }

    public class BrowsedEnterpriseViewHolder extends BaseRecyclerViewHolder<BrowsedEnterpriseResponse.BrowsedEnterpriseModel.EnterprisListBean> {
        private ImageView headImage;
        private TextView mTvCompanyName;


        BrowsedEnterpriseViewHolder(Context context, int layoutResId) {
            super(context, layoutResId);
            headImage = (ImageView) findViewById(R.id.iv_user_head);
            mTvCompanyName = (TextView) findViewById(R.id.tv_company_name);
        }

        @Override
        public void onBindData(BrowsedEnterpriseResponse.BrowsedEnterpriseModel.EnterprisListBean data, int position) {
            ImageLoaderManager.INSTANCE.loadHomeHotCompnayIcon(getContext(), headImage,data.logoUrl, R.drawable.img_search_company_default,"#DDE2EB",1);
            mTvCompanyName.setText(data.companyName);
        }
    }
}
