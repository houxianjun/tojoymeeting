package com.tojoy.cloud.module_shop.Order.OrderDetail.Model;

/**
 * 订单状态改变的通知
 */
public class OrderChangeEvent {
    //1:支付成功：刷新待支付、全部、待发货
    //2:关闭:刷新待支付、全部
    //3.取消:刷新待支付、全部
    //4.确认收货：刷新待收货、已完成
    public String changeType;

    public OrderChangeEvent(String changeType) {
        this.changeType = changeType;
    }
}
