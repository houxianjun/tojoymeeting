package com.tojoy.cloud.module_shop.Order.OrderSegment.Model;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

/**
 * @author fanxi
 * @date 2019-12-26.
 * description：
 */
public class OrderManagementResponse  extends OMBaseResponse {
    public DataListBean data;
    public static class DataListBean {
        public  List<OrderManagementModel> records;
    }
}
