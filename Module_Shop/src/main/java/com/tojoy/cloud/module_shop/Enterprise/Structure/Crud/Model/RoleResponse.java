package com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

public class RoleResponse extends OMBaseResponse {

    public DataBean data;

    public class DataBean {
        public List<RoleListBean> roleList;
        public class RoleListBean {
            public String id;
            public boolean isChecked;
            //新增
            public String roleName;//":"超级管理员",
        }
    }
}
