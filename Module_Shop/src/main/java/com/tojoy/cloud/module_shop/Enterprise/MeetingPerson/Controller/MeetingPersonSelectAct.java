package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.meetingperson.AttendeePersonModel;
import com.tojoy.tjoybaselib.model.meetingperson.EmployeeModelList;
import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Constants.AttendeeConstants;
import com.tojoy.tjoybaselib.model.meetingperson.SingleTonModel.MeetingPersonTon;
import com.tojoy.cloud.module_shop.R;
import com.tojoy.common.App.BaseFragment;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import rx.Observer;


/**
 * @author houxianjun
 * @Date 2020.05.11
 * 云洽会1.2.3版本
 * 修改原MeetingPersonSelectAct类 首次进入使用 MeetingAttendeeSelectFragment
 * 显示带有外部联系人，组织架构页面
 */
@Route(path = RouterPathProvider.MeetingPersonEnterprise)
public class MeetingPersonSelectAct extends UI {

    private static final String ALL_DEPARTMENT = "ALL_DEPARTMENT";
    private static final String USER_IDS = "userIds";
    private static final String MOBILES = "mobiles";
    //参会人数据标示
    private static final String PERSONDATA = "attendeeDatas";
    private static final String MAX_MEETING_PERSON = "max_meeting_person";

    private static final int REQ_CODE_SEARCH = 100;
    //从添加外部联系人返回状态标示
    private static final int REQ_CODE_ADD_PERSON = 101;

    private CheckBox cbAllSelect;
    private TextView tvRateSelect, tvSelectInfo;
    private RelativeLayout rlConfirm;
    private LinearLayout llSearch;
    //已选人员图标
    private LinearLayout llTotalSelectPerson;
    public MeetingPersonDepartmentResponse.DepartmentModel mData;
    //总人数
    private ArrayMap<String, MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> mPersonCountMap;

    //选中的人员集合 包括外部联系人和组织架构内部
    private HashMap<String, String> mAllCheckedPersonCountMap = new HashMap<>();

    public HashMap<String, String> getmAllCheckedPersonCountMap() {
        if (mAllCheckedPersonCountMap == null) {
            mAllCheckedPersonCountMap = new HashMap<>();
        }
        return mAllCheckedPersonCountMap;
    }

    //1.2.3新增所有有选中人员实体对象集合
    private HashMap<String, MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> mAllCheckedPersonModels = new HashMap<>();

    //所有员工人员集合
    private List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> mAllEmployeeModel;
    //外部联系人集合
    private List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> mOutSidePersonEmoloyModels;

    public List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> getmOutSidePersonEmoloyModels() {
        if (mOutSidePersonEmoloyModels == null) {
            mOutSidePersonEmoloyModels = new ArrayList<>();
        }
        return mOutSidePersonEmoloyModels;
    }

    public void setmOutSidePersonEmoloyModels(List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> mOutSidePersonEmoloyModels) {
        this.mOutSidePersonEmoloyModels = mOutSidePersonEmoloyModels;

        //更新外部联系人集合
        MeetingPersonTon.getInstance().setOutSidePersonList(mOutSidePersonEmoloyModels);

    }

    private boolean isPressKeyBack;
    //最大参会人数

    private String mMaxMeetingPerson;

    String meetingForm;
    String meeting_auth_type;

    HashMap<String, String> tmphashMap = new HashMap<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting_person_select);
        initTitle();
        initView();
        initData();
        initListener();
        setSwipeBackEnable(false);
    }

    private void initTitle() {
        setStatusBar();
        setUpNavigationBar();
        setTitle(R.string.meeting_person_select);
        showBack();
    }

    private void initView() {
        cbAllSelect = findView(R.id.cbAllSelect);
        tvRateSelect = findView(R.id.tvRateSelect);
        tvSelectInfo = findView(R.id.tvSelectInfo);
        rlConfirm = findView(R.id.rlConfirm);
        llSearch = findView(R.id.llSearch);
        llTotalSelectPerson = (LinearLayout) findViewById(R.id.ll_total_select_person);
    }

    String isBack = "";

    private void initData() {
        mPersonCountMap = new ArrayMap<>();
        mAllEmployeeModel = new ArrayList<>();
        if (getIntent() != null) {
            meetingForm = getIntent().getStringExtra(AttendeeConstants.MEETING_FROM);
            meeting_auth_type = getIntent().getStringExtra(AttendeeConstants.MEETING_AUTHTYPE);
            mOutSidePersonEmoloyModels = (List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel>) getIntent().getSerializableExtra("outsideperson");
            mData = (MeetingPersonDepartmentResponse.DepartmentModel) getIntent().getSerializableExtra("departPersonModel");
            tmphashMap = (HashMap<String, String>) getIntent().getSerializableExtra("checkhashmap");
            isBack = getIntent().getStringExtra("isBack");
            if (mOutSidePersonEmoloyModels == null) {
                mOutSidePersonEmoloyModels = new ArrayList<>();
            }
            String maxMeetingPerson = getIntent().getStringExtra(MAX_MEETING_PERSON);

            MeetingPersonTon.getInstance().meetingAuthType = meeting_auth_type;
            MeetingPersonTon.getInstance().setmDepartmentData(mData);
            if (!TextUtils.isEmpty(maxMeetingPerson)) {
                mMaxMeetingPerson = maxMeetingPerson;
            }
            if (meetingForm != null && !"ApplyEditFragment".equals(meetingForm)) {
                if (MeetingPersonTon.getInstance().getOutSidePersonList() != null) {
                    mOutSidePersonEmoloyModels = MeetingPersonTon.getInstance().getOutSidePersonList();
                }
            } else {
                initAllCheckedPersonCountMap();
            }
        }
        if (tmphashMap != null) {
            //重置组织架构的人员状态
            recSetCheckStatusPersonEmployee(tmphashMap, mData);
            if ("2".equals(meeting_auth_type)) {
                if (MeetingPersonTon.getInstance().outSidePersonList != null) {
                    List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> list = MeetingPersonTon.getInstance().outSidePersonList;
                    for (int i = 0; i < list.size(); i++) {
                        tmphashMap.remove(list.get(i).userId);
                    }
                }
            } else if (MeetingPersonTon.getInstance().outSidePersonList != null) {
                if (isBack != null) {
                    if ("back".equals(isBack) && !MeetingPersonTon.getInstance().isClickSure()) {
                        List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> list = MeetingPersonTon.getInstance().outSidePersonList;
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).isChecked = false;
                        }
                        MeetingPersonTon.getInstance().recSetDepartmentPseronStatus(false, mData);
                        tmphashMap = new HashMap<>();
                    } else {
                        List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> list = MeetingPersonTon.getInstance().outSidePersonList;
                        for (int i = 0; i < list.size(); i++) {
                            if (tmphashMap.containsKey(list.get(i).userId)) {
                                list.get(i).isChecked = true;
                            } else {
                                list.get(i).isChecked = false;
                            }
                        }
                    }
                }
            }
            mAllCheckedPersonCountMap = tmphashMap;
        } else {
            //重置部门数据为空
            MeetingPersonTon.getInstance().recSetDepartmentPseronStatus(false, mData);
            //重置外部联系人为空
            if (MeetingPersonTon.getInstance().outSidePersonList != null) {
                for (int i = 0; i < MeetingPersonTon.getInstance().outSidePersonList.size(); i++) {
                    MeetingPersonTon.getInstance().outSidePersonList.get(i).isChecked = false;
                }
            }
            mAllCheckedPersonCountMap = tmphashMap;
        }
        if (mAllCheckedPersonCountMap == null) {
            mAllCheckedPersonCountMap = new HashMap<>();
        }
        //根据已选人数判断是否隐藏，根据选中监听会更新ui
        llTotalSelectPerson.setVisibility(mAllCheckedPersonCountMap.size() == 0 ? View.GONE : View.VISIBLE);
        //默认初始化已选择人数为0人
        updateBottomTextViewStatus();
        //获取组织架构数据
        obtainOptionalMeetingPerson();
    }


    /**
     * 只是在首页部门用到的
     */
    public void recSetCheckStatusPersonEmployee(HashMap<String, String> list, MeetingPersonDepartmentResponse.DepartmentModel departmentModel) {
        if (departmentModel != null) {
            //遍历员工
            if (departmentModel.respUserForDepartmentDtoList != null && !departmentModel.respUserForDepartmentDtoList.isEmpty()) {
                for (int i = 0; i < departmentModel.respUserForDepartmentDtoList.size(); i++) {
                    departmentModel.respUserForDepartmentDtoList.get(i).isChecked = list.containsKey(departmentModel.respUserForDepartmentDtoList.get(i).userId);
                }
            }
            //遍历部门人员
            if (departmentModel.corDepartmentList != null && !departmentModel.corDepartmentList.isEmpty()) {
                for (int i = 0; i < departmentModel.corDepartmentList.size(); i++) {
                    recSetCheckStatusPersonEmployee(list, departmentModel.corDepartmentList.get(i));
                }
            }
        }
    }

    private void initListener() {
        // 全部选中事件
        cbAllSelect.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!buttonView.isPressed()) {
                return;
            }
            BaseFragment currentFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.flContain);
            if (currentFragment != null) {
                if (currentFragment instanceof MeetingPersonSelectFragment) {
                    // 当前显示联系人ui
                    MeetingPersonSelectFragment meetingPersonSelectFragment = ((MeetingPersonSelectFragment) currentFragment);
                    meetingPersonSelectFragment.notifyDataChange(isChecked);
                } else if (currentFragment instanceof MeetingDepartmentSelectFragment) {
                    // 当前显示部门联系人ui
                    MeetingDepartmentSelectFragment meetingDepartmentSelectFragment = ((MeetingDepartmentSelectFragment) currentFragment);
                    meetingDepartmentSelectFragment.notifyDataChange(isChecked);
                } else if (currentFragment instanceof MeetingAttendeeSelectFragment) {
                    // 添加参会人首页ui。显示外部联系人
                    MeetingAttendeeSelectFragment meetingAttendeeSelectFragment = ((MeetingAttendeeSelectFragment) currentFragment);
                    //通知页面状态改变
                    meetingAttendeeSelectFragment.notifyDataChange(isChecked);
                }
            }
        });

        //返回事件监听
        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            if (isPressKeyBack) {
                BaseFragment currentFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.flContain);
                if (currentFragment != null) {
                    ArrayList<String> checkedList = new ArrayList(mAllCheckedPersonCountMap.keySet());
                    if (currentFragment instanceof MeetingPersonSelectFragment) {
                        MeetingPersonSelectFragment meetingPersonSelectFragment = ((MeetingPersonSelectFragment) currentFragment);
                        meetingPersonSelectFragment.setPopBackListener(checkedList);
                    } else if (currentFragment instanceof MeetingDepartmentSelectFragment) {
                        MeetingDepartmentSelectFragment meetingDepartmentSelectFragment = ((MeetingDepartmentSelectFragment) currentFragment);
                        meetingDepartmentSelectFragment.setPopBackListener(checkedList);
                    } else if (currentFragment instanceof MeetingAttendeeSelectFragment) {
                        MeetingAttendeeSelectFragment meetingAttendSelectFragment = ((MeetingAttendeeSelectFragment) currentFragment);
                        meetingAttendSelectFragment.setPopBackListener(checkedList);
                    }
                }
            }
            isPressKeyBack = false;
        });


        //选择确定参会人选按钮事件 确定按钮
        rlConfirm.setOnClickListener(view -> {
            //内部会议，提交的时候除
            StringBuilder sbOldUserIds = new StringBuilder();
            StringBuilder sbOldMobiles = new StringBuilder();
            StringBuilder sbOldNameIds = new StringBuilder();

            StringBuilder sbUserIds = new StringBuilder();
            StringBuilder sbMobiles = new StringBuilder();
            StringBuilder sbNameIds = new StringBuilder();

            StringBuilder sbOutNameIds = new StringBuilder();
            StringBuilder sbOutMobiles = new StringBuilder();

            if (mAllCheckedPersonModels != null) {
                //把要提交的数据重新计算，没有点中的就移除
                for (Iterator<Map.Entry<String, MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel>> it = mAllCheckedPersonModels.entrySet().iterator(); it.hasNext(); ) {
                    Map.Entry<String, MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> item = it.next();
                    if (!mAllCheckedPersonCountMap.containsKey(item.getKey())) {
                        it.remove();
                    }
                }
                for (Map.Entry<String, MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> entry : mAllCheckedPersonModels.entrySet()) {
                    //旧接口用到的参数为了保持逻辑不变暂时保留
                    sbOldUserIds.append(entry.getValue().userId).append(",");
                    sbOldMobiles.append(entry.getValue().mobile).append(",");
                    sbOldNameIds.append(entry.getValue().trueName).append(",");
                    //1.2.3版本新增字段
                    if (entry.getValue().isOutSidePerson) {
                        sbOutNameIds.append(entry.getValue().trueName).append(",");
                        sbOutMobiles.append(entry.getValue().mobile).append(",");
                    } else {
                        sbUserIds.append(entry.getValue().userId).append(",");
                        sbMobiles.append(entry.getValue().mobile).append(",");
                        sbNameIds.append(entry.getValue().trueName).append(",");
                    }
                }
            }

            String olduserIds = "";
            String oldmobiles = "";
            String oldTrueName = "";
            //旧的接口参数
            if (sbOldUserIds.length() > 0) {
                olduserIds = sbOldUserIds.substring(0, sbOldUserIds.length() - 1);
            }
            if (sbOldUserIds.length() > 0) {
                oldmobiles = sbOldMobiles.substring(0, sbOldMobiles.length() - 1);
            }
            if (sbOldNameIds.length() > 0) {
                oldTrueName = sbOldNameIds.substring(0, sbOldNameIds.length() - 1);
            }

            //新增
            String userIds = "";
            String mobiles = "";
            String truenameIds = "";
            String outSideMobileIds = "";
            String outSideNameIds = "";
            if (sbUserIds.length() > 0) {
                userIds = sbUserIds.substring(0, sbUserIds.length() - 1);
            }

            if (sbMobiles.length() > 0) {
                mobiles = sbMobiles.substring(0, sbMobiles.length() - 1);
            }


            if (sbNameIds.length() > 0) {
                truenameIds = sbNameIds.substring(0, sbNameIds.length() - 1);
            }


            if (sbOutMobiles.length() > 0) {
                outSideMobileIds = sbOutMobiles.substring(0, sbOutMobiles.length() - 1);
            }

            if (sbOutNameIds.length() > 0) {
                outSideNameIds = sbOutNameIds.substring(0, sbOutNameIds.length() - 1);
            }


            //点击确定了
            MeetingPersonTon.getInstance().setClickSure(true);


            //来源是开会申请
            if ("ApplyEditFragment".equals(meetingForm)) {
                //保存当前页面的状态，包含部门以及所有的外部联系人保存住当前的
                MeetingPersonTon.getInstance().setSureDepartPersonData(mData);

                if (mAllCheckedPersonCountMap.size() > Integer.valueOf(mMaxMeetingPerson)) {
                    Toasty.normal(MeetingPersonSelectAct.this, String.format(getString(R.string.t_max_meeting_person), Integer.valueOf(mMaxMeetingPerson))).show();
                    return;
                }

                if (mAllCheckedPersonCountMap != null && !mAllCheckedPersonCountMap.isEmpty()) {
                    if ("2".equals(meeting_auth_type)) {
                        List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> list = MeetingPersonTon.getInstance().outSidePersonList;
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).isChecked = true) {
                                mAllCheckedPersonCountMap.put(list.get(i).userId, list.get(i).mobile);
                            }
                        }
                    }

                    Intent intent = new Intent();
                    //所有已选中人员
                    intent.putExtra(USER_IDS, olduserIds);
                    intent.putExtra(MOBILES, oldmobiles);

                    AttendeePersonModel attendeePersonModel = new AttendeePersonModel();
                    attendeePersonModel.setInnameIds(truenameIds);
                    attendeePersonModel.setInmobiles(mobiles);
                    attendeePersonModel.setInuserIds(userIds);


                    attendeePersonModel.setOutsideMobils(outSideMobileIds);
                    attendeePersonModel.setOutsideNames(outSideNameIds);
                    intent.putExtra("isBack", "sure");
                    intent.putExtra("departPersonModel", MeetingPersonTon.getInstance().getSureDepartPersonData());
                    intent.putExtra("attendeePersonModel", attendeePersonModel);
                    intent.putExtra("checkhashmap", mAllCheckedPersonCountMap);
                    intent.putExtra(PERSONDATA, (Serializable) getmOutSidePersonEmoloyModels());

                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            } else {
                //用于通过直播间到添加外部联系人页面，做临时保存
                if (MeetingPersonTon.getInstance().getOutSidePersonList() == null) {
                    //确定选择的时候保存
                    MeetingPersonTon.getInstance().addAllOutSidePersons(fragment.getmPersons());
                } else {
                    //确定选择的时候保存
                    MeetingPersonTon.getInstance().setOutSidePersonList(null);
                    MeetingPersonTon.getInstance().setOutSidePersonList(fragment.getmPersons());
                }

                //清空所有选中数据
                //mAllCheckedPersonCountMap=null;
                mAllCheckedPersonCountMap = new HashMap<>();
                //重置外部联系人状态
                List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> outsidpersons = MeetingPersonTon.getInstance().getOutSidePersonList();

                if (outsidpersons != null) {
                    for (MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel model : outsidpersons) {
                        model.isChecked = false;
                    }
                    MeetingPersonTon.getInstance().setOutSidePersonList(outsidpersons);
                }

                //重置组织架构内的状态
                MeetingPersonDepartmentResponse.DepartmentModel departmentModel = MeetingPersonTon.getInstance().getmDepartmentData();
                MeetingPersonTon.getInstance().setSelectDempartStatus(false, departmentModel);
                OMAppApiProvider.getInstance().addMember(LiveRoomInfoProvider.getInstance().liveId, oldmobiles, oldTrueName, new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(OMBaseResponse omBaseResponse) {

                        if (omBaseResponse.isSuccess()) {
                            MeetingPersonSelectAct.this.finish();
                        } else {
                            Toasty.normal(MeetingPersonSelectAct.this, omBaseResponse.msg).show();
                            if ("-2".equals(omBaseResponse.code)) {
                                finish();
                            }
                        }
                    }
                });

            }
        });

        llSearch.setOnClickListener(view -> {
            Intent itn = new Intent(getApplicationContext(), MeetingPersonSearchAct.class);
            itn.putExtra(MeetingPersonSearchAFragment.DATA, (Serializable) mAllEmployeeModel);
            itn.putExtra(MeetingPersonSearchAFragment.CHECKED_PERSON, mAllCheckedPersonCountMap);
            //当前已选中的
            itn.putExtra(AttendeeConstants.ATTENDEE_REMOVE_PERSON_DATA, mAllCheckedPersonModels);
            startActivityForResult(itn, REQ_CODE_SEARCH);
        });

        mLeftArrow.setOnClickListener(view -> {
            if (getSupportFragmentManager().getBackStackEntryCount() < 1) {
                finish();
            } else {
                isPressKeyBack = true;
                getSupportFragmentManager().popBackStack();
            }
        });
        //已选择人数按钮事件
        llTotalSelectPerson.setOnClickListener(v -> {
            // 页面跳转至移除联系人页面
            Intent itn = new Intent(getApplicationContext(), MeetingRemoveAttendeePersonAct.class);
            itn.putExtra(AttendeeConstants.ATTENDEE_REMOVE_PERSON_DATA, mAllCheckedPersonModels);
            itn.putExtra(AttendeeConstants.ATTENDEE_REMOVE_PERSON_MAP, mAllCheckedPersonCountMap);
            startActivityForResult(itn, REQ_CODE_SEARCH);

        });
    }


    public void sendToAddOutSide(List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> tmpList) {
        if (tmpList == null) {
            tmpList = new ArrayList<>();
        }
        // 页面跳转至移除联系人页面
        Intent itn = new Intent(getApplicationContext(), MeetingNewAddOutSidePersonAct.class);
        //有大数据传递异常风险
        Bundle bundle = new Bundle();
        bundle.putSerializable("AttendeePerson", (Serializable) tmpList);
        itn.putExtras(bundle);
        startActivityForResult(itn, REQ_CODE_ADD_PERSON);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SEARCH:
                if (data != null) {
                    mAllCheckedPersonCountMap = (HashMap<String, String>) data.getSerializableExtra(AttendeeConstants.ATTENDEE_CHECKED_PERSON);
                    mAllCheckedPersonModels = (HashMap<String, MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel>) data.getSerializableExtra(AttendeeConstants.ATTENDEE_REMOVE_PERSON_DATA);
                    //重置组织架构的人员状态
                    recSetCheckStatusPersonEmployee(mAllCheckedPersonCountMap, mData);

                    BaseFragment currentFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.flContain);
                    if (currentFragment != null) {
                        ArrayList<String> checkedList = new ArrayList(mAllCheckedPersonCountMap.keySet());
                        if (currentFragment instanceof MeetingPersonSelectFragment) {
                            MeetingPersonSelectFragment meetingPersonSelectFragment = ((MeetingPersonSelectFragment) currentFragment);
                            meetingPersonSelectFragment.setPopBackListener(checkedList);
                        } else if (currentFragment instanceof MeetingDepartmentSelectFragment) {
                            MeetingDepartmentSelectFragment meetingDepartmentSelectFragment = ((MeetingDepartmentSelectFragment) currentFragment);
                            meetingDepartmentSelectFragment.setPopBackListener(checkedList);
                        } else if (currentFragment instanceof MeetingAttendeeSelectFragment) {
                            MeetingAttendeeSelectFragment meetingAttendeeSelectFragment = ((MeetingAttendeeSelectFragment) currentFragment);
                            meetingAttendeeSelectFragment.setPopBackListener(checkedList);
                        }
                    }
                    updateBottomTextViewStatus();
                }
                break;
            //从添加外部联系人返回到该页面，
            case REQ_CODE_ADD_PERSON:
                BaseFragment currentFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.flContain);
                if (currentFragment != null) {
                    if (currentFragment instanceof MeetingAttendeeSelectFragment) {
                        MeetingAttendeeSelectFragment meetingAttendeeSelectFragment = ((MeetingAttendeeSelectFragment) currentFragment);
                        EmployeeModelList employeeModelList = null;
                        try {
                            assert data != null;
                            employeeModelList = (EmployeeModelList) data.getSerializableExtra(AttendeeConstants.ATTENDEE_ADD_PERSON);

                        } catch (Exception ex) {

                        }
                        if (employeeModelList == null) {
                            employeeModelList = new EmployeeModelList(new ArrayList<>());
                        }
                        meetingAttendeeSelectFragment.updateOutSidePerson(employeeModelList);
                    }
                }
                break;
            default:
                break;
        }
    }

    MeetingAttendeeSelectFragment fragment;

    /**
     * 进入页面第一次加载的fragment
     * 避免回退出现空白
     */
    public void firstDepartmentFragment() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        ArrayList<String> checkedList = new ArrayList<>();

        if (mAllCheckedPersonCountMap != null && mAllCheckedPersonCountMap.size() > 0) {
            checkedList = new ArrayList(mAllCheckedPersonCountMap.keySet());
        }
        //初始化部门数据
        MeetingPersonTon.getInstance().initDepartmentData(Integer.valueOf(mMaxMeetingPerson), mData);
        MeetingPersonTon.getInstance().recDeptEmlopee(mData.id, mData);
        fragment = MeetingAttendeeSelectFragment.getInstance((Serializable) mData.corDepartmentList,
                (Serializable) mOutSidePersonEmoloyModels, checkedList, ALL_DEPARTMENT, meeting_auth_type);
        transaction.add(R.id.flContain, fragment).commit();
    }


    /**
     * 递归加载部门下子部门和员工信息 Fragment
     *
     * @param departmentModels
     * @param employeeModels
     * @param curTitleDepartmentId
     */
    public void addDepartmentFragment(List<MeetingPersonDepartmentResponse.DepartmentModel> departmentModels, List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> employeeModels, String curTitleDepartmentId) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
        ArrayList<String> checkedList = new ArrayList(mAllCheckedPersonCountMap.keySet());
        MeetingDepartmentSelectFragment fragment = MeetingDepartmentSelectFragment.getInstance((Serializable) departmentModels, (Serializable) employeeModels, checkedList, curTitleDepartmentId);
        transaction.add(R.id.flContain, fragment).commit();
        transaction.addToBackStack(null);
    }

    /**
     * 新修改的逻辑
     *
     * @param employees
     * @param corporationId
     */
    public void addPersonFragment(List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> employees, String corporationId) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
        ArrayList<String> checkedList = new ArrayList(mAllCheckedPersonCountMap.keySet());
        MeetingPersonSelectFragment fragment = MeetingPersonSelectFragment.getInstance((Serializable) employees, checkedList);
        transaction.add(R.id.flContain, fragment).commit();
        transaction.addToBackStack(null);
    }

    /**
     * 更新底部文本显示选择数量
     */
    protected void updateBottomTextViewStatus() {
        int total = 0;
        if (mAllCheckedPersonCountMap != null && mAllCheckedPersonCountMap.size() > 0) {
            total = mAllCheckedPersonCountMap.size();
        }
        //确定按钮上的选择人数更新 mMaxMeetingPerson
        tvRateSelect.setText(String.format(getString(R.string.s_employee_rate_select), String.valueOf(total), String.valueOf(mMaxMeetingPerson)));

        if (total == 0) {
            rlConfirm.setEnabled(false);
            rlConfirm.setBackgroundResource(R.drawable.bg_btn_disable_meeting);
        } else {
            rlConfirm.setEnabled(true);
            rlConfirm.setBackgroundResource(R.drawable.bg_btn_mail_import);
        }

        String explain;
        if (total == 0) {
            explain = "<span margin:0;padding:0 font-size:15sp\"><font color= \"#BBC2CC\"> 已选择</font><font color= \"#BBC2CC\">" + total + "</font><font color= \"#BBC2CC\">人</font></span>";
        } else {
            explain = "<span margin:0;padding:0 font-size:15sp\"><font color= \"#BBC2CC\"> 已选择</font><font color= \"#2476ED\">" + total + "</font><font color= \"#BBC2CC\">人</font></span>";
        }

        tvSelectInfo.setText(Html.fromHtml(explain));
        llTotalSelectPerson.setVisibility(mAllCheckedPersonCountMap.size() == 0 ? View.GONE : View.VISIBLE);
    }

    /**
     * 检测是否可以继续添加参会人。
     * 规则：根据已选择人数+新添加人数判断是否超过最大可参加人数
     *
     * @return true 可以 false 不可以
     */
    public boolean checkMaxPerson(int addCount, boolean isShow) {
        //是否可以继续添加
        boolean isCanAdd;
        isCanAdd = mAllCheckedPersonCountMap != null && mAllCheckedPersonCountMap.size() + addCount <= Integer.valueOf(mMaxMeetingPerson);
        if (!isCanAdd) {
            if (isShow) {
                Toasty.normal(MeetingPersonSelectAct.this, String.format(getString(R.string.t_max_meeting_person), Integer.valueOf(mMaxMeetingPerson))).show();
            }
        }
        return isCanAdd;
    }

    /**
     * 更新底部全选状态
     */
    public void updateBottomCheckAllStatus(boolean isChecked) {
        if (isChecked) {
            cbAllSelect.setChecked(true);
        } else {
            cbAllSelect.setChecked(false);
        }
    }

    /**
     * 更新部门员工及子部门员工的选择状态集合变化
     */
    public void updateDeptCheckStatus(MeetingPersonDepartmentResponse.DepartmentModel data, boolean isChecked) {
        //更新部门选择状态
        updateCurDeptCheckStatus(data, isChecked);
        //更新子部门选择状态
        recChildDeptCheckStatus(data.corDepartmentList, isChecked);

        BaseFragment currentFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.flContain);
        if (currentFragment != null) {
            ArrayList<String> checkedList = new ArrayList(mAllCheckedPersonCountMap.keySet());
            if (currentFragment instanceof MeetingPersonSelectFragment) {
                MeetingPersonSelectFragment meetingPersonSelectFragment = ((MeetingPersonSelectFragment) currentFragment);
                meetingPersonSelectFragment.onItemCheckedAllListener(checkedList);
            } else if (currentFragment instanceof MeetingDepartmentSelectFragment) {
                MeetingDepartmentSelectFragment meetingDepartmentSelectFragment = ((MeetingDepartmentSelectFragment) currentFragment);
                meetingDepartmentSelectFragment.onItemCheckedAllListener(checkedList);
                meetingDepartmentSelectFragment.notifyDataChange();
            }

            //1.2.3 新增监听外部员工
            else if (currentFragment instanceof MeetingAttendeeSelectFragment) {
                MeetingAttendeeSelectFragment meetingAttendeeSelectFragment = ((MeetingAttendeeSelectFragment) currentFragment);
                meetingAttendeeSelectFragment.onItemCheckedAllListener(checkedList);
                meetingAttendeeSelectFragment.notifyDataChange();
            }
        }
        //更新底部已选参会人UI显示
        updateBottomTextViewStatus();
    }

    /**
     * 当前部门员工选中状态集合变化
     */
    private void updateCurDeptCheckStatus(MeetingPersonDepartmentResponse.DepartmentModel data, boolean isChecked) {
        if (data.respUserForDepartmentDtoList != null && !data.respUserForDepartmentDtoList.isEmpty()) {
            for (int i = 0; i < data.respUserForDepartmentDtoList.size(); i++) {
                if (isChecked) {
                    MeetingPersonTon.getInstance().addCheckPersonModelToMap(data.respUserForDepartmentDtoList.get(i).userId, data.respUserForDepartmentDtoList.get(i));

                    //1.2.3新增同时保存被选中员工对象
                    mAllCheckedPersonModels.put(data.respUserForDepartmentDtoList.get(i).userId, data.respUserForDepartmentDtoList.get(i));
                    mAllCheckedPersonCountMap.put(data.respUserForDepartmentDtoList.get(i).userId, data.respUserForDepartmentDtoList.get(i).mobile);
                } else {
                    if (mAllCheckedPersonCountMap.containsKey(data.respUserForDepartmentDtoList.get(i).userId)) {
                        MeetingPersonTon.getInstance().removeCheckPersonModeToMap(data.respUserForDepartmentDtoList.get(i).userId);
                        mAllCheckedPersonCountMap.remove(data.respUserForDepartmentDtoList.get(i).userId);
                        //1.2.3新增同步删除被选中员工对象
                        mAllCheckedPersonModels.remove(data.respUserForDepartmentDtoList.get(i).userId);
                    }
                }
            }
        }
    }

    /**
     * 当前部门的子部门员工选择状态集合变化
     */
    private void recChildDeptCheckStatus(List<MeetingPersonDepartmentResponse.DepartmentModel> departmentModels, boolean isChecked) {
        if (departmentModels != null && !departmentModels.isEmpty()) {
            for (MeetingPersonDepartmentResponse.DepartmentModel departmentModel : departmentModels) {
                updateCurDeptCheckStatus(departmentModel, isChecked);
                recChildDeptCheckStatus(departmentModel.corDepartmentList, isChecked);
            }
        }
    }

    /**
     * 操作单个员工选中状态的集合变化
     */
    public void updatePersonCheckStatus(MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel data, boolean isChecked) {
        if (isChecked) {
            MeetingPersonTon.getInstance().addCheckPersonModelToMap(data.userId, data);
            //1.2.3新增同时保存被选中员工对象,该集合用于在移除已选中联系人集合列表
            mAllCheckedPersonModels.put(data.userId, data);
            mAllCheckedPersonCountMap.put(data.userId, data.mobile);
        } else {
            if (mAllCheckedPersonCountMap.containsKey(data.userId)) {
                MeetingPersonTon.getInstance().removeCheckPersonModeToMap(data.userId);
                mAllCheckedPersonModels.remove(data.userId);
                mAllCheckedPersonCountMap.remove(data.userId);
            }
        }
        BaseFragment currentFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.flContain);
        if (currentFragment != null) {
            ArrayList<String> checkedList = new ArrayList(mAllCheckedPersonCountMap.keySet());
            if (currentFragment instanceof MeetingPersonSelectFragment) {
                MeetingPersonSelectFragment meetingPersonSelectFragment = ((MeetingPersonSelectFragment) currentFragment);
                meetingPersonSelectFragment.onItemCheckedAllListener(checkedList);
            } else if (currentFragment instanceof MeetingDepartmentSelectFragment) {
                MeetingDepartmentSelectFragment meetingDepartmentSelectFragment = ((MeetingDepartmentSelectFragment) currentFragment);
                meetingDepartmentSelectFragment.onItemCheckedAllListener(checkedList);
            } else if (currentFragment instanceof MeetingAttendeeSelectFragment) {
                //1.2.3 新增监听外部员工
                MeetingAttendeeSelectFragment meetingAttendeeSelectFragment = ((MeetingAttendeeSelectFragment) currentFragment);
                meetingAttendeeSelectFragment.onItemCheckedAllListener(checkedList);
            }
        }
        //更新底部拦文案： 确定 （ 已选人数／ 可邀请参会总人数）； 已选择 ？人员；
        updateBottomTextViewStatus();
    }

    /**
     * 计算总人数
     */
    protected void calAllPersonCount() {
        //1.2.3 新增总人数计算包含外部联系人动态数据
        BaseFragment currentFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.flContain);
        if (currentFragment != null) {
            if (currentFragment instanceof MeetingAttendeeSelectFragment) {
                //不是仅限内不联系人的就不加入到map中
                if (!"2".equals(meeting_auth_type)) {
                    MeetingAttendeeSelectFragment meetingAttendeeSelectFragment = ((MeetingAttendeeSelectFragment) currentFragment);
                    //当前外部联系人动态数据集合
                    List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> tmpPersons = meetingAttendeeSelectFragment.getmPersons();
                    if (tmpPersons != null) {
                        for (int i = 0; i < tmpPersons.size(); i++) {
                            mPersonCountMap.put(tmpPersons.get(i).userId, tmpPersons.get(i));
                            //已选中的能够在总数据中查到，则将该数据取出来添加到已选中人员结合中
                            if (mAllCheckedPersonCountMap.containsKey(tmpPersons.get(i).userId)) {
                                mAllCheckedPersonModels.put(tmpPersons.get(i).userId, tmpPersons.get(i));
                                MeetingPersonTon.getInstance().addCheckPersonModelToMap(tmpPersons.get(i).userId, tmpPersons.get(i));
                            }
                        }
                    }
                }
            }
        }
        if (mData != null) {
            //计算部门以外所有人的个数
            if (mData.respUserForDepartmentDtoList != null) {
                for (int i = 0; i < mData.respUserForDepartmentDtoList.size(); i++) {
                    mPersonCountMap.put(mData.respUserForDepartmentDtoList.get(i).userId, mData.respUserForDepartmentDtoList.get(i));
                    //已选中的能够在总数据中查到，则将该数据取出来添加到已选中人员结合中
                    if (mAllCheckedPersonCountMap.containsKey(mData.respUserForDepartmentDtoList.get(i).userId)) {
                        MeetingPersonTon.getInstance().addCheckPersonModelToMap(mData.respUserForDepartmentDtoList.get(i).userId,
                                mData.respUserForDepartmentDtoList.get(i));
                        mAllCheckedPersonModels.put(mData.respUserForDepartmentDtoList.get(i).userId, mData.respUserForDepartmentDtoList.get(i));
                    }
                }
            }
        }
        //递归部门以内所有人个数
        recDepartmentPersonCount(mData.corDepartmentList);
        //最大人数修改为接口限制的人数
        tvRateSelect.setText(String.format(getString(R.string.s_employee_rate_select), String.valueOf(0), String.valueOf(mMaxMeetingPerson)));
    }

    /**
     * 1.2.3版本 新增方法动态更新总人数
     *
     * @param addEmployeeModelList 新增加的人数
     */
    public void updateAllPersonCount(List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> addEmployeeModelList) {
        //新增总人数计算包含外部联系人动态数据
        BaseFragment currentFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.flContain);
        if (currentFragment != null) {
            if (currentFragment instanceof MeetingAttendeeSelectFragment) {
                //当前外部联系人动态数据集合
                if (addEmployeeModelList != null) {
                    for (int i = 0; i < addEmployeeModelList.size(); i++) {
                        mPersonCountMap.put(addEmployeeModelList.get(i).userId, addEmployeeModelList.get(i));
                        saveOutSidePersonToList(addEmployeeModelList.get(i));
                    }
                }
            }
        }
        //修改最大添加人数
        tvRateSelect.setText(String.format(getString(R.string.s_employee_rate_select), String.valueOf(mAllCheckedPersonCountMap.size()), String.valueOf(mMaxMeetingPerson)));
    }

    /**
     * 计算部门下人数
     */
    private void recDepartmentPersonCount(List<MeetingPersonDepartmentResponse.DepartmentModel> departments) {
        if (departments != null && !departments.isEmpty()) {
            for (MeetingPersonDepartmentResponse.DepartmentModel department : departments) {
                if (department.respUserForDepartmentDtoList != null) {
                    for (int i = 0; i < department.respUserForDepartmentDtoList.size(); i++) {
                        mPersonCountMap.put(department.respUserForDepartmentDtoList.get(i).userId, department.respUserForDepartmentDtoList.get(i));
                        //已选中的能够在总数据中查到，则将该数据取出来添加到已选中人员结合中
                        if (mAllCheckedPersonCountMap.containsKey(department.respUserForDepartmentDtoList.get(i).userId)) {
                            mAllCheckedPersonModels.put(department.respUserForDepartmentDtoList.get(i).userId, department.respUserForDepartmentDtoList.get(i));
                            MeetingPersonTon.getInstance().addCheckPersonModelToMap(department.respUserForDepartmentDtoList.get(i).userId, department.respUserForDepartmentDtoList.get(i));
                        }
                    }
                }
                recDepartmentPersonCount(department.corDepartmentList);
            }
        }
    }

    /**
     * 将所有员工Map转List
     */
    protected void saveAllPersonToList() {
        if (mPersonCountMap != null && !mPersonCountMap.isEmpty()) {
            for (String key : mPersonCountMap.keySet()) {
                mAllEmployeeModel.add(mPersonCountMap.get(key));
            }
        }
    }

    /**
     * 动态将外部联系人Map转成list
     */
    private void saveOutSidePersonToList(MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel) {
        if (employeeModel != null) {
            mAllEmployeeModel.add(employeeModel);
        }
    }

    /**
     * 获取参会人数据
     */
    private void obtainOptionalMeetingPerson() {
        showProgressHUD(this, "");
        String anchorUserId;
        String anchorCompanyCode;
        if ("ApplyEditFragment".equals(meetingForm)) {
            anchorUserId = "";
            anchorCompanyCode = "";
        } else {
            anchorUserId = LiveRoomInfoProvider.getInstance().hostUserId;
            anchorCompanyCode = LiveRoomInfoProvider.getInstance().comCode;
        }

        if (mData != null) {
            //加载首个ui,如果添加过外部联系人需要添加过的带入
            firstDepartmentFragment();
            dismissProgressHUD();
        } else {
            OMAppApiProvider.getInstance().selectMyDepartMentMemV2(anchorUserId, anchorCompanyCode, new Observer<MeetingPersonDepartmentResponse>() {
                @Override
                public void onCompleted() {
                    dismissProgressHUD();
                }

                @Override
                public void onError(Throwable e) {
                    dismissProgressHUD();
                    Toasty.normal(MeetingPersonSelectAct.this, getString(R.string.req_net_error)).show();
                }

                @Override
                public void onNext(MeetingPersonDepartmentResponse response) {
                    if (response.isSuccess()) {
                        //1.2.3版本 该数据只包含组织架构，外部人员返回是空，需要本地做处理
                        mData = response.data;
                        //获取到网络数据需要排重,移除重复的外部联系人数据
                        Iterator<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> iterator = mOutSidePersonEmoloyModels.iterator();
                        while (iterator.hasNext()) {
                            MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel employeeModel = iterator.next();
                            if (MeetingPersonTon.getInstance().recRmoveByUseridDeptEmployee(false, employeeModel.mobile, mData.corDepartmentList.get(0))) {
                                iterator.remove();
                            }
                        }
                        //过滤完号码后重新赋值
                        MeetingPersonTon.getInstance().setOutSidePersonList(mOutSidePersonEmoloyModels);
                        //加载首个ui,如果添加过外部联系人需要添加过的带入
                        firstDepartmentFragment();
                    } else {
                        Toasty.normal(MeetingPersonSelectAct.this, response.msg).show();
                    }
                }
            });
        }
    }


    public void showMaxPerson() {
        Toasty.normal(MeetingPersonSelectAct.this, String.format(getString(R.string.t_max_meeting_person), Integer.valueOf(mMaxMeetingPerson))).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void initAllCheckedPersonCountMap() {
        if (mData != null) {
            MeetingPersonDepartmentResponse.DepartmentModel model = mData;
            MeetingPersonTon.getInstance().recInitAllCheckedPersonCountMap(mAllCheckedPersonCountMap, model);
            MeetingPersonTon.getInstance().setOutSidePersonList(mOutSidePersonEmoloyModels);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (getSupportFragmentManager().getBackStackEntryCount() < 1) {
                backLogic();
            } else {
                isPressKeyBack = true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 点击返回按钮时的逻辑
     */
    public void backLogic() {
        List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> list = MeetingPersonTon.getInstance().outSidePersonList;
        //没点击确定之前，每次返回把上次的数据重置清空，用来解决返回清空数据，默认不显示状态问题
        if (!MeetingPersonTon.getInstance().isClickSure() && list != null) {
            for (int i = 0; i < list.size(); i++) {
                mAllCheckedPersonCountMap.remove(list.get(i).userId);
                list.get(i).isChecked = false;
            }

            //重置外部联系人状态
            List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> outsidpersons = MeetingPersonTon.getInstance().getOutSidePersonList();
            if (outsidpersons != null) {
                for (MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel model : outsidpersons) {
                    model.isChecked = false;
                }
                MeetingPersonTon.getInstance().setOutSidePersonList(outsidpersons);
            }
            //重置组织架构内的状态
            MeetingPersonDepartmentResponse.DepartmentModel departmentModel = MeetingPersonTon.getInstance().getmDepartmentData();
            MeetingPersonTon.getInstance().setSelectDempartStatus(false, departmentModel);
            Intent intent = new Intent();
            //所有已选中人员
            intent.putExtra(USER_IDS, "");
            intent.putExtra(MOBILES, "");

            AttendeePersonModel attendeePersonModel = new AttendeePersonModel();
            attendeePersonModel.setInnameIds("");
            attendeePersonModel.setInmobiles("");
            attendeePersonModel.setInuserIds("");

            attendeePersonModel.setOutsideMobils("");
            attendeePersonModel.setOutsideNames("");
            //标示是否是返回到编辑页面
            intent.putExtra("isBack", "back");
            intent.putExtra("departPersonModel", mData);
            intent.putExtra("attendeePersonModel", attendeePersonModel);

            //返回案件把上次的返回给
            if (MeetingPersonTon.getInstance().getAllDepartHashMapString() != null) {
                intent.putExtra("checkhashmap", (Serializable) null);
            }
            try {
                List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> newList = deepCopy(MeetingPersonTon.getInstance().getOutSidePersonList());
                intent.putExtra(PERSONDATA, (Serializable) newList);
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            setResult(Activity.RESULT_OK, intent);
            finish();
        } else {
            finish();
        }
    }

    public static <T> List<T> deepCopy(List<T> src) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(byteOut);
        out.writeObject(src);

        ByteArrayInputStream byteIn = new ByteArrayInputStream(byteOut.toByteArray());
        ObjectInputStream in = new ObjectInputStream(byteIn);
        @SuppressWarnings("unchecked")
        List<T> dest = (List<T>) in.readObject();
        return dest;
    }
}
