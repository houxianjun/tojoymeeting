package com.tojoy.cloud.module_shop.Order.OrderDetail.Model;

import android.text.TextUtils;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * 作者：jiangguoqiu on 2020/3/1 17:47
 */
public class NewOderDetailModel extends OMBaseResponse {

    public DataBean data;

    public static class DataBean {
        public String completeDate;
        public String createDate;
        public String goodsId;
        public String goodsImgurl;
        public String goodsName;
        public String goodsNum;

        public String goodsSellerPrice;//商品单价
        public String orderGoodsPrice;//商品总金额

        public String id;
        public String isDelivery;
        public String logisticsName;
        public String logisticsNum;
        public String orderCode;
        public String orderPrice;
        public String orderStatus;
        public String payDate;
        public String postagePrice;
        public String receiverAddress;
        public String receiverMobile;
        public String receiverName;
        public String remark;

        public String storeBusiContactPhone;
        public String storeId;
        public String storeName;
        public String storeAvatar;
        public String orderType;

        public String orderPayPrice;
        public String orderTradeNo;//

        public String expireTimes;//订单过期时间
        public String enterpriseId;//企业ID
        public String deliveryDate;//发货时间

        //订单是交易的定金还是普通类型（全额）
        public String isDeposit;

        /**
         * 交易商品订单是否是定金类型
         * @return false 普通或全款类型 true 定金类型
         */
        public boolean getIsDeposit() {
            return !"0".equals(isDeposit) && !TextUtils.isEmpty(isDeposit) && !"null".equals(isDeposit);
        }
        //如果是定金类型，显示交易提示文案
        public String text;


        public boolean isIntentOder() {
            return "2".equals(orderType) || "null".equals(storeId) || TextUtils.isEmpty(storeId);
        }

        public boolean isDelivery() {
            boolean isSupperSendGoods;
            if (isIntentOder()) {
                isSupperSendGoods = true;
            } else {
                isSupperSendGoods = "1".equals(isDelivery);
            }
            return isSupperSendGoods;
        }

        //(10：待付款、20：待发货、30：待收货、40：已收货、50：已完成、60：已取消、70：已超时）
        public boolean isShowEmalInfoLayout() {
            return "30".equals(orderStatus) || "40".equals(orderStatus) || "50".equals(orderStatus);
        }

        public boolean canEdOderRemaksEdText() {
            return "10".equals(orderStatus);
        }

        public boolean isWiatShopSendGoods() {
            return "20".equals(orderStatus);
        }

        public boolean isShopGoodsSended() {
            return "30".equals(orderStatus);
        }

        public boolean isFinshOder() {
            return "50".equals(orderStatus) || "40".equals(orderStatus);
        }

        public boolean isOderColse() {
            return "60".equals(orderStatus) || "70".equals(orderStatus);
        }

        public boolean isSetNeedPayTv() {
            return "60".equals(orderStatus) || "70".equals(orderStatus) || "10".equals(orderStatus);
        }

        public String getPostagePrice() {
            if (TextUtils.isEmpty(postagePrice) || "null".equals(postagePrice)) {
                return "0";
            } else {
                return postagePrice;
            }
        }

        public boolean isShowPaySuccess() {
            return "20".equals(orderStatus) || "30".equals(orderStatus) || "40".equals(orderStatus) || "50".equals(orderStatus);
        }

        public String getRemark() {
            if ("null".equals(remark) || TextUtils.isEmpty(remark)) {
                return "";
            } else {
                return remark;
            }
        }
    }
}
