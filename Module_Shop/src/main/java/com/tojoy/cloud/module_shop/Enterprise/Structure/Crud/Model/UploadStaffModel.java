package com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

import java.util.List;

/**
 * 批量新增员工专用实体类
 */

public class UploadStaffModel extends OMBaseResponse {

    public static class Staff{
        public String trueName;
        public String mobile;
        public String employeeId;
    }

    //==================================== 批量新增的成败数==========================
    public FailSuccessCountData data;
    public class FailSuccessCountData{
        public int failCount;
        public int successCount;
    }
}
