package com.tojoy.cloud.module_shop.Goods.Controller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.shopmall.ShopMallListResponse;
import com.tojoy.tjoybaselib.model.shopmall.ShopMallModel;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.net.NetWorkUtils;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.StatusbarUtils;
import com.tojoy.cloud.module_shop.Goods.Adapter.ShopMallCommdityCatPopAdapter;
import com.tojoy.cloud.module_shop.Goods.Adapter.ShopMallCommdityListAdapter;
import com.tojoy.cloud.module_shop.Goods.Model.ShopMallCommdityCategoryModel;
import com.tojoy.cloud.module_shop.R;
import com.tojoy.common.App.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * @author qll
 * 商城fragment
 */
@SuppressLint("ValidFragment")
public class ShopFragment extends BaseFragment {
    private Context mContext;
    private TJRecyclerView mRecyclerView;
    private ShopMallCommdityListAdapter mShopMallCommdityListAdapter;
    private int mPageNum = 1;
    private List<ShopMallModel> mShopMallList = new ArrayList<>();


    public ArrayList<ShopMallCommdityCategoryModel> mShopMallCommdityCates = new ArrayList<>();
    private View mProPickContainer;
    private int mProPopHeight = 0;
    private TextView mCommdityCatTv, mCommdityPriceTv, mCommdityTimeTv;
    private RecyclerView mCommdityCatRecycler;
    private View mDivView, mSelectView;
    private RelativeLayout.LayoutParams mCommdityCatRecyclerLps;
    private ShopMallCommdityCatPopAdapter mShopMallCommdityCatAdapter;

    private String mCateType = "";
    private String mPriceType = "";
    private String mTimeType = "1";

    private String enterCompanyCode;
    private String mCompaynUrl;
    private String mCompanyName;

    private TextView searchTv;
    private EditText mEtSearch;
    private View mIvClear;
    private String mKey;
    //是否是搜索页面
    private boolean mIsSearch = false;


    public ShopFragment() {
    }

    public ShopFragment(String companyCode, String compaynUrl, String companyName) {
        enterCompanyCode = companyCode;
        mCompaynUrl = compaynUrl;
        mCompanyName = companyName;
        mIsSearch = enterCompanyCode == null && mCompaynUrl == null && mCompanyName == null;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_shop_home, container, false);
        }
        isCreateView = true;
        return mView;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initUI() {
        super.initUI();

        mContext = getActivity();
        initTitle();

        mRecyclerView = mView.findViewById(R.id.recycleview);
        if (mIsSearch) {
            mRecyclerView.setEmptyImgAndTip(R.drawable.icon_no_content, getString(R.string.instant_search_no_content));
            mRecyclerView.setEmptyTextSpaceMargin(60, 40);
        } else {
            mRecyclerView.setEmptyImgAndTip(R.drawable.icon_empty_online_logo, getString(R.string.instant_no_content));
        }
        mRecyclerView.setLoadMoreView();
        mRecyclerView.getRecyclerView().setVerticalScrollBarEnabled(false);
        mRecyclerView.goneFooterView();
        mShopMallCommdityListAdapter = new ShopMallCommdityListAdapter(mContext, mShopMallList);
        mRecyclerView.setAdapter(mShopMallCommdityListAdapter);
        if (!mIsSearch) {
            mRecyclerView.autoRefresh();
        }
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPageNum = 1;
                if (!mIsSearch) {
                    requetDataList(false);
                }
            }

            @Override
            public void onLoadMore() {
                mPageNum++;
                requetDataList(false);

            }
        });
        mShopMallCommdityListAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
            //意向订单详情
            RouterJumpUtil.startGoodsDetails(data.goodsId);
        });


        mCommdityCatTv = mView.findViewById(R.id.tv_commditycatery);
        mCommdityPriceTv = mView.findViewById(R.id.tv_commdity_price);
        mCommdityTimeTv = mView.findViewById(R.id.tv_commdity_date);

        RelativeLayout mCommdityCateRlv = mView.findViewById(R.id.rlv_select_commditycatery);
        RelativeLayout mCommdityPriceRlv = mView.findViewById(R.id.rlv_select_price);
        RelativeLayout mCommdityTimeRlv = mView.findViewById(R.id.rlv_select_date);

        initSearch();


        mView.findViewById(R.id.recycleview).setVisibility(View.VISIBLE);
        mCommdityCateRlv.setOnClickListener(v -> {
            //商品类别
            if (!FastClickAvoidUtil.isFastDoubleClick(R.id.rlv_select_commditycatery)) {
                selectCommdityCat();
            }
        });

        mCommdityPriceRlv.setOnClickListener(v -> {
            //商品费用
            if (!FastClickAvoidUtil.isFastDoubleClick(R.id.rlv_select_price)) {
                if ("".equals(mPriceType)) {
                    mPriceType = "1";
                } else if ("1".equals(mPriceType)) {
                    mPriceType = "2";
                } else if ("2".equals(mPriceType)) {
                    mPriceType = "1";
                }
                mTimeType = "";
                updateShopTitleSort();
                cancleSelectCommdityCat();
            }
        });

        mCommdityTimeRlv.setOnClickListener(v -> {
            //上架时间
            if (!FastClickAvoidUtil.isFastDoubleClick(R.id.rlv_select_date)) {
                if ("".equals(mTimeType)) {
                    mTimeType = "1";
                } else if ("1".equals(mTimeType)) {
                    mTimeType = "2";
                } else if ("2".equals(mTimeType)) {
                    mTimeType = "1";
                }
                mPriceType = "";
                updateShopTitleSort();
                cancleSelectCommdityCat();
            }
        });


        mProPickContainer = mView.findViewById(R.id.rlv_proselect_pop);
        mProPickContainer.setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isFastDoubleClick(R.id.rlv_proselect_pop)) {
                cancleSelectCommdityCat();
            }
        });
        mCommdityCatRecycler = mView.findViewById(R.id.recycler_province);
        mDivView = mView.findViewById(R.id.div_line);
        mSelectView = mView.findViewById(R.id.v_commditycatery);
        mCommdityCatRecycler.setLayoutManager(new LinearLayoutManager(mContext));
        mShopMallCommdityCatAdapter = new ShopMallCommdityCatPopAdapter(mContext, mShopMallCommdityCates);
        mCommdityCatRecycler.setAdapter(mShopMallCommdityCatAdapter);
        mShopMallCommdityCatAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
            mCateType = mShopMallCommdityCates.get(position).type;
            if (0 == position) {
                mCommdityCatTv.setText("商品类别");
            } else {
                mCommdityCatTv.setText(mShopMallCommdityCates.get(position).title);
            }

            cancleSelectCommdityCat();
            updateDatas(position);

            updateShopTitleSort();
        });
        //点击搜索，跳入搜索页面
        mView.findViewById(R.id.iv_search).setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isFastDoubleClick(R.id.iv_search)) {
                ARouter.getInstance().build(RouterPathProvider.Enterprise_Shop)
                        .navigation();
            }
        });

        initCommdityCates();
    }

    /**
     * 初始化搜索相关的控件
     */
    private void initSearch() {
        RelativeLayout mRlvShopTitle = mView.findViewById(R.id.view_base_title);
        //搜索页面布局
        RelativeLayout mRlvSearch = mView.findViewById(R.id.rlTitle);
        mEtSearch = mView.findViewById(R.id.etSearch);
        mIvClear = mView.findViewById(R.id.ivClear);
        searchTv = mView.findViewById(R.id.tvSearch);
        mRlvSearch.setVisibility(mIsSearch ? View.VISIBLE : View.GONE);
        mRlvShopTitle.setVisibility(mIsSearch ? View.GONE : View.VISIBLE);

        mView.findViewById(R.id.ivBack).setOnClickListener(view -> ((Activity) mContext).finish());
        mIvClear.setOnClickListener(view -> mEtSearch.setText(""));
        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mKey = mEtSearch.getText().toString().trim();
                if (!TextUtils.isEmpty(mKey)) {
                    mIvClear.setVisibility(View.VISIBLE);
                    setRestBtClick(true);
                } else {
                    mIvClear.setVisibility(View.GONE);
                    setRestBtClick(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        searchTv.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mKey)) {
                updateShopTitleSort();
            } else {
                Toasty.normal(getActivity(), "搜索内容不能为空!").show();
            }
        });

        mEtSearch.setOnEditorActionListener((arg0, arg1, arg2) -> {
            if (arg1 == EditorInfo.IME_ACTION_SEARCH) {
                if (!TextUtils.isEmpty(mKey)) {
                    ((UI) getActivity()).showKeyboard(false);
                    updateShopTitleSort();
                } else {
                    Toasty.normal(getActivity(), "搜索内容不能为空!").show();
                }
            }
            return false;
        });

    }


    /**
     * 设置"重置"字体颜色及可点击状态
     *
     * @param isClick
     */
    public void setRestBtClick(boolean isClick) {
        if (isClick) {
            searchTv.setClickable(true);
            searchTv.setTextColor(getResources().getColor(R.color.color_2671e9));
        } else {
            searchTv.setClickable(false);
            searchTv.setTextColor(getResources().getColor(R.color.color_969697));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        StatusbarUtils.setStatusBarDarkMode(getActivity(), StatusbarUtils.statusbarlightmode(getActivity()));
    }

    private void initTitle() {
        if (!TextUtils.isEmpty(enterCompanyCode)) {
            mView.findViewById(R.id.tv_title_shop_mall).setVisibility(View.GONE);
            ((TextView) mView.findViewById(R.id.tv_shop_mall_companyname)).setText(mCompanyName);

            ImageLoaderManager.INSTANCE.loadHeadImage(mContext, (mView.findViewById(R.id.iv_shop_mall_companyicon)), OSSConfig.getOSSURLedStr(mCompaynUrl),
                    R.drawable.icon_online_meeting_home_video_defalut);
            mView.findViewById(R.id.iv_shop_mall_back).setOnClickListener(v -> getActivity().finish());
        } else {
            mView.findViewById(R.id.tv_shop_mall_companyname).setVisibility(View.GONE);
            mView.findViewById(R.id.iv_shop_mall_companyicon).setVisibility(View.GONE);
            mView.findViewById(R.id.iv_shop_mall_back).setVisibility(View.GONE);
            mView.findViewById(R.id.tv_shop_mall_certify).setVisibility(View.GONE);
        }
    }

    private void updateShopTitleSort() {
        if ("".equals(mCateType)) {
            Drawable drawableRight = getResources().getDrawable(R.drawable.icon_arrow_down);
            mCommdityCatTv.setCompoundDrawablesWithIntrinsicBounds(null,
                    null, drawableRight, null);
            mCommdityCatTv.setCompoundDrawablePadding(AppUtils.dip2px(mContext, 10));
            mCommdityCatTv.setTextColor(mContext.getResources().getColor(R.color.color_FF888888));
        } else {
            Drawable drawableRight = getResources().getDrawable(R.drawable.icon_arrow_down_selected);
            mCommdityCatTv.setCompoundDrawablesWithIntrinsicBounds(null,
                    null, drawableRight, null);
            mCommdityCatTv.setCompoundDrawablePadding(AppUtils.dip2px(mContext, 10));
            mCommdityCatTv.setTextColor(mContext.getResources().getColor(R.color.color_FF2671E9));
        }

        if ("".equals(mPriceType)) {
            Drawable drawableRight = getResources().getDrawable(R.drawable.icon_arrow_down);
            mCommdityPriceTv.setCompoundDrawablesWithIntrinsicBounds(null,
                    null, drawableRight, null);
            mCommdityPriceTv.setCompoundDrawablePadding(AppUtils.dip2px(mContext, 10));
            mCommdityPriceTv.setTextColor(mContext.getResources().getColor(R.color.color_FF888888));
        } else if ("1".equals(mPriceType)) {
            Drawable drawableRight = getResources().getDrawable(R.drawable.icon_arrow_down_selected);
            mCommdityPriceTv.setCompoundDrawablesWithIntrinsicBounds(null,
                    null, drawableRight, null);
            mCommdityPriceTv.setCompoundDrawablePadding(AppUtils.dip2px(mContext, 10));
            mCommdityPriceTv.setTextColor(mContext.getResources().getColor(R.color.color_FF2671E9));
        } else {
            Drawable drawableRight = getResources().getDrawable(R.drawable.icon_arrow_top);
            mCommdityPriceTv.setCompoundDrawablesWithIntrinsicBounds(null,
                    null, drawableRight, null);
            mCommdityPriceTv.setCompoundDrawablePadding(AppUtils.dip2px(mContext, 10));
            mCommdityPriceTv.setTextColor(mContext.getResources().getColor(R.color.color_FF2671E9));
        }

        if ("".equals(mTimeType)) {
            Drawable drawableRight = getResources().getDrawable(R.drawable.icon_arrow_down);
            mCommdityTimeTv.setCompoundDrawablesWithIntrinsicBounds(null,
                    null, drawableRight, null);
            mCommdityTimeTv.setCompoundDrawablePadding(AppUtils.dip2px(mContext, 10));
            mCommdityTimeTv.setTextColor(mContext.getResources().getColor(R.color.color_FF888888));
        } else if ("1".equals(mTimeType)) {
            Drawable drawableRight = getResources().getDrawable(R.drawable.icon_arrow_top);
            mCommdityTimeTv.setCompoundDrawablesWithIntrinsicBounds(null,
                    null, drawableRight, null);
            mCommdityTimeTv.setCompoundDrawablePadding(AppUtils.dip2px(mContext, 10));
            mCommdityTimeTv.setTextColor(mContext.getResources().getColor(R.color.color_FF2671E9));
        } else {
            Drawable drawableRight = getResources().getDrawable(R.drawable.icon_arrow_down_selected);
            mCommdityTimeTv.setCompoundDrawablesWithIntrinsicBounds(null,
                    null, drawableRight, null);
            mCommdityTimeTv.setCompoundDrawablePadding(AppUtils.dip2px(mContext, 10));
            mCommdityTimeTv.setTextColor(mContext.getResources().getColor(R.color.color_FF2671E9));
        }

        requetDataList(true);
    }

    private void updateDatas(int position) {
        if (!mShopMallCommdityCates.isEmpty()) {
            for (int i = 0; i < mShopMallCommdityCates.size(); i++) {
                if (i == position) {
                    mShopMallCommdityCates.get(i).isSelected = true;
                } else {
                    mShopMallCommdityCates.get(i).isSelected = false;
                }
            }
        }
    }

    @Override
    protected void initData() {

    }

    /**
     * 请求订单列表数据
     */
    private void requetDataList(boolean isRefresh) {
        if (!NetWorkUtils.isNetworkConnected(mContext)) {
            Toasty.normal(mContext, "网络断开，请检查网络").show();
            mRecyclerView.setRefreshing(false);
            return;
        }
        mPageNum = isRefresh ? 1 : mPageNum;

        String pageType = "3";
        if (!TextUtils.isEmpty(enterCompanyCode)) {
            pageType = "2";
        }
        ((UI) getActivity()).showProgressHUD(getActivity(), "");
        OMAppApiProvider.getInstance().mSearchGood(enterCompanyCode, mKey, mTimeType, mPriceType, mCateType, mPageNum, pageType, new Observer<ShopMallListResponse>() {
            @Override
            public void onCompleted() {
                ((UI) getActivity()).dismissProgressHUD();

                mRecyclerView.setRefreshing(false);
                if (mShopMallList.size() == 0) {
                    if (mIsSearch) {
                        mRecyclerView.setEmptyImgAndTip(R.drawable.icon_no_content, getString(R.string.instant_search_no_content));
                        mRecyclerView.setEmptyTextSpaceMargin(60, 40);

                    } else {
                        mRecyclerView.setEmptyImgAndTip(R.drawable.icon_empty_online_logo, getString(R.string.instant_no_content));
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                mRecyclerView.setRefreshing(false);
                Toasty.normal(mContext, "网络错误,请检查网络").show();
            }

            @Override
            public void onNext(ShopMallListResponse shopMallListResponse) {
                mRecyclerView.setRefreshing(false);
                if (shopMallListResponse.isSuccess()) {
                    if (mPageNum == 1) {
                        mShopMallList.clear();
                        mShopMallList.addAll(shopMallListResponse.data.list);
                        mShopMallCommdityListAdapter.updateData(mShopMallList);
                    } else {
                        mShopMallList.addAll(shopMallListResponse.data.list);
                        mShopMallCommdityListAdapter.addMore(shopMallListResponse.data.list);
                    }
                    if (mShopMallList.size() == 0) {
                        if (mIsSearch) {
                            mRecyclerView.setEmptyImgAndTip(R.drawable.icon_no_content, getString(R.string.instant_search_no_content));
                            mRecyclerView.setEmptyTextSpaceMargin(60, 40);
                        } else {
                            mRecyclerView.setEmptyImgAndTip(R.drawable.icon_empty_online_logo, getString(R.string.instant_no_content));
                        }
                    }
                    mRecyclerView.showFooterView();
                    mRecyclerView.refreshLoadMoreView(mPageNum, shopMallListResponse.data.list.size());
                } else {
//                    haveData(false);
                    Toasty.normal(mContext, shopMallListResponse.msg).show();
                }
            }
        });


    }

    /**
     * 初始化商品类别
     */
    private void initCommdityCates() {
        mShopMallCommdityCates.add(new ShopMallCommdityCategoryModel("", "全部", true));
        mShopMallCommdityCates.add(new ShopMallCommdityCategoryModel("1", "实体类", false));
        mShopMallCommdityCates.add(new ShopMallCommdityCategoryModel("2", "项目类", false));
        setCommdityCatLayoutParams();
    }


    /**
     * 选择商品类别
     */
    private boolean isCanSelect = true;
    private boolean isCanDismiss = false;

    public void setCommdityCatLayoutParams() {
        int cellHeight = AppUtils.dip2px(mContext, 34);
        int maxHeight = AppUtils.getHeight(mContext) - AppUtils.dip2px(mContext, 200);
        if (cellHeight * 3 > maxHeight) {
            mProPopHeight = maxHeight;
        } else {
            mProPopHeight = cellHeight * 3 + AppUtils.dip2px(mContext, 28);
        }

        int mProPopTop = mView.findViewById(R.id.view_base_title).getLayoutParams().height + mView.findViewById(R.id.rlv_head_layout).getLayoutParams().height + AppUtils.getStatusBarHeight(mContext) + AppUtils.dip2px(mContext, 38);

        mCommdityCatRecyclerLps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, mProPopHeight);
        mCommdityCatRecyclerLps.setMargins(0, mProPopTop, 0, 0);
        mCommdityCatRecycler.setLayoutParams(mCommdityCatRecyclerLps);
    }

    private void selectCommdityCat() {
        if (isCanDismiss) {
            cancleSelectCommdityCat();
            return;
        }

        if (!isCanSelect) {
            return;
        }


        mSelectView.setVisibility(View.VISIBLE);

        isCanSelect = false;
        mProPickContainer.setVisibility(View.VISIBLE);
        Drawable drawableRight = getResources().getDrawable(R.drawable.icon_arrow_down_selected);
        mCommdityCatTv.setCompoundDrawablesWithIntrinsicBounds(null,
                null, drawableRight, null);
        mCommdityCatTv.setCompoundDrawablePadding(AppUtils.dip2px(mContext, 10));
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 0.4f);
        alphaAnimation.setDuration(400);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                isCanSelect = false;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mProPickContainer.setClickable(true);
                isCanDismiss = true;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mProPickContainer.startAnimation(alphaAnimation);

        mCommdityCatRecycler.setLayoutParams(mCommdityCatRecyclerLps);
        mCommdityCatRecycler.setVisibility(View.VISIBLE);
        mDivView.setVisibility(View.VISIBLE);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, -mProPopHeight, 0);
        translateAnimation.setDuration(400);
        translateAnimation.setFillAfter(true);
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mShopMallCommdityCatAdapter.updateData(mShopMallCommdityCates);
                mCommdityCatRecycler.setEnabled(true);

                mView.findViewById(R.id.rlv_proselect_pop).bringToFront();
                mView.findViewById(R.id.recycler_province).bringToFront();
                mView.findViewById(R.id.view_base_title).bringToFront();
                mView.findViewById(R.id.rlv_head_layout).bringToFront();
                mDivView.bringToFront();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mCommdityCatRecycler.startAnimation(translateAnimation);
    }

    /**
     * 关闭省份选择弹窗
     */
    private void cancleSelectCommdityCat() {

        if (!isCanDismiss) {
            return;
        }

        mSelectView.setVisibility(View.GONE);

        mShopMallCommdityCatAdapter.updateData(new ArrayList<>());

        Drawable drawableRight = getResources().getDrawable(R.drawable.icon_arrow_down);
        mCommdityCatTv.setCompoundDrawablesWithIntrinsicBounds(null,
                null, drawableRight, null);
        mCommdityCatTv.setCompoundDrawablePadding(AppUtils.dip2px(mContext, 10));

        AlphaAnimation alphaAnimation = new AlphaAnimation(0.4f, 0.0f);
        alphaAnimation.setDuration(400);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                isCanDismiss = false;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                isCanSelect = true;
                mProPickContainer.setClickable(true);
                mView.findViewById(R.id.recycleview).bringToFront();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mProPickContainer.startAnimation(alphaAnimation);

        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0, -mProPopHeight);
        translateAnimation.setDuration(400);
        translateAnimation.setFillAfter(true);
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mCommdityCatRecycler.setLayoutParams(new RelativeLayout.LayoutParams(0, 0));
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mCommdityCatRecycler.startAnimation(translateAnimation);
    }
}
