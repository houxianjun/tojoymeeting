package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.model.meetingperson.SingleTonModel.MeetingPersonTon;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Adapter.MeetingRemoveAttendeePersonAdapter;
import com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Constants.AttendeeConstants;
import com.tojoy.cloud.module_shop.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;

/**
 * @author houxianjun
 * @date 2020/5/11.
 * 云洽会1.2.3需求 新增移除参会人员页面
 */
public class MeetingRemoveAttendeePersonAct extends UI implements MeetingRemoveAttendeePersonAdapter.OnRemovePersonListener {

    private RecyclerView rvRemovePerson;
    /**
     * 已选择参会人为空ui
     */
    private RelativeLayout rlEmptyData;

    private MeetingRemoveAttendeePersonAdapter adapter;

    private HashMap<String, MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> removeLishMap = null;
    ArrayList<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> tmparrayList = null;
    private HashMap<String, String> mAllCheckedPersonCountMap = new HashMap<String, String>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting_remove_attendee_person);

        tmparrayList = new ArrayList<>();
        Intent intent = getIntent();
        removeLishMap = (HashMap<String, MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel>) intent.getSerializableExtra(AttendeeConstants.ATTENDEE_REMOVE_PERSON_DATA);
        if (removeLishMap == null) {
            removeLishMap = new HashMap<>();
        } else {
            for (Map.Entry<String, MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> entry : removeLishMap.entrySet()) {
                tmparrayList.add(entry.getValue());
            }
        }
        mAllCheckedPersonCountMap = (HashMap<String, String>) intent.getSerializableExtra(AttendeeConstants.ATTENDEE_REMOVE_PERSON_MAP);
        initTitle();
        initUi();
        intiData();
    }

    /**
     * 初始化标题
     */
    private void initTitle() {
        setStatusBar();
        setUpNavigationBar();
        showBack();
    }

    /**
     * 初始化UI组件
     */
    private void initUi() {
        rvRemovePerson = (RecyclerView) findViewById(R.id.rv_remove_person);
        rvRemovePerson.setLayoutManager(new LinearLayoutManager(MeetingRemoveAttendeePersonAct.this));
        rlEmptyData = (RelativeLayout) findViewById(R.id.rl_empty_data);
        //初始化空数据ui
        rlEmptyData.setVisibility(tmparrayList != null && tmparrayList.size() == 0 ? View.VISIBLE : View.GONE);
    }

    /**
     * 初始化数据
     */
    private void intiData() {
        adapter = new MeetingRemoveAttendeePersonAdapter(MeetingRemoveAttendeePersonAct.this, tmparrayList);
        //数据更新监听
        adapter.setOnRemoveListener(this);
        rvRemovePerson.setAdapter(adapter);
        //已选择人数
        int countPerson = tmparrayList.size();
        setTitle(String.format(MeetingRemoveAttendeePersonAct.this.getResources().getString(R.string.meeting_attendee_person_remove), countPerson + ""));
    }

    @Override
    public void onRemoveListener(int position) {
        try {
            mAllCheckedPersonCountMap.remove(adapter.getDatas().get(position).userId);
            MeetingPersonTon.getInstance().updateCheckPersonModelToMap(adapter.getDatas().get(position).userId, false);
            removeLishMap.remove(adapter.getDatas().get(position).userId);
            adapter.updateRemoveData(position);
            setTitle(String.format(MeetingRemoveAttendeePersonAct.this.getResources().getString(R.string.meeting_attendee_person_remove), adapter.getDatas().size() + ""));

            //参会人个数为0时，更新页面UI
            rlEmptyData.setVisibility(adapter.getDatas().size() == 0 ? View.VISIBLE : View.GONE);
            rvRemovePerson.setVisibility(adapter.getDatas().size() == 0 ? View.GONE : View.VISIBLE);
            Toasty.normal(MeetingRemoveAttendeePersonAct.this, "移除成功").show();

        } catch (Exception ex) {

        }

    }


    @Override
    public void onBackPressed() {
        savePersonCheckedList(mAllCheckedPersonCountMap, removeLishMap);
        super.onBackPressed();

    }

    public void savePersonCheckedList(HashMap<String, String> checkedList, HashMap<String, MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> tmpdatas) {
        Intent itn = new Intent();
        itn.putExtra(AttendeeConstants.ATTENDEE_CHECKED_PERSON, checkedList);
        itn.putExtra(AttendeeConstants.ATTENDEE_REMOVE_PERSON_DATA, tmpdatas);
        setResult(RESULT_OK, itn);
        finish();
    }
}
