package com.tojoy.cloud.module_shop.Enterprise.Structure.Home.Controller;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.cloud.module_shop.Api.ShopModuleApiProvider;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Controller.StaffInfoAct;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Home.Model.EnterStructSearchResponse;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Home.View.EnterStructItemAdapter;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Model.StaffModel;
import com.tojoy.cloud.module_shop.R;
import com.tojoy.common.App.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

public class EnterStructSearchFragment extends BaseFragment {

    private RecyclerView mRecyclerView;
    private EnterStructItemAdapter mAdapter;
    private List<StaffModel> mDatas = new ArrayList<>();
    private String mSearchKey;
    public String mDepartId;
    public String mDepartPId;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_es_search_layout, container, false);
        }
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        isCreateView = true;
        return mView;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initUI() {
        mRecyclerView = mView.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);
    }


    /**
     * 进行搜索
     *
     * @param dynamicKey
     */
    public void doSearch(String dynamicKey) {
        mSearchKey = dynamicKey;
        mDatas.clear();
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
        loadData();
    }

    private void loadData() {
        ShopModuleApiProvider.getInstance().searchEnterStruct(BaseUserInfoCache.getCompanyCode(getContext()), mSearchKey, new Observer<EnterStructSearchResponse>() {
            @Override
            public void onCompleted() {
                if (mDatas.size() == 0) {
                    mView.findViewById(R.id.emptyLayout_order).setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(getContext(), "网络异常，稍后再试").show();
            }

            @Override
            public void onNext(EnterStructSearchResponse response) {
                if (response.isSuccess()) {
                    mDatas = response.data;
                    mView.findViewById(R.id.emptyLayout_order).setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mAdapter = new EnterStructItemAdapter(getContext(), mDatas, 1);
                    mAdapter.mStaffDepartClickListener = (meetingModel, type) -> {
                        //跳转员工详情
                        StaffInfoAct.start(getContext(), BaseUserInfoCache.getCompanyCode(getContext()), mDepartPId, mDepartId, new Gson().toJson(meetingModel));
                    };
                    mRecyclerView.setAdapter(mAdapter);
                } else {
                    Toasty.normal(getContext(), response.msg).show();
                }
            }
        });
    }

}
