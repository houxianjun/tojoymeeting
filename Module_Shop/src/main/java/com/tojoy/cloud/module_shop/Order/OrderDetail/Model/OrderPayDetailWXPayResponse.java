package com.tojoy.cloud.module_shop.Order.OrderDetail.Model;

import com.tojoy.tjoybaselib.net.OMBaseResponse;

/**
 * Created by yongchaowang
 * on 2020-03-03.
 */
public class OrderPayDetailWXPayResponse extends OMBaseResponse {

    public DataObjBean data;

    public DataObjBean getData() {
        return data;
    }

    public void setData(DataObjBean data) {
        this.data = data;
    }

    public static class DataObjBean {
        public String appPayRequest;
    }
}
