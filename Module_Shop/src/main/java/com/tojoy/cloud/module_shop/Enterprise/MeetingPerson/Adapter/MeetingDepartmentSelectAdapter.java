package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.cloud.module_shop.R;

import java.util.List;

/**
 * @author liuhm
 * @date 2020/2/26
 * @desciption 参会部门Adapter
 */
public class MeetingDepartmentSelectAdapter extends BaseRecyclerViewAdapter<MeetingPersonDepartmentResponse.DepartmentModel> {

    public MeetingDepartmentSelectAdapter(@NonNull Context context, @NonNull List<MeetingPersonDepartmentResponse.DepartmentModel> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull MeetingPersonDepartmentResponse.DepartmentModel data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_meeting_department;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new MeetingDepartmentSelectViewHolder(context, parent, getLayoutResId(viewType));
    }

    public class MeetingDepartmentSelectViewHolder extends BaseRecyclerViewHolder<MeetingPersonDepartmentResponse.DepartmentModel> {

        private CheckBox checkBox;
        private TextView mDepartnameTV;

        MeetingDepartmentSelectViewHolder(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            checkBox = (CheckBox) findViewById(R.id.cb);
            mDepartnameTV = (TextView) findViewById(R.id.tv_departname);
        }

        @Override
        public void onBindData(MeetingPersonDepartmentResponse.DepartmentModel data, int position) {
            mDepartnameTV.setText(data.name);
            checkBox.setChecked(data.isChecked);
            checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
                if (compoundButton.isPressed() && onCheckBoxListener != null) {
                    onCheckBoxListener.onDeptCheckListener(data, b);
                }
            });
        }
    }

    private OnDeptCheckBoxListener onCheckBoxListener;

    public void setOnCheckListener(OnDeptCheckBoxListener checkListener) {
        this.onCheckBoxListener = checkListener;
    }

    public interface OnDeptCheckBoxListener {
        void onDeptCheckListener(MeetingPersonDepartmentResponse.DepartmentModel data, boolean isChecked);
    }
}
