package com.tojoy.cloud.module_shop.Order.OrderSegment.View;

import android.content.Context;
import android.graphics.Typeface;

import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

/**
 * Created by yongchaowang
 * on 2020-03-01.
 * @author yongchaowang
 */
public class WycSimplePagerTitleView extends SimplePagerTitleView {
	public WycSimplePagerTitleView(Context context) {
		super(context);
	}

	@Override
	public void onLeave(int index, int totalCount, float leavePercent, boolean leftToRight) {
		super.onLeave(index, totalCount, leavePercent, leftToRight);
		setTypeface(Typeface.DEFAULT);//未选中的字体样式,根据需求自己做修改
	}

	/**
	 * 进入
	 *
	 * @param enterPercent 进入的百分比, 0.0f - 1.0f
	 * @param leftToRight  从左至右离开
	 */
	@Override
	public void onEnter(int index, int totalCount, float enterPercent, boolean leftToRight) {
		super.onEnter(index, totalCount, enterPercent, leftToRight);
		setTypeface(Typeface.DEFAULT_BOLD); //选中后的字体样式,根据需求自己做修改

	}

}
