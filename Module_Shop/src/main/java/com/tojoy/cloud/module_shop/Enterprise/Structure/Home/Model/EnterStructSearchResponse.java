package com.tojoy.cloud.module_shop.Enterprise.Structure.Home.Model;

import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Model.StaffModel;

import java.util.ArrayList;

public class EnterStructSearchResponse extends OMBaseResponse {
    public ArrayList<StaffModel> data = new ArrayList<>();
}
