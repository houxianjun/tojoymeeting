package com.tojoy.cloud.module_shop.Enterprise.Structure.Home.Controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.google.gson.Gson;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.cloud.module_shop.Api.ShopModuleApiProvider;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Controller.AddDepartmentAct;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Controller.StaffInfoAct;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.Model.DepartStaffEvent;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Crud.View.AddStaffDialog;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Home.Model.EnterStructModel;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Home.Model.EnterStructResponse;
import com.tojoy.cloud.module_shop.Enterprise.Structure.Home.View.EnterStructHomeAdapter;
import com.tojoy.cloud.module_shop.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 企业管理页面
 */
@Route(path = RouterPathProvider.EnterpriseStructHome)
public class EnterStructHomeVC extends UI {

    private static final String KEY_DEPT_ID = "deptId";
    private static final String KEY_DEPT_PID = "deptPId";
    private static final String KEY_DEPT_NAME = "deptName";

    private TJRecyclerView mRecyclerView;
    private EnterStructHomeAdapter mAdapter;
    private ArrayList<EnterStructModel> mStructList = new ArrayList();

    private boolean isDepart;

    private String departId = "";
    private String parentDepartId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rlv_enter_home_layout);
        initUI();
        initData();
    }

    private void initUI() {
        setStatusBar();
        setUpNavigationBar();
        hideBottomLine();
        showBack();

        findViewById(R.id.rlv_search).setOnClickListener(v -> EnterStructSearchAct.start(this, departId, parentDepartId));

        mRecyclerView = (TJRecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                getData();
            }

            @Override
            public void onLoadMore() {
            }
        });

        mAdapter = new EnterStructHomeAdapter(this, mStructList);

        mAdapter.mOperationCallback = new EnterStructHomeAdapter.OperationCallback() {
            @Override
            public void doSwitch(int pos) {
                mStructList.get(pos).isOpen = !mStructList.get(pos).isOpen;
                mAdapter.notifyItemChanged(pos);
            }

            @Override
            public void doAdd(int type) {
                //添加部门、员工
                if (type == 1) {
                    // 当前部门 departID 是"" 是首层进入添加员工 父部门 parentDepartId
                    new AddStaffDialog(BaseUserInfoCache.getCompanyCode(EnterStructHomeVC.this),
                            TextUtils.isEmpty(parentDepartId) ? "" : parentDepartId,
                            TextUtils.isEmpty(departId) ? "" : departId, EnterStructHomeVC.this).show();
                } else {
                    AddDepartmentAct.start(BaseUserInfoCache.getCompanyCode(EnterStructHomeVC.this),
                            TextUtils.isEmpty(parentDepartId) ? "" : parentDepartId,
                            TextUtils.isEmpty(departId) ? "" : departId,
                            isDepart ? getIntent().getStringExtra(KEY_DEPT_NAME) : BaseUserInfoCache.getCompanyName(EnterStructHomeVC.this),
                            EnterStructHomeVC.this);
                }
            }
        };

        mAdapter.mStaffDepartClickListener = (meetingModel, type) -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                if (type == 0 || type == 1) {
                    //跳转员工详情
                    StaffInfoAct.start(EnterStructHomeVC.this, BaseUserInfoCache.getCompanyCode(EnterStructHomeVC.this),
                            TextUtils.isEmpty(parentDepartId) ? "" : parentDepartId,
                            TextUtils.isEmpty(departId) ? "" : departId, new Gson().toJson(meetingModel));
                } else {
                    //跳转下一级
                    ARouter.getInstance().build(RouterPathProvider.EnterpriseStructHome)
                            .withString(KEY_DEPT_ID, meetingModel.deptId)//当前部门
                            .withString(KEY_DEPT_NAME, meetingModel.deptName)
                            .withString(KEY_DEPT_PID, departId)//当前的部门作为上级部门传入
                            .navigation();
                }
            }
        };
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initData() {
        if (!TextUtils.isEmpty(getIntent().getStringExtra(KEY_DEPT_ID))) {
            isDepart = true;
            //当前部门
            departId = getIntent().getStringExtra(KEY_DEPT_ID);
            //上级部门
            parentDepartId = getIntent().getStringExtra(KEY_DEPT_PID);
            setTitle(getIntent().getStringExtra(KEY_DEPT_NAME));
        } else {
            isDepart = false;
            setTitle(BaseUserInfoCache.getCompanyName(this));
        }
        mRecyclerView.autoRefresh();
    }

    private void getData() {
        //是子部门时，获取子部门的部门信息
        if (isDepart) {
            ShopModuleApiProvider.getInstance().getSubDepartUsers(BaseUserInfoCache.getCompanyCode(this), departId, new Observer<EnterStructResponse>() {
                @Override
                public void onCompleted() {
                    mRecyclerView.setRefreshing(false);
                }

                @Override
                public void onError(Throwable e) {
                    mRecyclerView.setRefreshing(false);
                    Toasty.normal(EnterStructHomeVC.this, "网络错误，请检查网络").show();
                }

                @Override
                public void onNext(EnterStructResponse response) {
                    if (response.isSuccess()) {
                        if (response.data == null) {
                            response.data = new EnterStructResponse.EnterStructResponseModel();
                        }
                        initStructModelList(response.data);

                    } else if ("-2".equals(response.code)) {
                        initStructModelList(new EnterStructResponse.EnterStructResponseModel());
                    } else {
                        Toasty.normal(EnterStructHomeVC.this, response.msg).show();
                    }
                }
            });
        } else {
            //组织架构首层数据获取
            ShopModuleApiProvider.getInstance().getEnterpriseUsers(BaseUserInfoCache.getCompanyCode(this), new Observer<EnterStructResponse>() {
                @Override
                public void onCompleted() {
                    mRecyclerView.setRefreshing(false);
                }

                @Override
                public void onError(Throwable e) {
                    mRecyclerView.setRefreshing(false);
                    Toasty.normal(EnterStructHomeVC.this, "网络错误，请检查网络").show();
                }

                @Override
                public void onNext(EnterStructResponse response) {
                    if (response.isSuccess()) {
                        if (response.data == null) {
                            response.data = new EnterStructResponse.EnterStructResponseModel();
                        }
                        initStructModelList(response.data);
                    } else {
                        Toasty.normal(EnterStructHomeVC.this, response.msg).show();
                    }
                }
            });
        }
    }

    private void initStructModelList(EnterStructResponse.EnterStructResponseModel response) {
        ArrayList<EnterStructModel> structList = new ArrayList<>();
        if (isDepart) {
            //员工
            EnterStructModel enterStructModel1 = new EnterStructModel();
            enterStructModel1.type = 1;
            enterStructModel1.staffList = response.userList;
            structList.add(enterStructModel1);

            //二级部门
            EnterStructModel enterStructModel2 = new EnterStructModel();
            enterStructModel2.type = 2;
            enterStructModel2.staffList = response.subDepartmentList;
            structList.add(enterStructModel2);
        } else {
            //管理员
            if (response.admin != null && response.admin.count > 0) {
                EnterStructModel enterStructModel = new EnterStructModel();
                enterStructModel.type = 0;
                enterStructModel.staffList = response.admin.userList;
                structList.add(enterStructModel);
            }

            //员工
            if (response.users != null) {
                EnterStructModel enterStructModel = new EnterStructModel();
                enterStructModel.type = 1;
                enterStructModel.staffList = response.users.userList;
                structList.add(enterStructModel);
            }

            //二级部门
            if (response.subDepartment != null) {
                EnterStructModel enterStructModel = new EnterStructModel();
                enterStructModel.type = 2;
                enterStructModel.staffList = response.subDepartment.subDepartmentList;
                structList.add(enterStructModel);
            }
        }

        mStructList = structList;
        mAdapter.updateData(mStructList);
    }


    /**
     * 接收刷新页面的事件
     * @param departStaffEvent
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDepartStaffEvent(DepartStaffEvent departStaffEvent) {
        if (TextUtils.isEmpty(departStaffEvent.departId)) {
            getData();
            return;
        }

       // if (departStaffEvent.departId.equals(departId)) {
            getData();
           // return;
       // }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()) {
            System.gc();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }
}
