package com.tojoy.cloud.module_shop.Enterprise.Structure.Home.Model;

import com.tojoy.cloud.module_shop.Enterprise.Structure.Model.StaffModel;
import com.tojoy.cloud.module_shop.R;

import java.util.ArrayList;
import java.util.List;

public class EnterStructModel {
    public String title;

    public ArrayList<StaffModel> staffList = new ArrayList();

    /**
     * 返回员工列表数据
     */
    public List<StaffModel> getStaffList() {
        //需要判断列表项是否折叠，
        if (isOpen) {
            //如果展开的则展示全部员工信息，
            return staffList;
        }
        //否则如果员工数超过2个人则只展示2前两个员工的信息
        else if (hasSwitch()) {
            return staffList.subList(0, 2);
        }
        return staffList;
    }

    public boolean hasSwitch() {
        if (staffList != null) {
            return staffList.size() > 2;
        } else {
            return false;
        }
    }

    public int type;
    public boolean isOpen;


    /**
     * 获取类型名称
     */
    public String getHeaderTitle() {

        String title = "";

        if (type == 0) {
            title = "管理员";
        }

        if (type == 1) {
            title = "部门员工";
        }

        if (type == 2) {
            title = "下级部门";
        }

        return title + "（" + (staffList == null ? "0" : staffList.size()) + "）";
    }

    /**
     * 获取类型图标
     */
    public int getTypeDrawable() {
        if (type == 0) {
            return R.drawable.icon_es_admin;
        } else if (type == 1) {
            return R.drawable.icon_es_manager;
        } else {
            return R.drawable.icon_es_department;
        }
    }
}
