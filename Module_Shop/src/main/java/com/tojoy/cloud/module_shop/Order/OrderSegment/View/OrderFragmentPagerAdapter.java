package com.tojoy.cloud.module_shop.Order.OrderSegment.View;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tojoy.cloud.module_shop.Order.OrderSegment.Controller.OrderManagementListFragment;

/**
 * 作者：jiangguoqiu on 2020/2/25 15:45
 */
public class OrderFragmentPagerAdapter extends FragmentPagerAdapter {
	private int[] CHANNELS;
	private Context mContext;

	public OrderFragmentPagerAdapter(Context context, FragmentManager fm, int[] strings) {
		super(fm);
		CHANNELS = strings;
		mContext = context;
	}

	@Override
	public Fragment getItem(int position) {
		int type = CHANNELS[position];
		return new OrderManagementListFragment(mContext, type);
	}

	@Override
	public int getCount() {
		return CHANNELS.length;
	}
}
