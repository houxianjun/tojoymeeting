package com.tojoy.cloud.module_shop.Order.CreatOrder.Controller;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.enterprise.NewGoodsDetailResponse;
import com.tojoy.tjoybaselib.model.intentionalorder.makesure.AddressDetailModel;
import com.tojoy.tjoybaselib.model.intentionalorder.makesure.NewOrderMakeOrderResponse;
import com.tojoy.tjoybaselib.model.user.AddressResponseInfo;
import com.tojoy.tjoybaselib.model.user.MyAddressListResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.util.log.LogUtil;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.net.NetWorkUtils;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.string.StringUtil;
import com.tojoy.cloud.module_shop.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import es.dmoral.toasty.Toasty;
import rx.Observer;


/**
 * 确认意向订单
 * Created by daibin
 * on 2019/9/9.
 * function
 */
@Route(path = RouterPathProvider.Pay_IntentionalOrderMakeSure)
public class IntentionalOrderMakeSure extends UI {
    private TextView mBuyerNameTv, mBuyerPhoneNumTv, mBuyerAdressTv, mCommodityNameTv, mCommodityReactPriceTv, mCommodityReactPriceBottomTv, mCommodityNumTv, mCommodityRemarksTv,
            mCommodityTotalTv, mTotoalNumTv, mTvRealyButGoodszAcount, mShopName;
    private ImageView mCommdityIconIv, mPlusTv, mReduceTv;

    private RelativeLayout mErrorRlv, mMakeSureRlv, mAddAddressRlv, mOderAddressDesRlv, mOderAddressRlv, mNoSupperOnlineSendRlv, mIntentSure;
    private View mLiAddress;
    private View mScDetailView;
    private ImageView mShopCover;

    private NewGoodsDetailResponse mGoodsDetailModel;
    private AddressDetailModel addressDetailModel;
    private AddressResponseInfo addressResponseInfo;
    private EditText mNumEdt, mRemarkEdt;


    //userOrderNo：用于支付宝、微信下单支付的订单号 aliWxTradeNo：支付宝、微信支付后返的用于查询支付是否成功的子订单
    private String userOrderNo;


    private boolean first = true;
    private String oldNum;

    private String mGoodsId, mRoomLiveId,sourceWay,agentCompanyCode,agentId,agentLive;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_intentional_order_makesure);
        EventBus.getDefault().register(this);

        setStatusBar();
        initTitle();
        initUI();
        initData();
    }

    private void initTitle() {
        setUpNavigationBar();
        showBack();
        mTitle.setText("确认订单");
    }

    private void initUI() {
        mBuyerNameTv = (TextView) findViewById(R.id.tvBuyerName);
        mBuyerPhoneNumTv = (TextView) findViewById(R.id.tvBuyerPhoneNum);
        mBuyerAdressTv = (TextView) findViewById(R.id.tvBuyerAdress);
        mCommodityNameTv = (TextView) findViewById(R.id.tvCommodityName);
        mCommodityReactPriceTv = (TextView) findViewById(R.id.tvCommodityReactPrice);
        mCommodityNumTv = (TextView) findViewById(R.id.tvCommodityNum);
        mCommodityTotalTv = (TextView) findViewById(R.id.tvCommodityTotal);
        mTotoalNumTv = (TextView) findViewById(R.id.tvTotoalNum);
        mShopName = (TextView) findViewById(R.id.tvCommdityOrderTitle);
        mTvRealyButGoodszAcount = (TextView) findViewById(R.id.sum_toll_2);
        mAddAddressRlv = (RelativeLayout) findViewById(R.id.rlv_add_address);
        mOderAddressDesRlv = (RelativeLayout) findViewById(R.id.rlv_oder_address_des);
        mOderAddressRlv = (RelativeLayout) findViewById(R.id.rlv_oder_address);
        mNoSupperOnlineSendRlv = (RelativeLayout) findViewById(R.id.rlv_no_supper_address);
        mCommodityReactPriceBottomTv = (TextView) findViewById(R.id.tvCommodityReactPrice_bottom);
        mShopCover = (ImageView) findViewById(R.id.img_shop);


        mLiAddress = findViewById(R.id.li_Address);
        mPlusTv = (ImageView) findViewById(R.id.tvPlusNum);
        mReduceTv = (ImageView) findViewById(R.id.tvReduceNum);

        mCommdityIconIv = (ImageView) findViewById(R.id.ivCommdityIcon);

        mNumEdt = (EditText) findViewById(R.id.edit_commdity_num);
        mRemarkEdt = (EditText) findViewById(R.id.edit_remarks);

        mErrorRlv = (RelativeLayout) findViewById(R.id.rlv_error);
        mMakeSureRlv = (RelativeLayout) findViewById(R.id.rlv_sure);
        mIntentSure = (RelativeLayout) findViewById(R.id.rlv_intent_sure);
        mScDetailView = findViewById(R.id.sc_detail);
    }

    private void initData() {
        mGoodsId = getIntent().getStringExtra("goodsId");
        mRoomLiveId = getIntent().getStringExtra("roomLiveId");
        sourceWay = getIntent().getStringExtra("sourceWay");
        agentCompanyCode = getIntent().getStringExtra("agentCompanyCode");
        agentId= getIntent().getStringExtra("agentId");
        agentLive= getIntent().getStringExtra("agentLive");
        LogUtil.d("代播商品","确认订单 agentCompanyCode=" +agentCompanyCode +"agentId="+agentId
                +"agentLive="+agentLive );
        requestData();
        getAddress();
        initListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @SuppressLint("ClickableViewAccessibility")
    private void initListener() {
        mNumEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mNumEdt.getText() != null) {
                    String numString = mNumEdt.getText().toString();
                    if (!TextUtils.isEmpty(numString)) {
                        long num = Long.parseLong(numString);
                        long limitCount;
                        if (mGoodsDetailModel == null || mGoodsDetailModel.data == null) {
                            limitCount = 1;
                        } else {
                            limitCount = mGoodsDetailModel.data.getStockNum();
                        }
                        if (num <= 0) {
                            mNumEdt.setText(oldNum);
                            mNumEdt.setSelection(mNumEdt.length());
                        } else if (num > limitCount) {

                            Toast toast = Toasty.normal(IntentionalOrderMakeSure.this, mGoodsDetailModel.data.getLimtString());
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                            mNumEdt.setText(String.valueOf(limitCount));
                            mNumEdt.setSelection(mNumEdt.length());
                        }
                        upDateGoodsAccountAndPice();
                    } else {
                        upDateGoodsAccountAndPice();

                    }

                } else {
                    mNumEdt.setText("1");
                    upDateGoodsAccountAndPice();
                }
                oldNum = mNumEdt.getText().toString();
            }
        });

        mNumEdt.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                // 此处为得到焦点时的处理内容
                int end = mNumEdt.getSelectionEnd();
                if (0 == end) {
                    mNumEdt.setSelection(mNumEdt.length());
                }
            }
        });
        mNumEdt.setOnClickListener(v -> {
            int end = mNumEdt.getSelectionEnd();
            if (0 == end) {
                mNumEdt.setSelection(mNumEdt.getText().length());
            }
            mNumEdt.setCursorVisible(true);
        });

        mNumEdt.setOnTouchListener((v, event) -> {
            int end = mNumEdt.getSelectionEnd();
            if (0 == end && first) {
                first = false;
                mNumEdt.setText("1");
                mNumEdt.setSelection(mNumEdt.getText().length());
                mNumEdt.setCursorVisible(true);
            }
            return false;
        });

        mPlusTv.setOnClickListener(v -> {
            mNumEdt.setCursorVisible(false);
            String count = "0";
            if (!TextUtils.isEmpty(mNumEdt.getText().toString())) {
                count = mNumEdt.getText().toString();
            }
            Long num = Long.parseLong(count);
            mNumEdt.setText((num + 1) + "");
        });

        mReduceTv.setOnClickListener(v -> {
            mNumEdt.setCursorVisible(false);
            String count = "0";
            if (!TextUtils.isEmpty(mNumEdt.getText().toString())) {
                count = mNumEdt.getText().toString();
            }
            Long num = Long.parseLong(count);
            if (num >= 2) {
                mNumEdt.setText((num - 1) + "");
            }
        });

        mMakeSureRlv.setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isFastDoubleClick(mMakeSureRlv.getId())) {
                //防止同时点击过多下单
                String numString = mNumEdt.getText().toString();
                if (TextUtils.isEmpty(numString)) {
                    Toasty.normal(IntentionalOrderMakeSure.this, "商品数量不能为空").show();
                    return;
                }

                if (mGoodsDetailModel != null && mGoodsDetailModel.data != null) {
                    if (mGoodsDetailModel.data.isSupperSendGoods() && addressDetailModel == null) {
                        Toasty.normal(IntentionalOrderMakeSure.this, "请添加收货地址").show();
                        return;
                    }
                }
                makeOrder();
            }
        });


        mIntentSure.setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isFastDoubleClick(mIntentSure.getId())) {
                //防止同时点击过多下单
                String numString = mNumEdt.getText().toString();
                if (TextUtils.isEmpty(numString)) {
                    Toasty.normal(IntentionalOrderMakeSure.this, "商品数量不能为空").show();
                    return;
                }

                if (mGoodsDetailModel != null && mGoodsDetailModel.data != null) {
                    if (mGoodsDetailModel.data.isSupperSendGoods() && addressDetailModel == null) {
                        Toasty.normal(IntentionalOrderMakeSure.this, "请添加收货地址").show();
                        return;
                    }
                }
                makeOrder();
            }
        });

        mLiAddress.setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isFastDoubleClick(mLiAddress.getId())) {
                if (addressDetailModel == null) {
                    //跳到新增地址
                    ARouter.getInstance().build(RouterPathProvider.APP_ADD_ADDRESS)
                            .withString("from", "2")
                            .navigation();
                } else {
                    //跳到地址管理选择
                    ARouter.getInstance().build(RouterPathProvider.App_AddressManager)
                            .withString("from", "1")
                            .navigation();
                }
            }
        });

        mScDetailView.setOnClickListener(v -> showKeyboard(false));
    }

    private void showReduceView(boolean isLarge) {
        mReduceTv.setBackgroundResource(isLarge ? R.drawable.order_subtract_on : R.drawable.order_subtract);
    }

    /**
     * 获取商品相关信息
     */
    private void requestData() {
        if (!NetWorkUtils.isNetworkConnected(this)) {
            Toasty.normal(this, "网络断开，请检查网络").show();
            haveData(false);
            return;
        }
        showProgressHUD(this, "");
        getGoods();
    }


    /**
     * 获取下单地址
     */
    private void getAddress() {
        OMAppApiProvider.getInstance().getMyAddressList(1, BaseUserInfoCache.getUserId(this), new Observer<MyAddressListResponse>() {
            @Override
            public void onCompleted() {
                updateAddress(addressDetailModel);
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(MyAddressListResponse baseResponse) {
                if (baseResponse.isSuccess()) {
                    if (baseResponse.data.list != null) {
                        if (baseResponse.data.list.size() > 0) {
                            addressResponseInfo = baseResponse.data.list.get(0);
                            addressDetailModel = new AddressDetailModel();
                            addressDetailModel.pkId = addressResponseInfo.pkId;
                            addressDetailModel.provinceName = addressResponseInfo.provinceName;
                            addressDetailModel.userName = addressResponseInfo.userName;
                            addressDetailModel.userMobile = addressResponseInfo.userMobile;
                            addressDetailModel.remark = addressResponseInfo.remark;
                            addressDetailModel.cityName = addressResponseInfo.cityName;
                            addressDetailModel.districtName = addressResponseInfo.districtName;
                        }
                    }
                }
            }
        });
    }

    private void getGoods() {
        OMAppApiProvider.getInstance().mNewGoodsDetail(mGoodsId, new Observer<NewGoodsDetailResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();

            }

            @Override
            public void onError(Throwable e) {
                dismissProgressHUD();
                haveData(false);
                Toasty.normal(IntentionalOrderMakeSure.this, "网络异常，稍后再试").show();

            }

            @Override
            public void onNext(NewGoodsDetailResponse newGoodsDetailResponse) {
                dismissProgressHUD();
                if (newGoodsDetailResponse.isSuccess()) {
                    mGoodsDetailModel = newGoodsDetailResponse;
                    updateDetail();
                } else {
                    haveData(false);
                    Toasty.normal(IntentionalOrderMakeSure.this, newGoodsDetailResponse.msg).show();
                }

            }
        });
    }

    /**
     * 有无数据的显示
     *
     * @param haveData
     */
    private void haveData(boolean haveData) {
        if (haveData) {
            mScDetailView.setVisibility(View.VISIBLE);
            mMakeSureRlv.setVisibility(View.VISIBLE);
            mIntentSure.setVisibility(View.VISIBLE);
            mErrorRlv.setVisibility(View.GONE);
        } else {
            mScDetailView.setVisibility(View.GONE);
            mMakeSureRlv.setVisibility(View.GONE);
            mIntentSure.setVisibility(View.GONE);
            mErrorRlv.setVisibility(View.VISIBLE);
        }
    }

    private void updateDetail() {
        if (mGoodsDetailModel == null || mGoodsDetailModel.data == null) {
            haveData(false);
            return;
        }
        if (mGoodsDetailModel.data.isSupperSendGoods()) {
            mOderAddressRlv.setVisibility(View.VISIBLE);
        } else {
            mNoSupperOnlineSendRlv.setVisibility(View.VISIBLE);
            mOderAddressRlv.setVisibility(View.GONE);
        }
        mCommodityNameTv.setText(mGoodsDetailModel.data.name);
        if (mGoodsDetailModel.data.isIntentOder()) {
            mShopName.setText(mGoodsDetailModel.data.companyName);
        } else {
            mShopName.setText(mGoodsDetailModel.data.sellStoreName);
        }
        ImageLoaderManager.INSTANCE.loadCircleImage(this, mShopCover, mGoodsDetailModel.data.sellStoreImg, R.drawable.order_shop);

        // mCommodityPriceTv.setText(StringUtil.getInsertedString(mGoodsDetailModel.data.costPrice, 1f));
        // mCommodityPriceTv.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);

        mCommodityReactPriceTv.setText(StringUtil.getInsertedString(mGoodsDetailModel.data.getSalePrice(), 1f));
        ImageLoaderManager.INSTANCE.loadImage(this, mCommdityIconIv, mGoodsDetailModel.data.getCoverImg(), R.drawable.nim_image_default, 0);
        if (mGoodsDetailModel.data.isIntentOder()) {
            mTitle.setText("确认意向订单");
            mIntentSure.setVisibility(View.VISIBLE);
            mMakeSureRlv.setVisibility(View.GONE);
        } else {
            mTitle.setText("确认订单");
            mIntentSure.setVisibility(View.GONE);
            mMakeSureRlv.setVisibility(View.VISIBLE);
        }
        upDateGoodsAccountAndPice();
    }


    private void upDateGoodsAccountAndPice() {
        if (mGoodsDetailModel == null || mGoodsDetailModel.data == null) {
            return;
        }
        String count = "0";
        if (!TextUtils.isEmpty(mNumEdt.getText().toString())) {
            count = mNumEdt.getText().toString();
        }
        if (Long.parseLong(count) > 1) {
            showReduceView(true);
        } else {
            showReduceView(false);
        }
        mCommodityNumTv.setText("x" + count);
        mTotoalNumTv.setText("共" + count + "件");
        //小计
        mCommodityTotalTv.setText(StringUtil.getInsertedString(StringUtil.getFormatedFloatString(Long.parseLong(count) * Double.parseDouble(mGoodsDetailModel.data.salePrice) + ""), 1f));

        if (!mGoodsDetailModel.data.isIntentOder()) {
            mTvRealyButGoodszAcount.setText("共计" + count + "件");
            //合计
            mCommodityReactPriceBottomTv.setText(StringUtil.getInsertedString(StringUtil.getFormatedFloatString(Long.parseLong(count) * Double.parseDouble(mGoodsDetailModel.data.salePrice) + ""), 1f));
        }
    }

    private void updateAddress(AddressDetailModel addressDetailModel) {
        if (addressDetailModel == null) {
            mAddAddressRlv.setVisibility(View.VISIBLE);
            mOderAddressDesRlv.setVisibility(View.GONE);
        } else {
            mAddAddressRlv.setVisibility(View.GONE);
            mOderAddressDesRlv.setVisibility(View.VISIBLE);
            mBuyerNameTv.setText(addressDetailModel.userName);
            mBuyerPhoneNumTv.setText(addressDetailModel.userMobile);
            mBuyerAdressTv.setText(String.format("%s %s %s %s", addressDetailModel.provinceName, addressDetailModel.cityName, addressDetailModel.districtName, addressDetailModel.remark));
        }
    }


    /**
     * 下单
     */
    private void makeOrder() {
        if (!NetWorkUtils.isNetworkConnected(this)) {
            Toasty.normal(this, "网络断开，请检查网络").show();
            return;
        }

        if (mGoodsDetailModel == null || mGoodsDetailModel.data == null) {
            Toasty.normal(this, "订单信息错误").show();
            return;
        }

        if (!mGoodsDetailModel.data.isShelf()) {
            Toasty.normal(this, "该商品已下架").show();
            return;
        }

        if (!mGoodsDetailModel.data.isChackSuccess()) {
            Toasty.normal(this, "该商品审核未通过").show();
            return;
        }

        showProgressHUD(this, "");
        OMAppApiProvider.getInstance().newSureOder(mGoodsId, addressDetailModel == null ? "" : addressDetailModel.pkId + "",
                mNumEdt.getText().toString(), "1",
                mGoodsDetailModel.data.getOderType(), mRoomLiveId,
                mGoodsDetailModel.data.sellStoreId, mGoodsDetailModel.data.companyCode,
                mRemarkEdt.getText().toString(), mGoodsDetailModel.data.salePrice,sourceWay!=null?sourceWay:"2", agentCompanyCode,agentId,agentLive,new Observer<NewOrderMakeOrderResponse>() {
                    @Override
                    public void onCompleted() {
                        dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismissProgressHUD();
                        Toasty.normal(IntentionalOrderMakeSure.this, "网络异常，稍后再试").show();
                    }

                    @Override
                    public void onNext(NewOrderMakeOrderResponse newOrderMakeOrderResponse) {
                        dismissProgressHUD();
                        if (newOrderMakeOrderResponse.isSuccess()) {
                            //下单成功
                            if (mGoodsDetailModel.data.isIntentOder()) {
                                //是意向订单
                                new TJMakeSureDialog(IntentionalOrderMakeSure.this, v1 -> finish()).setTitleAndCotent("提示", "您的意向订单已经提交成功,稍后会有客服人员与您联系").setBtnSureText("确认").showAlert();
                            } else {
                                //是实际订单 跳收银台
                                userOrderNo = newOrderMakeOrderResponse.data.orderCode;
                                RouterJumpUtil.startOrderPayDetailAct(newOrderMakeOrderResponse.data.orderId, userOrderNo,
                                        StringUtil.getInsertedString(StringUtil.getFormatedFloatString(Long.parseLong(mNumEdt.getText().toString()) * Double.parseDouble(mGoodsDetailModel.data.salePrice) + "")),
                                        "");
                                finish();
                            }
                        } else if ("2".equals(newOrderMakeOrderResponse.code)) {
                            //价格有变动
                            if (newOrderMakeOrderResponse.data == null) {
                                Toasty.normal(IntentionalOrderMakeSure.this, newOrderMakeOrderResponse.msg).show();
                                return;
                            }
                            new TJMakeSureDialog(IntentionalOrderMakeSure.this, v1 -> requestData()).setTitleAndCotent("提示", "当前商品价格变更为￥" + newOrderMakeOrderResponse.data.orderPayPrice).hideTitle().setBtnSureText("继续下单").show();
                        } else {
                            Toasty.normal(IntentionalOrderMakeSure.this, newOrderMakeOrderResponse.msg).show();
                        }
                    }
                });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    /**
     * 刷新地址信息
     *
     * @param detailModel
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void refreshData(AddressDetailModel detailModel) {
        addressDetailModel = detailModel;
        updateAddress(addressDetailModel);
    }
}
