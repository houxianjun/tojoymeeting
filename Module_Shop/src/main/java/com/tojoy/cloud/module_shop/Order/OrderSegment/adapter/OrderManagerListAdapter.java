package com.tojoy.cloud.module_shop.Order.OrderSegment.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.module_shop.Order.OrderSegment.Model.OrderManagementModel;
import com.tojoy.cloud.module_shop.Order.utils.OrderConstants;
import com.tojoy.cloud.module_shop.R;

import java.util.List;

/**
 * Created by yongchaowang
 * on 2020-02-29.
 *
 * @author yongchaowang
 */
public class OrderManagerListAdapter extends BaseQuickAdapter<OrderManagementModel, BaseViewHolder> {
    public OrderManagerListAdapter(List<OrderManagementModel> data) {
        super(R.layout.item_order_management_layout, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, OrderManagementModel data) {
        helper.setText(R.id.tv_oder_status, data.getOrderState());
        helper.setGone(R.id.tv_oder_status, true);
        helper.setGone(R.id.rlv_image_shop_in, true);
        switch (data.getOrderStatus()) {
            case OrderConstants.ORDER_STATUS_WAIT_PAY:
                helper.setGone(R.id.rlv_money, true);
                helper.setGone(R.id.tv_order_delete, false);
                helper.setGone(R.id.order_cancel_tv, true);
                helper.setGone(R.id.order_pay_tv, true);
                break;
            case OrderConstants.ORDER_STATUS_WAIT_EMAIL:
                helper.setGone(R.id.rlv_money, false);
                break;
            case OrderConstants.ORDER_STATUS_WAIT_RECEIVING:
                helper.setGone(R.id.rlv_money, false);
                break;
            case OrderConstants.ORDER_STATUS_CANCELED:
                helper.setGone(R.id.rlv_money, true);
                helper.setGone(R.id.tv_order_delete, true);
                helper.setGone(R.id.order_cancel_tv, false);
                helper.setGone(R.id.order_pay_tv, false);
                break;
            case OrderConstants.ORDER_STATUS_DONE:
                helper.setGone(R.id.rlv_money, false);
                break;
            default:
                helper.setGone(R.id.rlv_money, true);
                helper.setGone(R.id.tv_order_delete, true);
                helper.setGone(R.id.order_cancel_tv, false);
                helper.setGone(R.id.order_pay_tv, false);
                break;
        }
        if (!data.isNormalOrder()) {
            helper.setGone(R.id.rlv_money, true);
            helper.setGone(R.id.tv_order_delete, true);
            helper.setGone(R.id.order_cancel_tv, false);
            helper.setGone(R.id.order_pay_tv, false);
        }
        helper.setText(R.id.tvCommdityOrderTitle, data.getStoreName());
        helper.setText(R.id.tvCommodityName, data.getGoodsName());
        helper.setText(R.id.tvCommodityNum, "×" + data.getGoodsNum());
        helper.setText(R.id.tvTotoalNum, "共" + data.getGoodsNum() + "件");
        helper.setText(R.id.tvCommodityReactPrice, data.getGoodsSellerPrice());
        helper.setText(R.id.tvCommodityTotal, data.getOrderPrice());
        ImageLoaderManager.INSTANCE.loadRoundCornerImage(helper.itemView.getContext(), helper.getView(R.id.ivCommdityIcon), data.getGoodsImgurl(), 0, AppUtils.dip2px(helper.itemView.getContext(), 64), AppUtils.dip2px(helper.itemView.getContext(), 64), R.drawable.nim_image_default);
        ImageLoaderManager.INSTANCE.loadCircleImage(helper.itemView.getContext(), helper.getView(R.id.img_shop), data.getStoreAvatar(), R.drawable.order_shop);
        helper.addOnClickListener(R.id.order_cancel_tv);
        helper.addOnClickListener(R.id.order_pay_tv);
        helper.addOnClickListener(R.id.tv_order_delete);
        helper.addOnClickListener(R.id.img_shop);
        helper.addOnClickListener(R.id.tvCommdityOrderTitle);
        helper.addOnClickListener(R.id.rlv_image_shop_in);

    }
}
