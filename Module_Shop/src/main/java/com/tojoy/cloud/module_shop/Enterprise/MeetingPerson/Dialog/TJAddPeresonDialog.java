package com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.module_shop.Enterprise.MeetingPerson.Adapter.MeetingAddPersonWrongAdapter;
import com.tojoy.cloud.module_shop.R;

import java.util.List;

public class TJAddPeresonDialog {

    private Context mContext;
    private View mView;
    private Dialog alertDialog;

    private List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> mlist;

    private String mcount;

    public TJAddPeresonDialog(Context context, String count, List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> list) {
        mContext = context;
        mlist = list;
        mcount = count;
        initUI();

    }

    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.popup_add_person_dialog, null);
        RelativeLayout rvWrongContent = mView.findViewById(R.id.rv_wrong_content);
        TextView tvTitle = mView.findViewById(R.id.tv_title);
        tvTitle.setText(String.format(mContext.getResources().getString(R.string.meeting_add_person_success), mcount));
        RecyclerView rvContent = mView.findViewById(R.id.rv_content);
        if (mlist.size() > 0) {
            rvWrongContent.setVisibility(View.VISIBLE);
        } else {
            rvWrongContent.setVisibility(View.GONE);
        }
        MeetingAddPersonWrongAdapter wrongAdapter = new MeetingAddPersonWrongAdapter(mContext, mlist);
        rvContent.setLayoutManager(new LinearLayoutManager(mContext));
        rvContent.setAdapter(wrongAdapter);
    }


    public void show() {
        if (mView.getParent() != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            parent.removeAllViews();
        }
        alertDialog = new Dialog(mContext, R.style.tjoy_dialog_style);
        alertDialog.setContentView(mView);
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        alertDialog.setOnKeyListener((dialog, keyCode, event) -> true);
        //新增dialog整体宽高设置 适配ui
        Window win = alertDialog.getWindow();
        win.getDecorView().setPadding(0, 0, 0, 0);
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        win.setAttributes(lp);
    }

    public void show(int marginWidth) {
        if (mView.getParent() != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            parent.removeAllViews();
        }
        alertDialog = new Dialog(mContext, R.style.tjoy_dialog_style);
        alertDialog.setContentView(mView, new ViewGroup.LayoutParams(AppUtils.getWidth(mContext) - AppUtils.dip2px(mContext, marginWidth), ViewGroup.LayoutParams.WRAP_CONTENT));
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        alertDialog.setOnKeyListener((dialog, keyCode, event) -> true);
    }


    /**
     * 判断是否在展示
     *
     * @return
     */
    private boolean isShowing() {
        if (alertDialog == null) {
            return false;
        }
        return alertDialog.isShowing();
    }

    public void dismiss() {
        if (isShowing()) {
            alertDialog.dismiss();
        }
    }

    public void setEvent(View.OnClickListener onClickListener) {
        mView.findViewById(R.id.button_confirm_update).setOnClickListener(onClickListener);
    }
}
