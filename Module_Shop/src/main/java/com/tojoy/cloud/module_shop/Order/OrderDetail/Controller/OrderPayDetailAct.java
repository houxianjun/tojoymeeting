package com.tojoy.cloud.module_shop.Order.OrderDetail.Controller;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.PayDialog;
import com.tojoy.tjoybaselib.ui.CountDownView.CountDownTextView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.router.RouterConstants;
import com.tojoy.tjoybaselib.util.sys.ShopActAtack;
import com.tojoy.cloud.module_shop.Api.ShopModuleApiProvider;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.NewOderDetailModel;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.OrderChangeEvent;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.OrderPayDetailTimeResponse;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.OrderPayDetailWXPayResponse;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.RechargeOrderStatusResponse;
import com.tojoy.cloud.module_shop.R;
import com.tojoy.common.Services.EventBus.PayEvent;
import com.tojoy.common.Services.PayAndShare.AllPayRequestUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;
import rx.Observer;

import static com.tojoy.tjoybaselib.util.router.RouterJumpUtil.SKIP_FROM_DETAIL;
import static com.tojoy.tjoybaselib.util.router.RouterJumpUtil.SKIP_FROM_RECHARGE_RECORD;
import static com.tojoy.tjoybaselib.util.router.RouterJumpUtil.SKIP_FROM_RECHARGE_TIME;

@Route(path = RouterPathProvider.PayDetail)
public class OrderPayDetailAct extends UI implements CountDownTextView.OnCountDownFinishListener, CountDownTextView.OnCountDownTickListener {
    private RelativeLayout mPayingLayout;
    private RelativeLayout mPaySuccessLayout;

    private String mOrderId, mOrderCodeId, mTotalMoney;
    private OrderPayDetailWXPayResponse mOrderData;
    private TextView mTotalMoneyTv, mPayDetailsTv;
    private CountDownTextView mCountDownView;
    private long mNum;
    //mfrom 有3中状态 1 为"" 2 SKIP_FROM_DETAIL 3 SKIP_FROM_RECHARGE_TIME
    private String mFrom;
    //是否通过小程序从微信回来
    private PayDialog mPayCountDownDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_pay_detail);
        EventBus.getDefault().register(this);
        initUI();
        mOrderId = getIntent().getStringExtra(RouterConstants.ORDER_ID);
        mOrderCodeId = getIntent().getStringExtra(RouterConstants.ORDER_CODE_ID);
        mTotalMoney = getIntent().getStringExtra(RouterConstants.ORDER_TOTAL_MONEY);
        mFrom = getIntent().getStringExtra(RouterConstants.ORDER_PAY_FROM);
        getOrderPayTime(false, false);
    }

    private void initUI() {
        setStatusBar();
        setUpNavigationBar();
        showBack();
        setTitle("收银台");
        setSwipeBackEnable(false);
        mPayingLayout = findView(R.id.rlv_pay);
        mPaySuccessLayout = findView(R.id.rlv_paysuccess);
        mCountDownView = findView(R.id.countdownView);
        mTotalMoneyTv = findView(R.id.pay_money_num_tv);
        mPayDetailsTv = findView(R.id.pay_money_details_tv);
        mPayCountDownDialog = new PayDialog(this);
        mPayDetailsTv.setOnClickListener(v -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }
            getPayParams();
        });
        findView(R.id.pay_success_check_tv).setOnClickListener(v ->

                skipToRechargeRecode()
        );
    }


    private void getOrderPayTime(boolean forShowDialog, boolean needFinish) {


        if(mFrom!=null){

            if(mFrom.equals(SKIP_FROM_RECHARGE_TIME)|| mFrom.equals(SKIP_FROM_RECHARGE_RECORD)){

                ShopModuleApiProvider.getInstance().queryRechargeUserPaySurplusTime(mOrderCodeId, new Observer<OrderPayDetailTimeResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(OrderPayDetailAct.this, e.getMessage()).show();
                        if (needFinish) {
                            finish();
                        }
                    }

                    @Override
                    public void onNext(OrderPayDetailTimeResponse response) {
                        if (response.isSuccess()) {
                            if (forShowDialog) {
                                //绑定的activity不存在了
                                if(!mPayCountDownDialog.isLiving()){
                                     return;
                                }
                                if (mPayCountDownDialog.isShowing()) {
                                    mPayCountDownDialog.dismiss();
                                }
                                mPayCountDownDialog.show();
                                mPayCountDownDialog.setCountDownTime(response.getData().orderSurplusTime);
                                mPayCountDownDialog.initEvent(v -> getPayParams(), v -> {
                                    ShopActAtack.getActivityStack().popAllActivity();
                                    finish();
                                });
                            }
                            setMoneyAndCountDown(response);
                        } else {
                            Toasty.normal(OrderPayDetailAct.this, response.msg).show();
                            if (needFinish) {
                                finish();
                            }
                        }
                    }
                });
            }

            else {
                ShopModuleApiProvider.getInstance().getOrderTimeDetail(mOrderCodeId, new Observer<OrderPayDetailTimeResponse>() {
                    @Override
                    public void onCompleted() {
                        dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(OrderPayDetailAct.this, e.getMessage()).show();
                        if (needFinish) {
                            finish();
                        }
                    }

                    @Override
                    public void onNext(OrderPayDetailTimeResponse response) {
                        if (response.isSuccess()) {
                            if (forShowDialog) {
                                if (mPayCountDownDialog.isShowing()) {
                                    mPayCountDownDialog.dismiss();
                                }
                                mPayCountDownDialog.show();
                                mPayCountDownDialog.setCountDownTime(response.getData().orderSurplusTime);
                                mPayCountDownDialog.initEvent(v -> getPayParams(), v -> {
                                    ShopActAtack.getActivityStack().popAllActivity();
                                    finish();
                                });
                            }
                            setMoneyAndCountDown(response);
                        } else {
                            Toasty.normal(OrderPayDetailAct.this, response.msg).show();
                            if (needFinish) {
                                finish();
                            }
                        }
                    }
                });

            }
        }

    }


    public void setMoneyAndCountDown(OrderPayDetailTimeResponse response) {
        mTotalMoney = response.getData().orderPayPrice;
        mTotalMoneyTv.setText(mTotalMoney);
        mPayDetailsTv.setText(String.format(getResources().getString(R.string.order_wx_pay_more), mTotalMoney));
        mCountDownView
                .setCountDownText("支付剩余时间  ", "")
                .setCloseKeepCountDown(false)
                .setCountDownClickable(false)
                .setShowFormatTime(true)
                .setNormalText("处理中")
                .setOnCountDownFinishListener(this)
                .setOnCountDownTickListener(this)
                .setIntervalUnit(TimeUnit.SECONDS)
                .startCountDown(response.getData().orderSurplusTime);
    }

    //获取支付信息
    private void getPayParams() {

        showProgressHUD(this, "");

        if(mFrom!=null){

            if(mFrom.equals(SKIP_FROM_RECHARGE_TIME)|| mFrom.equals(SKIP_FROM_RECHARGE_RECORD)){
                ShopModuleApiProvider.getInstance().queryRechargePlatformAliWxPay(mOrderCodeId, "3", new Observer<OrderPayDetailWXPayResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(OrderPayDetailAct.this, e.getMessage()).show();
                    }

                    @Override
                    public void onNext(OrderPayDetailWXPayResponse response) {
                        if (response.isSuccess()) {
                            mOrderData = response;
                            AllPayRequestUtils.doMiniPay(OrderPayDetailAct.this, mOrderData.data.appPayRequest, mTotalMoney, mNum);
                            AppConfig.hasSkipToMini = true;
                        } else {
                            dismissProgressHUD();
                            Toasty.normal(OrderPayDetailAct.this, response.msg).show();
                        }
                    }
                });
            }
            else{
                ShopModuleApiProvider.getInstance().getOrderPayParams(mOrderCodeId, new Observer<OrderPayDetailWXPayResponse>() {
                    @Override
                    public void onCompleted() {
                        dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(OrderPayDetailAct.this, e.getMessage()).show();
                    }

                    @Override
                    public void onNext(OrderPayDetailWXPayResponse response) {
                        if (response.isSuccess()) {
                            mOrderData = response;
                            AllPayRequestUtils.doMiniPay(OrderPayDetailAct.this, mOrderData.data.appPayRequest, mTotalMoney, mNum);
                            AppConfig.hasSkipToMini = true;
                        } else {
                            Toasty.normal(OrderPayDetailAct.this, response.msg).show();
                        }
                    }
                });
            }

        }

    }

    //跳转到微信支付回来后查询订单信息
    private void queryOrderPayinfo(int retryIndex) {

        if(mFrom!=null){
            if(mFrom.equals(SKIP_FROM_RECHARGE_TIME)|| mFrom.equals(SKIP_FROM_RECHARGE_RECORD)){

                ShopModuleApiProvider.getInstance().queryRechargeOrderDetail(mOrderId, new Observer<RechargeOrderStatusResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        dismissProgressHUD();
                        Toasty.normal(OrderPayDetailAct.this, "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(RechargeOrderStatusResponse oderDetailModel) {
                        if (oderDetailModel.isSuccess()) {

                            //已关闭 & 已取消
                            if (oderDetailModel.getData().isPayStatusCancle()) {
                                dismissProgressHUD();
                                EventBus.getDefault().post(new OrderChangeEvent("2"));
                                skipToOrderDetail();
                            }

                            //待支付
                            if (oderDetailModel.getData().isPayStatusWait()) {
                                if (retryIndex == 1) {
                                    dismissProgressHUD();
                                    confirmOrderState(2);
                                } else {
                                    getOrderPayTime(true, false);
                                }
                            }

                            //已支付
                            if (oderDetailModel.getData().isPayStatusSuccess()) {
                                dismissProgressHUD();
                                mPaySuccessLayout.setVisibility(View.VISIBLE);
                                mPayingLayout.setVisibility(View.GONE);
                                EventBus.getDefault().post(new OrderChangeEvent("1"));
                                mCountDownView.onDestroy();
                            }
                        } else {
                            dismissProgressHUD();
                            Toasty.normal(OrderPayDetailAct.this, oderDetailModel.msg).show();
                        }
                    }
                });
            }
            else {
                ShopModuleApiProvider.getInstance().getNewOrderDetail(mOrderId, new Observer<NewOderDetailModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismissProgressHUD();
                        Toasty.normal(OrderPayDetailAct.this, "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(NewOderDetailModel oderDetailModel) {
                        if (oderDetailModel.isSuccess()) {
                            //已关闭 & 已取消
                            if (oderDetailModel.data.isOderColse()) {
                                dismissProgressHUD();
                                EventBus.getDefault().post(new OrderChangeEvent("2"));
                                skipToOrderDetail();
                            }

                            //待支付
                            if (oderDetailModel.data.canEdOderRemaksEdText()) {
                                if (retryIndex == 1) {
                                    dismissProgressHUD();
                                    confirmOrderState(2);
                                } else {
                                    getOrderPayTime(true, false);
                                }
                            }

                            //已支付
                            if (oderDetailModel.data.isShowPaySuccess()) {
                                dismissProgressHUD();
                                mPaySuccessLayout.setVisibility(View.VISIBLE);
                                mPayingLayout.setVisibility(View.GONE);
                                EventBus.getDefault().post(new OrderChangeEvent("1"));
                                mCountDownView.onDestroy();
                            }
                        } else {
                            Toasty.normal(OrderPayDetailAct.this, oderDetailModel.msg).show();
                        }
                    }
                });
            }
        }


    }

    private void skipToOrderDetail() {
        if(mFrom!=null){
            /**
             * 新增如果来源字充值直播时间则页面关闭回到我的费用页面
             */
        if(mFrom.equals(SKIP_FROM_RECHARGE_TIME)){
                //回到充值时间页面
                finish();
                return;
        }
            /**
             * 之前的逻辑，如果为"" 是页面跳转至NewOrderDetailAct页面
             */
        if (!SKIP_FROM_DETAIL.equals(mFrom)) {
            NewOrderDetailAct.start(OrderPayDetailAct.this, mOrderId, RouterConstants.ORDER_TYPE_DEFAULT);
        }
        finish();
        }
    }


    public void skipToRechargeRecode(){
        if(mFrom!=null){
            /**
             * 新增如果来源字充值直播时间则页面关闭回到我的费用页面
             */
            if(mFrom.equals(SKIP_FROM_RECHARGE_TIME)){
                //跳转至购买记录页面
                //支付成功返回会议服务管理购买记录页面
                ARouter.getInstance().build(RouterPathProvider.ServiceManager_PurchaseRecordAct).navigation();
                finish();
                return;
            }
            /**
             * 来源是充值购买记录列表页面
             */
            if(mFrom.equals(SKIP_FROM_RECHARGE_RECORD)){
                EventBus.getDefault().post(new OrderChangeEvent("1"));
                //结束当前页面
                finish();
                return;
            }

            /**
             * 之前的逻辑，如果为"" 是页面跳转至NewOrderDetailAct页面
             */
            if (!SKIP_FROM_DETAIL.equals(mFrom)) {
                NewOrderDetailAct.start(OrderPayDetailAct.this, mOrderId, RouterConstants.ORDER_TYPE_DEFAULT);
            }
            finish();
        }
    }


    @Override
    public void onBackPressed() {
        onLeftManagerImageClick();
    }

    @Override
    protected void onLeftManagerImageClick() {
        if (mPaySuccessLayout.getVisibility() == View.VISIBLE) {
            ShopActAtack.getActivityStack().popAllActivity();
            finish();
        } else {
            getOrderPayTime(true, true);
        }
    }

    @Override
    public void onFinish() {
        showProgressHUD(this, "", false);
        // mHandler.postDelayed(() -> queryOrderPayinfo(-1), 2000);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismissProgressHUD();
                Toasty.normal(OrderPayDetailAct.this, "订单已过期").show();
                EventBus.getDefault().post(new OrderChangeEvent("2"));
                ShopActAtack.getActivityStack().popAllActivity();
                finish();
            }
        }, 1000);
    }

    @Override
    public void onTick(long untilFinished) {
        mNum = untilFinished;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        confirmOrderState(1);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPublicStringEvent(PayEvent event) {
        if (event.isSuccess) {
            confirmOrderState(1);
        }
    }

    private void confirmOrderState(int currentRetryIndex) {
        if (currentRetryIndex == 1) {
            if (AppConfig.hasSkipToMini) {
                AppConfig.hasSkipToMini = false;
                showProgressHUD(this, "", false);
                mHandler.postDelayed(() -> queryOrderPayinfo(currentRetryIndex), 3000);
            }
        } else {
            showProgressHUD(this, "", false);
            mHandler.postDelayed(() -> queryOrderPayinfo(currentRetryIndex), 3000);
        }

    }

}