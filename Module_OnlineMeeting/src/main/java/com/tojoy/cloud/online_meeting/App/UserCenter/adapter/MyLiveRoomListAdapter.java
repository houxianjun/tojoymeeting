package com.tojoy.cloud.online_meeting.App.UserCenter.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mcxtzhang.swipemenulib.SwipeMenuLayout;
import com.netease.nim.uikit.business.chatroom.view.TJVideoPlayerAct;
import com.netease.nim.uikit.business.liveroom.JoinLiveRoomHelper;
import com.netease.nim.uikit.business.liveroom.JoinLiveRoomListener;
import com.tojoy.tjoybaselib.model.live.CheckLiveStatusResponse;
import com.tojoy.tjoybaselib.model.live.MyLiveRoomListResponse;
import com.tojoy.tjoybaselib.model.live.ProjectTagModel;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.cloud.online_meeting.App.Home.view.OnlineMeetingHomeTagsView;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.Services.Router.AppRouter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * Created by luuzhu on 2019/4/17.
 * 我的房间适配器
 */

public class MyLiveRoomListAdapter extends BaseRecyclerViewAdapter<MyLiveRoomListResponse.DataObjBean.ListBean> {
    private boolean mIsMyMeeting = true;
    private Activity mContext;

    public MyLiveRoomListAdapter(@NonNull Activity context, @NonNull List<MyLiveRoomListResponse.DataObjBean.ListBean> datas, boolean mIsMyMeeting) {
        super(context, datas);
        this.mIsMyMeeting = mIsMyMeeting;
        this.mContext = context;
    }

    @Override
    protected int getViewType(int position, @NonNull MyLiveRoomListResponse.DataObjBean.ListBean data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return mIsMyMeeting ? R.layout.item_my_room : R.layout.item_look_log;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return mIsMyMeeting ? new MyLiveRoomListVH(context, parent, getLayoutResId(viewType)) : new LookLogListVH(context, parent, getLayoutResId(viewType));
    }


    private class MyLiveRoomListVH extends BaseRecyclerViewHolder<MyLiveRoomListResponse.DataObjBean.ListBean> {

        TextView tvTime;
        ImageView ivIcon;
        TextView tvTitle;
        LinearLayout llTags;
        TextView tvWatchTimes;
        View line;

        MyLiveRoomListVH(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            tvTime = itemView.findViewById(R.id.tvTime);
            ivIcon = itemView.findViewById(R.id.ivIcon);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            llTags = itemView.findViewById(R.id.llTags);
            tvWatchTimes = itemView.findViewById(R.id.tvWatchTimes);
            line = itemView.findViewById(R.id.line);
        }

        @Override
        public void onBindData(MyLiveRoomListResponse.DataObjBean.ListBean data, int position) {

            if (position == datas.size() - 1) {
                line.setVisibility(View.GONE);
            } else {
                line.setVisibility(View.VISIBLE);
            }

            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            String dateStr = format.format(new Date(data.startDate));
            tvTime.setText(dateStr);
            tvTitle.setText(data.title);

            String tagNames = data.tagNames;
            if (!TextUtils.isEmpty(tagNames)) {
                llTags.setVisibility(View.GONE);
                String[] split = tagNames.split(",");
                ArrayList<ProjectTagModel> tags = new ArrayList<>();
                if (split.length > 0) {
                    for (int i = 0; i < split.length; i++) {
                        ProjectTagModel model = new ProjectTagModel();
                        model.projectTagName = split[i];
                        tags.add(model);
                    }
                    llTags.removeAllViews();
                    OnlineMeetingHomeTagsView tagsView = new OnlineMeetingHomeTagsView(context, tags, llTags);
                    llTags.addView(tagsView.getView());
                } else {
                    llTags.setVisibility(View.GONE);
                }
            } else {
                llTags.setVisibility(View.GONE);
            }

            tvWatchTimes.setText("观看次数：" + data.liveWatchTotal);

            if (TextUtils.isEmpty(data.coverOne)) {
                ImageLoaderManager.INSTANCE.loadImage(context, ivIcon, data.coverTwo, R.drawable.icon_online_meeting_home_grid_default, 0);
            } else {
                ImageLoaderManager.INSTANCE.loadImage(context, ivIcon, data.coverOne, R.drawable.icon_online_meeting_home_grid_default, 0);
            }

            //进入直播间
            itemView.findViewById(R.id.rlItem).setOnClickListener(view -> {
                if (FastClickAvoidUtil.isDoubleClick()) {
                    return;
                }

//                if (!mIsMyMeeting) {
                    JoinLiveRoomHelper.goToLiveRoom(mContext, data.roomLiveId, data.title,  new JoinLiveRoomListener() {
                        @Override
                        public void onJoinError(String error, int errorCode) {

                        }

                        @Override
                        public void onJoinSuccess() {

                        }
                    });
//                } else {
//                    //回放的情况
//                    if ("4".equals(data.status)) {
//                        ((UI) context).showProgressHUD(context, "处理中...");
//                        OMAppApiProvider.getInstance().getLiveRoomStatus(data.roomLiveId, new Observer<CheckLiveStatusResponse>() {
//                            @Override
//                            public void onCompleted() {
//                                ((UI) context).dismissProgressHUD();
//
//                            }
//
//                            @Override
//                            public void onError(Throwable e) {
//                                Toasty.normal(getContext(), "网络异常，请稍后再试").show();
//                            }
//
//                            @Override
//                            public void onNext(CheckLiveStatusResponse response) {
//                                if (response.isSuccess()) {
//                                    if ("4".equals(response.data.status)) {
//                                        TJVideoPlayerAct.start(context, data.videoPullUrl, data.roomLiveId, data.companyCode);
//                                    }
//                                } else {
//                                    Toasty.normal(getContext(), response.msg).show();
//                                }
//                            }
//                        });
//
//                        if (BaseUserInfoCache.getUserId(context).equals(data.userId)) {
//                            LiveRoomInfoProvider.getInstance().myUserType = "0";
//                        }
//                    } else {
//                        JoinLiveRoomHelper.goToLiveRoom(mContext, data.roomLiveId, data.title, new JoinLiveRoomListener() {
//                            @Override
//                            public void onJoinError(String error, int errorCode) {
//
//                            }
//
//                            @Override
//                            public void onJoinSuccess() {
//
//                            }
//                        });
//                    }
//                }
            });


            itemView.findViewById(R.id.rlDelete).setOnClickListener(view -> {
                if (mOnItemDelete != null) {
                    mOnItemDelete.onItemDeleteClicked(position, data.roomLiveId);
                    SwipeMenuLayout swipeMenuLayout = itemView.findViewById(R.id.deleteView);
                    swipeMenuLayout.quickClose();
                }
            });
        }
    }

    /**
     * 删除item 回调 ---------------------------------------------
     */
    public void setmOnItemDelete(OnItemDelete onItemDelete) {
        this.mOnItemDelete = onItemDelete;
    }

    private OnItemDelete mOnItemDelete;

    public interface OnItemDelete {
        void onItemDeleteClicked(int position, String liveId);
    }

    private class LookLogListVH extends BaseRecyclerViewHolder<MyLiveRoomListResponse.DataObjBean.ListBean> {

        TextView tvTime;
        ImageView ivIcon;
        TextView tvTitle;
        LinearLayout llTags;
        TextView tvWatchTimes;
        TextView tvliveState;
        TextView tvMeetingId;

        LookLogListVH(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            tvTime = itemView.findViewById(R.id.tvTime);
            ivIcon = itemView.findViewById(R.id.ivIcon);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            llTags = itemView.findViewById(R.id.llTags);
            tvWatchTimes = itemView.findViewById(R.id.tvWatchTimes);
            tvliveState = itemView.findViewById(R.id.tvliveState);
            tvMeetingId = itemView.findViewById(R.id.tvMeetingId);
        }

        @Override
        public void onBindData(MyLiveRoomListResponse.DataObjBean.ListBean data, int position) {

            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            String dateStr = format.format(new Date(data.startDate));
            tvTime.setText(dateStr);
            tvTitle.setText(data.title);

            String tagNames = data.tagNames;
            if (!TextUtils.isEmpty(tagNames)) {
                llTags.setVisibility(View.GONE);
                String[] split = tagNames.split(",");
                ArrayList<ProjectTagModel> tags = new ArrayList<>();
                if (split.length > 0) {
                    for (int i = 0; i < split.length; i++) {
                        ProjectTagModel model = new ProjectTagModel();
                        model.projectTagName = split[i];
                        tags.add(model);
                    }
                    llTags.removeAllViews();
                    OnlineMeetingHomeTagsView tagsView = new OnlineMeetingHomeTagsView(context, tags, llTags);
                    llTags.addView(tagsView.getView());
                } else {
                    llTags.setVisibility(View.GONE);
                }
            } else {
                llTags.setVisibility(View.GONE);
            }

            tvWatchTimes.setText("观看次数：" + data.liveWatchTotal);
            tvMeetingId.setText("会议号：" + data.roomCode);
            if ("4".equals(data.status)) {
                tvliveState.setText("已结束");
            } else {
                tvliveState.setText("进行中");
            }

            ImageLoaderManager.INSTANCE.loadImage(context, ivIcon, data.coverOne, R.drawable.icon_online_meeting_home_grid_default, 0);

            //进入直播间
            itemView.findViewById(R.id.rlItem).setOnClickListener(view -> {
                if (FastClickAvoidUtil.isDoubleClick()) {
                    return;
                }
                AppRouter.joinDirect(context, data.roomLiveId);
            });

            itemView.findViewById(R.id.rlDelete).setOnClickListener(view -> {
                if (mOnItemDelete != null) {
                    mOnItemDelete.onItemDeleteClicked(position, data.roomLiveId);
                    SwipeMenuLayout swipeMenuLayout = itemView.findViewById(R.id.deleteView);
                    swipeMenuLayout.quickClose();
                }
            });

        }
    }

}
