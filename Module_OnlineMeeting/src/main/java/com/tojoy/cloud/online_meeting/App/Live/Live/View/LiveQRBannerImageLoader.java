package com.tojoy.cloud.online_meeting.App.Live.Live.View;

import android.content.Context;
import android.widget.ImageView;

import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.ui.Banner.loader.ImageLoader;
import com.tojoy.cloud.online_meeting.R;


public class LiveQRBannerImageLoader extends ImageLoader {
    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {
        ImageLoaderManager.INSTANCE.loadWantonlyRadiu(context,
                imageView,
                OSSConfig.getRemoveStylePath((String) path),
                R.drawable.popup_default_qr_small,
                R.drawable.popup_default_qr_small,
                0,
                true, true, true, true);
    }
}
