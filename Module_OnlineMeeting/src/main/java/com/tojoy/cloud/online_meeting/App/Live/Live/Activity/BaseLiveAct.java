package com.tojoy.cloud.online_meeting.App.Live.Live.Activity;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishDistributionAttachment;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishQRAttachment;
import com.netease.nim.uikit.business.chatroom.helper.ChatRoomHelper;
import com.netease.nim.uikit.business.chatroom.module.LiveRoomMsgListPanel;
import com.netease.nim.uikit.business.chatroom.viewholder.ChatRoomMsgViewHolderText;
import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomHelper;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tencent.rtmp.TXLiveConstants;
import com.tencent.rtmp.ui.TXCloudVideoView;
import com.tencent.trtc.TRTCCloud;
import com.tencent.trtc.TRTCCloudDef;
import com.tojoy.common.Services.Distribution.DistrbutionHelper;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.live.AgentLiveModel;
import com.tojoy.tjoybaselib.model.live.DoumentIdStatusResponse;
import com.tojoy.tjoybaselib.model.live.DoumentVideoResponse;
import com.tojoy.tjoybaselib.model.live.DoumentVideoSendMsg;
import com.tojoy.tjoybaselib.model.live.ExploreMemeberStatusResponse;
import com.tojoy.tjoybaselib.model.live.LiveSignResponse;
import com.tojoy.tjoybaselib.model.live.OnlineFrameResponse;
import com.tojoy.tjoybaselib.model.live.PublishQRListResponse;
import com.tojoy.tjoybaselib.model.live.SelectOutLiveModel;
import com.tojoy.tjoybaselib.model.live.SelectOutliveStatusResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.NetChangeReceiver.NetChangeEvent;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.services.log.BaseLiveLog;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigCacheManager;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigConstantKey;
import com.tojoy.tjoybaselib.ui.Banner.Banner;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.ActivityStack;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.KeyboardControlUtil;
import com.tojoy.cloud.online_meeting.App.Config.LiveRoomConfig;
import com.tojoy.cloud.online_meeting.App.Controller.LRDialogController;
import com.tojoy.cloud.online_meeting.App.Controller.LRUEController;
import com.tojoy.common.LiveRoom.MeetingExtensionPopView;
import com.tojoy.cloud.online_meeting.App.Live.ChatRoom.LiveCRMessageFragment;
import com.tojoy.cloud.online_meeting.App.Live.CollectOuterLive.NewOutlivePop;
import com.tojoy.cloud.online_meeting.App.Live.CollectOuterLive.OuterLiveListAct;
import com.tojoy.cloud.online_meeting.App.Live.ExporeOnline.ApplyOnlineTipView;
import com.tojoy.cloud.online_meeting.App.Live.ExporeOnline.ExporeApiManager;
import com.tojoy.cloud.online_meeting.App.Live.ExporeOnline.GuestListForInveteAct;
import com.netease.nim.uikit.business.liveroom.FileShowCallback;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.InnerOutCallReceiver;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.LiveCustomMessageProvider;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.OnlineMembersHelper;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.TRTCCloudListenerImpl;
import com.tojoy.cloud.online_meeting.App.Live.Live.Model.HostVideoEvent;
import com.netease.nim.uikit.business.liveroom.LiveCustomMessage;
import com.tojoy.cloud.online_meeting.App.Live.Live.Model.OnlineMember;
import com.tojoy.cloud.online_meeting.App.Live.Live.Model.PhoneStateEvent;
import com.tojoy.cloud.online_meeting.App.Live.Live.View.BottomOperationView;
import com.tojoy.cloud.online_meeting.App.Live.Live.View.FloatTipView;
import com.tojoy.cloud.online_meeting.App.Live.Live.View.LiveNoticeView;
import com.tojoy.cloud.online_meeting.App.Live.Live.View.LiveOperationPop;
import com.tojoy.cloud.online_meeting.App.Live.Live.View.LiveQRBannerImageLoader;
import com.tojoy.cloud.online_meeting.App.Live.Live.View.MarqueeText;
import com.tojoy.cloud.online_meeting.App.Live.Live.View.TRTCVideoViewLayout;
import com.tojoy.cloud.online_meeting.App.Live.Whiteboard.WBEventCallback;
import com.tojoy.cloud.online_meeting.App.Live.Whiteboard.WBManager;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;
import com.tojoy.cloud.online_meeting.App.UserCenter.activity.UserHomePageAct;
import com.tojoy.cloud.online_meeting.App.UserCenter.callback.LiveCRFragmentCallback;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.App.LiveFloatAct;
import com.tojoy.common.App.TJBaseApp;
import com.tojoy.common.Services.EventBus.EndLiveEventBus;
import com.tojoy.common.Services.EventBus.OuterLiveEvent;
import com.tojoy.common.Services.EventBus.PublicStringEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.tencent.trtc.TRTCCloudDef.TRTCRoleAnchor;
import static com.tencent.trtc.TRTCCloudDef.TRTCRoleAudience;
import static com.tojoy.cloud.online_meeting.App.Live.Whiteboard.WBManager.DRAWPAD_WB_ID;

public class BaseLiveAct extends LiveFloatAct implements
        WBEventCallback,
        OperationCallback,
        FileShowCallback,
        OnlineMembersHelper.OnlineMemberChangeCallback, LiveCRFragmentCallback,
        Application.ActivityLifecycleCallbacks {

    public static boolean isKeyboardShowing;

    public int mAppScence = TRTCCloudDef.TRTC_APP_SCENE_LIVE;

    public TRTCVideoViewLayout mTRTCVideoViewLayout;
    /** TRTC SDK 视频通话房间进入所必须的参数 */
    public TRTCCloudDef.TRTCParams mTrtcParams;
    /** TRTC SDK 实例对象 */
    public TRTCCloud mTrtcCloud;
    /** TRTC SDK 回调监听 */
    protected TRTCCloudListenerImpl mTrtcListener;
    /** 观众首次进入，主播的视频是否初始化好 */
    public boolean isHostVideoAvailable = false;
    /** 底部操作栏目 */
    public BottomOperationView mBottomOperationView;
    /** 同台互动窗 */
    public LiveOperationPop mLiveOperationPop;
    /** 同台直播申请提示 */
    protected ApplyOnlineTipView mApplyOnlineTipView;
    /** 白板管理 */
    protected WBManager mWhiteboardManager;
    /** 聊天室 */
    public FrameLayout mChatMsgContainer;
    protected LiveCRMessageFragment mLiveCRMessageFragment;
    /** 项目推荐,用户端收到项目推荐时显示 */
    protected View mProjectLayout;
    /** 顶部公告显示栏 */
    private RelativeLayout mNoticeContainer;
    private LiveNoticeView mNoticeView;
    /** 进入直播间提示 */
    private TextView mTvJoinLiveTip;
    /** 是否展示进场UE（文档演示/白板时的控制） */
    private boolean mIsCanShowJoinTipUE = true;
    boolean isRecoverFromSystemFinish = false;

    private FloatTipView mFloatTipView;

    /**
     * 右下角接收活动（二维码）的视图
     */
    private View mQRListSmallView;
    /**
     * 分销按钮
     */
    private View mDistributionView;

    // 动态监听去电的广播接收器
    private InnerOutCallReceiver mInnerOutCallReceiver;

    @Override
    protected void onCreate(@android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBar();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.trtc_main_live_layout);
        EventBus.getDefault().register(this);
        setSwipeBackEnable(false);
        initCommonUI();

        //系统回收后恢复处理
        if (null != savedInstanceState) {
            isRecoverFromSystemFinish = true;
            LiveRoomInfoProvider.getInstance().onRecoverFromInstanceState(savedInstanceState);
        }

        //增加日志
        Intent pushIntent = getIntent();
        String pushJson = pushIntent.getStringExtra("liveJson");
        LiveLogTag.initLiveLog(LiveRoomInfoProvider.getInstance().liveTitle, LiveRoomInfoProvider.getInstance().liveId);
        LogUtil.i(LiveLogTag.IOLiveTag, "onCreate");
        LogUtil.i(LiveLogTag.IOLiveTag, "pushJson: " + pushJson);

        if (TextUtils.isEmpty(pushJson)) {
            LiveRoomInfoProvider.getInstance().isFromPush = false;
            loginTXLiveRoom();
        } else {
            LiveRoomInfoProvider.getInstance().isFromPush = true;
            doPush(pushJson);
        }

        //初始化聊天室
        initChatRoom();

        initPhoneReceiver();
    }

    /**
     * 初始化对系统电话的监听
     */
    private void initPhoneReceiver() {
        // 动态注册广播接收器监听去电信息
        mInnerOutCallReceiver = new InnerOutCallReceiver();
        // 手机拨打电话时会发送：android.intent.action.NEW_OUTGOING_CALL的广播
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_NEW_OUTGOING_CALL);
        registerReceiver(mInnerOutCallReceiver, intentFilter);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        LiveRoomInfoProvider.getInstance().onSaveInstanceState(savedInstanceState);
        super.onSaveInstanceState(savedInstanceState);
    }

    private void initCommonUI() {
        findViewById(R.id.rlv_close).setOnClickListener(view -> {
            if (findViewById(R.id.rlv_title).getVisibility() == VISIBLE) {
                onBackPressed();
            }
        });

        findViewById(R.id.iv_fileshow_back).setOnClickListener(v -> {
            if (FastClickAvoidUtil.isFastDoubleClick(R.id.iv_fileshow_back)) {
                return;
            }
            //主播、助手: 正在显示文档、结束
            if (mWhiteboardManager != null) {
                mWhiteboardManager.onBackPressed();
            }
        });
        findViewById(R.id.ivStartLive).setOnClickListener(v -> {
            if (FastClickAvoidUtil.isFastDoubleClick(R.id.ivStartLive)) {
                return;
            }
            LiveRoomInfoProvider.getInstance().isApplying = true;
            // 开播按钮
            ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_START_LIVE_BROADCAST_APPLY)
                    .withString("source", "menu")
                    .navigation();
        });

        if (!TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().isApply)
                && "1".equals(LiveRoomInfoProvider.getInstance().isApply)
                && !LiveRoomInfoProvider.getInstance().isHost()) {
            // 只有有开播权限的才显示开播按钮（除去主播）
            findViewById(R.id.ivStartLive).setVisibility(VISIBLE);
        } else {
            findViewById(R.id.ivStartLive).setVisibility(GONE);
        }
        mTvJoinLiveTip = (TextView) findViewById(R.id.tvJoinLiveTip);
        mFloatTipView = (FloatTipView) findViewById(R.id.floattip);
        mQRListSmallView = findViewById(R.id.rlv_small_banner_qr);
        mDistributionView = findViewById(R.id.iv_distribution);
        initQRBanner();
        LiveRoomHelper.getInstance().publishQRListSmallView = mQRListSmallView;
        // 初始化是否为可分销会议
        initDistributionStatus();
    }

    private void initQRBanner() {
        Banner mBanner = (Banner) findViewById(R.id.iv_qr_banner);
        RelativeLayout mIndicatorContainer = (RelativeLayout) findViewById(R.id.indicator_qr_container);
        TextView mQrIndicatorTv = (TextView) findViewById(R.id.tv_qr_indicator);
        if(LiveRoomHelper.getInstance().receivedQrSmallList.size() > 0){
            if (LiveRoomInfoProvider.getInstance().isHost() || LiveRoomInfoProvider.getInstance().isHostHelper()) {
                if (LiveRoomInfoProvider.getInstance().isShowFile) {
                    mQRListSmallView.setVisibility(GONE);
                } else {
                    mQRListSmallView.setVisibility(VISIBLE);
                }
            } else {
                mQRListSmallView.setVisibility(VISIBLE);
            }

            // 设置banner
            mBanner.setDelayTime(5000);
            mBanner.setIndicatorHeight(this, 0);
            //设置图片加载器
            mBanner.setImageLoader(new LiveQRBannerImageLoader());

            List<String> mBannerImageList = new ArrayList<>();
            for (int i = 0; i < LiveRoomHelper.getInstance().receivedQrSmallList.size(); i++) {
                mBannerImageList.add(LiveRoomHelper.getInstance().receivedQrSmallList.get(i).getPicSmall());
            }

            //设置图片集合
            mBanner.setImages(mBannerImageList);
            //banner设置方法全部调用完毕时最后调用
            mBanner.start();

            if (mBannerImageList.size() > 1) {
                mIndicatorContainer.setVisibility(View.VISIBLE);
            } else {
                mIndicatorContainer.setVisibility(GONE);
            }
            mQrIndicatorTv.setText("1/" + mBannerImageList.size());

            //页面改变的导航
            mBanner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {
                }

                @Override
                public void onPageSelected(int i) {
                    mQrIndicatorTv.setText((i+1) + "/" + mBannerImageList.size());
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });

            mBanner.setOnBannerListener(position -> {
                if (FastClickAvoidUtil.isFastDoubleClick(mBanner.getId())) {
                    return;
                }
                LRDialogController.getInstance().showQRCenterList(this,LiveRoomHelper.getInstance().receivedQrSmallList.get(position));
            });
        } else {
            mQRListSmallView.setVisibility(GONE);
        }

    }

    /**
     * 设置直播间是否显示可分销的按钮
     */
    private void initDistributionStatus(){
        if (DistrbutionHelper.isShowDistribution(this,LiveRoomInfoProvider.getInstance().ifDistribution,LiveRoomInfoProvider.getInstance().otherCompanyCode)) {
            mDistributionView.setVisibility(VISIBLE);
        } else {
            mDistributionView.setVisibility(GONE);
        }
        mDistributionView.setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()){
                if (DistrbutionHelper.isNeedJumpPop(this
                        ,LiveRoomInfoProvider.getInstance().level1CommissionEmployee
                        ,LiveRoomInfoProvider.getInstance().level2CommissionEmployee
                        ,LiveRoomInfoProvider.getInstance().otherCompanyCode)) {
                    MeetingExtensionPopView popView = new MeetingExtensionPopView(this);
                    popView.setMeetingData(LiveRoomInfoProvider.getInstance().roomId
                            ,LiveRoomInfoProvider.getInstance().liveId
                            ,LiveRoomInfoProvider.getInstance().liveTitle
                            ,LiveRoomInfoProvider.getInstance().coverOne
                            ,DistrbutionHelper.getDistributionMoney(this
                                    ,LiveRoomInfoProvider.getInstance().level1Commission
                                    ,LiveRoomInfoProvider.getInstance().level2Commission
                                    ,LiveRoomInfoProvider.getInstance().level1CommissionEmployee
                                    ,LiveRoomInfoProvider.getInstance().level2CommissionEmployee
                                    ,LiveRoomInfoProvider.getInstance().otherCompanyCode)
                            ,LiveRoomInfoProvider.getInstance().minPerson
                            ,LiveRoomInfoProvider.getInstance().minViewTime);
                    popView.show();
                } else {
                    // 判断二维码内容是否为空，为空生成海报没有意义
                    if (!TextUtils.isEmpty(BaseUserInfoCache.getUserInviteUrl(this))) {
                        ARouter.getInstance()
                                .build(RouterPathProvider.ONLINE_MEETING_EXTEN_SION_POSTER)
                                .withInt("fromType", 1)
                                .withString("mLiveTitle", LiveRoomInfoProvider.getInstance().liveTitle)
                                .withString("mLiveId", LiveRoomInfoProvider.getInstance().liveId)
                                .withString("mLiveCoverUrl", LiveRoomInfoProvider.getInstance().coverOne)
                                .withString("mRoomId", LiveRoomInfoProvider.getInstance().roomId)
                                .navigation();

                    } else {
                        Toasty.normal(this, this.getResources().getString(R.string.extension_qr_error)).show();
                    }
                }
            }
        });
    }

    public int[] getQRSmallBannerLocation() {
        //左上角坐标    (left,top)
        int top = mQRListSmallView.getTop();
        int left = mQRListSmallView.getLeft();
        //右下角坐标
        int right = mQRListSmallView.getRight();
        int bottom = mQRListSmallView.getBottom();

        //View的 宽
        int viewWidth = right - left;
        //View的 高
        int viewHeight = bottom - top;

        //中心点的Y坐标
        int centerY = top+viewHeight/2;
        //中心点的X坐标
        int centerX = left+viewWidth/2;

        int[] location = {centerX,centerY};
        return location;


//        int[] location = new int[2];
//        mQRListSmallView.getLocationOnScreen(location);
//        return location;
    }

    protected void doPush(String pushJson) {
    }

    /**
     * 登录直播间
     */
    public void loginTXLiveRoom() {
        OMAppApiProvider.getInstance().getLiveUserSign(LiveRoomInfoProvider.getInstance().liveId,new rx.Observer<LiveSignResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(BaseLiveAct.this, "网络错误，请检查网络").show();
                finish();
            }

            @Override
            public void onNext(LiveSignResponse response) {
                if (response.isSuccess()) {
                    try {
                        LogUtil.i(LiveLogTag.LRApiRequestTag, "tencent/login--success:" + response.data.loginParams.userSign);
                        LiveRoomInfoProvider.getInstance().userSign = response.data.loginParams.userSign;
                        initVideoViewLayout();
                        initTRTCSDKAndEnter();
                    } catch (Exception e) {

                    }
                } else {
                    Toasty.normal(BaseLiveAct.this, response.msg).show();
                    finish();
                }

            }
        });
    }

    private void initTRTCSDKAndEnter() {
        initTRTCSDK(LiveRoomInfoProvider.getInstance().userSign);
        if (mTrtcCloud != null) {
            mTrtcCloud.setBeautyStyle(TRTCCloudDef.TRTC_BEAUTY_STYLE_SMOOTH, 5, 5, 5);
            mTrtcCloud.setGSensorMode(TRTCCloudDef.TRTC_GSENSOR_MODE_DISABLE);
        }
        enterRoom();
    }

    private void initTRTCSDK(String sign) {
        mTrtcParams = new TRTCCloudDef.TRTCParams(
                LiveRoomConfig.getTXSDKAppID(),
                BaseUserInfoCache.getUserId(this),
                sign,
                Integer.parseInt(LiveRoomInfoProvider.getInstance().liveId),
                "",
                "");

        mTrtcCloud = TRTCCloud.sharedInstance(this);
        mTrtcListener = new TRTCCloudListenerImpl(this);
        mTrtcCloud.setListener(mTrtcListener);

        setVideoParams();

        // 统一修改码率、分辨率
        TRTCCloudDef.TRTCNetworkQosParam qosParam = new TRTCCloudDef.TRTCNetworkQosParam();
        qosParam.controlMode = TRTCCloudDef.VIDEO_QOS_CONTROL_SERVER;
        qosParam.preference = TRTCCloudDef.TRTC_VIDEO_QOS_PREFERENCE_CLEAR;
        if (mTrtcCloud != null) {
            mTrtcCloud.setNetworkQosParam(qosParam);
        }
    }

    /**
     * 设置推流分辨率及码率
     */
    public void setVideoParams() {
        // 统一修改码率、分辨率 ,本期不上注释掉
        TRTCCloudDef.TRTCVideoEncParam encParam = new TRTCCloudDef.TRTCVideoEncParam();
        encParam.videoResolution = TRTCCloudDef.TRTC_VIDEO_RESOLUTION_960_540;
        encParam.videoBitrate = 750;
        encParam.videoFps = 15;
        encParam.videoResolutionMode = TRTCCloudDef.TRTC_VIDEO_RESOLUTION_MODE_PORTRAIT;
        if (mTrtcCloud != null) {
            mTrtcCloud.setVideoEncoderParam(encParam);
        }
    }

    @Override
    public void changeVideoQst() {
        // 动态调整码率
//        setVideoParams();
    }

    public void destoryTRCC() {
        LogUtil.i(LiveLogTag.IOLiveTag, "destoryTRCC");
        mTrtcCloud = null;
    }

    protected void initVideoViewLayout() {
        mTRTCVideoViewLayout = (TRTCVideoViewLayout) findViewById(R.id.ll_mainview);
        mTRTCVideoViewLayout.setOperationCallback(this);
    }

    /**
     * 进出房间
     */
    //是否在退出房间中
    public boolean isExitingRoom = false;

    protected void enterRoom() {

    }

    public void onEnterRoomSuccess() {
        LogUtil.i(LiveLogTag.IOLiveTag, "onEnterRoomSuccess");

        if (mTrtcCloud == null) {
            initTRTCSDK(LiveRoomInfoProvider.getInstance().userSign);
        }

        OnlineMembersHelper.getInstance().isHostVideoOn = true;
        OnlineMembersHelper.getInstance().isHostAudioOn = true;
        OnlineMembersHelper.getInstance().mOnlineMembers.clear();

        mTrtcCloud.setAudioRoute(TRTCCloudDef.TRTC_AUDIO_ROUTE_SPEAKER);
        mTrtcCloud.setMicVolumeOnMixing(150);
        mTrtcCloud.setVideoEncoderMirror(LiveRoomInfoProvider.getInstance().isFrontCamera);

        initEnterRoomSuccessUI();

        LiveRoomInfoProvider.getInstance().isInitWhiteBoardSuccess = false;
        initWBManager(-2, false);

        // 初始化后检测当前房间状态：是否在引流、是否在白板中
        initHostOperation();

        //如果是助手的话刷新当前数据
        if (LiveRoomInfoProvider.getInstance().isHostHelper()) {
            refreshHelperInfo();
        }

        //获取项目推荐信息（和身份变更时共用）
        if (!LiveRoomInfoProvider.getInstance().isHost()) {
            refreshDialogs();
        }

        //非主播检测主播视频流是否可见
        mHandler.postDelayed(() -> {
            if (!isHostVideoAvailable
                    && !LiveRoomInfoProvider.getInstance().isHost()
                    && !LiveRoomInfoProvider.getInstance().isOfficalLive()
                    && !LiveRoomInfoProvider.getInstance().hasTemplateLeave) {

                LogUtil.i(LiveLogTag.IOLiveTag, "checkNoHostVideo");

                mTRTCVideoViewLayout.updateVideoStatus(LiveRoomInfoProvider.getInstance().hostUserId, false);
            }
        }, 1500);


        //官方大会处理
        if (LiveRoomInfoProvider.getInstance().isOfficalLive()) {
            mTRTCVideoViewLayout.startMeeting();
        }

        //是否在暂离状态
        if (LiveRoomInfoProvider.getInstance().isTemplateLeaveWhenInit()) {
            LiveRoomInfoProvider.getInstance().hasTemplateLeave = true;
            refreshTempLiveTip(LiveRoomInfoProvider.getInstance().templateLeaveTip, "", true);
        }

        //获取当前申请人数
        if (!LiveRoomInfoProvider.getInstance().isGuest()) {
            mApplyOnlineTipView.getCurrentApplyExploreCount();
        }

        TJBaseApp.getInstance().registerActivityLifecycleCallbacks(this);

        // 初始化默认数据
        LiveRoomInfoProvider.getInstance().clearLiveRoomProviderData();

        initNotice();

        //关闭其他App的声音
        controlOtherAppAudio();

        //注册耳机事件
        registerHeadsetPlugReceiver();

        if (isRecoverFromSystemFinish && LiveRoomInfoProvider.getInstance().isHost() && !LiveRoomInfoProvider.getInstance().isOfficalLive()) {
            mTrtcListener.checkliveStatus();
        }

        if (mTrtcCloud != null) {
            mTrtcCloud.callExperimentalAPI("{\"api\":\"enableAudioANS\",\"params\":{\"enable\":1,\"level\": 30}}");

            if (LiveRoomInfoProvider.getInstance().isHost()) {
                LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "muteLocalVideo--onEnterRoomSuccess:" + false);
                mTrtcCloud.muteLocalVideo(false);
            }
        }
    }

    protected void initEnterRoomSuccessUI() {
        // 关闭、退出按钮显示
        findViewById(R.id.rlv_close).setVisibility(View.VISIBLE);
        // 预览界面（背景深蓝色鸽子图）隐藏
        findViewById(R.id.rlv_preview_layout).setVisibility(GONE);
        // 顶部、底部标题渐变色背景显示
        findViewById(R.id.view_graient).setVisibility(VISIBLE);
        findViewById(R.id.view_graient_down).setVisibility(VISIBLE);
        // 设置直播间标题
        ((MarqueeText) findViewById(R.id.tvTitle)).setText(LiveRoomInfoProvider.getInstance().liveTitle);

        // 直播间id设置
        findViewById(R.id.tvLiveIDGuest).setVisibility(VISIBLE);
        ((TextView) findViewById(R.id.tvLiveIDGuest)).setText("ID" + LiveRoomInfoProvider.getInstance().roomCode);

        //同台直播申请提示
        if (!LiveRoomInfoProvider.getInstance().isGuest()) {
            mApplyOnlineTipView = new ApplyOnlineTipView(this, this);
            ((RelativeLayout) findViewById(R.id.rlv_apply_info)).addView(mApplyOnlineTipView.getView());
        }

        //底部操作栏目
        if (mBottomOperationView == null) {
            mBottomOperationView = new BottomOperationView(this, this, (TextView) findViewById(R.id.tvDuration));
            ((RelativeLayout) (findViewById(R.id.rlBottomBtn))).addView(mBottomOperationView.getView());
        }
        LRUEController.getInstance().smallMsgListUE(this, mChatMsgContainer, mBottomOperationView);

        //初始化操作弹窗
        mLiveOperationPop = new LiveOperationPop(this, this);
        controlCaremaVisible();

    }

    boolean isPausing = false;

    @Override
    public void checkShowTip1(boolean needCheckOnLineMember) {
        if (mFloatTipView == null || mTRTCVideoViewLayout == null) {
            return;
        }
        //主播
        if (LiveRoomInfoProvider.getInstance().isHost()
                && (needCheckOnLineMember ? OnlineMembersHelper.getInstance().hasOnlineMember() : true)
                && !BaseUserInfoCache.getLiveTip1ForHost(this)
                && !LiveRoomInfoProvider.getInstance().isShowFile
                && !isPausing
        ) {
            BaseUserInfoCache.saveLiveTip1ForHost(this);
            mFloatTipView.initView(this);
            mFloatTipView.showTip1(!mTRTCVideoViewLayout.hasScrollToTop);
        }

        //助手
        if (LiveRoomInfoProvider.getInstance().isHostHelper()
                && (needCheckOnLineMember ? OnlineMembersHelper.getInstance().hasOnlineMember() : true)
                && !BaseUserInfoCache.getLiveTip1ForHelper(this)
                && !LiveRoomInfoProvider.getInstance().isShowFile
                && !isPausing
        ) {
            BaseUserInfoCache.saveLiveTip1ForHelper(this);
            mFloatTipView.initView(this);
            mFloatTipView.showTip1(!mTRTCVideoViewLayout.hasScrollToTop);
        }
    }

    @Override
    public void checkShowTip2() {
        if (!BaseUserInfoCache.getLiveTip2(this)
        ) {
            BaseUserInfoCache.saveLiveTip2(this);
            mHandler.postDelayed(() -> {
                mFloatTipView.initView(BaseLiveAct.this);
                mFloatTipView.showTip2(!mTRTCVideoViewLayout.hasScrollToTop, OnlineMembersHelper.getInstance().mOnlineMembers.size());
            }, 1000);
        }
    }

    @Override
    public void hideTip() {
        mFloatTipView.hideTip();
    }

    //-1:退出直播间不跳转到结束页面
    //1：主播结束直播
    //2：直播结束，观众退出
    //3：观众被提出
    @Override
    public void exitRoom(int status) {
        LogUtil.i(LiveLogTag.IOLiveTag, "exitRoom" + status);

        //5.22大会主播结束直播的时候 需要调用endLive
        boolean isNeedEndLiveMeeting = status == 1 && LiveRoomInfoProvider.getInstance().isOfficalLive();

        try {
            showProgressHUD(this, "处理中");
        } catch (Exception e) {

        }

        isExitingRoom = true;

        LiveRoomIOHelper.leaveLiveRoom(this, new IOLiveRoomListener() {
            @Override
            public void onIOError(String error, int errorCode) {
                onLeaveRoomSuccess(status);
            }

            @Override
            public void onIOSuccess() {
                onLeaveRoomSuccess(status);
            }
        }, isNeedEndLiveMeeting);

    }

    private void onLeaveRoomSuccess(int status) {

        if (status == 1) {
            LiveRoomInfoProvider.getInstance().isHostEndLive();
        }

        try {
            dismissProgressHUD();
        } catch (Exception e) {

        }

        if (mWhiteboardManager != null) {
            mWhiteboardManager.finishLive();
        }

        String duration = mBottomOperationView == null ? "00:00:00" : mBottomOperationView.getLiveDuration();

        if (status > 0) {
            // 如果在引流列表页面也直接结束
            EventBus.getDefault().post(new EndLiveEventBus(LiveRoomInfoProvider.getInstance().imRoomId, true));
            //如果用户没有停留在开会申请页，就去直播结束页
            if (!LiveRoomInfoProvider.getInstance().isApplying) {
                UserHomePageAct.startEndLive(BaseLiveAct.this,
                        String.valueOf(status), duration);
            }
        }

        if (mTrtcCloud != null) {
            mTrtcCloud.exitRoom();
        }
        finish();

        LogUtil.i(LiveLogTag.IOLiveTag, "onLeaveRoomSuccess");
    }

    @Override
    public void onKickedOutCallback() {
        LogUtil.i(LiveLogTag.IOLiveTag, "onKickedOutCallback");
        if (isExitingRoom) {
            return;
        }
        exitRoom(3);
    }

    @Override
    public void onEndingLive() {
        if (mTrtcCloud != null) {
            if (isExitingRoom) {
                return;
            }
            LogUtil.i(LiveLogTag.IOLiveTag, "onEndingLive");
            stopLiveOperation(LiveRoomInfoProvider.getInstance().isHost());
        }
    }

    @Override
    public void enterpriseForbidLive() {
        if (LiveRoomInfoProvider.getInstance().isHost()) {
            try {
                new TJMakeSureDialog(BaseLiveAct.this, v -> {

                }).setOkText("知道了").setcontent("企业已禁用，60s 后关闭会议", "").hideTitle().showAlert();
            } catch (Exception e) {

            }
        }
    }


    public void stopLiveByGuestOrOfficalHost() {
        if (LiveRoomInfoProvider.getInstance().isHost() && LiveRoomInfoProvider.getInstance().isOfficalLive()) {
            LRDialogController.getInstance().quitLive(this, "2");
            return;
        }

        if (OnlineMembersHelper.getInstance().isMyselfExploring(this)) {
            LRDialogController.getInstance().quitLive(this, "3");
        } else {
            LRDialogController.getInstance().quitLive(this, "2");
        }
    }

    @Override
    public void stopLive(boolean initiative, boolean isCloseOfficialLive) {
        //5.20日 主播退出和观众一致，不做结束调用
        if (LiveRoomInfoProvider.getInstance().isOfficalLive() && !isCloseOfficialLive) {
            exitRoom(-1);
            return;
        }

        LogUtil.i(LiveLogTag.IOLiveTag, "stopLive" + initiative);

        if (LiveRoomInfoProvider.getInstance().isHost(BaseUserInfoCache.getUserId(this))) {
            if (initiative) {
                LRDialogController.getInstance().quitLive(this, "1");
            } else {
                stopLiveOperation(true);
            }
            showBottomView(true);
        } else {
            stopLiveOperation(false);
        }
    }

    //直接结束直播跳转直播结束页面
    public void stopLiveOperation(boolean isHost) {
        LogUtil.i(LiveLogTag.IOLiveTag, "stopLiveOperation");
        if (isHost) {
            ExporeApiManager.mStopAllExploreLive(BaseUserInfoCache.getUserId(this));
        }
        exitRoom(isHost ? 1 : 2);
    }

    protected void refreshDialogs() {

    }

    /**
     * 互动
     */
    @Override
    public void showBottomView(boolean show) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "showBottomView:" + show);

        if (show && LiveRoomInfoProvider.getInstance().isShowFile && !LiveRoomInfoProvider.getInstance().isGuest()) {
            // 若要展示底部，判断当前是否显示文档演示（包含视频），且判断当前文档演示发起人是否是自己,若满足条件则不显示底部
            return;
        }

        if (mBottomOperationView == null) {
            return;
        }
        mBottomOperationView.showSelf(show);
        if (show && LiveRoomInfoProvider.getInstance().isShowFile) {
            mChatMsgContainer.setVisibility(GONE);
        } else if (show && !OnlineMembersHelper.getInstance().hasOnlineMember()) {
            mChatMsgContainer.setVisibility(VISIBLE);
            // 只要消息列表显示了就清除掉红点，他们是互斥的
            goneUnreadCount();
        } else {
            mChatMsgContainer.setVisibility(GONE);
        }

        findViewById(R.id.msgCoverLayout).setVisibility(show ? VISIBLE : GONE);
    }

    @Override
    public void showOperationView() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "showOperationView");

        if (mLiveOperationPop == null) {
            mLiveOperationPop = new LiveOperationPop(this, this);
        }
        mLiveOperationPop.show();
        mLiveOperationPop.refreshContainerHeight();
        showBottomView(false);
    }

    @Override
    public void dismissOperationView() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "dismissOperationView");

        if (mLiveOperationPop != null) {
            mLiveOperationPop.dismiss();
        }

        if (LRDialogController.getInstance().mMemverListDialog != null) {
            LRDialogController.getInstance().mMemverListDialog.dismiss();
        }
        LRDialogController.getInstance().dimissAllDialogs();

    }

    @Override
    public void switchCamera() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "switchCamera");
        if (mTrtcCloud != null) {
            mTrtcCloud.switchCamera();
            LiveRoomInfoProvider.getInstance().isFrontCamera = !LiveRoomInfoProvider.getInstance().isFrontCamera;
            mTrtcCloud.setVideoEncoderMirror(LiveRoomInfoProvider.getInstance().isFrontCamera);
        }
    }

    private void reshowTitleView() {
        findViewById(R.id.rlv_title).setVisibility(VISIBLE);
        findViewById(R.id.rlv_title).setAlpha(1);
        findViewById(R.id.view_graient).setVisibility(VISIBLE);
        findViewById(R.id.view_graient_down).setVisibility(VISIBLE);
        findViewById(R.id.view_graient).setAlpha(1);
        findViewById(R.id.view_graient_down).setAlpha(1);
    }

    /**
     * 聊天室
     */
    @SuppressLint("ClickableViewAccessibility")
    protected void initChatRoom() {
        ChatRoomHelper.isFromLiveRoom = true;
        mChatMsgContainer = (FrameLayout) findViewById(R.id.chatMsgContainer);
        mLiveCRMessageFragment = new LiveCRMessageFragment(this);
        mNoticeContainer = (RelativeLayout) findViewById(R.id.noticeLayout);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.chatMsgContainerGone, mLiveCRMessageFragment).commitAllowingStateLoss();
        addMsgListView();
        KeyboardControlUtil.observerKeyboardVisibleChange(this, (keyboardHeight, isVisible) -> {
            isKeyboardShowing = isVisible;
            if (!isVisible && mLiveCRMessageFragment.inputPanel != null && !mLiveCRMessageFragment.inputPanel.isClickedEmoji
                    && LRDialogController.getInstance().mLiveChatMsgDialog != null) {
                LRDialogController.getInstance().mLiveChatMsgDialog.mTvInput.setVisibility(VISIBLE);
                LRDialogController.getInstance().mLiveChatMsgDialog.mInputContainer.removeAllViews();
            }

            //根据键盘的显示隐藏 控制公告/举办窗的高度
            if (!isVisible && LRDialogController.getInstance().mNoticeDialog != null && LRDialogController.getInstance().mNoticeDialog.isShowing()) {
                LRDialogController.getInstance().mNoticeDialog.update(true);
            }

            //如果是成员列表的搜索键盘消失，就恢复list的高度  在mMemverListDialog
            if (LRDialogController.getInstance().mMemverListDialog != null && LRDialogController.getInstance().mMemverListDialog.isShowing()) {
                if (!isVisible) {
                    TJRecyclerView mRecyclerView = LRDialogController.getInstance().mMemverListDialog.mRecyclerView;
                    if (mRecyclerView != null) {
                        ViewGroup.LayoutParams params = mRecyclerView.getLayoutParams();
                        params.height = (int) (AppUtils.getHeight(BaseLiveAct.this) * 0.55) + AppUtils.dip2px(BaseLiveAct.this, 45 + 2);
                        mRecyclerView.setLayoutParams(params);
                    }
                }
            }
        });
    }

    @Override
    public void onEnterChatRoom() {
        if (mLiveOperationPop != null) {
            mLiveOperationPop.refreshMuteBtnState();
        }
    }


    public void addMsgListView() {
        LiveRoomMsgListPanel messageListPanel = mLiveCRMessageFragment.messageListPanel;

        getHandler().postDelayed(() -> {
            if (messageListPanel == null) {
                addMsgListView();
            } else {
                RecyclerView messageRecyclerView = mLiveCRMessageFragment.getMessageRecyclerView();
                if (messageRecyclerView != null) {
                    if (mChatMsgContainer != null) {
                        mChatMsgContainer.removeAllViews();
                        ViewGroup parent = (ViewGroup) messageRecyclerView.getParent();
                        if (parent != null) {
                            parent.removeAllViews();
                        }
                        mChatMsgContainer.addView(messageRecyclerView);
                    }
                    messageListPanel.refreshMsgBg(ChatRoomMsgViewHolderText.SHOW_BG_STATE_SHOW);
                    LinearLayoutManager layoutManager = (LinearLayoutManager) mLiveCRMessageFragment.getMessageRecyclerView().getLayoutManager();
                    layoutManager.setStackFromEnd(true);
                }
            }
        }, 52);
    }

    @Override
    public void showChatRoomMessage() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "showChatRoomMessage");
        LRDialogController.getInstance().showChatRoomMessageDialog(this, mLiveCRMessageFragment, mChatMsgContainer, mBottomOperationView);
    }

    @Override
    public void onReceiveLiveRoomCustomMessage(String message) {
        if (mTrtcListener != null) {
            mTrtcListener.onReceiveTicCmdMsg(message.getBytes());
        }
    }

    @Override
    public void onMemberJoin(String name) {
        LRUEController.getInstance().joinUE(BaseLiveAct.this, name, mTvJoinLiveTip, mIsCanShowJoinTipUE);
    }

    @Override
    public void onReceiveGuestTips(String info) {
        LRUEController.getInstance().startGuestTipsUE(BaseLiveAct.this, (TextView) findViewById(R.id.tvGuestTips), info);
    }

    /**
     * 需要刷新数据
     * 右下角小轮播图的数据
     */
    @Override
    public void onQRDataRefresh(CRMsgLivePublishQRAttachment attachment) {
        initQRBanner();
        if (LiveRoomHelper.getInstance().receivedQrBigList.size() > 0) {
            LRDialogController.getInstance().showQRCenterList(this, LiveRoomHelper.getInstance().receivedQrBigList.get(0));
        }
    }

    /**
     * 直播间（本场直播被设置为可推广会议）
     * @param attachment
     */
    @Override
    public void onDistributionCallBack(CRMsgLivePublishDistributionAttachment attachment) {
        if (attachment != null) {
            LiveRoomInfoProvider.getInstance().ifDistribution = attachment.getIfDistribution();
            LiveRoomInfoProvider.getInstance().maxBudget = attachment.getMaxBudget();
            LiveRoomInfoProvider.getInstance().level1Commission = attachment.getLevel1Commission();
            LiveRoomInfoProvider.getInstance().level2Commission = attachment.getLevel2Commission();
            LiveRoomInfoProvider.getInstance().level1CommissionEmployee = attachment.getLevel1CommissionEmployee();
            LiveRoomInfoProvider.getInstance().level2CommissionEmployee = attachment.getLevel2CommissionEmployee();
            LiveRoomInfoProvider.getInstance().minPerson = attachment.getMinPerson();
            LiveRoomInfoProvider.getInstance().minViewTime = attachment.getMinViewTime();
            initDistributionStatus();
        }
    }


    /**
     * 公告及聊天
     */
    @Override
    public void onNoticeUpdateCallback() {
        initNotice();
    }

    @Override
    public void onUnreadCallBack(int addCount) {
        if (mBottomOperationView != null) {
            mBottomOperationView.refreshUnreadInfo(addCount);
        }
        if (mWhiteboardManager != null) {
            mWhiteboardManager.refreshUnreadInfo(addCount);
        }
    }

    @Override
    public void goneUnreadCount() {
        // 刷新未读消息数
        if (mBottomOperationView != null) {
            mBottomOperationView.refreshUnreadInfo(-1);
        }
        if (mWhiteboardManager != null) {
            mWhiteboardManager.refreshUnreadInfo(-1);
        }
    }

    private void initNotice() {
        if (mNoticeView == null) {
            mNoticeView = new LiveNoticeView(BaseLiveAct.this, mNoticeContainer);
            mNoticeContainer.removeAllViews();
            mNoticeContainer.addView(mNoticeView.getNoticeView());
        } else {
            mNoticeView.update();
        }
    }

    /**
     * 成员列表
     */
    @Override
    public void showMemberList() {
        LRDialogController.getInstance().showMemberList(this, this);
    }

    /**
     * 分享
     */
    @Override
    public void showShareView() {
        if (FastClickAvoidUtil.isDoubleClick()) {
            return;
        }
        showProgressHUD(this, "");
        LRDialogController.getInstance().showShare(this);
    }

    /**
     * 连麦
     */
    public boolean needResendApplyOnline = false;

    //观众 & 助手 点击互动弹窗 -> 申请同台
    @Override
    public void applyExporeMe() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "applyExporeMe");

        if (OnlineMembersHelper.getInstance().isMyselfExploring(this)) {
            String title = TextUtils.isEmpty(TextConfigCacheManager.getInstance(this).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1)) ? "正在同台互动中" :
                    "正在" + TextConfigCacheManager.getInstance(this).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1) + "中";
            Toasty.normal(this, title).show();
            return;
        }

        new TJMakeSureDialog(this, v -> {
            showProgressHUD(BaseLiveAct.this, "");


            ExporeApiManager.mApplyExploreMe(new Observer<OMBaseResponse>() {
                @Override
                public void onCompleted() {
                    mLiveOperationPop.dismiss();
                    showBottomView(true);
                    dismissProgressHUD();
                }

                @Override
                public void onError(Throwable e) {
                    mLiveOperationPop.dismiss();
                    showBottomView(true);
                    dismissProgressHUD();
                    e.printStackTrace();
                }

                @Override
                public void onNext(OMBaseResponse response) {
                    if (response.isSuccess()) {
                        Toasty.normal(BaseLiveAct.this, "申请成功！等待回复！").show();

                        dismissProgressHUD();

                        needResendApplyOnline = true;

                        LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                                LiveCustomMessageProvider.getApplyExporeData(TJBaseApp.getInstance()), false);


                        //处理发一次不成功的场景
                        mHandler.postDelayed(() -> {
                            if (needResendApplyOnline) {
                                needResendApplyOnline = false;
                                LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                                        LiveCustomMessageProvider.getApplyExporeData(TJBaseApp.getInstance()), false);
                            }
                        }, 3000);

                    } else {
                        //77由梦君确定给出的
                        if ("77".equals(response.code)) {
                            new TJMakeSureDialog(BaseLiveAct.this, v1 -> {
                            }).setOkText("知道了").setcontent("您的申请已被拒绝",
                                    "无法再次申请！").hideTitle().showAlert();
                        } else {
                            Toasty.normal(BaseLiveAct.this, response.msg).show();
                        }
                    }
                }
            });


        }).setTitleAndCotent(TextUtils.isEmpty(TextConfigCacheManager.getInstance(BaseLiveAct.this).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1)) ? "同台互动" :
                        TextConfigCacheManager.getInstance(BaseLiveAct.this).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1),

                TextUtils.isEmpty(TextConfigCacheManager.getInstance(BaseLiveAct.this).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1)) ? "马上申请同台互动？" :
                        "马上申请" + TextConfigCacheManager.getInstance(BaseLiveAct.this).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1) + "?").hideTitle().setBtnSureText("马上申请").setCancelListener(new TJMakeSureDialog.OnDialogCancelListener() {
            @Override
            public void onCancelClick() {

            }
        }).show();
    }

    //主播 & 助手 收到互动申请
    @Override
    public void receiveExporeApply(LiveCustomMessage customMessage) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "receiveExporeApply");

        if (mApplyOnlineTipView == null) {
            return;
        }
        mApplyOnlineTipView.addApplyUser(customMessage);
    }

    //主播 & 助手 刷新当前的申请弹窗
    public void refreshOnlineExplorePop(String userId, boolean isAgree) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "refreshOnlineExplorePop");
        if (mApplyOnlineTipView != null) {
            mApplyOnlineTipView.refreshOnlineExplorePop(userId, isAgree);
        }
    }

    //助手 & 主播 点击互动弹窗 -> 邀请同台
    @Override
    public void inviteExpore() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "inviteExpore");
        GuestListForInveteAct.start(this);
    }

    public void inviteExplore(String fromUserId, String extroId) {
        mHandler.post(() -> LRDialogController.getInstance().showInviteExploreDialog(this, fromUserId, extroId));
    }

    public void startExplore(String toUserId, String extroId) {
        mHandler.post(() -> LRDialogController.getInstance().showStartExploreDialog(this, toUserId, extroId));
    }

    boolean isNeedExploreMe = false;

    public void onSwitchRole() {
        LogUtil.i(BaseLiveLog.BaseLiveTag, "onSwitchRole-BaseLive :" + isNeedExploreMe);
        if (isNeedExploreMe) {
            TXCloudVideoView exploreView = mTRTCVideoViewLayout.onMemberEnter(BaseUserInfoCache.getUserId(this));

            if (exploreView == null) {
                LogUtil.i(LiveLogTag.BaseLiveTag, "exploreMeFail");
                return;
            }
            // 清除摄像头镜像相关参数和设置
            LiveRoomInfoProvider.getInstance().isFrontCamera = true;
            mTrtcCloud.startLocalAudio();
            mTrtcCloud.startLocalPreview(true, exploreView);
            LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "startLocalPreview--onSwitchRole");
            OnlineMembersHelper.getInstance().onMemeberIn(BaseUserInfoCache.getUserId(this));
            onExploreMemberCountChange();

            mLiveOperationPop.mSameTableState = 1;
            mLiveOperationPop.refreshSameTableState();
        } else {
            //避免同台过程中有被关闭声音，然后没有开启就下麦了
            mTrtcCloud.muteLocalAudio(false);
            mTrtcCloud.stopLocalPreview();
            LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "stopLocalPreview--onSwitchRole");
            mTrtcCloud.stopLocalAudio();
            mTRTCVideoViewLayout.onMemberLeave(BaseUserInfoCache.getUserId(this));
            OnlineMembersHelper.getInstance().onMemeberLeave(BaseUserInfoCache.getUserId(this));
            onExploreMemberCountChange();
            mLiveOperationPop.mSameTableState = 0;
            mLiveOperationPop.refreshSameTableState();
        }
    }

    //观众 & 助手 开始上麦
    public void exploreMe() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "exploreMe");
        isNeedExploreMe = true;
        mTrtcCloud.switchRole(TRTCRoleAnchor);
        mTrtcCloud.muteLocalVideo(false);
        // 清除摄像头镜像相关参数和设置
        LiveRoomInfoProvider.getInstance().isFrontCamera = true;
        mTrtcCloud.setVideoEncoderMirror(LiveRoomInfoProvider.getInstance().isFrontCamera);
        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "muteLocalVideo--exploreMe:" + false);
    }

    //观众 & 助手 开始下麦
    @Override
    public void disExploreMe() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "disExploreMe");
        isNeedExploreMe = false;
        mTrtcCloud.switchRole(TRTCRoleAudience);
    }

    //观众 & 助手 收到消息后切换自己的音频
    public void switchGuestAudio(boolean isClose) {
        mTrtcCloud.muteLocalAudio(isClose);
    }

    //切换大会的音频
    public void switchMeetingAudio(boolean isClose) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "switchMeetingAudio");
        if (mTRTCVideoViewLayout != null) {
            mTRTCVideoViewLayout.muteGuestAudio(LiveRoomInfoProvider.getInstance().hostUserId, isClose);
        }
    }

    //切换引流的音频
    public void switchOutLiveAudio(String userId, boolean isClose) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "switchOutLiveAudio");
        mTRTCVideoViewLayout.muteGuestAudio(userId, isClose);
    }

    //观众 & 助手 收到消息后切换自己的视频
    public void switchGuestVideo(boolean isClose, String operationUserId) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "switchOutLiveAudio" + operationUserId);
        mTRTCVideoViewLayout.muteGuestVideo(operationUserId, isClose);

        if (BaseUserInfoCache.isMySelf(this, operationUserId)) {
            mTrtcCloud.muteLocalVideo(isClose);
            LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "muteLocalVideo--oswitchGuestVideo:" + isClose);
            if (isClose) {
                mTrtcCloud.stopLocalPreview();
                LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "stopLocalPreview--s/witchGuestVideo");
            } else {
                mTrtcCloud.startLocalPreview(LiveRoomInfoProvider.getInstance().isFrontCamera,mTRTCVideoViewLayout.getCloudVideoViewByUseId(operationUserId));
                LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "startLocalPreview--switchGuestVideo");
            }
        }
    }

    //切换大小屏
    public void exchangeGuestScreen(boolean isUp, String userId) {
        if (isUp) {
            mTRTCVideoViewLayout.bigUp();
        } else {
            mTRTCVideoViewLayout.bigDown(mTRTCVideoViewLayout.getCloudVideoViewByUseId(userId));
        }
    }

    //助手或主播收到对方操作观众音视频的时候关闭弹窗、助手身份改变的时候关闭弹窗
    public void closeGuestOperationView() {
        LogUtil.i(LiveLogTag.LRMsgTag, "closeGuestOperationView");
        mTRTCVideoViewLayout.hideMenuView();
    }

    //连麦人数变化
    @Override
    public void onExploreMemberCountChange() {
        if (mChatMsgContainer != null) {
            mChatMsgContainer.setVisibility(OnlineMembersHelper.getInstance().hasOnlineMember() ? GONE : VISIBLE);
            if (mChatMsgContainer.getVisibility() == VISIBLE) {
                goneUnreadCount();
            }
        }

        findViewById(R.id.msgCoverLayout).setVisibility(OnlineMembersHelper.getInstance().hasOnlineMember() ? GONE : VISIBLE);
        controlCaremaVisible();
    }

    //控制申请人数提示的显示
    @Override
    public void controlApplyViewVisile(boolean needHide) {
        if (mApplyOnlineTipView == null) {
            return;
        }
        if (needHide) {
            mApplyOnlineTipView.getView().setVisibility(GONE);
        } else {
            mApplyOnlineTipView.refreshApplyCount();
        }
    }

    /**
     * 文档演示
     */
    private boolean isIniting = false;

    private void initWBManager(int type, boolean isClicked) {
        LogUtil.i(LiveLogTag.WhiteboardTag, "initWBManager");
        isIniting = true;
        mWhiteboardManager = new WBManager(this, this, (RelativeLayout) findViewById(R.id.file_board), this);
        mWhiteboardManager.setmInitEvent(type);
        mWhiteboardManager.setFromClick(isClicked);
        mWhiteboardManager.loginTICSDK();
        mWhiteboardManager.mAnimationControllView.clear();
        mWhiteboardManager.addAnimationView(findViewById(R.id.rlv_title));
        mWhiteboardManager.addAnimationView(findViewById(R.id.view_graient));
//        mWhiteboardManager.addAnimationView(findViewById(R.id.view_graient_down));
    }

    @Override
    public void showFileShow(String boardId, boolean isClicked) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "showFileShow" + boardId);

        if (isIniting) {
            if (mWhiteboardManager.getmInitEvent() == -1 || mWhiteboardManager.getmInitEvent() == -2) {
                showProgressHUD(this, "白板初始化中");
                mWhiteboardManager.setmInitEvent(1);
                mWhiteboardManager.setInitBoardId(boardId);
                mWhiteboardManager.setFromClick(isClicked);
            }
        } else {
            if (mWhiteboardManager == null) {
                initWBManager(1, isClicked);
            } else {
                if (isClicked) {
                    mWhiteboardManager.showFileSelectPop();
                } else {
                    if (!LiveRoomInfoProvider.getInstance().isInitWhiteBoardSuccess) {
                        // 初始化失败
                        Toasty.normal(this, "文档演示加载失败").show();
                        LogUtil.i(LiveLogTag.BaseLiveTag, "showFileShowInitError");
                        showBottomView(true);
                        return;
                    }
                    mWhiteboardManager.dismissAllPopview();
                    dismissOperationView();
                    mWhiteboardManager.showClientFileView(boardId);
                }
            }
        }
    }

    @Override
    public void disShowFile() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "disShowFile");
        if (mWhiteboardManager != null) {
            mWhiteboardManager.disShowFile();
        }
        mWhiteboardManager.setmInitEvent(-1);
        reshowTitleView();
    }

    @Override
    public void showDoumentVideo(DoumentVideoSendMsg msg, boolean isInitJump) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "showDoumentVideo");

        if (mWhiteboardManager == null) {
            LogUtil.i(LiveLogTag.BaseLiveTag, "showDoumentVideo-Fail");
            return;
        }

        // 开启文档演示视频
        mWhiteboardManager.dismissAllPopview();
        dismissOperationView();
        mWhiteboardManager.showClientVideoView(msg, isInitJump);
    }

    @Override
    public void disDoumentVideo() {
        // 关闭文档演示视频
        LogUtil.i(LiveLogTag.BaseLiveTag, "disDoumentVideo");
        if (mWhiteboardManager != null) {
            mWhiteboardManager.disShowVideo();
        }
        reshowTitleView();
    }

    @Override
    public void exitFileShow(FileShowCallback callback) {
        // 退出文档演示功能
        LogUtil.i(LiveLogTag.BaseLiveTag, "exitFileShow");
        if (mWhiteboardManager != null) {
            mWhiteboardManager.exitFileShow(callback);
        }
        reshowTitleView();
    }

    /**
     * 文档演示时
     * 设置公告显示与隐藏（仅在文档演示时使用）
     * visible：公告应该显示（文档演示已退出）
     * gone：公告应该隐藏（文档演示已展示）
     *
     * @param status
     */
    @Override
    public void setNoticeViewShowStatus(int status) {
        // 公告显示的情况下（进入文档演示隐藏公告，退出文档演示功能显示出来）
        if (mNoticeView != null) {
            mNoticeView.setShowStatus(status);
        }

        //网洽会三期---控制 项目推荐/报名上会/活动/进入聊天欢迎UE 的显示隐藏
        if (status == View.VISIBLE) {
            mIsCanShowJoinTipUE = true;
            // 顶部区域：主播，助手显示ID
            if (!LiveRoomInfoProvider.getInstance().isGuest()) {
                findViewById(R.id.iv_fileshow_back).setVisibility(GONE);
                findViewById(R.id.view_graient_down).setVisibility(VISIBLE);

                // 同台人数显示
                controlApplyViewVisile(false);
            }

            // 开播按钮：显示
            if (!TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().isApply)
                    && "1".equals(LiveRoomInfoProvider.getInstance().isApply)
                    && !LiveRoomInfoProvider.getInstance().isHost()) {
                // 只有有开播权限的才显示开播按钮（除去主播）
                findViewById(R.id.ivStartLive).setVisibility(VISIBLE);
            } else {
                findViewById(R.id.ivStartLive).setVisibility(GONE);
            }

            // 项目推荐
            if (LiveRoomInfoProvider.getInstance().isCanShowProjectTJ && mProjectLayout != null) {
                mProjectLayout.setVisibility(VISIBLE);
            }
            mWhiteboardManager.showParentGraientView();
            // 防止ios关闭不发消息的问题，自己手动显示，双保险
            showBottomView(true);
            checkShowTip1(true);

            // 文档演示退出后判断右下角轮播图是否有数据，有数据就显示并且已经隐藏就显示（不区分身份，因为有可能演示中助手身份被取消了）
            if (mQRListSmallView.getVisibility() != VISIBLE && LiveRoomHelper.getInstance().receivedQrSmallList.size() > 0) {
                mQRListSmallView.setVisibility(VISIBLE);
            }
        } else {
            mIsCanShowJoinTipUE = false;
            // 顶部区域：主播，助手不显示ID显示退出文档的
            if (!LiveRoomInfoProvider.getInstance().isGuest()) {
                findViewById(R.id.iv_fileshow_back).setVisibility(VISIBLE);
                findViewById(R.id.view_graient_down).setVisibility(GONE);

//                // 同台人数显示
                controlApplyViewVisile(true);
            }

            // 开播按钮：隐藏
            findViewById(R.id.ivStartLive).setVisibility(GONE);

            if (mProjectLayout != null) {
                mProjectLayout.setVisibility(GONE);
            }
            // 文档演示时，主播/助手不显示右下角的轮播图
            if (LiveRoomInfoProvider.getInstance().isHost() || LiveRoomInfoProvider.getInstance().isHostHelper()) {
                mQRListSmallView.setVisibility(GONE);
            }
        }
    }

    /**
     * 手绘板展示时
     * 设置进入房间提醒显示与隐藏（仅在手绘板演示时使用）
     * true：手绘板正在展示（进入房间提醒需要隐藏）
     * false：手绘板已退出（进入房间提醒可以显示了）
     *
     * @param isShow
     */
    @Override
    public void onWacomShowHidden(boolean isShow) {
        mIsCanShowJoinTipUE = !isShow;
    }


    // 是否已经处于隐藏状态
    private boolean isShowUpward = false;

    @Override
    public void setBottomGraientHeight(boolean isScrolling) {
        if (findViewById(R.id.view_graient_down) == null) {
            return;
        }
        if (isScrolling) {
//             若此时处于显示状态则触发渐隐，否则不触发动画
            // 向上滑动
            if (isShowUpward) {
                // 若此时处于上隐状态则出发渐显动画，否则不出触发
                isShowUpward = false;
                //图片平移出去还能弹回来
                //如果没有mimg.getTranslationX()则图片不会弹回来
                ObjectAnimator translationDown = ObjectAnimator.ofFloat(findViewById(R.id.view_graient_down), "translationY", -AppUtils.dip2px(this, 70), 0f);
                //时间
                translationDown.setDuration(400);
                //执行动画
                translationDown.start();
            }
        } else {
            // 从上面滑动回来
            if (!isShowUpward) {
                isShowUpward = true;
                //图片平移出去还能弹回来
                //如果没有mimg.getTranslationX()则图片不会弹回来
                ObjectAnimator translationUpward = ObjectAnimator.ofFloat(findViewById(R.id.view_graient_down), "translationY",
                        0f, -AppUtils.dip2px(this, 70));
                //时间
                translationUpward.setDuration(400);
                //执行动画
                translationUpward.start();
            }

        }
    }

    public void getWhiteBoardIsShow() {
        //2.是否在绘画 & 文档
        LiveRoomIOHelper.getDocumentStatus(new Observer<DoumentIdStatusResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(DoumentIdStatusResponse baseResponse) {
                if (baseResponse.isSuccess()) {
                    if (baseResponse.data == null) {
                        return;
                    }

                    if ("1".equals(baseResponse.data.isShow)) {
                        if (!TextUtils.isEmpty(baseResponse.data.documentId) && DRAWPAD_WB_ID.equals(baseResponse.data.documentId)) {
                            if (LiveRoomInfoProvider.getInstance().isHost()) {
                                // 若是主播重新从进入发现正在展示手绘板则先关闭手绘板
                                LiveRoomIOHelper.setDocumentStatus(BaseLiveAct.this, "0", DRAWPAD_WB_ID, new IOLiveRoomListener() {
                                    @Override
                                    public void onIOError(String error, int errorCode) {
                                    }

                                    @Override
                                    public void onIOSuccess() {
                                        LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                                                LiveCustomMessageProvider.getSwitchShowDrawPop(BaseLiveAct.this, false), false);
                                    }
                                });
                            } else {
                                showDrawPad(false);
                            }
                        } else {
                            if (LiveRoomInfoProvider.getInstance().isHost()) {
                                // 若是主播重新从进入发现正在展示文档则先关闭文档
                                LiveRoomIOHelper.setDocumentStatus(BaseLiveAct.this, "0", "", new IOLiveRoomListener() {
                                    @Override
                                    public void onIOError(String error, int errorCode) {

                                    }

                                    @Override
                                    public void onIOSuccess() {
                                        LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                                                LiveCustomMessageProvider.getWBColoseData(BaseLiveAct.this), false);
                                    }
                                });
                            } else {
                                // 判断控制人是否是自己
                                showFileShow(baseResponse.data.documentId, false);
                            }
                        }
                    }
                }
            }
        });

    }

    public WBManager getWBManager() {
        return mWhiteboardManager;
    }

    //点击暂离文档演示关闭成功后回调
    @Override
    public void exitFileShowSuccess() {
        mWhiteboardManager.dismissFileSelectPopView();
        LRDialogController.getInstance().templeteLeave(this);
        showBottomView(false);
    }

    /**
     * 手绘板
     */
    @Override
    public void showDrawPad(boolean isClick) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "showDrawPad");

        if (LiveRoomInfoProvider.getInstance().isShowFile) {
            disShowFile();
        }

        if (isIniting) {
            if (mWhiteboardManager.getmInitEvent() == -1 || mWhiteboardManager.getmInitEvent() == -2) {
                showProgressHUD(this, "白板初始化中");
                mWhiteboardManager.setmInitEvent(0);
                mWhiteboardManager.setFromClick(isClick);
            }
        } else {
            if (mWhiteboardManager == null) {
                initWBManager(0, isClick);
            } else {
                if (!LiveRoomInfoProvider.getInstance().isInitWhiteBoardSuccess) {
                    // 初始化失败
                    Toasty.normal(this, "手写板加载失败").show();
                    LogUtil.i(LiveLogTag.BaseLiveTag, "showDrawPadInitError");
                    showBottomView(true);
                    return;
                }
                if (isClick) {
                    LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                            LiveCustomMessageProvider.getSwitchShowDrawPop(this, true), false);
                }
                dismissOperationView();
                mWhiteboardManager.showDrawPad(isClick);
            }
        }
    }

    @Override
    public void disDrawPad(boolean isClick) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "disDrawPad");
        if (isClick) {
            LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                    LiveCustomMessageProvider.getSwitchShowDrawPop(this, false), false);
        }
        if (mWhiteboardManager != null) {
            mWhiteboardManager.disDrawPad(isClick);
        }
    }

    @Override
    public void onInitWBFail(int initType) {
        LiveRoomInfoProvider.getInstance().isInitWhiteBoardSuccess = false;
        isIniting = false;
        try {
            dismissProgressHUD();
        } catch (Exception e) {
        }
    }

    @Override
    public void onInitWBSuccess(int initType) {
        try {
            dismissProgressHUD();
        } catch (Exception e) {
        }
        isIniting = false;
        LiveRoomInfoProvider.getInstance().isInitWhiteBoardSuccess = true;
        // 白板初始化成功后判断是否正在展示白板
        getWhiteBoardIsShow();
    }

    /**
     * 引流
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1008 && resultCode == 2008) {
            ArrayList<SelectOutLiveModel> selectedArray = data.getParcelableArrayListExtra("selectedList");

            if (selectedArray == null) {
                return;
            }

            if ((selectedArray.size() + OnlineMembersHelper.getInstance().mOnlineMembers.size()) > (LiveRoomConfig.ROOM_MAX_USER - 1)) {
                LogUtil.i(LiveLogTag.BaseLiveTag, "已达上线依然引流，异常");
                return;
            }

            for (SelectOutLiveModel model : selectedArray) {
                String outlinkUrl = model.pullFlvUrl;
                String outlinkRtmp = model.pullRtmpUrl;
                String streamId = model.streamId;
                showOuterLive(outlinkUrl, outlinkRtmp, streamId, false);
            }

            if (!LiveRoomInfoProvider.getInstance().isGuest()) {
                LiveCustomMessageProvider.sendLiveRoomCustomMessage("", LiveCustomMessageProvider.getOpenOutlink(this, selectedArray), false);
            }
        }
    }

    @Override
    public void selectOuterLive() {
        OuterLiveListAct.start(this);
        showBottomView(true);
    }

    @Override
    public void showOuterLive(String outlinkUrl, String outlinkRtmp, String streamId, boolean isFromInit) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "showOuterLive:" + streamId);
        mTRTCVideoViewLayout.startOutLive(outlinkUrl, outlinkRtmp, streamId, isFromInit);
        OnlineMembersHelper.getInstance().onMemeberIn(streamId);
        onExploreMemberCountChange();
    }

    @Override
    public void disOuterLive(String streamId, boolean isDireact, EndOuterLiveCallback endOuterLiveCallback) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "disOuterLive:" + streamId);

        if (LiveRoomInfoProvider.getInstance().isGuest() || !isDireact) {
            mTRTCVideoViewLayout.disOuterLive(streamId);
            OnlineMembersHelper.getInstance().onMemeberLeave(streamId);
            onExploreMemberCountChange();
            LogUtil.i(LiveLogTag.BaseLiveTag, "disOuterLiveSuccess");
            if (endOuterLiveCallback != null) {
                endOuterLiveCallback.end();
            }
        } else {
            showProgressHUD(this, "");
            OMAppApiProvider.getInstance().closeLiveStream(streamId, LiveRoomInfoProvider.getInstance().liveId, new Observer<OMBaseResponse>() {
                @Override
                public void onCompleted() {
                    dismissProgressHUD();
                }

                @Override
                public void onError(Throwable e) {
                    dismissProgressHUD();
                    Toasty.normal(BaseLiveAct.this, "网络错误，请稍后再试").show();
                }

                @Override
                public void onNext(OMBaseResponse response) {
                    if (response.isSuccess()) {
                        if (endOuterLiveCallback != null) {
                            endOuterLiveCallback.end();
                        }
                        showBottomView(true);
                        mTRTCVideoViewLayout.disOuterLive(streamId);
                        OnlineMembersHelper.getInstance().onMemeberLeave(streamId);
                        onExploreMemberCountChange();
                        LiveCustomMessageProvider.sendLiveRoomCustomMessage("", LiveCustomMessageProvider.getSwitchOutlink(BaseLiveAct.this, "", "", streamId), false);

                    } else {
                        //5.17不做异常处理
                        Toasty.normal(BaseLiveAct.this, response.msg).show();
                    }

                }
            });
        }
    }

    @Override
    public void stopSomeLive(String streamId, String roomTitle) {
        //收到一个没有被引入的流下线的通知<上线的时候 因为数量限制没有加入>
        if (!OnlineMembersHelper.getInstance().someBodyHasOnline(streamId)) {
            return;
        }

        if (LiveRoomInfoProvider.getInstance().isHost()) {
            try {
                new TJMakeSureDialog(BaseLiveAct.this, v1 -> disOuterLive(streamId, true, null)).setOkText("知道了").setcontent(roomTitle + "的视频资源链接中断", "请检查视频来源").hideTitle().showAlert();
            } catch (Exception e) {

            }
        }
    }

    @Override
    public void onOutliveRoomLiveAdd(SelectOutLiveModel selectOutLiveModel) {
        if (OnlineMembersHelper.getInstance().someBodyHasOnline(selectOutLiveModel.streamId)) {
            return;
        }
        if (LiveRoomInfoProvider.getInstance().isHost()) {
            NewOutlivePop outlivePop = new NewOutlivePop(BaseLiveAct.this, v -> startANewOutlive(selectOutLiveModel));
            outlivePop.setType(selectOutLiveModel.listType, selectOutLiveModel.anchorName, selectOutLiveModel.audienceName);
            outlivePop.show();
        }
    }

    private void startANewOutlive(SelectOutLiveModel selectOutLiveModel) {
        if (!LiveRoomInfoProvider.getInstance().isHost()) {
            return;
        }

        if (!OnlineMembersHelper.getInstance().canOnlineNow()) {
            Toasty.normal(this, "数量达到上限，未引入新流").show();
            return;
        }

        showProgressHUD(BaseLiveAct.this, "处理中");
        OMAppApiProvider.getInstance().startANewLiveStream(LiveRoomInfoProvider.getInstance().liveId, BaseUserInfoCache.getUserId(this), selectOutLiveModel.liveAcceptId, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(OMBaseResponse response) {
                if (response.isSuccess()) {
                    mHandler.postDelayed(() -> {
                        //新流增加主播发消息
                        String outlinkUrl = selectOutLiveModel.pullFlvUrl;
                        String outlinkRtmp = selectOutLiveModel.pullRtmpUrl;
                        String streamId = selectOutLiveModel.streamId;
                        showOuterLive(outlinkUrl, outlinkRtmp, streamId, false);

                        ArrayList<SelectOutLiveModel> selectedArray = new ArrayList<>();
                        selectedArray.add(selectOutLiveModel);

                        if (!LiveRoomInfoProvider.getInstance().isGuest()) {
                            LiveCustomMessageProvider.sendLiveRoomCustomMessage("", LiveCustomMessageProvider.getOpenOutlink(BaseLiveAct.this, selectedArray), false);
                        }
                    }, 100);
                } else {
                    Toasty.normal(BaseLiveAct.this, response.msg).show();
                }
            }
        });
    }

    /**
     * 发起成交
     */
    @Override
    public void startDeal() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "startDeal");
        this.showBottomView(false);
        showProgressHUD(this, "");
        //查询直播间主播所属企业是否有代播权限
        OMAppApiProvider.getInstance().queryCompanyAgentPower(LiveRoomInfoProvider.getInstance().otherCompanyCode, new Observer<AgentLiveModel>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {

                dismissProgressHUD();
                LRDialogController.getInstance().startDeal(BaseLiveAct.this);
            }

            @Override
            public void onNext(AgentLiveModel omBaseResponse) {
                dismissProgressHUD();
                if(omBaseResponse.isSuccess()){
                    mHandler.postDelayed(() -> {
                        switch (omBaseResponse.data.agentLive){
                            case 0:
                                LRDialogController.getInstance().startDeal(BaseLiveAct.this);

                                break;
                            case 1:
                                LRDialogController.getInstance().startAgentDeal(BaseLiveAct.this);

                                break;
                        }
                    },200);

                }
                else{

                    LRDialogController.getInstance().startDeal(BaseLiveAct.this);
                }

            }
        });
    }

    /**
     * 发起活动（二维码）
     */
    @Override
    public void startQR() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "startQR");
        LRDialogController.getInstance().startPublishQrList(this);
    }

    /**
     * 暂离
     */
    // 点击暂离时做处理
    @Override
    public void onClickTempleteLeave() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "onClickTempleteLeave");
        // 点击暂离判断是否在文档演示
        if (LiveRoomInfoProvider.getInstance().isShowFile) {
            new TJMakeSureDialog(this, "是否结束文档演示？",
                    v -> {
                        // 继续演示，关闭弹窗

                    }).setBtnText("继续演示", "结束").setCancelListener(() -> {
                // 离开，退出文档
                exitFileShow(this);
            }).show();
        } else {
            LRDialogController.getInstance().templeteLeave(this);
        }

    }

    // 发起暂离
    public void templateLive(String templateLiveTip, String time) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "templateLive");
        if (LRDialogController.getInstance().mShortLivePop != null) {
            if (LRDialogController.getInstance().mShortLivePop.isShowing()) {
                LRDialogController.getInstance().mShortLivePop.dismiss();
            }
        }
        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "muteLocalVideo--templateLive:" + true);
        mTrtcCloud.muteLocalVideo(true);
        mTrtcCloud.muteLocalAudio(true);
        LiveRoomInfoProvider.getInstance().hasTemplateLeave = true;
        refreshTempLiveTip(templateLiveTip, time, false);
        mTRTCVideoViewLayout.updateVideoStatus(BaseUserInfoCache.getUserId(BaseLiveAct.this), false);
    }

    //大会暂离开关
    public void templateLiveMeeting() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "templateLiveMeeting");
        mTRTCVideoViewLayout.isChangingOfficeMeetingStatus = true;
        mTRTCVideoViewLayout.updateVideoStatus(LiveRoomInfoProvider.getInstance().hostUserId, !LiveRoomInfoProvider.getInstance().hasTemplateLeave);
    }

    // 暂离后继续直播
    @Override
    public void finishTempleteLeave() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "finishTempleteLeave");

        LiveRoomInfoProvider.getInstance().hasTemplateLeave = false;

        if (LiveRoomInfoProvider.getInstance().isHost()) {
            //兼容大会，大会的主播不需要视频上送
            if (!LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                if (OnlineMembersHelper.getInstance().isHostVideoOn) {
                    mTrtcCloud.muteLocalVideo(false);
                    LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "muteLocalVideo--finishTempleteLeave:" + false);
                }
                if (OnlineMembersHelper.getInstance().isHostAudioOn) {
                    mTrtcCloud.muteLocalAudio(false);
                }
                if (mTRTCVideoViewLayout.checkBigUpIsHostNow()) {
                    // 2020年5月25日：取消暂离后如果在大屏幕则开启声音（不论之前声音的状态是什么）
                    mTRTCVideoViewLayout.startLiveAudio(mTRTCVideoViewLayout.getCloudVideoViewByUseId(LiveRoomInfoProvider.getInstance().hostUserId));
                }
            }
            LiveRoomIOHelper.goOnLive(this);
            mTRTCVideoViewLayout.updateVideoStatus(BaseUserInfoCache.getUserId(this), OnlineMembersHelper.getInstance().isHostVideoOn);
            mTRTCVideoViewLayout.refreshHostUI();
        }


        if (LiveRoomInfoProvider.getInstance().isHostHelper()) {
            LiveRoomIOHelper.goOnLive(this);
        }

        refreshTempLiveTip("", "", false);

        // 结束大会暂离
        if (LiveRoomInfoProvider.getInstance().isOfficalLive()) {
            templateLiveMeeting();
        }
    }

    // isInit:是否在刚进入直播时处理
    public void refreshTempLiveTip(String tip, String time, boolean isInit) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "refreshTempLiveTip");
        LiveRoomInfoProvider.getInstance().templateLeaveTime = time;
        if (LiveRoomInfoProvider.getInstance().hasTemplateLeave) {
            mTRTCVideoViewLayout.refreshTemplateLeaveTip(tip, time, isInit);
        }

        if (mLiveOperationPop != null) {
            mLiveOperationPop.refreshTemplateLive();
        }
    }

    /**
     * 公告
     */
    @Override
    public void showNotice() {
        LRDialogController.getInstance().showNotice(this, "1");
    }

    /**
     * 举报
     */
    @Override
    public void showReport() {
        LRDialogController.getInstance().showNotice(this, "2");
    }

    @Override
    public void callOffHook(String selectUserId) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "callOffHook:" + selectUserId);
        if (LiveRoomInfoProvider.getInstance().isHost(selectUserId)) {
            // 主播
            if (!LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                LiveRoomInfoProvider.getInstance().isCallPhoneIng = true;
                mTRTCVideoViewLayout.updateCloudViewOfCallPhone(mTRTCVideoViewLayout.getCloudVideoViewByUseId(selectUserId),selectUserId);
            }
        } else {
            // 同台人员
            OnlineMember onlineMember = OnlineMembersHelper.getInstance().getMemberByUserId(selectUserId);
            TXCloudVideoView txCloudVideoView = mTRTCVideoViewLayout.getCloudVideoViewByUseId(selectUserId);
            if(onlineMember != null && txCloudVideoView != null) {
                onlineMember.isCallPhoneIng = true;
                mTRTCVideoViewLayout.updateCloudViewOfCallPhone(txCloudVideoView,selectUserId);
            }
        }
    }

    @Override
    public void callIdle(String selectUserId) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "callIdle:" + selectUserId);
        if (LiveRoomInfoProvider.getInstance().isHost(selectUserId)) {
            // 主播
            if (!LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                LiveRoomInfoProvider.getInstance().isCallPhoneIng = false;
                mTRTCVideoViewLayout.updateCloudViewOfCallPhone(mTRTCVideoViewLayout.getCloudVideoViewByUseId(selectUserId),selectUserId);
            }
        } else {
            // 同台人员
            OnlineMember onlineMember = OnlineMembersHelper.getInstance().getMemberByUserId(selectUserId);
            TXCloudVideoView txCloudVideoView = mTRTCVideoViewLayout.getCloudVideoViewByUseId(selectUserId);
            if(onlineMember != null && txCloudVideoView != null){
                onlineMember.isCallPhoneIng = false;
                mTRTCVideoViewLayout.updateCloudViewOfCallPhone(txCloudVideoView,selectUserId);
            }
        }
    }

    /**
     * LifeCycler
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void finish() {
        super.finish();
        onFinish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPausing = true;
        LogUtil.i(LiveLogTag.IOLiveTag, "onPause");
        if (isFinishing()) {
            onFinish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isPausing = false;
        checkShowTip1(true);

        LRDialogController.getInstance().refreshMemberListDialog();
    }

    boolean hasFinished = false;

    public void onFinish() {
        LogUtil.i(LiveLogTag.IOLiveTag, "onPause - finish");

        if (mTrtcCloud != null) {
            mTrtcCloud.exitRoom();
        }

        if (mLiveCRMessageFragment != null) {
            mLiveCRMessageFragment.registerObservers(false);
        }

        //释放底部操作栏
        if (mBottomOperationView != null) {
            mBottomOperationView.destory();
            mBottomOperationView = null;
        }

        //释放引流View
        if (mTRTCVideoViewLayout != null) {
            mTRTCVideoViewLayout.onDestory();
        }

        //释放白板
        if (mWhiteboardManager != null) {
            // 无论主播还是观众，结束页面都判断一下当前视频播放器的状态，若正在播放则释放掉
            mWhiteboardManager.releaseVideoPlayer();
            mWhiteboardManager.quitClassroom();
            mWhiteboardManager = null;
        }

        try {
            if (headsetPlugReceiver != null) {
                unregisterReceiver(headsetPlugReceiver);
            }
        } catch (Exception e) {

        }

        try {
            if (mInnerOutCallReceiver != null) {
                unregisterReceiver(mInnerOutCallReceiver);
            }
        } catch (Exception e) {

        }

        hasFinished = true;
        ChatRoomHelper.isFromLiveRoom = false;
        if (OnlineMembersHelper.getInstance().mOnlineMembers != null) {
            OnlineMembersHelper.getInstance().mOnlineMembers.clear();
        }
        LRDialogController.getInstance().cleanDialogs();
        LiveRoomInfoProvider.getInstance().onDestory();
        LiveRoomHelper.getInstance().receivedQrBigList.clear();
        LiveRoomHelper.getInstance().receivedQrSmallList.clear();
        LogUtil.i(LiveLogTag.IOLiveTag, "finish-end");
    }

    @Override
    public void onBackPressed() {
        if (LiveRoomInfoProvider.getInstance().isHost()) {
            LogUtil.i(LiveLogTag.IOLiveTag, "onBackPressed");
            if (LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                stopLiveByGuestOrOfficalHost();
            } else {
                stopLive(true, false);
            }
        } else {
            stopLiveByGuestOrOfficalHost();
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        //跳转至项目详情播视频时直播间静音，返回时再恢复
        if (mTrtcCloud != null) {
            mTrtcCloud.muteAllRemoteAudio(false);
        }
    }

    /**
     * EventBus
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPublicStringEvent(PublicStringEvent event) {

        //重新进入直播间
        if (PublicStringEvent.CHAT_ROOM_LIVE_REENTER.equals(event.eventTitle)) {
            if (mLiveCRMessageFragment != null) {
                mLiveCRMessageFragment.enterChatRoom();
            }
        }


        //身份变更
        if (PublicStringEvent.LIVE_GENGLE_CHANGE.equals(event.eventTitle)) {
            refreshDialogs();

            refreshHelperInfo();

            if (LiveRoomInfoProvider.getInstance().hasTemplateLeave) {
                mTRTCVideoViewLayout.refreshTemplateLeaveView();
            }

            //获取当前申请人数
            if (!LiveRoomInfoProvider.getInstance().isGuest()) {
                mApplyOnlineTipView.getCurrentApplyExploreCount();
            }

            //5.28日新增设为助手的提示
            if (LiveRoomInfoProvider.getInstance().isHostHelper()) {
                Toasty.normal(this, "您被主持人设置为助手").show();
                checkShowTip1(true);
            }
        }

        // 以前不是分销客现在变成分销客了，直播间根据判断是否需要显示分销按钮
        if (PublicStringEvent.DISTRIBUTOR_CHANGE.equals(event.eventTitle)) {
            initDistributionStatus();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOuterLiveEvent(OuterLiveEvent event) {
        // 此方法主要针对与在引流列表点击引流后，没有等接口返回就直接finish的情况
        if (event != null && event.respStreamRoomDtoList != null && event.respStreamRoomDtoList.size() > 0) {
            if ((event.respStreamRoomDtoList.size() + OnlineMembersHelper.getInstance().mOnlineMembers.size()) > (LiveRoomConfig.ROOM_MAX_USER - 1)) {
                LogUtil.i(LiveLogTag.BaseLiveTag, "已达上线依然引流，异常");
                return;
            }

            for (SelectOutLiveModel model : event.respStreamRoomDtoList) {
                String outlinkUrl = model.pullFlvUrl;
                String outlinkRtmp = model.pullRtmpUrl;
                String streamId = model.streamId;
                showOuterLive(outlinkUrl, outlinkRtmp, streamId, false);
            }

            if (!LiveRoomInfoProvider.getInstance().isGuest()) {
                LiveCustomMessageProvider.sendLiveRoomCustomMessage("", LiveCustomMessageProvider.getOpenOutlink(this, event.respStreamRoomDtoList), false);
            }
        }
    }

    /**
     * 网络处理
     */
    //处理多次收到同一个消息
    int netStatus = -1;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetChangeEvent(NetChangeEvent event) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "onNetChangeEvent" + event.currentState);
        if (netStatus == event.currentState) {
            return;
        }
        netStatus = event.currentState;
        if (0 == event.currentState) {
            findViewById(R.id.rlv_preview_layout).setVisibility(VISIBLE);
            Toasty.normal(BaseLiveAct.this, "网络错误，请检查网络").show();
        } else {
            if (findViewById(R.id.rlv_preview_layout).getVisibility() == VISIBLE) {
                findViewById(R.id.rlv_preview_layout).setVisibility(GONE);
                if (mTRTCVideoViewLayout != null) {
                    //
                    if (!LiveRoomInfoProvider.getInstance().isHost()) {
                        TXCloudVideoView txCloudVideoView = mTRTCVideoViewLayout.getCloudVideoViewByUseId(BaseUserInfoCache.getUserId(this));
                        if (txCloudVideoView != null && BaseUserInfoCache.getUserId(this).equals(txCloudVideoView.getUserId())) {
                            // 判断本地连麦用户是否有自己，如果有则判断当前直播间的连麦人的音视频状态
                            //  获取当前连麦人的音视频状态
                            ExporeApiManager.refreshGuestMediaStatus(new Observer<ExploreMemeberStatusResponse>() {
                                @Override
                                public void onCompleted() {
                                }

                                @Override
                                public void onError(Throwable e) {
                                }

                                @Override
                                public void onNext(ExploreMemeberStatusResponse exploreMemeberStatusResponse) {
                                    if (exploreMemeberStatusResponse.isSuccess()) {
                                        boolean isHaveMe = false;
                                        if (exploreMemeberStatusResponse.data != null && exploreMemeberStatusResponse.data.size() > 0) {
                                            for (int i = 0; i < exploreMemeberStatusResponse.data.size(); i++) {
                                                if (exploreMemeberStatusResponse.data.get(i).selectUserId.equals(BaseUserInfoCache.getUserId(BaseLiveAct.this))) {
                                                    isHaveMe = true;
                                                    break;
                                                }
                                            }
                                        }

                                        if (!isHaveMe) {
                                            LogUtil.i(LiveLogTag.BaseLiveTag, "onNetChangeEvent:LeaveMe");
                                            mTRTCVideoViewLayout.onMemberLeave(BaseUserInfoCache.getUserId(BaseLiveAct.this));
                                            OnlineMembersHelper.getInstance().onMemeberLeave(BaseUserInfoCache.getUserId(BaseLiveAct.this));
                                            onExploreMemberCountChange();
                                            disExploreMe();
                                        }
                                        mTRTCVideoViewLayout.onResume();
                                    }
                                }
                            });
                        } else {
                            mTRTCVideoViewLayout.onResume();
                        }
                    } else {
                        mTRTCVideoViewLayout.onResume();
                    }
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPhoneSateEvent(PhoneStateEvent event) {
        switch (event.state) {
            case "CALL_STATE_RINGING":
                // 响铃中
                break;
            case "CALL_STATE_OFFHOOK":
                // 接听：自己呼出去电话也是这个状态
                if (OnlineMembersHelper.getInstance().isMyselfExploring(this) || ((LiveRoomInfoProvider.getInstance().isHost() && !LiveRoomInfoProvider.getInstance().isOfficalLive()))) {
                    LiveCustomMessageProvider.sendLiveRoomCustomMessage("",LiveCustomMessageProvider.getCallOffHookData(BaseLiveAct.this),false);
                    // 设置通话状态
                    ExporeApiManager.mControlGuestMediaStatus(BaseUserInfoCache.getUserId(BaseLiveAct.this), "0", "5","1");
                    callOffHook(BaseUserInfoCache.getUserId(BaseLiveAct.this));
                }
                break;
            case "CALL_STATE_IDLE":
                // 挂断
                if (OnlineMembersHelper.getInstance().isMyselfExploring(this) || ((LiveRoomInfoProvider.getInstance().isHost() && !LiveRoomInfoProvider.getInstance().isOfficalLive()))) {
                    LiveCustomMessageProvider.sendLiveRoomCustomMessage("",LiveCustomMessageProvider.getCallIdelData(BaseLiveAct.this),false);
                    // 设置通话状态
                    ExporeApiManager.mControlGuestMediaStatus(BaseUserInfoCache.getUserId(BaseLiveAct.this), "0", "5","0");
                    callIdle(BaseUserInfoCache.getUserId(BaseLiveAct.this));
                }
                break;
            default:
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHostVideoState(HostVideoEvent event) {
        if (event == null) {
            return;
        }
        if (BaseUserInfoCache.isMySelf(this, event.selectUserId)) {
            mTrtcCloud.muteLocalVideo(event.isClose);
            LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "muteLocalVideo--onHostVideoState:" + false);
        }    }

    /**
     * 观众 & 助手 初始化当前直播间状态：
     * 0.助手的话获取当前同台状态
     * 1.是否在引流
     * 2.是否在绘画
     * 3.是否在文档
     */
    private void initHostOperation() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "initHostOperation");

        //0.获取当前同台状态
        mHandler.postDelayed(() -> ExporeApiManager.refreshGuestMediaStatus(new Observer<ExploreMemeberStatusResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(ExploreMemeberStatusResponse exploreMemeberStatusResponse) {
                // onUserEnter不会回调临时的想法
                if (!LiveRoomInfoProvider.getInstance().isHost() && exploreMemeberStatusResponse.data.size() > 0){
                    for (int i = 0; i<exploreMemeberStatusResponse.data.size(); i++){
                        if (BaseUserInfoCache.getUserId(BaseLiveAct.this).equals(exploreMemeberStatusResponse.data.get(i).selectUserId)) {
                            exploreMe();
                            break;
                        }
                    }
                }
                for (OnlineMember onlineMember : OnlineMembersHelper.getInstance().mOnlineMembers) {
                    mTRTCVideoViewLayout.muteGuestVideo(onlineMember.userId, !onlineMember.isOpenVideo);
                    mTRTCVideoViewLayout.muteGuestAudio(onlineMember.userId, !onlineMember.isOpenAudio);
                }

                if (!OnlineMembersHelper.getInstance().isHostVideoOn) {
                    mTRTCVideoViewLayout.muteGuestVideo(LiveRoomInfoProvider.getInstance().hostUserId, true);
                }

                if (!OnlineMembersHelper.getInstance().isHostAudioOn) {
                    mTRTCVideoViewLayout.muteGuestAudio(LiveRoomInfoProvider.getInstance().hostUserId, true);

                    if (LiveRoomInfoProvider.getInstance().isHost()) {
                        mTrtcCloud.muteLocalAudio(true);
                    }
                }

                for (ExploreMemeberStatusResponse.ExploreMemeberStatus exploreMemeberStatus : exploreMemeberStatusResponse.data) {
                    if (LiveRoomInfoProvider.getInstance().isOfficalLive() && LiveRoomInfoProvider.getInstance().isHost(exploreMemeberStatus.selectUserId)) {
                        // 处理官方大会的主播声音状态
                        mTRTCVideoViewLayout.muteGuestAudio(exploreMemeberStatus.selectUserId, "0".equals(exploreMemeberStatus.audioStatus) ? true : false);
                    }

                    // 对主播、同台人员接听/挂断电话的初始化
                    if (!TextUtils.isEmpty(exploreMemeberStatus.isCalling) && "1".equals(exploreMemeberStatus.isCalling)) {
                        // 只特殊处理接听状态
                        if (LiveRoomInfoProvider.getInstance().isHost(exploreMemeberStatus.selectUserId)) {
                            if (!LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                                // 非官方大会才对主播做处理
                                LiveRoomInfoProvider.getInstance().isCallPhoneIng = true;
                                callOffHook(exploreMemeberStatus.selectUserId);
                            }
                        } else {
                                //同台观众
                                OnlineMember onlineMember = OnlineMembersHelper.getInstance().getMemberByUserId(exploreMemeberStatus.selectUserId);
                                if (onlineMember != null) {
                                    onlineMember.isCallPhoneIng = true;
                                    callOffHook(exploreMemeberStatus.selectUserId);
                                }
                        }
                    }
                }

            }
        }), 1500);

        //1.是否在引流
        OMAppApiProvider.getInstance().getLiveStreamStatus(LiveRoomInfoProvider.getInstance().liveId, new Observer<SelectOutliveStatusResponse>() {
            @Override
            public void onCompleted() {
                checkHostUp(0);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(SelectOutliveStatusResponse response) {
                if (response.isSuccess()) {
                    for (SelectOutLiveModel selectOutLiveModel : response.data) {
                        showOuterLive(selectOutLiveModel.pullFlvUrl, selectOutLiveModel.pullRtmpUrl, selectOutLiveModel.streamId, true);
                        mTRTCVideoViewLayout.muteGuestAudio(selectOutLiveModel.streamId, !OnlineMembersHelper.getInstance().getMemberByUserId(selectOutLiveModel.streamId).isOpenAudio);
                    }
                    OnlineMembersHelper.getInstance().initOutliveStatus(response.data);
                }
            }
        });

        // 获取当前直播间是否有正在展示的视频
        LiveRoomIOHelper.getVideoStatus(new Observer<DoumentVideoResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(DoumentVideoResponse baseResponse) {
                if (baseResponse.isSuccess()) {
                    // 有正在展示的视频
                    if ("0".equals(baseResponse.data.status) || "1".equals(baseResponse.data.status)) {
                        if (LiveRoomInfoProvider.getInstance().isHost()) {
                            // 若是主播重新从进入发现正在播放视频则先关闭视频
                            LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                                    LiveCustomMessageProvider.getDocumentVideoColoseData(BaseLiveAct.this, "", "0", "2"), false);
                            LiveRoomIOHelper.setVideoStatus("2", "", "0");
                        } else {
                            showDoumentVideo(baseResponse.data, true);
                        }
                    }
                }
            }
        });

        // 初始化获取已发布的活动列表
        OMAppApiProvider.getInstance().selectLivePromotion(LiveRoomInfoProvider.getInstance().liveId, new Observer<PublishQRListResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(PublishQRListResponse omBaseResponse) {
                if (omBaseResponse.isSuccess() && omBaseResponse.data != null && omBaseResponse.data.list != null) {
                    // 初始化右下角小轮播图
                    for(int i = 0; i < omBaseResponse.data.list.size(); i++){
                        CRMsgLivePublishQRAttachment attachment = new CRMsgLivePublishQRAttachment();
                        attachment.setId(omBaseResponse.data.list.get(i).id);
                        attachment.setName(omBaseResponse.data.list.get(i).name);
                        attachment.setPicBig(omBaseResponse.data.list.get(i).picBig);
                        attachment.setPicSmall(omBaseResponse.data.list.get(i).picSmall);
                        attachment.setPicShare(omBaseResponse.data.list.get(i).picShare);
                        attachment.setStatus(omBaseResponse.data.list.get(i).status);
                        attachment.setOpenid(omBaseResponse.data.list.get(i).openid);
                        attachment.setOprationStatus(omBaseResponse.data.list.get(i).oprationStatus);
                        attachment.setShareRemark(omBaseResponse.data.list.get(i).shareRemark);
                        attachment.setSortNum(omBaseResponse.data.list.get(i).sortNum);
                        attachment.setStatusStr(omBaseResponse.data.list.get(i).statusStr);
                        attachment.setQrType(omBaseResponse.data.list.get(i).type);
                        attachment.setUrl(omBaseResponse.data.list.get(i).url);
                        attachment.setWechatName(omBaseResponse.data.list.get(i).wechatName);
                        attachment.setWechatUrl(omBaseResponse.data.list.get(i).wechatUrl);
                        LiveRoomHelper.getInstance().receivedQrSmallList.add(attachment);
                    }
                    initQRBanner();
                }
            }
        });
    }


    /**
     * 需要监测是否需要上屏幕
     * 1.延时2s请求
     * 2.监测当前用户是否进入了
     * 3.用户进入了是否有一定的延时 超过200ms
     */
    private void checkHostUp(int checkIndex) {
        mHandler.postDelayed(() -> {
            //查询当前谁在主屏上 ： 同台的用户 & 引流的用户 都渲染完毕
            OMAppApiProvider.getInstance().mGetBigScreenUser(LiveRoomInfoProvider.getInstance().liveId, new Observer<OnlineFrameResponse>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onNext(OnlineFrameResponse response) {
                    //默认是主播在大屏，当数据异常时不做处理
                    if (response.isSuccess()) {
                        if (response.data != null) {
                            //该用户不是主播 并且 该用户在同台
                            if (!LiveRoomInfoProvider.getInstance().isHost(response.data.userId)) {
                                if (OnlineMembersHelper.getInstance().getMemberByUserId(response.data.userId) != null) {
                                    if (System.currentTimeMillis() - OnlineMembersHelper.getInstance().getMemberByUserId(response.data.userId).enterTime < 500) {
                                        mHandler.postDelayed(() -> mTRTCVideoViewLayout.bigDown(mTRTCVideoViewLayout.getCloudVideoViewByUseId(response.data.userId)), 500);
                                    } else {
                                        mTRTCVideoViewLayout.bigDown(mTRTCVideoViewLayout.getCloudVideoViewByUseId(response.data.userId));
                                    }
                                } else {
                                    if (checkIndex == 0) {
                                        checkHostUp(1);
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }, checkIndex == 0 ? 1000 : 500);
    }

    /**
     * 助手身份变化的时候回调
     */
    private void refreshHelperInfo() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "refreshHelperInfo");

        //0.刷新互动弹窗
        if (mLiveOperationPop != null) {
            mLiveOperationPop.refreshContainerHeight();
        }

        //1.判断是否需要申请同窗提示
        if (mApplyOnlineTipView == null) {
            mApplyOnlineTipView = new ApplyOnlineTipView(this, this);
            ((RelativeLayout) findViewById(R.id.rlv_apply_info)).addView(mApplyOnlineTipView.getView());
        }

        if (!LiveRoomInfoProvider.getInstance().isHostHelper()) {
            mApplyOnlineTipView.getView().setVisibility(GONE);
        }


        //2.同台数据的变化
        //  获取当前连麦人的音视频状态
        ExporeApiManager.refreshGuestMediaStatus(null);

        //  关闭当前同台弹窗<针对助手身份被取消>
        if (mTRTCVideoViewLayout != null) {
            mTRTCVideoViewLayout.hideMenuView();
        }

        //3.白板的身份状态修改
        if (mWhiteboardManager != null) {
            // 刷新手绘板
            mWhiteboardManager.refreshDrawpad();
            // 刷新文档演示
            if (findViewById(R.id.iv_fileshow_back).getVisibility() == VISIBLE
                && LiveRoomInfoProvider.getInstance().isGuest()) {
                // 首先把关闭白板按钮取消掉
                findViewById(R.id.iv_fileshow_back).setVisibility(GONE);
            }
            // 即使发起了，白板绘制的也要跟着变化
        }

        //5.当被取消助手身份的时候 如果当前在<选择引流、选择同台>需要退出当前页面
        if (!LiveRoomInfoProvider.getInstance().isHostHelper()) {
            if (ActivityStack.getActivityStack().currentActivity() instanceof OuterLiveListAct) {
                ActivityStack.getActivityStack().popAcitivityAndReomve(ActivityStack.getActivityStack().currentActivity());
            }

            if (ActivityStack.getActivityStack().currentActivity() instanceof GuestListForInveteAct) {
                ActivityStack.getActivityStack().popAcitivityAndReomve(ActivityStack.getActivityStack().currentActivity());
            }
        }

        LRDialogController.getInstance().refreshHelperMsg();
        // 若主播在主屏幕，"开启画面"的按钮根据身份展示
        mTRTCVideoViewLayout.refreshHostUI();
    }


    /**
     * ====== 音频、视频控制
     */
    //控制外部App音频
    private void controlOtherAppAudio() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            audioManager.requestAudioFocus(null, AudioManager.STREAM_MUSIC,
                    AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
        }
    }

    //控制摄像头
    private void controlCaremaVisible() {
        if (mLiveOperationPop != null) {
            mLiveOperationPop.controlCarema();
        }
    }

    private HeadsetPlugReceiver headsetPlugReceiver;

    private void registerHeadsetPlugReceiver() {
        headsetPlugReceiver = new HeadsetPlugReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.HEADSET_PLUG");
        registerReceiver(headsetPlugReceiver, filter);
    }


    class HeadsetPlugReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra("state")) {
                if (intent.getIntExtra("state", 0) == 0) {
                    mTrtcCloud.setAudioRoute(TXLiveConstants.AUDIO_ROUTE_SPEAKER);
                } else if (intent.getIntExtra("state", 0) == 1) {
                    mTrtcCloud.setAudioRoute(TXLiveConstants.AUDIO_ROUTE_RECEIVER);
                }
            }
        }

    }

    private int resumeCount = 1;

    @Override
    public void onActivityResumed(Activity activity) {
        LogUtil.i(LiveLogTag.IOLiveTag, "onActivityResumed");

        //如果正在演示文档，当跳出App时不做处理
        if (LiveRoomInfoProvider.getInstance().isShowFile) {
            return;
        }

//        if (hasFinished) {
//            destoryTRCC();
//            return;
//        }

        resumeCount++;
        if (resumeCount > 0) {
//            if (mTRTCVideoViewLayout != null) {
//                mTRTCVideoViewLayout.onResume();
//            }

            LogUtil.i(LiveLogTag.BaseLiveTag, "onActivityResumed_isHostVideoOn ： " + OnlineMembersHelper.getInstance().isHostVideoOn);
            if (!OnlineMembersHelper.getInstance().isHostVideoOn) {
                return;
            }

            if (mTrtcCloud != null) {
                if (LiveRoomInfoProvider.getInstance().isHost() && !LiveRoomInfoProvider.getInstance().hasTemplateLeave) {
                    LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "muteLocalVideo--onActivityResume:" + false);
                    mTrtcCloud.muteLocalVideo(false);
                }
//                mTrtcCloud.muteAllRemoteAudio(false);
            }
        }

        controlOtherAppAudio();
    }

    @Override
    public void onActivityPaused(Activity activity) {
        //如果正在演示文档，当跳出App时不做处理
        if (LiveRoomInfoProvider.getInstance().isShowFile) {
            return;
        }

//        if (hasFinished) {
//            destoryTRCC();
//            return;
//        }

        resumeCount--;
        LogUtil.i(LiveLogTag.IOLiveTag, "onActivityPaused");
        mHandler.postDelayed(() -> {
            LogUtil.i(LiveLogTag.BaseLiveTag, "onActivityPaused_resumeCount ： " + resumeCount);
            if (resumeCount == 0) {
                if (isFinishing()) {
                    return;
                }

                if (mTRTCVideoViewLayout == null) {
                    return;
                }

//                mTRTCVideoViewLayout.onPause();

                LogUtil.i(LiveLogTag.BaseLiveTag, "onActivityPaused_isHostVideoOn ： " + OnlineMembersHelper.getInstance().isHostVideoOn);
                if (!OnlineMembersHelper.getInstance().isHostVideoOn) {
                    return;
                }
                if (LiveRoomInfoProvider.getInstance().isHost() && !LiveRoomInfoProvider.getInstance().hasTemplateLeave) {
                    mTrtcCloud.muteLocalVideo(true);
                    LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "muteLocalVideo--onActivityPause:" + false);
                }
//                mTrtcCloud.muteAllRemoteAudio(true);
            }
        }, 400);
    }


    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }


    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }


}
