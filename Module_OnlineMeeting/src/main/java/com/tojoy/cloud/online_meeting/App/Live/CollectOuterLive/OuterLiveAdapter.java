package com.tojoy.cloud.online_meeting.App.Live.CollectOuterLive;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.model.live.OnlineMeetingHomeGridModel;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;

public class OuterLiveAdapter extends BaseRecyclerViewAdapter<OnlineMeetingHomeGridModel> {

    OuterLiveAdapter(@NonNull Context context, @NonNull List<OnlineMeetingHomeGridModel> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull OnlineMeetingHomeGridModel data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_outlink_room_layout;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new OuterLiveRoomViewHolder(context, getLayoutResId(viewType));
    }


    public class OuterLiveRoomViewHolder extends BaseRecyclerViewHolder<OnlineMeetingHomeGridModel> {

        RelativeLayout mContainer;
        TextView mUserStatus;
        TextView mUserStatusIng;
        RelativeLayout mUserStatusContainer;
        RelativeLayout mUserStatusIngContainer;
        ImageView mCoverImg;

        TextView mRoomIdTv;
        TextView mRoomIdType;
        TextView mRoomTitleTv;

        int width;

        OuterLiveRoomViewHolder(Context context, int layoutResId) {
            super(context, layoutResId);
            width = (AppUtils.getWidth(context) - AppUtils.dip2px(context, 32)) / 2;
            mContainer = (RelativeLayout) findViewById(R.id.rlv_container);
            mCoverImg = (ImageView) findViewById(R.id.iv_room_cover);
            mRoomIdTv = (TextView) findViewById(R.id.tv_room_id);
            mUserStatusContainer = (RelativeLayout) findViewById(R.id.rlv_userlive);
            mUserStatusIngContainer = (RelativeLayout) findViewById(R.id.rlv_userlive_ing);
            mUserStatus = (TextView) findViewById(R.id.user_status);
            mUserStatusIng = (TextView) findViewById(R.id.user_status_ing);
            mRoomIdType = (TextView) findViewById(R.id.tv_room_type);
            mRoomTitleTv = (TextView) findViewById(R.id.tv_title);
            mCoverImg.setLayoutParams(new RelativeLayout.LayoutParams(width, width));
            findViewById(R.id.rlv_userlive_container).setLayoutParams(new RelativeLayout.LayoutParams(width, width));
        }

        @Override
        public void onBindData(OnlineMeetingHomeGridModel data, int position) {
            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(width, width + AppUtils.dip2px(context, 35));
            if (position < 2) {
                lps.setMargins(AppUtils.dip2px(context, 4), AppUtils.dip2px(context, 18), AppUtils.dip2px(context, 4), AppUtils.dip2px(context, 4));
            } else {
                lps.setMargins(AppUtils.dip2px(context, 4), AppUtils.dip2px(context, 4), AppUtils.dip2px(context, 4), AppUtils.dip2px(context, 4));
            }
            mContainer.setLayoutParams(new RelativeLayout.LayoutParams(lps));


            String ossURL = OSSConfig.getOSSURLedStr(data.getOutLiveCover());
            ImageLoaderManager.INSTANCE.loadCoverSizedImage(context,
                    mCoverImg,
                    ossURL,
                    10, width, width, 0);

            mRoomIdTv.setText("ID" + data.roomCode);
            mRoomTitleTv.setText(data.title);

            if (data.isOfficialLive()) {
                mRoomIdType.setVisibility(View.VISIBLE);
            } else {
                mRoomIdType.setVisibility(View.GONE);
            }

            if ("1".equals(data.isUse)) {
                mUserStatusContainer.setVisibility(View.INVISIBLE);
                mUserStatusIngContainer.setVisibility(View.VISIBLE);
                mUserStatusIngContainer.setBackgroundResource(R.drawable.bg_has_use);
                mUserStatusIng.setText("使用中");
            } else {
                mUserStatusContainer.setVisibility(View.VISIBLE);
                mUserStatusIngContainer.setVisibility(View.INVISIBLE);
                mUserStatusContainer.setBackgroundResource(R.drawable.bg_liveend_tip1);
                mUserStatus.setText("开始引用画面");
            }
        }
    }
}