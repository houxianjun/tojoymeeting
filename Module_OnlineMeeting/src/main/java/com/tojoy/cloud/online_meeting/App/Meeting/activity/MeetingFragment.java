package com.tojoy.cloud.online_meeting.App.Meeting.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.StatusbarUtils;
import com.tojoy.cloud.online_meeting.App.Meeting.adapter.MeetingPageAdapter;
import com.tojoy.tjoybaselib.model.home.MeetingHomeModelResponse;
import com.tojoy.cloud.online_meeting.App.Meeting.view.MeetingHeaderView;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.App.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 首页会议页面
 */
public class MeetingFragment extends BaseFragment {
    private TJRecyclerView mRecyclerView;
    private MeetingHeaderView mHeaderView;
    private View mStatusBarBgView;

    // 列表数据
    private List<String> mDatas = new ArrayList<>();

    private int firstHeight;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_meeting, container, false);
        }
        isCreateView = true;
        return mView;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initUI() {
        StatusbarUtils.enableTranslucentStatusbar(getActivity());
        mStatusBarBgView = mView.findViewById(R.id.view_status_bar_bg);
        int statusBarHeight = AppUtils.getStatusBarHeight(getActivity());
        RelativeLayout.LayoutParams mStatusBarBgParams = (RelativeLayout.LayoutParams) mStatusBarBgView.getLayoutParams();
        mStatusBarBgParams.height = statusBarHeight;
        mStatusBarBgView.setLayoutParams(mStatusBarBgParams);

        mRecyclerView = mView.findViewById(R.id.reycler_view);
        mRecyclerView.getRecyclerView().setVerticalScrollBarEnabled(false);
        mRecyclerView.getRecyclerView().setHorizontalScrollBarEnabled(false);

        MeetingPageAdapter mAdapter = new MeetingPageAdapter(getContext(), mDatas);
        mHeaderView = new MeetingHeaderView(getActivity());
        mRecyclerView.addHeaderView(mHeaderView.getView());
        mRecyclerView.setAdapter(mAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.getRecyclerView().setLayoutManager(linearLayoutManager);
        mRecyclerView.hideFooterView();
        mRecyclerView.autoRefresh();

        firstHeight = AppUtils.dip2px(getContext(), 80);

        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                requestData();
            }

            @Override
            public void onLoadMore() {
            }
        });
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                // 第一个可见Item的位置
                int position = layoutManager.findFirstVisibleItemPosition();
                // 是第一项才去渐变
                if (position == 0) {
                    // 注意此操作如果第一项划出屏幕外,拿到的是空的，所以必须是position是0的时候才能调用
                    View firstView = layoutManager.findViewByPosition(position);
                    // 距离顶部的距离，是负数，也就是说-top就是它向上滑动的距离
                    int scrollY = -firstView.getTop();
                    // 要在它滑到二分之一的时候去渐变
                    int changeHeight = firstHeight / 2;
                    // 小于头部高度一半隐藏标题栏
                    if (scrollY <= changeHeight) {
                        mStatusBarBgView.setVisibility(View.GONE);
                    } else {
                        mStatusBarBgView.setVisibility(View.VISIBLE);
                        // 从高度的一半开始算透明度，也就是说移动到头部Item的中部，透明度从0开始计算
                        float alpha = (float) (scrollY - changeHeight) / changeHeight;
                        mStatusBarBgView.setAlpha(alpha);
                    }
                    // 其他的时候就设置都可见，透明度是1
                } else {
                    mStatusBarBgView.setVisibility(View.VISIBLE);
                    mStatusBarBgView.setAlpha(1);
                }
            }
        });

    }


    /**
     * 请求接口数据
     */
    private void requestData() {

        OMAppApiProvider.getInstance().getMainPageInfo(new Observer<MeetingHomeModelResponse>() {
            @Override
            public void onCompleted() {
                mRecyclerView.setRefreshing(false);
            }

            @Override
            public void onError(Throwable e) {

                mRecyclerView.setRefreshing(false);
                Toasty.normal(getContext(), "网络异常，请下拉刷新").show();
            }

            @Override
            public void onNext(MeetingHomeModelResponse homeHotLiveListResponse) {
                if (homeHotLiveListResponse != null && homeHotLiveListResponse.isSuccess() && homeHotLiveListResponse.data != null) {
                    mRecyclerView.setRefreshing(false);

                    mHeaderView.setBannerData(homeHotLiveListResponse.data.banner);

                    mHeaderView.setH5ResourceData(homeHotLiveListResponse.data.topic);
                    //填充热门企业数据
                    mHeaderView.setHotCompanyData(homeHotLiveListResponse.data.hot);
                    //填充招商推荐数据
                    mHeaderView.setRecommendInvestmentData(homeHotLiveListResponse.data.recommend);
                } else {
                    if (homeHotLiveListResponse != null) {
                        Toasty.normal(getContext(), homeHotLiveListResponse.msg + "，请下拉刷新").show();
                    }
                }
            }
        });
    }
}
