package com.tojoy.cloud.online_meeting.App.Live.Interaction;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishProjectAttachment;
import com.netease.nim.uikit.business.liveroom.FileShowCallback;
import com.netease.nim.uikit.business.liveroom.LiveRoomHelper;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.tojoy.cloud.online_meeting.App.Live.MemberList.LiveMemberFragment;
import com.tojoy.cloud.online_meeting.App.Live.MemberList.LiveRoomMemverListDialog;
import com.tojoy.cloud.online_meeting.App.Live.MemberList.SecurityEditText;
import com.tojoy.cloud.online_meeting.App.UserCenter.adapter.LiveRoomMemberListAdapter;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.live.GoodsResponse;
import com.tojoy.tjoybaselib.model.user.MemberListResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigCacheManager;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigConstantKey;
import com.tojoy.tjoybaselib.ui.TJTablayout.SlidingTabLayout;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.ui.editView.ClearEmojInputFilter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.net.NetworkUtil;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.KeyboardControlUtil;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * @author  houxianjun
 * @Date  2020/09/07
 * 发布商品/代播商品列表
 *
 */

@SuppressLint("ValidFragment")
public class LaunchProductTJDialog extends DialogFragment  {

    private UI mContext;
    private View mView;

    private SlidingTabLayout mTabLayout;
    private ViewPager mViewPager;
    private String[] mTitles = new String[2];
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private LauchProductListFragment fragment1;
    private LauchProductListFragment fragment2;
    private GoodsResponse.DataBean.RecordsBean selectBean=null;
    private DialogInterface.OnDismissListener dismissListener;
    public LaunchProductTJDialog(Context context) {
        mContext = (UI) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NORMAL, R.style.LiveDialogMemberList);
        setCancelable(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_launch_product_list, null);
        initView(mView);
        return mView;
    }

    @Override
    public void onResume() {

        Window win = getDialog().getWindow();
        WindowManager.LayoutParams lp = win.getAttributes();
        win.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.windowAnimations = R.style.BottomInAndOutStyle;
        lp.gravity = Gravity.BOTTOM;
        win.setAttributes(lp);
        getDialog().setCanceledOnTouchOutside(true);

        super.onResume();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView(View view) {
            initOwner();
    }



    private void initOwner() {
        ImageView closeDialogIV = mView.findViewById(R.id.iv_close);
        fragment1 = new LauchProductListFragment("1", this);
        fragment2 = new LauchProductListFragment("2", this);
        mFragments.add(fragment1);
        mFragments.add(fragment2);

        mTabLayout = mView.findViewById(R.id.tabLayot);
        mViewPager = mView.findViewById(R.id.viewPager);
        mTabLayout.setTabSpaceEqual(true);
        mViewPager.setOffscreenPageLimit(2);

        ViewGroup.LayoutParams params = mViewPager.getLayoutParams();
        params.height = (int) (AppUtils.getHeight(mContext) * 0.55);
        mViewPager.setLayoutParams(params);

        FragmentPagerAdapter mViewPagerAdapter = new FragmentPagerAdapter(getChildFragmentManager()) {

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                return mTitles[position];
            }

            @Override
            public int getCount() {
                return mTitles.length;
            }

            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }
        };
        mViewPager.setAdapter(mViewPagerAdapter);

        mTitles[0] = "企业商品";
        mTitles[1] = "代播商品";
        mTabLayout.setViewPager(mViewPager, mTitles);
        closeDialogIV.setOnClickListener(v -> LaunchProductTJDialog.this.dismiss());

        mView.findViewById(R.id.tvPublish).setOnClickListener(view -> {
            if (getSelectBean()!=null) {
                GoodsResponse.DataBean.RecordsBean bean = getSelectBean();
                OMAppApiProvider.getInstance().publishProject(LiveRoomInfoProvider.getInstance().liveId,
                        LiveRoomInfoProvider.getInstance().imRoomId, bean.id, bean.name, bean.liveRoomCoverImgUrl,
                        bean.subhead, bean.attribute, bean.sellStoreId,bean.agentCompanyCode,bean.agentId,bean.agentLive ,new Observer<OMBaseResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                Toasty.normal(mContext, "网络异常，请检查网络").show();
                            }

                            @Override
                            public void onNext(OMBaseResponse response) {
                                if (response.isSuccess()) {
                                    if (LiveRoomHelper.getInstance().productListBtn != null) {
                                        LiveRoomHelper.getInstance().productListBtn.setVisibility(View.VISIBLE);
                                    }
                                    Toasty.normal(mContext, "发布成功").show();
                                    LaunchProductTJDialog.this.dismiss();
                                } else {
                                    Toasty.normal(mContext, response.msg).show();
                                }
                            }
                        });
            }
            else{

                Toasty.normal(mContext, "请选择商品").show();

            }
        });
    }


    public void refreshTabTitle(int index, String title) {
        mTitles[index] = title;
        if (!TextUtils.isEmpty(mTitles[0]) && !TextUtils.isEmpty(mTitles[1])) {
            new Handler().postDelayed(() -> mTabLayout.setViewPager(mViewPager, mTitles), 300);
        }
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener listener) {
        this.dismissListener = listener;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (dismissListener != null) {
            dismissListener.onDismiss(dialog);
        }
        super.onDismiss(dialog);
    }
    public boolean isShowing() {
        return isVisible();
    }

    public void clearOtherSelectStatus(int type){
        //0 发布商品 1 代播商品
        switch (type){
            case 0:
                fragment1.clearSelect();
                break;
            case 1:
                fragment2.clearSelect();
                break;
            default:
                break;
        }
    }

    public GoodsResponse.DataBean.RecordsBean getSelectBean() {
        return selectBean;
    }

    public void setSelectBean(GoodsResponse.DataBean.RecordsBean selectBean) {
        this.selectBean = selectBean;
    }
}
