package com.tojoy.cloud.online_meeting.App.Live.ExporeOnline;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.cloud.online_meeting.App.Live.Live.Model.OnlineMember;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.OnlineMembersHelper;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;
import com.tojoy.cloud.online_meeting.R;

public class ExploreOperationMenu {

    private View mView;
    private Activity mContext;
    private int currentType;
    private TextView mAudioSwitchTv;
    private TextView mVideoSwitchTv;

    public ExploreOperationMenu(Context context) {
        this.mContext = (Activity) context;
    }

    public View getView() {
        if (mView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mView = aLayoutInflater.inflate(R.layout.menu_explore_operation, null);
            this.mView.setVisibility(View.GONE);
        }

        mAudioSwitchTv = mView.findViewById(R.id.tv_audio_switch);
        mVideoSwitchTv = mView.findViewById(R.id.tv_video_switch);

        initUI();
        return mView;
    }

    public void refreshUIByUserId(String userId) {
        if ((!LiveRoomInfoProvider.getInstance().isGuest()) && LiveRoomInfoProvider.getInstance().isHost(userId)) {
            //助手 & 主播 点下屏后的主播
            mView.findViewById(R.id.tv_switch_screen).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.tv_stop_invite).setVisibility(View.GONE);
            // 2019.11.28需求更改：官方大会显示语音关闭
            if (LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                mView.findViewById(R.id.tv_video_switch).setVisibility(View.GONE);
                mView.findViewById(R.id.tv_audio_switch).setVisibility(View.VISIBLE);
            } else {
                mView.findViewById(R.id.tv_video_switch).setVisibility(View.VISIBLE);
                mView.findViewById(R.id.tv_audio_switch).setVisibility(View.VISIBLE);
            }
        } else if (BaseUserInfoCache.isMySelf(mContext, userId)) {
            mView.findViewById(R.id.tv_switch_screen).setVisibility(View.GONE);
            mView.findViewById(R.id.tv_stop_invite).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.tv_audio_switch).setVisibility(View.GONE);
            mView.findViewById(R.id.tv_video_switch).setVisibility(View.GONE);
        } else {
            mView.findViewById(R.id.tv_switch_screen).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.tv_stop_invite).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.tv_audio_switch).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.tv_video_switch).setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(userId) && userId.length() > 10) {
            currentType = 1;
            ((TextView) mView.findViewById(R.id.tv_stop_invite)).setText("关闭画面");
            ((TextView) mView.findViewById(R.id.tv_switch_screen)).setText("放大画面");
            mView.findViewById(R.id.tv_video_switch).setVisibility(View.GONE);
        } else {
            ((TextView) mView.findViewById(R.id.tv_stop_invite)).setText("取消互动");
            ((TextView) mView.findViewById(R.id.tv_switch_screen)).setText("设为主讲");
            currentType = 0;
        }

        if (LiveRoomInfoProvider.getInstance().isHost(userId)) {
            if (LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                mAudioSwitchTv.setText(LiveRoomInfoProvider.getInstance().isAudioOpen ? "语音关闭" : "语音开启");
            } else {
                mVideoSwitchTv.setText(OnlineMembersHelper.getInstance().isHostVideoOn ? "关闭画面" : "开启画面");
                mAudioSwitchTv.setText(OnlineMembersHelper.getInstance().isHostAudioOn ? "语音关闭" : "语音开启");
            }
        } else {
            OnlineMember onlineMember = OnlineMembersHelper.getInstance().getMemberByUserId(userId);
            if (onlineMember != null) {
                mAudioSwitchTv.setText(onlineMember.isOpenAudio ? "语音关闭" : "语音开启");
                mVideoSwitchTv.setText(onlineMember.isOpenVideo ? "关闭画面" : "开启画面");
            }
        }
    }

    private void initUI() {
        mView.findViewById(R.id.tv_switch_screen).setOnClickListener(v -> {
            LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "mSwitchScreenClick");
            mExploreOperationCallback.doSwitchScreen();
        });
        mView.findViewById(R.id.tv_stop_invite).setOnClickListener(v -> {
            LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "mStopInviteClick");
            if (currentType == 1) {
                mExploreOperationCallback.doStopOutlive();
            } else {
                mExploreOperationCallback.doStopInvite();
            }
        });

        mAudioSwitchTv.setOnClickListener(v -> {
            LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "mAudioSwitchTvClick");
            if ("语音关闭".equals(mAudioSwitchTv.getText().toString())) {
                mExploreOperationCallback.doStopAudio();
            } else {
                mExploreOperationCallback.doStartAudio();
            }
        });

        mVideoSwitchTv.setOnClickListener(v -> {
            LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "mVideoSwitchTvClick");
            if ("关闭画面".equals(mVideoSwitchTv.getText().toString())) {
                mExploreOperationCallback.doStopVideo();
            } else {
                mExploreOperationCallback.doStartVideo();
            }
        });
    }

    public interface ExploreOperationCallback {
        void doSwitchScreen();

        void doStopInvite();

        void doStopAudio();

        void doStartAudio();

        void doStopVideo();

        void doStartVideo();

        void doStopOutlive();
    }

    private ExploreOperationCallback mExploreOperationCallback;

    public void setmExploreOperationCallback(ExploreOperationCallback mExploreOperationCallback) {
        this.mExploreOperationCallback = mExploreOperationCallback;
    }
}
