package com.tojoy.cloud.online_meeting.App.Live.Live.View;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tojoy.tjoybaselib.model.live.ShortLeaveListModel;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.dialog.PickerViewAnimateUtil;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

public class ShortLivePop {

    private Context context;
    protected ViewGroup contentContainer;
    private Animation outAnim;
    private Animation inAnim;
    private boolean isAnim = true;
    private boolean isShowing;
    private int gravity = Gravity.BOTTOM;
    public ViewGroup decorView;
    private ViewGroup rootView;
    private boolean dismissing;

    private RecyclerView mRecyclerView;
    private ShortLeaveAdapter mAdapter;
    private List<ShortLeaveListModel.DataObjBean.ListBean> mDatas;
    private OperationCallback mOperationCallback;

    public ShortLivePop(Context context, OperationCallback operationCallback) {
        this.context = context;
        this.mOperationCallback = operationCallback;
        init();
        initViews();
    }

    protected void init() {
        inAnim = getInAnimation();
        outAnim = getOutAnimation();
    }

    private void initViews() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        //decorView是activity的根View
        if (decorView == null) {
            decorView = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        }

        //将控件添加到decorView中
        rootView = (ViewGroup) layoutInflater.inflate(R.layout.dialog_live_short_leave, decorView, false);
        rootView.setLayoutParams(new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        ));
        rootView.setBackgroundColor(context.getResources().getColor(R.color.colorTranslucent));

        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        rootView.findViewById(R.id.container_base).setLayoutParams(lps);
        setKeyBackCancelable(true);

        contentContainer = rootView.findViewById(R.id.content_container);

        mRecyclerView = rootView.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mAdapter = new ShortLeaveAdapter();
        mRecyclerView.setAdapter(mAdapter);

        rootView.findViewById(R.id.tvCancle).setOnClickListener(view -> dismiss());

        setOutSideCancelable(true);
    }

    public void refreshLeaveReason() {
        OMAppApiProvider.getInstance().getShortLeaveTextLeave(new Observer<ShortLeaveListModel>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(ShortLeaveListModel response) {
                if (response.isSuccess()) {
                    mDatas = response.data.list;
                    mAdapter.notifyDataSetChanged();
                    show();
                    mOperationCallback.showBottomView(false);
                }
            }
        });
    }


    private class ShortLeaveAdapter extends RecyclerView.Adapter {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.item_short_leave, parent, false);
            return new ShortLeaveVH(itemView);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ShortLeaveVH projectVH = (ShortLeaveVH) holder;
            projectVH.bindData(position);
        }

        @Override
        public int getItemCount() {
            return mDatas == null ? 0 : mDatas.size();
        }
    }

    private class ShortLeaveVH extends RecyclerView.ViewHolder {

        private TextView tvContent;
        private ImageView ivAdd;
        private View line;
        private View item;

        ShortLeaveVH(View itemView) {
            super(itemView);
            item = itemView;
            tvContent = itemView.findViewById(R.id.tvContent);
            ivAdd = itemView.findViewById(R.id.ivAdd);
            line = itemView.findViewById(R.id.line);
        }

        void bindData(int position) {

            ShortLeaveListModel.DataObjBean.ListBean data = mDatas.get(position);

            tvContent.setText(data.pauseMsg);
            line.setVisibility(position == mDatas.size() - 1 ? View.GONE : View.VISIBLE);

            item.setOnClickListener(view -> {
                OMAppApiProvider.getInstance().commitShortLeaveInfo(LiveRoomInfoProvider.getInstance().liveId, data.pauseMsg,
                        data.weight + "", new Observer<OMBaseResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                Toasty.normal(context, "网络错误,请检查网络").show();
                                if (mShortLeaveCallback != null) {
                                    mShortLeaveCallback.onShortLevaeFail();
                                }
                            }

                            @Override
                            public void onNext(OMBaseResponse response) {
                                if (response.isSuccess()) {
                                    if (mShortLeaveCallback != null) {
                                        mShortLeaveCallback.onShortLeave(tvContent.getText().toString().trim(), data.weight + "");
                                    }
                                    dismiss();
                                } else {
                                    Toasty.normal(context, response.msg, Toast.LENGTH_SHORT).show();
                                    if (mShortLeaveCallback != null) {
                                        mShortLeaveCallback.onShortLevaeFail();
                                    }
                                }
                            }
                        });
            });
        }
    }

    public interface ShortLeaveCallback {
        /**
         * @param showText 暂离时选择展示的文本
         */
        void onShortLeave(String showText, String time);

        void onShortLevaeFail();
    }

    private ShortLeaveCallback mShortLeaveCallback;

    public void setmShortLeaveCallback(ShortLeaveCallback mShortLeaveCallback) {
        this.mShortLeaveCallback = mShortLeaveCallback;
    }


    /**
     * @param v      (是通过哪个View弹出的)
     * @param isAnim 是否显示动画效果
     */
    public void show(View v, boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(View v) {
        show();
    }

    public void show() {
        if (isShowing()) {
            return;
        }
        isShowing = true;
        onAttached(rootView);
        rootView.requestFocus();
    }

    private void onAttached(View view) {
        decorView.addView(view);
        if (isAnim) {
            contentContainer.startAnimation(inAnim);
        }
    }

    public boolean isShowing() {
        return rootView.getParent() != null && isShowing;
    }


    public void dismiss() {
        if (dismissing && !isShowing()) {
            return;
        }
        if (isAnim) {
            //消失动画
            outAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    dismissImmediately();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            contentContainer.startAnimation(outAnim);
        } else {
            dismissImmediately();
        }
        dismissing = true;
    }

    public void dismissImmediately() {
        decorView.post(() -> {
            //从根视图移除
            decorView.removeView(rootView);
            isShowing = false;
            dismissing = false;
            mOperationCallback.showBottomView(true);
        });

    }

    public Animation getInAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, true);
        return AnimationUtils.loadAnimation(context, res);
    }

    public Animation getOutAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, false);
        return AnimationUtils.loadAnimation(context, res);
    }

    public void setKeyBackCancelable(boolean isCancelable) {
        ViewGroup View;
        View = rootView;
        View.setFocusable(isCancelable);
        View.setFocusableInTouchMode(isCancelable);
        if (isCancelable) {
            View.setOnKeyListener(onKeyBackListener);
        } else {
            View.setOnKeyListener(null);
        }
    }

    private View.OnKeyListener onKeyBackListener = (v, keyCode, event) -> {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == MotionEvent.ACTION_DOWN
                && isShowing()) {
            dismiss();
            mOperationCallback.showBottomView(true);
            return true;
        }
        return false;
    };

    /**
     * Called when the user touch on black overlay in order to dismiss the dialog
     */
    @SuppressLint("ClickableViewAccessibility")
    private final View.OnTouchListener onCancelableTouchListener = (v, event) -> {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            dismiss();
        }
        return false;
    };

    private void setOutSideCancelable(boolean isCancelable) {
        if (rootView != null) {
            View view = rootView.findViewById(com.netease.nim.uikit.R.id.outmost_container);

            if (isCancelable) {
                view.setOnTouchListener(onCancelableTouchListener);
            } else {
                view.setOnTouchListener(null);
            }
        }

    }
}
