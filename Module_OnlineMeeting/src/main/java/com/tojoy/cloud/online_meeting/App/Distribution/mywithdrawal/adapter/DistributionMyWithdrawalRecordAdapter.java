package com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.model.home.MineDistributionChannelRecordResponse;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;

import java.util.List;

public class DistributionMyWithdrawalRecordAdapter extends BaseRecyclerViewAdapter<MineDistributionChannelRecordResponse.ChannelModel> {
    Context mcontext;

    public DistributionMyWithdrawalRecordAdapter(@NonNull Context context, @NonNull List<MineDistributionChannelRecordResponse.ChannelModel> datas) {
        super(context, datas);
        this.mcontext = context;
    }

    @Override
    protected int getViewType(int position, @NonNull MineDistributionChannelRecordResponse.ChannelModel data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_distribution_mywithdrawal_record;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new MyRecordVH(this.mcontext, parent, getLayoutResId(viewType));
    }

    private static class MyRecordVH extends BaseRecyclerViewHolder<MineDistributionChannelRecordResponse.ChannelModel> {
        private TextView mWithdrawalChannelNameTv;
        private TextView mWithdrawalChannelTv;
        Context context;
        String mtype;

        public void setType(String distributionType) {
            this.mtype = distributionType;
        }

        public MyRecordVH(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            this.context = context;
            bindViews();
        }

        private void bindViews() {
            mWithdrawalChannelNameTv = itemView.findViewById(R.id.tv_withdrawal_channel_name);
            mWithdrawalChannelTv = itemView.findViewById(R.id.tv_withdrawal_channel);
        }

        @Override
        public void onBindData(MineDistributionChannelRecordResponse.ChannelModel data, int position) {
            mWithdrawalChannelNameTv.setText(data.withdrawalName);
            mWithdrawalChannelTv.setText(data.withdrawalAccountNo);
        }
    }
}
