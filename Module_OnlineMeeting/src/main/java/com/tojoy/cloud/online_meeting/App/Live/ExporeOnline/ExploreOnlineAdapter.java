package com.tojoy.cloud.online_meeting.App.Live.ExporeOnline;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.model.live.ExploreOnlineModel;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.imageManager.YuanJiaoImageView;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;


public class ExploreOnlineAdapter extends BaseRecyclerViewAdapter<ExploreOnlineModel> {

    ExploreOnlineAdapter(@NonNull Context context, @NonNull List<ExploreOnlineModel> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull ExploreOnlineModel data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_explore_selecte_member;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new ChatMemberViewHolder(context, getLayoutResId(viewType));
    }


    public static class ChatMemberViewHolder extends BaseRecyclerViewHolder<ExploreOnlineModel> {

        TextView mNickNameLable;
        TextView mCompanyLable;
        TextView mJobLable;
        ImageView mSelectStatusIv;
        YuanJiaoImageView mHeaderIv;

        ChatMemberViewHolder(Context context, int layoutResId) {
            super(context, layoutResId);
            mHeaderIv = itemView.findViewById(R.id.ivHead);
            mNickNameLable = itemView.findViewById(R.id.tv_nickname);
            mJobLable = itemView.findViewById(R.id.tv_jobLevel);
            mCompanyLable = itemView.findViewById(R.id.tv_company);
            mSelectStatusIv = itemView.findViewById(R.id.iv_selected);
        }

        @Override
        public void onBindData(ExploreOnlineModel data, int position) {
            ImageLoaderManager.INSTANCE.loadHeadImage(getContext(), mHeaderIv, OSSConfig.getOSSURLedStr(data.userAvatar),
                    R.drawable.icon_dorecord);

            mNickNameLable.setText(data.userName);
            mJobLable.setText(data.job);
            mCompanyLable.setText(data.company);

            if (data.selectedSratus == 0) {
                mSelectStatusIv.setBackgroundResource(R.drawable.icon_no_select);
            } else if (data.selectedSratus == 1) {
                mSelectStatusIv.setBackgroundResource(R.drawable.icon_selected);
            } else if (data.selectedSratus == 2) {
                mSelectStatusIv.setBackgroundResource(R.drawable.icon_has_selected);
            }
        }

    }
}