package com.tojoy.cloud.online_meeting.App.Live.Interaction;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.business.liveroom.LiveRoomHelper;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.model.live.GoodsResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * Created by luuzhu on 2019/4/20.
 * 发起成交
 */

public class StartDealDialog {

    private List<GoodsResponse.DataBean.RecordsBean> mDatas;
    private Dialog mDialog;
    private BaseLiveAct mContext;
    private View mView;
    private TJRecyclerView mRecyclerView;
    private RelativeLayout mEmptyTip;
    private DealListAdapter mAdapter;
    private int selectIndex = -1;

    private int mPage = 1;

    public StartDealDialog(Context context) {
        mContext = (BaseLiveAct) context;
        initUI();
    }

    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_live_deal, null);
        initView();
        getData();
    }

    private void getData() {
        OMAppApiProvider.getInstance().getLiveRoomProductList(LiveRoomInfoProvider.getInstance().otherCompanyCode, null, mPage, new Observer<GoodsResponse>() {
            @Override
            public void onCompleted() {
                mContext.dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(mContext, "网络错误，请检查网络").show();
                mContext.showBottomView(true);
            }

            @Override
            public void onNext(GoodsResponse response) {
                if (response.isSuccess()) {
                    if (mPage == 1) {
                        mDatas = response.data.records;
                    } else {
                        mDatas.addAll(response.data.records);
                    }
                    mAdapter.notifyDataSetChanged();
                    mRecyclerView.refreshLoadMoreView(mPage, response.data.records.size());
                } else {
                    mDialog.dismiss();
                    mContext.showBottomView(true);
                    Toasty.normal(mContext, response.msg).show();
                }
                mEmptyTip.setVisibility(mDatas.isEmpty() ? View.VISIBLE : View.GONE);
            }
        });
    }

    public void show() {

        mDialog = new Dialog(mContext, R.style.LiveDialogFragmentEnterExitAnim);
        mDialog.setContentView(mView);

        Window win = mDialog.getWindow();
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.windowAnimations = R.style.BottomInAndOutStyle;
        lp.gravity = Gravity.BOTTOM;
        win.setAttributes(lp);

        mDialog.setCanceledOnTouchOutside(true);
        try {
            mDialog.show();
            mDialog.setOnKeyListener((dialog, keyCode, event) -> true);
        } catch (Exception e) {

        }
    }

    private void initView() {
        ImageView closeIv = mView.findViewById(R.id.iv_close);
        mRecyclerView = mView.findViewById(R.id.recyclerView);
        mEmptyTip = mView.findViewById(R.id.rlv_empty);
        mRecyclerView.setLoadMoreView();
        mRecyclerView.setRefreshable(false);
        mAdapter = new DealListAdapter();
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
            }

            @Override
            public void onLoadMore() {
                mPage++;
                getData();
            }
        });


        mView.findViewById(R.id.tvPublish).setOnClickListener(view -> {
            if (selectIndex != -1) {
                GoodsResponse.DataBean.RecordsBean bean = mDatas.get(selectIndex);
                OMAppApiProvider.getInstance().publishProject(LiveRoomInfoProvider.getInstance().liveId,
                        LiveRoomInfoProvider.getInstance().imRoomId, bean.id, bean.name, bean.liveRoomCoverImgUrl,
                        bean.subhead, bean.attribute, bean.sellStoreId,bean.agentCompanyCode,bean.agentId,bean.agentLive , new Observer<OMBaseResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                Toasty.normal(mContext, "网络异常，请检查网络").show();
                            }

                            @Override
                            public void onNext(OMBaseResponse response) {
                                if (response.isSuccess()) {
                                    if (LiveRoomHelper.getInstance().productListBtn != null) {
                                        LiveRoomHelper.getInstance().productListBtn.setVisibility(View.VISIBLE);
                                    }
                                    Toasty.normal(mContext, "发布成功").show();
                                    mDialog.dismiss();
                                } else {
                                    Toasty.normal(mContext, response.msg).show();
                                }
                            }
                        });
            }
            else{

                Toasty.normal(mContext, "请选择商品").show();

            }
        });
        closeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener listener) {
        mDialog.setOnDismissListener(listener);
    }

    public boolean isShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }


    private class DealListAdapter extends RecyclerView.Adapter {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_deal_item, parent, false);
            return new DealListVH(itemView);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            DealListVH vh = (DealListVH) holder;
            vh.bindData(position);
        }

        @Override
        public int getItemCount() {
            return mDatas == null ? 0 : mDatas.size();
        }
    }

    private class DealListVH extends RecyclerView.ViewHolder {

        private CheckBox checkbox;
        private ImageView ivCover;
        private TextView tvInfo;
        private View item;

        DealListVH(View itemView) {
            super(itemView);
            checkbox = itemView.findViewById(R.id.checkbox);
            tvInfo = itemView.findViewById(R.id.tvInfo);
            ivCover = itemView.findViewById(R.id.ivCover);
            item = itemView;

        }

        void bindData(int position) {
            int size = mDatas.size();
            GoodsResponse.DataBean.RecordsBean bean = mDatas.get(position);
            checkbox.setChecked(bean.isChecked);
            tvInfo.setText(bean.name);
            ImageLoaderManager.INSTANCE.loadCoverSizedImage(mContext, ivCover, OSSConfig.getOSSURLedStr(bean.liveRoomCoverImgUrl), 0, AppUtils.dip2px(mContext, 116), AppUtils.dip2px(mContext, 116), 0);

            item.setOnClickListener(view -> {
                if (!checkbox.isChecked()) {
                    selectIndex = position;
                    for (int i = 0; i < size; i++) {
                        GoodsResponse.DataBean.RecordsBean recordsBean = mDatas.get(i);
                        recordsBean.isChecked = position == i;
                    }
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }


}
