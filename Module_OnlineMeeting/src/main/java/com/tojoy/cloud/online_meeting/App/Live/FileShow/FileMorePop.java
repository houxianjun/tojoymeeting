package com.tojoy.cloud.online_meeting.App.Live.FileShow;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.configs.AppConfig;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.tojoy.cloud.online_meeting.R;

/**
 * @author qululu
 * 文档演示（主播、助手）更多弹窗
 */
class FileMorePop extends PopupWindow {

    private View mView;
    private Activity mContext;

    private View mUnreadLayout;
    private TextView mTvUnread;
    private View mAddMark;

    //操作回调
    private OperationCallback mOperationCallback;


    FileMorePop(Context context, OperationCallback operationCallback) {
        super(context);
        this.mContext = (Activity) context;
        this.mOperationCallback = operationCallback;
        initUI();
    }

    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.pop_file_show_more_layout, null);
        setContentView(mView);
        LinearLayout popup_contianer = mView.findViewById(R.id.popup_contianer);

        // 根据显示的条目控制不同的高度（背景图片太大导致wrap_content有问题）
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) popup_contianer.getLayoutParams();
        params.width = RelativeLayout.LayoutParams.MATCH_PARENT;
        params.height = RelativeLayout.LayoutParams.WRAP_CONTENT;
        popup_contianer.setLayoutParams(params);

        mUnreadLayout = mView.findViewById(R.id.unreadLayout);
        mTvUnread = mView.findViewById(R.id.tvUnread);
        mAddMark = mView.findViewById(R.id.addMark);

        if (AppConfig.isCanToast()) {
            // 显示评论
            mView.findViewById(R.id.rlv_file_more_comment).setVisibility(View.VISIBLE);
        } else {
            // 不显示评论
            mView.findViewById(R.id.rlv_file_more_comment).setVisibility(View.GONE);
        }

        setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        setAnimationStyle(R.style.pop_anim_alpha_file_show);
        setBackgroundDrawable(new ColorDrawable());
        setFocusable(true);
        setOutsideTouchable(true);
        setTouchable(true);
        initListener();
    }

    private void initListener() {
        /**
         * 消失时屏幕变亮
         */
        setOnDismissListener(() -> {
            WindowManager.LayoutParams layoutParams = mContext.getWindow().getAttributes();
            layoutParams.alpha = 1.0f;
            mContext.getWindow().setAttributes(layoutParams);
        });

        mView.findViewById(R.id.rlv_file_more_comment).setOnClickListener(v -> {
            // 评论
            //判断是否有权限
            if (mOperationCallback != null) {
                mUnreadLayout.setVisibility(View.GONE);
                mTvUnread.setText("0");
                mOperationCallback.showChatRoomMessage();
                mOperationCallback.goneUnreadCount();
                dismiss();
            }

        });

        mView.findViewById(R.id.rlv_file_more_connections).setOnClickListener(v -> {
            if (mOperationCallback != null) {
                mOperationCallback.showMemberList();
                dismiss();
            }
        });
        mView.findViewById(R.id.rlv_file_more_share).setOnClickListener(v -> {
            // 分享
            if (mOperationCallback != null) {
                mOperationCallback.showShareView();
                dismiss();
            }
        });
        mView.findViewById(R.id.rlv_file_more_interaction).setOnClickListener(v -> {
            // 互动
            if (mOperationCallback != null) {
                mOperationCallback.showOperationView();
                dismiss();
            }
        });
    }

    /**
     * 刷新更多弹窗聊天的未读消息
     *
     * @param addCount
     */
    @SuppressLint("SetTextI18n")
    void refreshUnreadInfo(int addCount) {
        if (addCount == -1) {
            if (mUnreadLayout.getVisibility() != View.GONE) {
                mUnreadLayout.setVisibility(View.GONE);
            }
            mTvUnread.setText("0");
        } else if (mUnreadLayout.getVisibility() == View.GONE) {
            // 首次接到未读
            mUnreadLayout.setVisibility(View.VISIBLE);
            mTvUnread.setText(addCount + "");
            mAddMark.setVisibility(View.GONE);
        } else {
            int unread = Integer.parseInt(mTvUnread.getText().toString());
            unread += addCount;
            if (unread <= 99) {
                mTvUnread.setText(unread + "");
                mAddMark.setVisibility(View.GONE);
            } else {
                mTvUnread.setText("99");
                mAddMark.setVisibility(View.VISIBLE);
            }
        }
    }
}
