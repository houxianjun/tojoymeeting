package com.tojoy.cloud.online_meeting.App.UserCenter.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.live.EndLiveInfoResponse;
import com.tojoy.tjoybaselib.model.live.MyLiveRoomListResponse;
import com.tojoy.tjoybaselib.model.user.CustomerServiceResponse;
import com.tojoy.tjoybaselib.model.user.LoginResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.ui.statusbar.StatusBarHeightView;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.ui.vlayout.DelegateAdapter;
import com.tojoy.tjoybaselib.ui.vlayout.VirtualLayoutManager;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.App.Home.adapter.OnlineMeetingUserHomeFooterAdapter;
import com.tojoy.cloud.online_meeting.App.UserCenter.adapter.UserHomePageLiveAdapter;
import com.tojoy.cloud.online_meeting.App.UserCenter.adapter.UserHomePageTopAdapter;
import com.tojoy.cloud.online_meeting.App.UserCenter.model.UserHomePageTopModel;
import com.tojoy.cloud.online_meeting.App.UserCenter.view.ApplyPlaybacktDialog;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.Services.EventBus.LiveRoomEvent;
import com.yanzhenjie.permission.AndPermission;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * @author qululu
 * 首页搜索--跳转的个人主页
 */
@Route(path = RouterPathProvider.ONLINE_MEETING_USER_CENTER)
public class UserHomePageAct extends UI {
    private TJRecyclerView mRecyclerView;
    private RelativeLayout mTitleRlv;

    private DelegateAdapter mAdapter;
    private UserHomePageTopAdapter mUserHomePageTopAdapter;
    private UserHomePageLiveAdapter mUserHomePageLiveAdapter;
    private OnlineMeetingUserHomeFooterAdapter mFooterAdapter;

    private List<DelegateAdapter.Adapter> mAdaptersList = new ArrayList<>();

    private UserHomePageTopModel userHomePageTopModel = new UserHomePageTopModel();
    private List<MyLiveRoomListResponse.DataObjBean.ListBean> mDatas = new ArrayList<>();

    private String mUserId;
    // 8：代表从搜索列表或推送进入的，不显示标签信息
    private String mType;
    //页面是否是初始化进入
    private boolean mIniResume = false;
    //数据是否通过刷新获取
    private boolean mIsRefreshing = false;

    public static void startEndLive(Context context, String type, String duration) {
        Intent intent = new Intent();
        intent.setClass(context, UserHomePageAct.class);
        intent.putExtra("hostPic", LiveRoomInfoProvider.getInstance().hostAvatar);
        intent.putExtra("roomId", LiveRoomInfoProvider.getInstance().roomId);
        intent.putExtra("roomCode", LiveRoomInfoProvider.getInstance().roomCode);
        intent.putExtra("userName", LiveRoomInfoProvider.getInstance().hostName);
        intent.putExtra("duration", duration);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_home_page);
        setStatusBar();
        setSwipeBackEnable(false);
        setStatusBarLightMode();

        fromPush();

        mRecyclerView = (TJRecyclerView) findViewById(R.id.recycler_view);


        mTitleRlv = (RelativeLayout) findViewById(R.id.rlv_title);

        int statusBarHeight = AppUtils.getStatusBarHeight(this);
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.dip2px(this, 48));
        lps.setMargins(0, statusBarHeight, 0, 0);
        mTitleRlv.setLayoutParams(lps);
        findViewById(R.id.iv_leftimg).setOnClickListener(v -> finish());
        findViewById(R.id.iv_close).setOnClickListener(v -> finish());
        VirtualLayoutManager mLayoutManager = new VirtualLayoutManager(this);
        mRecyclerView.getRecyclerView().setLayoutManager(mLayoutManager);
        // 设置复用
        mRecyclerView.getRecyclerView().setItemViewCacheSize(30);
        mRecyclerView.setRefreshable(true);

        mAdapter = new DelegateAdapter(mLayoutManager, false);
        mRecyclerView.setAdapter(mAdapter);

        initLoaclData();
        mUserHomePageTopAdapter = new UserHomePageTopAdapter(this, userHomePageTopModel, 0, mType);
        mAdapter.addAdapter(mUserHomePageTopAdapter);
        mUserHomePageLiveAdapter = new UserHomePageLiveAdapter(UserHomePageAct.this, mDatas, 1);
        mFooterAdapter = new OnlineMeetingUserHomeFooterAdapter(this, 6);

        requestEndPageVideoList();

        if ("-1".equals(mType)) {
            findViewById(R.id.iv_close).setVisibility(View.GONE);
            findViewById(R.id.iv_leftimg).setVisibility(View.VISIBLE);
        } else {
            requestLiveEndData();
            findViewById(R.id.iv_close).setVisibility(View.VISIBLE);
            findViewById(R.id.iv_leftimg).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tvTitle)).setText(LiveRoomInfoProvider.getInstance().liveTitle);
        }
        sysClosedLive();

        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {//下拉刷新监听
                mPage = 1;
                mIsRefreshing = true;
                requestEndPageVideoList();
            }

            @Override
            public void onLoadMore() {
            }
        });

        //新增更新标题栏背景色渐变方法
        updateTitleBackground();

        requestUserInfo();
    }

    /**
     * 如果是刚结束直播的主播，弹出是否申请生成回放弹窗
     */
    private void applyPlayback(String isPlayback) {
        if (LiveRoomInfoProvider.getInstance().isHost() && "1".equals(mType) && LiveRoomInfoProvider.getInstance().isHostEndLive
                && (!TextUtils.isEmpty(isPlayback) && "1".equals(isPlayback))) {
            ApplyPlaybacktDialog dialog = new ApplyPlaybacktDialog(this);
            dialog.show();
        }
    }

    /**
     * push已结束的直播进来的，关闭页面
     */
    private void fromPush() {
        EventBus.getDefault().post(new LiveRoomEvent(LiveRoomEvent.LIVE_CLOSE_ALL, ""));
    }

    private void initLoaclData() {
        String mHostPic = getIntent().getStringExtra("hostPic");
        String mRoomCode = getIntent().getStringExtra("roomCode");
        String mRoomId = getIntent().getStringExtra("roomId");
        mUserId = getIntent().getStringExtra("userId");
        String mName = getIntent().getStringExtra("userName");
        mType = getIntent().getStringExtra("type");
        userHomePageTopModel.hostPic = mHostPic;
        userHomePageTopModel.roomId = mRoomId;
        userHomePageTopModel.roomCode= mRoomCode;
        userHomePageTopModel.userName = mName;
        userHomePageTopModel.duration = getIntent().getStringExtra("duration");
    }

    private void requestUserInfo(){
        if(TextUtils.isEmpty(userHomePageTopModel.userName) && !TextUtils.isEmpty(mUserId)){
            OMAppApiProvider.getInstance().mGetUserInfo(mUserId, new Observer<LoginResponse>() {
                @Override
                public void onCompleted() {
                }

                @Override
                public void onError(Throwable e) {
                }

                @Override
                public void onNext(LoginResponse baseResponse) {
                    if (baseResponse.isSuccess()) {
                        userHomePageTopModel.hostPic = OSSConfig.getOSSURLedStr(baseResponse.data.avatar);
                        userHomePageTopModel.userName = baseResponse.data.trueName;
                        if(mUserHomePageTopAdapter != null){
                            mUserHomePageTopAdapter.setNameAndHeadImg();
                            mUserHomePageTopAdapter.notifyDataSetChanged();
                        }
                    }
                }
            });
        }
    }


    private void requestLiveEndData() {
        OMAppApiProvider.getInstance().getEndLiveRoomInfo(LiveRoomInfoProvider.getInstance().liveId, "3".equals(mType) ? "0" : "1",
                new Observer<EndLiveInfoResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(EndLiveInfoResponse response) {
                        if (response.isSuccess()) {

                            if (!TextUtils.isEmpty(response.data.durationTime)) {
                                try {
                                    long l = Long.parseLong(response.data.durationTime);
                                    userHomePageTopModel.duration = LiveRoomIOHelper.getTimeStr((int) (l / 1000));
                                } catch (NumberFormatException e) {

                                }
                            }
                            userHomePageTopModel.endLiveInfoModel = response.data;
                            mUserHomePageTopAdapter.notifyDataSetChanged();
                            applyPlayback(response.data.isPlayback);
                        }
                    }
                });
    }

    /**
     * 直播结束页-请求结束页面列表数据
     */
    private void requestEndPageVideoList() {
        if (!mIsRefreshing) {
            showProgressHUD(this, "加载中...");
        }
        OMAppApiProvider.getInstance().getRecommendLiveList(mPage + "", "4", "2", new Observer<MyLiveRoomListResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
                mRecyclerView.setRefreshing(false);
                mAdapter.removeAdapters(mAdaptersList);
                if (mIsRefreshing) {
                    mAdaptersList.remove(mUserHomePageLiveAdapter);
                    mAdaptersList.remove(mFooterAdapter);

                }
                if (mDatas != null && mDatas.size() != 0) {
                    mAdaptersList.add(mUserHomePageLiveAdapter);
                    mAdaptersList.add(mFooterAdapter);
                }


                mAdapter.addAdapters(mAdaptersList);
                mAdapter.notifyDataSetChanged();

            }

            @Override
            public void onError(Throwable e) {
                dismissProgressHUD();
                Toasty.normal(UserHomePageAct.this, "网络异常，请稍后再试").show();
                mRecyclerView.stopRefresh();
                if (!mIsRefreshing) {
                    mAdapter.addAdapters(mAdaptersList);
                    mAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onNext(MyLiveRoomListResponse response) {
                if (response.isSuccess()) {
                    if (mPage == 1) {
                        mDatas.clear();
                        mDatas.addAll(response.data.list);
                    } else {
                        mDatas.addAll(response.data.list);
                    }
                } else {
                    Toasty.normal(UserHomePageAct.this, response.msg).show();
                }
            }
        });
    }


    /**
     * 被后台关闭的直播
     */
    private void sysClosedLive() {
        if (LiveRoomInfoProvider.getInstance().isSysClosedLiveRoom) {
            LiveRoomInfoProvider.getInstance().isSysClosedLiveRoom = false;
            String tipsMsgViewer = LiveRoomInfoProvider.getInstance().tipsMsgViewer;
            if (!LiveRoomInfoProvider.getInstance().isHost()) {

                OMAppApiProvider.getInstance().mGetCustomerServicePhone(new Observer<CustomerServiceResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(CustomerServiceResponse customerServiceResponse) {
                        if(customerServiceResponse.isSuccess()){
                            showBreakDialog(customerServiceResponse.data);
                        }
                    }
                });

            } else {
                new TJMakeSureDialog(UserHomePageAct.this, v1 -> {
                })
                        .setOkText("我知道了")
                        .setTitleAndCotent("", TextUtils.isEmpty(tipsMsgViewer) ? "抱歉，该直播中断，去看看其他直播吧！" : tipsMsgViewer)
                        .hideTitle()
                        .showAlert();
            }
        }
    }

    private void showBreakDialog(String phone) {

        String tipsMsgHost = LiveRoomInfoProvider.getInstance().tipsMsgHost;

        new TJMakeSureDialog(UserHomePageAct.this, v1 -> {
            AndPermission.with(UserHomePageAct.this)
                    .permission(
                            Manifest.permission.CALL_PHONE
                    )
                    .rationale((context, permissions, executor) -> {
                        // 此处可以选择显示提示弹窗
                        executor.execute();
                    })
                    // 用户给权限了
                    .onGranted(permissions -> {
                                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                    )
                    // 用户拒绝权限，包括不再显示权限弹窗也在此列
                    .onDenied(permissions -> {
                        // 判断用户是不是不再显示权限弹窗了，是的话进入权限设置页
                        if (AndPermission.hasAlwaysDeniedPermission(UserHomePageAct.this, permissions)) {
                            // 打开权限设置页
                            AndPermission.permissionSetting(UserHomePageAct.this).execute();
                        }
                    })
                    .start();
        })
                .setBtnText("拨打电话", "我知道了")
                .setTitleAndCotent("", TextUtils.isEmpty(tipsMsgHost) ? "你的直播被驳回，请联系"+ phone +"进行咨询" : tipsMsgHost)
                .hideTitle()
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mIniResume) {
            mPage = 1;
            mIsRefreshing = true;
            requestEndPageVideoList();
        }
        mIniResume = true;
    }

    /**
     * 添加标题栏背景色渐变方法
     */
    //设置其透明度
    float alpha = 0;
    //标题栏高度
    int mTitle_ViewHeight;
    //头部图片(轮播图的高度)
    int mRecyclerHeaderBannerHeight = 0;
    //头部的高度
    int mRecyclerHeaderHeight;
    int recyclerItemHeight;
    StatusBarHeightView statusbarheightview;
    RelativeLayout rlv_title_background;
    public void updateTitleBackground(){
        //轮播图片的高度--和xml图片的高度是一样的
        mRecyclerHeaderBannerHeight = (int) getResources().getDimension(R.dimen.dp_200);
        //RecyclerView每个Item之间的距离,和Adapter中设置的距离一样
        recyclerItemHeight = (int) getResources().getDimension(R.dimen.dp_100);

        statusbarheightview = (StatusBarHeightView) findViewById(R.id.statusbarheightview) ;
        rlv_title_background= (RelativeLayout) findViewById(R.id.rlv_title_background) ;
        //获取标题栏的高度
        mTitle_ViewHeight = mTitleRlv.getHeight();

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.d("onScrollStateChanged","newState="+newState);

            }

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.d("onScrollStateChanged","dx="+dx+"dy="+dy);
                int mdx = dx;

                int mdy = dy;

                int scollYHeight = 0;
                scollYHeight=   mRecyclerView.getScollYHeight(true, mRecyclerHeaderHeight);
                //起始截止变化高度,如可以变化高度为mRecyclerHeaderHeight
                int baseHeight = mRecyclerHeaderBannerHeight - recyclerItemHeight - mTitle_ViewHeight;

                if(scollYHeight >= baseHeight) {
                    //完全不透明
                    alpha = 1;


                }else {
                    //产生渐变效果
                    alpha = scollYHeight / (baseHeight*1.0f);

                }
                statusbarheightview.setAlpha(alpha);
                rlv_title_background.setAlpha(alpha);
            }
        });
    }
}