package com.tojoy.cloud.online_meeting.App.EnterpriseEditionHome.view;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.netease.nim.uikit.business.chatroom.helper.ChatRoomHelper;
import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.enterprise.EnterpriseBannerModel;
import com.tojoy.tjoybaselib.model.live.OnlineMeetingHomeGridModel;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.ui.indicator.CoustomIndicator;
import com.tojoy.tjoybaselib.ui.recyclerview.decoration.SpacingDecoration;
import com.tojoy.tjoybaselib.ui.Banner.Banner;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.LiveActAtack;
import com.tojoy.cloud.online_meeting.App.EnterpriseEditionHome.EnterpriseHomeDetailBannerImageLoader;
import com.tojoy.cloud.online_meeting.App.Home.activity.ProductsRecommendListActivity;
import com.tojoy.cloud.online_meeting.App.Home.adapter.RoadOrHotLiveListAdapter;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.Services.EventBus.LiveRoomEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;


/**
 * @author fanxi
 * @date 2020-02-28.
 * description：
 */
public class CustomizedEnterpriseHeaderView {
    public View mView;
    private Banner mBanner;
    private CoustomIndicator topCoustomIndicator;
    private RelativeLayout mRecyclerViewRlv;
    private TextView mTvHotLiveTitle, mTvProductTitle;
    private List<EnterpriseBannerModel> mBannerList = new ArrayList<>();
    private List<OnlineMeetingHomeGridModel> mHotLiveRecommendList = new ArrayList<>();

    private RoadOrHotLiveListAdapter mHotAdapter;
    private String mCompanyCode;
    private Context mContext;

    //热门推荐默认标题
    private String mHotRecommendTitle = "热门推荐";
    //产品推荐默认标题
    private String mProductRecommendTitle = "产品推荐";

    /**
     * 若来源为直播间，需暂时记录直播间信息
     */
    private String roomLiveId;
    private String roomId;
    private String isFromLive;
    private String sourceWay;


    public CustomizedEnterpriseHeaderView(Activity context) {
        mContext = context;
    }

    public View getView() {
        if (mView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mView = aLayoutInflater.inflate(R.layout.header_customized_enterprise_layout, null);
            ButterKnife.bind(this, mView);
        }
        initView();
        return mView;
    }


    private void initView() {
        mBanner = mView.findViewById(R.id.iv_banner);
        topCoustomIndicator = mView.findViewById(R.id.indicatorview);
        RecyclerView mRecyclerView = mView.findViewById(R.id.hot_recycler);
        mRecyclerViewRlv = mView.findViewById(R.id.rlv_hot_recycler);
        mTvHotLiveTitle = mView.findViewById(R.id.tv_hot_live_title);
        mTvProductTitle = mView.findViewById(R.id.tv_product_title);


        mHotAdapter = new RoadOrHotLiveListAdapter(mContext, mHotLiveRecommendList, 1);
        GridLayoutManager mGridLayoutManager = new GridLayoutManager(mContext, 2, LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(mGridLayoutManager);
        mRecyclerView.setAdapter(mHotAdapter);
        mRecyclerView.addItemDecoration(new SpacingDecoration(AppUtils.dip2px(mContext, 6), 0, true));
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setEnabled(false);


        initWidgetListener();
        int widht = AppUtils.getWidth(mContext) - AppUtils.dip2px(mContext, 12) * 2;
        int height = 160 * widht / 351;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mBanner.getLayoutParams();
        params.height = height;
        params.width = widht;
        mBanner.setLayoutParams(params);
        mBanner.setDelayTime(5000);

        mBanner.setIndicatorHeight(mContext, 0);
        //设置图片加载器
        mBanner.setImageLoader(new EnterpriseHomeDetailBannerImageLoader());
        mHotAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
            if (FastClickAvoidUtil.isFastDoubleClick(v.getId())) {
                return;
            }
            if (ChatRoomHelper.isFromLiveRoom) {
                if (LiveRoomInfoProvider.getInstance().isHost()) {
                    Toasty.normal(mContext, "正在直播，请稍后再试").show();
                } else if (LiveRoomInfoProvider.getInstance().liveId.equals(data.roomLiveId)) {
                    LiveActAtack.getActivityStack().popAllActivity();
                } else {
                    TJMakeSureDialog tjMakeSureDialog = new TJMakeSureDialog(mContext);
                    tjMakeSureDialog.setTitleAndCotent("", "您还未离开当前会议\n要加入新的会议么");
                    tjMakeSureDialog.setBtnSureText("加入");
                    tjMakeSureDialog.setBtnCancelText("不加入");
                    tjMakeSureDialog.hideTitle();
                    tjMakeSureDialog.setEvent(v1 -> {
                        joinThisLiveRoom(data);
                        tjMakeSureDialog.dismissStrongly();
                        //关闭之前打开的
                        EventBus.getDefault().post(new LiveRoomEvent(LiveRoomEvent.LIVE_CLOSE_GUEST, ""));
                    });
                    tjMakeSureDialog.show();
                }
            } else {
                joinThisLiveRoom(data);
            }
        });
    }


    private void joinThisLiveRoom(OnlineMeetingHomeGridModel data) {
        if (!TextUtils.isEmpty(data.hasPassword) && "1".equals(data.hasPassword)) {
            LiveRoomIOHelper.checkEnterLiveRoom(mContext, data.roomLiveId, new IOLiveRoomListener() {
                @Override
                public void onIOError(String error, int errorCode) {
                    if (errorCode == 301) {
                        ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_ENTER_ROOM_PSW)
                                .withString("roomName", data.title)
                                .withString("liveId", data.roomLiveId)
                                .navigation();
                    } else if (errorCode == 302) {
                    } else {
                        Toasty.normal(mContext, error).show();
                    }
                }

                @Override
                public void onIOSuccess() {
                    joinRoom(data.roomLiveId);
                }
            });
        } else {
            joinRoom(data.roomLiveId);
        }
    }

    /**
     * 进入直播间
     */
    private void joinRoom(String roomLiveId) {

        //先调一次退出直播间 针对该用户从直播间点主页后跳另外的直播间
        if (!TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().liveId) && !TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().imRoomId)
                && !LiveRoomInfoProvider.getInstance().liveId.equals(roomLiveId)) {
            LiveRoomInfoProvider.getInstance().lastLiveId = LiveRoomInfoProvider.getInstance().liveId;
            LiveRoomInfoProvider.getInstance().lastIMRoomId = LiveRoomInfoProvider.getInstance().imRoomId;
        } else {
            LiveRoomInfoProvider.getInstance().lastLiveId = "";
        }

        LiveRoomInfoProvider.getInstance().joinPassword = "";
        LiveRoomInfoProvider.getInstance().liveId = roomLiveId;
        ((UI) mContext).showProgressHUD(mContext, "加载中");
        LiveRoomIOHelper.joinLiveRoom(mContext, new IOLiveRoomListener() {
            @Override
            public void onIOError(String error, int errorCode) {
                ((UI) mContext).dismissProgressHUD();
                if (!TextUtils.isEmpty(error)) {
                    Toasty.normal(mContext, error).show();
                }
            }

            @Override
            public void onIOSuccess() {
                ((UI) mContext).dismissProgressHUD();
                LiveRoomIOHelper.exitOldUser(BaseUserInfoCache.getUserId(mContext));
            }
        });
    }

    /**
     * 设置banner的数据
     */
    public void setBannerData(List<EnterpriseBannerModel> datas) {
        if (datas != null && datas.size() != 0) {
            setBannerVisible(true);
            mBannerList.clear();
            mBannerList.addAll(datas);
            topCoustomIndicator.setVisibility(View.VISIBLE);
            initBanner();
        } else {
            setBannerVisible(false);
            topCoustomIndicator.setVisibility(View.GONE);
        }
    }

    /**
     * 设置热门直播推荐列表的数据
     */
    public void setHotLiveListData(List<OnlineMeetingHomeGridModel> datas) {
        mHotLiveRecommendList.clear();
        if (datas != null && datas.size() != 0) {
            mView.findViewById(R.id.rlv_hot).setVisibility(View.VISIBLE);
            mRecyclerViewRlv.setVisibility(View.VISIBLE);
            if (datas.size() == 1) {
                mHotLiveRecommendList.addAll(datas);
            } else {
                mHotLiveRecommendList.add(datas.get(0));
                mHotLiveRecommendList.add(datas.get(1));
            }
            mHotAdapter.updateData(mHotLiveRecommendList);
        } else {
            mView.findViewById(R.id.rlv_hot).setVisibility(View.GONE);
            mRecyclerViewRlv.setVisibility(View.GONE);
        }
    }

    /**
     * 设置公司编码
     */
    public void setCompanyCode(String companyCode) {
        mCompanyCode = companyCode;
    }

    /**
     * 设置页面来源（直播间/其他）
     */
    public void setPageSource(String isFrom, String roomLiveId, String roomId, String sourceWay) {
        this.roomId = roomId;
        this.roomLiveId = roomLiveId;
        this.isFromLive = isFrom;
        this.sourceWay = sourceWay;
    }

    /**
     * 初始化控件点击事件
     */
    private void initWidgetListener() {

        //热门推荐点击更多
        mView.findViewById(R.id.tv_more_hot).setOnClickListener(v -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }
            ARouter.getInstance().build(RouterPathProvider.ENTERPRISE_ROAD_OR_HOT_LIVE_LIST)
                    .withString("pageTitle", "热门推荐")
                    .withString("code", mCompanyCode)
                    .withString("title", mHotRecommendTitle)
                    .navigation();

        });
        //产品推荐点击更多
        mView.findViewById(R.id.tv_more).setOnClickListener(v -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }
            ProductsRecommendListActivity.start(mContext, mProductRecommendTitle, mCompanyCode, isFromLive, roomLiveId, roomId, sourceWay);
        });
    }

    /**
     * 热门推荐title
     */
    public void setLiveListTitle(String title) {
        mTvHotLiveTitle.setText(title);
        mHotRecommendTitle = title;
    }

    /**
     * 产品推荐title
     */
    public void setProductListTitle(String title) {
        mTvProductTitle.setText(title);
        mProductRecommendTitle = title;
    }

    /**
     * 初始化banner
     */
    private void initBanner() {
        ArrayList<String> images = new ArrayList<>();
        for (EnterpriseBannerModel data : mBannerList) {
            images.add(data.coverOne);
        }
        if (images.size() != 0) {
            topCoustomIndicator.setMaxSize(images.size());

        }
        topCoustomIndicator.setCurIndex(0);
        //设置图片集合
        mBanner.setImages(images);
        //banner设置方法全部调用完毕时最后调用
        mBanner.start();
        //页面改变的导航
        mBanner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                topCoustomIndicator.setCurIndex(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        mBanner.setOnBannerListener(position -> {
            if (FastClickAvoidUtil.isFastDoubleClick(mBanner.getId())) {
                return;
            }
            // 1:直播会议；2:商品；3:广告；4:企业宣传
            switch (mBannerList.get(position).bannerType) {
                case 1:
                    // 主播不能再跳入直播间
                    if (ChatRoomHelper.isFromLiveRoom) {
                        if (LiveRoomInfoProvider.getInstance().isHost()) {
                            //主播
                            Toasty.normal(mContext, "正在直播，请稍后再试").show();
                        } else if (LiveRoomInfoProvider.getInstance().liveId.equals(
                                mBannerList.get(position).bannerId)) {
                            //观众
                            LiveActAtack.getActivityStack().popAllActivity();
                        } else {
                            TJMakeSureDialog tjMakeSureDialog = new TJMakeSureDialog(mContext);
                            tjMakeSureDialog.setTitleAndCotent("", "您还未离开当前会议\n要加入新的会议么");
                            tjMakeSureDialog.setBtnSureText("加入");
                            tjMakeSureDialog.setBtnCancelText("不加入");
                            tjMakeSureDialog.hideTitle();
                            tjMakeSureDialog.setEvent(v1 -> {
                                joinBannerRoom(mBannerList.get(position).bannerId, mBannerList.get(position).bannerTitle);
                                tjMakeSureDialog.dismissStrongly();
                                //关闭之前打开的
                                EventBus.getDefault().post(new LiveRoomEvent(LiveRoomEvent.LIVE_CLOSE_GUEST, ""));
                            });
                            tjMakeSureDialog.show();
                        }
                    } else {
                        joinBannerRoom(mBannerList.get(position).bannerId, mBannerList.get(position).bannerTitle);
                    }
                    break;
                case 2:
                    // 商品
                    if (!TextUtils.isEmpty(isFromLive) && "1".equals(isFromLive)) {
                        // 从直播间跳过来的，来源记录为app直播间
                        RouterJumpUtil.startGoodsDetailFromLiveRoom(mBannerList.get(position).bannerId, roomId, roomLiveId, sourceWay != null ? sourceWay : "2", "", "", "0");
                    } else {
                        RouterJumpUtil.startGoodsDetails(mBannerList.get(position).bannerId, sourceWay != null ? sourceWay : "2", "", "", "0");
                    }
                    break;
                case 3:
                    // 广告
                    ARouter.getInstance().build(RouterPathProvider.ONLINE_ENTERPRISE_AND_PRODUCT_DETAIL)
                            .withString("productId", mBannerList.get(position).bannerId)
                            .navigation();
                    break;
                case 4:
                    // 企业宣
                    ARouter.getInstance().build(RouterPathProvider.ONLINE_ENTERPRISE_AND_PRODUCT_DETAIL)
                            .withString("enterpriseId", mBannerList.get(position).bannerId)
                            .navigation();
                    break;
                default:
                    break;
            }

        });
    }


    private void joinBannerRoom(String roomLiveId, String roomname) {
        LiveRoomIOHelper.checkEnterLiveRoom(mContext, roomLiveId, new IOLiveRoomListener() {
            @Override
            public void onIOError(String error, int errorCode) {
                if (errorCode == 301) {
                    ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_ENTER_ROOM_PSW)
                            .withString("roomName", roomname)
                            .withString("liveId", roomLiveId)
                            .navigation();
                } else if (errorCode == 302) {
                } else {
                    Toasty.normal(mContext, error).show();
                }
            }

            @Override
            public void onIOSuccess() {
                joinRoom(roomLiveId);
            }
        });
    }

    /**
     * 设置产品title显示隐藏
     */
    public void setProductTitleVisible(boolean visible) {
        mView.findViewById(R.id.rlv_product).setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    /**
     * 设置banner显示隐藏
     */
    private void setBannerVisible(boolean visible) {
        mBanner.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
}