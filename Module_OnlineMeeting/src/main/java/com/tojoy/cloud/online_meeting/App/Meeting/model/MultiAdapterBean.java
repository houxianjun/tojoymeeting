package com.tojoy.cloud.online_meeting.App.Meeting.model;

import com.tojoy.tjoybaselib.model.live.OnlineMeetingHomeGridModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chengyanfang on 2018/4/3.
 */

public class MultiAdapterBean {

    public enum CellType {
        //瀑布流
        MULTI_CELL,
    }

    public CellType type;

    public enum ResouseType {
        //会议中心
        CENTER_LIVE_LIST,

        // 热门推荐列表
        ROAD_OR_HOT_LIVE_LIST,
    }

    public ResouseType resouseType;

    public MultiAdapterBean(CellType type,ResouseType resouseType) {
        this.type = type;
        this.resouseType = resouseType;
    }

    /**
     * 会议中心
     */
    public List<OnlineMeetingHomeGridModel> liveDataList = new ArrayList<>();

    public MultiAdapterBean setLiveDataList(List<OnlineMeetingHomeGridModel> liveDataList) {
        this.liveDataList = liveDataList;
        return this;
    }

    /**
     * 热门推荐直播列表
     */
    public List<OnlineMeetingHomeGridModel> roadOrHotLiveDataList = new ArrayList<>();

    public MultiAdapterBean setRoadOrHotLiveDataList(List<OnlineMeetingHomeGridModel> dataList) {
        this.roadOrHotLiveDataList = dataList;
        return this;
    }
}
