package com.tojoy.cloud.online_meeting.App.UserCenter.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.bumptech.glide.Glide;
import com.tojoy.common.Services.Distribution.DistrbutionHelper;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.vlayout.DelegateAdapter;
import com.tojoy.tjoybaselib.ui.vlayout.LayoutHelper;
import com.tojoy.tjoybaselib.ui.vlayout.layout.StaggeredGridLayoutHelper;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;

import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.tojoy.common.LiveRoom.MeetingExtensionPopView;
import com.tojoy.cloud.online_meeting.App.Home.view.VideoPlayAnimationView;
import com.tojoy.tjoybaselib.model.live.MyLiveRoomListResponse;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;

import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * @author qll
 * @date 2019/4/28
 * 直播间数据
 */
public class UserHomePageLiveAdapter extends DelegateAdapter.Adapter<UserHomePageLiveAdapter.UserHomePageLiveViewHolder> {
    private StaggeredGridLayoutHelper mHelper;
    private List<MyLiveRoomListResponse.DataObjBean.ListBean> mDatas;
    private UserHomePageLiveViewHolder mViewHolder;
    private Context mContext;
    private int mViewTypeItem = 0;//1代表他的直播  2代表热门直播

    public UserHomePageLiveAdapter(Context context, List<MyLiveRoomListResponse.DataObjBean.ListBean> mData, int vieType) {
        this.mDatas = mData;
        this.mContext = context;
        this.mViewTypeItem = vieType;
    }

    @Override
    public LayoutHelper onCreateLayoutHelper() {
        if (mHelper == null) {
            mHelper = new StaggeredGridLayoutHelper(2);
            mHelper.setVGap(AppUtils.dip2px(mContext, 13));
            mHelper.setHGap(AppUtils.dip2px(mContext, 10));
            mHelper.setMargin(AppUtils.dip2px(mContext, 12),
                    AppUtils.dip2px(mContext, 6),
                    AppUtils.dip2px(mContext, 12),
                    AppUtils.dip2px(mContext, 6));
//            mHelper.setAutoExpand(false);
        }
        return mHelper;
    }

    @Override
    public UserHomePageLiveViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_online_meeting_home_grid, parent, false);
        return new UserHomePageLiveViewHolder(view);

    }

    @Override
    public void onViewAttachedToWindow(@NonNull UserHomePageLiveViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.mRlvAnimContainer.removeAllViews();
        VideoPlayAnimationView videoPlayAnimationView = new VideoPlayAnimationView((Activity) mContext);
        holder.mRlvAnimContainer.addView(videoPlayAnimationView.getView());
    }


    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(UserHomePageLiveViewHolder holder, int position) {
        MyLiveRoomListResponse.DataObjBean.ListBean model = mDatas.get(position);
        mViewHolder = holder;
        mViewHolder.mRoomIdTv.setVisibility(View.GONE);
        //隐藏标签组件，在结束会议页面时不显示
        mViewHolder.mProjectTagLlv.setVisibility(View.GONE);
        mViewHolder.mTitleTv.setText(model.title);
        if (TextUtils.isEmpty(model.roomCode)) {
            holder.mRoomIdTv.setVisibility(View.GONE);
        } else {
            holder.mRoomIdTv.setVisibility(View.VISIBLE);
            holder.mRoomIdTv.setText(model.getRoomIdText());
        }
        mViewHolder.mRoomCoverIv.setVisibility(View.VISIBLE);
        ImageLoaderManager.INSTANCE.loadWantonlyRadiu(mContext,
                mViewHolder.mRoomCoverIv,
                !TextUtils.isEmpty(model.coverOne) ? model.coverOne : model.coverTwo,
                R.drawable.icon_online_meeting_home_grid_default,
                R.drawable.icon_online_meeting_home_grid_default,
                AppUtils.dip2px(mContext, 5),
                false, false, true, true);


        mViewHolder.rootView.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(model.password) && "0".equals(model.password)) {
                joinRoom(position);
            } else {
                // 房间设置了密码
                LiveRoomIOHelper.checkEnterLiveRoom(mContext, model.roomLiveId, new IOLiveRoomListener() {
                    @Override
                    public void onIOError(String error, int errorCode) {
                        if (errorCode == 301) {
                            ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_ENTER_ROOM_PSW)
                                    .withString("roomName", model.title)
                                    .withString("liveId", model.roomLiveId)
                                    .navigation();
                        } else if (errorCode == 302) {
                            // 弹窗
//                        ((UserHomePageAct)mContext).callPhone();
                        } else {
                            Toasty.normal(mContext, error).show();
                        }
                    }

                    @Override
                    public void onIOSuccess() {
                        // 不需要输入密码，直接进入,包括判断密码正确与否与人数是否已满
                        joinRoom(position);
                    }
                });
            }
        });

        if (!TextUtils.isEmpty(model.status)) {
            // 根据直播状态显示不同标签
            switch (model.status) {
                case "1":
                    // 预告
                    holder.mLiveStatusAlreadyTv.setVisibility(View.GONE);
                    holder.ll_bg_gifview.setVisibility(View.GONE);
                    break;
                case "2":
                case "3":
                    // 直播
                    holder.mLiveStatusAlreadyTv.setVisibility(View.GONE);
                    Glide.with(mContext).asGif().load(R.drawable.animvideo_player).into(holder.mIvLive);
                    holder.ll_bg_gifview.setVisibility(View.VISIBLE);
                    break;
                case "4":
                    holder.ll_bg_gifview.setVisibility(View.GONE);
                    // 回放
                    holder.mLiveStatusAlreadyTv.setVisibility(View.VISIBLE);
                    break;
                default:
                    holder. mLiveStatusAlreadyTv.setVisibility(View.GONE);
                    holder.ll_bg_gifview.setVisibility(View.GONE);
                    break;
            }
        } else {
            holder.mLiveStatusAlreadyTv.setVisibility(View.GONE);
        }
        holder.mRlvAnimContainer.setVisibility(View.GONE);
        // 设置观看人数
        holder.mLiveSeeCountTv.setVisibility(View.GONE);

        if (DistrbutionHelper.isShowDistribution(mContext,model.ifDistribution,model.companyCode)) {
            holder.mAllTopRlv.setPadding(0,0,0,AppUtils.dip2px(mContext,15));
            holder.mDistributionMoneyTv.setText(DistrbutionHelper.getMaxBudgetText(mContext
                    ,model.level1Commission
                    ,model.level2Commission
                    ,model.level1CommissionEmployee
                    ,model.level2CommissionEmployee
                    ,model.companyCode,true));
            holder.mDistributionLlv.setVisibility(View.VISIBLE);
        } else {
            holder.mAllTopRlv.setPadding(0,0,0,0);
            holder.mDistributionLlv.setVisibility(View.GONE);
        }

        holder.mDistributionLlv.setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                // 判断是否需要展示分销金额，不展示直接跳转海报页面
                if (DistrbutionHelper.isNeedJumpPop(mContext,model.level1CommissionEmployee,model.level2CommissionEmployee,model.companyCode)) {
                    MeetingExtensionPopView popView = new MeetingExtensionPopView(mContext);
                    popView.setMeetingData(TextUtils.isEmpty(model.roomCode) ? model.roomId : model.roomCode
                            ,model.roomLiveId
                            ,model.title
                            ,!TextUtils.isEmpty(model.coverOne) ? model.coverOne : model.coverTwo
                            , DistrbutionHelper.getDistributionMoney(mContext
                                    ,model.level1Commission
                                    ,model.level2Commission
                                    ,model.level1CommissionEmployee
                                    ,model.level2CommissionEmployee
                                    ,model.companyCode)
                            ,model.minPerson
                            ,model.minViewTime);
                    popView.show();
                } else {
                    // 判断二维码内容是否为空，为空生成海报没有意义
                    if (!TextUtils.isEmpty(BaseUserInfoCache.getUserInviteUrl(mContext))) {
                        ARouter.getInstance()
                                .build(RouterPathProvider.ONLINE_MEETING_EXTEN_SION_POSTER)
                                .withInt("fromType", 1)
                                .withString("mLiveTitle", model.title)
                                .withString("mLiveId", model.roomLiveId)
                                .withString("mLiveCoverUrl", !TextUtils.isEmpty(model.coverOne) ? model.coverOne : model.coverTwo)
                                .withString("mRoomId", TextUtils.isEmpty(model.roomCode) ? model.roomId : model.roomCode)
                                .navigation();

                    } else {
                        Toasty.normal(mContext, mContext.getResources().getString(R.string.extension_qr_error)).show();
                    }
                }
            }
        });
    }

    public void joinRoom(int postion) {
        LiveRoomInfoProvider.getInstance().joinPassword = "";
        LiveRoomInfoProvider.getInstance().liveId = mDatas.get(postion).roomLiveId;
        ((UI) mContext).showProgressHUD(mContext, "加载中");
        LiveRoomIOHelper.joinLiveRoom(mContext, new IOLiveRoomListener() {
            @Override
            public void onIOError(String error, int errorCode) {
                ((UI) mContext).dismissProgressHUD();
                if (!TextUtils.isEmpty(error)) {
                    Toasty.normal(mContext, error).show();
                }
            }

            @Override
            public void onIOSuccess() {
                ((UI) mContext).dismissProgressHUD();
//                ((UserHomePageAct) mContext).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * 必须重写不然会出现滑动不流畅的情况
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mViewTypeItem;
    }

    /**
     * 正常条目的item的ViewHolder
     */
    static class UserHomePageLiveViewHolder extends RecyclerView.ViewHolder {

        public ImageView mRoomCoverIv;
        private TextView mRoomIdTv;
        private TextView mTitleTv;
        private LinearLayout mProjectTagLlv;
        private View rootView;
        private RelativeLayout mRlvAnimContainer;
        public TextView mLiveStatusAlreadyTv;
        public TextView mLiveSeeCountTv;
        private ImageView mIvLive;
        // 增加直播状态标签父组件,状态显示有父组件控制显示或隐藏
        private LinearLayout ll_bg_gifview;

        private LinearLayout mDistributionLlv;
        private RelativeLayout mAllTopRlv;
        private TextView mDistributionMoneyTv;

        public UserHomePageLiveViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            mRoomCoverIv = itemView.findViewById(R.id.marquee_view);
            mRoomIdTv = itemView.findViewById(R.id.tv_room_id);
            mTitleTv = itemView.findViewById(R.id.tv_title);
            mProjectTagLlv = itemView.findViewById(R.id.llv_project_tag);
            mRlvAnimContainer = itemView.findViewById(R.id.rlv_anim_container);
            mLiveStatusAlreadyTv = itemView.findViewById(R.id.tv_live_status_already);
            mLiveSeeCountTv = itemView.findViewById(R.id.tv_see_count);
            mIvLive = itemView.findViewById(R.id.iv_live_play_already);
            ll_bg_gifview= itemView.findViewById(R.id.ll_bg_gifview);
            mDistributionLlv = itemView.findViewById(R.id.llv_distribution);
            mAllTopRlv = itemView.findViewById(R.id.rlv_all_top);
            mDistributionMoneyTv = itemView.findViewById(R.id.tv_distribution_money);
        }
    }
}
