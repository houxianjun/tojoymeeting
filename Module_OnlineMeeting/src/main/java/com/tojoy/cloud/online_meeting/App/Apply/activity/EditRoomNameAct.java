package com.tojoy.cloud.online_meeting.App.Apply.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.editView.ClearEmojInputFilter;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.util.sys.KeyboardControlUtil;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * @author qululu
 * 开会申请：设置房间名称、设置密码页面
 */
@Route(path = RouterPathProvider.ONLINE_MEETING_EDIT_ROOM_NAME)
public class EditRoomNameAct extends UI {
    private ImageView mIvEditClear;
    private EditText mEditRoomName;
    private String mType;
    private int maxNumber;
    private boolean mIsFirstShowToast = true;
    private String loaclData = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_room_name);
        mType = getIntent().getStringExtra("type");
        loaclData = getIntent().getStringExtra("loaclData");
        setStatusBar();
        initTitle();
        initUI();
        initListener();
    }

    private void initTitle() {
        setUpNavigationBar();
        setTitle(getIntent().getStringExtra("title"));
        showBackCancelTitle();
        mleftCancleTv.setTextColor(Color.parseColor("#596271"));
        setRightManagerTitle("完成");
        mRightManagerTv.setTextColor(getResources().getColor(com.tojoy.tjoybaselib.R.color.color_2a62dd));

    }

    /**
     * 重写取消按钮
     */
    @Override
    public void showBackCancelTitle(){
        if (mleftCancleTv != null) {
            mleftCancleTv.setVisibility(View.VISIBLE);
            mleftCancleTv.setOnClickListener(v -> {
                KeyboardControlUtil.closeKeyboard((EditText) findViewById(R.id.edit_room_name), EditRoomNameAct.this);
                EditRoomNameAct.this.finish();
            });
        }
    }

    private void initUI() {
        mIvEditClear = (ImageView) findViewById(R.id.iv_edit_clear);
        mEditRoomName = (EditText) findViewById(R.id.edit_room_name);
        mEditRoomName.setText(TextUtils.isEmpty(loaclData) ? "" : loaclData);
    }

    private void initListener() {
        // 一键清除
        mIvEditClear.setOnClickListener(v -> {
            mEditRoomName.setText("");
        });

        if ("psw".equals(mType)) {
            maxNumber = 6;
            mEditRoomName.setHint(getResources().getString(R.string.apply_power_set_room_psw_hint));
            mEditRoomName.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else if ("editUserName".equals(mType)) {
            maxNumber = 16;
            mEditRoomName.setHint("请输入姓名");
            if (!TextUtils.isEmpty(BaseUserInfoCache.getUserName(this))) {
                mEditRoomName.setText(BaseUserInfoCache.getUserName(this));
            }
        } else {
            maxNumber = 20;
        }
        mEditRoomName.setMaxEms(maxNumber);
        InputFilter[] emojiFilters = {new ClearEmojInputFilter()};
        mEditRoomName.setFilters(emojiFilters);
        //禁止换行
        mEditRoomName.setOnEditorActionListener((v, actionId, event) -> actionId == EditorInfo.IME_ACTION_SEND
                || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER));
        mEditRoomName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int length = charSequence.length();
                if (length > 0 && mIvEditClear.getVisibility() != View.VISIBLE) {
                    mIvEditClear.setVisibility(View.VISIBLE);
                } else if (length == 0) {
                    mIvEditClear.setVisibility(View.GONE);
                }

                if (length > maxNumber) {
                    mEditRoomName.setText(mEditRoomName.getText().subSequence(0, maxNumber));
                    mEditRoomName.setSelection(maxNumber);
                    if (mIsFirstShowToast) {
                        Toasty.normal(EditRoomNameAct.this, "已达最大字数限制").show();
                        mIsFirstShowToast = false;
                    }
                } else if (length < maxNumber) {
                    mIsFirstShowToast = true;
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        mHandler.postDelayed(() -> showKeyboard(true), 500);

        if(mEditRoomName.getText() != null){
            mEditRoomName.setSelection(mEditRoomName.length());
        }

    }

    @Override
    protected void onRightManagerTitleClick() {
        String roomName = mEditRoomName.getText().toString();

        if ("editUserName".equals(mType)) {
            if (TextUtils.isEmpty(roomName)) {
                Toasty.normal(this, "请输入姓名").show();
                return;
            }

            OMAppApiProvider.getInstance().mEditUserName(BaseUserInfoCache.getUserId(EditRoomNameAct.this),roomName, new Observer<OMBaseResponse>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onNext(OMBaseResponse omBaseResponse) {
                    if (omBaseResponse.isSuccess()) {
                        BaseUserInfoCache.setUserName(roomName, EditRoomNameAct.this);
                        KeyboardControlUtil.closeKeyboard((EditText) findViewById(R.id.edit_room_name), EditRoomNameAct.this);
                        EditRoomNameAct.this.finish();
                    } else {
                        Toasty.normal(EditRoomNameAct.this, omBaseResponse.msg).show();
                    }
                }
            });

        } else {
            if (TextUtils.isEmpty(roomName)) {
                if (!"psw".equals(mType)) {
                    Toasty.normal(this, getResources().getString(R.string.apply_power_room_name_hint)).show();
                    return;
                }
            } else {
                if ("psw".equals(mType)) {
                    // 检验密码的位数
                    if (roomName.length() != 6) {
                        Toasty.normal(this, getResources().getString(R.string.apply_power_set_room_psw_hint)).show();
                        return;
                    }
                }
            }

            Intent intent = new Intent();
            intent.putExtra("value", TextUtils.isEmpty(roomName) ? "" : roomName);
            setResult(Activity.RESULT_OK, intent);
            KeyboardControlUtil.closeKeyboard((EditText) findViewById(R.id.edit_room_name), EditRoomNameAct.this);
            EditRoomNameAct.this.finish();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
