package com.tojoy.cloud.online_meeting.App.Live.CollectOuterLive;

import android.app.Dialog;
import android.content.Context;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.R;

public class OuterLiveLimitPop {
    private Context mContext;
    private View mView;
    private Dialog alertDialog;

    OuterLiveLimitPop(Context context) {
        mContext = context;
        initUI();
    }

    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_outlive_limit_tip_layout, null);
        mView.findViewById(R.id.btn_makesure).setOnClickListener(v -> dismiss());
    }

    public void show() {
        int width = AppUtils.getWidth(mContext) - AppUtils.dip2px(mContext, 114);
        int height = 355 * width / 510;
        alertDialog = new Dialog(mContext, com.tojoy.tjoybaselib.R.style.tjoy_dialog_style);
        alertDialog.setContentView(mView, new ViewGroup.LayoutParams(width, height));
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.setCanceledOnTouchOutside(false);
        try {
            alertDialog.show();
            alertDialog.setOnKeyListener((dialog, keyCode, event) -> true);
        } catch (Exception e) {

        }
    }

    public boolean isShowing() {
        if (alertDialog == null) {
            return false;
        }
        return alertDialog.isShowing();
    }

    public void dismiss() {
        if (isShowing()) {
            alertDialog.dismiss();
        }
    }

    void setOutliveCount(String count, String delCount) {
        TextView outliveCount = mView.findViewById(R.id.tv_center_tip);
        String title = "<font color='#596271'>引流内容有</font><font color='#0f88eb'>" + count + "</font><font color='#596271'>个视频流</font>";
        outliveCount.setText(Html.fromHtml(title));

        TextView titleTv = mView.findViewById(R.id.tv_title_left);
        titleTv.setText("添加失败，视频流已达上限\n删除" + delCount + "个已引流视频再来试试吧");
    }
}
