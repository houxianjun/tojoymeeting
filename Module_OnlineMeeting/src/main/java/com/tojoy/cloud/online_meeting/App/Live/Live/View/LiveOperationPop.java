package com.tojoy.cloud.online_meeting.App.Live.Live.View;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigCacheManager;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigConstantKey;
import com.tojoy.tjoybaselib.ui.dialog.PickerViewAnimateUtil;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.netease.nim.uikit.business.liveroom.FileShowCallback;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.LiveCustomMessageProvider;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.OnlineMembersHelper;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;
import com.tojoy.cloud.online_meeting.R;

import es.dmoral.toasty.Toasty;
import rx.Observer;

public class LiveOperationPop implements FileShowCallback {

    private Context context;
    protected ViewGroup contentContainer;
    private Animation outAnim;
    private Animation inAnim;
    private boolean isAnim = true;
    private boolean isShowing;
    private int gravity = Gravity.BOTTOM;
    public ViewGroup decorView;
    public ViewGroup rootView;
    private boolean dismissing;
    private OperationCallback mOperationCallback;

    private int selectViewId = 0;

    private TextView mTvMuteAll;
    private TextView mTvSameTableLive;
    private TextView mTvAskMoney;
    private TextView mTvUseMeetingPic;
    public int mSameTableState;

    public LiveOperationPop(Context context, OperationCallback operationCallback) {
        this.context = context;
        this.mOperationCallback = operationCallback;
        init();
        initViews();
        setOutSideCancelable(true);
    }

    protected void init() {
        inAnim = getInAnimation();
        outAnim = getOutAnimation();
    }

    private void initViews() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        //decorView是activity的根View
        if (decorView == null) {
            decorView = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        }

        //将控件添加到decorView中
        rootView = (ViewGroup) layoutInflater.inflate(R.layout.live_operation_popview, decorView, false);
        rootView.setLayoutParams(new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        ));
        rootView.setBackgroundColor(context.getResources().getColor(R.color.colorTranslucent));

        rootView.findViewById(R.id.iv_close).setOnClickListener(v -> {
            dismiss();
            showBottomView();
        });

        mTvSameTableLive = rootView.findViewById(R.id.tvSameTableLive);

        setKeyBackCancelable(true);

        contentContainer = rootView.findViewById(R.id.content_container);

        mTvMuteAll = rootView.findViewById(R.id.tvMuteAll);

        mTvAskMoney = rootView.findViewById(R.id.ask_money);
        mTvUseMeetingPic = rootView.findViewById(R.id.use_meeting_pic);

        mTvAskMoney.setText(TextUtils.isEmpty(TextConfigCacheManager.getInstance(context).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION4))
                ? "发布商品" : TextConfigCacheManager.getInstance(context).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION4));

        mTvUseMeetingPic.setText(TextUtils.isEmpty(TextConfigCacheManager.getInstance(context).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION5))
                ? "引用会议画面" : TextConfigCacheManager.getInstance(context).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION5));


        rootView.findViewById(R.id.llv_op_sub3).setOnClickListener(v -> {
            showHintDialog(R.id.llv_op_sub3);
        });

        rootView.findViewById(R.id.llv_op_sub2).setOnClickListener(v -> {
            showHintDialog(R.id.llv_op_sub2);
        });

        rootView.findViewById(R.id.llv_op_sub4).setOnClickListener(v -> {
            showHintDialog(R.id.llv_op_sub4);
        });
        rootView.findViewById(R.id.llv_op_sub5).setOnClickListener(v -> {
            showHintDialog(R.id.llv_op_sub5);
        });

        rootView.findViewById(R.id.llv_op_sub6).setOnClickListener(v -> {
            showHintDialog(R.id.llv_op_sub6);
        });

        rootView.findViewById(R.id.llv_op_sub12).setOnClickListener(v -> {
            showHintDialog(R.id.llv_op_sub12);
        });

        rootView.findViewById(R.id.llv_op_qr).setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                dismiss();
                mOperationCallback.startQR();
            }
        });

        rootView.findViewById(R.id.llv_op_sub8).setOnClickListener(v -> {
            if ("全体禁言".equals(mTvMuteAll.getText().toString())) {
                mute("true");
                LogUtil.i(LiveLogTag.BaseLiveTag, "muteAll");
            } else {
                mute("false");
                LogUtil.i(LiveLogTag.BaseLiveTag, "deMuteAll");
            }
        });

        rootView.findViewById(R.id.llv_op_sub11).setOnClickListener(v -> {
            if (LiveRoomInfoProvider.getInstance().isHost()) {
                mOperationCallback.stopLive(true, true);
            } else {
                ((BaseLiveAct) context).stopLiveByGuestOrOfficalHost();
                ((BaseLiveAct) context).showBottomView(true);
            }
            dismiss();
        });


        //操作按钮
        rootView.findViewById(R.id.llv_op_sub1).setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                showHintDialog(R.id.llv_op_sub1);
            }
        });


        rootView.findViewById(R.id.llv_op_sub9).setOnClickListener(v -> {
            showHintDialog(R.id.llv_op_sub9);
        });
        rootView.findViewById(R.id.llv_op_sub10).setOnClickListener(v -> {
            mOperationCallback.showReport();
            dismiss();
        });

        // 开播提醒
        rootView.findViewById(R.id.llv_op_sub13).setOnClickListener(v -> {
            if (FastClickAvoidUtil.isFastDoubleClick(R.id.llv_op_sub13)) {
                return;
            }
            new TJMakeSureDialog(context, "是否向邀请但未参会的用户\n发送开会提醒",
                    view -> {
                        LiveRoomIOHelper.livePushRemind(context, LiveRoomInfoProvider.getInstance().roomId,
                                LiveRoomInfoProvider.getInstance().hostUserId, LiveRoomInfoProvider.getInstance().liveId);
                    }).setBtnText("发送", "不发送").show();
        });

        //5.21 互动弹窗的空白页面不能点击
        rootView.findViewById(R.id.content_container).setOnClickListener(v -> {

        });
        refreshContainerHeight();
    }

    public void refreshMuteBtnState() {
        rootView.findViewById(R.id.ivMute).setBackgroundResource(
                LiveRoomInfoProvider.getInstance().isMuteAll ? R.drawable.icon_live_unmute : R.drawable.icon_liveop_11);
        mTvMuteAll.setText(LiveRoomInfoProvider.getInstance().isMuteAll ? "解除禁言" : "全体禁言");
    }

    private void mute(String type) {

        OMAppApiProvider.getInstance().muteAll(LiveRoomInfoProvider.getInstance().imRoomId, LiveRoomInfoProvider.getInstance().liveId,
                type, new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(context, "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        if (response.isSuccess()) {
                            mTvMuteAll.setText("true".equals(type) ? "解除禁言" : "全体禁言");
                            Toasty.normal(context, "true".equals(type) ? "已开启全员禁言" : "已关闭全员禁言").show();

                            rootView.findViewById(R.id.ivMute).setBackgroundResource("true".equals(type) ? R.drawable.icon_live_unmute : R.drawable.icon_liveop_11);

                        } else {
                            Toasty.normal(context, response.msg).show();
                        }
                    }
                });
    }

    public void controlCarema() {
        //1.观众\助手只有在连麦的时候才显示摄像头
        //2.主播只有在自己开的直播的时候才显示摄像头
        if (LiveRoomInfoProvider.getInstance().isHost()) {
            if (LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                rootView.findViewById(R.id.llv_op_sub12).setVisibility(View.GONE);
            } else {
                rootView.findViewById(R.id.llv_op_sub12).setVisibility(View.VISIBLE);
            }
        } else {
            if (OnlineMembersHelper.getInstance().isMyselfExploring(context)) {
                rootView.findViewById(R.id.llv_op_sub12).setVisibility(View.VISIBLE);
            } else {
                rootView.findViewById(R.id.llv_op_sub12).setVisibility(View.GONE);
            }
        }
    }

    private void showBottomView() {
        new Handler().postDelayed(() -> mOperationCallback.showBottomView(true), 400);
    }


    /**
     * @param v      (是通过哪个View弹出的)
     * @param isAnim 是否显示动画效果
     */
    public void show(View v, boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(View v) {
        show();
    }

    public void show() {
        if (isShowing()) {
            return;
        }
        isShowing = true;
        onAttached(rootView);
        rootView.requestFocus();
    }

    private void onAttached(View view) {
        decorView.addView(view);
        if (isAnim) {
            contentContainer.startAnimation(inAnim);
        }
    }

    public boolean isShowing() {
        return rootView.getParent() != null || isShowing;
    }


    public void dismiss() {
        if (dismissing) {
            dismissImmediately();
            contentContainer.clearAnimation();
            return;
        }
        if (isAnim) {
            //消失动画
            outAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    dismissImmediately();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            contentContainer.startAnimation(outAnim);
        } else {
            dismissImmediately();
        }
        dismissing = true;
    }

    public void dismissImmediately() {
        decorView.post(() -> {
            //从根视图移除
            decorView.removeView(rootView);
            isShowing = false;
            dismissing = false;
        });

    }

    public Animation getInAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, true);
        return AnimationUtils.loadAnimation(context, res);
    }

    public Animation getOutAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, false);
        return AnimationUtils.loadAnimation(context, res);
    }

    public void setKeyBackCancelable(boolean isCancelable) {
        ViewGroup View;
        View = rootView;
        View.setFocusable(isCancelable);
        View.setFocusableInTouchMode(isCancelable);
        if (isCancelable) {
            View.setOnKeyListener(onKeyBackListener);
        } else {
            View.setOnKeyListener(null);
        }
    }

    private View.OnKeyListener onKeyBackListener = (v, keyCode, event) -> {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == MotionEvent.ACTION_DOWN
                && isShowing()) {
            dismiss();
            mOperationCallback.showBottomView(true);
            return true;
        }
        return false;
    };

    /**
     * Called when the user touch on black overlay in order to dismiss the dialog
     */
    @SuppressLint("ClickableViewAccessibility")
    private final View.OnTouchListener onCancelableTouchListener = (v, event) -> {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            dismiss();
            showBottomView();
        }
        return false;
    };

    public void setOutSideCancelable(boolean isCancelable) {
        if (rootView != null) {
            View view = rootView.findViewById(com.netease.nim.uikit.R.id.outmost_container);

            if (isCancelable) {
                view.setOnTouchListener(onCancelableTouchListener);
            } else {
                view.setOnTouchListener(null);
            }
        }
    }

    /**
     * 初始化、获取到活动、更改身份后更新
     */
    public void refreshContainerHeight() {
        int height = AppUtils.dip2px(context, 45) + AppUtils.dip2px(context, 73) + AppUtils.dip2px(context, 139);

        if (!LiveRoomInfoProvider.getInstance().isGuest()) {
            height = height + AppUtils.dip2px(context, 73);
        }

        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height);
        rootView.findViewById(R.id.container_base).setLayoutParams(lps);


        if (LiveRoomInfoProvider.getInstance().isGuest()) {
            rootView.findViewById(R.id.llv_op_sub3).setVisibility(View.GONE);
            rootView.findViewById(R.id.llv_op_sub4).setVisibility(View.GONE);
            rootView.findViewById(R.id.llv_op_sub5).setVisibility(View.GONE);
            rootView.findViewById(R.id.llv_op_sub6).setVisibility(View.GONE);
            rootView.findViewById(R.id.llv_op_sub8).setVisibility(View.GONE);
            rootView.findViewById(R.id.llv_line2).setVisibility(View.GONE);
            rootView.findViewById(R.id.llv_op_sub10).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.llv_op_sub13).setVisibility(View.GONE);
            rootView.findViewById(R.id.llv_op_qr).setVisibility(View.GONE);
            rootView.findViewById(R.id.llv_op_sub2).setVisibility(View.GONE);
        } else {
            rootView.findViewById(R.id.llv_op_sub2).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.llv_op_sub3).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.llv_op_sub4).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.llv_op_sub5).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.llv_op_sub6).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.llv_line2).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.llv_op_sub10).setVisibility(View.GONE);
            rootView.findViewById(R.id.llv_op_sub13).setVisibility(View.GONE);
            rootView.findViewById(R.id.llv_op_qr).setVisibility(View.VISIBLE);
        }


        if (!LiveRoomInfoProvider.getInstance().isHost()) {
            rootView.findViewById(R.id.iv_close_live).setBackgroundResource(R.drawable.icon_liveop_8);
            ((TextView) rootView.findViewById(R.id.tv_close_live)).setText("离开");
            rootView.findViewById(R.id.llv_op_sub8).setVisibility(View.GONE);

        }
        refreshSameTableState();
    }

    public void refreshSameTableState() {
        String title = TextUtils.isEmpty(TextConfigCacheManager.getInstance(context).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1)) ? "同台互动" :
                TextConfigCacheManager.getInstance(context).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1);
        mTvSameTableLive.setText(mSameTableState == 1 ? (LiveRoomInfoProvider.getInstance().isHostHelper() ? title : "取消互动") : title);

    }

    public void refreshTemplateLive() {
        ((TextView) rootView.findViewById(R.id.tv_template_live)).setText(LiveRoomInfoProvider.getInstance().hasTemplateLeave ? "取消暂离" : "暂离");
    }

    public void showHintDialog(int viewId) {
        if (LiveRoomInfoProvider.getInstance().isShowFile && !LiveRoomInfoProvider.getInstance().isGuest()) {
            // 若当前正在展示文档演示，且当前身份不是观众
            new TJMakeSureDialog(context, "是否结束会议文件？",
                    v -> {
                        // 继续演示，关闭弹窗

                    }).setBtnText("继续演示", "结束").setCancelListener(() -> {
                // 离开，退出文档
                selectViewId = viewId;
                if (mOperationCallback != null) {
                    mOperationCallback.exitFileShow(this);
                }
            }).show();
        } else {
            // 否则直接响应操作
            viewClick(viewId);
        }
    }

    /**
     * @param id
     */
    public void viewClick(int id) {
        if (id == R.id.llv_op_sub1) {
            // 判断是否有权限
            if ("取消互动".equals(mTvSameTableLive.getText().toString())) {
                LogUtil.i(LiveLogTag.BaseLiveTag, "互动弹窗-取消同台");
                mOperationCallback.disExploreMe();
                OMAppApiProvider.getInstance().mStopExploreSomebody(BaseUserInfoCache.getUserId(context),
                        LiveRoomInfoProvider.getInstance().liveId, new Observer<OMBaseResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(OMBaseResponse response) {

                            }
                        });


                String title = TextUtils.isEmpty(TextConfigCacheManager.getInstance(context).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1)) ? "同台互动" :
                        TextConfigCacheManager.getInstance(context).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1);
                mTvSameTableLive.setText(title);

            } else {
                if (LiveRoomInfoProvider.getInstance().isGuest()) {
                    if (LiveRoomInfoProvider.getInstance().isMute) {
                        Toasty.normal(context, "您已被主播禁言，无法发起同台").show();
                    } else {
                        mOperationCallback.applyExporeMe();
                    }
                } else {
                    dismiss();
                    mOperationCallback.inviteExpore();
                    new android.os.Handler().postDelayed(() -> mOperationCallback.showBottomView(true), 300);
                }
            }
        } else if (id == R.id.llv_op_sub3) {
            // 手绘板
            dismiss();
            mOperationCallback.showBottomView(true);
            mOperationCallback.showDrawPad(true);
        } else if (id == R.id.llv_op_sub2) {
            // 会议文件
            dismiss();
            mOperationCallback.showBottomView(true);
            mOperationCallback.showFileShow("", true);
        } else if (id == R.id.llv_op_sub4) {
            // 发起成交
            dismiss();
            mOperationCallback.startDeal();
        } else if (id == R.id.llv_op_sub5) {
            // 视频引流
            mOperationCallback.selectOuterLive();
            dismiss();
        } else if (id == R.id.llv_op_sub6) {
            // 暂离
            if (LiveRoomInfoProvider.getInstance().hasTemplateLeave) {
                LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                        LiveCustomMessageProvider.getHostReStartData(context), false);
                mOperationCallback.finishTempleteLeave();
                mOperationCallback.showBottomView(true);
            } else {
                mOperationCallback.onClickTempleteLeave();
            }
            dismiss();
        } else if (id == R.id.llv_op_sub9) {
            // 公告
            mOperationCallback.showNotice();
            dismiss();
        } else if (id == R.id.llv_op_sub12) {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                // 翻转摄像头
                mOperationCallback.switchCamera();
            }
        }
    }

    /**
     * 退出文档演示成功
     */
    @Override
    public void exitFileShowSuccess() {
        viewClick(selectViewId);
    }

}
