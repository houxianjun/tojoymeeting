package com.tojoy.cloud.online_meeting.App.Poster.View;

import android.Manifest;
import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.Services.PayAndShare.WXShareUtil;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.media.ImageUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.MathematicalUtils;
import com.tojoy.tjoybaselib.util.sys.ViewUtil;
import com.yanzhenjie.permission.AndPermission;

import es.dmoral.toasty.Toasty;

/**
 * 海报缩略图
 * @author qll
 */
public class ThumbnailPosterView {
    private Activity mContext;
    private View mRootView;
    private ImageView mSmallPosterIv;
    private LinearLayout mShareLlv;
    private Bitmap posterBitmap;

    public ThumbnailPosterView(Activity context){
        mContext = context;
        getView();
    }

    public View getView() {
        if (mRootView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mRootView = aLayoutInflater.inflate(R.layout.layout_poster_share_bottom, null);
        }
        initView();
        return mRootView;
    }

    private void initView() {
        mSmallPosterIv = mRootView.findViewById(R.id.iv_small_poster);
        mShareLlv = mRootView.findViewById(R.id.llv_share);
        mShareLlv.setVisibility(View.GONE);

        // 海报缩略图大小
        RelativeLayout.LayoutParams shareSmallParams = (RelativeLayout.LayoutParams) mSmallPosterIv.getLayoutParams();
        double heighSmallProportion = MathematicalUtils.div(AppUtils.getHeight(mContext) - AppUtils.dip2px(mContext, 300), AppUtils.getHeight(mContext), 2);
        shareSmallParams.height = (int) (AppUtils.getHeight(mContext) * heighSmallProportion);
        shareSmallParams.width = (int) (AppUtils.getScreenWidth(mContext) * heighSmallProportion);
        mSmallPosterIv.setLayoutParams(shareSmallParams);

        mRootView.findViewById(R.id.tv_wechat_moments).setOnClickListener(v -> {
            // 微信朋友圈
            if (!FastClickAvoidUtil.isDoubleClick()) {
                WXShareUtil.shareImage(mContext, posterBitmap, false);
                mShareLlv.setVisibility(View.GONE);
            }
        });
        mRootView.findViewById(R.id.tv_wechat_firends).setOnClickListener(v -> {
            // 微信好友
            if (!FastClickAvoidUtil.isDoubleClick()) {
                WXShareUtil.shareImage(mContext, posterBitmap, true);
                mShareLlv.setVisibility(View.GONE);
            }
        });
        mRootView.findViewById(R.id.tv_save_pic).setOnClickListener(v -> {
            // 保存到相册
            if (!FastClickAvoidUtil.isDoubleClick()) {
                savePic();
            }
        });
        mRootView.findViewById(R.id.tv_cancle_share).setOnClickListener(v -> {
            // 取消分享
            if (!FastClickAvoidUtil.isDoubleClick()) {
                mShareLlv.setVisibility(View.GONE);
                if (posterBitmap != null) {
                    posterBitmap.recycle();
                }
            }
        });
        mRootView.setOnClickListener(v -> {

        });
    }

    /**
     * 根据布局view生成海报图片，并且展示缩略图及底部分享弹窗
     */
    public void showThumbnailPoster(ViewGroup viewGroup){
        posterBitmap = ViewUtil.getViewGroupBitmap(viewGroup);
        ImageLoaderManager.INSTANCE.loadImage(mContext, mSmallPosterIv, posterBitmap, AppUtils.dip2px(mContext, 5));
        mShareLlv.setVisibility(View.VISIBLE);
    }

    /**
     * 隐藏缩略图的布局以及底部分享框
     */
    public void goneThumbnailPoster(){
        mShareLlv.setVisibility(View.GONE);
    }

    /**
     * 保存图片到相册
     */
    private void savePic() {
        AndPermission.with(mContext)
                .permission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .rationale((context, permissions, executor) -> {
                    // 此处可以选择显示提示弹窗
                    executor.execute();
                })
                // 用户给权限了
                .onGranted(permissions -> {
                    new Thread(() -> {
                        try {
                            ImageUtil.saveImageToGallery(mContext, posterBitmap);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }).start();
                    Toasty.normal(mContext, "保存图片成功,请到相册查看!").show();
                    mShareLlv.setVisibility(View.GONE);
                })
                // 用户拒绝权限，包括不再显示权限弹窗也在此列
                .onDenied(permissions -> {
                    // 没权限，拒绝了也要统计
                    Toasty.normal(mContext, "请开启权限再进行保存!").show();
                })
                .start();
    }
}
