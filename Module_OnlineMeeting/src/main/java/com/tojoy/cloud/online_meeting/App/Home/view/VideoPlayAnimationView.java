package com.tojoy.cloud.online_meeting.App.Home.view;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

import com.tojoy.cloud.online_meeting.R;


/**
 * @author fanxi
 * @date 2019/6/17.
 * description： 云洽会视屏播放音量动画
 */
public class VideoPlayAnimationView  {
    private Activity mContext;
    private View mRootView;

    public VideoPlayAnimationView(Activity context) {
        mContext = context;
        getView();

    }

    public View getView() {
        if (mRootView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mRootView = aLayoutInflater.inflate(R.layout.view_videoplay_animation_layout, null);
        }
        initView();
        return mRootView;
    }



    public void initView() {
        View mView1 = mRootView.findViewById(R.id.view1);
        View mView2 = mRootView.findViewById(R.id.view2);
        View mView3 = mRootView.findViewById(R.id.view3);
        View mView4 = mRootView.findViewById(R.id.view4);

        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.0f, 1f, 0.1f,Animation.RELATIVE_TO_PARENT
                ,1.0f,Animation.RELATIVE_TO_PARENT
                ,1.0f);
        scaleAnimation.setDuration(400);
        scaleAnimation.setRepeatMode(Animation.REVERSE);
        scaleAnimation.setRepeatCount(-1);
        mView1.startAnimation(scaleAnimation);


        ScaleAnimation scaleAnimation1 = new ScaleAnimation(1.0f, 1.0f, 1f, 0.1f,Animation.RELATIVE_TO_PARENT
                ,1.0f,Animation.RELATIVE_TO_PARENT
                ,1.0f);
        scaleAnimation1.setDuration(350);
        scaleAnimation1.setRepeatMode(Animation.REVERSE);
        scaleAnimation1.setRepeatCount(-1);
        mView2.startAnimation(scaleAnimation1);



        ScaleAnimation scaleAnimation12 = new ScaleAnimation(1.0f, 1.0f, 1f, 0.1f,Animation.RELATIVE_TO_PARENT
                ,1.0f,Animation.RELATIVE_TO_PARENT
                ,1.0f);
        scaleAnimation12.setDuration(450);
        scaleAnimation12.setRepeatMode(Animation.REVERSE);
        scaleAnimation12.setRepeatCount(-1);
        mView3.startAnimation(scaleAnimation12);



        ScaleAnimation scaleAnimation13 = new ScaleAnimation(1.0f, 1.0f, 1f, 0.1f,Animation.RELATIVE_TO_PARENT
                ,1.0f,Animation.RELATIVE_TO_PARENT
                ,1.0f);
        scaleAnimation13.setDuration(500);
        scaleAnimation13.setRepeatMode(Animation.REVERSE);
        scaleAnimation13.setRepeatCount(-1);
        mView4.startAnimation(scaleAnimation13);
    }

}
