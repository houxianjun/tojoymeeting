package com.tojoy.cloud.online_meeting.App.Live.Live.Model;

public class OnlineMember {
    public String userId;
    public long enterTime;
    public boolean isOpenAudio;
    public boolean isOpenVideo;
    /**
     * 是否是主播主动关闭同台
     */
    public boolean isHostCloseExpore;
    /**
     * 是否正在拨打电话：
     * true：正在接听
     * false：挂断/未接听状态（默认）
     */
    public boolean isCallPhoneIng;

    public int width;
    public int height;

    public boolean isAtBigScreen;

    public OnlineMember(String userId) {
        this.userId = userId;
        this.isOpenVideo = true;
        this.isOpenAudio = true;
    }
}
