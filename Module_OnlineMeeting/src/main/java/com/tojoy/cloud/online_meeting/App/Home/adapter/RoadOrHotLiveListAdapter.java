package com.tojoy.cloud.online_meeting.App.Home.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.bumptech.glide.Glide;
import com.tojoy.common.Services.Distribution.DistrbutionHelper;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.model.live.OnlineMeetingHomeGridModel;
import com.tojoy.common.LiveRoom.MeetingExtensionPopView;
import com.tojoy.cloud.online_meeting.App.Home.view.OnlineMeetingHomeGridTagsView;
import com.tojoy.cloud.online_meeting.App.Home.view.VideoPlayAnimationView;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;

import es.dmoral.toasty.Toasty;

import static com.tojoy.tjoybaselib.ui.recyclerview.adapter.IRecyclerView.FOOTER_VIEW;

/**
 * Created by qll on 2019/4/23.
 * 热门直播间、精选路演师页面列表适配器
 */

public class RoadOrHotLiveListAdapter extends BaseRecyclerViewAdapter<OnlineMeetingHomeGridModel> {
    //1：定制首页热门直播间过来需要做特殊处理
    //2：热门列表
    private int mEnterType;

    public RoadOrHotLiveListAdapter(@NonNull Context context, @NonNull List<OnlineMeetingHomeGridModel> datas, int enterType) {
        super(context, datas);
        mEnterType = enterType;
    }

    @Override
    protected int getViewType(int position, @NonNull OnlineMeetingHomeGridModel data) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position == datas.size() ? FOOTER_VIEW : position;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_online_hot_live_layout;
    }


    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new RoadOrHotLiveListVH(context, parent, getLayoutResId(viewType));
    }

    class RoadOrHotLiveListVH extends BaseRecyclerViewHolder<OnlineMeetingHomeGridModel> {
        private TextView mRoomIdTv;
        private TextView mTitleTv;
        private LinearLayout mProjectTagLlv;
        private RelativeLayout mRlvAnimContainer;
        private VideoPlayAnimationView videoPlayAnimationView;
        private TextView mLiveStatusAlreadyTv;
        private TextView mLiveSeeCountTv;
        private ImageView mIconPlay;
        private ImageView mWycTwoImageView;
        private ImageView mIvLive;
        private int width;
        // 增加直播状态标签父组件,状态显示有父组件控制显示或隐藏
        private LinearLayout llBgGifview;
        private LinearLayout mTitleLlv;
        private TextView mDistributionMoneyTv;
        private LinearLayout mDistributionLlv;

        RoadOrHotLiveListVH(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            mRoomIdTv = itemView.findViewById(R.id.tv_room_id);
            mTitleTv = itemView.findViewById(R.id.tv_title);
            mProjectTagLlv = itemView.findViewById(R.id.llv_project_tag);
            mRlvAnimContainer = itemView.findViewById(R.id.rlv_anim_container);
            videoPlayAnimationView = new VideoPlayAnimationView((Activity) context);
            mRlvAnimContainer.addView(videoPlayAnimationView.getView());
            mLiveStatusAlreadyTv = itemView.findViewById(R.id.tv_live_status_already);
            mLiveSeeCountTv = itemView.findViewById(R.id.tv_see_count);
            mIconPlay = itemView.findViewById(R.id.iv_icon_play);
            mWycTwoImageView = itemView.findViewById(R.id.wyc_view);
            mIvLive = itemView.findViewById(R.id.iv_live_play_already);
            llBgGifview = itemView.findViewById(R.id.ll_bg_gifview);
            if (mEnterType == 1) {
                width = (AppUtils.getWidth(context) - AppUtils.dip2px(context, 30)) / 2;
            } else {
                width = (AppUtils.getWidth(context) - AppUtils.dip2px(context, 26)) / 2;
            }
            mTitleLlv = itemView.findViewById(R.id.llv_title);
            mDistributionMoneyTv = itemView.findViewById(R.id.tv_distribution_money);
            mDistributionLlv = itemView.findViewById(R.id.llv_distribution);
        }

        @Override
        public void onBindData(OnlineMeetingHomeGridModel data, int position) {

            /**
             * 初始化循环播放图片控件：VideoPlayAnimationView
             */
            initWycView(data);
            mTitleTv.setText(data.title);
            if (TextUtils.isEmpty(data.roomCode)) {
                mRoomIdTv.setVisibility(View.GONE);
            } else {
                mRoomIdTv.setVisibility(View.VISIBLE);
                mRoomIdTv.setText(data.getListRoomIdText());
            }
            if (data.tags != null && data.tags.size() > 0) {
                mProjectTagLlv.removeAllViews();
                OnlineMeetingHomeGridTagsView tagsView = new OnlineMeetingHomeGridTagsView(context, data.tags, mProjectTagLlv);
                mProjectTagLlv.addView(tagsView.getView());
            }
            mIconPlay.setVisibility(View.GONE);


            if (!TextUtils.isEmpty(data.status)) {
                // 根据直播状态显示不同标签
                switch (data.status) {
                    case "1":
                        // 预告
                        mLiveStatusAlreadyTv.setVisibility(View.GONE);
                        // 直播状态标签隐藏由父组件控制，以下同理
                        llBgGifview.setVisibility(View.GONE);
                        break;
                    case "2":
                    case "3":
                        // 直播
                        mLiveStatusAlreadyTv.setVisibility(View.GONE);
                        llBgGifview.setVisibility(View.VISIBLE);
                        Glide.with(context).asGif().load(R.drawable.animvideo_player).into(mIvLive);

                        break;
                    case "4":
                        // 回放
                        mLiveStatusAlreadyTv.setVisibility(View.VISIBLE);
                        llBgGifview.setVisibility(View.GONE);
                        break;
                    default:
                        mLiveStatusAlreadyTv.setVisibility(View.GONE);
                        llBgGifview.setVisibility(View.GONE);
                        break;
                }
            } else {
                mLiveStatusAlreadyTv.setVisibility(View.GONE);
            }
            mRlvAnimContainer.setVisibility(View.GONE);
            // 设置观看人数
            mLiveSeeCountTv.setVisibility(View.GONE);
            if (DistrbutionHelper.isShowDistribution(context,data.ifDistribution,data.companyCode)) {
                mTitleLlv.setPadding(0,0,0,AppUtils.dip2px(context,15));
                mDistributionLlv.setVisibility(View.VISIBLE);
                mDistributionMoneyTv.setText(DistrbutionHelper.getMaxBudgetText(context
                        ,data.level1Commission
                        ,data.level2Commission
                        ,data.level1CommissionEmployee
                        ,data.level2CommissionEmployee
                        ,data.companyCode,true));
            } else {
                mDistributionLlv.setVisibility(View.GONE);
                mTitleLlv.setPadding(0,0,0,0);
            }
            mDistributionLlv.setOnClickListener(v -> {
                if (!FastClickAvoidUtil.isDoubleClick()) {
                    // 判断是否需要展示分销金额，不展示直接跳转海报页面
                    if (DistrbutionHelper.isNeedJumpPop(context,data.level1CommissionEmployee,data.level2CommissionEmployee,data.companyCode)) {
                        MeetingExtensionPopView popView = new MeetingExtensionPopView(context);
                        popView.setMeetingData(data.roomId
                                ,data.roomLiveId
                                ,data.title
                                ,!TextUtils.isEmpty(data.verticalBanner) ? data.verticalBanner : data.transverseBanner
                                ,DistrbutionHelper.getDistributionMoney(context
                                        ,data.level1Commission
                                        ,data.level2Commission
                                        ,data.level1CommissionEmployee
                                        ,data.level2CommissionEmployee
                                        ,data.companyCode)
                                ,data.minPerson
                                ,data.minViewTime);
                        popView.show();
                    } else {
                        // 判断二维码内容是否为空，为空生成海报没有意义
                        if (!TextUtils.isEmpty(BaseUserInfoCache.getUserInviteUrl(context))) {
                            ARouter.getInstance()
                                    .build(RouterPathProvider.ONLINE_MEETING_EXTEN_SION_POSTER)
                                    .withInt("fromType", 1)
                                    .withString("mLiveTitle", data.title)
                                    .withString("mLiveId", data.roomLiveId)
                                    .withString("mLiveCoverUrl", !TextUtils.isEmpty(data.verticalBanner) ? data.verticalBanner : data.transverseBanner)
                                    .withString("mRoomId", data.roomId)
                                    .withString("goodsId","")
                                    .withString("goodsTitle","")
                                    .withString("goodsPrice","")
                                    .navigation();

                        } else {
                            Toasty.normal(context, context.getResources().getString(R.string.extension_qr_error)).show();
                        }
                    }

                }
            });
        }

        /**
         * 初始化循环播放图片控件：VideoPlayAnimationView
         */
        private void initWycView(OnlineMeetingHomeGridModel data) {
            ImageLoaderManager.INSTANCE.loadCoverSizedImage(context, mWycTwoImageView, !TextUtils.isEmpty(data.verticalBanner) ? data.verticalBanner : data.transverseBanner, AppUtils.dip2px(context, 8), width, AppUtils.dip2px(context, 173), R.drawable.icon_online_meeting_home_grid_default);
        }
    }
}
