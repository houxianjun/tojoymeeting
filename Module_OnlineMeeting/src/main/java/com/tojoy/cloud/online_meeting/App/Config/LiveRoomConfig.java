package com.tojoy.cloud.online_meeting.App.Config;

import com.tojoy.tjoybaselib.configs.AppConfig;

/**
 * 直播SDK Config Info
 * SDKAppID：
 * 开发dev：1400340972（欠费）、1400325381（新应用）（喻银根）
 * 测试：1400253610、1400325381、1400215491（胡伟临时改）、改回1400253610（老应用）（刘振涛）、1400325377
 * 预生产：1400325377
 * 正式：1400289037（老应用）（龙会平）
 */
public class LiveRoomConfig {

    public static int getTXSDKAppID(){
        switch (AppConfig.environmentInstance) {
            case DEV:
                // 开发dev环境
                return 1400325381;
            case TEST:
                // 测试环境
                return 1400325377;
            case FORMAL:
                // 正式环境
                return 1400289037;
            default:
                // 正式环境
                return 1400289037;
        }
    }
    public final static int ROOM_MAX_USER = 9;

}
