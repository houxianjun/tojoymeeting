package com.tojoy.cloud.online_meeting.App.Live.FileShow;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.netease.nim.uikit.business.liveroom.FileShowCallback;
import com.tojoy.tjoybaselib.model.live.DoumentVideoSendMsg;
import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tencent.teduboard.TEduBoardController;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.model.live.FileFolderModel;
import com.tojoy.tjoybaselib.model.live.FileModel;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.hud.ProgressHUD;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.LiveCustomMessageProvider;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.live.tenxunyun.listener.OnSimpleCLickListener;
import com.tojoy.live.tenxunyun.TxVideoPlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static com.tencent.teduboard.TEduBoardController.TEduBoardToolType.TEDU_BOARD_TOOL_TYPE_PEN;
import static com.tencent.teduboard.TEduBoardController.TEduBoardToolType.TEDU_BOARD_TOOL_TYPE_ZOOM_DRAG;

public class FileShowView {

    private Context mContext;
    private View mView;
    private FrameLayout mWhiteboardView;
    private RelativeLayout mBtnsRlv;
    public RecyclerView mFileListRecycler;
    private View mFileListRecyclerBg;
    private FileBottomListAdapter mAdapter;
    private ArrayList<FileModel> mList = new ArrayList<>();
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private ProgressHUD mProgressHUD;
    private GestureDetector gestureDetector;
    private FileMorePop mFileMorePop;
    //操作回调
    private OperationCallback mOperationCallback;

    private ImageView mDrawView, /*mMoreView,*/
            mClearView;
    private boolean hasHidden = false;
    private boolean canDrawable = false;
    private ArrayList<View> mAnimationControllView = new ArrayList<>();
    private Map<String, String> mBordMaps = new HashMap<>();

    private Map<String, ArrayList<String>> mFileMaps = new HashMap<>();
    // 存储文档关闭前显示在哪页
    private Map<String, Integer> mPdfSelectMaps = new HashMap<>();

    // 是否正在播放视频
    private boolean isStartVideo = false;

    private TxVideoPlayer mTxVideoPlayer;

    private ArrayList<FileModel> mLocalList = new ArrayList<>();
    private int loaclSelectIndex = 0;
    private boolean isNowRootDir = true;

    public boolean isShowBoardCLient = false;

    /**
     * 当前是否是控制身份
     */
    public boolean isInControl;

    private FileShowCallback mFileShowCallback;

    public FileShowView(Context context, OperationCallback operationCallback) {
        mContext = context;
        mOperationCallback = operationCallback;
    }

    public View getView() {
        if (mView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mView = aLayoutInflater.inflate(R.layout.view_fileshow_layout, null);
            initView();
        }
        return mView;
    }

    private void initView() {
        mWhiteboardView = mView.findViewById(R.id.board_view_container);
        mFileListRecycler = mView.findViewById(R.id.recycler_filelist);
        mFileListRecyclerBg = mView.findViewById(R.id.recycler_filelist_bg);
        mBtnsRlv = mView.findViewById(R.id.rlv_bt);
        mTxVideoPlayer = mView.findViewById(R.id.video_player);
        mTxVideoPlayer.isInitShowCenterStart(false);
        mDrawView = mView.findViewById(R.id.icon_draw);
//        mMoreView = mView.findViewById(R.id.icon_more);
        mClearView = mView.findViewById(R.id.icon_clear);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mFileListRecycler.setLayoutManager(linearLayoutManager);
        mFileListRecycler.setItemViewCacheSize(30);
        mAdapter = new FileBottomListAdapter(mContext, mList);
        mAdapter.setHasStableIds(true);
        mFileListRecycler.setAdapter(mAdapter);

        mFileMorePop = new FileMorePop(mContext, mOperationCallback);

        mDrawView.setOnClickListener(v -> {
            canDrawable = !canDrawable;
            disableWhiteboard(canDrawable);
            LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow mDrawViewClick : canDrawable:" + canDrawable + ",isMyselfOpenDocument:" + LiveRoomInfoProvider.getInstance().isMyselfOpenDocument);
            if (canDrawable) {
                mAnimationControllView.clear();
                if (LiveRoomInfoProvider.getInstance().isMyselfOpenDocument) {
                    mAnimationControllView.add(mFileListRecycler);
                    mAnimationControllView.add(mFileListRecyclerBg);
                }
                mAnimationControllView.add(mClearView);
                mAnimationControllView.add(mDrawView);
//                mAnimationControllView.add(mMoreView);

                hideGraientView();
                mReSelectFileCallback.onSingleTouch();
            }
        });

//        mMoreView.setOnClickListener(v -> {
//            // 更多按钮
//            mFileMorePop.showAsDropDown(mMoreView, AppUtils.dip2px(mContext, 40), -AppUtils.dip2px(mContext, 135));
//            LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow mMoreViewClick");
//        });

        mClearView.setOnClickListener(v -> {
            // 清除当前白板的绘画轨迹
            ((BaseLiveAct) mContext).getWBManager().getBoard().clear(false);
            LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow mClearViewClick");
        });

        mAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
            for (FileModel filePageModel : mList) {
                filePageModel.isSelected = false;
            }
            mList.get(position).isSelected = true;
            mAdapter.notifyDataSetChanged();
            LiveRoomInfoProvider.getInstance().isCountingUnread = true;
            if (!TextUtils.isEmpty(data.type) && ("1".equals(data.type) || "4".equals(data.type))) {
                loaclSelectIndex = position;
            }

            if (!TextUtils.isEmpty(data.type) && "2".equals(data.type)) {
                // 视频，直接播放视频
                mDrawView.clearAnimation();
                mFileListRecycler.clearAnimation();
                mFileListRecyclerBg.clearAnimation();
                mClearView.clearAnimation();
//                mMoreView.clearAnimation();

                mFileListRecycler.setVisibility(View.GONE);
                mFileListRecyclerBg.setVisibility(View.GONE);
                mTxVideoPlayer.setVisibility(View.VISIBLE);
                mDrawView.setVisibility(View.GONE);
                mClearView.setVisibility(View.GONE);
                showVideo(data);
                isNowRootDir = false;
            } else if (!TextUtils.isEmpty(data.type) && "4".equals(data.type)) {
                // pdf，直接显示pdf预览图片
                mTxVideoPlayer.setVisibility(View.GONE);
                mWhiteboardView.setVisibility(View.VISIBLE);
                mDrawView.setVisibility(View.VISIBLE);
                mClearView.setVisibility(View.VISIBLE);
                downLoadFile(data, false);
                isNowRootDir = false;
            } else {
                mTxVideoPlayer.setVisibility(View.GONE);
                mWhiteboardView.setVisibility(View.VISIBLE);
                mDrawView.setVisibility(View.VISIBLE);
                mClearView.setVisibility(View.VISIBLE);
                showSubboardWithDocumentId(data.getDocumentIdPic(), data.url, false);
                if (TextUtils.isEmpty(data.type) && !"1".equals(data.type)) {
                    // 展示图片的类型不是服务器返回的图片（pdf的图片）
                    mPdfSelectMaps.put(data.documentId, position);
                }
            }
        });

        gestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {//单击事件
                mReSelectFileCallback.onSingleTouch();
                if (hasHidden) {
                    showGraientView();
                } else {
                    hideGraientView();
                }
                return true;
            }
        });
    }

    public void showGraientView() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow showGraientView");
        for (View view : mAnimationControllView) {
            if (!isInControl && view instanceof RecyclerView) {
                continue;
            }

            view.setVisibility(View.VISIBLE);
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setDuration(200);
            alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    hasHidden = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            view.startAnimation(alphaAnimation);
        }
    }

    private void hideGraientView() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow hideGraientView");
        for (View view : mAnimationControllView) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setDuration(200);
            alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    hasHidden = true;
                    view.setVisibility(View.GONE);
                    view.clearAnimation();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            view.startAnimation(alphaAnimation);
        }
    }

    @SuppressLint("ResourceType")
    public void initWhiteBoard() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow initWhiteBoard ");
        if (((BaseLiveAct) mContext).getWBManager() == null || ((BaseLiveAct) mContext).getWBManager().getBoard() == null) {
            return;
        }
        // 初始化白板，主播、助手可操作白板，普通观众不可操作
        // 设置是否涂鸦
        ((BaseLiveAct) mContext).getWBManager().getBoard().setToolType(TEDU_BOARD_TOOL_TYPE_PEN);
        ((BaseLiveAct) mContext).getWBManager().getBoard().setDrawEnable(!LiveRoomInfoProvider.getInstance().isGuest());
        ((BaseLiveAct) mContext).getWBManager().getBoard().setBoardRatio(AppUtils.getWidth(mContext) + ":" + (1.78 * AppUtils.getWidth(mContext)));
        ((BaseLiveAct) mContext).getWBManager().getBoard().setDataSyncEnable(!LiveRoomInfoProvider.getInstance().isGuest());
    }

    private void disableWhiteboard(boolean isEnable) {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow disableWhiteboard : isEnable:" + isEnable);
        if (isEnable) {
            ((BaseLiveAct) mContext).getWBManager().getBoard().setToolType(TEDU_BOARD_TOOL_TYPE_PEN);
        } else {
            ((BaseLiveAct) mContext).getWBManager().getBoard().setToolType(TEDU_BOARD_TOOL_TYPE_ZOOM_DRAG);
        }

        mDrawView.setBackgroundResource(isEnable ? R.drawable.icon_file_draw_select : R.drawable.icon_file_draw);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initAddView(boolean isclick) {
        View boardview = ((BaseLiveAct) mContext).getWBManager().getBoard().getBoardRenderView();
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);

        if (boardview != null) {
            // 设置背景色
            boardview.setBackgroundColor(0);
            // 设置填充透明度 范围：0-255
            boardview.getBackground().setAlpha(0);
            ((BaseLiveAct) mContext).getWBManager().getBoard().setBoardContentFitMode(TEduBoardController.TEduBoardContentFitMode.TEDU_BOARD_CONTENT_FIT_MODE_NONE);

            ViewGroup parent = (ViewGroup) boardview.getParent();
            if (parent != null) {
                parent.removeAllViews();
            }
            mWhiteboardView.removeAllViews();
            mWhiteboardView.addView(boardview, layoutParams);
            FrameLayout.LayoutParams layoutParams1 = (FrameLayout.LayoutParams) boardview.getLayoutParams();
            layoutParams1.height = AppUtils.getHeight(mContext);
            layoutParams1.width = AppUtils.getWidth(mContext);
            boardview.setLayoutParams(layoutParams1);
            if (canDrawable) {
                ((BaseLiveAct) mContext).getWBManager().getBoard().setToolType(TEDU_BOARD_TOOL_TYPE_PEN);
            } else {
                ((BaseLiveAct) mContext).getWBManager().getBoard().setToolType(TEDU_BOARD_TOOL_TYPE_ZOOM_DRAG);
            }

            if (isclick) {
                ((BaseLiveAct) mContext).getWBManager().getBoard().setBoardRatio(AppUtils.getWidth(mContext) + ":" + (1.78 * AppUtils.getWidth(mContext)));
            }
            boardview.setOnTouchListener((v, event) -> {
                if (!LiveRoomInfoProvider.getInstance().isGuest() && LiveRoomInfoProvider.getInstance().isShowFile) {
                    return gestureDetector.onTouchEvent(event);
                } else {
                    return false;
                }
            });
        }

        if (isclick) {
            mFileListRecyclerBg.setVisibility(View.VISIBLE);
        } else {
            mFileListRecyclerBg.setVisibility(View.GONE);
        }
    }

    public void showProgressHUD(Activity context, String showMessage, boolean cancleAble) {
        if (context.isFinishing()) {
            this.mContext = context;
            return;
        }
        mHandler.post(() ->
                mProgressHUD = ProgressHUD.show(context, showMessage, cancleAble, dialog -> {}));
    }

    public void dismissProgressHUD() {
        try {
            if (mProgressHUD != null && mContext != null && !((Activity) mContext).isFinishing()) {
                mHandler.post(() ->
                        mProgressHUD.dismiss());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exitFileShow(FileShowCallback callback) {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow exitFileShow");
        this.mFileShowCallback = callback;
        isShowBoardCLient = false;
        LiveRoomInfoProvider.getInstance().isShowFile = false;
        LiveRoomInfoProvider.getInstance().isCountingUnread = false;
        if (isStartVideo) {
            isStartVideo = false;
            if (mTxVideoPlayer != null){
                mTxVideoPlayer.releaseVideoSource();
            }
            if (!LiveRoomInfoProvider.getInstance().isGuest()) {
                // 发送文档演示视频播放消息通知
                sendShowVideoMsg("", "2", "0");
            }
        }
        if (!LiveRoomInfoProvider.getInstance().isGuest()) {
            sendCloseWhiteBoardMsg("exitFileShow");
        } else {
            mReSelectFileCallback.reSelectFile();
            if (mFileShowCallback != null) {
                mFileShowCallback.exitFileShowSuccess();
            }
        }
    }

    public interface ReSelectFileCallback {
        void reSelectFile();

        void onSingleTouch();
    }

    private ReSelectFileCallback mReSelectFileCallback;

    public void setmReSelectFileCallback(ReSelectFileCallback mReSelectFileCallback) {
        this.mReSelectFileCallback = mReSelectFileCallback;
    }

    /**
     * 设置当前的：文件夹、选择的文件、文件索引
     */
    @SuppressLint("ClickableViewAccessibility")
    public void setCurrentSelectInfo(ArrayList<FileModel> currentFile, int fileIndex, FileFolderModel folder) {
        initAddView(true);
        // 走此方法的均为本人发起的
        LiveRoomInfoProvider.getInstance().isMyselfOpenDocument = true;
        LiveRoomInfoProvider.getInstance().isShowFile = true;
        canDrawable = false;
        disableWhiteboard(canDrawable);
        mList.clear();
        mLocalList.clear();
        mBtnsRlv.setVisibility(View.VISIBLE);
        mFileListRecycler.setVisibility(View.VISIBLE);
        mFileListRecyclerBg.setVisibility(View.VISIBLE);
        loaclSelectIndex = fileIndex;
        // 判断所选的类型
        if ("2".equals(currentFile.get(fileIndex).type)) {
            // 视频，直接播放视频
            mTxVideoPlayer.setVisibility(View.VISIBLE);
            mDrawView.setVisibility(View.GONE);
            mClearView.setVisibility(View.GONE);
            showVideo(currentFile.get(fileIndex));
            isNowRootDir = false;
            mFileListRecycler.setVisibility(View.GONE);
            mFileListRecyclerBg.setVisibility(View.GONE);
        } else if ("4".equals(currentFile.get(fileIndex).type)) {
            // pdf，直接显示pdf预览图片
            mTxVideoPlayer.setVisibility(View.GONE);
            mDrawView.setVisibility(View.VISIBLE);
            mClearView.setVisibility(View.VISIBLE);
            mList.clear();
            mAdapter.updateData(mList);
            downLoadFile(currentFile.get(fileIndex), true);
            isNowRootDir = false;
        } else {
            // 其余当作图片处理
            mTxVideoPlayer.setVisibility(View.GONE);
            mWhiteboardView.setVisibility(View.VISIBLE);
            mDrawView.setVisibility(View.VISIBLE);
            mClearView.setVisibility(View.VISIBLE);
            filterPdfFile(currentFile);
            mList.addAll(currentFile);
            for (FileModel filePageModel : mList) {
                filePageModel.isSelected = false;
            }
            mList.get(fileIndex).isSelected = true;
            mAdapter.updateData(mList);
            mLocalList.clear();
            mLocalList.addAll(mList);
            isNowRootDir = true;
            showSubboardWithDocumentId(currentFile.get(fileIndex).documentId, currentFile.get(fileIndex).url, true);
        }
        // 防止身份变更后不能响应ontouch事件
        ((BaseLiveAct) mContext).getWBManager().getBoard().setDrawEnable(!LiveRoomInfoProvider.getInstance().isGuest());
    }

    /**
     * 主播端展示白板并下发展示白板的通知
     *
     * @param documentKey
     * @param picUrl
     */
    private void showSubboardWithDocumentId(String documentKey, String picUrl, boolean isFileSeletPop) {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow showSubboardWithDocumentId : documentKey:" + documentKey + ",picUrl:" + OSSConfig.getOSSURLedStr(picUrl) + "isFileSeletPop:" + isFileSeletPop);

        if (!LiveRoomInfoProvider.getInstance().isGuest()) {
            String subBoardId = "";
            if (mBordMaps.containsKey(documentKey)) {
                subBoardId = mBordMaps.get(documentKey);
            }

            mHandler.post(() -> {
                mTxVideoPlayer.setVisibility(View.GONE);
                mWhiteboardView.setVisibility(View.VISIBLE);
                mBtnsRlv.setVisibility(View.VISIBLE);
                mDrawView.setVisibility(View.VISIBLE);
                mClearView.setVisibility(View.VISIBLE);
//                mMoreView.setVisibility(View.VISIBLE);
            });

            ((BaseLiveAct) mContext).getWBManager().getBoard().setBrushColor(new TEduBoardController.TEduBoardColor(Color.RED));
            ((BaseLiveAct) mContext).getWBManager().getBoard().setDataSyncEnable(!LiveRoomInfoProvider.getInstance().isGuest());
            ((BaseLiveAct) mContext).getWBManager().getBoard().setBoardRatio(AppUtils.getWidth(mContext) + ":" + (1.7 * AppUtils.getWidth(mContext)));

            String currentBoardImageUrll = OSSConfig.getOSSURLedStr(picUrl);
            if (TextUtils.isEmpty(subBoardId)) {
                subBoardId = ((BaseLiveAct) mContext).getWBManager().getBoard().addBoard(currentBoardImageUrll);
                LogUtil.i("CurrentBoardImageUrl:", currentBoardImageUrll);
                mBordMaps.put(documentKey, subBoardId);
            } else {
                ((BaseLiveAct) mContext).getWBManager().getBoard().gotoBoard(subBoardId);
                ((BaseLiveAct) mContext).getWBManager().getBoard().setBackgroundImage(currentBoardImageUrll, TEduBoardController.TEduBoardImageFitMode.TEDU_BOARD_IMAGE_FIT_MODE_CENTER);
            }

            LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow showSubboardWithDocumentId（setBoardBackGround） Success:isShowFile:" + LiveRoomInfoProvider.getInstance().isShowFile);
            if (LiveRoomInfoProvider.getInstance().isShowFile) {
                dismissProgressHUD();
                // 发送消息通知观众
                LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                        LiveCustomMessageProvider.getWBChangeData(mContext, subBoardId), false);
                // 文档演示的功能已经弹出来了（此时才可以发送消息，避免pdf未上传成功后退出文档演示）
                LiveRoomInfoProvider.getInstance().isCountingUnread = true;
                if (isFileSeletPop) {

                    // 展示图片的仅调用一次接口
                    LiveRoomIOHelper.setDocumentStatus(mContext, "1", subBoardId, new IOLiveRoomListener() {
                        @Override
                        public void onIOError(String error, int errorCode) {

                        }

                        @Override
                        public void onIOSuccess() {

                        }
                    });
                }
            }
        }
    }

    /**
     * 主播端播放视频并下发播放视频的消息
     *
     * @param data
     */
    private void showVideo(FileModel data) {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow showVideo");
        if (mDrawView.getVisibility() != View.GONE) {
            mDrawView.setVisibility(View.GONE);
        }
        if (mClearView.getVisibility() != View.GONE) {
            mClearView.setVisibility(View.GONE);
        }
        LiveRoomInfoProvider.getInstance().isShowFile = true;
        LiveRoomInfoProvider.getInstance().isCountingUnread = true;
        mHandler.post(() -> {
            //设置视频地址和请求头部
            mTxVideoPlayer.releaseVideoSource();
            mTxVideoPlayer.setVideoUrl(OSSConfig.getOSSURLedStr(data.url), 2);
            //创建视频控制器
            mTxVideoPlayer.isShowFullScreenLayout(false);
            mTxVideoPlayer.setChatRoomStyle(true);
            mTxVideoPlayer.setLivingRoom(true);
            mTxVideoPlayer.hideSeekBar = true;
            mTxVideoPlayer.isOnineClientClick = false;
            mTxVideoPlayer.autoStart();
            isStartVideo = true;
            isInitPauseIng = false;
            // 发送文档演示视频播放消息通知
            sendShowVideoMsg(data.url, "0", String.valueOf(0));
            // 进度条拖放的监听
            mTxVideoPlayer.setOnSeekBarDragListener(positon -> {
                // 发送文档演示视频播放消息通知
                sendShowVideoMsg(data.url, "0", String.valueOf(mTxVideoPlayer.getVodCurrentTime()));
            });
            // 播放暂停的监听
            mTxVideoPlayer.setOnPlayOrPauseListener(isStart -> {
                if (isStart) {
                    // 播放
                    // 发送文档演示视频播放消息通知
                    sendShowVideoMsg(data.url, "0", String.valueOf(mTxVideoPlayer.getVodCurrentTime()));
                } else {
                    // 暂停
                    sendShowVideoMsg(data.url, "1", String.valueOf(mTxVideoPlayer.getVodCurrentTime()));
                }
            });
            mTxVideoPlayer.isOnineCanControl = true;
            mTxVideoPlayer.setOnSimpleCLickListener(new OnSimpleCLickListener() {
                @Override
                public void onSingleClick() {
                    mReSelectFileCallback.onSingleTouch();
                }

                @Override
                public void onDoubleClick() {

                }
            });
        });
    }

    /**
     * 上传文件，然后加载白板
     */
    private void upLoadFile(String documentId, String fileDir, FileModel fileModel, boolean isFileSeletPop) {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow upLoadFile: documentId:" + documentId + ",fileDir:" + fileDir + ",isFileSeletPop:" + isFileSeletPop);
        selectFileModel = fileModel;
        if (!LiveRoomInfoProvider.getInstance().isShowFile) {
            // 若文档演示的弹窗不显示则不往下走流程
            dismissProgressHUD();
            return;
        }
        if (mFileMaps.containsKey(documentId)) {
            // 若果map里面有数据则证明上传过了，不需要再次上传，直接显示图片
            ArrayList<String> picList;
            picList = mFileMaps.get(documentId);
            mList.clear();
            // 当前pdf文件上次显示的位置
            int index = 0;
            if (mPdfSelectMaps.containsKey(documentId) && index < picList.size()) {
                index = mPdfSelectMaps.get(documentId);
            }
            // 已经上传过，不需要再次上传，直接显示
            for (int i = 0; i < picList.size(); i++) {
                FileModel model = new FileModel();
                model.documentId = documentId;
                model.documentIdPic = documentId + i;
                model.url = picList.get(i);

                model.isSelected = i == index;
                mList.add(model);
            }
            int finalIndex = index;
            mHandler.post(() -> {
                mAdapter.updateData(mList);
                if (mList.size() > 0) {
                    showSubboardWithDocumentId(mList.get(finalIndex).getDocumentIdPic(), mList.get(finalIndex).url, isFileSeletPop);
                }
                mFileListRecycler.setVisibility(View.VISIBLE);
                mFileListRecyclerBg.setVisibility(View.VISIBLE);
            });

            return;
        }

        showProgressHUD((Activity) mContext, "", true);
        /**
         * d4.10 添加转码文件
         * @param title 文件名
         * @param resolution 文件的显示分辨率
         * @param url 转码后的url
         * @param pages 文件的总页数
         * @return
         */
        TEduBoardController.TEduBoardTranscodeFileResult fileResult = new TEduBoardController.TEduBoardTranscodeFileResult();
        fileResult.title = fileModel.title;
        fileResult.resolution = fileModel.resolution;
        fileResult.url = fileModel.resultUrl;
        fileResult.pages = fileModel.pages;
        ((BaseLiveAct) mContext).getWBManager().getBoard().addTranscodeFile(fileResult);
//        ((BaseLiveAct) mContext).getWBManager().getBoard().addTranscodeFile(
//                fileModel.title, fileModel.resolution, fileModel.resultUrl, fileModel.pages);
    }

    private FileModel selectFileModel;

    public void addFileSuccess(String fileId) {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow addFileSuccess");
        dismissProgressHUD();
        if (selectFileModel == null) {
            return;
        }

        if (TextUtils.isEmpty(fileId) && !fileId.equals(selectFileModel.documentId)) {
            return;
        }
        // 获取指定的文件信息。
        TEduBoardController.TEduBoardFileInfo fileinfo = ((BaseLiveAct) mContext).getWBManager().getBoard().getFileInfo(fileId);
        if (fileinfo == null) {
            return;
        }
        mList.clear();
        ArrayList<String> picList = new ArrayList<>();
        for (int i = 0; i < fileinfo.boardInfoList.size(); i++) {
            picList.add(fileinfo.boardInfoList.get(i).backgroundUrl);
            FileModel model = new FileModel();
            model.documentId = selectFileModel.documentId;
            model.documentIdPic = selectFileModel.documentId + i;
            model.url = fileinfo.boardInfoList.get(i).backgroundUrl;
            model.isSelected = i == 0;
            mList.add(model);
            mBordMaps.put(model.documentIdPic, fileinfo.boardInfoList.get(i).boardId);
        }

        mHandler.post(() -> {
            mFileMaps.put(selectFileModel.documentId, picList);
            mPdfSelectMaps.put(selectFileModel.documentId, 0);
            mAdapter.updateData(mList);
            mFileListRecycler.setVisibility(View.VISIBLE);
            mFileListRecyclerBg.setVisibility(View.VISIBLE);
            if (mList.size() > 0) {
                showSubboardWithDocumentId(mList.get(0).getDocumentIdPic(), mList.get(0).url, true);
            }
        });
    }

    /**
     * 过滤pdf文件
     */
    private void filterPdfFile(ArrayList<FileModel> currentFile) {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow filterPdfFile");
        // 过滤一下列表中的pdf文件
        for (int i = 0; i < currentFile.size(); i++) {
            if (!TextUtils.isEmpty(currentFile.get(i).type)
                    && ("1".equals(currentFile.get(i).type))) {
                // 如果是图片（默认没有ppt）
                ArrayList<String> picList = new ArrayList<>();
                picList.add(currentFile.get(i).url);
                mFileMaps.put(currentFile.get(i).documentId, picList);
            }
        }
    }

    /**
     * 下载文件
     *
     * @param data 要下载的对象
     */
    private void downLoadFile(FileModel data, boolean isFileSeletPop) {
        if (TextUtils.isEmpty(data.url) || TextUtils.isEmpty(data.resultUrl)) {
            Toasty.normal(mContext, "文档失效").show();
            dismissProgressHUD();
            return;
        }
        upLoadFile(data.documentId, data.fileDir, data, isFileSeletPop);
    }

    /**
     * 用户端接到消息展示白板
     */
    public void clientShowBoard(String boardId) {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow clientShowBoard: boardId:" + boardId);
        LiveRoomInfoProvider.getInstance().isMyselfOpenDocument = false;
        LiveRoomInfoProvider.getInstance().isCountingUnread = true;
        LiveRoomInfoProvider.getInstance().isShowFile = true;
        if (mWhiteboardView.getChildCount() == 0) {
            initAddView(false);
        }
        ((BaseLiveAct) mContext).getWBManager().getBoard().setBoardRatio(AppUtils.getWidth(mContext) + ":" + (1.7 * AppUtils.getWidth(mContext)));
        if (mWhiteboardView.getVisibility() != View.VISIBLE) {
            mWhiteboardView.setVisibility(View.VISIBLE);
        }

        if (!isStartVideo && mTxVideoPlayer.getVisibility() != View.GONE) {
            mTxVideoPlayer.setVisibility(View.GONE);
        }
        // 不是助手后不能画(是助手或主播可以显示画笔点击画笔可画)
        canDrawable = false;
        disableWhiteboard(canDrawable);
        if (LiveRoomInfoProvider.getInstance().isGuest()) {
            if (mBtnsRlv.getVisibility() != View.GONE) {
                mBtnsRlv.setVisibility(View.GONE);
            }
            // 初始化白板，主播、助手可操作白板，普通观众不可操作（防止中途身份变化）
            ((BaseLiveAct) mContext).getWBManager().getBoard().setDrawEnable(false);
            ((BaseLiveAct) mContext).getWBManager().getBoard().setDataSyncEnable(!LiveRoomInfoProvider.getInstance().isGuest());
        } else {
            mBtnsRlv.setVisibility(View.VISIBLE);
            mDrawView.setVisibility(View.VISIBLE);
            mClearView.setVisibility(View.VISIBLE);
            if (!LiveRoomInfoProvider.getInstance().isMyselfOpenDocument) {
                mAnimationControllView.remove(mFileListRecycler);
                mAnimationControllView.remove(mFileListRecyclerBg);
            }
            // 初始化白板，主播、助手可操作白板，普通观众不可操作（防止中途身份变化）
            ((BaseLiveAct) mContext).getWBManager().getBoard().setDrawEnable(true);
            ((BaseLiveAct) mContext).getWBManager().getBoard().setDataSyncEnable(!LiveRoomInfoProvider.getInstance().isGuest());
        }
        ((BaseLiveAct) mContext).getWBManager().getBoard().setBrushColor(new TEduBoardController.TEduBoardColor(Color.RED));

        isShowBoardCLient = true;
    }

    /**
     * 观众端接到消息关闭白板
     */
    public void closeBoard() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow closeBoard");
        isShowBoardCLient = false;
        LiveRoomInfoProvider.getInstance().isShowFile = false;
        LiveRoomInfoProvider.getInstance().isCountingUnread = false;
        if (mWhiteboardView.getVisibility() != View.GONE) {
            mWhiteboardView.setVisibility(View.GONE);
        }

        if (mFileMorePop != null && mFileMorePop.isShowing()) {
            mFileMorePop.dismiss();
        }
    }

    /**
     * 标志是否一进入直播间就是视频暂停的状态
     * true：才跳到指定位置然后暂停
     * false：首帧视频后不做处理
     */
    private boolean isInitPauseIng = false;

    /**
     * 用户端接到消息：开启、暂停视频
     *
     * @param msg
     * @param isInitJump 标识是否是请求接口进来的
     */
    public void clientShowVideo(DoumentVideoSendMsg msg, boolean isInitJump) {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow clientShowVideo: isInitJump:" + isInitJump);
        LiveRoomInfoProvider.getInstance().isMyselfOpenDocument = false;
        LiveRoomInfoProvider.getInstance().isCountingUnread = true;
        LiveRoomInfoProvider.getInstance().isShowFile = true;
        if (mFileListRecycler.getVisibility() != View.GONE) {
            mFileListRecycler.setVisibility(View.GONE);
            mFileListRecyclerBg.setVisibility(View.GONE);
        }

        if (mTxVideoPlayer.getVisibility() != View.VISIBLE) {
            mTxVideoPlayer.setVisibility(View.VISIBLE);
        }


        if (LiveRoomInfoProvider.getInstance().isGuest()) {
            if (mBtnsRlv.getVisibility() != View.GONE) {
                mBtnsRlv.setVisibility(View.GONE);
            }
        } else {
            mBtnsRlv.setVisibility(View.VISIBLE);
            mDrawView.setVisibility(View.GONE);
            mClearView.setVisibility(View.GONE);
        }

        if (!isInitJump && !TextUtils.isEmpty(msg.status) && "1".equals(msg.status)) {
            isInitPauseIng = false;
            // 暂停
            mTxVideoPlayer.pauseVideo();
        } else {
            isStartVideo = true;
            //以下代码之前是在状态不为status=1 中。后迁移到外部
            //设置请求头部
            mTxVideoPlayer.isShowFullScreenLayout(false);
            // 观众播放器来是观看的，不能操作暂停播放拖动等
            mTxVideoPlayer.isOnineClientClick = true;
            mTxVideoPlayer.hideSeekBar = true;
            mTxVideoPlayer.setChatRoomStyle(true);
            // 设置视频地址
            float position = Float.valueOf(msg.playTime);
            if (!LiveRoomInfoProvider.getInstance().isGuest()) {
                mTxVideoPlayer.isOnineCanControl = true;
                // 有控制权限的都可以监听到点击事件只不过会根据是否是当前控制者区分权限
                mTxVideoPlayer.setOnSimpleCLickListener(new OnSimpleCLickListener() {
                    @Override
                    public void onSingleClick() {
                        mReSelectFileCallback.onSingleTouch();
                        hasHidden = !hasHidden;
                    }

                    @Override
                    public void onDoubleClick() {

                    }
                });
            }
            //*****************************************
            if (!TextUtils.isEmpty(msg.status) && "1".equals(msg.status)) {
                // 权限版本修复的bug18636，主播暂停后，观众进入直播间换面黑屏。方案添加了第一桢回调监听。
                // 会有的问题是先播放视频，在调用seek到指定位置，之后暂停会有画面延迟问题
                if( mTxVideoPlayer.getVideoUrl().isEmpty()){
                    isInitPauseIng = true;
                    // 进直播间就暂停了
                    mTxVideoPlayer.setVideoUrl(OSSConfig.getOSSURLedStr(msg.url), 2);
                    //开始加载视频
                    mTxVideoPlayer.startVideo();
                    mTxVideoPlayer.setFirstFrameCallBack(() -> {
                        //指定视频播放位置
                        if (isInitPauseIng) {
                            mTxVideoPlayer.startVodVideo(position);
                            mTxVideoPlayer.pauseVideo();
                        }
                    });
                } else {
                    isInitPauseIng = false;
                    mHandler.postDelayed(() -> {
                        // 暂停
                        mTxVideoPlayer.pauseVideo();
                    }, 700);
                }

            } else {
                isInitPauseIng = false;
                boolean isSameVideo = mTxVideoPlayer.getVideoUrl().equals(OSSConfig.getOSSURLedStr(msg.url));

                if (position > 0 && isSameVideo) {
                    mTxVideoPlayer.startVodVideo(position);
                } else {
                    mTxVideoPlayer.releaseVideoSource();
                    mTxVideoPlayer.setVideoUrl(OSSConfig.getOSSURLedStr(msg.url), 2);
                    mTxVideoPlayer.startVideo();
                    mTxVideoPlayer.setFirstFrameCallBack(() -> {
                        //指定视频播放位置
                        mTxVideoPlayer.startVodVideo(position);
                    });
                }

            }
        }
    }

    /**
     * 观众端接到消息关闭视频
     */
    public void closeVideo() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow closeVideo");
        isInitPauseIng = false;
        if (mTxVideoPlayer != null) {
            mTxVideoPlayer.pauseVideo();
            mTxVideoPlayer.releaseVideoSource();
        }
        if (mTxVideoPlayer.getVisibility() != View.GONE) {
            mTxVideoPlayer.setVisibility(View.GONE);
        }
        if (hasHidden) {
            mReSelectFileCallback.onSingleTouch();
            hasHidden = !hasHidden;
        }
        mClearView.setVisibility(View.VISIBLE);
        mDrawView.setVisibility(View.VISIBLE);
//        mMoreView.setVisibility(View.VISIBLE);
        if (mFileMorePop != null && mFileMorePop.isShowing()) {
            mFileMorePop.dismiss();
        }
        if (!isShowBoardCLient) {
            LiveRoomInfoProvider.getInstance().isShowFile = false;
            LiveRoomInfoProvider.getInstance().isCountingUnread = false;
        } else {
            ((BaseLiveAct) mContext).getWBManager().getBoard().refresh();
        }
    }

    /**
     * 按返回时判断
     */
    public void onBack() {
        if (!LiveRoomInfoProvider.getInstance().isGuest()) {
            if (isStartVideo) {
                isStartVideo = false;
                mTxVideoPlayer.releaseVideoSource();
                mTxVideoPlayer.setVisibility(View.GONE);
                if (!LiveRoomInfoProvider.getInstance().isGuest()) {
                    // 发送文档演示视频播放消息通知
                    sendShowVideoMsg("", "2", "0");
                }
            }
            if (mLocalList.size() > 0 && !isNowRootDir) {
                for (int i = 0; i < mLocalList.size(); i++) {
                    if (loaclSelectIndex == i) {
                        mLocalList.get(i).isSelected = true;
                    } else {
                        mLocalList.get(i).isSelected = false;
                    }
                }
                mList.clear();
                mList.addAll(mLocalList);
                mAdapter.updateData(mList);
                mFileListRecycler.setVisibility(View.VISIBLE);
                mFileListRecyclerBg.setVisibility(View.VISIBLE);
                mBtnsRlv.setVisibility(View.VISIBLE);
                mDrawView.setVisibility(View.VISIBLE);
                mClearView.setVisibility(View.VISIBLE);
                isNowRootDir = true;
            } else {
                sendCloseWhiteBoardMsg("onBack");
            }
        } else {
            mReSelectFileCallback.reSelectFile();
            mWhiteboardView.setVisibility(View.GONE);
            if (mFileMorePop != null && mFileMorePop.isShowing()) {
                mFileMorePop.dismiss();
            }
        }
    }

    /**
     * 发送关闭白板的消息
     */
    private void sendCloseWhiteBoardMsg(String fromSource) {
        // 不依赖接口返回
        LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                LiveCustomMessageProvider.getWBColoseData(mContext), false);
        fromSource = TextUtils.isEmpty(fromSource) ? "" : fromSource;
        switch (fromSource){
            case "exitFileShow":
                mReSelectFileCallback.reSelectFile();
                if (mFileShowCallback != null) {
                    mFileShowCallback.exitFileShowSuccess();
                }
                break;
            case "onBack":
                mReSelectFileCallback.reSelectFile();
                mWhiteboardView.setVisibility(View.GONE);
                if (mFileMorePop != null && mFileMorePop.isShowing()) {
                    mFileMorePop.dismiss();
                }
                break;
            default:
                mReSelectFileCallback.reSelectFile();
                break;
        }

        LiveRoomIOHelper.setDocumentStatus(mContext, "0", "", new IOLiveRoomListener() {
            @Override
            public void onIOError(String error, int errorCode) {

            }

            @Override
            public void onIOSuccess() {

            }
        });
    }

    /**
     * 发送播放视频的通知
     *
     * @param videoUrl
     * @param staus    0:开始播放视频， 1：暂停，2：退出播放视频
     * @param playTime
     */
    private void sendShowVideoMsg(String videoUrl, String staus, String playTime) {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow sendShowVideoMsg: status:" + staus);
        // 发送文档演示视频播放消息通知
        if (!LiveRoomInfoProvider.getInstance().isGuest()) {
            if ("2".equals(staus)) {
                if (!LiveRoomInfoProvider.getInstance().isGuest()) {
                    LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                            LiveCustomMessageProvider.getDocumentVideoColoseData(mContext, videoUrl, playTime, staus), false);
                }
            } else {
                LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                        LiveCustomMessageProvider.getDocumentVideoChangeData(mContext, OSSConfig.getOSSURLedStr(videoUrl), playTime, staus), false);

            }
            LiveRoomIOHelper.setVideoStatus(staus, OSSConfig.getOSSURLedStr(videoUrl), playTime);
        }
    }

    /**
     * 当前直播间若关闭则判断是否有需要释放的播放器
     */
    public void releaseVideoPlayer() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow releaseVideoPlayer");
        if (mTxVideoPlayer != null) {
            mTxVideoPlayer.releaseVideoSource();
            mTxVideoPlayer.releaseVideoListener();
            Log.d("FileShowView", "releaseVideoPlayer");
        }
    }

    /**
     * 关闭当前文档演示视屏
     */
    public void pauseVideoPlayer() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "FileShow pauseVideoPlayer");
        if (mTxVideoPlayer != null) {
            mTxVideoPlayer.pauseVideo();
        }
    }

    /**
     * 判断当前是否正在展示白板
     *
     * @return true：正在展示；false：没有展示
     */
    public boolean isShowWhiteBoardIng() {
        if (mWhiteboardView.getVisibility() == View.VISIBLE) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 刷新更多按钮中的未读消息数量
     *
     * @param addCount
     */
    public void refreshUnreadInfo(int addCount) {
        if (mFileMorePop != null) {
            mFileMorePop.refreshUnreadInfo(addCount);
        }
    }
}
