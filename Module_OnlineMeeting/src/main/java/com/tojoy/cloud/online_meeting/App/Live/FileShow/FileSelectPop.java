package com.tojoy.cloud.online_meeting.App.Live.FileShow;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.live.FileFolderModel;
import com.tojoy.tjoybaselib.model.live.FileListResponse;
import com.tojoy.tjoybaselib.model.live.FileModel;
import com.tojoy.tjoybaselib.model.live.FileTagListResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.PickerViewAnimateUtil;
import com.tojoy.tjoybaselib.ui.editView.ClearEmojInputFilter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.tojoy.cloud.online_meeting.R;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 文件夹及文件选择弹窗
 */
public class FileSelectPop {

    private int animationDuration = 250;

    private Context context;
    private ViewGroup contentContainer;
    private Animation outAnim;
    private Animation inAnim;
    private boolean isAnim = true;
    private boolean isShowing;
    private int gravity = Gravity.BOTTOM;
    private ViewGroup decorView;
    private ViewGroup rootView;
    private boolean dismissing;

    //文件夹
    private RecyclerView mFileFolderRe;
    private FileFolderAdapter mFolderAdapter;
    private ArrayList<FileFolderModel> mFolders = new ArrayList<>();

    //文件列表
    private RelativeLayout mFileListRlv;
    private TJRecyclerView mFileListRe;
    private EditText mInputEdit;
    private FilePopListAdapter mFileAdapter;
    private ArrayList<FileModel> mFiles = new ArrayList<>();
    private FileFolderModel selectFolder ;
    private int mPage = 1;
    private String searchFileName ;

    private boolean isCanLoadMore = false;
    private boolean isLoadFilesList = false;
    private int mFilesDeleteCheckedIndex = -1;

    public FileSelectPop(Context context) {
        this.context = context;
        init();
        initViews();
    }

    private void initViews() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        //decorView是activity的根View
        if (decorView == null) {
            decorView = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        }

        //将控件添加到decorView中
        rootView = (ViewGroup) layoutInflater.inflate(R.layout.livefile_select_popview, decorView, false);
        rootView.setLayoutParams(new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        ));
        rootView.setBackgroundColor(context.getResources().getColor(R.color.colorTranslucent));

        rootView.findViewById(R.id.iv_close).setOnClickListener(v -> dismiss());
        rootView.findViewById(R.id.rv_back).setOnClickListener(v -> {
            showleft();
            hideRight();
        });
        rootView.findViewById(R.id.selected_folderName).setOnClickListener(v -> {
            showleft();
            hideRight();
        });

        int height = (int) (AppUtils.getWidth(context) * 800.0f / 750.0f) + AppUtils.dip2px(context, 45);
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height);
        rootView.findViewById(R.id.container_base).setLayoutParams(lps);
        setKeyBackCancelable(true);
        contentContainer = rootView.findViewById(R.id.content_container);

        //文件夹
        mFileFolderRe = rootView.findViewById(R.id.recycler_fileSelect);
        GridLayoutManager mCoverLayoutManager = new GridLayoutManager(context, 4);
        mCoverLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFileFolderRe.setLayoutManager(mCoverLayoutManager);
        mFolderAdapter = new FileFolderAdapter(context, mFolders);
        mFileFolderRe.setAdapter(mFolderAdapter);

        //文件
        mFileListRlv = rootView.findViewById(R.id.rlv_file_list);
        mInputEdit = rootView.findViewById(R.id.ed_input);
        TextView mSearchTv = rootView.findViewById(R.id.tv_search);
        mFileListRe = rootView.findViewById(R.id.recycler_filelist);
        mFileListRlv.setVisibility(View.GONE);
        mFileListRe.setVisibility(View.GONE);
        GridLayoutManager mFileLayoutManager = new GridLayoutManager(context, 3);
        mFileLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFileListRe.getRecyclerView().setLayoutManager(mFileLayoutManager);
        mFileAdapter = new FilePopListAdapter(context, mFiles);
        mFileListRe.setAdapter(mFileAdapter);
        hideRight();

        mFileListRe.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mFiles.clear();
                mPage = 1;
                requestFileList(selectFolder,searchFileName,String.valueOf(mPage),false);
            }

            @Override
            public void onLoadMore() {
            }
        });

        mFileListRe.getRecyclerView().addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int last = mFileLayoutManager.findLastVisibleItemPosition();
                if (mFiles.size() < 10) {
                    return;
                }
                if (isCanLoadMore && !isLoadFilesList && last >= mFiles.size()-3) {
                    mPage ++ ;
                    requestFileList(selectFolder,searchFileName,String.valueOf(mPage),false);
                }
            }
        });

        mFolderAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {

            ((TextView) rootView.findViewById(R.id.selected_folderName)).setText(data.tagName);
            mFileAdapter.updateData(new ArrayList<>());
            selectFolder = data;
            mFiles.clear();
            mPage = 1;
            searchFileName = null;
            mInputEdit.setText("");
           ((UI)context).showProgressHUD(context,"");
            requestFileList(data,null,String.valueOf(mPage),true);
        });

        mFileAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
            mOnSelectFileFolder.onSelectFile(mFiles, position,selectFolder);
            // 白板退出后显示文件列表
            rootView.setVisibility(View.GONE);
            isShowing = false;
            dismissing = false;
            mOnSelectFileFolder.onDismissSeletePop();
        });

        mFileAdapter.setOnLongItemClickListener(new FilePopListAdapter.OnLongItemClickListener() {
            @Override
            public void onLongItemClick(int position) {
                // 长按状态
                // 是我自己上传的可以删除
                if ("1".equals(mFiles.get(position).isMe)) {
                    mFiles.get(position).isDeleteChecked = true;
                    if (mFilesDeleteCheckedIndex != -1) {
                        mFiles.get(mFilesDeleteCheckedIndex).isDeleteChecked = false;
                    }
                    mFilesDeleteCheckedIndex = position;
                    mFileAdapter.updateData(mFiles);
                }
            }

            @Override
            public void onDelete(int position) {
                // 处于删除状态，点击删除图标
                removeDocumentFile(mFiles.get(position).documentId,mFiles.get(position).isMe);
            }
        });

        InputFilter[] emojiFilters = {new ClearEmojInputFilter()};
        mInputEdit.setFilters(emojiFilters);
        mInputEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int length = s.length();
                if (length > 20) {
                    mInputEdit.setText(mInputEdit.getText().subSequence(0, 20));
                    mInputEdit.setSelection(20);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // 搜索
        mSearchTv.setOnClickListener(v -> {
            // 调用搜索接口
            mFiles.clear();
            mPage = 1;
            searchFileName = mInputEdit.getText().toString();
            requestFileList(selectFolder,searchFileName,String.valueOf(mPage),false);
            ((BaseLiveAct)context).showKeyboard(false);
        });

        rootView.findViewById(R.id.rlv_search).setOnClickListener(v -> {

        });

        setOutSideCancelable(true);
    }

    /**
     * 获取文件列表
     * @param data      当前选中的文件夹
     * @param fileName  要搜索的文件名称（只有搜索的时候才有值）
     */
    private void requestFileList(FileFolderModel data,String fileName,String pageNum,boolean isSelectFlod){
        if (data == null) {
            return;
        }
        isLoadFilesList = true;

        OMAppApiProvider.getInstance().mGetFileListWithTagId(LiveRoomInfoProvider.getInstance().liveId, data.tagId,fileName,pageNum, LiveRoomInfoProvider.getInstance().otherCompanyCode,new Observer<FileListResponse>() {
            @Override
            public void onCompleted() {
                ((UI)context).dismissProgressHUD();
                mFileListRe.setRefreshing(false);
                isLoadFilesList = false;
            }

            @Override
            public void onError(Throwable e) {
                ((UI)context).dismissProgressHUD();
                mFileListRe.setRefreshing(false);
                isLoadFilesList = false;
            }

            @Override
            public void onNext(FileListResponse response) {
                ((UI)context).dismissProgressHUD();
                if (response.isSuccess()) {
                    if(TextUtils.isEmpty(pageNum) && "1".equals(pageNum)){
                        mFiles.clear();
                    }
                    mFiles.addAll(response.data.list);
                    mFileAdapter.updateData(mFiles);
                    if (response.data.list.size()>= AppConfig.PER_PAGE_COUNT) {
                        isCanLoadMore = true;
                    } else {
                        isCanLoadMore = false;
                    }

                    if (!TextUtils.isEmpty(fileName) && mFiles.size()==0){
                        Toasty.normal(context,"未搜到匹配内容，请换个词搜索").show();
                    }
                    //如果是选择文件
                    if(isSelectFlod){
                        hideLeft();
                        showRight();
                    }
                }
            }
        });
    }



    /**
     * 删除文件夹下图片
     */
    private void removeDocumentFile(String extraId,String isMe){
        ((UI)context).showProgressHUD(context,"");
        OMAppApiProvider.getInstance().removeDocumentFile(extraId, isMe, LiveRoomInfoProvider.getInstance().liveId,
                new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        ((UI)context).dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ((UI)context).dismissProgressHUD();
                    }

                    @Override
                    public void onNext(OMBaseResponse baseResponse) {
                        if (baseResponse.isSuccess()) {
                            // 删除成功
                            mFiles.remove(mFilesDeleteCheckedIndex);
                            mFilesDeleteCheckedIndex = -1;
                            mFileAdapter.updateData(mFiles);
                        }
                    }
                });
    }

    private void hideLeft() {
        mFileListRlv.setVisibility(View.VISIBLE);
        mFileListRe.setVisibility(View.VISIBLE);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, -AppUtils.getWidth(context), 0.0f, 0.0f);
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setFillAfter(true);
        animationSet.setDuration(animationDuration);
        animationSet.addAnimation(translateAnimation);
        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                rootView.findViewById(R.id.iv_close).setVisibility(View.GONE);
                rootView.findViewById(R.id.rv_back).setVisibility(View.VISIBLE);
                rootView.findViewById(R.id.selected_folderName).setVisibility(View.VISIBLE);
                mFileFolderRe.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        mFileFolderRe.startAnimation(animationSet);
    }

    private void showleft() {
        mFileFolderRe.setVisibility(View.VISIBLE);
        TranslateAnimation translateAnimation = new TranslateAnimation(-AppUtils.getWidth(context), 0, 0.0f, 0.0f);
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setFillAfter(true);
        animationSet.setDuration(animationDuration);
        animationSet.addAnimation(translateAnimation);
        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                rootView.findViewById(R.id.iv_close).setVisibility(View.VISIBLE);
                rootView.findViewById(R.id.rv_back).setVisibility(View.GONE);
                rootView.findViewById(R.id.selected_folderName).setVisibility(View.GONE);
                mFileFolderRe.bringToFront();
                mFileListRlv.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        mFileFolderRe.startAnimation(animationSet);
        mFileListRe.startAnimation(animationSet);
        mFileListRlv.startAnimation(animationSet);
    }

    private void hideRight() {
        TranslateAnimation translateAnimation = new TranslateAnimation(0, AppUtils.getWidth(context), 0.0f, 0.0f);
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setFillAfter(true);
        animationSet.setDuration(animationDuration);
        animationSet.addAnimation(translateAnimation);
        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mFileAdapter.updateData(new ArrayList<>());
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        mFileListRlv.startAnimation(animationSet);
        mFileListRe.startAnimation(animationSet);
    }

    private void showRight() {
        mFileListRlv.setVisibility(View.VISIBLE);
        mFileListRe.setVisibility(View.VISIBLE);

        TranslateAnimation translateAnimation = new TranslateAnimation(AppUtils.getWidth(context), 0, 0.0f, 0.0f);
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setFillAfter(true);
        animationSet.setDuration(animationDuration);
        animationSet.addAnimation(translateAnimation);
        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mFileListRe.bringToFront();
                mFileListRlv.bringToFront();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        mFileListRe.startAnimation(animationSet);
        mFileListRlv.startAnimation(animationSet);
    }


    protected void init() {
        inAnim = getInAnimation();
        outAnim = getOutAnimation();
    }

    /**
     * @param v      (是通过哪个View弹出的)
     * @param isAnim 是否显示动画效果
     */
    public void show(View v, boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(View v) {
        show();
    }

    public void show() {
        if (isShowing()) {
            return;
        }
        isShowing = true;
        onAttached(rootView);
        rootView.requestFocus();
    }

    /**
     * 白板退出后显示文件列表
     */
    public void showVisibility() {
        rootView.setVisibility(View.VISIBLE);
        rootView.requestFocus();
    }

    private void onAttached(View view) {
        decorView.addView(view);
        if (isAnim) {
            contentContainer.startAnimation(inAnim);
        }
        ((UI)context).showProgressHUD(context,"");
        OMAppApiProvider.getInstance().mGetFileTagList(LiveRoomInfoProvider.getInstance().liveId, LiveRoomInfoProvider.getInstance().otherCompanyCode, new Observer<FileTagListResponse>() {
            @Override
            public void onCompleted() {
                ((UI)context).dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {
                ((UI)context).dismissProgressHUD();
            }

            @Override
            public void onNext(FileTagListResponse response) {
                if (response.isSuccess()) {
                    mFolders.clear();
                    mFolders.addAll(response.data);
                    mFolderAdapter.updateData(mFolders);
                }
            }
        });
    }

    public boolean isShowing() {
        return rootView.getParent() != null || isShowing;
    }


    public void dismiss() {
        if (dismissing) {
            dismissImmediately();
            contentContainer.clearAnimation();
            return;
        }
        dismissing = true;
        if (isAnim) {
            //消失动画
            outAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    dismissImmediately();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            contentContainer.startAnimation(outAnim);
        } else {
            dismissImmediately();
        }
        ((BaseLiveAct)context).showKeyboard(false);
    }

    public void dismissImmediately() {
        decorView.post(() -> {
            //从根视图移除
            decorView.removeView(rootView);
            isShowing = false;
            dismissing = false;
            showleft();
            hideRight();
            mOnSelectFileFolder.onDismissSeletePop();
//            WhiteboardManager.getInstance().clear();
        });

    }

    public Animation getInAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, true);
        return AnimationUtils.loadAnimation(context, res);
    }

    public Animation getOutAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, false);
        return AnimationUtils.loadAnimation(context, res);
    }

    public void setKeyBackCancelable(boolean isCancelable) {
        ViewGroup View;
        View = rootView;
        View.setFocusable(isCancelable);
        View.setFocusableInTouchMode(isCancelable);
        if (isCancelable) {
            View.setOnKeyListener(onKeyBackListener);
        } else {
            View.setOnKeyListener(null);
        }
    }

    private View.OnKeyListener onKeyBackListener = (v, keyCode, event) -> {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == MotionEvent.ACTION_DOWN
                && isShowing()) {
            dismiss();
            return true;
        }
        return false;
    };

    /**
     * Called when the user touch on black overlay in order to dismiss the dialog
     */
    @SuppressLint("ClickableViewAccessibility")
    private final View.OnTouchListener onCancelableTouchListener = (v, event) -> {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            dismiss();
        }
        return false;
    };

    private void setOutSideCancelable(boolean isCancelable) {
        if (rootView != null) {
            View view = rootView.findViewById(com.netease.nim.uikit.R.id.outmost_container);

            if (isCancelable) {
                view.setOnTouchListener(onCancelableTouchListener);
            } else {
                view.setOnTouchListener(null);
            }
        }

    }

    public interface OnSelectFileFolder {
        void onSelectFile(ArrayList<FileModel> fileFolders, int fileIndex,FileFolderModel selectFolder );

        void onDismissSeletePop();
    }

    private OnSelectFileFolder mOnSelectFileFolder;

    public void setmOnSelectFileFolder(OnSelectFileFolder mOnSelectFileFolder) {
        this.mOnSelectFileFolder = mOnSelectFileFolder;
    }
}


