package com.tojoy.cloud.online_meeting.App.Live.ExporeOnline;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.live.ApplyExploreCountResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigCacheManager;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigConstantKey;
import com.netease.nim.uikit.business.liveroom.LiveCustomMessage;
import com.netease.nim.uikit.business.liveroom.LiveCustomMessageCode;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.LiveCustomMessageProvider;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.App.TJBaseApp;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 同台直播申请
 */
public class ApplyOnlineTipView {

    private Context mContext;
    private View mView;
    private TextView mApplyCountTv;
    private ArrayList<LiveCustomMessage> mApplyMessage = new ArrayList<>();
    private OperationCallback mBottomOperationCallback;
    private ApplyGuestListPopView mApplyGuestListPopView;

    public ApplyOnlineTipView(Context context, OperationCallback bottomOperationCallback) {
        mContext = context;
        mBottomOperationCallback = bottomOperationCallback;
    }

    public View getView() {
        if (mView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mView = aLayoutInflater.inflate(R.layout.view_guest_applytip_layout, null);
            this.mView.setVisibility(View.GONE);
        }
        initView();
        return mView;
    }

    private void initView() {
        mApplyCountTv = mView.findViewById(R.id.tv_apply_count);

        mView.setOnClickListener(v -> {
            if (mApplyGuestListPopView == null) {
                mApplyGuestListPopView = new ApplyGuestListPopView(mContext, mBottomOperationCallback);
            }
            mApplyGuestListPopView.show();
            mBottomOperationCallback.showBottomView(false);
            mBottomOperationCallback.dismissOperationView();

            mApplyGuestListPopView.setmOperationCallback(new ApplyOperationCallback() {
                @Override
                public void onAgreen(int index, String applyForId, String userId) {
                    for (int i = 0; i < mApplyMessage.size(); i++) {
                        if (mApplyMessage.get(i).fromUserId.equals(userId)) {
                            mApplyMessage.get(i).applyAgreenStatus = 1;
                            break;
                        }
                    }
                    applyOrInviteExplor(index, LiveCustomMessageCode.AGREE_EXPORE, applyForId, userId);
                }

                @Override
                public void onRefuse(int index, String applyForId, String userId) {
                    for (int i = 0; i < mApplyMessage.size(); i++) {
                        if (mApplyMessage.get(i).fromUserId.equals(userId)) {
                            mApplyMessage.get(i).applyAgreenStatus = 2;
                            break;
                        }
                    }
                    applyOrInviteExplor(index, LiveCustomMessageCode.REFUSE_EXPORE, applyForId, userId);
                }
            });

            //19.6.2 点击显示弹窗的时候 请求当前申请人数
            getCurrentApplyExploreCount();
        });
    }

    private void applyOrInviteExplor(int index, LiveCustomMessageCode code, String applyId, String userId) {
        ExporeApiManager.mResponseForApplyOrInviteExplore(code, applyId, userId, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(OMBaseResponse baseResponse) {
                if (baseResponse.isSuccess()) {
                    if (code == LiveCustomMessageCode.AGREE_EXPORE) {
                        LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                                LiveCustomMessageProvider.getAgreeApplyExporeData(TJBaseApp.getInstance(), userId, applyId), false);
                    }

                    if (code.getCode() == LiveCustomMessageCode.REFUSE_EXPORE.getCode()) {
                        LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                                LiveCustomMessageProvider.getRefuseApplyExporeData(TJBaseApp.getInstance(), userId), false);
                    }

                    mApplyGuestListPopView.refreshItemStatus(index, code == LiveCustomMessageCode.AGREE_EXPORE ? 1 : -1);
                    // 只要从列表拉取了，初始化的值就没有意义了
                    applyCountInit = 0;
                    refreshApplyCount();
                } else {
                    Toasty.normal(mContext, baseResponse.msg).show();
                }
            }
        });
    }

    public void refreshApplyCount() {
        if (LiveRoomInfoProvider.getInstance().isGuest()) {
            return;
        }
        int noOperationCount = 0;
        for (LiveCustomMessage message : mApplyMessage) {
            if (message.applyAgreenStatus == 0) {
                noOperationCount++;
            }
        }

        if (applyCountInit > 0){
            noOperationCount = noOperationCount + applyCountInit;
        }

        if (noOperationCount > 0 && !LiveRoomInfoProvider.getInstance().isShowFile) {
            mView.setVisibility(View.VISIBLE);
            String title = TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1);
            String videoTitle = TextUtils.isEmpty(title) ? "人申请同台互动" : "人申请" + title;
            mApplyCountTv.setText(noOperationCount + videoTitle);
        } else {
            mView.setVisibility(View.GONE);
        }
    }

    /**
     * 有消息进来了
     * @param user
     */
    public void addApplyUser(LiveCustomMessage user) {
        for (LiveCustomMessage message : mApplyMessage) {
            //已经申请过，并且没有被处理过，则再次显示
            if (message.fromUserId.equals(user.fromUserId) && message.applyAgreenStatus == 0) {
                return;
            }
        }
        mApplyMessage.add(0, user);
        refreshApplyCount();
    }

    // 初始化时 mApplyMessage 没有数据，后续会有异常
    int applyCountInit = 0;
    /**
     * 初始化进入的时候查询
     *
     * @param count
     */
    private void refreshCountView(String count) {

        try {
            applyCountInit = Integer.parseInt(count);
        } catch (Exception e) {
            return;
        }
        if (applyCountInit > 0 && !LiveRoomInfoProvider.getInstance().isShowFile) {
            mView.setVisibility(View.VISIBLE);
            String title = TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1);
            String videoTitle = TextUtils.isEmpty(title) ? "人申请同台互动" : "人申请" + title;
            mApplyCountTv.setText(count + videoTitle);
        } else {
            // 当前无申请的人数则清零
            mApplyMessage.clear();
            mView.setVisibility(View.GONE);
        }
    }

    /**
     * 助手 & 主播 收到另一个人对同台的申请操作后刷新自己的数据
     */
    public void refreshOnlineExplorePop(String userId, boolean isAgree) {
        if (mApplyGuestListPopView != null) {
            mApplyGuestListPopView.refreshApply();
        }

        for (LiveCustomMessage message : mApplyMessage) {
            if (message.fromUserId.equals(userId)) {
                message.applyAgreenStatus = isAgree ? 1 : 2;
                break;
            }
        }

        refreshApplyCount();
    }

    /**
     * 获取当前申请直播的人数
     */
    public void getCurrentApplyExploreCount() {
        OMAppApiProvider.getInstance().mGetApplyCount(LiveRoomInfoProvider.getInstance().liveId, new Observer<ApplyExploreCountResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(ApplyExploreCountResponse response) {
                if (response.isSuccess()) {
                    refreshCountView(response.data.count);
                }
            }
        });
    }

}
