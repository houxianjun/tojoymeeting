package com.tojoy.cloud.online_meeting.App.Live.ExporeOnline;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.live.ExploreOnlineListResponse;
import com.tojoy.tjoybaselib.model.live.ExploreOnlineModel;
import com.tojoy.tjoybaselib.model.live.InviteApplyResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.KeyboardControlUtil;
import com.tojoy.cloud.online_meeting.App.Config.LiveRoomConfig;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.LiveCustomMessageProvider;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.OnlineMembersHelper;
import com.tojoy.cloud.online_meeting.App.Live.Live.Model.OnlineMember;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.Services.EventBus.EndLiveEventBus;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 邀请同台直播用户列表
 */
public class GuestListForInveteAct extends UI {

    RelativeLayout mUnicornProjectRlv;
    TJRecyclerView mRecyclerView;
    ArrayList<ExploreOnlineModel> mList = new ArrayList<>();
    ArrayList<ExploreOnlineModel> mSelectedMember = new ArrayList<>();
    ExploreOnlineAdapter mAdapter;

    TextView mCurrentTv;
    EditText mInputView;
    TextView mCanInviteCountTv;
    ImageView mIvSearchCancel;

    String mSearchKey = "";
    int mInviteMaxCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore_select_layout);
        EventBus.getDefault().register(this);
        initTitle();
        initView();

        mInviteMaxCount = LiveRoomConfig.ROOM_MAX_USER - 1 - OnlineMembersHelper.getInstance().mOnlineMembers.size();

        mCanInviteCountTv.setText("最多可邀请" + (mInviteMaxCount) + "位参会者和您同台互动");
    }

    public static void start(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, GuestListForInveteAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    private void initTitle() {
        setStatusBar();
        int statusBarHeight = AppUtils.getStatusBarHeight(this);
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.dip2px(this, 48));
        lps.setMargins(0, statusBarHeight, 0, 0);
        findViewById(R.id.rtl_normal_title).setLayoutParams(lps);
        mIvSearchCancel = (ImageView) findViewById(R.id.iv_search_cancel);

    }

    private void initView() {
        mCurrentTv = (TextView) findViewById(R.id.tv_canselect_count);
        mUnicornProjectRlv = (RelativeLayout) findViewById(R.id.mUnicornProjectRlv);
        mRecyclerView = new TJRecyclerView(this);
        mUnicornProjectRlv.addView(mRecyclerView);
        mRecyclerView.setLoadMoreView();
        View headerView = View.inflate(GuestListForInveteAct.this, R.layout.headerview_invite_count_layout, null);
        mCanInviteCountTv = headerView.findViewById(R.id.tv_can_select_count);
        mRecyclerView.addHeaderView(headerView);
        mRecyclerView.setEmptyImgAndTip(R.drawable.icon_empty_online_logo, "暂无参会者");
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                requestData();
            }

            @Override
            public void onLoadMore() {
                if (mList.size() == 0) {
                    return;
                }
                mPage++;
                requestData();
            }
        });
        mAdapter = new ExploreOnlineAdapter(this, mList);
        mAdapter.hasHeader = true;
        mRecyclerView.setAdapter(mAdapter);

        findViewById(R.id.iv_basetitle_leftimg).setOnClickListener(v -> finish());
        mInputView = (EditText) findViewById(R.id.basequery);
        setInputView();

        mAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
            if (mList.get(position).selectedSratus == 0) {
                mList.get(position).selectedSratus = 1;
                mSelectedMember.add(mList.get(position));

                //够数时其余的都改变
                if (mSelectedMember.size() == mInviteMaxCount) {
                    for (ExploreOnlineModel exploreOnlineModel : mList) {
                        if (exploreOnlineModel.selectedSratus == 0) {
                            exploreOnlineModel.selectedSratus = 2;
                        }
                    }
                }
            } else if (mList.get(position).selectedSratus == 1) {
                mList.get(position).selectedSratus = 0;
                mSelectedMember.remove(mList.get(position));

                //去除不可点击的 其余变为可点击
                for (ExploreOnlineModel exploreOnlineModel : mList) {
                    exploreOnlineModel.selectedSratus = 0;
                    doFilterStatus(exploreOnlineModel);
                }
            }
            mAdapter.updateData(mList);
            refreshOnlineCount();
        });
        findViewById(R.id.rlv_invete_online).setOnClickListener(v -> doInvite());
        requestData();
    }

    /**
     * 搜索处理
     */
    private void setInputView() {
        // 设置默认提示文案
        mInputView.setHint("请输入姓名进行搜索");
        // 清空搜索内容
        mIvSearchCancel.setOnClickListener(v -> mInputView.setText(""));

        InputFilter inputFilter = new InputFilter() {
            Pattern emoji = Pattern.compile("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]",
                    Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);

            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                Matcher emojiMatcher = emoji.matcher(source);
                if (emojiMatcher.find()) {
                    Toasty.normal(GuestListForInveteAct.this, "不支持输入表情").show();
                    return "";
                }
                return null;
            }
        };
        mInputView.setFilters(new InputFilter[]{inputFilter});

        mInputView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSearchKey = s.toString().trim();
                mPage = 1;
                //requestData();
                findViewById(R.id.iv_search_cancel).setVisibility(s.toString().length() > 0 ? View.VISIBLE : View.GONE);
                if (s.toString().length() == 0) {
                    KeyboardControlUtil.hideKeyboard(mInputView);
                }
                mIvSearchCancel.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mInputView.setOnEditorActionListener((arg0, arg1, arg2) -> {

            if (arg1 == EditorInfo.IME_ACTION_SEARCH) {
                mSearchKey = mInputView.getText().toString();

                if (!TextUtils.isEmpty(mSearchKey.trim())) {
                    requestData();
                }
            }

            return false;

        });
    }


    private void requestData() {
        OMAppApiProvider.getInstance().mSearchOnlineGuestList(LiveRoomInfoProvider.getInstance().liveId,
                mSearchKey,
                String.valueOf(mPage),
                new Observer<ExploreOnlineListResponse>() {
                    @Override
                    public void onCompleted() {
                        mRecyclerView.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(ExploreOnlineListResponse response) {
                        if (mPage == 1) {
                            mList.clear();
                            mList.addAll(refreshStatusForAdd(response.data.list));
                            mAdapter.updateData(mList);
                        } else {
                            mList.addAll(refreshStatusForAdd(response.data.list));
                            mAdapter.addMore(response.data.list);
                        }
                        mRecyclerView.refreshLoadMoreView(mPage, response.data.list.size());
                    }
                }
        );
    }

    /**
     * 翻页处理的时候，处理新增的数据
     */
    private ArrayList<ExploreOnlineModel> refreshStatusForAdd(ArrayList<ExploreOnlineModel> list) {

        //当已经选够人数的时候，加载更多页的时候需要默认置灰
        if (mSelectedMember.size() == mInviteMaxCount) {
            for (ExploreOnlineModel exploreOnlineModel : list) {
                exploreOnlineModel.selectedSratus = 2;
            }
            return list;
        }

        //还可以选择人数的时候，判断当前要加载的人还是否可以选择
        for (ExploreOnlineModel exploreOnlineModel : list) {
            doFilterStatus(exploreOnlineModel);
        }

        return list;
    }


    /**
     * 过滤用户的状态
     */
    private void doFilterStatus(ExploreOnlineModel exploreOnlineModel) {
        //  过滤已经选择的
        for (ExploreOnlineModel selectExplore : mSelectedMember) {
            if (selectExplore.userId.equals(exploreOnlineModel.userId)) {
                exploreOnlineModel.selectedSratus = 1;
                break;
            }
        }

        //  过滤当前同台的用户
        for (OnlineMember model : OnlineMembersHelper.getInstance().mOnlineMembers) {
            if (model.userId.equals(exploreOnlineModel.userId)) {
                exploreOnlineModel.selectedSratus = 2;
                break;
            }
        }

        // 过滤主播 & 助手
        if ("01".contains(exploreOnlineModel.userType)) {
            exploreOnlineModel.selectedSratus = 2;
        }
    }

    /**
     * 刷新当前在线人数
     */
    private void refreshOnlineCount() {
        mSelectedMember.clear();
        for (ExploreOnlineModel model : mList) {
            if (model.selectedSratus == 1) {
                mSelectedMember.add(model);
            }
        }

        if (mSelectedMember.size() > 0) {
            mCurrentTv.setText("邀请以上" + mSelectedMember.size() + "人同台互动");
            findViewById(R.id.rlv_invete_online).setBackgroundResource(R.drawable.bg_seize_btn);
        } else {
            mCurrentTv.setText("邀请");
            findViewById(R.id.rlv_invete_online).setBackgroundResource(R.drawable.bg_7b8090);
        }
    }


    /**
     * 开始邀请
     */
    private void doInvite() {
        if (!OnlineMembersHelper.getInstance().canOnlineNow()) {
            return;
        }

        if (mSelectedMember.size() == 0) {
            Toasty.normal(this, "请选择邀请人").show();
        } else if (mSelectedMember.size() > mInviteMaxCount) {
            Toasty.normal(this, "最多可邀请" + (mInviteMaxCount) + "位参会者和您同台互动").show();
        } else {
            String selectedStr = "";
            for (ExploreOnlineModel exploreOnlineModel : mSelectedMember) {
                selectedStr = selectedStr + exploreOnlineModel.userId + ",";
            }
            showProgressHUD(this, "处理中");
            ExporeApiManager.mInviteExploreSomebody(selectedStr.substring(0, selectedStr.length() - 1), new Observer<InviteApplyResponse>() {
                @Override
                public void onCompleted() {
                    finish();
                    dismissProgressHUD();
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onNext(InviteApplyResponse response) {
                    if (response.isSuccess()) {
                        for (InviteApplyResponse.InviteApplyResModel model : response.data) {
                            LiveCustomMessageProvider.sendLiveRoomCustomMessage(
                                    model.receiveId,
                                    LiveCustomMessageProvider.getInviteApplyExporeData(GuestListForInveteAct.this, model.receiveId, model.applyForId), false);
                        }
                    } else {
                        Toasty.normal(GuestListForInveteAct.this, response.msg).show();
                    }
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()) {
            KeyboardControlUtil.hideKeyboard(findViewById(R.id.rlv_invete_online));
            findViewById(R.id.rlv_invete_online).setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        findViewById(R.id.rlv_invete_online).setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEndLiveEvent(EndLiveEventBus event) {
        if (event.isEnd) {
            finish();
        }
    }
}
