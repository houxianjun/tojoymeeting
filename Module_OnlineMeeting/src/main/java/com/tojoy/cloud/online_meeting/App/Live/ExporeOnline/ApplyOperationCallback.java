package com.tojoy.cloud.online_meeting.App.Live.ExporeOnline;

public interface ApplyOperationCallback {
    void onAgreen(int index,String applyForId,String userId);

    void onRefuse(int index,String applyForId,String userId);
}
