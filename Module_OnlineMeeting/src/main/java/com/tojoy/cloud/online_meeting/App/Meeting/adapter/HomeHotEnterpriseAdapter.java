package com.tojoy.cloud.online_meeting.App.Meeting.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.model.home.HomeModelResponse;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;

/**
 * 首页热门企业横向列表适配器
 */
public class HomeHotEnterpriseAdapter extends BaseRecyclerViewAdapter<HomeModelResponse.HotEnterPriseModel> {


    public HomeHotEnterpriseAdapter(@NonNull Context context, @NonNull List<HomeModelResponse.HotEnterPriseModel> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull HomeModelResponse.HotEnterPriseModel data) {
        return position;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_home_hot_enterprise;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new HotEnterpriseVH(context, parent, getLayoutResId(viewType));
    }


    public static class HotEnterpriseVH extends BaseRecyclerViewHolder<HomeModelResponse.HotEnterPriseModel> {

        private TextView mEnterpriseNameTv;
        private ImageView mEnterpriseIconIv;
;

        public HotEnterpriseVH(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            mEnterpriseNameTv = itemView.findViewById(R.id.tv_enterprise_name);
            mEnterpriseIconIv = itemView.findViewById(R.id.iv_enterprise_icon);
        }

        @Override
        public void onBindData(HomeModelResponse.HotEnterPriseModel data, int position) {
            ImageLoaderManager.INSTANCE.loadHomeHotCompnayIcon(getContext(), mEnterpriseIconIv,data.logoUrl, R.drawable.img_search_company_default,"#DDE2EB",1);
            mEnterpriseNameTv.setText(data.companyName);
        }
    }
}
