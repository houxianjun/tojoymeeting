package com.tojoy.cloud.online_meeting.App.Meeting.view;

import android.content.Context;
import android.widget.ImageView;

import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.ui.Banner.loader.ImageLoader;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.R;


public class MeetingHomeBannerImageLoader extends ImageLoader {
    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {
        ImageLoaderManager.INSTANCE.loadWantonlyRadiu(context,
                imageView,
                OSSConfig.getRemoveStylePath((String) path),
                R.drawable.icon_online_meeting_home_video_defalut,
                R.drawable.icon_online_meeting_home_video_defalut,
                AppUtils.dip2px(context, 3),
                false, false, false, false);
    }
}
