package com.tojoy.cloud.online_meeting.App.Apply.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tojoy.tjoybaselib.ui.editView.ClearEmojInputFilter;
import com.tojoy.tjoybaselib.util.time.TimeUtility;
import com.tojoy.cloud.online_meeting.App.Apply.activity.ApplyAct;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApplyPhotoInfo;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * @author qll
 * @date 2019/4/18
 */
public class SubmitFileOrCoverAdapter extends RecyclerView.Adapter<SubmitFileOrCoverAdapter.ViewHolder> {
    private Context context;
    private List<ApplyPhotoInfo> data;
    private String mShowType;
    // 是否为封面
    private boolean mIsCover;

    private boolean mIsFirstShowToast = true;

    public SubmitFileOrCoverAdapter(Context context, List<ApplyPhotoInfo> data, String type, boolean isCover) {
        this.context = context;
        this.data = data;
        this.mShowType = type;
        this.mIsCover = isCover;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mIsCover) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_submit_cover_view, parent, false);
            return new ViewHolder(view, mIsCover);
        } else {
            View view = LayoutInflater.from(context).inflate(R.layout.item_submit_file_view, parent, false);
            return new ViewHolder(view, mIsCover);
        }
    }

    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        if (TextUtils.isEmpty(data.get(position).getFilePath()) && TextUtils.isEmpty(data.get(position).ossUrl)) {
            holder.mPhotoView.setScaleType(ImageView.ScaleType.CENTER);
            Glide.with(context)
                    .load("text")
                    .apply(new RequestOptions().placeholder(R.drawable.icon_online_add_photo))
                    .into(holder.mPhotoView);

            holder.mDeleteImg.setVisibility(View.GONE);
            if (!mIsCover) {
                holder.mFileNameEt.setVisibility(View.GONE);
                holder.mFileNameTv.setVisibility(View.GONE);
            }
        } else {
            if ("ppt".equals(data.get(position).type)
                    || data.get(position).getGileUrl().endsWith("ppt")
                    || data.get(position).getGileUrl().endsWith("pptx")) {
                holder.mPhotoView.setScaleType(ImageView.ScaleType.CENTER);
                Glide.with(context)
                        .load("ppt")
                        .apply(new RequestOptions().placeholder(R.drawable.icon_file_ppt))
                        .into(holder.mPhotoView);
            } else {
                // 剩下的当作图片处理
                holder.mPhotoView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                Glide.with(context).load(data.get(position).getGileUrl()).apply(new RequestOptions().placeholder(R.drawable.icon_online_meeting_home_grid_default)).into(holder.mPhotoView);
            }

            if (ApplyAct.TYPE_EDIT.equals(mShowType)) {
                holder.mDeleteImg.setVisibility(View.VISIBLE);
                if (!mIsCover) {
                    String nowTime = TimeUtility.getTimeFormat(System.currentTimeMillis(), "yyyyMMdd");
                    String fileName = nowTime.substring(nowTime.length() - 6) + (position + 1);

                    data.get(position).name = TextUtils.isEmpty(data.get(position).name) ? fileName : data.get(position).name;

                    holder.mFileNameEt.setVisibility(View.VISIBLE);
                    holder.mFileNameTv.setVisibility(View.GONE);
                    holder.mFileNameEt.setText(data.get(position).name);
                    holder.mFileNameEt.setMaxEms(20);
                    InputFilter[] emojiFilters = {new ClearEmojInputFilter()};
                    holder.mFileNameEt.setFilters(emojiFilters);
                    holder.mFileNameEt.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            int lenght = s.length();
                            data.get(position).name = s.toString();
                            if (lenght > 20) {
                                holder.mFileNameEt.setText(holder.mFileNameEt.getText().subSequence(0, 20));
                                holder.mFileNameEt.setSelection(20);
                                if (mIsFirstShowToast) {
                                    Toasty.normal(context, "最多输入20字").show();
                                    mIsFirstShowToast = false;
                                }
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                }
            } else {
                holder.mDeleteImg.setVisibility(View.GONE);
                if (!mIsCover) {
                    holder.mFileNameTv.setVisibility(View.VISIBLE);
                    holder.mFileNameEt.setVisibility(View.GONE);
                    holder.mFileNameTv.setMaxEms(20);
                    holder.mFileNameTv.setText(data.get(position).name);
                }
            }
            holder.mProgressBar.setVisibility(View.GONE);
        }
        holder.mPhotoView.setOnClickListener(v -> {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onPhotoClick(position, holder.mPhotoView);
            }
        });
        holder.mDeleteImg.setOnClickListener(v -> {
            if (mOnItemClickListener != null ) {
                if (TextUtils.isEmpty(data.get(position).ossUrl) && !TextUtils.isEmpty(data.get(position).getFilePath())) {
                    mOnItemClickListener.onCancleUpload(position);
                } else {
                    mOnItemClickListener.onPhotoDelete(position,
                            TextUtils.isEmpty(data.get(position).ossUrl) ? data.get(position).getFilePath() : data.get(position).ossUrl);
                }
            }
        });

        if (!TextUtils.isEmpty(data.get(position).getFilePath()) && TextUtils.isEmpty(data.get(position).ossUrl)){
            holder.mProgressBar.setVisibility(View.VISIBLE);
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onUploadStart(position);
            }
        } else {
            holder.mProgressBar.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mPhotoView;
        private ImageView mDeleteImg;
        private EditText mFileNameEt;
        private TextView mFileNameTv;
        private ProgressBar mProgressBar;

        public ViewHolder(View itemView, boolean isCover) {
            super(itemView);
            mPhotoView = itemView.findViewById(R.id.iv_photo);
            mDeleteImg = itemView.findViewById(R.id.iv_delete);
            mProgressBar = itemView.findViewById(R.id.pb_upload);
            if (!isCover) {
                mFileNameEt = itemView.findViewById(R.id.et_file_name);
                mFileNameTv = itemView.findViewById(R.id.tv_file_name);
            }
        }
    }

    /**
     * 点击回调
     */
    public interface OnItemClickListener {

        /**
         * 选中图片放大显示
         */
        void onPhotoClick(int index, ImageView imageView);

        /**
         * 删除图片
         */
        void onPhotoDelete(int index,String path);

        /**
         * 图片上传
         */
        void onUploadStart(int index);

        /**
         * 先取消该图片的上传，再删除图片
         */
        void onCancleUpload(int index);
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }
}
