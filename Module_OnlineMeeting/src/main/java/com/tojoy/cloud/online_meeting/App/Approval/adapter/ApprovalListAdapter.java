package com.tojoy.cloud.online_meeting.App.Approval.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.imageManager.YuanJiaoImageView;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.cloud.online_meeting.App.Apply.activity.ApplyAct;
import com.tojoy.cloud.online_meeting.App.Apply.adapter.ApplyResultLeaderAdapter;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApplyJoinLiveModel;
import com.tojoy.cloud.online_meeting.App.Approval.activity.ApprovalListAct;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApprovalModel;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;

import java.util.List;

/**
 * @author qululu
 * 搜索结果列表适配器
 */
public class ApprovalListAdapter extends BaseRecyclerViewAdapter<ApprovalModel> {

    public ApprovalListAdapter(@NonNull ApprovalListAct context, @NonNull List<ApprovalModel> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull ApprovalModel data) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_approval_list_layout;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new ApprovalViewHolder(context, parent, getLayoutResId(viewType));
    }

    /**
     * ViewHolder
     */
    public class ApprovalViewHolder extends BaseRecyclerViewHolder<ApprovalModel> {

        TextView mAgreeApplyTv;
        TextView mNoAgreeApplyTv;
        YuanJiaoImageView mUserHeadIv;
        TextView mNameTv;
        TextView mCompanyJobTv;
        TextView mRoomNameTv;
        TextView mStartReasonTv;
        TextView mSeeDetailTv;
        TextView mApplyTimeTv;
        RecyclerView mLeaderRecyclerView;
        TextView mRejectMsgTv;
        LinearLayout mStartReasonLlv;
        ImageView mApprovalStateIv;

        ApprovalViewHolder(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            mAgreeApplyTv = itemView.findViewById(R.id.tv_agree_apply);
            mNoAgreeApplyTv = itemView.findViewById(R.id.tv_no_agree_apply);
            mUserHeadIv = itemView.findViewById(R.id.iv_user_head);
            mNameTv = itemView.findViewById(R.id.tv_name);
            mCompanyJobTv = itemView.findViewById(R.id.tv_company_job);
            mRoomNameTv = itemView.findViewById(R.id.tv_room_name);
            mStartReasonTv = itemView.findViewById(R.id.tv_start_reason);
            mSeeDetailTv = itemView.findViewById(R.id.tv_see_detail);
            mApplyTimeTv = itemView.findViewById(R.id.tv_apply_time);
            mLeaderRecyclerView = itemView.findViewById(R.id.recycler_leader);
            mRejectMsgTv = itemView.findViewById(R.id.tv_reject_msg);
            mStartReasonLlv = itemView.findViewById(R.id.llv_start_reason);
            mApprovalStateIv = itemView.findViewById(R.id.iv_approval_state);
        }

        @Override
        public void onBindData(ApprovalModel data, int position) {
            // 设置头像
            mUserHeadIv.setType(1);
            mUserHeadIv.setBorderRadius(24);
            mUserHeadIv.setWidthHeight(24, 24);
            ImageLoaderManager.INSTANCE.loadHeadImage(getContext(),
                    mUserHeadIv,
                    data.getHeadPicUrl(),
                    R.drawable.icon_dorecord);
            // 设置姓名
            mNameTv.setText(data.getApplyName());
            // 设置公司职位
            mCompanyJobTv.setText(data.getCompanyAndJob());
            // 设置房间主题名称
            mRoomNameTv.setText(data.getRoomName());
            // 设置开播理由
            if (TextUtils.isEmpty(data.getStartReason())) {
                mStartReasonLlv.setVisibility(View.GONE);
            } else {
                mStartReasonLlv.setVisibility(View.VISIBLE);
                mStartReasonTv.setText(data.getStartReason());
            }

            // 设置开会申请时间
            mApplyTimeTv.setText(data.getApplyTime());
            // 根据是否操作过了显示"同意"按钮的颜色
            switch (data.getStatus()) {
                case "1":
                    // 未处理
                    mNoAgreeApplyTv.setText("驳回");
                    mAgreeApplyTv.setText("同意申请");
                    // 未处理
                    mAgreeApplyTv.setTextColor(context.getResources().getColor(R.color.color_2a62dd));
                    mAgreeApplyTv.setBackgroundResource(R.drawable.shape_approval_blue);
                    break;
                case "2":
                    // 本人操作驳回
                    mNoAgreeApplyTv.setText("已驳回");
                    mAgreeApplyTv.setText("同意申请");
                    // 处理过
                    mAgreeApplyTv.setTextColor(context.getResources().getColor(R.color.color_7a8090));
                    mAgreeApplyTv.setBackgroundResource(R.drawable.shape_approval_grey);
                    break;
                case "3":
                    // 本人已同意
                    mAgreeApplyTv.setText("已同意");
                    mNoAgreeApplyTv.setText("驳回");
                    // 处理过
                    mAgreeApplyTv.setTextColor(context.getResources().getColor(R.color.color_7a8090));
                    mAgreeApplyTv.setBackgroundResource(R.drawable.shape_approval_grey);
                    break;
                case "4":
                    // 后台管理员驳回
                    mNoAgreeApplyTv.setText("驳回");
                    mAgreeApplyTv.setText("同意申请");
                    // 置灰，不能点击
                    mAgreeApplyTv.setTextColor(context.getResources().getColor(R.color.color_7a8090));
                    mAgreeApplyTv.setBackgroundResource(R.drawable.shape_approval_grey);
                    break;
                case "5":
                    // 申请者自己撤回，但不可以操作
                    mNoAgreeApplyTv.setText("驳回");
                    mAgreeApplyTv.setText("同意申请");
                    // 置灰，不能点击
                    mAgreeApplyTv.setTextColor(context.getResources().getColor(R.color.color_7a8090));
                    mAgreeApplyTv.setBackgroundResource(R.drawable.shape_approval_grey);
                    break;
                default:
                    // 其他情况，均不可以操作
                    mNoAgreeApplyTv.setText("驳回");
                    mAgreeApplyTv.setText("同意申请");
                    // 置灰，不能点击
                    mAgreeApplyTv.setTextColor(context.getResources().getColor(R.color.color_7a8090));
                    mAgreeApplyTv.setBackgroundResource(R.drawable.shape_approval_grey);
                    break;
            }
            ApplyResultLeaderAdapter mAdapter = new ApplyResultLeaderAdapter(getContext(), data.auditList,"approval");
            mAdapter.setApplyStatus(data.getStatus());
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            //设置布局管理器
            mLeaderRecyclerView.setLayoutManager(layoutManager);
            //设置为垂直布局，这也是默认的
            layoutManager.setOrientation(OrientationHelper.VERTICAL);
            mLeaderRecyclerView.setOverScrollMode(View.OVER_SCROLL_NEVER);
            mLeaderRecyclerView.setAdapter(mAdapter);
            // 若本人操作驳回则显示驳回理由
            if ("2".equals(data.getStatus())){
                // 已驳回，显示驳回理由
                mRejectMsgTv.setVisibility(View.VISIBLE);
                mRejectMsgTv.setText("驳回理由:    " + data.getRejectMsg());
            } else {
                // 否则不显示
                mRejectMsgTv.setVisibility(View.GONE);
            }

            if ("5".equals(data.getStatus())) {
                // 已撤回，显示图标
                mApprovalStateIv.setVisibility(View.VISIBLE);
            } else {
                mApprovalStateIv.setVisibility(View.GONE);
            }

            setIteamClicklistener(data);
        }

        private void setIteamClicklistener(ApprovalModel data) {
            if (!data.isCheckenOver()) {
                // 判断是否处理过，处理过不响应点击事件
                mAgreeApplyTv.setOnClickListener(v -> {
                    if (!"2".equals(data.getStatus()) && !"3".equals(data.getStatus()) && !"4".equals(data.getStatus()) && !"5".equals(data.getStatus())
                            && !"已驳回".equals(mNoAgreeApplyTv.getText().toString())
                            && !"已同意".equals(mAgreeApplyTv.getText().toString())) {
                        ((ApprovalListAct) context).applyCheck(data.checkId,data.applyId,"3","已同意");
                    }

                });
                mNoAgreeApplyTv.setOnClickListener(v -> {
                    if (!"2".equals(data.getStatus()) && !"3".equals(data.getStatus()) && !"4".equals(data.getStatus()) && !"5".equals(data.getStatus())
                            && !"已驳回".equals(mNoAgreeApplyTv.getText().toString())
                            && !"已同意".equals(mAgreeApplyTv.getText().toString())){
                        ((ApprovalListAct) context).showApplyCheckRemarkDialog(data);
                    }
                });
            }
            mSeeDetailTv.setOnClickListener(v -> {
                // 查看详情，跳转到开播详情页面
                ApplyJoinLiveModel joinLiveModel = new ApplyJoinLiveModel();
                joinLiveModel.applyId = data.applyId;
                joinLiveModel.status = 1;
                ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_START_LIVE_BROADCAST_APPLY)
                        .withString("type", ApplyAct.TYPE_SHOW)
                        .withSerializable("joinLiveModel",joinLiveModel)
                        .navigation();
            });
        }
    }
}
