package com.tojoy.cloud.online_meeting.App.Live.ExporeOnline;

import android.content.Context;

import com.tojoy.tjoybaselib.model.live.ExploreMemeberStatusResponse;
import com.tojoy.tjoybaselib.model.live.InviteApplyResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.LiveCustomMessageProvider;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.OnlineMembersHelper;
import com.netease.nim.uikit.business.liveroom.LiveCustomMessageCode;
import com.tojoy.common.App.TJBaseApp;

import rx.Observer;

/**
 * 同台直播操作接口
 */
public class ExporeApiManager {

    /**
     * 观众 - 申请同台直播
     */
    public static void mApplyExploreMe(Observer<OMBaseResponse> observer) {
        OMAppApiProvider.getInstance().mApplyExploreMe(LiveRoomInfoProvider.getInstance().roomId,
                LiveRoomInfoProvider.getInstance().liveId,
                BaseUserInfoCache.getUserId(TJBaseApp.getInstance()),
                LiveRoomInfoProvider.getInstance().hostUserId,
                new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 观众 - 开始同台直播
     */
    public static void mStartExploreMe(String applyForId, Observer<OMBaseResponse> observer) {
        OMAppApiProvider.getInstance().mStartExploreMe(applyForId,
                LiveRoomInfoProvider.getInstance().liveId, new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        observer.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        observer.onError(e);
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        observer.onNext(response);
                    }
                });
    }

    /**
     * 主播 - 结束直播
     */
    public static void mStopAllExploreLive(String guestId) {
        OMAppApiProvider.getInstance().mStopExploreMe(true,guestId,
                LiveRoomInfoProvider.getInstance().liveId, new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(OMBaseResponse response) {

                    }
                });
    }


    /**
     * 主播关闭某个用户、用户主动关闭自己
     */
    public static void mStopExploreSomebody(Context context, String someBodyUserId) {
        LiveCustomMessageProvider.sendLiveRoomCustomMessage(someBodyUserId,
                LiveCustomMessageProvider.getCloseExporeSomebodyData(TJBaseApp.getInstance(), someBodyUserId), false);
        OnlineMembersHelper.getInstance().onHostCloseExpore(someBodyUserId);

        try {
            ((BaseLiveAct) context).showProgressHUD(context, "");
        } catch (Exception e) {

        }

        OMAppApiProvider.getInstance().mStopExploreSomebody(someBodyUserId,
                LiveRoomInfoProvider.getInstance().liveId, new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        try {
                            ((BaseLiveAct) context).dismissProgressHUD();
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(OMBaseResponse response) {

                    }
                });
    }


    /**
     * 主播 - 邀请同台直播
     */
    static void mInviteExploreSomebody(String somebodysUserId, Observer<InviteApplyResponse> observer) {
        OMAppApiProvider.getInstance().mInviteExploreSomebody(LiveRoomInfoProvider.getInstance().liveId, somebodysUserId, observer);
    }


    /**
     * 主播 - 同意或拒绝同台直播申请
     * 观众 - 同意或拒绝同台直播邀请
     */
    public static void mResponseForApplyOrInviteExplore(LiveCustomMessageCode code, String applyId, String applyUserId, Observer<OMBaseResponse> observer) {
        String applyForId = "";
        String receiveId = "";
        String oprationType = "";
        String oprationStatus = "";

        //主播同意观众申请
        if (code.getCode() == LiveCustomMessageCode.AGREE_EXPORE.getCode()) {
            applyForId = applyId;
            receiveId = LiveRoomInfoProvider.getInstance().hostUserId;
            oprationType = "1";
            oprationStatus = "1";
        }

        //主播拒绝观众申请
        if (code.getCode() == LiveCustomMessageCode.REFUSE_EXPORE.getCode()) {
            applyForId = applyId;
            receiveId = LiveRoomInfoProvider.getInstance().hostUserId;
            oprationType = "1";
            oprationStatus = "-1";
        }

        //观众同意主播邀请
        if (code.getCode() == LiveCustomMessageCode.AGREE_INVITE_EXPORE.getCode()) {
            applyForId = applyId;
            receiveId = LiveRoomInfoProvider.getInstance().hostUserId;
            oprationType = "2";
            oprationStatus = "1";
        }

        //观众拒绝主播邀请
        if (code.getCode() == LiveCustomMessageCode.REFUSE_INVITE_EXPORE.getCode()) {
            applyForId = applyId;
            receiveId = LiveRoomInfoProvider.getInstance().hostUserId;
            oprationType = "2";
            oprationStatus = "-1";
        }


        OMAppApiProvider.getInstance().mResponseForApplyOrInviteExplore(LiveRoomInfoProvider.getInstance().roomId,
                applyForId,
                receiveId,
                oprationType,
                oprationStatus,
                observer);
    }

    /**
     * 主播设置观众的视频、音频开启关闭状态
     * <p>
     * 1：开启、0：关闭
     * 1：视频、2：音频
     */
    public static void mControlGuestMediaStatus(String guestId, String status, String type,String isCallIng) {
        OMAppApiProvider.getInstance().mControlGuestMediaStatus(LiveRoomInfoProvider.getInstance().liveId, guestId, status, type, isCallIng, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(OMBaseResponse response) {

            }
        });
    }

    /**
     * 带有回调的处理
     *
     * @param guestId
     * @param status
     * @param type
     */
    public static void mControlGuestMediaStatusWithObserver(String guestId, String status, String type,String isCallIng, Observer<OMBaseResponse> observer) {
        OMAppApiProvider.getInstance().mControlGuestMediaStatus(LiveRoomInfoProvider.getInstance().liveId, guestId, status, type, isCallIng, observer);
    }


    /**
     * 助手查询连麦观众的视频、音频开启关闭状态
     * <p>
     * 1：开启、0：关闭
     * 1：视频、2：音频
     */
    public static void refreshGuestMediaStatus(Observer<ExploreMemeberStatusResponse> observer) {
        OMAppApiProvider.getInstance().mGetGuestMediaStatus(LiveRoomInfoProvider.getInstance().liveId, new Observer<ExploreMemeberStatusResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(ExploreMemeberStatusResponse response) {
                if (response.isSuccess()) {
                    OnlineMembersHelper.getInstance().initOnlineMemberStatus(response.data);
                    if (observer != null) {
                        observer.onNext(response);
                    }
                }
            }
        });
    }

    /**
     * 设置某个用户上屏
     */
    public static void mSetBigScreenUser(String userId) {
        OMAppApiProvider.getInstance().mSetBigScreenUser(LiveRoomInfoProvider.getInstance().liveId, userId, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(OMBaseResponse response) {

            }
        });
    }

    public static void mSetBigScreenUserWithObserver(String userId, Observer<OMBaseResponse> observer) {
        OMAppApiProvider.getInstance().mSetBigScreenUser(LiveRoomInfoProvider.getInstance().liveId, userId, observer);
    }
}