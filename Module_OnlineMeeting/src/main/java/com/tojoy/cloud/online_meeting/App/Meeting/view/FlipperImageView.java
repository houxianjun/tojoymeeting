package com.tojoy.cloud.online_meeting.App.Meeting.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.R;

/**
 * Created by yongchaowang
 * on 2020-02-26.
 */
public class FlipperImageView extends FrameLayout {
    private Context mContext;
    private ViewFlipper mViewFlipper;
    private ImageView mOneImg;
    private int firstAnim = -1;
    private int secondAnim = -1;

    public FlipperImageView(@NonNull Context context) {
        super(context);
        initView(context);
    }

    public FlipperImageView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public FlipperImageView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    void initView(Context context) {
        this.mContext = context;
        inflate(mContext, R.layout.view_wyc_two_img_layout, this);
        mViewFlipper = findViewById(R.id.wyc_vf);
        mOneImg = findViewById(R.id.wyc_one_iv);
    }

    public void showPrevious() {
        mViewFlipper.stopFlipping();
        mViewFlipper.setInAnimation(mContext, getAnimFadeIn());
        mViewFlipper.setOutAnimation(mContext, getAnimFadeOut());
        mViewFlipper.showPrevious();
    }

    public void showNext() {
        mViewFlipper.stopFlipping();
        mViewFlipper.setInAnimation(mContext, getAnimFadeIn());
        mViewFlipper.setOutAnimation(mContext, getAnimFadeOut());
        mViewFlipper.showNext();
    }

    public void startFlipping() {
//        mViewFlipper.setInAnimation(mContext, getAnimFadeIn());
//        mViewFlipper.setOutAnimation(mContext, getAnimFadeOut());
//        mViewFlipper.startFlipping();
    }

    public void stopFlipping() {
//        mViewFlipper.stopFlipping();
    }

    private int getAnimFadeIn() {
        return firstAnim == -1 ? R.anim.anim_fade_in : firstAnim;
    }

    private int getAnimFadeOut() {
        return secondAnim == -1 ? R.anim.anim_fade_out : secondAnim;
    }

    /**
     * @param url
     * @param errImg
     * @param dpRadiusValue 第一张传入半径参数
     */
    public void setFirstImgUrl(String url, int errImg, int dpRadiusValue) {
        loadImg(url, mOneImg, errImg, dpRadiusValue);
    }

    /**
     * @param url
     * @param errImg
     * @param dpRadiusValue 第二张传入半径参数
     */
    public void setSecondImgUrl(String url, int errImg, int dpRadiusValue) {
//        loadImg(url, mTwoImg, errImg, dpRadiusValue);
    }

    public void setImageAnimation(int firstAnim, int secondAnim) {
        this.firstAnim = firstAnim;
        this.secondAnim = secondAnim;
    }

    /**
     * .error(errImg)
     *
     * @param url
     * @param imageView
     */
    public void loadImg(String url, ImageView imageView, int errImg, int dpRadiusValue) {
        if (imageView != null) {

            ImageLoaderManager.INSTANCE.loadCoverSizedImage(mContext, imageView, url, AppUtils.dip2px(mContext, dpRadiusValue), imageView.getHeight(), imageView.getWidth(), errImg);
        }

    }
}
