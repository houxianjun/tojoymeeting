package com.tojoy.cloud.online_meeting.App.Apply.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.netease.nim.uikit.api.NimUIKit;
import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.netease.nim.uikit.common.NIMCache;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.configs.UserPermissions;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApplyJoinLiveModel;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApplyResultLeaderInfo;
import com.tojoy.tjoybaselib.model.enterprise.CorporationMsgResponse;
import com.tojoy.tjoybaselib.model.enterprise.MyInvitationShareMsgResponse;
import com.tojoy.tjoybaselib.model.user.IMTokenResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.OnlineMeeting.ApplyStateCode;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.ActivityStack;
import com.tojoy.cloud.online_meeting.App.Apply.activity.ApplyAct;
import com.tojoy.cloud.online_meeting.App.Apply.adapter.ApplyResultLeaderAdapter;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.SeeLiveAct;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.App.BaseFragment;
import com.tojoy.common.LiveRoom.LiveSharePopview;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * @author qll
 * @date 2019/3/29
 * 开会申请--结果
 * 待审核、审核通过、审核驳回
 */
@SuppressLint("ValidFragment")
public class ApplyResultFragment extends BaseFragment {
    private ApplyStateCode mStateCoce;

    private ImageView mApplyResultIv;
    private TextView mApplyResultStateTitleTv;
    private Button mSubmitBtn;
    private RelativeLayout mApplyResultGoDetailRlv;
    private TextView mApplyErrorReasonTv;
    private RecyclerView mLeaderRecyclerView;
    private RelativeLayout mApplyResultRlv;
    private RelativeLayout mMyInvitationRlv;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Button mRecallBtn, mLeaderRecallBtn;
    private LinearLayout mApplySuccessRemindLlv;

    private List<ApplyResultLeaderInfo> mLeaderList = new ArrayList<>();
    ApplyJoinLiveModel mApplyJoinLiveModel;
    private boolean isFirstIn = true;
    private Context mContext;

    private boolean isHandle = false;

    public ApplyResultFragment() {
    }

    public ApplyResultFragment setApplyStateCode(ApplyStateCode applyStateCode, ApplyJoinLiveModel applyJoinLiveModel) {
        this.mStateCoce = applyStateCode;
        this.mApplyJoinLiveModel = applyJoinLiveModel;
        return this;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_apply_result, container, false);
        }
        isCreateView = true;
        return mView;
    }

    @Override
    protected void initUI() {
        super.initUI();
        mApplyResultRlv = mView.findViewById(R.id.rlv_apply_result);
        mApplyResultIv = mView.findViewById(R.id.iv_apply_result_state);
        mApplyResultStateTitleTv = mView.findViewById(R.id.tv_apply_result_state_title);
        mSubmitBtn = mView.findViewById(R.id.bt_submit);
        mApplyResultGoDetailRlv = mView.findViewById(R.id.rlv_apply_result_go_detail);
        mApplyErrorReasonTv = mView.findViewById(R.id.tv_apply_error_reason);
        mLeaderRecyclerView = mView.findViewById(R.id.recycler_leader);
        mMyInvitationRlv = mView.findViewById(R.id.rlv_my_invitation);
        mSwipeRefreshLayout = mView.findViewById(R.id.swiprefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.color_0f88eb);
        mRecallBtn = mView.findViewById(R.id.bt_recall);
        mApplySuccessRemindLlv = mView.findViewById(R.id.llv_remind);
        mLeaderRecallBtn = mView.findViewById(R.id.bt_leader_recall);
        initListener();

        setApplyResultInfo(mApplyJoinLiveModel);
    }

    private void initListener() {
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            if (getActivity() != null) {
                ((ApplyAct) getActivity()).requestJoinLiveApply(true);
            }
        });

        mSubmitBtn.setOnClickListener(v -> {
            if (FastClickAvoidUtil.isFastDoubleClick(mSubmitBtn.getId())) {
                return;
            }
            if ("马上开会".equals(mSubmitBtn.getText())) {
                OMAppApiProvider.getInstance().mCorporationQuery(BaseUserInfoCache.getCompanyCode(getActivity()), false, new rx.Observer<CorporationMsgResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(getActivity(), "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(CorporationMsgResponse omBaseResponse) {
                        if (omBaseResponse != null && omBaseResponse.isSuccess()) {

                            // 判断是否余额不足，余额不足不能开播
                            if (!TextUtils.isEmpty(omBaseResponse.data.isAllowJoin) && "1".equals(omBaseResponse.data.isAllowJoin)) {
                                if (AppConfig.isLoginIM) {
                                    preStartLive();
                                } else {
                                    loginIM(new LoginIMState() {
                                        @Override
                                        public void success() {
                                            preStartLive();
                                            statitiscIMLogin("1");
                                        }

                                        @Override
                                        public void faild() {
                                            dismissProgressHUD();
                                            Toasty.normal(mContext, "网络异常，请稍后再试").show();
                                            statitiscIMLogin("0");
                                        }
                                    });
                                }
                            } else {
                                Toasty.normal(getContext(),TextUtils.isEmpty(omBaseResponse.data.overdueFeesNotify) ?
                                        getResources().getString(R.string.apply_online_service_hint) : omBaseResponse.data.overdueFeesNotify).show();
                            }

                        } else {
                            if ("-2".equals(omBaseResponse.code)) {
                                // 企业禁用
                                getActivity().finish();
                            }
                            Toasty.normal(getActivity(), omBaseResponse.msg).show();
                        }


                    }
                });

            } else if ("重新申请".equals(mSubmitBtn.getText())) {
                isFirstIn = false;
                // 进入编辑页面重新申请，上次申请的数据要带入
                ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_START_LIVE_BROADCAST_APPLY)
                        .withString("type", ApplyAct.TYPE_EDIT)
                        .withSerializable("joinLiveModel", mApplyJoinLiveModel)
                        .navigation();
            }
        });
        mApplyResultGoDetailRlv.setOnClickListener(v -> {
            if (FastClickAvoidUtil.isFastDoubleClick(mApplyResultGoDetailRlv.getId())) {
                return;
            }
            isFirstIn = false;
            // 进入详情展示页，不可编辑纯展示
            ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_START_LIVE_BROADCAST_APPLY)
                    .withString("type", ApplyAct.TYPE_SHOW)
                    .withSerializable("joinLiveModel", mApplyJoinLiveModel)
                    .navigation();
        });

        mMyInvitationRlv.setOnClickListener(v -> {
            if (FastClickAvoidUtil.isFastDoubleClick(mMyInvitationRlv.getId())) {
                return;
            }
            isFirstIn = false;
            getMyInvitationMsg();
        });

        mRecallBtn.setOnClickListener(v -> {
            if (FastClickAvoidUtil.isFastDoubleClick(mRecallBtn.getId())) {
                return;
            }
            // 撤回申请
            if (isHandle) {
                // 已经有审批记录了不能点击
                return;
            }
            new TJMakeSureDialog(mContext, "确定撤回申请吗？",
                    view -> {
                        if (getActivity() != null) {
                            ((ApplyAct) getActivity()).reCallApply(mApplyJoinLiveModel.applyId);
                        }
                    }).setBtnText("确定", "取消").show();
        });
        mLeaderRecallBtn.setOnClickListener(v -> {
            if (FastClickAvoidUtil.isFastDoubleClick(mLeaderRecallBtn.getId())) {
                return;
            }
            // 领导撤回
            new TJMakeSureDialog(mContext, "确定撤回申请吗？",
                    view -> {
                        if (getActivity() != null) {
                            ((ApplyAct) getActivity()).reCallApply(mApplyJoinLiveModel.applyId);
                        }
                    }).setBtnText("确定", "取消").show();
        });
    }

    private void getMyInvitationMsg() {
        OMAppApiProvider.getInstance().generateInvitation(mApplyJoinLiveModel.roomId, BaseUserInfoCache.getUserId(mContext), mApplyJoinLiveModel.liveId, new Observer<MyInvitationShareMsgResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(mContext, "网络错误，请稍后再试").show();
            }

            @Override
            public void onNext(MyInvitationShareMsgResponse omBaseResponse) {
                if (omBaseResponse.isSuccess()) {
                    LiveSharePopview wxPopView = new LiveSharePopview(mContext, LiveSharePopview.STYLE_WHITE);
                    wxPopView.setLiveShareData(OSSConfig.getOSSURLedStr(omBaseResponse.data.ossUrl), omBaseResponse.data.invitationUrl,
                            omBaseResponse.data.roomName, omBaseResponse.data.roomLiveDesc,null);
                    wxPopView.show();
                } else {
                    Toasty.normal(mContext, omBaseResponse.msg).show();
                }
            }
        });
    }


    private void loginIM(LoginIMState loginIMState) {
        showProgressHUD(mContext, "加载中");

        OMAppApiProvider.getInstance().getIMToken(BaseUserInfoCache.getUserId(mContext), "0", new Observer<IMTokenResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                loginIMState.faild();
            }

            @Override
            public void onNext(IMTokenResponse imToken) {
                if (imToken.isSuccess()) {
                    BaseUserInfoCache.setIMAccountHeaderBase(mContext, imToken.data.env);
                    NimUIKit.setIMHeaderStr(imToken.data.env);
                    BaseUserInfoCache.saveIMTokenBase(mContext, imToken.data.token);

                    nimlogin(imToken.data.token, loginIMState);
                } else {
                    loginIMState.faild();
                }

            }
        });
    }

    private void nimlogin(String token, LoginIMState loginIMState) {
        String account = BaseUserInfoCache.getIMHeaderedAccount(mContext);
        NimUIKit.login(new LoginInfo(account, token), new RequestCallback<LoginInfo>() {
            @Override
            public void onSuccess(LoginInfo param) {
                NIMCache.setAccount(account);
                loginIMState.success();
            }

            @Override
            public void onFailed(int code) {
                loginIMState.faild();
            }

            @Override
            public void onException(Throwable exception) {
                loginIMState.faild();
            }
        });
    }


    private void preStartLive() {
        isFirstIn = false;
        if (ActivityStack.getActivityStackList() != null) {
            // 若站内有开播的页面先干掉开播页面,离开房间
            try {
                Activity activity = ActivityStack.getActivityStack().currentActivity();
                if (activity instanceof SeeLiveAct && SeeLiveAct.SINGTON != null) {
                    SeeLiveAct.SINGTON.finish();
                    SeeLiveAct.SINGTON = null;
                    LiveRoomIOHelper.leaveLiveRoom(mContext, new IOLiveRoomListener() {
                        @Override
                        public void onIOError(String error, int errorCode) {

                        }

                        @Override
                        public void onIOSuccess() {
                            startLive();
                        }
                    }, false);
                } else {
                    startLive();
                }
            } catch (Exception e) {
                startLive();
            }

        } else {
            startLive();
        }
    }

    @Override
    protected void lazyLoad() {
        super.lazyLoad();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isFirstIn) {
            // 从其他页面回来时刷新页面数据
            if (getActivity() != null) {
                ((ApplyAct) getActivity()).requestJoinLiveApply(true);
            }
        }
    }

    /**
     * 设置审核结果信息
     */
    public void setApplyResultInfo(ApplyJoinLiveModel info) {
        mSwipeRefreshLayout.setRefreshing(false);
        mApplyJoinLiveModel = info;
        this.mStateCoce = (info == null || info.getApplyStatus() == null) ? ApplyStateCode.getStateCode(0) : info.getApplyStatus();
        isHandle = false;
        mLeaderList.clear();
        if (mApplyJoinLiveModel != null && mApplyJoinLiveModel.respLiveApplyCheckDtoList != null){
            mLeaderList.addAll(mApplyJoinLiveModel.respLiveApplyCheckDtoList);
        }
        ApplyResultLeaderAdapter mAdapter = new ApplyResultLeaderAdapter(getContext(), mLeaderList, "apply");
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        //设置布局管理器
        mLeaderRecyclerView.setLayoutManager(layoutManager);
        //设置为垂直布局，这也是默认的
        layoutManager.setOrientation(OrientationHelper.VERTICAL);
        mLeaderRecyclerView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        mLeaderRecyclerView.setAdapter(mAdapter);

        switch (mStateCoce) {
            // 审核中
            case ApplyReview:
                mApplyResultRlv.setVisibility(View.VISIBLE);
                mApplyResultGoDetailRlv.setVisibility(View.VISIBLE);
                mApplyResultIv.setImageResource(R.drawable.icon_apply_review);
                mApplyResultStateTitleTv.setText("您的开会申请已提交！请等待回复！");
                mSubmitBtn.setVisibility(View.GONE);
                mApplyErrorReasonTv.setVisibility(View.GONE);
                mMyInvitationRlv.setVisibility(View.GONE);
                mApplySuccessRemindLlv.setVisibility(View.GONE);
                mLeaderRecallBtn.setVisibility(View.GONE);
                mRecallBtn.setVisibility(View.VISIBLE);

                // 根据当前是否有人审批判断按钮的显示颜色以及是否可以点击
                for (int i = 0; i < mLeaderList.size(); i++) {
                    ApplyResultLeaderInfo leaderInfo = mLeaderList.get(i);
                    if (!"1".equals(leaderInfo.status) && !"5".equals(leaderInfo.status)) {
                        // 只要状态不是未审批就是处理过
                        isHandle = true;
                        break;
                    }
                }
                if (isHandle) {
                    // 灰色
                    mRecallBtn.setTextColor(getActivity().getResources().getColor(R.color.white));
                    mRecallBtn.setBackground(getActivity().getResources().getDrawable(R.drawable.shape_apply_recall_cancle_bg));
                } else {
                    // 蓝色
                    mRecallBtn.setTextColor(getActivity().getResources().getColor(R.color.white));
                    mRecallBtn.setBackground(getActivity().getResources().getDrawable(R.drawable.shape_submit_examine_bg));
                }

                break;
            // 审核通过
            case ApplySuccess:
                mApplyResultRlv.setVisibility(View.VISIBLE);
                mApplyResultGoDetailRlv.setVisibility(View.VISIBLE);
                mApplyResultIv.setImageResource(R.drawable.icon_apply_success);
                mApplyResultStateTitleTv.setText("您的开会申请已通过！");
                mSubmitBtn.setVisibility(View.VISIBLE);
                mSubmitBtn.setText("马上开会");
                mApplyErrorReasonTv.setVisibility(View.GONE);
                // 判断是否是仅邀请人参加的会议
                if ("3".equals(mApplyJoinLiveModel.authType)) {
                    mMyInvitationRlv.setVisibility(View.GONE);
                } else {
                    mMyInvitationRlv.setVisibility(View.VISIBLE);
                }
                mApplySuccessRemindLlv.setVisibility(View.GONE);
                mRecallBtn.setVisibility(View.GONE);

                if (BaseUserInfoCache.getUserIsHavePermiss(getContext(),UserPermissions.NO_APPROVE)
                        && !TextUtils.isEmpty(mApplyJoinLiveModel.isShowRevert) && "1".equals(mApplyJoinLiveModel.isShowRevert)) {
                    // 领导且需要展示撤回按钮
                    mLeaderRecallBtn.setVisibility(View.VISIBLE);
                    mLeaderRecallBtn.setTextColor(getActivity().getResources().getColor(R.color.color_2a62dd));
                    mLeaderRecallBtn.setBackground(getActivity().getResources().getDrawable(R.drawable.shape_online_apply_blue_bg));
                } else {
                    mLeaderRecallBtn.setVisibility(View.GONE);
                }

                break;
            // 审核驳回
            case ApplyError:
                mApplyResultRlv.setVisibility(View.VISIBLE);
                mApplyResultGoDetailRlv.setVisibility(View.VISIBLE);
                mApplyResultIv.setImageResource(R.drawable.icon_apply_error);
                mApplyResultStateTitleTv.setText("您的开会申请已驳回！");
                mSubmitBtn.setVisibility(View.VISIBLE);
                mSubmitBtn.setText("重新申请");
                mApplyErrorReasonTv.setVisibility(View.VISIBLE);
                SpannableString spannableString = new SpannableString("驳回理由： " + (TextUtils.isEmpty(mApplyJoinLiveModel.remark) ? "" : mApplyJoinLiveModel.remark));
                spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#353f5e")), 0, 5, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                mApplyErrorReasonTv.setText(spannableString);
                mMyInvitationRlv.setVisibility(View.GONE);
                mApplySuccessRemindLlv.setVisibility(View.GONE);
                mRecallBtn.setVisibility(View.GONE);
                mLeaderRecallBtn.setVisibility(View.GONE);
                break;
            // 已撤回
            case ApplyReCall:
                mApplyResultRlv.setVisibility(View.VISIBLE);
                mApplyResultGoDetailRlv.setVisibility(View.VISIBLE);
                mApplyResultIv.setImageResource(R.drawable.icon_apply_recall);
                mApplyResultStateTitleTv.setText("您的开会申请已撤回");
                mApplyErrorReasonTv.setVisibility(View.GONE);
                mMyInvitationRlv.setVisibility(View.GONE);
                mApplySuccessRemindLlv.setVisibility(View.GONE);
                mRecallBtn.setVisibility(View.GONE);
                mLeaderRecallBtn.setVisibility(View.GONE);
                mSubmitBtn.setVisibility(View.VISIBLE);
                mSubmitBtn.setText("重新申请");
                break;
            default:
                break;

        }
    }

    /**
     * 马上开播，创建IMRoomId
     */
    public void startLive() {
        LiveRoomIOHelper.checkMediaPermission(mContext, new LiveRoomIOHelper.PermissionCallback() {
            @Override
            public void onGranted() {
                showProgressHUD(getContext(), "处理中");
                LiveRoomInfoProvider.getInstance().roomId = mApplyJoinLiveModel.roomId;
                LiveRoomIOHelper.startLive(getContext(), new IOLiveRoomListener() {
                    @Override
                    public void onIOError(String error, int errorCode) {
                        dismissProgressHUD();
                        if (errorCode == 121 && getActivity() != null) {
                            // 进入房间失败时刷新页面数据
                            ((ApplyAct) getActivity()).requestJoinLiveApply(true);
                        }
                        if (!TextUtils.isEmpty(error)) {
                            Toasty.normal(mContext, error).show();
                        }
                    }

                    @Override
                    public void onIOSuccess() {
                        //关闭页面
                        dismissProgressHUD();
                        ((ApplyAct) (mContext)).finish();
                    }
                });
            }

            @Override
            public void onDenied() {
                Toasty.normal(mContext, "请开启相关权限后重试").show();
            }
        });
    }

    private void statitiscIMLogin(String loginIMState) {

        OMAppApiProvider.getInstance().statitiscIMLogin(loginIMState, "enterLivePage", new rx.Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(OMBaseResponse omBaseResponse) {
            }
        });
    }

    interface LoginIMState {
        void success();

        void faild();
    }
}
