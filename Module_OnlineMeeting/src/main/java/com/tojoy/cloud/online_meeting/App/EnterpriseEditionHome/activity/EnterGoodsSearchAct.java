package com.tojoy.cloud.online_meeting.App.EnterpriseEditionHome.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.ui.activity.BaseSearchAct;
import com.tojoy.cloud.online_meeting.App.Home.activity.ProductSearchFragment;
import com.tojoy.common.App.BaseFragment;


@Route(path = RouterPathProvider.GoodsSearchAct)
public class EnterGoodsSearchAct extends BaseSearchAct {
    ProductSearchFragment mProductSearchFragment;

    private BaseFragment[] mFragments;

    public static void start(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, EnterGoodsSearchAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preSearch();
    }

    @Override
    protected void initUI() {
        super.initUI();
        setSearchHint("请输入搜索关键词");
        hideBottomLine();
    }

    private void initViewPager() {
        mFragments = new BaseFragment[1];
        //产品推荐列表相关
        mProductSearchFragment = new ProductSearchFragment(getIntent().getStringExtra("companyCode"));
        mFragments[0] = mProductSearchFragment;

        FragmentPagerAdapter mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments[position];
            }

            @Override
            public int getCount() {
                return mFragments.length;
            }
        };
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(1);
        mViewPager.setCurrentItem(0);
    }

    @Override
    protected void doSearch() {
        super.doSearch();
        if (mProductSearchFragment == null) {
            initViewPager();
        }

        String searchKey = mSeachEditView.getText().toString().trim();
        ((ProductSearchFragment) mFragments[0]).doSearch(searchKey);
    }

    @Override
    protected void stopSearch() {
        finish();
    }

}
