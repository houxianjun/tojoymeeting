package com.tojoy.cloud.online_meeting.App.Live.Live.Helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;

import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.cloud.online_meeting.App.Config.LiveRoomConfig;
import com.tojoy.tjoybaselib.model.live.SelectOutLiveModel;
import com.tojoy.tjoybaselib.model.live.ExploreMemeberStatusResponse;
import com.tojoy.cloud.online_meeting.App.Live.Live.Model.OnlineMember;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;

import java.util.ArrayList;
import java.util.Random;

/**
 * 当前连麦的人员控制
 */
public class OnlineMembersHelper {

    public ArrayList<OnlineMember> mOnlineMembers = new ArrayList<>();

    public boolean isHostVideoOn = true, isHostAudioOn = true;

    private OnlineMembersHelper() {

    }

    private static class SingletonInstance {
        @SuppressLint("StaticFieldLeak")
        private static final OnlineMembersHelper INSTANCE = new OnlineMembersHelper();
    }

    public static OnlineMembersHelper getInstance() {
        return OnlineMembersHelper.SingletonInstance.INSTANCE;
    }


    public interface OnlineMemberChangeCallback {
        void onExploreMemberCountChange();
    }


    public boolean someBodyHasOnline(String id) {
        for (OnlineMember onlineMember : mOnlineMembers) {
            if (onlineMember.userId.equals(id)) {
                return true;
            }
        }
        return false;
    }


    public boolean canOnlineNow() {
        return mOnlineMembers.size() < LiveRoomConfig.ROOM_MAX_USER - 1;
    }

    public boolean hasOnlineMember() {
        LiveRoomInfoProvider.getInstance().isCountingUnread2 = mOnlineMembers.size() > 0;
        return mOnlineMembers.size() > 0;
    }

    public OnlineMember getMemberByUserId(String userId) {
        for (OnlineMember onlineMember : mOnlineMembers) {
            if (onlineMember.userId.equals(userId)) {
                return onlineMember;
            }
        }
        return null;
    }

    public boolean isMyselfExploring(Context context) {
        return getMemberByUserId(BaseUserInfoCache.getUserId(context)) != null;
    }

    public void onMemeberIn(String userId) {
        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "onlineMember-onMemberIn:userId:" + userId);
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        for (OnlineMember onlineMember : mOnlineMembers) {
            if (onlineMember.userId.equals(userId)) {
                //引流默认静音
                onlineMember.isOpenAudio = userId.length() <= 10;
                onlineMember.isOpenVideo = true;
                // 同台人员进入默认未接打电话
                onlineMember.isCallPhoneIng = false;
                return;
            }
        }

        if (!LiveRoomInfoProvider.getInstance().isHost(userId)) {
            OnlineMember onlineMember = new OnlineMember(userId);
            onlineMember.enterTime = System.currentTimeMillis();
            onlineMember.isOpenAudio = userId.length() > 10 ? false : true;
            // 同台人员进入默认未接打电话
            onlineMember.isCallPhoneIng = false;
            mOnlineMembers.add(onlineMember);
        }
    }

    public void onMemeberLeave(String userId) {
        for (OnlineMember onlineMember : mOnlineMembers) {
            if (onlineMember.userId.equals(userId)) {
                mOnlineMembers.remove(onlineMember);
                return;
            }
        }
    }

    /**
     * 主动关闭某个用户的同台状态
     *
     * @param userId
     */
    public void onHostCloseExpore(String userId) {
        for (OnlineMember onlineMember : mOnlineMembers) {
            if (onlineMember.userId.equals(userId)) {
                onlineMember.isHostCloseExpore = true;
                return;
            }
        }
    }


    public void doStopVideo(String userId) {

        if (LiveRoomInfoProvider.getInstance().isHost(userId)) {
            isHostVideoOn = false;
        }

        for (OnlineMember onlineMember : mOnlineMembers) {
            if (onlineMember.userId.equals(userId)) {
                onlineMember.isOpenVideo = false;
            }
        }
    }

    public void doStartVideo(String userId) {
        if (LiveRoomInfoProvider.getInstance().isHost(userId)) {
            isHostVideoOn = true;
        }

        for (OnlineMember onlineMember : mOnlineMembers) {
            if (onlineMember.userId.equals(userId)) {
                onlineMember.isOpenVideo = true;
            }
        }
    }

    public void doStopAudio(String userId) {
        if (LiveRoomInfoProvider.getInstance().isHost(userId)) {
            isHostAudioOn = false;
        }

        for (OnlineMember onlineMember : mOnlineMembers) {
            if (onlineMember.userId.equals(userId)) {
                onlineMember.isOpenAudio = false;
            }
        }
    }

    public void doStartAudio(String userId) {
        if (LiveRoomInfoProvider.getInstance().isHost(userId)) {
            isHostAudioOn = true;
        }

        for (OnlineMember onlineMember : mOnlineMembers) {
            if (onlineMember.userId.equals(userId)) {
                onlineMember.isOpenAudio = true;
            }
        }
    }

    /**
     * 初始化同台的音视频状态
     *
     * @param onlineMembers
     */
    public void initOnlineMemberStatus(ArrayList<ExploreMemeberStatusResponse.ExploreMemeberStatus> onlineMembers) {
        //处理同台
        for (OnlineMember onlineMember : mOnlineMembers) {
            for (ExploreMemeberStatusResponse.ExploreMemeberStatus exploreMemeberStatus : onlineMembers) {
                if (onlineMember.userId.equals(exploreMemeberStatus.selectUserId)) {

                    //初始化音频开关
                    if (TextUtils.isEmpty(exploreMemeberStatus.audioStatus)) {
                        onlineMember.isOpenAudio = true;
                    } else if ("1".equals(exploreMemeberStatus.audioStatus)) {
                        onlineMember.isOpenAudio = true;
                    } else {
                        onlineMember.isOpenAudio = false;
                    }

                    //初始化视频开关
                    if (TextUtils.isEmpty(exploreMemeberStatus.videoStatus)) {
                        onlineMember.isOpenVideo = true;
                    } else if ("1".equals(exploreMemeberStatus.videoStatus)) {
                        onlineMember.isOpenVideo = true;
                    } else {
                        onlineMember.isOpenVideo = false;
                    }

                    // 初始化接听电话的状态
                    if (TextUtils.isEmpty(exploreMemeberStatus.isCalling)) {
                        onlineMember.isCallPhoneIng = false;
                    } else if ("1".equals(exploreMemeberStatus.isCalling)) {
                        onlineMember.isCallPhoneIng = true;
                    } else {
                        onlineMember.isCallPhoneIng = false;
                    }
                }
            }
        }

        for (ExploreMemeberStatusResponse.ExploreMemeberStatus exploreMemeberStatus : onlineMembers) {
            if (LiveRoomInfoProvider.getInstance().isHost(exploreMemeberStatus.selectUserId)) {
                isHostVideoOn = "1".equals(exploreMemeberStatus.videoStatus);
                isHostAudioOn = "1".equals(exploreMemeberStatus.audioStatus);
                break;
            }
        }
    }

    /**
     * 初始化引流的音频状态
     */
    public void initOutliveStatus(ArrayList<SelectOutLiveModel> onlineMembers) {
        for (OnlineMember onlineMember : mOnlineMembers) {
            for (SelectOutLiveModel exploreMemeberStatus : onlineMembers) {
                if (onlineMember.userId.equals(exploreMemeberStatus.streamId)) {

                    //初始化音频开关
                    if (TextUtils.isEmpty(exploreMemeberStatus.audioStatus)) {
                        onlineMember.isOpenAudio = false;
                    } else if ("1".equals(exploreMemeberStatus.audioStatus)) {
                        onlineMember.isOpenAudio = true;
                    } else {
                        onlineMember.isOpenAudio = false;
                    }

                }
            }
        }
    }


    /**
     * 获取当前小窗的StreamIds
     */
    public String getCurrentOnlineStreams() {
        String streamId = LiveRoomInfoProvider.getInstance().getStreamIdWithUserId(LiveRoomInfoProvider.getInstance().hostUserId,LiveRoomConfig.getTXSDKAppID());

        for (OnlineMember onlineMember : mOnlineMembers) {
            if (onlineMember.userId.length() >= 10) {
                streamId = streamId + "," + onlineMember.userId;
            } else {
                streamId = streamId + "," + LiveRoomInfoProvider.getInstance().getStreamIdWithUserId(onlineMember.userId,LiveRoomConfig.getTXSDKAppID());
            }
        }

        LogUtil.i(LiveLogTag.LRMsgTag, "getCurrentOnlineStreams:" + streamId);

        return streamId;
    }

}
