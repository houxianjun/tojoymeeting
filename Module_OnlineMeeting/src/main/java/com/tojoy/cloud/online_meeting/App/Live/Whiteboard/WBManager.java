package com.tojoy.cloud.online_meeting.App.Live.Whiteboard;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.RelativeLayout;

import com.tojoy.tjoybaselib.model.live.DoumentVideoSendMsg;
import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tencent.rtmp.TXLog;
import com.tencent.teduboard.TEduBoardController;
import com.tencent.tic.core.TICClassroomOption;
import com.tencent.tic.core.TICManager;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.App.Config.LiveRoomConfig;
import com.tojoy.tjoybaselib.model.live.FileFolderModel;
import com.tojoy.tjoybaselib.model.live.FileModel;
import com.tojoy.cloud.online_meeting.App.Live.FileShow.FileSelectPop;
import com.netease.nim.uikit.business.liveroom.FileShowCallback;
import com.tojoy.cloud.online_meeting.App.Live.FileShow.FileShowView;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

import static com.tencent.teduboard.TEduBoardController.TEduBoardBackgroundH5Status.TEDU_BOARD_BACKGROUND_H5_STATUS_LOADING;
import static com.tencent.teduboard.TEduBoardController.TEduBoardBackgroundH5Status.TEDU_BOARD_BACKGROUND_H5_STATUS_LOAD_FINISH;
import static com.tencent.teduboard.TEduBoardController.TEduBoardErrorCode.TEDU_BOARD_ERROR_AUTH;
import static com.tencent.teduboard.TEduBoardController.TEduBoardErrorCode.TEDU_BOARD_ERROR_INIT;
import static com.tencent.teduboard.TEduBoardController.TEduBoardErrorCode.TEDU_BOARD_ERROR_LOAD;
import static com.tencent.teduboard.TEduBoardController.TEduBoardErrorCode.TEDU_BOARD_ERROR_TIM_INVALID;
import static com.tencent.teduboard.TEduBoardController.TEduBoardImageStatus.TEDU_BOARD_IMAGE_STATUS_LOADING;
import static com.tencent.teduboard.TEduBoardController.TEduBoardImageStatus.TEDU_BOARD_IMAGE_STATUS_LOAD_ABORT;
import static com.tencent.teduboard.TEduBoardController.TEduBoardImageStatus.TEDU_BOARD_IMAGE_STATUS_LOAD_DONE;
import static com.tencent.teduboard.TEduBoardController.TEduBoardImageStatus.TEDU_BOARD_IMAGE_STATUS_LOAD_ERROR;
import static com.tencent.teduboard.TEduBoardController.TEduBoardToolType.TEDU_BOARD_TOOL_TYPE_PEN;
import static com.tencent.teduboard.TEduBoardController.TEduBoardUploadStatus.TEDU_BOARD_UPLOAD_STATUS_FAILED;
import static com.tencent.teduboard.TEduBoardController.TEduBoardUploadStatus.TEDU_BOARD_UPLOAD_STATUS_SUCCEED;
import static com.tencent.teduboard.TEduBoardController.TEduBoardWarningCode.TEDU_BOARD_WARNING_H5PPT_ALREADY_EXISTS;
import static com.tencent.teduboard.TEduBoardController.TEduBoardWarningCode.TEDU_BOARD_WARNING_SYNC_DATA_PARSE_FAILED;
import static com.tencent.teduboard.TEduBoardController.TEduBoardWarningCode.TEDU_BOARD_WARNING_TIM_SEND_MSG_FAILED;


public class WBManager implements FileSelectPop.OnSelectFileFolder, FileShowView.ReSelectFileCallback {

    private static final String TAG = "WhiteboardManager";

    public static final String DRAWPAD_WB_ID = "#DEFAULT";

    public static final String CLASS_GROUP_TYPE = "ChatRoom";

    private int mRetryCount = 3;

    //Board
    private TEduBoardController mBoard;
    TJBoardCallback mBoardCallback;


    private WBEventCallback mWBEventCallback;

    private Context mContext;

    //文档演示
    private RelativeLayout mFileContainer;
    private FileShowView mFileShowView;

    //绘画版弹窗
    private DrawPadPop mDrawPadPop;

    //触发初始化的事件 ： -1 ：静默初始化、0：手绘板、1：文件演示
    private int mInitEvent = -1;

    // 初始化白板ID
    private String mInitBoarId = "";

    //是否是主动点击显示文档
    private boolean isFromClick;

    //操作回调
    private OperationCallback mOperationCallback;

    private FileSelectPop mFileSelectPop;

    public ArrayList<View> mAnimationControllView = new ArrayList<>();

    private boolean hasHidden = false;

    private int mRoomId;

    private int logoutCountMax = 3;

    public WBManager(Context context, WBEventCallback wBEventCallback, RelativeLayout fileWhiteboardView, OperationCallback operationCallback) {
        mContext = context;
        mBoard = new TEduBoardController(context);
        mWBEventCallback = wBEventCallback;
        mDrawPadPop = new DrawPadPop(context, operationCallback);
        mFileContainer = fileWhiteboardView;
        mOperationCallback = operationCallback;
        mFileShowView = new FileShowView(context, mOperationCallback);
        mFileShowView.getView();
        mDrawPadPop.setmOnDismissListener(() -> mOperationCallback.showBottomView(true));
        mFileShowView.setmReSelectFileCallback(this);
        mRoomId = Integer.parseInt(LiveRoomInfoProvider.getInstance().liveId) + 10000000;
    }

    public void setmInitEvent(int mInitEvent) {
        this.mInitEvent = mInitEvent;
    }

    public int getmInitEvent() {
        return mInitEvent;
    }

    public void setInitBoardId(String boardId) {
        this.mInitBoarId = boardId;
    }

    public void setFromClick(boolean fromClick) {
        isFromClick = fromClick;
    }

    /**
     * 登录SDK
     */
    public void loginTICSDK() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "loginTICSDK--UserId:" + BaseUserInfoCache.getUserId(mContext)+ ";liveMyUserType:" + LiveRoomInfoProvider.getInstance().myUserType);
        TICManager.getInstance().login(BaseUserInfoCache.getUserId(mContext),
                LiveRoomInfoProvider.getInstance().userSign, new TICManager.TICCallback() {
            @Override
            public void onSuccess(Object data) {
                LogUtil.i(LiveLogTag.WhiteboardTag, "login Success");
                if (LiveRoomInfoProvider.getInstance().isHost()) {
                    // 是主播先去创建房间
                    createClassRoom();
                } else {
                    // 是游客直接进入房间
                    joinClassRoom();
                }
                // 2.0版本joinClass不一定有回调
                initWhiteBoard();
            }

            @Override
            public void onError(String module, int errCode, String errMsg) {
                LogUtil.i(LiveLogTag.WhiteboardTag, "login Fail : code:" + errCode + ",msg:" + errMsg);
                Log.d("onError", "login Fail : code:" + errCode + ",msg:" + errMsg);
                onJoinClassRoomFail(0);
            }
        });
    }

    /**
     * 进入课堂
     */
    private void joinClassRoom() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "joinClassRoom:" + mRoomId);
        TICClassroomOption classroomOption = new TICClassroomOption().setClassId(mRoomId);

        TICManager.getInstance().joinClassroom(classroomOption, new TICManager.TICCallback() {
            @Override
            public void onSuccess(Object data) {
                LogUtil.i(LiveLogTag.WhiteboardTag, "joinClassroom Success : " + data);
            }

            @Override
            public void onError(String module, int errCode, String errMsg) {
                LogUtil.i(LiveLogTag.WhiteboardTag, "joinClassroom Fail : code:" + errCode + ",errMsg:" + errMsg);
                if (errCode == 10015 || errCode == 10010) {
                    createClassRoom();
                } else if (errCode == 71) {
                    //avsdk这个错误忽略
                    if (logoutCountMax > 0) {
                        logoutCountMax--;
                        TICManager.getInstance().logout(new TICManager.TICCallback() {
                            @Override
                            public void onSuccess(Object data) {
                                LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager logout Success");
                                loginTICSDK();
                            }

                            @Override
                            public void onError(String module, int errCode, String errMsg) {
                                LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager logout Fail: errMsg:" + errMsg + ",errCode:" + errCode + ",module:" + module);
                                onJoinClassRoomFail(0);
                            }
                        });
                    } else {
                        onJoinClassRoomFail(0);
                    }

                } else {
                    onJoinClassRoomFail(1);
                }
            }
        });
    }

    /**
     * 创建课堂
     */
    public void createClassRoom() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "createClassRoom:" + mRoomId);
        TICManager.getInstance().createClassroom(mRoomId, TICManager.TICClassScene.TIC_CLASS_SCENE_LIVE,new TICManager.TICCallback() {
            @Override
            public void onSuccess(Object data) {
                LogUtil.i(LiveLogTag.WhiteboardTag, "createClassroom Success : " + data);
                joinClassRoom();
            }

            @Override
            public void onError(String module, int errCode, String errMsg) {
                LogUtil.i(LiveLogTag.WhiteboardTag, "createClassroom Fail : code:" + errCode + ",errMsg:" + errMsg);
                mWBEventCallback.onInitWBFail(mInitEvent);
                if (errCode == 10021) {
                    Log.i(TAG, "该课堂已被他人创建，请\"加入课堂\"");
                    joinClassRoom();
                } else if (errCode == 10025) {
                    Log.i(TAG, "该课堂已创建，请\"加入课堂\"");
                    joinClassRoom();
                } else {
                    Log.i(TAG, "创建课堂失败, 房间号：" + mRoomId + " err:" + errCode + " msg:" + errMsg);
                    onJoinClassRoomFail(2);
                }
            }
        });
    }

    /**
     * 初始化成功
     */
    private void onJoinClassRoomSuccess() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "onJoinClassRoomSuccess");

        mDrawPadPop.initWhiteboard();
        mFileShowView.initWhiteBoard();

        mWBEventCallback.onInitWBSuccess(mInitEvent);

        if (mInitEvent == 0) {
            mOperationCallback.showDrawPad(isFromClick);
        } else if (mInitEvent == 1) {
            mOperationCallback.showFileShow(mInitBoarId, isFromClick);
        }
    }

    /**
     * @param type : 0\Login、1\Join、2\Create
     */
    private void onJoinClassRoomFail(int type) {
        if (mRetryCount > 0) {
            mRetryCount--;
            LogUtil.i(LiveLogTag.WhiteboardTag, "Retry:" + mRetryCount);
            new Handler().postDelayed(() -> {
                switch (type) {
                    case 0:
                        loginTICSDK();
                        break;
                    case 1:
                        joinClassRoom();
                        break;
                    case 2:
                        createClassRoom();
                        break;
                    default:
                        break;
                }
            }, 5000);
        }

        if (mRetryCount == 3 || type == 4) {
            mWBEventCallback.onInitWBFail(mInitEvent);
            mDrawPadPop.initWhiteboard();
            mFileShowView.initWhiteBoard();
        }
    }

    /**
     * 主播：退出课堂 --> 销毁课堂 --> 退出TIC
     * 观众：退出课堂 --> 退出TIC
     */
    public void quitClassroom() {
        if (LiveRoomInfoProvider.getInstance().isOfficalLive()) {
            //如果是官方大会则不退出
            return;
        }
        LogUtil.i(LiveLogTag.WhiteboardTag, "quitClassroom");
        TICManager.getInstance().quitClassroom(LiveRoomInfoProvider.getInstance().isHost(), new TICManager.TICCallback() {
            @Override
            public void onSuccess(Object data) {
                LogUtil.i(LiveLogTag.WhiteboardTag, "quitClassroom Success : ");
                if (LiveRoomInfoProvider.getInstance().isHost()) {
                    destroyClassroom();
                } else {
                    logoutTic();
                }
            }

            @Override
            public void onError(String module, int errCode, String errMsg) {
                LogUtil.i(LiveLogTag.WhiteboardTag, "loginOutSDK Fail : " + errMsg);
            }
        });
    }

    private void destroyClassroom() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "destroyClassroom");
        TICManager.getInstance().destroyClassroom(Integer.parseInt(LiveRoomInfoProvider.getInstance().liveId) + 10000000, new TICManager.TICCallback() {
            @Override
            public void onSuccess(Object data) {
                LogUtil.i(LiveLogTag.WhiteboardTag, "destroyClassroom Success");
                logoutTic();
            }

            @Override
            public void onError(String module, int errCode, String errMsg) {
                LogUtil.i(LiveLogTag.WhiteboardTag, "destroyClassroom Error");
                logoutTic();
            }
        });
    }

    private void logoutTic() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager logoutTic");

        TICManager.getInstance().logout(new TICManager.TICCallback() {
            @Override
            public void onSuccess(Object data) {
                LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager logout Success");
            }

            @Override
            public void onError(String module, int errCode, String errMsg) {
                LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager logout Fail: errMsg:" + errMsg + ",errCode:" + errCode + ",module:" + module);
            }
        });
    }

    public TEduBoardController getBoard() {
        if (mBoard == null) {
            Log.e("TEduBoardController", "TICManager: getBoard null, Do you call init?");
        }
        return mBoard;
    }

    private void initWhiteBoard() {
        LogUtil.i(LiveLogTag.WhiteboardTag,"initWhiteBoard-TEduBoardControllerInit");
                //生成一个继承于TEduBoardController.TEduBoardCallback事件监听，交给白板对象，用于处理白板事件响应。
        mBoardCallback = new TJBoardCallback((BaseLiveAct) mContext);
        mBoard.addCallback(mBoardCallback);

        //如果用户希望白板显示出来时，不使用系统默认的参数，就需要设置特性缺省参数，如是使用默认参数，则填null。
        TEduBoardController.TEduBoardAuthParam authParam = new TEduBoardController.TEduBoardAuthParam(
                LiveRoomConfig.getTXSDKAppID(),
                BaseUserInfoCache.getUserId(mContext),
                LiveRoomInfoProvider.getInstance().userSign);
        Log.d(TAG, "appid:" + LiveRoomConfig.getTXSDKAppID() + ",userId:" + BaseUserInfoCache.getUserId(mContext) + ",userSign:" + LiveRoomInfoProvider.getInstance().userSign);
        TEduBoardController.TEduBoardInitParam initParam = new TEduBoardController.TEduBoardInitParam();
        initParam.ratio = AppUtils.getWidth(mContext) + ":" + (1.78 * AppUtils.getWidth(mContext));
        initParam.dataSyncEnable = true;
        initParam.timSync = true;
        initParam.toolType = TEDU_BOARD_TOOL_TYPE_PEN;
        initParam.brushColor = new TEduBoardController.TEduBoardColor(Color.RED);
        initParam.globalBackgroundColor = new TEduBoardController.TEduBoardColor(0, 0, 0, 0);
        initParam.contentFitMode = TEduBoardController.TEduBoardContentFitMode.TEDU_BOARD_CONTENT_FIT_MODE_NONE;
        //调用初始化函数
        mBoard.init(authParam, Integer.parseInt(LiveRoomInfoProvider.getInstance().liveId) + 10000000, initParam);
    }

    public void addAnimationView(View view) {
        if (!mAnimationControllView.contains(view)) {
            mAnimationControllView.add(view);
        }
    }

    /**
     * 手绘板控制
     */
    public void showDrawPad(boolean isClick) {
        // 若没关闭文档演示的文件弹窗则关闭，因为手绘板位透明可看到后面
        dismissFileSelectPopView();
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager showDrawPad : isCLick:" + isClick);
        mBoard.gotoBoard(DRAWPAD_WB_ID);
        mDrawPadPop.show();

        new Handler().postDelayed(() -> mOperationCallback.showBottomView(false), 500);

        if (isClick) {
            LiveRoomIOHelper.setDocumentStatus(mContext, "1", DRAWPAD_WB_ID, new IOLiveRoomListener() {
                @Override
                public void onIOError(String error, int errorCode) {

                }

                @Override
                public void onIOSuccess() {

                }
            });
        }
    }

    public void disDrawPad(boolean isClick) {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager disDrawPad : isCLick:" + isClick);
        mDrawPadPop.dismiss(isClick);
        mOperationCallback.showBottomView(true);
        if (isClick) {
            LiveRoomIOHelper.setDocumentStatus(mContext, "0", DRAWPAD_WB_ID, new IOLiveRoomListener() {
                @Override
                public void onIOError(String error, int errorCode) {

                }

                @Override
                public void onIOSuccess() {

                }
            });
        }
    }

    /**
     * 用户身份变化刷新绘画板权限：观众 -> 助手 ；助手 -> 观众
     */
    public void refreshDrawpad() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager refreshDrawpad");
        // 刷新手绘板
        mDrawPadPop.refreshOnStatusChange();
        // 若身份由助手变为观众且当前正在显示文件夹dialog则消失
        if (LiveRoomInfoProvider.getInstance().isGuest()) {
            if (mFileSelectPop != null) {
                mFileSelectPop.dismiss();
            }
        }
    }

    /**
     * 文档演示控制
     */
    public void showFileSelectPop() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager showFileSelectPop");
        if (mFileSelectPop == null) {
            LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager showFileSelectPop: mFileSelectPop == null");
            mFileSelectPop = new FileSelectPop(mContext);
            mFileSelectPop.setmOnSelectFileFolder(this);
        }
        mOperationCallback.showBottomView(false);
        mFileSelectPop.show();
        mFileSelectPop.showVisibility();
    }

    @Override
    public void reSelectFile() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager reSelectFile");
        // 退出白板的时候要显示文件列表
        LiveRoomInfoProvider.getInstance().isShowFile = false;
        if (mFileSelectPop == null) {
            LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager reSelectFile: mFileSelectPop == null");
            mFileSelectPop = new FileSelectPop(mContext);
            mFileSelectPop.setmOnSelectFileFolder(this);
            showFileSelectPop();
        } else {
            LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager reSelectFile: isMyselfOpenDocument：" + LiveRoomInfoProvider.getInstance().isMyselfOpenDocument);
            // 判断是否是发起人，是的话展示文件列表，不是的话弹出弹窗
            if (LiveRoomInfoProvider.getInstance().isMyselfOpenDocument) {
                mFileSelectPop.showVisibility();
                LiveRoomInfoProvider.getInstance().isMyselfOpenDocument = false;
            } else {
                showFileSelectPop();
            }
        }
        showParentGraientView();
//        mOperationCallback.controlApplyViewVisile(false);
        mFileContainer.setVisibility(View.GONE);
        mFileContainer.removeAllViews();

        /**
         * 退出时若公告应显示则显示公告
         */
        if (mOperationCallback != null) {
            mOperationCallback.setNoticeViewShowStatus(View.VISIBLE);
        }
        mFileShowView.isShowBoardCLient = false;
        // 以防万一，释放播放器资源
        releaseVideoPlayer();
    }

    @SuppressLint("ClickableViewAccessibility")
    public void showFileView() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager showFileView");
        mFileContainer.setVisibility(View.VISIBLE);
        mFileContainer.removeAllViews();
        mFileContainer.addView(mFileShowView.getView());

        /**
         * 进入时若公告应显示则隐藏公告
         */
        if (mOperationCallback != null) {
            mOperationCallback.setNoticeViewShowStatus(View.GONE);
        }
    }

    public void showClientFileView(String boardId) {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager showClientFileView: boardId " + boardId + ",isShowBoardCLient : " + mFileShowView.isShowBoardCLient);
        mFileShowView.isInControl = false;
        LiveRoomInfoProvider.getInstance().isShowFile = true;
        LiveRoomInfoProvider.getInstance().isMyselfOpenDocument = false;
        if (!mFileShowView.isShowBoardCLient) {
            showFileView();
            mFileShowView.mFileListRecycler.setVisibility(View.GONE);
            mOperationCallback.showBottomView(LiveRoomInfoProvider.getInstance().isGuest());
        }
        mFileShowView.clientShowBoard(boardId);
    }

    public void disShowFile() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager disShowFile: isMyselfOpenDocument: " + LiveRoomInfoProvider.getInstance().isMyselfOpenDocument);
        mFileShowView.closeBoard();
        if (LiveRoomInfoProvider.getInstance().isMyselfOpenDocument) {
            reSelectFile();
        } else {
            mFileContainer.setVisibility(View.GONE);
            mFileContainer.removeAllViews();
            mOperationCallback.showBottomView(true);
            /**
             * 退出时若公告应显示则显示公告
             */
            if (mOperationCallback != null) {
                mOperationCallback.setNoticeViewShowStatus(View.VISIBLE);
            }
        }
        clearAnimation();
    }

    public void showClientVideoView(DoumentVideoSendMsg msg, boolean isInitJump) {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager showClientVideoView: isInitJump : " + isInitJump);
        // 根据msg.status状态判断当前播放状态
        mFileShowView.clientShowVideo(msg, isInitJump);
        mFileShowView.isInControl = false;
        mFileShowView.mFileListRecycler.setVisibility(View.GONE);
        showFileView();
        mOperationCallback.showBottomView(LiveRoomInfoProvider.getInstance().isGuest());
    }

    public void disShowVideo() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager disShowVideo: isMyselfOpenDocument:" + LiveRoomInfoProvider.getInstance().isMyselfOpenDocument);
        LiveRoomInfoProvider.getInstance().isShowFile = true;
        LiveRoomInfoProvider.getInstance().isMyselfOpenDocument = false;
        mFileShowView.closeVideo();
        if (LiveRoomInfoProvider.getInstance().isMyselfOpenDocument) {
            reSelectFile();
        } else {
            if (!mFileShowView.isShowBoardCLient) {
                mFileContainer.setVisibility(View.GONE);
                mFileContainer.removeAllViews();
                /**
                 * 退出时若公告应显示则显示公告
                 */
                if (mOperationCallback != null) {
                    mOperationCallback.setNoticeViewShowStatus(View.VISIBLE);
                }
            }
            boolean i = mFileShowView.isShowWhiteBoardIng();
            if (!mFileShowView.isShowWhiteBoardIng()) {
                // 若正在展示白板则不显示下方按钮，否则显示
                mOperationCallback.showBottomView(true);
            }
        }
        clearAnimation();
    }

    public void releaseVideoPlayer() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager releaseVideoPlayer ");
        LiveRoomInfoProvider.getInstance().isShowFile = false;
        if (mFileShowView != null) {
            mFileShowView.releaseVideoPlayer();
        }
    }

    public void exitFileShow(FileShowCallback callback) {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager exitFileShow ");
        if (mFileShowView != null) {
            mFileShowView.exitFileShow(callback);
        }
    }

    /**
     * 离开房间调用
     */
    public void finishLive() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager finishLive ");
        if (mDrawPadPop != null && mDrawPadPop.isShowing()) {
            mDrawPadPop.dismissImmediately(false);
        }
    }

    /**
     * 刷新更多按钮中的未读消息数量
     *
     * @param addCount
     */
    public void refreshUnreadInfo(int addCount) {
        if (mFileShowView != null) {
            mFileShowView.refreshUnreadInfo(addCount);
        }
    }

    @Override
    public void onSelectFile(ArrayList<FileModel> currentFile, int fileIndex, FileFolderModel selectFolder) {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager onSelectFile ");
        if (!LiveRoomInfoProvider.getInstance().isInitWhiteBoardSuccess && !"2".equals(currentFile.get(fileIndex).type)) {
            // 初始化失败,并且选中的不是视频（需展示白板）
            Toasty.normal(mContext, "初始化失败").show();
            mOperationCallback.showBottomView(true);
            return;
        }
        mFileShowView.setCurrentSelectInfo(currentFile, fileIndex, selectFolder);
        mFileShowView.isInControl = true;
        if (!"2".equals(currentFile.get(fileIndex).type)) {
            mFileShowView.mFileListRecycler.setVisibility(View.VISIBLE);
        }
        showFileView();
        mOperationCallback.showBottomView(false);
//        mOperationCallback.controlApplyViewVisile(true);

    }

    @Override
    public void onDismissSeletePop() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager onDismissSeletePop ");
        if (mFileContainer.getVisibility() == View.VISIBLE) {
            mOperationCallback.showBottomView(false);
        } else {
            mOperationCallback.showBottomView(true);
        }
    }


    @Override
    public void onSingleTouch() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager onSingleTouch ");
//        if (!mFileShowView.isShowBoardCLient) {
//            // 防止异常，若当前白板已不显示了，不触发touch事件
//            return;
//        }
//        if (mAnimationControllView.size() > 0) {
//            if (hasHidden) {
//                showParentGraientView();
//            } else {
//                hideParentGraientView();
//            }
//        }
    }

    public void showParentGraientView() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager showParentGraientView ");
        for (View view : mAnimationControllView) {
            view.setVisibility(View.VISIBLE);
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setDuration(200);
            alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    hasHidden = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            view.startAnimation(alphaAnimation);
        }
    }

    private void clearAnimation() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager clearAnimation ");
        for (View view : mAnimationControllView) {
            view.clearAnimation();
        }
    }

    private void hideParentGraientView() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager hideParentGraientView ");
        for (View view : mAnimationControllView) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setDuration(200);
            alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    hasHidden = true;
                    view.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            view.startAnimation(alphaAnimation);
        }
    }

    public boolean onBackPressed() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager onBackPressed ");
        if (mFileContainer.getVisibility() == View.VISIBLE) {
            if (LiveRoomInfoProvider.getInstance().isShowFile) {
                mFileShowView.showGraientView();
                onSingleTouch();
                mFileShowView.onBack();
            }
            return true;
        } else if (mDrawPadPop.isShowing()) {
            disDrawPad(true);
            return true;
        }
        return false;
    }


    public void dismissAllPopview() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "WBManager dismissAllPopview");

        dismissFileSelectPopView();

        if (mDrawPadPop != null) {
            mDrawPadPop.dismiss(false);
        }
    }

    public void dismissFileSelectPopView() {
        if (mFileSelectPop != null && mFileSelectPop.isShowing()) {
            mFileSelectPop.dismiss();
        }
    }

    //Board Callback
    private class TJBoardCallback implements TEduBoardController.TEduBoardCallback {
        WeakReference<BaseLiveAct> mActivityRef;

        TJBoardCallback(BaseLiveAct activityEx) {
            mActivityRef = new WeakReference<>(activityEx);
        }

        @Override
        public void onTEBError(int code, String msg) {
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBError: code:" + code + ";msg:" + msg);
            onJoinClassRoomFail(4);
            // 错误回调函数
            if (code == TEDU_BOARD_ERROR_INIT) {
                // 1: 初始化失败（Android 端不会发生）
            } else if (code == TEDU_BOARD_ERROR_AUTH) {
                // 2: 服务鉴权失败，请先购买服务
            } else if (code == TEDU_BOARD_ERROR_LOAD) {
                // 3: 白板加载失败
            } else if (code == TEDU_BOARD_ERROR_TIM_INVALID) {
                // 5: IMSDK 不可用
            }
        }

        @Override
        public void onTEBWarning(int code, String msg) {
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBWarning: code:" + code + ";msg:" + msg);
            // 警告回调函数
            if (code == TEDU_BOARD_WARNING_SYNC_DATA_PARSE_FAILED) {
                // 1: 接收到其他端数据后，解析错误
            } else if (code == TEDU_BOARD_WARNING_TIM_SEND_MSG_FAILED) {
                // 2: IMSDK 发送消息失败
            } else if (code == TEDU_BOARD_WARNING_H5PPT_ALREADY_EXISTS) {
                // 3: 添加的 H5PPT 已存在时抛出该警告
            }
        }

        @Override
        public void onTEBInit() {
            // 白板初始化完成回调函数（收到该回调后，表示白板已处于可正常工作状态。）
            BaseLiveAct activity = mActivityRef.get();
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBInit");
            onJoinClassRoomSuccess();
        }

        @Override
        public void onTEBHistroyDataSyncCompleted() {
            // 白板历史数据同步完成回调函数
            BaseLiveAct activityEx = mActivityRef.get();
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBHistroyDataSyncCompleted");
        }

        @Override
        public void onTEBSyncData(String data) {
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBSyncData: data:" + data);
            // 白板同步数据回调函数
            // 收到该回调时需要将回调数据通过信令通道发送给房间内其他人，接受者收到后调用 AddSyncData 接口将数据添加到白板以实现数据同步。
            // 该回调用于多个白板间的数据同步，使用内置 IM 作为信令通道时，不会收到该回调。


        }

        @Override
        public void onTEBImageStatusChanged(String boardId, String url, int status) {
            // 白板图片状态改变回调函数（status新的白板图片状态），可以用于切换白板和文档时判断状态
            TXLog.i(TAG, "onTEBImageStatusChanged:" + status);
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBImageStatusChanged: status:" + status + ";boardId: " + boardId + ";url: " + url);
            if (status == TEDU_BOARD_IMAGE_STATUS_LOADING) {
                // 1: 正在加载中
            } else if (status == TEDU_BOARD_IMAGE_STATUS_LOAD_DONE) {
                // 2: 加载完成
            } else if (status == TEDU_BOARD_IMAGE_STATUS_LOAD_ABORT) {
                // 3: 图片加载中断
            } else if (status == TEDU_BOARD_IMAGE_STATUS_LOAD_ERROR) {
                // 4: 加载错误
            }

        }

        @Override
        public void onTEBAddBoard(List<String> boardId, final String fileId) {
            // 增加白板页回调函数（boardId：增加的白板页 ID 列表；fileId：增加的白板页所属的文件 ID）
            TXLog.i(TAG, "onTEBAddBoard:" + fileId);
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBAddBoard: fileId:" + fileId);
        }

        @Override
        public void onTEBDeleteBoard(List<String> boardId, final String fileId) {
            // 删除白板页回调函数
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBDeleteBoard: fileId:" + fileId);
        }

        @Override
        public void onTEBGotoBoard(String boardId, final String fileId) {
            // 跳转白板页回调函数
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBGotoBoard: fileId:" + fileId + ";boardId: " + boardId);
        }

        @Override
        public void onTEBGotoStep(int i, int i1) {

        }

        @Override
        public void onTEBRectSelected() {

        }

        @Override
        public void onTEBRefresh() {

        }

        @Override
        public void onTEBDeleteFile(String fileId) {
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBDeleteFile: fileId:" + fileId);
            // 删除文件回调函数
        }

        @Override
        public void onTEBSwitchFile(String fileId) {
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBSwitchFile: fileId:" + fileId);
            // 切换文件回调函数
        }

        @Override
        public void onTEBAddTranscodeFile(String s) {
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBAddTranscodeFile: msg:" + s);
            if (LiveRoomInfoProvider.getInstance().isShowFile && LiveRoomInfoProvider.getInstance().isMyselfOpenDocument) {
                mFileShowView.addFileSuccess(s);
            }
        }


        @Override
        public void onTEBUndoStatusChanged(boolean canUndo) {
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBUndoStatusChanged: canUndo:" + canUndo);
            // 白板可撤销状态改变回调函数（白板当前是否还能执行 Undo 操作）
            BaseLiveAct activityEx = mActivityRef.get();

        }

        @Override
        public void onTEBRedoStatusChanged(boolean canredo) {
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBRedoStatusChanged: canredo:" + canredo);
            // 白板可重做状态改变回调函数（白板当前是否还能执行 Redo 操作）
            BaseLiveAct activityEx = mActivityRef.get();

        }

        @Override
        public void onTEBFileUploadProgress(final String fid, int currentBytes, int totalBytes, int uploadSpeed, float percent) {
            // 文件上传进度回调函数
            TXLog.i(TAG, "onTEBFileUploadProgress:" + fid + " percent:" + percent);
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBFileUploadProgress: fid:" + fid + ";percent: " + percent);
            // 文件上传进度
        }

        @Override
        public void onTEBFileUploadStatus(final String fid, int status, int code, String statusMsg) {
            TXLog.i(TAG, "onTEBFileUploadStatus:" + fid + " status:" + status);
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBFileUploadStatus: fid:" + fid + ";status: " + status);
            // 文件上传状态
            if (status == TEDU_BOARD_UPLOAD_STATUS_SUCCEED) {
                // 1: 上传成功

            } else if (status == TEDU_BOARD_UPLOAD_STATUS_FAILED) {
                // 2: 上传失败

            }
        }

        @Override
        public void onTEBFileTranscodeProgress(String s, String s1, String s2, TEduBoardController.TEduBoardTranscodeFileResult tEduBoardTranscodeFileResult) {

        }

        @Override
        public void onTEBH5FileStatusChanged(String s, int i) {

        }

        @Override
        public void onTEBAddImagesFile(String s) {

        }

        @Override
        public void onTEBVideoStatusChanged(String s, int i, float v, float v1) {

        }

        @Override
        public void onTEBSnapshot(String s, int i, String s1) {

        }

        @Override
        public void onTEBSetBackgroundImage(final String url) {
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBSetBackgroundImage: url:" + url);
            // 设置白板背景图片回调函数（调用 SetBackgroundImage 时传入的 URL）
            // 只有本地调用 SetBackgroundImage 时会收到该回调, 收到该回调表示背景图片已经上传或下载成功，并且显示出来。
        }

        @Override
        public void onTEBAddImageElement(String s) {

        }

        @Override
        public void onTEBBackgroundH5StatusChanged(String boardId, String url, int status) {
            TXLog.i(TAG, "onTEBBackgroundH5StatusChanged:" + boardId + " url:" + boardId + " status:" + status);
            LogUtil.i(LiveLogTag.WhiteboardTag, "onTEBBackgroundH5StatusChanged: boardId:" + boardId + ";status: " + status + ";url: " + url);
            // 设置白板背景 H5 状态改变回调函数
            if (status == TEDU_BOARD_BACKGROUND_H5_STATUS_LOADING) {
                // 1: H5 背景正在加载

            } else if (status == TEDU_BOARD_BACKGROUND_H5_STATUS_LOAD_FINISH) {
                // 2: H5 背景加载完成

            }
        }
    }
}

