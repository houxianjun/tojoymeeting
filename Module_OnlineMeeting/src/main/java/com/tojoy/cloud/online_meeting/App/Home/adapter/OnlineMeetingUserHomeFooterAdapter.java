package com.tojoy.cloud.online_meeting.App.Home.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.ui.vlayout.DelegateAdapter;
import com.tojoy.tjoybaselib.ui.vlayout.LayoutHelper;
import com.tojoy.tjoybaselib.ui.vlayout.layout.SingleLayoutHelper;
import com.tojoy.cloud.online_meeting.R;

import static android.view.View.GONE;

/**
 * @author fanxi
 * @date 2019/7/16.
 * description：
 */
public class OnlineMeetingUserHomeFooterAdapter extends DelegateAdapter.Adapter<OnlineMeetingUserHomeFooterAdapter.OnlineMeetingHomeFooterHolder> {

    private Context mContext;
    private LayoutHelper mLayoutHelper;
    private OnlineMeetingHomeFooterHolder mViewHolder;
    private int mViewTypeItem;

    public OnlineMeetingUserHomeFooterAdapter(Context mContext, int mViewTypeItem) {
        this.mContext = mContext;
        this.mViewTypeItem = mViewTypeItem;
    }


    @Override
    public LayoutHelper onCreateLayoutHelper() {
        if (mLayoutHelper == null) {
            mLayoutHelper = new SingleLayoutHelper();
        }
        return mLayoutHelper;
    }

    @NonNull
    @Override
    public OnlineMeetingHomeFooterHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new OnlineMeetingHomeFooterHolder(LayoutInflater.from(mContext).inflate(R.layout.item_online_meeting_home_footer, viewGroup, false));

    }

    @Override
    public void onBindViewHolder(@NonNull OnlineMeetingHomeFooterHolder onlineMeetingHomeFooterHolder, int i) {
        mViewHolder = onlineMeetingHomeFooterHolder;
        setNoMore();

    }

    private void setNoMore() {
        mViewHolder.mRlvLoadMoreView.setVisibility(View.VISIBLE);
        mViewHolder.mTextView.setText("~ 全部加载完毕 ~");
        mViewHolder.loadingView.setVisibility(GONE);
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public int getmViewTypeItem() {
        return mViewTypeItem;
    }

    static class OnlineMeetingHomeFooterHolder extends RecyclerView.ViewHolder {

        ProgressBar loadingView;
        TextView mTextView;
        public Context mContext;
        RelativeLayout mRlvLoadMoreView;

        OnlineMeetingHomeFooterHolder(View itemView) {
            super(itemView);
            mRlvLoadMoreView = itemView.findViewById(R.id.rvl_load_more);
            loadingView = itemView.findViewById(R.id.loading);
            mTextView = itemView.findViewById(R.id.loadmore_text);
        }
    }
}
