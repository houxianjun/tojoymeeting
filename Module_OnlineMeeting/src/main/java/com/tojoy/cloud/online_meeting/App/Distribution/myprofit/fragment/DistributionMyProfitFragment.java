package com.tojoy.cloud.online_meeting.App.Distribution.myprofit.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.home.MineDistributionSignStatusResponse;
import com.tojoy.tjoybaselib.model.home.MineProfitInfoDistributionResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.ui.TJTablayout.SlidingTabLayout;
import com.tojoy.tjoybaselib.ui.scrollableHelper.ScrollableLayout;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.StatusbarUtils;
import com.tojoy.cloud.online_meeting.App.Distribution.myprofit.activity.DistributionMyProfitAct;
import com.tojoy.cloud.online_meeting.App.Distribution.myprofit.adapter.DistributionProfitPagerAdapter;
import com.tojoy.cloud.online_meeting.App.Distribution.view.CommonProfitView;
import com.tojoy.cloud.online_meeting.R;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import rx.Observer;

/**
 * 我的收益页面
 */
public class DistributionMyProfitFragment extends Fragment {
    private DistributionProfitPagerAdapter adapter;
    private ArrayList<DistributionSettlementFragment> mFragmentTab;
    private DistributionSettlementFragment alreadySettlementFragment;
    private DistributionSettlementFragment waitSettlementFragment;
    private ScrollableLayout mScrollLayout;
    private SlidingTabLayout mTabLayout;
    private ViewPager mViewPager;
    private String[] tableTitle = new String[]{"会议", "商品"};
    //可提现金额
    private TextView tvProfit;
    //最低提现金额
    private TextView tvLimitWithdrawal;
    //今日成交
    private CommonProfitView viewTodayincome;
    //本月成交
    private CommonProfitView viewMonthincome;
    //累计收益
    private CommonProfitView viewTotalincome;
    //待结算收益
    private CommonProfitView viewPreincome;

    private PtrClassicFrameLayout mPtrFrame;

    //标题栏透明度
    float alpha;

    MineProfitInfoDistributionResponse.MineProfitDistributionModel myProfitModel;

    public DistributionMyProfitFragment() {

    }

    public static DistributionMyProfitFragment newInstance(DistributionMyProfitAct activity) {

        return new DistributionMyProfitFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_distribution_myprofit, container, false);
        initUI(layout);
        initFragmentPager(mScrollLayout);
        setupPullToRefresh(layout);

        return layout;
    }

    public void initFragmentPager(final ScrollableLayout mScrollLayout) {
        initFragmentTab();
        adapter = new DistributionProfitPagerAdapter(getFragmentManager(), getActivity(), mFragmentTab);
        mViewPager.setAdapter(adapter);
        mScrollLayout.getHelper().setCurrentScrollableContainer(adapter.getCurrentFragmentTab(0));
        mScrollLayout.setOnScrollListener((currentY, maxY) -> {
            //显示标题栏
            if (currentY <= 100) {
                //完全不透明
                alpha = 1;
                StatusbarUtils.enableTranslucentStatusbar(getActivity());
                StatusbarUtils.setStatusBarLightMode(getActivity(), StatusbarUtils.statusbarlightmode(getActivity()));
            } else {
                if (currentY * 1.0f / (maxY * 1.0f) > 0.2) {
                    //完全透明
                    alpha = 0;
                    StatusbarUtils.clossTranslucentStatusbar(getActivity());
                } else {
                    //产生渐变效果
                    alpha = currentY * 1.0f / (maxY * 1.0f);
                    //StatusBarUtil.setStatusBarDarkTheme(getActivity(),true);
                }
            }
            ((DistributionMyProfitAct) getActivity()).setTitleAlpha(alpha);

        });
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                /** 标注当前页面 **/
                mScrollLayout.getHelper().setCurrentScrollableContainer(adapter.getCurrentFragmentTab(position));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mTabLayout.setViewPager(mViewPager, tableTitle);
        mViewPager.setCurrentItem(0);
    }

    private void initFragmentTab() {
        alreadySettlementFragment = new DistributionSettlementFragment("1");
        waitSettlementFragment = new DistributionSettlementFragment("0");
        mFragmentTab = new ArrayList<>();
        mFragmentTab.add(alreadySettlementFragment);
        mFragmentTab.add(waitSettlementFragment);

    }

    private void setupPullToRefresh(View layout) {
        mPtrFrame = layout.findViewById(R.id.rotate_header_web_view_frame);
        mPtrFrame.setLastUpdateTimeRelateObject(this);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return mScrollLayout.canPtr();
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                mPtrFrame.postDelayed(() -> {
                    mPtrFrame.refreshComplete();
                    initData();
                    alreadySettlementFragment.pageNum = 1;
                    alreadySettlementFragment.initData();
                    waitSettlementFragment.pageNum = 1;
                    waitSettlementFragment.initData();
                    mViewPager.setCurrentItem(0);

                }, 100);
            }
        });

        // the following are default settings
        mPtrFrame.setResistance(1.7f);
        mPtrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        mPtrFrame.setDurationToClose(200);
        mPtrFrame.setDurationToCloseHeader(1000);
        // default is false
        mPtrFrame.setPullToRefresh(false);
        // default is true
        mPtrFrame.setKeepHeaderWhenRefresh(true);
    }


    private void initUI(View layout) {
        mTabLayout = layout.findViewById(R.id.tabLayot);
        mViewPager = layout.findViewById(R.id.viewPager);
        mTabLayout.setTabSpaceEqual(true);
        mTabLayout.setTextBold(2);
        mTabLayout.setIndicatorMargin(0, 0, 0, 8);
        mViewPager.setOffscreenPageLimit(2);
        mScrollLayout = layout.findViewById(R.id.scrollableLayout);
        tvProfit = layout.findViewById(R.id.tv_profit);
        tvLimitWithdrawal = layout.findViewById(R.id.tv_limit_withdrawal);
        //提现按钮
        TextView tvProfitWithdrawal = layout.findViewById(R.id.tv_profit_withdrawal);
        viewTodayincome = layout.findViewById(R.id.view_todayIncome);
        viewMonthincome = layout.findViewById(R.id.view_monthIncome);
        viewTotalincome = layout.findViewById(R.id.view_totalIncome);
        viewPreincome = layout.findViewById(R.id.view_preIncome);
        /**
         * 提现按钮
         */
        tvProfitWithdrawal.setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                querytDistributionWithdrawApply();
            }
        });
    }


    public void initData() {
        OMAppApiProvider.getInstance().queryDistributionMyProfit(new Observer<MineProfitInfoDistributionResponse>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(getActivity(), "网络错误，请检查网络").show();

            }

            @Override
            public void onNext(MineProfitInfoDistributionResponse response) {
                if (response.isSuccess()) {
                    myProfitModel = response.data;
                    updateProfitData(myProfitModel);
                } else {
                    Toasty.normal(getActivity(), response.msg).show();
                }
            }
        });
    }

    public void updateProfitData(MineProfitInfoDistributionResponse.MineProfitDistributionModel model) {
        tvProfit.setText(model.cashIncome);
        String content = String.format(getActivity().getResources().getString(R.string.distribution_limit_withdrawal), model.minMoney + "");
        tvLimitWithdrawal.setText(content);

        viewTodayincome.initData(model.todayIncome, "今日成交(元)");
        viewMonthincome.initData(model.monthIncome, "本月收益(元)");
        viewTotalincome.initData(model.totalIncome, "累计收益(元)");
        viewPreincome.initData(model.preIncome, "待结算收益(元)");

    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public void querytDistributionWithdrawApply() {
        if (myProfitModel != null) {
            if (Double.parseDouble(myProfitModel.minMoney) > Double.parseDouble(myProfitModel.cashIncome)) {
                Toasty.normal(getActivity(), "您未达到最低提现额度，暂时不能提现！").show();
                return;
            }
           querytDistributionWithdrawSignStatus();

        }
    }

    /**
     * 获取签约状态，已签约跳转至提现页面。
     * 未签约跳转至信息录入页面
     */
    public void querytDistributionWithdrawSignStatus(){
        OMAppApiProvider.getInstance().querytDistributionWithdrawSignStatus(new Observer<MineDistributionSignStatusResponse>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(getActivity(), "网络错误，请检查网络").show();

            }

            @Override
            public void onNext(MineDistributionSignStatusResponse response) {
                if (response.isSuccess()) {
                    switch (response.data.status){
                        case 0:
                            ARouter.getInstance().build(RouterPathProvider.Distribution_Information_Entry_Act)
                                    .withString("from_page","myprofit")
                                    .navigation();
                            break;
                        case 2:
                            ARouter.getInstance().build(RouterPathProvider.Distribution_MyWithdrawal_Act)
                                    .navigation();
                            break;
                        case 1:
                            break;
                        case 3:
                            break;
                    }
                } else {
                    Toasty.normal(getActivity(), response.msg).show();
                }
            }
        });

    }

}
