package com.tojoy.cloud.online_meeting.App.Live.Whiteboard;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.netease.nim.uikit.common.util.log.LogUtil;
//import com.tencent.boardsdk.board.WhiteboardManager;
//import com.tencent.boardsdk.board.WhiteboardView;
import com.tencent.teduboard.TEduBoardController;
import com.tojoy.tjoybaselib.ui.dialog.PickerViewAnimateUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;
import com.tojoy.cloud.online_meeting.R;

import static com.tencent.teduboard.TEduBoardController.TEduBoardToolType.TEDU_BOARD_TOOL_TYPE_PEN;
import static com.tencent.teduboard.TEduBoardController.TEduBoardToolType.TEDU_BOARD_TOOL_TYPE_ZOOM_DRAG;

/**
 * 手绘板
 */
public class DrawPadPop {
    private Context context;
    protected ViewGroup contentContainer;
    private Animation outAnim;
    private Animation inAnim;
    private boolean isAnim = true;
    private boolean isShowing;
    private int gravity = Gravity.BOTTOM;
    public ViewGroup decorView;
    private ViewGroup rootView;
    private FrameLayout mWhiteboardView;
    private OperationCallback operationCallback;
    private int selectPaintColor = Color.WHITE;
    // 是否处于放大状态，默认是处于绘制状态
    private boolean isEnlargeStatus;

    private boolean isDrawPadPopIniting = true;


    public DrawPadPop(Context context, OperationCallback operationCallback) {
        this.context = context;
        init();
        initViews();
        this.operationCallback = operationCallback;
    }

    private void initViews() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        //decorView是activity的根View
        if (decorView == null) {
            decorView = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        }

        //将控件添加到decorView中
        rootView = (ViewGroup) layoutInflater.inflate(R.layout.draw_pad_popview, decorView, false);
        rootView.setLayoutParams(new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        ));

        contentContainer = rootView.findViewById(R.id.content_container);
        rootView.findViewById(R.id.iv_close).setOnClickListener(v -> dismiss(true));

        initBallView();
        //2、获取View，并将其加入到View视频中。
        mWhiteboardView = rootView.findViewById(R.id.board_view_container);
        refreshOnStatusChange();

    }

    private void initBallView() {
        //设置Ball的大小
        int ballWidth = (AppUtils.getWidth(context) - AppUtils.dip2px(context, 100)) / 7;
        LinearLayout.LayoutParams ballLps = new LinearLayout.LayoutParams(ballWidth, ballWidth);
        ballLps.setMargins(0, 0, AppUtils.dip2px(context, 10), 0);
        rootView.findViewById(R.id.rlv_ball_1).setLayoutParams(ballLps);
        rootView.findViewById(R.id.rlv_ball_2).setLayoutParams(ballLps);
        rootView.findViewById(R.id.rlv_ball_3).setLayoutParams(ballLps);
        rootView.findViewById(R.id.rlv_ball_4).setLayoutParams(ballLps);
        rootView.findViewById(R.id.rlv_ball_5).setLayoutParams(ballLps);
        rootView.findViewById(R.id.rlv_ball_6).setLayoutParams(ballLps);
        rootView.findViewById(R.id.rlv_ball_7).setLayoutParams(ballLps);

        rootView.findViewById(R.id.rlv_ball_1).setOnClickListener(onClickListener);
        rootView.findViewById(R.id.rlv_ball_2).setOnClickListener(onClickListener);
        rootView.findViewById(R.id.rlv_ball_3).setOnClickListener(onClickListener);
        rootView.findViewById(R.id.rlv_ball_4).setOnClickListener(onClickListener);
        rootView.findViewById(R.id.rlv_ball_5).setOnClickListener(onClickListener);
        rootView.findViewById(R.id.rlv_ball_6).setOnClickListener(onClickListener);
        rootView.findViewById(R.id.rlv_ball_7).setOnClickListener(onClickListener);
        rootView.findViewById(R.id.iv_erase).setOnClickListener(v ->{
            ((BaseLiveAct)context).getWBManager().getBoard().clear(false);
            setPaintStatusWriting();
        });

        rootView.findViewById(R.id.iv_enlarge).setOnClickListener(v -> {
            LogUtil.i(LiveLogTag.WhiteboardTag, "DrawPadPop isEnlargeStatus:" + isEnlargeStatus);
            // 点击放大镜
            if (isEnlargeStatus){
                ((BaseLiveAct)context).getWBManager().getBoard().setToolType(TEDU_BOARD_TOOL_TYPE_PEN);
                // 设置放大镜为未选中状态
                ((ImageView)rootView.findViewById(R.id.iv_enlarge)).setImageResource(R.drawable.icon_draw_enlarge);
            } else {
                ((BaseLiveAct)context).getWBManager().getBoard().setToolType(TEDU_BOARD_TOOL_TYPE_ZOOM_DRAG);
                // 设置放大镜为选中状态
                ((ImageView)rootView.findViewById(R.id.iv_enlarge)).setImageResource(R.drawable.icon_draw_enlarge_check);
            }
            isEnlargeStatus = !isEnlargeStatus;
        });

        rootView.findViewById(R.id.rlv_ball_1).performClick();
        LogUtil.i(LiveLogTag.WhiteboardTag, "DrawPadPop init");
    }

    /**
     * 每次点击橡皮擦或切换画笔都设置状态为绘画状态
     */
    private void setPaintStatusWriting(){
        if (isDrawPadPopIniting) {
            return;
        }
        ((BaseLiveAct)context).getWBManager().getBoard().setToolType(TEDU_BOARD_TOOL_TYPE_PEN);
        // 设置放大镜为未选中状态
        ((ImageView)rootView.findViewById(R.id.iv_enlarge)).setImageResource(R.drawable.icon_draw_enlarge);
        isEnlargeStatus = false;
    }

    private View.OnClickListener onClickListener = v -> {
        rootView.findViewById(R.id.rlv_ball_1).setBackgroundResource(0);
        rootView.findViewById(R.id.rlv_ball_2).setBackgroundResource(0);
        rootView.findViewById(R.id.rlv_ball_3).setBackgroundResource(0);
        rootView.findViewById(R.id.rlv_ball_4).setBackgroundResource(0);
        rootView.findViewById(R.id.rlv_ball_5).setBackgroundResource(0);
        rootView.findViewById(R.id.rlv_ball_6).setBackgroundResource(0);
        rootView.findViewById(R.id.rlv_ball_7).setBackgroundResource(0);
        v.setBackgroundResource(R.drawable.icon_ball_white);

        switch (Integer.parseInt(v.getTag().toString())) {
            case 1:
                selectPaintColor = Color.WHITE;
                setBrushColor();
                setPaintStatusWriting();
                break;
            case 2:
                selectPaintColor = Color.parseColor("#fffc3f36");
                setBrushColor();
                setPaintStatusWriting();
                break;
            case 3:
                selectPaintColor = Color.parseColor("#ffffc520");
                setBrushColor();
                setPaintStatusWriting();
                break;
            case 4:
                selectPaintColor = Color.parseColor("#ff5ad548");
                setBrushColor();
                setPaintStatusWriting();
                break;
            case 5:
                selectPaintColor = Color.parseColor("#ff3bc1fc");
                setBrushColor();
                setPaintStatusWriting();
                break;
            case 6:
                selectPaintColor = Color.parseColor("#ff1565f8");
                setBrushColor();
                setPaintStatusWriting();
                break;
            case 7:
                selectPaintColor = Color.parseColor("#fff11c6b");
                setBrushColor();
                setPaintStatusWriting();
                break;
            default:
                break;
        }
    };

    private void setBrushColor(){
        if (((BaseLiveAct)context).getWBManager() != null && ((BaseLiveAct)context).getWBManager().getBoard() != null){
            ((BaseLiveAct)context).getWBManager().getBoard().setBrushColor(new TEduBoardController.TEduBoardColor(selectPaintColor));
        }
    }


    @SuppressLint("ResourceType")
    public void initWhiteboard() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "DrawPadPop initWhiteboard");
        if(((BaseLiveAct)context).getWBManager() == null || ((BaseLiveAct)context).getWBManager().getBoard() == null ){
            return;
        }
        // 设置工具为画笔
        ((BaseLiveAct)context).getWBManager().getBoard().setToolType(TEDU_BOARD_TOOL_TYPE_PEN);

        // 设置可以涂鸦（如果不调用此函数，白板内部默认可以进行涂押）
        ((BaseLiveAct)context).getWBManager().getBoard().setDrawEnable(!LiveRoomInfoProvider.getInstance().isGuest());
        // 设置是否数据同步
        // 设置宽高比(字符串形式：16：9)
        int hTatio = AppUtils.getScreenHeight(context) / 4 * 3 - AppUtils.dip2px(context,43);
        ((BaseLiveAct)context).getWBManager().getBoard().setBoardRatio(AppUtils.getWidth(context) + ":" + hTatio);
        ((BaseLiveAct)context).getWBManager().getBoard().setDataSyncEnable(!LiveRoomInfoProvider.getInstance().isGuest());
    }

    protected void init() {
        inAnim = getInAnimation();
        outAnim = getOutAnimation();
    }

    /**
     * 刷新手绘板的状态（若此时身份被设置为助手，则助手也可绘制和显示触发各功能按钮）
     */
    public void refreshOnStatusChange() {
        LogUtil.i(LiveLogTag.WhiteboardTag, "DrawPadPop refreshOnStatusChange");
        int height = AppUtils.getScreenHeight(context) / 4 * 3;
        if (!LiveRoomInfoProvider.getInstance().isGuest()) {
            rootView.findViewById(R.id.rlv_top_layout).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.llv_color_selector).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.iv_erase).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.iv_enlarge).setVisibility(View.VISIBLE);
        } else {
            rootView.findViewById(R.id.rlv_top_layout).setVisibility(View.GONE);
            rootView.findViewById(R.id.llv_color_selector).setVisibility(View.GONE);
            rootView.findViewById(R.id.iv_erase).setVisibility(View.GONE);
            rootView.findViewById(R.id.iv_enlarge).setVisibility(View.GONE);
        }

        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height);
        rootView.findViewById(R.id.container_base).setLayoutParams(lps);

        if (((BaseLiveAct)context).getWBManager() != null) {
            // 设置工具为画笔
            ((BaseLiveAct)context).getWBManager().getBoard().setToolType(TEDU_BOARD_TOOL_TYPE_PEN);
            // 设置是否涂鸦
            ((BaseLiveAct)context).getWBManager().getBoard().setDrawEnable(!LiveRoomInfoProvider.getInstance().isGuest());
        }
    }


    /**
     * @param v      (是通过哪个View弹出的)
     * @param isAnim 是否显示动画效果
     */
    public void show(View v, boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(View v) {
        show();
    }

    /**
     * 添加View到根视图
     */
    public void show() {
        if (isShowing()) {
            return;
        }
        isShowing = true;
        LiveRoomInfoProvider.getInstance().isShowDrawPad = true;
        onAttached(rootView);
        rootView.requestFocus();
        isDrawPadPopIniting = false;
        initAddView();
//        if (LiveRoomInfoProvider.getInstance().isHost()) {
//            ((BaseLiveAct)context).getWBManager().getBoard().setGlobalBackgroundColor(new TEduBoardController.TEduBoardColor(0, 0, 0, 0));
//        }
        ((BaseLiveAct)context).getWBManager().getBoard().setDrawEnable(!LiveRoomInfoProvider.getInstance().isGuest());
        ((BaseLiveAct)context).getWBManager().getBoard().setDataSyncEnable(!LiveRoomInfoProvider.getInstance().isGuest());
        ((BaseLiveAct)context).getWBManager().getBoard().setBoardRatio(AppUtils.getWidth(context) + ":" + (1.3 * AppUtils.getWidth(context)));
        ((ImageView)rootView.findViewById(R.id.iv_enlarge)).setImageResource(R.drawable.icon_draw_enlarge);
        isEnlargeStatus = false;
        setBrushColor();

        new Handler().postDelayed(() -> {
            mWhiteboardView.setVisibility(View.VISIBLE);
        }, 500);
        // 正在展示手绘板，通知进入房间提醒隐藏
        if (operationCallback != null){
            operationCallback.onWacomShowHidden(true);
        }
        LogUtil.i(LiveLogTag.WhiteboardTag, "DrawPadPop show");
    }

    private void onAttached(View view) {
        decorView.addView(view);
        if (isAnim) {
            contentContainer.startAnimation(inAnim);
        }
    }

    private void initAddView(){
        View boardview = ((BaseLiveAct)context).getWBManager().getBoard().getBoardRenderView();
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);

        if (boardview != null) {
            // 设置背景色
            boardview.setBackgroundColor(0);
            // 设置填充透明度 范围：0-255
            boardview.getBackground().setAlpha(0);
            ViewGroup parent = (ViewGroup) boardview.getParent();
            if (parent != null) {
                parent.removeAllViews();
            }
            mWhiteboardView.removeAllViews();
            mWhiteboardView.addView(boardview,layoutParams);
            FrameLayout.LayoutParams layoutParams1 = (FrameLayout.LayoutParams) boardview.getLayoutParams();
            layoutParams1.height = (int) (1.3 * AppUtils.getWidth(context));
            layoutParams1.width = AppUtils.getWidth(context);
            boardview.setLayoutParams(layoutParams1);
            ((BaseLiveAct)context).getWBManager().getBoard().setToolType(TEDU_BOARD_TOOL_TYPE_PEN);
            if (!LiveRoomInfoProvider.getInstance().isGuest()){
                ((BaseLiveAct)context).getWBManager().getBoard().setBoardRatio(AppUtils.getWidth(context) + ":" + (1.3 * AppUtils.getWidth(context)));
            }
        }
    }

    public boolean isShowing() {
        return rootView.getParent() != null || isShowing;
    }


    public void dismiss(boolean isClick) {
        if (isAnim) {
            //消失动画
            outAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    dismissImmediately(isClick);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            contentContainer.startAnimation(outAnim);
        } else {
            dismissImmediately(isClick);
        }
    }

    /**
     * 手绘板关闭最终都会走此方法
     */
    public void dismissImmediately(boolean isClick) {
        decorView.post(() -> {
            //从根视图移除
            decorView.removeView(rootView);
            isShowing = false;
            LiveRoomInfoProvider.getInstance().isShowDrawPad = false;
            mOnDismissListener.onDismiss();
            mWhiteboardView.setVisibility(View.GONE);
            if (operationCallback != null){
                operationCallback.disDrawPad(isClick);
                // 关闭手绘板，通知进入房间提醒显示
                operationCallback.onWacomShowHidden(false);
            }
            LogUtil.i(LiveLogTag.WhiteboardTag, "DrawPadPop dismiss");
        });

    }

    public Animation getInAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, true);
        return AnimationUtils.loadAnimation(context, res);
    }

    public Animation getOutAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, false);
        return AnimationUtils.loadAnimation(context, res);
    }

    public void setKeyBackCancelable(boolean isCancelable) {
        ViewGroup View;
        View = rootView;
        View.setFocusable(isCancelable);
        View.setFocusableInTouchMode(isCancelable);
        if (isCancelable) {
            View.setOnKeyListener(onKeyBackListener);
        } else {
            View.setOnKeyListener(null);
        }
    }

    private View.OnKeyListener onKeyBackListener = (v, keyCode, event) -> {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == MotionEvent.ACTION_DOWN
                && isShowing()) {
            return true;
        }
        return false;
    };

    public DrawPadPop setOutSideCancelable(boolean isCancelable) {
        if (rootView != null) {
            View view = rootView.findViewById(com.netease.nim.uikit.R.id.outmost_container);

            if (isCancelable) {
                view.setOnTouchListener(onCancelableTouchListener);
            } else {
                view.setOnTouchListener(null);
            }
        }
        return this;
    }

    /**
     * Called when the user touch on black overlay in order to dismiss the dialog
     */
    @SuppressLint("ClickableViewAccessibility")
    private final View.OnTouchListener onCancelableTouchListener = (v, event) -> {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            dismiss(true);
        }
        return false;
    };

    public interface OnDismissListener {
        void onDismiss();
    }

    private OnDismissListener mOnDismissListener;

    public void setmOnDismissListener(OnDismissListener mOnDismissListener) {
        this.mOnDismissListener = mOnDismissListener;
    }
}
