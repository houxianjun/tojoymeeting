package com.tojoy.cloud.online_meeting.App.Distribution.view.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.util.string.RegixUtil;
import com.tojoy.tjoybaselib.util.string.StringUtil;

import java.util.Timer;
import java.util.TimerTask;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * @author  houxianjun
 * @Date 2020/07/15
 */
public class ApplyPhoneVerificationPopView extends PopupWindow {
    private Activity mContext;
     private ImageView mPopClose;
     private TextView suretv;
     private TextView rightManagerTv;

    public ApplyPhoneVerificationPopView(Context context) {
        super(context);
        mContext = (Activity) context;

        initUI();
    }

    private void initUI() {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.popup_apply_withdrawal_verification, null);
        setContentView(mView);
        suretv = mView.findViewById(R.id.tv_sure);
        mPopClose = mView.findViewById(R.id.popClose);
        rightManagerTv= mView.findViewById(R.id.tv_right_manager);
        setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        setAnimationStyle(R.style.distribution_pop_anim_scale_alpha);
        setBackgroundDrawable(new ColorDrawable());
        setFocusable(true);
        setOutsideTouchable(false);
        setTouchable(true);
        setData();
        initListener();
    }

    private void setData() {
        requestSendMsg();
    }

    private void initListener() {
        suretv.setOnClickListener(v -> {
            dismiss();
            ARouter.getInstance().build(RouterPathProvider.Distribution_WithdrawalDetail_Act)
                    .navigation();
        });
        mPopClose.setOnClickListener(v -> dismiss());
    }


    /**
     * ***************************************** 验证码 **************************************
     */
    private void requestSendMsg() {
        final String account = "13261590769";
        if (StringUtil.isEmpty(account)) {
            Toasty.normal(mContext, "输入的手机号不能为空").show();
            return;
        }
        if (account.length() < 11) {
            Toasty.normal(mContext, "输入正确手机号码").show();
            return;
        }

        if (!RegixUtil.checkMobile(account)) {
            Toasty.normal(mContext, "输入正确手机号码").show();
            return;
        }


        OMAppApiProvider.getInstance().getCode(account, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                Toasty.normal(mContext, "网络异常").show();
            }

            @Override
            public void onNext(OMBaseResponse randomCodeResponse) {
                if (!randomCodeResponse.isSuccess()) {
                    Toasty.normal(mContext, randomCodeResponse.msg).show();
                }
            }
        });
       new Handler().postDelayed(() -> {
            status = 2;
            refreshSendStatus();
            startCountDown();
        }, 500);
    }

    /**
     * 倒计时
     */
    //倒计时
    Timer mTimer;
    Handler mTimerHandler;
    int mSecond = 60;

    @SuppressLint("HandlerLeak")
    private void startCountDown() {
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Message message = new Message();
                message.what = 1;
                mTimerHandler.sendMessage(message);
            }
        }, 1000, 1000);
        mTimerHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        if (mSecond <= 0) {
                            mTimer.cancel();
                            status = 3;
                            refreshSendStatus();
                            return;
                        }
                        updateTimeCount();
                        break;
                }
                super.handleMessage(msg);
            }
        };
    }

    /**
     * 更改时间
     */
    private void updateTimeCount() {
        mSecond--;
        if (mSecond <= 0) {
            status = 3;
            refreshSendStatus();
            return;
        }
        status = 2;
        refreshSendStatus();
    }

    /**
     * 刷新发送按钮状态
     */
    int status = -1;

    private void refreshSendStatus() {
        switch (status) {
            case -1://不可发送
                rightManagerTv.setClickable(false);
                rightManagerTv.setText(mContext.getResources().getString(R.string.get_check_code));
                rightManagerTv.setTextColor(Color.parseColor("#7b808f"));
                break;
            case 0://可发送
                rightManagerTv.setClickable(true);
                rightManagerTv.setText(mContext.getResources().getString(R.string.get_check_code));
                rightManagerTv.setTextColor(Color.parseColor("#7b808f"));
                break;
            case 1://发送中
                rightManagerTv.setClickable(false);
                rightManagerTv.setText(mContext.getResources().getString(R.string.sending_check_code));
                rightManagerTv.setTextColor(Color.parseColor("#7b808f"));
                break;
            case 2://倒计时ing
                rightManagerTv.setClickable(false);
                rightManagerTv.setText( mSecond + "秒后重发");
                rightManagerTv.setTextColor(Color.parseColor("#2671E9"));
                break;
            case 3://重新发送
                mSecond = 60;
                if (mTimer != null) {
                    mTimer.cancel();
                }
                rightManagerTv.setClickable(true);
                rightManagerTv.setText("重新获取");
                rightManagerTv.setTextColor(Color.parseColor("#7b808f"));
                break;
            default:
                break;
        }
    }


}
