package com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.activity;

import android.os.Bundle;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.adapter.DistributionWithdrawalApplyRecordAdapter;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.home.MineDistributionApplyRecordResponse;
import com.tojoy.tjoybaselib.model.home.MineProfitListDistributionResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 分销客 提现明细列表
 *
 * @author houxianjun
 * @Date 2020/07/16
 */
@Route(path = RouterPathProvider.Distribution_WithdrawalRecord_Act)
public class DistributionWithdrawalRecordAct extends UI {
    private TJRecyclerView mRecyclerView;
    private DistributionWithdrawalApplyRecordAdapter mAdapter;
    private int mPage = 1;
    private List<MineDistributionApplyRecordResponse.ApplyModel> mDatas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distribution_mymenber_teamlist);
        ARouter.getInstance().inject(this);
        setStatusBar();
        setSwipeBackEnable(false);
        initTitle();
        initUI();
        querytDistributionWithdrawApplpList();
    }

    private void initTitle() {
        setUpNavigationBar();
        showBack();
        setTitle("提现明细");
    }

    public void initUI() {
        mRecyclerView = (TJRecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.getRecyclerView().setVerticalScrollBarEnabled(false);
        mAdapter = new DistributionWithdrawalApplyRecordAdapter(this, mDatas);
        mRecyclerView.setLoadMoreViewWithHide();
        mRecyclerView.setEmptyImgAndTip(R.drawable.icon_no_content, "还没有提现记录");
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                querytDistributionWithdrawApplpList();
            }

            @Override
            public void onLoadMore() {
                mPage++;
                querytDistributionWithdrawApplpList();
            }
        });
    }

    /**
     * 提现列表请求
     */
    public void querytDistributionWithdrawApplpList() {
        OMAppApiProvider.getInstance().querytDistributionWithdrawApplpList(mPage + "", "10", new Observer<MineDistributionApplyRecordResponse>() {

            @Override
            public void onCompleted() {
                mRecyclerView.stopRefresh();
            }

            @Override
            public void onError(Throwable e) {
                mRecyclerView.stopRefresh();
            }

            @Override
            public void onNext(MineDistributionApplyRecordResponse response) {
                if (response.isSuccess()) {
                    if (mPage == 1) {
                        mDatas.clear();
                    }
                    mDatas.addAll(response.data.records);
                    mAdapter.updateData(mDatas);
                    mRecyclerView.refreshLoadMoreView(mPage, response.data.records.size());
                } else {
                    Toasty.normal(DistributionWithdrawalRecordAct.this, response.msg).show();
                }
            }
        });
    }
}
