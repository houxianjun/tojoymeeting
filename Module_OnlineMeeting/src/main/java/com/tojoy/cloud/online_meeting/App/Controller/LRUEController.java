package com.tojoy.cloud.online_meeting.App.Controller;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.OnlineMembersHelper;
import com.tojoy.cloud.online_meeting.App.Live.Live.View.BottomOperationView;
import com.tojoy.cloud.online_meeting.R;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by luuzhu on 2019/7/23.
 * 直播间的UE效果控制类
 */

public class LRUEController {

    private static LRUEController instance;

    private LRUEController() {
    }

    public synchronized static LRUEController getInstance() {
        if (instance == null) {
            instance = new LRUEController();
        }
        return instance;
    }

    /**
     * 关闭观众接收屏幕中间商品的ue
     */
    public void doCloseCenterListUE(Context context, int[] location, View targetView, View locationView){

        //缩放 从1到0
        ScaleAnimation scale = new ScaleAnimation(1f, 0, 1f, 0,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        float btnX, btnY;
        int[] type1 = new int[2];
        locationView.getLocationOnScreen(type1);

        btnX = type1[0] + locationView.getWidth() / 2;
        btnY = type1[1] + locationView.getHeight() / 2;

        TranslateAnimation trans = new TranslateAnimation(
                Animation.ABSOLUTE, 0,
                Animation.ABSOLUTE, location[0] - btnX + AppUtils.dip2px(context, 15),
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.ABSOLUTE, location[1] - btnY);

        AnimationSet set = new AnimationSet(false);
        set.addAnimation(scale);
        set.addAnimation(trans);
        set.setDuration(400);
        set.setFillAfter(true);
        targetView.startAnimation(set);

        set.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                targetView.setVisibility(GONE);
                targetView.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    /**
     * 关闭观众接收屏幕中间活动二维码的ue
     */
    public void doCloseQRCenterUE(Context context, int[] location, View targetView, View locationView,onUEAnimationListener onUEAnimationListener){

        //缩放 从1到0
        ScaleAnimation scale = new ScaleAnimation(1f, 0, 1f, 0,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        float btnX, btnY;
        int[] type1 = new int[2];
        locationView.getLocationOnScreen(type1);

        btnX = type1[0] + locationView.getWidth() / 2;
        btnY = type1[1] + locationView.getHeight() / 2;

        TranslateAnimation trans = new TranslateAnimation(
                Animation.ABSOLUTE, 0,
                Animation.ABSOLUTE, location[0] - btnX + AppUtils.dip2px(context, 15),
                Animation.RELATIVE_TO_SELF, 0f,
                Animation.ABSOLUTE, location[1] - btnY);

        AnimationSet set = new AnimationSet(false);
        set.addAnimation(scale);
        set.addAnimation(trans);
        set.setDuration(400);
        set.setFillAfter(true);
        targetView.startAnimation(set);

        set.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (onUEAnimationListener != null) {
                    onUEAnimationListener.onSart();
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                targetView.setVisibility(GONE);
                targetView.clearAnimation();
                if (onUEAnimationListener != null) {
                    onUEAnimationListener.onEnd();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                if (onUEAnimationListener != null) {
                    onUEAnimationListener.onRepeat();
                }
            }
        });
    }


    /**
     * 执行报名上会按钮关闭动关
     */
    public void doCloseUE(Context context, BottomOperationView mBottomOperationView, View mRlMeetingSign, View mIvCloseMeetingSign) {
        //互动按钮坐标
        int[] location = mBottomOperationView.getInteractionIconLocation();
        //缩放 从1到0
        ScaleAnimation scale = new ScaleAnimation(1f, 0, 1f, 0,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        //位移 X轴距离为互动x坐标 - 按钮中点坐标 + 互动按钮宽度的一半     Y轴距离为互动y坐标  -  按钮中点y坐标
        float btnX = 0, btnY = 0;
        int[] type1 = new int[2];
        mRlMeetingSign.getLocationOnScreen(type1);
        btnX = type1[0] + mRlMeetingSign.getWidth() / 2;
        btnY = type1[1] + mRlMeetingSign.getHeight() / 2;
        TranslateAnimation trans = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.ABSOLUTE, location[0] - btnX + AppUtils.dip2px(context, 15),
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.ABSOLUTE, location[1] - btnY);
        AnimationSet set = new AnimationSet(false);
        set.addAnimation(scale);
        set.addAnimation(trans);
        set.setDuration(500);
        set.setFillAfter(true);
        mIvCloseMeetingSign.setVisibility(GONE);
        mRlMeetingSign.startAnimation(set);

        set.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mRlMeetingSign.setVisibility(GONE);
                mRlMeetingSign.clearAnimation();
                mBottomOperationView.getRedDot().setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    private float downX;

    /**
     * 小聊天框左右滑动显示隐藏
     */
    @SuppressLint("ClickableViewAccessibility")
    public void smallMsgListUE(BaseLiveAct act, FrameLayout mChatMsgContainer, BottomOperationView mBottomOperationView) {
        act.findViewById(R.id.msgCoverLayout).setOnTouchListener((view, motionEvent) -> {

            //如果正在同台直播，就不走小聊天布局的动画了
            if (OnlineMembersHelper.getInstance().hasOnlineMember()) {
                return false;
            }
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    downX = motionEvent.getX();
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (LiveRoomInfoProvider.getInstance().isShowFile) {
                        break;
                    }
                    float moveX = motionEvent.getX() - downX;
                    if (moveX > AppUtils.dip2px(act, 20)) {
                        if (mChatMsgContainer != null && mChatMsgContainer.getVisibility() == GONE) {
                            startMsgContainerAlphaAnima(true, mChatMsgContainer);
                            //未读数清零
                            mBottomOperationView.mUnreadLayout.setVisibility(GONE);
                            mBottomOperationView.mTvUnread.setText("0");
                        }
                    } else if (moveX < -AppUtils.dip2px(act, 20)) {
                        if (mChatMsgContainer != null && mChatMsgContainer.getVisibility() == VISIBLE) {
                            startMsgContainerAlphaAnima(false, mChatMsgContainer);
                        }
                    }
                    break;
                default:
                    break;
            }
            return true;
        });
    }

    private void startMsgContainerAlphaAnima(boolean isShow, FrameLayout mChatMsgContainer) {

        LiveRoomInfoProvider.getInstance().isCountingUnread = !isShow;

        AlphaAnimation anima;
        if (isShow) {
            anima = new AlphaAnimation(0, 1);
        } else {
            anima = new AlphaAnimation(1, 0);
        }
        anima.setDuration(200);
        anima.setFillAfter(true);
        mChatMsgContainer.startAnimation(anima);

        anima.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mChatMsgContainer.setVisibility(isShow ? VISIBLE : GONE);
                mChatMsgContainer.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /** 上一次进入直播间的动画是否执行完毕 */
    private boolean lastJoinUEOver = true;

    /**
     * 加入直播间UE
     */
    @SuppressLint("SetTextI18n")
    public void joinUE(BaseLiveAct act, String name, TextView mTvJoinLiveTip, boolean mIsCanShowJoinTipUE) {
        if (TextUtils.isEmpty(name) || !LiveRoomInfoProvider.getInstance().joinNamesArr.contains(name)) {
            return;
        }
        if (lastJoinUEOver) {
            lastJoinUEOver = false;
            String tempName = "";
            if (name.length() > 9 && !name.contains("****")) {
                tempName = name.substring(0, 8) + "…";
            }
            mTvJoinLiveTip.setText((TextUtils.isEmpty(tempName) ? name : tempName) + "    进入了会议");
            mTvJoinLiveTip.setVisibility(mIsCanShowJoinTipUE ? VISIBLE : GONE);
            ObjectAnimator in = ObjectAnimator.ofFloat(mTvJoinLiveTip, "translationX",
                    AppUtils.getScreenWidth(act), 0);
            in.setDuration(200);
            in.start();
            new Handler().postDelayed(() -> {
                mTvJoinLiveTip.clearAnimation();
                ObjectAnimator out = ObjectAnimator.ofFloat(mTvJoinLiveTip, "translationX",
                        0, -AppUtils.getScreenWidth(act));
                out.setDuration(200);
                out.start();
                lastJoinUEOver = true;
                LiveRoomInfoProvider.getInstance().joinNamesArr.remove(name);

                if (LiveRoomInfoProvider.getInstance().joinNamesArr.size() > 0) {
                    String nextName = LiveRoomInfoProvider.getInstance().joinNamesArr.get(0);
                    new Handler().postDelayed(() -> joinUE(act, nextName, mTvJoinLiveTip, mIsCanShowJoinTipUE), 200);
                }

            }, 2500);
        }
    }

    /** 上一次提示动画是否执行完毕 */
    private boolean lastTipsUEOver = true;
    /**
     * 观众感兴趣/抢占/我要合作 的提示
     */
    public void startGuestTipsUE(BaseLiveAct act, TextView textView, String info) {
        if (!LiveRoomInfoProvider.getInstance().guestTipsArr.contains(info)) {
            return;
        }
        if(!lastTipsUEOver) {
            return;
        }
        lastTipsUEOver = false;

        textView.setVisibility(VISIBLE);
        textView.setText(info);

        TranslateAnimation in = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 2, Animation.RELATIVE_TO_SELF, 0);

        in.setDuration(500);
        textView.startAnimation(in);

        new Handler().postDelayed(() -> {
            TranslateAnimation out = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                    Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, -2);

            out.setDuration(500);
            textView.startAnimation(out);
            new Handler().postDelayed(() -> {
                textView.clearAnimation();
                textView.setVisibility(GONE);
                lastTipsUEOver = true;
                LiveRoomInfoProvider.getInstance().guestTipsArr.remove(info);
                if(!LiveRoomInfoProvider.getInstance().guestTipsArr.isEmpty()) {
                    startGuestTipsUE(act, textView, LiveRoomInfoProvider.getInstance().guestTipsArr.get(0));
                }

            }, 490);

        }, 3600);

    }

    public interface onUEAnimationListener{
        void onSart();
        void onEnd();
        void onRepeat();
    }
}

