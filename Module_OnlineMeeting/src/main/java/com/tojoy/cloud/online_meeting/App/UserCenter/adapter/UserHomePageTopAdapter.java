package com.tojoy.cloud.online_meeting.App.UserCenter.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.vlayout.DelegateAdapter;
import com.tojoy.tjoybaselib.ui.vlayout.LayoutHelper;
import com.tojoy.tjoybaselib.ui.vlayout.layout.SingleLayoutHelper;
import com.tojoy.cloud.online_meeting.App.Home.view.VideoPlayAnimationView;
import com.tojoy.cloud.online_meeting.App.Live.Live.View.CircleBorderTransform;
import com.tojoy.cloud.online_meeting.App.UserCenter.activity.UserHomePageAct;
import com.tojoy.tjoybaselib.model.live.MyLiveRoomListResponse;
import com.tojoy.cloud.online_meeting.App.UserCenter.model.UserHomePageTopModel;
import com.tojoy.cloud.online_meeting.R;

import es.dmoral.toasty.Toasty;
import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * @author qll
 * @date 2019/4/1
 */
public class UserHomePageTopAdapter extends DelegateAdapter.Adapter<UserHomePageTopAdapter.UserHomePageTopHolder> {
    private Context mContext;
    private LayoutHelper mLayoutHelper;
    private UserHomePageTopModel mUserHomeTopModel;

    private UserHomePageTopHolder mViewHolder;
    private int mViewTypeItem;
    private String mViewType;


    public UserHomePageTopAdapter(Context context, UserHomePageTopModel model, int vieType, String viewType) {
        this.mContext = context;
        this.mUserHomeTopModel = model;
        this.mViewTypeItem = vieType;
        this.mViewType = viewType;
    }

    @Override
    public LayoutHelper onCreateLayoutHelper() {
        if (mLayoutHelper == null) {
            mLayoutHelper = new SingleLayoutHelper();
        }
        return mLayoutHelper;
    }

    @Override
    public void onViewAttachedToWindow(@NonNull UserHomePageTopHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.mRlvAnimContainer.removeAllViews();
        VideoPlayAnimationView videoPlayAnimationView = new VideoPlayAnimationView((Activity) mContext);
        holder.mRlvAnimContainer.addView(videoPlayAnimationView.getView());
    }

    @Override
    public UserHomePageTopHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserHomePageTopHolder(LayoutInflater.from(mContext).inflate(R.layout.activity_user_home_top_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(UserHomePageTopHolder holder, int position) {
        mViewHolder = holder;

        if (LiveRoomInfoProvider.getInstance().mLiveTags.size() > 0) {
            mViewHolder.mTagdoll1.setVisibility(View.VISIBLE);
            mViewHolder.mTagContainer1.setVisibility(View.VISIBLE);
            mViewHolder.mTag1.setText(LiveRoomInfoProvider.getInstance().mLiveTags.get(0).tagName);
        }

        if (LiveRoomInfoProvider.getInstance().mLiveTags.size() > 1) {
            mViewHolder.mTagdoll2.setVisibility(View.VISIBLE);
            mViewHolder.mTagContainer2.setVisibility(View.VISIBLE);
            mViewHolder.mTag2.setText(LiveRoomInfoProvider.getInstance().mLiveTags.get(1).tagName);
        }

        if (LiveRoomInfoProvider.getInstance().mLiveTags.size() > 2) {
            mViewHolder.mTagdoll3.setVisibility(View.VISIBLE);
            mViewHolder.mTagContainer3.setVisibility(View.VISIBLE);
            mViewHolder.mTag3.setText(LiveRoomInfoProvider.getInstance().mLiveTags.get(2).tagName);
        }

        if (LiveRoomInfoProvider.getInstance().mLiveTags.size() > 3) {
            mViewHolder.mTagdoll4.setVisibility(View.VISIBLE);
            mViewHolder.mTagContainer4.setVisibility(View.VISIBLE);
            mViewHolder.mTag4.setText(LiveRoomInfoProvider.getInstance().mLiveTags.get(3).tagName);
        }

        if (LiveRoomInfoProvider.getInstance().mLiveTags.size() > 4) {
            mViewHolder.mTagdoll5.setVisibility(View.VISIBLE);
            mViewHolder.mTagContainer5.setVisibility(View.VISIBLE);
            mViewHolder.mTag5.setText(LiveRoomInfoProvider.getInstance().mLiveTags.get(4).tagName);
        }


        mViewHolder.mLiveRoomId.setText("ID:" + mUserHomeTopModel.roomCode);
        mViewHolder.mUserName.setText(TextUtils.isEmpty(mUserHomeTopModel.userName) ? "" : mUserHomeTopModel.userName);

        if (!TextUtils.isEmpty(mUserHomeTopModel.hostPic)) {
            Glide.with(mContext)
                    .load(OSSConfig.getOSSURLedStr(OSSConfig.getRemoveStylePath((String) mUserHomeTopModel.hostPic)))
                    .apply(RequestOptions.bitmapTransform(new BlurTransformation(25, 3))
                            .placeholder(R.drawable.bg_online_user_center_top))
                    .into(mViewHolder.mGasBg);

            Glide.with(mContext)
                    .load(OSSConfig.getOSSURLedStr(OSSConfig.getRemoveStylePath((String) mUserHomeTopModel.hostPic)))
                    .apply(new RequestOptions().centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .transform(new CircleBorderTransform(mContext, 2, Color.parseColor("#ffffff")))
                            .placeholder(R.drawable.icon_dorecord))
                    .into(mViewHolder.mUserHeaderIv);
        } else {
            Glide.with(mContext)
                    .load(R.drawable.bg_online_user_center_top)
                    .apply(RequestOptions.bitmapTransform(new BlurTransformation(25, 3)))
                    .into(mViewHolder.mGasBg);

            Glide.with(mContext)
                    .load(R.drawable.icon_dorecord)
                    .apply(new RequestOptions().centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .transform(new CircleBorderTransform(mContext, 2, Color.parseColor("#ffffff")))
                            .placeholder(R.drawable.icon_dorecord))
                    .into(mViewHolder.mUserHeaderIv);
        }

        if (mUserHomeTopModel.endLiveInfoModel != null) {
            mViewHolder.mLiveInfoLayout.setVisibility(View.VISIBLE);
            mViewHolder.mLiveCount.setText(mUserHomeTopModel.endLiveInfoModel.getFormatNum(mUserHomeTopModel.endLiveInfoModel.enterNum));
//            mViewHolder.mLiveCommentCount.setText(mUserHomeTopModel.endLiveInfoModel.getFormatNum(mUserHomeTopModel.endLiveInfoModel.commentNum));
            mViewHolder.mLiveLikeCount.setText(mUserHomeTopModel.endLiveInfoModel.getFormatNum(mUserHomeTopModel.endLiveInfoModel.interestedKeyNum));
            mViewHolder.mLiveGetCount.setText(mUserHomeTopModel.endLiveInfoModel.getFormatNum(mUserHomeTopModel.endLiveInfoModel.seizeNum));
            // 如果没有值，或者没有这个字段: 立即购买数; 字段值为0:意向购买数; 字段值为1:立即购买数
            mViewHolder.mTvWant.setText(TextUtils.isEmpty(mUserHomeTopModel.endLiveInfoModel.payment) ? "立即购买数"
                    : ("0".equals(mUserHomeTopModel.endLiveInfoModel.payment) ? "意向购买数" : "立即购买数"));
        } else {
            mViewHolder.mLiveInfoLayout.setVisibility(View.GONE);
        }

        if ("-1".equals(mViewType) || "8".equals(mViewType)) {
            mViewHolder.mLiveTimeTv.setVisibility(View.GONE);
            mViewHolder.mEndTipLayout.setVisibility(View.GONE);
            mViewHolder.mLiveRoomId.setVisibility(View.VISIBLE);
            mViewHolder.mUserRoomId.setVisibility(View.GONE);
        } else {
            mViewHolder.mLiveTimeTv.setVisibility(View.VISIBLE);
            mViewHolder.mEndTipLayout.setVisibility(View.VISIBLE);
            mViewHolder.mLiveRoomId.setVisibility(View.VISIBLE);
            mViewHolder.mUserRoomId.setVisibility(View.GONE);

            mViewHolder.mLiveTimeTv.setText("会议时长：" + mUserHomeTopModel.duration);

            if ("1".equals(mViewType) || "8".equals(mViewType)) {
                mViewHolder.mEndTipLayout.setBackgroundResource(R.drawable.bg_liveend_tip1);
                mViewHolder.mEndTipTv.setText("会议已结束");
            }

            if ("2".equals(mViewType)) {
                mViewHolder.mEndTipLayout.setBackgroundResource(R.drawable.bg_liveend_tip3);
                mViewHolder.mEndTipTv.setText("会议已结束，去看看其他会议吧！");
            }

            if ("3".equals(mViewType)) {
                mViewHolder.mEndTipLayout.setBackgroundResource(R.drawable.bg_liveend_tip2);
                mViewHolder.mEndTipTv.setText("很抱歉！您已经移除直播间！");
            }

        }
    }

    public void setNameAndHeadImg() {
        mViewHolder.mUserName.setText(TextUtils.isEmpty(mUserHomeTopModel.userName) ? "" : mUserHomeTopModel.userName);
        if (!TextUtils.isEmpty(mUserHomeTopModel.hostPic)) {
            Glide.with(mContext)
                    .load(OSSConfig.getOSSURLedStr(OSSConfig.getRemoveStylePath((String) mUserHomeTopModel.hostPic)))
                    .apply(RequestOptions.bitmapTransform(new BlurTransformation(25, 3))
                            .placeholder(R.drawable.bg_online_user_center_top))
                    .into(mViewHolder.mGasBg);

            Glide.with(mContext)
                    .load(OSSConfig.getOSSURLedStr(OSSConfig.getRemoveStylePath((String) mUserHomeTopModel.hostPic)))
                    .apply(new RequestOptions().centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .transform(new CircleBorderTransform(mContext, 2, Color.parseColor("#ffffff")))
                            .placeholder(R.drawable.icon_dorecord))
                    .into(mViewHolder.mUserHeaderIv);
        } else {
            Glide.with(mContext)
                    .load(R.drawable.bg_online_user_center_top)
                    .apply(RequestOptions.bitmapTransform(new BlurTransformation(25, 3)))
                    .into(mViewHolder.mGasBg);

            Glide.with(mContext)
                    .load(R.drawable.icon_dorecord)
                    .apply(new RequestOptions().centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .transform(new CircleBorderTransform(mContext, 2, Color.parseColor("#ffffff")))
                            .placeholder(R.drawable.icon_dorecord))
                    .into(mViewHolder.mUserHeaderIv);
        }
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    /**
     * 必须重写不然会出现滑动不流畅的情况
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mViewTypeItem;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class UserHomePageTopHolder extends RecyclerView.ViewHolder {

        private ImageView mGasBg;
        private ImageView mUserHeaderIv;

        private RelativeLayout mEndTipLayout;
        private TextView mEndTipTv;

        //房间ID
        private TextView mLiveRoomId;

        private RelativeLayout mTagContainer1;
        private RelativeLayout mTagContainer2;
        private RelativeLayout mTagContainer3;
        private RelativeLayout mTagContainer4;
        private RelativeLayout mTagContainer5;

        private TextView mTag1;
        private TextView mTag2;
        private TextView mTag3;
        private TextView mTag4;
        private TextView mTag5;

        private View mTagdoll1;
        private View mTagdoll2;
        private View mTagdoll3;
        private View mTagdoll4;
        private View mTagdoll5;


        //直播信息
        private LinearLayout mLiveInfoLayout;
        private TextView mLiveTimeTv;
        private TextView mLiveCount;
        private TextView mLiveCommentCount;
        private TextView mLiveLikeCount;
        private TextView mLiveGetCount;

        private TextView mUserName;
        private TextView mUserRoomId;

        private RelativeLayout mRlvAnimContainer;
        private TextView mTvWant;


        public UserHomePageTopHolder(View itemView) {
            super(itemView);
            mGasBg = itemView.findViewById(R.id.iv_gas_bg);
            mUserHeaderIv = itemView.findViewById(R.id.iv_user_header);

            //结束直播提示音
            mEndTipLayout = itemView.findViewById(R.id.rlv_live_end_tip);
            mEndTipTv = itemView.findViewById(R.id.tv_end_tip);

            mLiveTimeTv = itemView.findViewById(R.id.tv_time);

            mLiveRoomId = itemView.findViewById(R.id.tv_liveroom);

            mLiveInfoLayout = itemView.findViewById(R.id.rlv_liveinfo);
            mLiveCount = itemView.findViewById(R.id.tv_live_count);
//            mLiveCommentCount = itemView.findViewById(R.id.tv_comment_count);
            mLiveLikeCount = itemView.findViewById(R.id.tv_like_count);
            mLiveGetCount = itemView.findViewById(R.id.tv_get_count);
            mTvWant = itemView.findViewById(R.id.tv_want);

            //标签
            mTagdoll1 = itemView.findViewById(R.id.doll1);
            mTagdoll2 = itemView.findViewById(R.id.doll2);
            mTagdoll3 = itemView.findViewById(R.id.doll3);
            mTagdoll4 = itemView.findViewById(R.id.doll4);
            mTagdoll5 = itemView.findViewById(R.id.doll5);

            mTagContainer1 = itemView.findViewById(R.id.rlv_tag1);
            mTagContainer2 = itemView.findViewById(R.id.rlv_tag2);
            mTagContainer3 = itemView.findViewById(R.id.rlv_tag3);
            mTagContainer4 = itemView.findViewById(R.id.rlv_tag4);
            mTagContainer5 = itemView.findViewById(R.id.rlv_tag5);

            mTag1 = itemView.findViewById(R.id.tv_tag1);
            mTag2 = itemView.findViewById(R.id.tv_tag2);
            mTag3 = itemView.findViewById(R.id.tv_tag3);
            mTag4 = itemView.findViewById(R.id.tv_tag4);
            mTag5 = itemView.findViewById(R.id.tv_tag5);

            mUserName = itemView.findViewById(R.id.tv_user_name);
            mUserRoomId = itemView.findViewById(R.id.tv_user_room_id);

            mRlvAnimContainer = itemView.findViewById(R.id.rlv_anim_container);

        }
    }
}
