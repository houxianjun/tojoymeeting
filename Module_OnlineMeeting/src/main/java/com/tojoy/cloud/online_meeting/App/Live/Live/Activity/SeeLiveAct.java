package com.tojoy.cloud.online_meeting.App.Live.Live.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.google.gson.Gson;
import com.netease.nim.uikit.business.chatroom.helper.ChatRoomHelper;
import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tencent.rtmp.ui.TXCloudVideoView;
import com.tencent.trtc.TRTCCloudDef;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.cloud.online_meeting.App.Controller.LRDialogController;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.Model.Router.AppRouterJumpModel;
import com.tojoy.common.Services.EventBus.LiveRoomEvent;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import es.dmoral.toasty.Toasty;

import static com.tencent.trtc.TRTCCloudDef.TRTCRoleAudience;

/**
 * 观看直播
 */
@Route(path = RouterPathProvider.ONLINE_MEETING_SEE_LIVE_ROOM)
public class SeeLiveAct extends BaseLiveAct {
    @SuppressLint("StaticFieldLeak")
    public static SeeLiveAct SINGTON;
    private String pushLiveId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SINGTON = this;

        // 初始化超级播放器
//        mSuperPlayerView = (MyPlayView) findViewById(R.iad.superVodPlayerView);
//        initFloatLive();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String pushJson = intent.getStringExtra("liveJson"); //{"hasPassword":"0","roomLiveId":"11143","title":"0","userId":1082279}
        if (!TextUtils.isEmpty(pushJson)) {
            if (!BaseUserInfoCache.isLogined(this)) {
                ARouter.getInstance().build(RouterPathProvider.Login)
                        .navigation();
                finish();
            } else {
                doPush(pushJson);
            }
        }
    }

    @Override
    protected void doPush(String pushJson) {
        if (pushJson.startsWith("{") && pushJson.endsWith("}")) {
            AppRouterJumpModel model = new Gson().fromJson(pushJson, AppRouterJumpModel.class);
            pushLiveId = model.roomLiveId;
        } else {
            pushLiveId = pushJson;
        }
        boolean isSameRoom = pushLiveId.equals(LiveRoomInfoProvider.getInstance().liveId);

        //用户正在一个直播间
        if (ChatRoomHelper.isFromLiveRoom) {
            if (!isSameRoom) {
                TJMakeSureDialog tjMakeSureDialog = new TJMakeSureDialog(SeeLiveAct.this);
                tjMakeSureDialog.setTitleAndCotent("", "您还未离开当前会议\n要加入新的会议么");
                tjMakeSureDialog.setBtnSureText("加入");
                tjMakeSureDialog.setBtnCancelText("不加入");
                tjMakeSureDialog.hideTitle();
                tjMakeSureDialog.setEvent(v1 -> {
                    leaveAndJoin();
                });
                tjMakeSureDialog.show();
            }
        } else {
            joinLive();
        }
    }

    private void leaveAndJoin() {
        LiveRoomIOHelper.leaveLiveRoom(SeeLiveAct.this, new IOLiveRoomListener() {
            @Override
            public void onIOError(String error, int errorCode) {

            }

            @Override
            public void onIOSuccess() {
                if (mTrtcCloud != null) {
                    mTrtcCloud.exitRoom();
                }
                getHandler().postDelayed(() -> joinLive(), 500);
            }
        }, false);
    }

    private void joinLive() {
        LiveRoomInfoProvider.getInstance().liveId = pushLiveId;
        LiveRoomInfoProvider.getInstance().joinPassword = "";

        LiveRoomIOHelper.joinLiveRoom(SeeLiveAct.this, new IOLiveRoomListener() {
            @Override
            public void onIOError(String error, int errorCode) {
                finish();
                Toasty.normal(SeeLiveAct.this, error).show();
            }

            @Override
            public void onIOSuccess() {
                loginTXLiveRoom();
            }
        });
    }

    @Override
    protected void refreshDialogs() {
        changeDialogState();
    }

    private void changeDialogState() {
        if (LiveRoomInfoProvider.getInstance().isGuest()) {

            //公告
            if (LRDialogController.getInstance().mNoticeDialog != null && LRDialogController.getInstance().mNoticeDialog.isShowing() && "1".equals(LRDialogController.getInstance().mNoticeDialog.mType)) {
                LRDialogController.getInstance().mNoticeDialog.initView();
            }

            //成员列表
            if (LRDialogController.getInstance().mMemverListDialog != null && LRDialogController.getInstance().mMemverListDialog.isShowing()) {
                LRDialogController.getInstance().mMemverListDialog.changeMemberType();
            }

            //发起成交
            if (LRDialogController.getInstance().mStartDealDialog != null && LRDialogController.getInstance().mStartDealDialog.isShowing()) {
                LRDialogController.getInstance().mStartDealDialog.dismiss();
            }

            //带有代播商品列表弹框
            if (LRDialogController.getInstance(). launchProductTJDialog!= null && LRDialogController.getInstance().launchProductTJDialog.isShowing()) {
                LRDialogController.getInstance().launchProductTJDialog.dismiss();
            }

        } else {

            //公告
            if (LRDialogController.getInstance().mNoticeDialog != null && LRDialogController.getInstance().mNoticeDialog.isShowing() && "1".equals(LRDialogController.getInstance().mNoticeDialog.mType)) {
                LRDialogController.getInstance().mNoticeDialog.initView();
            }

            //举报
            if (LRDialogController.getInstance().mNoticeDialog != null && LRDialogController.getInstance().mNoticeDialog.isShowing() && "2".equals(LRDialogController.getInstance().mNoticeDialog.mType)) {
                LRDialogController.getInstance().mNoticeDialog.dismiss();
            }

            //成员列表
            if (LRDialogController.getInstance().mMemverListDialog != null && LRDialogController.getInstance().mMemverListDialog.isShowing()) {
                LRDialogController.getInstance().mMemverListDialog.changeMemberType();
            }
        }

    }


    @Override
    protected void initVideoViewLayout() {
        super.initVideoViewLayout();
        //设置大屏幕为主播
        TXCloudVideoView localVideoView = mTRTCVideoViewLayout.getCloudVideoViewByIndex(0);
        localVideoView.setUserId(LiveRoomInfoProvider.getInstance().hostUserId);
    }

    @Override
    protected void enterRoom() {
        mTrtcParams.role = TRTCRoleAudience;

        //主播端采用视频通话模式
        mTrtcCloud.enterRoom(mTrtcParams, TRTCCloudDef.TRTC_APP_SCENE_LIVE);
        findViewById(R.id.rlv_close).setVisibility(View.VISIBLE);
    }

    @Override
    public void onEnterRoomSuccess() {
        super.onEnterRoomSuccess();
    }

    public interface GotProjectListCallback {
        void onGot();
        void onError();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPublicStringEvent(LiveRoomEvent event) {
        // 看是否观众点击push异常退出是因为这中情况
        LogUtil.i(LiveLogTag.SeeLiveTag, "LiveRoomEvent:" + event.getType());
        if(LiveRoomEvent.LIVE_CLOSE_GUEST.equals(event.getType()) || LiveRoomEvent.LIVE_CLOSE_ALL.equals(event.getType())) {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(LiveRoomInfoProvider.getInstance().isApplying) {
            LiveRoomInfoProvider.getInstance().isApplying = false;
        }

        // 超级播放器相关逻辑
//        if (isFloatMute) {
//            // 回到直播页面，展示直播信息，如果正在播放则停止小窗口并隐藏
//            showFloatLive(false);
//            muteLiveRoomAudio(false);
//        }
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onFloatVideoEvent(FloatEvent event) {
//        // 接收：播放/停止超级播放器的入口
//        showFloatLive(event.isFloating);
//        muteLiveRoomAudio(event.isFloating);
//    }

//    // 默认小窗口是关闭状态
//    boolean isFloatMute = false;
//
//    public void muteLiveRoomAudio(boolean muteAudio) {
//        isFloatMute = muteAudio;
//        if (muteAudio) {
//            if (mTRTCVideoViewLayout == null) {
//                return;
//            }
//            mTRTCVideoViewLayout.onPause();
//            LogUtil.i(LiveLogTag.BaseLiveTag, "onActivityPaused_isHostVideoOn ： " + OnlineMembersHelper.getInstance().isHostVideoOn);
//            if (!OnlineMembersHelper.getInstance().isHostVideoOn) {
//                return;
//            }
//            mTrtcCloud.muteAllRemoteAudio(true);
//        } else {
//            if (mTRTCVideoViewLayout != null) {
//                mTRTCVideoViewLayout.onResume();
//            }
//            LogUtil.i(LiveLogTag.BaseLiveTag, "onActivityResumed_isHostVideoOn ： " + OnlineMembersHelper.getInstance().isHostVideoOn);
//            if (!OnlineMembersHelper.getInstance().isHostVideoOn) {
//                return;
//            }
//            mTrtcCloud.muteAllRemoteAudio(false);
//        }
//    }
}
