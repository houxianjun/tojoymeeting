package com.tojoy.cloud.online_meeting.App.Apply.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.App.Apply.activity.ApplyAct;
import com.tojoy.cloud.online_meeting.App.Apply.adapter.SubmitFileOrCoverAdapter;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApplyJoinLiveModel;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApplyPhotoInfo;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApplyPowerTagModel;
import com.tojoy.tjoybaselib.services.OnlineMeeting.ApplyStateCode;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.App.BaseFragment;
import com.tojoy.common.Config.UserEmployeeFlagCode;
import com.tojoy.file.browser.FileViewAct;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * @author qll
 * @date 2019/3/29
 * 开会申请--可编辑
 * ApplyOver（未申请）、ApplyError（驳回，需将上次传的信息回显）可编辑
 * 其他：查看详情，只回显数据，不可编辑上传
 */
public class ApplyDetailFragment extends BaseFragment{

    private TagFlowLayout mIdflowlayout;
    private TextView mRoomIdTv, mRoomNameTv, mRoomPswTv, mRoomLookCountTv, mRoomLookPowerTv, mRoomDrainageTypeTv,mRoomInvitationTv,mRoomPlayBackTv;
    private TextView mStartLiveReasonTv;
    private RecyclerView mRoomCoverUploadRecyclerView, mRoomFileUploadRecyclerView;
    private LinearLayout mFileUploadLlv;
    private LinearLayout mPlayResaonLlv;
    private RelativeLayout mPswRlv;
    private View mLinePswView;

    private List<ApplyPowerTagModel> mTagData = new ArrayList();
    protected List<ApplyPhotoInfo> mCoverInfos = new ArrayList<>();
    protected List<ApplyPhotoInfo> mFileInfos = new ArrayList<>();

    private int gridCountHor = 3;

    private ApplyJoinLiveModel mApplyJoinLiveModel;

    @SuppressLint("ValidFragment")
    public ApplyDetailFragment() {
    }

    public ApplyDetailFragment setApplyStateCode(ApplyJoinLiveModel applyJoinLiveModel){
        this.mApplyJoinLiveModel = applyJoinLiveModel;
        return this;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_apply_look_detail, container, false);
        }
        isCreateView = true;
        return mView;
    }

    @Override
    protected void initUI() {
        super.initUI();
        mIdflowlayout = mView.findViewById(R.id.id_flowlayout);
        mRoomIdTv = mView.findViewById(R.id.tv_room_id);
        mRoomNameTv = mView.findViewById(R.id.tv_room_name);
        mRoomPswTv = mView.findViewById(R.id.tv_room_psw);
        mRoomLookCountTv = mView.findViewById(R.id.tv_room_look_count);
        mStartLiveReasonTv = mView.findViewById(R.id.tv_start_live_reason);
        mRoomCoverUploadRecyclerView = mView.findViewById(R.id.rlv_room_cover_upload);
        mRoomFileUploadRecyclerView = mView.findViewById(R.id.rlv_room_file_upload);
        mFileUploadLlv = mView.findViewById(R.id.llv_file_upload);
        mPlayResaonLlv = mView.findViewById(R.id.llv_play_resaon);
        mRoomLookPowerTv = mView.findViewById(R.id.tv_room_look_power);
        mPswRlv = mView.findViewById(R.id.rlv_psw);
        mLinePswView = mView.findViewById(R.id.v_line_psw);
        mRoomDrainageTypeTv = mView.findViewById(R.id.tv_room_drainage_type);
        mRoomInvitationTv = mView.findViewById(R.id.tv_room_invitation_count);
        mRoomPlayBackTv = mView.findViewById(R.id.tv_room_playback_power);

        gridCountHor = (AppUtils.getScreenWidth(getContext()) - AppUtils.dip2px(getContext(),37) * 2) / AppUtils.dip2px(getContext(), 80);
        initRoomData();
    }

    private void initRoomData() {
        mRoomIdTv.setText(mApplyJoinLiveModel.roomCode);
        mRoomNameTv.setText(mApplyJoinLiveModel.roomName);
        mPswRlv.setVisibility(View.VISIBLE);
        mLinePswView.setVisibility(View.VISIBLE);
        mRoomPswTv.setText(mApplyJoinLiveModel.roomPassword);
        mRoomLookCountTv.setText(String.valueOf(mApplyJoinLiveModel.viewNum));
        mRoomLookPowerTv.setText(mApplyJoinLiveModel.getAuthType());
        mRoomDrainageTypeTv.setText(mApplyJoinLiveModel.getDrainageType());
        mRoomPlayBackTv.setText(mApplyJoinLiveModel.getPlayBackType());
        if (TextUtils.isEmpty(BaseUserInfoCache.getCompanyCode(getActivity()))) {
            // 没有企业则不显示邀请人员信息
            mView.findViewById(R.id.v_line_invitation).setVisibility(View.GONE);
            mView.findViewById(R.id.rlv_room_invitation_count).setVisibility(View.GONE);
        } else {
            mView.findViewById(R.id.v_line_invitation).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.rlv_room_invitation_count).setVisibility(View.VISIBLE);
            mRoomInvitationTv.setText(mApplyJoinLiveModel.getInvitationPeoplesNum());
        }
        if (TextUtils.isEmpty(mApplyJoinLiveModel.playReason)) {
            mPlayResaonLlv.setVisibility(View.GONE);
        } else {
            mPlayResaonLlv.setVisibility(View.VISIBLE);
            mStartLiveReasonTv.setText(mApplyJoinLiveModel.playReason);
        }

        // 初始化选中的标签
        mTagData.addAll(mApplyJoinLiveModel.selestTagList);
        // 初始化上传的封面
        mCoverInfos.addAll(mApplyJoinLiveModel.picList);
        // 初始化上传的文件
        mFileInfos.addAll(mApplyJoinLiveModel.docList);

        initCoverAdapter();
        initFileAdapter();
        if (mTagData != null && mTagData.size() > 0){
            mView.findViewById(R.id.llv_select_tags).setVisibility(View.VISIBLE);
            initTagAdatper();
        } else {
            mView.findViewById(R.id.llv_select_tags).setVisibility(View.GONE);
        }
    }

    /**
     * 初始化标签适配器
     */
    private void initTagAdatper() {
        mIdflowlayout.setMaxSelectCount(0);
        mIdflowlayout.setAdapter(new TagAdapter<ApplyPowerTagModel>(mTagData) {
            @Override
            public View getView(FlowLayout parent, int position, ApplyPowerTagModel tag) {
                TextView tv = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.item_apply_power_tag, mIdflowlayout, false);
                tv.setText(tag.tagName);
                tv.setTextColor(getResources().getColor(R.color.white));
                tv.setBackgroundResource(R.drawable.shape_apply_power_tag_select);
                return tv;
            }
        });
    }

    /**
     * 初始化封面上传适配器
     */
    private void initCoverAdapter() {
        GridLayoutManager mCoverLayoutManager = new GridLayoutManager(getContext(), gridCountHor) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        mCoverLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRoomCoverUploadRecyclerView.setLayoutManager(mCoverLayoutManager);
        SubmitFileOrCoverAdapter mSubmitCoverAdapter = new SubmitFileOrCoverAdapter(getContext(), mCoverInfos, ApplyAct.TYPE_SHOW, true);
        mRoomCoverUploadRecyclerView.setAdapter(mSubmitCoverAdapter);
        mSubmitCoverAdapter.setOnItemClickListener(new SubmitFileOrCoverAdapter.OnItemClickListener() {
            @Override
            public void onPhotoClick(int index,ImageView imageView) {
                ((ApplyAct)getActivity()).showPhotoView(mCoverInfos.get(index),imageView);
            }

            @Override
            public void onPhotoDelete(int index, String path) {

            }

            @Override
            public void onUploadStart(int index) {

            }

            @Override
            public void onCancleUpload(int index) {

            }
        });
    }

    private void initFileAdapter(){
        if (mFileInfos.size()==0) {
            mFileUploadLlv.setVisibility(View.GONE);
            return;
        }
        mFileUploadLlv.setVisibility(View.VISIBLE);
        GridLayoutManager mFIleLayoutManager = new GridLayoutManager(getContext(), gridCountHor) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        mFIleLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRoomFileUploadRecyclerView.setLayoutManager(mFIleLayoutManager);
        SubmitFileOrCoverAdapter mSubmitFileAdapter = new SubmitFileOrCoverAdapter(getContext(), mFileInfos, ApplyAct.TYPE_SHOW, false);
        mRoomFileUploadRecyclerView.setAdapter(mSubmitFileAdapter);
        mSubmitFileAdapter.setOnItemClickListener(new SubmitFileOrCoverAdapter.OnItemClickListener() {
            @Override
            public void onPhotoClick(int index,ImageView imageView) {
                // 判断是文件还是图片
                if (mFileInfos.get(index).getGileUrl().endsWith("ppt") || mFileInfos.get(index).getGileUrl().endsWith("pptx")) {
                    FileViewAct.startActivity(getActivity(), "文档", mFileInfos.get(index).getGileUrl(), false, null);
                } else {
                    ((ApplyAct)getActivity()).showPhotoView(mFileInfos.get(index),imageView);
                }
            }

            @Override
            public void onPhotoDelete(int index, String path) {

            }

            @Override
            public void onUploadStart(int index) {

            }

            @Override
            public void onCancleUpload(int index) {

            }
        });
    }

}
