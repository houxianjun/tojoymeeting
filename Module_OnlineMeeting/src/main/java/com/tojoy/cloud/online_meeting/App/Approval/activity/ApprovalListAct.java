package com.tojoy.cloud.online_meeting.App.Approval.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.KeyboardControlUtil;
import com.tojoy.cloud.online_meeting.App.Approval.adapter.ApprovalListAdapter;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApprovalListResponse;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApprovalModel;
import com.tojoy.cloud.online_meeting.App.Approval.view.FliterPopup;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.App.TJBaseApp;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.common.Widgets.CommentInputView.ApplyCheckInputView;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * @author qululu
 * 领导审批列表页面
 */
@Route(path = RouterPathProvider.ONLINE_MEETING_MY_APPROVAL_LIST)
public class ApprovalListAct extends UI {
    private TJRecyclerView mRecyclerView;
    private ApprovalListAdapter mAdapter;
    private FliterPopup fliterPopup;
    private RelativeLayout mTitleRlv;
    private TextView mArrowDownAllTv;
    private RelativeLayout mArrowDownAllRlv;

    private List<ApprovalModel> mDatas = new ArrayList<>();

    ApplyCheckInputView mInputView;
    private ApprovalModel checkData;
    /**
     * 全部""；未处理0；已处理1;已撤回2
     */
    private String mStatus = "";

    private boolean isRefreshing = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval);
        setStatusBar();
        initUI();
        initTitle();
        updateReadStatus();
    }

    private void initUI() {
        mRecyclerView = (TJRecyclerView) findViewById(R.id.recycleview);
        mTitleRlv = (RelativeLayout) findViewById(R.id.rlv_title);
        mArrowDownAllTv = (TextView) findViewById(R.id.tv_arrow_down_all);
        mArrowDownAllRlv = (RelativeLayout) findViewById(R.id.rlv_arrow_down_all);
        mAdapter = new ApprovalListAdapter(this, mDatas);
        mRecyclerView.setLoadMoreView();
        mRecyclerView.goneFooterView();
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.autoRefresh();
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                mDatas.clear();
                BaseUserInfoCache.saveOnlineMeetingCheck(TJBaseApp.getInstance(),false);
                queryLiveApplyCheck(mStatus);
                isRefreshing = true;
            }

            @Override
            public void onLoadMore() {
                queryLiveApplyCheck(mStatus);
            }
        });

        fliterPopup = new FliterPopup(ApprovalListAct.this, (status, title) -> {
            mArrowDownAllTv.setText(title);
            mPage = 1;
            mDatas.clear();
            queryLiveApplyCheck(status);
        });

        mInputView = new ApplyCheckInputView(ApprovalListAct.this, KeyboardControlUtil.getKeyboardHeight(ApprovalListAct.this));
        mInputView.add2Window(ApprovalListAct.this);
        mInputView.setOnHeightReceivedListener((screenOri, height) -> KeyboardControlUtil.setKeyboardHeight(ApprovalListAct.this, height));
        mInputView.setOnSendClickListener((commentContent) -> {
            if (checkData == null) {
                return;
            }
            if (TextUtils.isEmpty(commentContent)) {
                Toasty.normal(ApprovalListAct.this, "驳回理由不能为空").show();
                return;
            }
            applyCheck(checkData.checkId, checkData.applyId, "2", commentContent);
        });
    }

    private void initTitle() {
        int statusBarHeight = AppUtils.getStatusBarHeight(this);
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.dip2px(this, 48));
        lps.setMargins(0, statusBarHeight, 0, 0);
        mTitleRlv.setLayoutParams(lps);

        ((TextView)findViewById(R.id.tv_cetener_title)).setText("审批");
        findViewById(R.id.iv_arrow_left).setOnClickListener(v -> {

            finish();
        });

        mArrowDownAllRlv.setOnClickListener(v -> {
            // 展示筛选弹窗
            fliterPopup.showAsDropDown(mArrowDownAllTv, -AppUtils.dip2px(this, 35), AppUtils.dip2px(this, 10));
            fliterPopup.lightOff();
        });
    }

    /**
     * 查询权限处理列表
     */
    private void queryLiveApplyCheck(String status) {
        mStatus = status;
        OMAppApiProvider.getInstance().mQueryLiveApplyCheck(new Observer<ApprovalListResponse>() {
            @Override
            public void onCompleted() {
                if (mDatas.size()== 0){
                    mRecyclerView.setEmptyImgAndTip(R.drawable.icon_empty_online_logo, "暂无内容");
                    mRecyclerView.refreshLoadMoreView(mPage, mDatas.size());
                }
                mAdapter.updateData(mDatas);
                mRecyclerView.setRefreshing(false);
                isRefreshing = false;
            }

            @Override
            public void onError(Throwable e) {
                if (mDatas.size() == 0) {
                    mRecyclerView.setEmptyImgAndTip(R.drawable.icon_empty_online_logo, "暂无内容");
                    mRecyclerView.refreshLoadMoreView(mPage, mDatas.size());
                }
                mAdapter.updateData(mDatas);
                mRecyclerView.setRefreshing(false);
                isRefreshing = false;
                Toasty.normal(ApprovalListAct.this, "网络异常，稍后再试！").show();
            }

            @Override
            public void onNext(ApprovalListResponse baseResponse) {
                if (baseResponse.isSuccess() && baseResponse.data != null && baseResponse.data.list != null ) {
                    mDatas.addAll(baseResponse.data.list);
                    mPage++;
                    if (baseResponse.data.isLastPage) {
                        mRecyclerView.refreshLoadMoreView(mPage, 0);
                    } else {
                        mRecyclerView.refreshLoadMoreView(mPage, mDatas.size());
                    }
                } else {
                    if (!baseResponse.isSuccess()) {
                        mDatas.clear();
                        Toasty.normal(ApprovalListAct.this, baseResponse.msg).show();
                    }
                }
            }
        }, status,String.valueOf(mPage));
    }

    /**
     * 展示处理弹窗，输入处理理由
     */
    public void showApplyCheckRemarkDialog(ApprovalModel data) {
        if (isRefreshing) {
            return;
        }
        checkData = data;
        mInputView.show(KeyboardControlUtil.getKeyboardHeight(ApprovalListAct.this));
    }

    /**
     * 审核开播请求（同意、驳回）
     */
    public void applyCheck(String checkId, String applyId, String status, String remark) {
        if (isRefreshing) {
            return;
        }
        isRefreshing = true;
        showProgressHUD(this,"正在提交");
        OMAppApiProvider.getInstance().mApplyCheck(new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {
                dismissProgressHUD();
                isRefreshing = false;
                Toasty.normal(ApprovalListAct.this, "网络异常，稍后再试！").show();
            }

            @Override
            public void onNext(OMBaseResponse baseResponse) {
                if (baseResponse.isSuccess()) {
                    mHandler.postDelayed(() -> mRecyclerView.autoRefresh(),500);
                } else {
                    isRefreshing = false;
                    Toasty.normal(ApprovalListAct.this, baseResponse.msg).show();
                }
            }
        }, checkId, applyId, status, remark);
    }

    /**
     * 通知后台已读消息
     */
    private void updateReadStatus() {
        OMAppApiProvider.getInstance().updateReadStatus(new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(OMBaseResponse omBaseResponse) {

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mInputView.isShow()) {
            mInputView.dismiss();
        }
    }
}
