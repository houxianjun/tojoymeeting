package com.tojoy.cloud.online_meeting.App.Live.ExporeOnline;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.live.ApplyGuestInfo;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.imageManager.YuanJiaoImageView;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigCacheManager;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigConstantKey;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.time.DateTimeUtil;
import com.tojoy.cloud.online_meeting.App.Config.LiveRoomConfig;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.OnlineMembersHelper;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class ApplyGuestAdapter extends BaseRecyclerViewAdapter<ApplyGuestInfo> {

    ApplyGuestAdapter(@NonNull Context context,
                      @NonNull List<ApplyGuestInfo> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull ApplyGuestInfo data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return 0;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new ApplyGuestVH(context, parent);
    }

    class ApplyGuestVH extends BaseRecyclerViewHolder<ApplyGuestInfo> {
        Context mContext;
        YuanJiaoImageView avatar;
        TextView nick;
        TextView jobLevel;
        TextView time;
        //未操作
        TextView mApplyContent;
        RelativeLayout mApplyAgreemApplyAgree;
        RelativeLayout mApplyRefuse;
        //已操作
        TextView mApplyContent2;
        //状态
        ImageView mApplyStatusImg;

        ApplyGuestVH(Context context, ViewGroup parent) {
            super(context, parent, R.layout.item_applyguest_view);
            this.mContext = context;
            avatar = (YuanJiaoImageView) findViewById(R.id.head_image_s);
            nick = (TextView) findViewById(R.id.tv_nickname);
            jobLevel = (TextView) findViewById(R.id.tv_jobLevel);
            time = (TextView) findViewById(R.id.tv_time);

            mApplyContent = (TextView) findViewById(R.id.tv_apply);
            mApplyAgreemApplyAgree = (RelativeLayout) findViewById(R.id.rlv_green);
            mApplyRefuse = (RelativeLayout) findViewById(R.id.rlv_refuse);

            mApplyContent2 = (TextView) findViewById(R.id.tv_result);
            mApplyStatusImg = (ImageView) findViewById(R.id.iv_agree_status);
        }

        @Override
        public void onBindData(ApplyGuestInfo data, int position) {
            ImageLoaderManager.INSTANCE.loadHeadImage(mContext, avatar, OSSConfig.getOSSURLedStr(data.getAvatar()),
                    R.drawable.icon_dorecord);

            avatar.setVisibility(View.VISIBLE);
            nick.setText(data.getTrueName());
            jobLevel.setText(data.getCompany());
            time.setText(DateTimeUtil.getFormatCurrentTime(data.postTime, DateTimeUtil.PATTERN_CURRENT_TIME));
            String title = TextUtils.isEmpty(TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1)) ? "同台互动" :
                    TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1);
            //发起同台互动，是否同意
            mApplyContent.setText(TextUtils.isEmpty(title) ? "发起同台互动，是否同意" : "发起"+title+"，是否同意");
            mApplyContent2.setText(TextUtils.isEmpty(title) ? "发起同台互动，是否同意" : "发起"+title+"，是否同意");

            // 0:未处理；1接受；-1：拒绝
            if (data.oprationStatus == 0) {
                mApplyContent2.setVisibility(View.GONE);
                mApplyStatusImg.setVisibility(View.GONE);

                mApplyContent.setVisibility(View.VISIBLE);
                mApplyAgreemApplyAgree.setVisibility(View.VISIBLE);
                mApplyRefuse.setVisibility(View.VISIBLE);
            } else {
                mApplyContent2.setVisibility(View.VISIBLE);
                mApplyStatusImg.setVisibility(View.VISIBLE);

                mApplyContent.setVisibility(View.GONE);
                mApplyAgreemApplyAgree.setVisibility(View.GONE);
                mApplyRefuse.setVisibility(View.GONE);

                if (data.oprationStatus == 1 || data.oprationStatus == 3) {
                    mApplyStatusImg.setBackgroundResource(R.drawable.icon_applyonline_agreeed);
                } else {
                    mApplyStatusImg.setBackgroundResource(R.drawable.icon_applyonline_refresud);
                }
            }

            mApplyAgreemApplyAgree.setOnClickListener(v -> {
                if (OnlineMembersHelper.getInstance().mOnlineMembers.size() == LiveRoomConfig.ROOM_MAX_USER - 1) {
                    Toasty.normal(mContext, "数量已达上线，请稍后重试").show();
                    return;
                }
                mApplyOperationCallback.onAgreen(position, data.applyForId, data.userId);
            });
            mApplyRefuse.setOnClickListener(v -> mApplyOperationCallback.onRefuse(position, data.applyForId, data.userId));
        }
    }

    private ApplyOperationCallback mApplyOperationCallback;

    void setmApplyOperationCallback(ApplyOperationCallback mApplyOperationCallback) {
        this.mApplyOperationCallback = mApplyOperationCallback;
    }
}
