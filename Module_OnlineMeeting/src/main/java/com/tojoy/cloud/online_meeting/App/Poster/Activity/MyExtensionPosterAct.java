package com.tojoy.cloud.online_meeting.App.Poster.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tojoy.cloud.online_meeting.App.Poster.View.ThumbnailPosterView;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.imageManager.YuanJiaoImageView;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.util.QR.QRCodeUtil;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.MathematicalUtils;

/**
 * 我的推广码页面
 * @author qll
 */
@Route(path = RouterPathProvider.OnlineMeetingMyExtenSionPoster)
public class MyExtensionPosterAct extends UI {
    /**
     * 个人推广码
     */
    private RelativeLayout mMyExtensionPosterDetailRlv;
    private YuanJiaoImageView mMyExtensionHeadIv;
    private TextView mMyExtensionUserNameTv;
    private RelativeLayout mMyExtensionPosterBottomRlv;
    private ImageView mMyExtensionPosterQrIV;
    private TextView mMyExtensionPosterQrHintTv;

    /**
     * 公共使用
     */
    private ImageView mRootBgIv;
    private ImageView mLogoIv;
    private RelativeLayout flv_root;
    private LinearLayout mPosterBigLlv;
    private FrameLayout mThumbnailFlv;
    private RelativeLayout mShareMeetingRlv;

    private ThumbnailPosterView thumbnailPosterView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_extension_poster);
        setStatusBar();
        initUI();
        initTitle();
        initListener();
    }

    private void initUI() {
        mMyExtensionPosterDetailRlv = (RelativeLayout) findViewById(R.id.rlv_my_poster_detail);
        mMyExtensionHeadIv = (YuanJiaoImageView) findViewById(R.id.iv_my_extension_head);
        mMyExtensionUserNameTv = (TextView) findViewById(R.id.tv_my_extension_user_name);
        mMyExtensionPosterBottomRlv = (RelativeLayout) findViewById(R.id.rlv_my_extension_poster_bottom);
        mMyExtensionPosterQrIV = (ImageView) findViewById(R.id.iv_my_extension_poster_qr);
        mMyExtensionPosterQrHintTv = (TextView) findViewById(R.id.tv_my_extension_qr_hint);
        mPosterBigLlv = (LinearLayout) findViewById(R.id.llv_poster_big);
        mRootBgIv = (ImageView) findViewById(R.id.iv_root_bg);
        mLogoIv = (ImageView) findViewById(R.id.iv_logo);
        flv_root = (RelativeLayout) findViewById(R.id.flv_root);
        mShareMeetingRlv = (RelativeLayout) findViewById(R.id.rlv_share);
        mThumbnailFlv = (FrameLayout) findViewById(R.id.flv_thumbnail);
        thumbnailPosterView = new ThumbnailPosterView(this);
        mThumbnailFlv.addView(thumbnailPosterView.getView());

        thumbnailPosterView.goneThumbnailPoster();
        mShareMeetingRlv.setBackgroundResource(R.drawable.shape_meeting_extension_poster_bg_blue);

        // 设计图尺寸（375 * 812），实际机型整体按照高度的比例进行缩放
        double hScale = MathematicalUtils.div(AppUtils.getHeight(this), AppUtils.dip2px(this, 812), 2);
        int mBigW = (int) (MathematicalUtils.div(300, 375, 2) * AppUtils.getWidth(this) * hScale);
        int marginTop = (int) (AppUtils.dip2px(this, 100) * hScale + AppUtils.getStatusBarHeight(this));

        RelativeLayout.LayoutParams mBigPosterParams = (RelativeLayout.LayoutParams) mPosterBigLlv.getLayoutParams();
        mBigPosterParams.width = mBigW;
        mBigPosterParams.setMargins(0, marginTop, 0, 0);
        mPosterBigLlv.setLayoutParams(mBigPosterParams);

        LinearLayout.LayoutParams logoParams = (LinearLayout.LayoutParams) mLogoIv.getLayoutParams();
        logoParams.setMargins(0, 0, 0, (int) (AppUtils.dip2px(this, 50) * hScale));
        mLogoIv.setLayoutParams(logoParams);

        // 我的推广码
        mLogoIv.setImageResource(R.drawable.icon_my_extension_poster_logo);
        mMyExtensionPosterDetailRlv.setVisibility(View.VISIBLE);

        LinearLayout.LayoutParams myExtensionPosterDetailRlvParams = (LinearLayout.LayoutParams) mMyExtensionPosterDetailRlv.getLayoutParams();
        myExtensionPosterDetailRlvParams.width = mBigW;
        myExtensionPosterDetailRlvParams.height = (int) (MathematicalUtils.div(335, 296, 2) * mBigW);
        mMyExtensionPosterDetailRlv.setLayoutParams(myExtensionPosterDetailRlvParams);

        RelativeLayout.LayoutParams myHeadIvParams = (RelativeLayout.LayoutParams) mMyExtensionHeadIv.getLayoutParams();
        myHeadIvParams.width = (int) (AppUtils.dip2px(this, 85) * hScale);
        myHeadIvParams.height = (int) (AppUtils.dip2px(this, 85) * hScale);
        myHeadIvParams.setMargins(0, (int) (AppUtils.dip2px(this, 36.5f) * hScale), 0, (int) (AppUtils.dip2px(this, 8) * hScale));
        mMyExtensionHeadIv.setLayoutParams(myHeadIvParams);
        mMyExtensionHeadIv.setType(1);
        mMyExtensionHeadIv.setBorderRadius((int) (85 * hScale));
        mMyExtensionHeadIv.setWidthHeight((int) (85 * hScale), (int) (85 * hScale));
        ImageLoaderManager.INSTANCE.loadHeadImage(this, mMyExtensionHeadIv, BaseUserInfoCache.getUserHeaderPic(this), R.drawable.icon_dorecord);
        mMyExtensionUserNameTv.setTextSize((float) (21.5f * hScale));
        mMyExtensionUserNameTv.setText(BaseUserInfoCache.getUserName(this));

        RelativeLayout.LayoutParams myExtensionPosterBottomRlvParams = (RelativeLayout.LayoutParams) mMyExtensionPosterBottomRlv.getLayoutParams();
        myExtensionPosterBottomRlvParams.height = (int) (MathematicalUtils.div(335, 296, 2) * mBigW) / 3;
        myExtensionPosterBottomRlvParams.setMargins((int) (AppUtils.dip2px(this, 20) * hScale), 0, (int) (AppUtils.dip2px(this, 20) * hScale), 0);
        mMyExtensionPosterBottomRlv.setLayoutParams(myExtensionPosterBottomRlvParams);

        RelativeLayout.LayoutParams posterQrIvParams = (RelativeLayout.LayoutParams) mMyExtensionPosterQrIV.getLayoutParams();
        posterQrIvParams.width = (int) (AppUtils.dip2px(this, 65) * hScale);
        posterQrIvParams.height = (int) (AppUtils.dip2px(this, 65) * hScale);
        posterQrIvParams.setMargins(0, 0, (int) (AppUtils.dip2px(this, 10) * hScale), 0);
        mMyExtensionPosterQrIV.setLayoutParams(posterQrIvParams);
        mMyExtensionPosterQrIV.setImageBitmap(QRCodeUtil.createQRCode(BaseUserInfoCache.getUserInviteUrl(this), (int) (AppUtils.dip2px(this, 65) * hScale)));

        mMyExtensionPosterQrHintTv.setTextSize((float) (14 * hScale));

        RelativeLayout.LayoutParams mShareMeetingParams = (RelativeLayout.LayoutParams) mShareMeetingRlv.getLayoutParams();
        mShareMeetingParams.setMargins(0, 0, 0, (int) (AppUtils.dip2px(this, 70) * hScale));
        mShareMeetingRlv.setLayoutParams(mShareMeetingParams);

    }

    private void initTitle() {
        setUpNavigationBar();
        setAlphaBackground();
        mRootBgIv.setImageResource(R.drawable.my_extension_code_poster_bg);
        showBack();
    }

    private void initListener() {
        mShareMeetingRlv.setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                thumbnailPosterView.showThumbnailPoster(flv_root);
            }
        });
    }
}
