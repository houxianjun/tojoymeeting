package com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.utils.TextUtils;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.model.home.MineDistributionSignStatusResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.string.RegixUtil;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 提现信息录入
 */
public class DistributionInformationEntryFragment extends Fragment {

    private String cardNo = "";
    private String idCard = "";
    private String mobile = "";
    private String name = "";

    private LinearLayout mMainSignInfoLlv;
    private TextView mInformationCommitTv;
    private EditText idcardEt;
    private EditText trueNameEt;
    private TextView idcardEffectiveTv;
    private TextView usernameEffectiveTv;
    private TextView userPhoneContentTv;
    //页面来源
    private static String mFrompage = "";

    private int signStatus;

    private MineDistributionSignStatusResponse.DistributionSignStatus distributionSignStatus;

    public DistributionInformationEntryFragment() {

    }

    public static DistributionInformationEntryFragment newInstance(String fromPage) {
        mFrompage = fromPage;
        return new DistributionInformationEntryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_distribution_information_entry, container, false);
        initUI(layout);
        return layout;
    }

    /**
     * 初始化UI组件
     *
     * @param layout
     */
    private void initUI(View layout) {
        mMainSignInfoLlv = layout.findViewById(R.id.ll_main_sign_info);
        mInformationCommitTv = layout.findViewById(R.id.tv_information_commit);
        mInformationCommitTv.setOnClickListener(v -> {
            //连续点击限制
            if (!FastClickAvoidUtil.isFastDoubleClick(mInformationCommitTv.getId())) {
                idCard = idcardEt.getText().toString().toLowerCase();
                name = trueNameEt.getText().toString();
                mobile = BaseUserInfoCache.getUserMobile(getActivity());
                usernameEffectiveTv.setVisibility(View.GONE);
                idcardEffectiveTv.setVisibility(View.GONE);
                if (!RegixUtil.isChineseName(name)) {
                    usernameEffectiveTv.setVisibility(View.VISIBLE);
                    return;
                } else {
                    usernameEffectiveTv.setVisibility(View.GONE);
                }

                //身份证号格式错误
                if (!RegixUtil.idCardEffective(idCard)) {
                    idcardEffectiveTv.setVisibility(View.VISIBLE);
                    return;
                } else {
                    idcardEffectiveTv.setVisibility(View.GONE);
                }

                querytDistributionWithdrawSign(cardNo, idCard, mobile, name);
            }
        });
        usernameEffectiveTv = layout.findViewById(R.id.tv_username_effective);
        idcardEffectiveTv = layout.findViewById(R.id.tv_idcard_effective);
        idcardEt = layout.findViewById(R.id.ed_idcard);
        idcardEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //内容为空隐藏错误提示信息
                String idcard = idcardEt.getText().toString().trim();
                if (idcard.equals("") || TextUtils.isEmpty(idcard)) {
                    idcardEffectiveTv.setVisibility(View.GONE);
                }
            }
        });
        trueNameEt = layout.findViewById(R.id.et_user_true_name);
        trueNameEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //内容为空隐藏错误提示信息
                String name = trueNameEt.getText().toString().trim();
                if (TextUtils.isEmpty(name)) {
                    usernameEffectiveTv.setVisibility(View.GONE);
                }
            }
        });
        userPhoneContentTv = layout.findViewById(R.id.tv_user_true_phone_content);
    }


    public void querytDistributionWithdrawSignStatus() {
        OMAppApiProvider.getInstance().querytDistributionWithdrawSignStatus(new Observer<MineDistributionSignStatusResponse>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(getActivity(), "网络错误，请检查网络").show();

            }

            @Override
            public void onNext(MineDistributionSignStatusResponse response) {
                if (response.isSuccess()) {
                    distributionSignStatus = response.data;
                    signStatus = distributionSignStatus.status;
                    initData();
                } else {
                    Toasty.normal(getActivity(), response.msg).show();
                }
            }
        });

    }

    public void initData() {
        if (signStatus == 0) {
            // 0:未签约
            idcardEt.setText("");
            trueNameEt.setText("");
            userPhoneContentTv.setText(BaseUserInfoCache.getUserMobile(getActivity()));
            mInformationCommitTv.setVisibility(View.VISIBLE);
        } else {
            // 1:签约中;2:已签约;3:签约失败
            idcardEt.setEnabled(false);
            idcardEt.setFocusable(false);
            trueNameEt.setEnabled(false);
            trueNameEt.setFocusable(false);
            idcardEt.setText(distributionSignStatus.idCardNo);
            trueNameEt.setText(distributionSignStatus.name);
            userPhoneContentTv.setText(distributionSignStatus.mobile);
            mInformationCommitTv.setVisibility(View.GONE);
        }
        mMainSignInfoLlv.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mFrompage.equals("homepage")) {
            //查询签约状态
            querytDistributionWithdrawSignStatus();
        } else {
            signStatus = 0;
            initData();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public void querytDistributionWithdrawSign(String cardNo, String idCard, String mobile, String name) {
        OMAppApiProvider.getInstance().querytDistributionWithdrawSign(cardNo, idCard, mobile, name, new Observer<OMBaseResponse>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(getActivity(), "网络错误，请检查网络").show();
            }

            @Override
            public void onNext(OMBaseResponse response) {
                Toasty.normal(getActivity(), response.msg).show();
                if (response.isSuccess()) {
                    getActivity().finish();
                }
            }
        });
    }
}
