package com.tojoy.cloud.online_meeting.App.Meeting.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.bumptech.glide.Glide;
import com.tojoy.common.Services.Distribution.DistrbutionHelper;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.live.OnlineMeetingHomeGridModel;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.common.LiveRoom.MeetingExtensionPopView;
import com.tojoy.cloud.online_meeting.App.Home.view.VideoPlayAnimationView;
import com.tojoy.cloud.online_meeting.App.Meeting.view.FlipperImageView;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * @author fanxi
 * @date 2020-02-25.
 * description：
 */
public class MeetingCenterAdapter extends BaseRecyclerViewAdapter<OnlineMeetingHomeGridModel> {
    public MeetingCenterAdapter(@NonNull Context context, @NonNull List<OnlineMeetingHomeGridModel> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull OnlineMeetingHomeGridModel data) {
        return position;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_center_meeting_layout;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new MeetingCenterListVH(context, parent, getLayoutResId(viewType));
    }

    public class MeetingCenterListVH extends BaseRecyclerViewHolder<OnlineMeetingHomeGridModel> {
        private TextView mRoomIdTv;
        private TextView mTitleTv;
        private RelativeLayout mRlvAnimContainer;
        private FlipperImageView mFlipperImageView;
        private TextView mTvStatus;
        private LinearLayout llBgGifview;
        private ImageView mIvLive;
        private LinearLayout mDistributionLlv;
        private TextView mDistributionMoneyTv;

        public MeetingCenterListVH(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            mTitleTv = itemView.findViewById(R.id.tv_title);
            mRlvAnimContainer = itemView.findViewById(R.id.rlv_anim_container);
            mRoomIdTv = itemView.findViewById(R.id.tv_room_id);
            VideoPlayAnimationView videoPlayAnimationView = new VideoPlayAnimationView((Activity) context);
            mRlvAnimContainer.addView(videoPlayAnimationView.getView());
            mFlipperImageView = itemView.findViewById(R.id.wyc_view);
            mTvStatus = itemView.findViewById(R.id.tv_live_status_already);
            mIvLive = itemView.findViewById(R.id.iv_live_play_already);
            RelativeLayout mCoverLayout = itemView.findViewById(R.id.rlv_top);
            llBgGifview = itemView.findViewById(R.id.ll_bg_gifview);
            int width = (AppUtils.getWidth(context) - AppUtils.dip2px(context, 32)) / 2;
            mCoverLayout.setLayoutParams(new RelativeLayout.LayoutParams(width, width));
            mDistributionLlv = itemView.findViewById(R.id.llv_distribution);
            mDistributionMoneyTv = itemView.findViewById(R.id.tv_distribution_money);
        }

        @Override
        public void onBindData(OnlineMeetingHomeGridModel data, int position) {
            /**
             * 初始化循环播放图片控件：VideoPlayAnimationView
             */
            initWycView(data);
            mTitleTv.setText(data.title);
            if (TextUtils.isEmpty(data.roomCode)) {
                mRoomIdTv.setVisibility(View.GONE);
            } else {
                mRoomIdTv.setVisibility(View.VISIBLE);
                mRoomIdTv.setText(data.getListRoomIdText());
            }
            mTvStatus.setVisibility(View.GONE);
            mIvLive.setVisibility(View.GONE);
            mRlvAnimContainer.setVisibility(View.GONE);
            llBgGifview.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(data.status)) {
                // 根据直播状态显示不同标签
                switch (data.status) {
                    case "1":
                        // 预告
                        break;
                    case "2":
                    case "3":
                        llBgGifview.setVisibility(View.VISIBLE);
                        // 直播
                        mIvLive.setVisibility(View.VISIBLE);
                        Glide.with(context).asGif().load(R.drawable.animvideo_player).into(mIvLive);
                        break;
                    case "4":
                        // 回放
                        mTvStatus.setVisibility(View.VISIBLE);
                        break;
                    default:
                        break;
                }
            }

            if (DistrbutionHelper.isShowDistribution(context,data.ifDistribution,data.companyCode)) {
                mDistributionLlv.setVisibility(View.VISIBLE);
                mDistributionMoneyTv.setText(DistrbutionHelper.getMaxBudgetText(context
                        ,data.level1Commission
                        ,data.level2Commission
                        ,data.level1CommissionEmployee
                        ,data.level2CommissionEmployee
                        ,data.companyCode,true));
                mTitleTv.setPadding(AppUtils.dip2px(context,3),AppUtils.dip2px(context,8),
                        AppUtils.dip2px(context,3),AppUtils.dip2px(context,18));
            } else {
                mDistributionLlv.setVisibility(View.GONE);
                mTitleTv.setPadding(AppUtils.dip2px(context,3),AppUtils.dip2px(context,8),
                        AppUtils.dip2px(context,3),AppUtils.dip2px(context,4));
            }

            mDistributionLlv.setOnClickListener(v -> {
                if (!FastClickAvoidUtil.isDoubleClick()) {
                    if (DistrbutionHelper.isNeedJumpPop(context,data.level1CommissionEmployee,data.level2CommissionEmployee,data.companyCode)) {
                        MeetingExtensionPopView popView = new MeetingExtensionPopView(context);
                        popView.setMeetingData(data.roomId
                                ,data.roomLiveId
                                ,data.title
                                ,!TextUtils.isEmpty(data.verticalBanner) ? data.verticalBanner:data.transverseBanner
                                ,DistrbutionHelper.getDistributionMoney(context
                                        ,data.level1Commission
                                        ,data.level2Commission
                                        ,data.level1CommissionEmployee
                                        ,data.level2CommissionEmployee
                                        ,data.companyCode)
                                ,data.minPerson
                                ,data.minViewTime);
                        popView.show();
                    } else {
                        // 判断二维码内容是否为空，为空生成海报没有意义
                        if (!TextUtils.isEmpty(BaseUserInfoCache.getUserInviteUrl(context))) {
                            ARouter.getInstance()
                                    .build(RouterPathProvider.ONLINE_MEETING_EXTEN_SION_POSTER)
                                    .withInt("fromType", 1)
                                    .withString("mLiveTitle", data.title)
                                    .withString("mLiveId", data.roomLiveId)
                                    .withString("mLiveCoverUrl", !TextUtils.isEmpty(data.verticalBanner) ? data.verticalBanner:data.transverseBanner)
                                    .withString("mRoomId", data.roomId)
                                    .navigation();
                        } else {
                            Toasty.normal(context, context.getResources().getString(R.string.extension_qr_error)).show();
                        }
                    }
                }
            });
        }

        /**
         * 初始化循环播放图片控件：VideoPlayAnimationView
         */
        private void initWycView(OnlineMeetingHomeGridModel data) {
            mFlipperImageView.setFirstImgUrl(!TextUtils.isEmpty(data.verticalBanner) ? data.verticalBanner:data.transverseBanner , R.drawable.icon_online_meeting_home_grid_default, 4);
        }
    }
}
