package com.tojoy.cloud.online_meeting.App.Live.Interaction;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;
import com.tojoy.cloud.online_meeting.R;

/**
 * Created by luuzhu on 2019/7/19.
 * 结束直播
 */

public class StopLiveDialog {

    private String mType;
    private Dialog mDialog;
    private BaseLiveAct mContext;
    private View mView;

    private TextView mTvShortLeave;

    /**
     * @param context
     * @param type    1:主播  2非主播  3同台
     */
    public StopLiveDialog(Context context, String type) {
        mContext = (BaseLiveAct) context;
        this.mType = type;
        initUI();
    }

    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_stop_live, null);
        initView();
    }

    public void show() {

        mDialog = new Dialog(mContext, R.style.LiveDialogStopLive);

        ViewGroup parent = (ViewGroup) mView.getParent();
        if (parent != null) {
            parent.removeAllViews();
        }
        mDialog.setContentView(mView);

        Window win = mDialog.getWindow();
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.windowAnimations = R.style.CenterInAndOutStyle;
        lp.gravity = Gravity.CENTER;
        win.setAttributes(lp);

        mDialog.setCanceledOnTouchOutside(true);
        // 刷新暂离会议按钮状态，是否要显示根据当前暂离状态决定
        if (mTvShortLeave != null) {
            if (!LiveRoomInfoProvider.getInstance().isHost()) {
                mTvShortLeave.setVisibility(View.GONE);
            } else {
                mTvShortLeave.setVisibility(LiveRoomInfoProvider.getInstance().hasTemplateLeave ? View.GONE : View.VISIBLE);
            }
        }

        try {
            mDialog.show();
            mDialog.setOnKeyListener((dialog, keyCode, event) -> true);
        } catch (Exception e) {

        }
    }

    private void initView() {

        TextView mTvGoOn = mView.findViewById(R.id.tvGoOn);
        TextView mTvStop = mView.findViewById(R.id.tvStop);
        mTvShortLeave = mView.findViewById(R.id.tvShortLeave);
        TextView mTvTip = mView.findViewById(R.id.tvTip);

        if ("1".equals(mType)) {
            mTvTip.setVisibility(View.GONE);
            mTvShortLeave.setVisibility(LiveRoomInfoProvider.getInstance().hasTemplateLeave ? View.GONE : View.VISIBLE);

            try {
                mTvStop.setText("关闭会议");
                mTvGoOn.setText("继续开会");
            } catch (Exception e) {

            }

            if (mTvShortLeave.getVisibility() == View.GONE) {
                mTvGoOn.setText("取消");
            }
        } else if ("2".equals(mType)) {
            mTvTip.setText("真的要离开吗？");
            mTvStop.setText("确认离开");
            mTvGoOn.setText("继续观看");
            mTvTip.setVisibility(View.VISIBLE);

            if (LiveRoomInfoProvider.getInstance().isHost() && LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                mTvShortLeave.setVisibility(View.VISIBLE);
            } else {
                mTvShortLeave.setVisibility(View.GONE);
            }


        } else if ("3".equals(mType)) {
            mTvShortLeave.setVisibility(View.GONE);
            mTvStop.setText("确认离开");
            mTvGoOn.setText("继续观看");
            mTvTip.setVisibility(View.VISIBLE);

            String liveUnion = "同台互动";
            String liveRoom = "直播间";
            String title = "确定结束" + liveUnion + "并离开" + liveRoom + "？";

            mTvTip.setText(title);
            mTvStop.setText("确定");
            mTvGoOn.setText("取消");
        }

        mTvGoOn.setOnClickListener(view -> dismiss());
        mTvStop.setOnClickListener(view -> {
            dismiss();
            if ("1".equals(mType)) {
                mContext.stopLiveOperation(true);
            } else {
                mContext.exitRoom(-1);
            }
        });
        mTvShortLeave.setOnClickListener(view -> {
            LogUtil.i(LiveLogTag.IOLiveTag, "click_shortLeave");
            dismiss();
            mContext.onClickTempleteLeave();
        });
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener listener) {
        mDialog.setOnDismissListener(listener);
    }

    public boolean isShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }
}
