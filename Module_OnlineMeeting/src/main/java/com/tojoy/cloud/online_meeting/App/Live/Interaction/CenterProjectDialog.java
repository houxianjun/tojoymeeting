package com.tojoy.cloud.online_meeting.App.Live.Interaction;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.netease.nim.uikit.R;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishProjectAttachment;
import com.netease.nim.uikit.business.liveroom.LiveRoomHelper;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigCacheManager;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigConstantKey;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.net.NetworkUtil;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.App.Controller.LRUEController;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * Created by luuzhu on 2019/4/24.
 * 直播间发项目的中间弹窗
 */

public class CenterProjectDialog {

    private Activity mContext;
    private Dialog mDialog;
    private View mView;
    private ViewPager mViewPager;
    public ArrayList<CRMsgLivePublishProjectAttachment> mDatas;
    public PagerAdapter mAdapter;
    private int mCurPosition;

    public CenterProjectDialog(Context context, CRMsgLivePublishProjectAttachment data) {
        mContext = (Activity) context;
        mDatas = new ArrayList<>();
        mDatas.add(data);
        initUI();
    }

    @SuppressLint("WrongConstant")
    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_live_project_center, null);
        initView();
    }

    public boolean isShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

    private void initView() {

        mViewPager = mView.findViewById(R.id.viewPager);
        mViewPager.setAdapter(createAdapter());
        mViewPager.setPageMargin(AppUtils.dip2px(mContext, 0));
        mViewPager.setOffscreenPageLimit(3);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mCurPosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /**
     * @param isDelete 是不是删除数据
     */
    public void updata(boolean isDelete, CRMsgLivePublishProjectAttachment data) {
        if (isDelete) {
            if (mDatas.size() == 0) {
                mDialog.dismiss();
            } else {
                mViewPager.setAdapter(createAdapter());
            }
        } else {
            boolean isContains = false;
            for (int i = 0; i < mDatas.size(); i++) {
                if (data.getProjectId().equals(mDatas.get(i).getProjectId())) {
                    isContains = true;
                }
            }
            if (!isContains) {
                mDatas.add(data);
                mViewPager.setAdapter(createAdapter());
            }
        }

        int size = mDatas.size();

        if (size > mCurPosition) {
            mViewPager.setCurrentItem(mCurPosition);
        } else if (size == mCurPosition && size > 0) {
            mViewPager.setCurrentItem(size - 1);
        }

    }

    public void refresh() {
        mViewPager.setAdapter(createAdapter());
        mViewPager.setCurrentItem(mCurPosition);
    }

    private PagerAdapter createAdapter() {

        mAdapter = new PagerAdapter() {
            @Override
            public int getCount() {
                return mDatas == null ? 0 : mDatas.size();
            }

            @Override
            public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
                return view == object;
            }

            @NonNull
            @Override
            public Object instantiateItem(@NonNull ViewGroup container, int position) {


                View itemPagerView = View.inflate(mContext, R.layout.item_vp_live_center_project, null);
                CRMsgLivePublishProjectAttachment attachment = mDatas.get(position);
                ImageView ivCover = itemPagerView.findViewById(R.id.ivCover);
                TextView tvInterested = itemPagerView.findViewById(R.id.tvInterested);
                TextView tvWantBuy = itemPagerView.findViewById(R.id.tvWantBuy);
                View ueLayout = itemPagerView.findViewById(R.id.doUELayout);
                View ivClose = itemPagerView.findViewById(R.id.ivClose);

                if ("1".equals(attachment.getIsInterest())) {
                    tvInterested.setText("已提交");
                } else {
                    String title = TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.PRODUCT1);
                    tvInterested.setText(TextUtils.isEmpty(title) ? "我感兴趣" : title);
                }

                String wantBuytitle = TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.PRODUCT3);
                String title = TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.PRODUCT2);

                if (TextUtils.isEmpty(attachment.getStoreId())) {
                    tvWantBuy.setText(TextUtils.isEmpty(title) ? "意向购买" : title);
                } else {
                    if (attachment.getIsDeposit() == 1) {
                        // 支付定金
                        tvWantBuy.setText("预付定金");
                    } else {
                        // 全款
                        tvWantBuy.setText(TextUtils.isEmpty(wantBuytitle) ? "立即购买" : wantBuytitle);
                    }
                }

                int w = AppUtils.getScreenWidth(mContext) - AppUtils.dip2px(mContext, 48);
                int h = AppUtils.dip2px(mContext, 161);
                ImageLoaderManager.INSTANCE.loadCoverSizedImage(mContext, ivCover, attachment.getCoverUrl(), AppUtils.dip2px(mContext, 8), w, h, 0);

                ivCover.setOnClickListener(view -> {

                    if (FastClickAvoidUtil.isFastDoubleClick(ivCover.getId())) {
                        return;
                    }
                    if ("1".equals(attachment.getProductType())) {
                        //1 商品 2广告
                        RouterJumpUtil.startGoodsDetailFromLiveRoom(attachment.getProjectId(), LiveRoomInfoProvider.getInstance().roomId,LiveRoomInfoProvider.getInstance().liveId,"0",attachment.getAgentCompanyCode(),attachment.getAgentId(),attachment.getAgentLive());
                    } else if ("2".equals(attachment.getProductType())) {
                        ARouter.getInstance().build(RouterPathProvider.ONLINE_ENTERPRISE_AND_PRODUCT_DETAIL)
                                .withString("productId", attachment.getProjectId())
                                .navigation();
                    }
                });

                //我感兴趣
                tvInterested.setOnClickListener(view -> {
                    if (FastClickAvoidUtil.isDoubleClick()) {
                        return;
                    }
                    if ("已提交".equals(tvInterested.getText().toString())) {
                        return;
                    }
                    OMAppApiProvider.getInstance().interestedV2(attachment.getProjectId(),
                            LiveRoomInfoProvider.getInstance().roomId, LiveRoomInfoProvider.getInstance().liveId,
                            attachment.getAgentCompanyCode(),attachment.getAgentId(),attachment.getAgentLive(),new Observer<OMBaseResponse>() {
                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {
                                    Toasty.normal(mContext, "网络错误，请检查网络").show();
                                }

                                @Override
                                public void onNext(OMBaseResponse response) {
                                    if (response.isSuccess()) {
                                        tvInterested.setText("已提交");
                                        LiveRoomHelper.getInstance().receivedProjects.get(position).setIsInterest("1");
                                    } else {
                                        Toasty.normal(mContext, response.msg).show();
                                    }
                                }
                            });
                });

                //意向购买
                tvWantBuy.setOnClickListener(view -> {

                    if (FastClickAvoidUtil.isDoubleClick()) {
                        return;
                    }

                    if (!NetworkUtil.isNetAvailable(mContext)) {
                        Toasty.normal(mContext, "网络错误，请检查网络").show();
                        return;
                    }
                    if ("1".equals(attachment.getProductType())) {
                        //1 商品 2广告
                        RouterJumpUtil.startOderSureActivity(attachment.getProjectId(), LiveRoomInfoProvider.getInstance().liveId,"0",attachment.getAgentCompanyCode(),attachment.getAgentId(),attachment.getAgentLive());
                    } else if ("2".equals(attachment.getProductType())) {
                        ARouter.getInstance().build(RouterPathProvider.ONLINE_ENTERPRISE_AND_PRODUCT_DETAIL)
                                .withString("productId", attachment.getProjectId())
                                .navigation();
                    }
                });
                //关闭按钮
                ivClose.setOnClickListener(view -> {

                    int[] activityIconLocation = ((BaseLiveAct) mContext).mBottomOperationView.getCenterListIconLocation();
                    LRUEController.getInstance().doCloseCenterListUE(mContext, activityIconLocation, ueLayout, ivCover);

                    ivClose.setVisibility(View.GONE);

                    new Handler().postDelayed(() -> {
                        ((BaseLiveAct) mContext).mBottomOperationView.showCenterListRedDot(true);
                        mDatas.remove(position);
                        updata(true, null);
                    }, 510);
                });

                container.addView(itemPagerView);
                return itemPagerView;
            }

            @Override
            public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
                container.removeView((View) object);
            }
        };

        return mAdapter;
    }

    public void show() {

        mDialog = null;
        mDialog = new Dialog(mContext, com.tojoy.cloud.online_meeting.R.style.CenterProject);
        mDialog.setContentView(mView);
        Window win = mDialog.getWindow();
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.windowAnimations = com.tojoy.cloud.online_meeting.R.style.CenterInAndOutStyle;
        lp.gravity = Gravity.CENTER;
        win.setAttributes(lp);

        mDialog.setCanceledOnTouchOutside(false);
        try {
            mDialog.show();
            mDialog.setOnKeyListener((dialog, keyCode, event) -> true);
        } catch (Exception e) {
            Log.e("TAG", "e === " + e.getMessage());
        }
    }
}
