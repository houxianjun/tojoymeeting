package com.tojoy.cloud.online_meeting.App.Live.FileShow;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.model.live.FileModel;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;

public class FilePopListAdapter extends BaseRecyclerViewAdapter<FileModel> {

    private int width, height;

    FilePopListAdapter(@NonNull Context context, @NonNull List<FileModel> datas) {
        super(context, datas);
        width = (AppUtils.getWidth(context) - AppUtils.dip2px(context, 40)) / 3;
        height = width;
    }

    @Override
    protected int getViewType(int position, @NonNull FileModel data) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_file_select_view;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new FileListVH(context, parent, getLayoutResId(viewType));
    }

    public void setDatas(List<FileModel> datas) {
        this.datas.clear();
        this.datas.addAll(datas);
        notifyDataSetChanged();
    }

    /**
     * 选择框Cell
     */
    public class FileListVH extends BaseRecyclerViewHolder<FileModel> {
        private ImageView mFolderCover;
        private TextView mFolderName;
        private ImageView mIcon;
        private ImageView mDeleteIcon;
        Context context;

        FileListVH(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            this.context = context;
            mFolderCover = itemView.findViewById(R.id.iv_folder_cover);
            mFolderName = itemView.findViewById(R.id.tv_folder_name);
            mIcon = itemView.findViewById(R.id.iv_icon);
            mDeleteIcon = itemView.findViewById(R.id.iv_delete);
            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(width, height);
            lps.setMargins(AppUtils.dip2px(context, 3), AppUtils.dip2px(context, 8), AppUtils.dip2px(context, 3), 0);
            mFolderCover.setLayoutParams(lps);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mDeleteIcon.getLayoutParams();
            params.height = height;
            params.width = width;
            mDeleteIcon.setLayoutParams(params);
        }

        @Override
        public void onBindData(FileModel message, int position) {
            //类型1、图片 2、视频,3 ppt,4 pdf, 999 其他
            if ("4".equals(message.type)) {
                // 显示默认图标
                mIcon.setVisibility(View.VISIBLE);
                mFolderCover.setScaleType(ImageView.ScaleType.CENTER_CROP);
                Glide.with(context)
                        .load("pdf")
                        .apply(new RequestOptions().placeholder(R.drawable.shape_white_bg_online))
                        .into(mFolderCover);
                Glide.with(context)
                        .load("pdf")
                        .apply(new RequestOptions().placeholder(R.drawable.icon_file_pdf))
                        .into(mIcon);
            } else if ("2".equals(message.type)) {
                // 视频显示默认图标
                mIcon.setVisibility(View.VISIBLE);
                mFolderCover.setScaleType(ImageView.ScaleType.CENTER_CROP);
                ImageLoaderManager.INSTANCE.loadVideoScreenshot(context, mFolderCover,
                        OSSConfig.getOSSURLedStr(message.url), R.drawable.icon_online_meeting_home_grid_default, AppUtils.dip2px(context, 2.5f),
                        false, false, false, false, 0);
                Glide.with(context)
                        .load("video")
                        .apply(new RequestOptions().placeholder(R.drawable.icon_online_start_video))
                        .into(mIcon);
            } else {
                mIcon.setVisibility(View.GONE);
                mFolderCover.setScaleType(ImageView.ScaleType.CENTER_CROP);
                ImageLoaderManager.INSTANCE.loadRoundCornerImage(context,
                        mFolderCover,
                        OSSConfig.getOSSURLedStr(message.url),
                        AppUtils.dip2px(context, 2.5f),
                        (AppUtils.getScreenWidth(context) - AppUtils.dip2px(context, 22)) / 3,
                        (AppUtils.getScreenWidth(context) - AppUtils.dip2px(context, 22)) / 3,
                        R.drawable.shape_white_bg_online);
            }
            if (message.isDeleteChecked) {
                // 选中状态显示删除图标
                mDeleteIcon.setVisibility(View.VISIBLE);
            } else {
                mDeleteIcon.setVisibility(View.GONE);
            }

            mFolderName.setText(message.fileName);

            itemView.setOnLongClickListener(v -> {
                if (mOnLongItemClickListener != null) {
                    mOnLongItemClickListener.onLongItemClick(position);
                }
                return false;
            });

            mDeleteIcon.setOnClickListener(v -> {
                if (mOnLongItemClickListener != null) {
                    mOnLongItemClickListener.onDelete(position);
                }
            });
        }
    }

    public interface OnLongItemClickListener {
        void onLongItemClick(int position);

        void onDelete(int position);
    }

    private OnLongItemClickListener mOnLongItemClickListener;

    void setOnLongItemClickListener(OnLongItemClickListener longItemClickListener) {
        this.mOnLongItemClickListener = longItemClickListener;
    }
}