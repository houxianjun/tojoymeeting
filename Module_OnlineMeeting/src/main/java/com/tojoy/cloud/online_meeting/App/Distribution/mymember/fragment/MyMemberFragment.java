package com.tojoy.cloud.online_meeting.App.Distribution.mymember.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tojoy.cloud.online_meeting.App.Distribution.mymember.adapter.MyMemberAdapter;
import com.tojoy.tjoybaselib.model.home.MineMemberDistributionResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.imageManager.YuanJiaoImageView;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.cloud.online_meeting.App.Distribution.mymember.activity.DistributionMyMemberAct;
import com.tojoy.cloud.online_meeting.App.Distribution.view.CommonProfitView;
import com.tojoy.cloud.online_meeting.R;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

@SuppressLint("ValidFragment")
public class MyMemberFragment extends Fragment {
    /**
     * 0 我的会员 1 我的团队
     */
    private String mStatus;
    private View mView;
    private TJRecyclerView mRecyclerView;
    private MyMemberAdapter mAdapter;
    private int mPage = 1;

    public void setmPage(int mPage) {
        this.mPage = mPage;
    }

    private List<MineMemberDistributionResponse.MyMemberRecordsModel> mDatas = new ArrayList<>();

    public MyMemberFragment(String status) {
        this.mStatus = status;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_distribution_mymember, container, false);
        initUI();
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPage = 1;
        initData();

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }

    public void initData() {
        //我的会员
        if ("0".equals(this.mStatus)) {
            OMAppApiProvider.getInstance().queryDistributionMyMemberList(mPage, new Observer<MineMemberDistributionResponse>() {

                @Override
                public void onCompleted() {
                    mRecyclerView.stopRefresh();
                }

                @Override
                public void onError(Throwable e) {
                    Toasty.normal(getActivity(), "网络错误，请检查网络").show();
                    mRecyclerView.stopRefresh();
                }

                @Override
                public void onNext(MineMemberDistributionResponse response) {
                    if (response.isSuccess()) {
                        if (mPage == 1) {
                            mDatas.clear();
                        }
                        mDatas.addAll(response.data.records);
                        mAdapter.updateData(mDatas);
                        mRecyclerView.refreshLoadMoreView(mPage, response.data.records.size());
                    } else {
                        Toasty.normal(getActivity(), response.msg).show();
                    }
                }
            });
        } else {
            //我的团队
            OMAppApiProvider.getInstance().queryDistributionMyTeamList(mPage, new Observer<MineMemberDistributionResponse>() {

                @Override
                public void onCompleted() {
                    mRecyclerView.stopRefresh();
                }

                @Override
                public void onError(Throwable e) {
                    Toasty.normal(getActivity(), "网络错误，请检查网络").show();
                    mRecyclerView.stopRefresh();
                }

                @Override
                public void onNext(MineMemberDistributionResponse response) {
                    if (response.isSuccess()) {
                        if (mPage == 1) {
                            mDatas.clear();
                        }
                        mDatas.addAll(response.data.records);

                        mAdapter.updateData(mDatas);
                        mRecyclerView.refreshLoadMoreView(mPage, response.data.records.size());

                    } else {
                        Toasty.normal(getActivity(), response.msg).show();
                    }
                }
            });
        }

    }

    private void initUI() {
        mRecyclerView = mView.findViewById(R.id.recyclerView);
        mRecyclerView.getRecyclerView().setVerticalScrollBarEnabled(false);
        mAdapter = new MyMemberAdapter(getActivity(), this.mStatus, mDatas);
        mRecyclerView.setLoadMoreViewWithHide();

        if ("0".equals(this.mStatus)) {
            mRecyclerView.setEmptyImgAndTip(R.drawable.icon_no_content, "还没有会员…");
        } else {
            mRecyclerView.setEmptyImgAndTip(R.drawable.icon_no_content, "还没有团队…");
        }

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                refreshData();
            }

            @Override
            public void onLoadMore() {
                mPage++;
                initData();
            }
        });
    }

    /**
     * 如果是二级分销客，在下拉刷新时获取当前分销客等级接口。
     * 如果是一级分销客。下拉刷新获取获取我的会员，我的团队人数接口
     */
    public void refreshData() {
        if (getActivity() != null && !getActivity().isFinishing()) {
            //二级分销客 当前是我的会员还是我的团队
            ((DistributionMyMemberAct) getActivity()).queryDistributionData();
        }
    }
}

