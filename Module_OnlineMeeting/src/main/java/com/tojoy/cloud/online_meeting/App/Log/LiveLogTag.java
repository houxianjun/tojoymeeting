package com.tojoy.cloud.online_meeting.App.Log;

import android.os.Environment;
import android.util.Log;

import com.netease.nim.uikit.common.NIMCache;
import com.netease.nim.uikit.common.util.log.sdk.wrapper.NimLog;
import com.tojoy.tjoybaselib.services.log.BaseLiveLog;
import com.tojoy.tjoybaselib.util.time.DateTimeUtil;

public class LiveLogTag extends BaseLiveLog {

    public static void initLiveLog(String liveTitle, String liveId) {
        NimLog.initNLog("",
                Environment.getExternalStorageDirectory() + "/" + NIMCache.getContext().getPackageName() + "/liveLog/" + DateTimeUtil.getCurrentTime(DateTimeUtil.PATTERN_DAY_WITH_SPERATOR),
                liveTitle + "：" + liveId + "：" +
                        DateTimeUtil.getCurrentTime(DateTimeUtil.PATTERN_CURRENT_TIME_HOUR) +
                        ".log",
                Log.INFO,
                0,
                0,
                false,
                null);
    }
}
