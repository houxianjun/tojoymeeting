package com.tojoy.cloud.online_meeting.App.Live.Live.View;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.live.NoticeModel;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.cloud.online_meeting.R;

import rx.Observer;

/**
 * Created by luuzhu on 2019/4/30.
 * 直播间 间歇性显示的公告
 */

public class LiveNoticeView {


    private Context mContext;
    private RelativeLayout mNoticeContainer;

    //共显示多少次
    private int mNoticeShowTimes;
    //显示频率 多少分钟显示一次
    private int mNoticeShowRate;
    //每次显示时长
    private int mNoticeShowDuration;

    private View mView;
    private TextView mTvNotice;

    private final int SHOW_NOTICE = 888;
    private final int HIDE_NOTICE = 999;

    private boolean isShouldShow = true;

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == SHOW_NOTICE) {
                mHandler.removeMessages(SHOW_NOTICE);
                mNoticeContainer.setVisibility(isShouldShow ? View.VISIBLE : View.GONE);
                mNoticeShowTimes--;
                mHandler.sendEmptyMessageDelayed(HIDE_NOTICE, 1000 * mNoticeShowDuration);

            } else if (msg.what == HIDE_NOTICE) {
                mHandler.removeMessages(HIDE_NOTICE);
                mNoticeContainer.setVisibility(View.GONE);
                if (mNoticeShowTimes > 0) {
                    mHandler.sendEmptyMessageDelayed(SHOW_NOTICE, 1000 * mNoticeShowRate);
                }
            }
        }
    };


    public LiveNoticeView(Context context, RelativeLayout mNoticeContainer) {
        this.mContext = context;
        mView = View.inflate(mContext, R.layout.live_notice, null);
        mTvNotice = mView.findViewById(R.id.tvNotice);
        this.mNoticeContainer = mNoticeContainer;

        update();
    }


    public void update() {

        OMAppApiProvider.getInstance().getNotice(LiveRoomInfoProvider.getInstance().liveId, new Observer<NoticeModel>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(NoticeModel response) {
                if (response.isSuccess()) {

                    if ("1".equals(response.data.isShow)) {
                        try {
                            mNoticeShowRate = Integer.parseInt(response.data.boardFrequency);
                            mNoticeShowDuration = Integer.parseInt(response.data.boardDuration);
                            mNoticeShowTimes = Integer.parseInt(response.data.boardTimesNum);

                            if (mNoticeShowDuration > 0 && mNoticeShowTimes > 0 && !TextUtils.isEmpty(response.data.publicBoardName)) {

                                mHandler.removeMessages(SHOW_NOTICE);
                                mHandler.removeMessages(HIDE_NOTICE);
                                mNoticeContainer.setVisibility(View.GONE);

                                mTvNotice.setText(response.data.publicBoardName);
                                mHandler.sendEmptyMessage(SHOW_NOTICE);
                                mHandler.sendEmptyMessageDelayed(HIDE_NOTICE, 1000 * mNoticeShowDuration);
                            }
                        } catch (Exception e) {
                        }
                    } else {
                        mHandler.removeMessages(SHOW_NOTICE);
                        mHandler.removeMessages(HIDE_NOTICE);
                        mNoticeContainer.setVisibility(View.GONE);
                    }
                }

            }
        });
    }

    public View getNoticeView() {
        return mView;
    }

    public void setShowStatus(int status) {
        isShouldShow = status == View.VISIBLE;
    }
}
