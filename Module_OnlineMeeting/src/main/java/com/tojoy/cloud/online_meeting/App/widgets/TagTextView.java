package com.tojoy.cloud.online_meeting.App.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.util.sys.AppUtils;

/**
 * @author qll
 * @date 2019/4/12
 */
@SuppressLint("AppCompatCustomView")
public class TagTextView extends TextView{

    public TagTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public TagTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TagTextView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        //设置观察者，烂大街的代码
        //这里未把观察者注销，是因为我的TextView宽高是随时变化的，如果是固定的最好根据需要注销掉
        getViewTreeObserver().addOnGlobalLayoutListener(() -> analyzeProcess());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(AppUtils.dip2px(context,5), 0, 0, 0);
        setLayoutParams(params);
        setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
        setLines(1);
        setEllipsize(TextUtils.TruncateAt.END);
        setTextSize(10);
        setPadding(AppUtils.dip2px(context,5),
                AppUtils.dip2px(context,1),
                AppUtils.dip2px(context,5),
                AppUtils.dip2px(context,1));
    }

    /**
     * 通过 layout 的 getEllipsisCount(int line) 方法，来获取被省略的部分数量
     * 为0时就是没省略
     * 在利用自定义的监听器返回给待操作的对象；
     */
    private void analyzeProcess() {
        if (onEllipsisListener == null) {
            return;
        }
        Layout layout = getLayout();//拿到Layout
        int line = getLineCount();//获取文字行数
        if (line > 0) {
            int ellipsisCount = layout.getEllipsisCount(line - 1);
            //ellipsisCount > 0 时，说明省略生效
            onEllipsisListener.onEllipsis(ellipsisCount > 0, ellipsisCount);
        }
    }

//    SizeChangeListener l;
//
//    public void setSizeChangeListener(SizeChangeListener orlExt) {
//        l = orlExt;
//    }
//
//    @Override
//    public void onSizeChanged(int w, int h, int oldw, int oldh) {
//        // TODO Auto-generated method stub
//        l.sizeChanged(w, h, oldw, oldh);
//        super.onSizeChanged(w, h, oldw, oldh);
//    }
//
//    public interface SizeChangeListener {
//        void sizeChanged(int w, int h, int oldw, int oldh);
//    }


    private OnEllipsisListener onEllipsisListener;
    public void setOnEllipsisListener(OnEllipsisListener onEllipsisListener) {
        this.onEllipsisListener = onEllipsisListener;
    }
        /**
         * 自定义监听器
         * boolean 省略是否生效  ellipsisCount 省略部分字数
         */
    public interface OnEllipsisListener {
        void onEllipsis(boolean isEllipsis, int ellipsisCount);
    }
}
