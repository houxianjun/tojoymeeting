package com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.activity;

import android.os.Bundle;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.fragment.DistributionInformationEntryFragment;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.util.sys.ActivityUtils;
import com.tojoy.tjoybaselib.util.sys.StatusbarUtils;

/**
 * 分销客 信息录入
 *
 * @author houxianjun
 * @Date 2020/07/16
 */
@Route(path = RouterPathProvider.Distribution_Information_Entry_Act)
public class DistributionInformationEntryAct extends UI {

    DistributionInformationEntryFragment distributionInformationEntryFragment;

    //页面来源
    String fromPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distribution_information_entry);
        ARouter.getInstance().inject(this);
        StatusbarUtils.enableTranslucentStatusbar(this);
        StatusbarUtils.setStatusBarLightMode(this, StatusbarUtils.statusbarlightmode(this));
        setSwipeBackEnable(false);
        initTitle();
        fromPage = getIntent().getStringExtra("from_page");
        distributionInformationEntryFragment = DistributionInformationEntryFragment.newInstance(fromPage);

        ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), distributionInformationEntryFragment, R.id.fragmentContainer);

    }

    private void initTitle() {
        setUpNavigationBar();
        setTitle("");
        showBack();
        setAlphaBackground();
    }

    @Override
    protected void onLeftManagerImageClick() {
        this.finish();
    }
}
