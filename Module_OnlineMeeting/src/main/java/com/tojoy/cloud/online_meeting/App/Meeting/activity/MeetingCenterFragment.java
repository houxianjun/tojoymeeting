package com.tojoy.cloud.online_meeting.App.Meeting.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.enterprise.EnterpriseHomeHotLiveListResponse;
import com.tojoy.tjoybaselib.model.live.OnlineMeetingHomeGridModel;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigCacheManager;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigConstantKey;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.App.Meeting.adapter.LivecenterMultiAdapter;
import com.tojoy.cloud.online_meeting.App.Meeting.model.MultiAdapterBean;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.App.BaseFragment;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 会议中心搜索 & 会议中心列表
 */
@SuppressLint("ValidFragment")
public class MeetingCenterFragment extends BaseFragment {

    private TJRecyclerView mRecyclerView;
    LivecenterMultiAdapter mAdapter;
    ArrayList<MultiAdapterBean> mSelfLiveList = new ArrayList<>();

    private int mPage = 1;

    private RelativeLayout mRlvSearch;
    private RelativeLayout mTitleRelativeLayout;
    private TextView mCenterTitle;

    //状态栏高度
    private int mStatusBarHeight;

    private boolean isRefreshing;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_meeting_center_home, container, false);
        }
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        isCreateView = true;
        return mView;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initUI() {
        //初始化标题栏
        mView.findViewById(R.id.view_base_title).setVisibility(View.VISIBLE);
        mRlvSearch = mView.findViewById(R.id.content_seach);
        mRlvSearch.setVisibility(View.VISIBLE);
        mTitleRelativeLayout = mView.findViewById(R.id.view_base_title);
        mCenterTitle = mView.findViewById(R.id.tv_basetitle_cetener);
        mRecyclerView = mView.findViewById(R.id.recyclerView);
        initTitle();

        mRecyclerView.getRecyclerView().setVerticalScrollBarEnabled(false);
        mRecyclerView.setLoadMoreView();
        mRecyclerView.goneFooterView();
        mRecyclerView.setEmptyImgAndTip(R.drawable.icon_empty_online_logo, getString(R.string.instant_no_content));
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                requestMeeingList();
            }

            @Override
            public void onLoadMore() {
                mPage++;
                requestMeeingList();
            }
        });
        mAdapter = new LivecenterMultiAdapter(getActivity(), mSelfLiveList);
        mRecyclerView.setAdapter(mAdapter);

        //初始化控件监听事件
        initListener();
    }


    /**
     * 初始化标题栏
     */
    private void initTitle() {

        setUpNavigationBar(mTitleRelativeLayout);
        String title = TextConfigCacheManager.getInstance(getContext()).getWidgetTextByKey(TextConfigConstantKey.MEETING_TITLE);
        setNavTitle(TextUtils.isEmpty(title) ? getContext().getString(R.string.meeting) : title);
        hideBottomLine();
        mStatusBarHeight = AppUtils.getStatusBarHeight(getActivity());
        RelativeLayout.LayoutParams layoutParamsSearch = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, AppUtils.dip2px(getActivity(), 38));
        layoutParamsSearch.setMargins(0, mStatusBarHeight + AppUtils.dip2px(getActivity(), 48), 0, 0);
        mRlvSearch.setLayoutParams(layoutParamsSearch);
    }


    /**
     * indicator 随这recyceView滑动 改变位置
     *
     * @param verticalDis
     */
    private void drawIndicator(int verticalDis) {
        if (verticalDis < 0) {
            return;
        }
        int rlvTopViewMarginTop = verticalDis;
        if (verticalDis >= AppUtils.dip2px(getActivity(), 39)) {
            rlvTopViewMarginTop = AppUtils.dip2px(getActivity(), 39);
        }

        if (verticalDis > AppUtils.dip2px(getActivity(), 8)) {
            if (!isRefreshing) {
                mRecyclerView.setRefreshable(false);
            }
        } else {
            mRecyclerView.setRefreshable(true);
        }


        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.dip2px(getActivity(), 38));
        params.setMargins(0, (mStatusBarHeight + AppUtils.dip2px(getActivity(), 48)) - rlvTopViewMarginTop, 0, 0);
        mRlvSearch.setLayoutParams(params);

    }

    private void changeApha(int dy) {
        float mRlvReacherAlpha = 0;
        if (dy >= AppUtils.dip2px(getActivity(), 5)) {
            mRlvReacherAlpha = (float) ((1.0 * (dy - AppUtils.dip2px(getActivity(), 5))) / (1.0 * AppUtils.dip2px(getActivity(), 20)));
            if (mRlvReacherAlpha > 1) {
                mRlvReacherAlpha = 1;
            }
            mCenterTitle.setAlpha(1 - mRlvReacherAlpha);
        } else {
            mRlvReacherAlpha = 0;
            mCenterTitle.setAlpha(1 - mRlvReacherAlpha);
        }
    }


    @Override
    protected void lazyLoad() {
        refreshData();
    }

    public void refreshData() {
        mRecyclerView.autoRefresh();
    }

    /**
     * 初始化动画View
     *
     * @param mRootView
     */
    public void initAnimationView(View mRootView) {
        View mView1 = mRootView.findViewById(R.id.view1);
        View mView2 = mRootView.findViewById(R.id.view2);
        View mView3 = mRootView.findViewById(R.id.view3);
        View mView4 = mRootView.findViewById(R.id.view4);

        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.0f, 1f, 0.1f, Animation.RELATIVE_TO_PARENT
                , 1.0f, Animation.RELATIVE_TO_PARENT
                , 1.0f);
        scaleAnimation.setDuration(400);
        scaleAnimation.setRepeatMode(Animation.REVERSE);
        scaleAnimation.setRepeatCount(-1);
        mView1.startAnimation(scaleAnimation);


        ScaleAnimation scaleAnimation1 = new ScaleAnimation(1.0f, 1.0f, 1f, 0.1f, Animation.RELATIVE_TO_PARENT
                , 1.0f, Animation.RELATIVE_TO_PARENT
                , 1.0f);
        scaleAnimation1.setDuration(350);
        scaleAnimation1.setRepeatMode(Animation.REVERSE);
        scaleAnimation1.setRepeatCount(-1);
        mView2.startAnimation(scaleAnimation1);


        ScaleAnimation scaleAnimation12 = new ScaleAnimation(1.0f, 1.0f, 1f, 0.1f, Animation.RELATIVE_TO_PARENT
                , 1.0f, Animation.RELATIVE_TO_PARENT
                , 1.0f);
        scaleAnimation12.setDuration(450);
        scaleAnimation12.setRepeatMode(Animation.REVERSE);
        scaleAnimation12.setRepeatCount(-1);
        mView3.startAnimation(scaleAnimation12);


        ScaleAnimation scaleAnimation13 = new ScaleAnimation(1.0f, 1.0f, 1f, 0.1f, Animation.RELATIVE_TO_PARENT
                , 1.0f, Animation.RELATIVE_TO_PARENT
                , 1.0f);
        scaleAnimation13.setDuration(500);
        scaleAnimation13.setRepeatMode(Animation.REVERSE);
        scaleAnimation13.setRepeatCount(-1);
        mView4.startAnimation(scaleAnimation13);
    }


    /**
     * 初始化控件事件
     */
    private void initListener() {
        //滑动监听，回收播放器资源
        mRecyclerView.getRecyclerView().addOnChildAttachStateChangeListener(new RecyclerView.OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(View view) {
                RelativeLayout mRlvAnimContainer = view.findViewById(R.id.rlv_anim_container);
                if (mRlvAnimContainer == null) {
                    return;
                }
                initAnimationView(mRlvAnimContainer.getChildAt(0));
            }

            @Override
            public void onChildViewDetachedFromWindow(View view) {

            }
        });

        //点击进入直播间会议
        mAdapter.setmOnClickJoinDetailListener((view, position, data) -> {
            enterHotLiveDetail(view, data);
        });


        //点击搜索框进入搜索页面
        mView.findViewById(R.id.content_seach).setOnClickListener(v -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }
            MeetingSearchAct.start(getContext());
        });


        mRecyclerView.getRecyclerView().addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                drawIndicator(getScollYDistance());
                changeApha(getScollYDistance());
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (2 == newState) {
                    mCenterTitle.setAlpha(1);
                }
            }
        });

    }

    /**
     * 获取recycleView 滑动到巨鹿
     *
     * @return
     */
    private int getScollYDistance() {
        LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerView.getRecyclerView().getLayoutManager();
        int position = layoutManager.findFirstVisibleItemPosition();
        View firstVisiableChildView = layoutManager.findViewByPosition(position);
        if (firstVisiableChildView != null) {
            int itemHeight = firstVisiableChildView.getHeight();
            return (position) * itemHeight - firstVisiableChildView.getTop();
        }
        return 0;
    }


    /**
     * 进入直播间详情
     *
     * @param v
     * @param data
     */
    private void enterHotLiveDetail(View v, OnlineMeetingHomeGridModel data) {
        if (!TextUtils.isEmpty(data.hasPassword) && "1".equals(data.hasPassword)) {
            // 房间设置了密码
            // 检验是否要输入密码
            LiveRoomIOHelper.checkEnterLiveRoom(getContext(), data.roomLiveId, new IOLiveRoomListener() {
                @Override
                public void onIOError(String error, int errorCode) {
                    if (errorCode == 301) {
                        ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_ENTER_ROOM_PSW)
                                .withString("roomName", data.title)
                                .withString("liveId", data.roomLiveId)
                                .navigation();
                    } else if (errorCode == 302) {
                    } else {
                        Toasty.normal(getContext(), error).show();
                    }
                }

                @Override
                public void onIOSuccess() {
                    // 不需要输入密码，直接进入,包括判断密码正确与否与人数是否已满
                    joinRoom(data);
                }
            });
        } else {
            joinRoom(data);
        }
    }


    public void joinRoom(OnlineMeetingHomeGridModel data) {

        //先调一次退出直播间 针对该用户从直播间点主页后跳另外的直播间
        if (!TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().liveId) && !TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().imRoomId)
                && !LiveRoomInfoProvider.getInstance().liveId.equals(data.roomLiveId)) {
            LiveRoomInfoProvider.getInstance().lastLiveId = LiveRoomInfoProvider.getInstance().liveId;
            LiveRoomInfoProvider.getInstance().lastIMRoomId = LiveRoomInfoProvider.getInstance().imRoomId;
        } else {
            LiveRoomInfoProvider.getInstance().lastLiveId = "";
        }

        LiveRoomInfoProvider.getInstance().joinPassword = "";
        LiveRoomInfoProvider.getInstance().liveId = data.roomLiveId;
        LiveRoomIOHelper.joinLiveRoom(getContext(), new IOLiveRoomListener() {
            @Override
            public void onIOError(String error, int errorCode) {
                if (!TextUtils.isEmpty(error)) {
                    Toasty.normal(getContext(), error).show();
                }
            }

            @Override
            public void onIOSuccess() {
                LiveRoomIOHelper.exitOldUser(BaseUserInfoCache.getUserId(getContext()));
            }
        });
    }


    /**
     * 会议列表
     */
    private void requestMeeingList() {
        OMAppApiProvider.getInstance().mGetMeetingList(mPage, 1, null, new Observer<EnterpriseHomeHotLiveListResponse>() {
            @Override
            public void onCompleted() {
                mRecyclerView.setRefreshing(false);
                setRefreshing();
                try {
                    if (mSelfLiveList.get(0).liveDataList.size() == 0) {
                        mView.findViewById(R.id.emptyLayout).setVisibility(View.VISIBLE);
                    } else {
                        mView.findViewById(R.id.emptyLayout).setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                }

            }

            @Override
            public void onError(Throwable e) {
                setRefreshing();
                mRecyclerView.setRefreshing(false);
                Toasty.normal(getContext(), "网络异常，稍后再试").show();
            }

            @Override
            public void onNext(EnterpriseHomeHotLiveListResponse homeHotLiveListResponse) {
                setRefreshing();
                if (homeHotLiveListResponse.isSuccess() && homeHotLiveListResponse.data.list != null) {
                    if (mSelfLiveList.size() <= 0) {
                        mSelfLiveList.add(new MultiAdapterBean(MultiAdapterBean.CellType.MULTI_CELL, MultiAdapterBean.ResouseType.CENTER_LIVE_LIST)
                                .setLiveDataList(homeHotLiveListResponse.data.list));
                    } else {
                        if (mPage == 1) {
                            mSelfLiveList.get(0).liveDataList.clear();
                        }
                        mSelfLiveList.get(0).liveDataList.addAll(homeHotLiveListResponse.data.list);
                    }

                    mRecyclerView.refreshLoadMoreView(mPage, homeHotLiveListResponse.data.list.size());
                    mAdapter.updateData(mSelfLiveList);
                } else {
                    if (!homeHotLiveListResponse.isSuccess()) {
                        Toasty.normal(getContext(), homeHotLiveListResponse.msg).show();
                    }
                }
            }
        });
    }

    private void setRefreshing() {
        new Handler().postDelayed(() -> isRefreshing = false, 500);
    }

}