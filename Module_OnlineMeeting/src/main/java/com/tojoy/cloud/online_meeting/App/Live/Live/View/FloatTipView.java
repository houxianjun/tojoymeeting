package com.tojoy.cloud.online_meeting.App.Live.Live.View;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RelativeLayout;

import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.R;

import butterknife.ButterKnife;

public class FloatTipView extends RelativeLayout {
    private View mView;
    private Context mContext;
    private int mTop;
    private int leftMargin;
    private int subWidth;
    private int operationHeight;
    private int lineHeight;
    private ValueAnimator floatAnimator;
    RelativeLayout mTip1Container;
    RelativeLayout mTip2Container;

    public FloatTipView(Context context) {
        super(context);
    }

    public FloatTipView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FloatTipView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void initView(Context context) {
        mContext = context;
        mView = LayoutInflater.from(context).inflate(R.layout.tip_guide_layout, this);
        ButterKnife.bind(this, mView);
        leftMargin = AppUtils.dip2px(mContext, 8);
        subWidth = (AppUtils.getWidth(mContext) - leftMargin * 7) / 4;
        operationHeight = AppUtils.dip2px(mContext, 70);
        lineHeight = AppUtils.dip2px(mContext, 8);
        mTop = AppUtils.dip2px(mContext, 10);
        mTip1Container = (RelativeLayout) findViewById(R.id.tip_container);
        mTip2Container = (RelativeLayout) findViewById(R.id.tip_container2);
    }

    public void hideTip() {
        if (mTip1Container != null) {
            mTip1Container.clearAnimation();
            mTip1Container.setVisibility(GONE);
        }

        if (mTip2Container != null) {
            mTip2Container.clearAnimation();
            mTip2Container.setVisibility(GONE);
        }
    }

    public void showTip1(boolean isOneLice) {
        LayoutParams childLps = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, AppUtils.dip2px(mContext, 66));
        childLps.addRule(ALIGN_PARENT_BOTTOM);
        childLps.setMargins(leftMargin, 0, 0, isOneLice ? (subWidth + operationHeight) : (subWidth * 2 + operationHeight + lineHeight));
        setLayoutParams(childLps);
        setVisibility(VISIBLE);
        showTip(mTip1Container);
    }

    public void showTip2(boolean isOneLice, int onlineCount) {
        LayoutParams childLps = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, AppUtils.dip2px(mContext, 66));
        childLps.addRule(ALIGN_PARENT_BOTTOM);

        if (onlineCount > 2) {
            mView.findViewById(R.id.arrow2).setVisibility(VISIBLE);
            mView.findViewById(R.id.arrow3).setVisibility(GONE);
            int leftMargin = (AppUtils.getWidth(mContext) - AppUtils.dip2px(mContext, 272)) / 2;
            childLps.setMargins(leftMargin, 0, 0, isOneLice ? (subWidth + operationHeight) : (subWidth * 2 + operationHeight + lineHeight));
        } else {
            mView.findViewById(R.id.arrow2).setVisibility(GONE);
            mView.findViewById(R.id.arrow3).setVisibility(VISIBLE);
            childLps.setMargins(leftMargin, 0, 0, isOneLice ? (subWidth + operationHeight) : (subWidth * 2 + operationHeight + lineHeight));
        }

        setLayoutParams(childLps);
        setVisibility(VISIBLE);
        showTip(mTip2Container);
    }

    /**
     * 渐变显示
     *
     * @param tipView
     */
    private void showTip(View tipView) {
        tipView.setVisibility(VISIBLE);
        ObjectAnimator animator = ObjectAnimator.ofFloat(tipView, "alpha", 0f, 1f);
        animator.setDuration(400);  //设置动画时间
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                tipView.setOnClickListener(v -> hideTip(tipView));
                startVerticalFloat(tipView);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.start();
    }

    /**
     * 上下跳动
     *
     * @param floatView
     */
    private void startVerticalFloat(View floatView) {
        floatAnimator = ValueAnimator.ofInt(0, AppUtils.dip2px(mContext, 10), 0);
        floatAnimator.addUpdateListener(animation -> {
            int dx = (int) animation.getAnimatedValue();
            LayoutParams lps = (LayoutParams) floatView.getLayoutParams();
            lps.setMargins(0, dx, 0, 0);
            floatView.setLayoutParams(lps);
        });

        floatAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                hideTip(floatView);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        floatAnimator.setRepeatMode(ValueAnimator.RESTART);
        floatAnimator.setRepeatCount(4);
        floatAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        floatAnimator.setDuration(1500);
        floatAnimator.start();
    }

    /**
     * 渐变隐藏
     *
     * @param tipView
     */
    private void hideTip(View tipView) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(tipView, "alpha", 1f, 0f);
        animator.setDuration(400);  //设置动画时间
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                tipView.setOnClickListener(v -> {
                });
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                tipView.setVisibility(GONE);
                floatAnimator.cancel();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.start();
    }
}
