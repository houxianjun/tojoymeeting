package com.tojoy.cloud.online_meeting.App.Live.Live.Helper;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomHelper;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tencent.liteav.TXLiteAVCode;
import com.tencent.rtmp.ui.TXCloudVideoView;
import com.tencent.trtc.TRTCCloudDef;
import com.tencent.trtc.TRTCCloudListener;
import com.tencent.trtc.TRTCStatistics;
import com.tojoy.tjoybaselib.model.live.DoumentVideoSendMsg;
import com.tojoy.tjoybaselib.model.live.LiveNeedShowLeavePop;
import com.tojoy.tjoybaselib.model.live.OnlineUserInfoResponse;
import com.tojoy.tjoybaselib.model.live.SelectOutLiveModel;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigCacheManager;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigConstantKey;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.netease.nim.uikit.business.liveroom.LiveCustomMessage;
import com.netease.nim.uikit.business.liveroom.LiveCustomMessageCode;
import com.tojoy.cloud.online_meeting.App.Live.Live.Model.LiveTemplateLeaveMsg;
import com.tojoy.cloud.online_meeting.App.Live.Live.Model.OnlineMember;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;

import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

import static com.tencent.trtc.TRTCCloudDef.TRTCRoleAudience;

/**
 * SDK内部状态回调
 * 1.加入房间成功
 * 2.离开房间成功
 * 3.房间异常：进入异常、退出异常、房间内异常
 * 4.新用户进入
 * 5.用户离开
 * 6.用户开启或关闭了视频
 * 7.用户音频变化
 * 8.获取到了第一帧画面
 */
public class TRTCCloudListenerImpl extends TRTCCloudListener implements TRTCCloudListener.TRTCVideoRenderListener {
    private final static String TAG = TRTCCloudListenerImpl.class.getSimpleName();

    private WeakReference<BaseLiveAct> mContext;

    public TRTCCloudListenerImpl(BaseLiveAct activity) {
        super();
        mContext = new WeakReference<>(activity);
    }

    /**
     * 加入房间
     */
    @Override
    public void onEnterRoom(long elapsed) {
        final BaseLiveAct activity = mContext.get();
        if (activity != null) {
            activity.mTRTCVideoViewLayout.onRoomEnter();
            activity.onEnterRoomSuccess();
        }
    }

    /**
     * 离开房间
     */
    @Override
    public void onExitRoom(int reason) {
        BaseLiveAct activity = mContext.get();
        if (activity != null) {
            LogUtil.i(LiveLogTag.IOLiveTag, "onExitRoom");
            if (!LiveRoomHelper.getInstance().loginReenterLiveRoom) {
                activity.destoryTRCC();
                activity.finish();
            } else {
                if (!LiveRoomInfoProvider.getInstance().isHost()) {
                    activity.mTrtcParams.role = TRTCRoleAudience;
                }

                if (!LiveRoomHelper.getInstance().loginReenterLiveRoom) {
                    activity.mTrtcCloud.enterRoom(activity.mTrtcParams, activity.mAppScence);
                }
            }
            LiveRoomHelper.getInstance().loginReenterLiveRoom = false;
        }
    }

    /**
     * ERROR 大多是不可恢复的错误，需要通过 UI 提示用户
     */
    @Override
    public void onError(int errCode, String errMsg, Bundle extraInfo) {
        LogUtil.i(LiveLogTag.IOLiveTag, "exitRoom-onError" + errMsg + errCode);
        BaseLiveAct activity = mContext.get();
        if (activity != null) {
            if (errCode == TXLiteAVCode.ERR_ROOM_ENTER_FAIL) {
                if (activity != null) {
                    Toasty.normal(activity, "进入直播间异常").show();
                }
                activity.exitRoom(-1);
            }
        }
    }


    /**
     * WARNING 大多是一些可以忽略的事件通知，SDK内部会启动一定的补救机制
     */
    @Override
    public void onWarning(int warningCode, String warningMsg, Bundle extraInfo) {
        Log.d(TAG, "sdk callback onWarning");
    }

    @Override
    public void onSwitchRole(int i, String s) {
        super.onSwitchRole(i, s);
        LogUtil.i(LiveLogTag.TRTCLayout, "onSwitchRole-Impl" + s);
        BaseLiveAct activity = mContext.get();
        activity.onSwitchRole();
    }

    /**
     * 有新的用户加入了当前视频房间,创建一个View用来显示新的一路画面
     */
    @Override
    public void onRemoteUserEnterRoom(String userId) {
        super.onRemoteUserEnterRoom(userId);
        LogUtil.i(LiveLogTag.IOLiveTag, "onRemoteUserEnterRoom:userId--" + userId);

        BaseLiveAct activity = mContext.get();
        if (activity != null) {
            //5.19 同台/引流人数已满，再有人误入需要处理一下
            TXCloudVideoView renderView = activity.mTRTCVideoViewLayout.onMemberEnter(userId);
            if (renderView == null) {
                return;
            }

            renderView.setVisibility(View.VISIBLE);
        }
        OnlineMembersHelper.getInstance().onMemeberIn(userId);
        activity.onExploreMemberCountChange();
    }

    /**
     * 有用户离开了当前视频房间,停止观看画面,更新视频UI
     */
    @Override
    public void onRemoteUserLeaveRoom(String userId, int i) {
        super.onRemoteUserLeaveRoom(userId, i);
        BaseLiveAct activity = mContext.get();
        LogUtil.i(LiveLogTag.IOLiveTag, "onRemoteUserLeaveRoom:userId--" + userId + ",reason--"+i);
        if (activity != null) {
            if (LiveRoomInfoProvider.getInstance().isHost(userId) /**&& !LiveRoomInfoProvider.getInstance().isOfficalLive()**/) {
                LogUtil.i(LiveLogTag.IOLiveTag, "onHostExit");
                checkliveStatus();
            } else {
                //连麦的用户处理
                activity.mTRTCVideoViewLayout.onMemberLeave(userId);

                OnlineMember onlineMember = OnlineMembersHelper.getInstance().getMemberByUserId(userId);

                //不是主播主动关闭的同台直播用户离开，主播会收到提示
                if (onlineMember != null && !onlineMember.isHostCloseExpore && LiveRoomInfoProvider.getInstance().isHost()) {

                    OMAppApiProvider.getInstance().needShowLeavePop(LiveRoomInfoProvider.getInstance().liveId,
                            userId, new Observer<LiveNeedShowLeavePop>() {
                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }

                                @Override
                                public void onNext(LiveNeedShowLeavePop response) {
                                    if (response.isSuccess() && "0".equals(response.data)) {
                                        showLeavePop(userId, activity);
                                    }
                                }
                            });
                }
            }
        }

        OnlineMembersHelper.getInstance().onMemeberLeave(userId);
        activity.onExploreMemberCountChange();
    }

    private void showLeavePop(String userId, Context activity) {
        OMAppApiProvider.getInstance().mGetUserByUserId(userId, new Observer<OnlineUserInfoResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(OnlineUserInfoResponse response) {
                String userName = response.data.trueName;
                if (!TextUtils.isEmpty(userName)) {
                    new TJMakeSureDialog(activity, null)
                            .setTitleAndCotent("", userName + "结束互动")
                            .setOkText("知道了").hideTitle().showAlert();
                }

            }
        });
    }

    /**
     * 有用户屏蔽了画面
     */
    @Override
    public void onUserVideoAvailable(final String userId, boolean available) {
        LogUtil.i(LiveLogTag.IOLiveTag, "onUserVideoAvailable:" + userId + available);

        BaseLiveAct activity = mContext.get();
        if (activity != null) {
            if (available) {
                final TXCloudVideoView renderView = activity.mTRTCVideoViewLayout.onMemberEnter(userId);

                if (LiveRoomInfoProvider.getInstance().isHost(userId)) {
                    try {
                        renderView.requestLayout();
                    } catch (Exception e) {
                    }
                }

                if (renderView != null) {
                    LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "startRemoteView--onUserVideoAvailable");
                    activity.mTrtcCloud.startRemoteView(userId, renderView);
                    activity.runOnUiThread(() -> renderView.setUserId(userId));
                }

                if (LiveRoomInfoProvider.getInstance().isHost(userId)) {
                    //首次渲染好主播的视频
                    if (!activity.isHostVideoAvailable) {
                        activity.isHostVideoAvailable = true;
                    }
                }
            } else {
                activity.mTrtcCloud.stopRemoteView(userId);
                LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "stopRemoteView--onUserVideoAvailable");
                if (!LiveRoomInfoProvider.getInstance().hasTemplateLeave) {
                    //观众会短暂性的收到主播的离线，不能直接退出，应查询是否直播已结束
                    if (LiveRoomInfoProvider.getInstance().isHost(userId)) {
                        LogUtil.i(LiveLogTag.IOLiveTag, "checkliveStatus - onUserVideoAvailable");
                        checkliveStatus();
                        return;
                    }
                }
            }
            activity.mTRTCVideoViewLayout.updateVideoStatus(userId, available);
        }

        //三期处理主播的视频关闭功能
        if (!OnlineMembersHelper.getInstance().isHostVideoOn) {
            activity.mTRTCVideoViewLayout.muteGuestVideo(LiveRoomInfoProvider.getInstance().hostUserId, true);
        }

        if (!OnlineMembersHelper.getInstance().isHostAudioOn) {
            activity.mTRTCVideoViewLayout.muteGuestAudio(LiveRoomInfoProvider.getInstance().hostUserId, true);
        }
    }


    /**
     * 有用户屏蔽了声音
     */
    @Override
    public void onUserAudioAvailable(String userId, boolean available) {

        LogUtil.i(LiveLogTag.IOLiveTag, "onUserAudioAvailable:" + userId + available);

        BaseLiveAct activity = mContext.get();
        if (activity != null) {
            if (available) {
                final TXCloudVideoView renderView = activity.mTRTCVideoViewLayout.onMemberEnter(userId);
                if (renderView != null) {
                    renderView.setVisibility(View.VISIBLE);
                }
            }
        }

        //三期处理主播的视频关闭功能
        if (!OnlineMembersHelper.getInstance().isHostVideoOn) {
            activity.mTRTCVideoViewLayout.muteGuestVideo(LiveRoomInfoProvider.getInstance().hostUserId, true);
        }

        if (!OnlineMembersHelper.getInstance().isHostAudioOn) {
            activity.mTRTCVideoViewLayout.muteGuestAudio(LiveRoomInfoProvider.getInstance().hostUserId, true);
        }
    }


    @Override
    public void onUserVoiceVolume(ArrayList<TRTCCloudDef.TRTCVolumeInfo> userVolumes, int totalVolume) {

    }


    /**
     * 网络情况实时回调
     */
    @Override
    public void onNetworkQuality(TRTCCloudDef.TRTCQuality localQuality, ArrayList<TRTCCloudDef.TRTCQuality> remoteQuality) {

    }

    /**
     * 收到房间内消息
     *
     * @param userId
     * @param cmdId
     * @param seq
     * @param message
     */
    @Override
    public void onRecvCustomCmdMsg(String userId, int cmdId, int seq, byte[] message) {
        super.onRecvCustomCmdMsg(userId, cmdId, seq, message);
    }

    /**
     * TIC 消息回调，暂时保留一版本 V3.2.2
     *
     * @param message
     */
    public void onReceiveTicCmdMsg(byte[] message) {

        LiveCustomMessage customMessage = LiveCustomMessageProvider.getCustomMessageExtra(message);
        if (customMessage == null) {
            return;
        }

        if (mContext.get() == null) {
            return;
        }

        BaseLiveAct activity = mContext.get();
        if (activity == null) {
            return;
        }

        if (TextUtils.isEmpty(customMessage.toUserId)) {
            customMessage.toUserId = "";
        }

        LogUtil.i(LiveLogTag.LRMsgTag, customMessage.toString());

        //过滤自己发的
        String myUserId = BaseUserInfoCache.getUserId(mContext.get());

        //切换大会音频
        if (LiveRoomInfoProvider.getInstance().hostUserId.equals(customMessage.fromUserId)
                && customMessage.fromUserId.equals(customMessage.toUserId)) {

            if (customMessage.msgCode == LiveCustomMessageCode.CLOSE_AUDIO.getCode()) {
                activity.switchMeetingAudio(true);
                OnlineMembersHelper.getInstance().doStopAudio(customMessage.toUserId);
                activity.closeGuestOperationView();
                return;
            }

            if (customMessage.msgCode == LiveCustomMessageCode.OPEN_AUDIO.getCode()) {
                activity.switchMeetingAudio(false);
                OnlineMembersHelper.getInstance().doStartAudio(customMessage.toUserId);
                activity.closeGuestOperationView();
                return;
            }
        }

        if (BaseUserInfoCache.isMySelf(mContext.get(), customMessage.fromUserId)) {
            return;
        }

        //过滤不是发给自己的单聊消息
        if (customMessage.type == 0 && !myUserId.equals(customMessage.toUserId)) {
            return;
        }

        int cmdId = customMessage.msgCode;

        /**
         * ==================== 同台直播
         */

        //主播 & 助手 收到上麦申请
        if (cmdId == LiveCustomMessageCode.APPLY_EXPORE.getCode() && !LiveRoomInfoProvider.getInstance().isGuest()) {
            mContext.get().receiveExporeApply(customMessage);
            LiveCustomMessageProvider.sendLiveRoomCustomMessage(customMessage.fromUserId, LiveCustomMessageProvider.getHasReceiveOnlineTipData(activity, customMessage.fromUserId), false);
        }

        if (cmdId == LiveCustomMessageCode.HAS_RECEIVE_TIP.getCode()) {
            activity.needResendApplyOnline = false;
        }

        //观众 & 助手(主播) 收到上麦同意
        if (cmdId == LiveCustomMessageCode.AGREE_EXPORE.getCode()) {
            activity.startExplore(customMessage.toUserId, customMessage.extroId);
        }

        //观众 & 助手(主播) 收到上麦拒绝
        if (cmdId == LiveCustomMessageCode.REFUSE_EXPORE.getCode()) {
            if (BaseUserInfoCache.isMySelf(activity, customMessage.toUserId)) {
                new TJMakeSureDialog(activity, v1 -> {

                }).setOkText("知道了").setTitleAndCotent("", "您的申请已被拒绝\n" +
                        "   无法再次申请！").hideTitle().showAlert();
            }


            //如果当前主播 & 助手的申请列表在显示 需要刷新状态
            if (!LiveRoomInfoProvider.getInstance().isGuest()) {
                activity.refreshOnlineExplorePop(customMessage.toUserId, false);
            }
        }

        //观众 & 助手 收到上麦邀请
        if (cmdId == LiveCustomMessageCode.INVITE_EXPORE.getCode()) {
            activity.inviteExplore(customMessage.fromUserId, customMessage.extroId);
        }

        String title = TextUtils.isEmpty(TextConfigCacheManager.getInstance(mContext.get()).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1)) ? "同台互动" :
                TextConfigCacheManager.getInstance(mContext.get()).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1);


        //主播收到观众的拒绝同台邀请消息
        if (cmdId == LiveCustomMessageCode.REFUSE_INVITE_EXPORE.getCode()) {
            // 禅道#27560：只要不是观众就弹
            if (!LiveRoomInfoProvider.getInstance().isGuest()) {
                new TJMakeSureDialog(activity, null)
                        .setTitleAndCotent("", customMessage.fromUserName +
                                ("拒绝了" + title))
                        .setOkText("知道了").hideTitle().showAlert();
            }
        }

        //观众收到 ：主播关闭自己的同台信息
        if (cmdId == LiveCustomMessageCode.CLOSE_EXPLORE.getCode()) {
            activity.disExploreMe();

            if (LiveRoomInfoProvider.getInstance().isGuest()) {
                new TJMakeSureDialog(activity, null)
                        .setTitleAndCotent("", "您被移出" + title)
                        .setOkText("知道了").hideTitle().showAlert();
            }
        }

        //观众&助手 收到关闭自己的语音
        if (cmdId == LiveCustomMessageCode.CLOSE_AUDIO.getCode()) {
            if (BaseUserInfoCache.getUserId(activity).equals(customMessage.toUserId)) {
                activity.switchGuestAudio(true);
            }

            if (!TextUtils.isEmpty(customMessage.toUserId) && customMessage.toUserId.length() > 10) {
                activity.switchOutLiveAudio(customMessage.toUserId, true);
            }

            //助手&主播更新自己的状态
            OnlineMembersHelper.getInstance().doStopAudio(customMessage.toUserId);
            activity.closeGuestOperationView();

            if (LiveRoomInfoProvider.getInstance().isOfficalLive() /**&& LiveRoomInfoProvider.getInstance().isHost()**/ && LiveRoomInfoProvider.getInstance().isHost(customMessage.toUserId)){
                activity.switchMeetingAudio(true);
            }
        }

        //观众&助手 收到开启自己的语音
        if (cmdId == LiveCustomMessageCode.OPEN_AUDIO.getCode()) {
            // 20200327：新增，主播若暂离收到开启主播语音的消息过滤掉
            if (LiveRoomInfoProvider.getInstance().isHost(customMessage.toUserId) && LiveRoomInfoProvider.getInstance().hasTemplateLeave) {
                return;
            }
            // 天九云洽会注释调此拦截，老板云版本控制问题需要放开
//            if (LiveRoomInfoProvider.getInstance().isHost(customMessage.toUserId) && !activity.mTRTCVideoViewLayout.checkBigUpIsHostNow()) {
//                // 收到的被开启语音对象是主播，判断当前主播是否在大屏幕，不在大屏幕就return不处理
//                return;
//            }

            // 如果被开启的是自己
            if (BaseUserInfoCache.getUserId(activity).equals(customMessage.toUserId)) {
                LogUtil.i(LiveLogTag.LRMsgTag, "OPEN_AUDIO_switchGuestAudio");
                activity.switchGuestAudio(false);
            }

            // 如果被开启的是引流的
            if (!TextUtils.isEmpty(customMessage.toUserId) && customMessage.toUserId.length() > 10) {
                LogUtil.i(LiveLogTag.LRMsgTag, "OPEN_AUDIO_switchOutLiveAudio");
                activity.switchOutLiveAudio(customMessage.toUserId, false);
            }

            //助手&主播更新自己的状态
            OnlineMembersHelper.getInstance().doStartAudio(customMessage.toUserId);
            activity.closeGuestOperationView();

            if (LiveRoomInfoProvider.getInstance().isOfficalLive() /**&& LiveRoomInfoProvider.getInstance().isHost()**/ && LiveRoomInfoProvider.getInstance().isHost(customMessage.toUserId)){
                activity.switchMeetingAudio(false);
            }
        }

        //观众&助手 收到关闭自己的视频
        if (cmdId == LiveCustomMessageCode.CLOSE_VIDEO.getCode()) {
            if (LiveRoomInfoProvider.getInstance().isHost(customMessage.toUserId)) {
                OnlineMembersHelper.getInstance().isHostVideoOn = false;
            }
            //观众收到关闭自己的视频后显示默认头像
            activity.switchGuestVideo(true, customMessage.toUserId);
//            //助手&主播更新自己的状态
            OnlineMembersHelper.getInstance().doStopVideo(customMessage.toUserId);
            activity.closeGuestOperationView();
        }

        //观众&助手 收到开启自己的视频
        if (cmdId == LiveCustomMessageCode.OPEN_VIDEO.getCode()) {
            if (LiveRoomInfoProvider.getInstance().isHost(customMessage.toUserId)) {
                OnlineMembersHelper.getInstance().isHostVideoOn = true;
            }

            //观众收到开启自己的视频后显示视频
            activity.switchGuestVideo(false, customMessage.toUserId);
            //助手&主播更新自己的状态
            OnlineMembersHelper.getInstance().doStartVideo(customMessage.toUserId);
            activity.closeGuestOperationView();
        }

        //屏幕切换
        if (cmdId == LiveCustomMessageCode.HOST_VIDEO_DOWN.getCode()) {
            activity.exchangeGuestScreen(false, customMessage.extroId);
        }

        //屏幕切换
        if (cmdId == LiveCustomMessageCode.HOST_VIDEO_UP.getCode()) {
            activity.exchangeGuestScreen(true, "");
        }

        /**
         * ===================== 暂离
         */
        //主播暂离直播间
        if (cmdId == LiveCustomMessageCode.TEMP_EXIT_ROOM_NEW.getCode()) {
            LiveTemplateLeaveMsg liveTemplateLeaveMsg = new Gson().fromJson(customMessage.extroId, LiveTemplateLeaveMsg.class);
            if (LiveRoomInfoProvider.getInstance().isHost()) {
                activity.templateLive(liveTemplateLeaveMsg.reason, liveTemplateLeaveMsg.time);
            }

            LiveRoomInfoProvider.getInstance().hasTemplateLeave = true;
            activity.refreshTempLiveTip(liveTemplateLeaveMsg.reason, liveTemplateLeaveMsg.time, false);

            if (LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                activity.templateLiveMeeting();
            }

        }

        //主播重播
        if (cmdId == LiveCustomMessageCode.RESTART_LIVE.getCode()) {
            if (LiveRoomInfoProvider.getInstance().isHost()) {
                activity.finishTempleteLeave();
            } else {
                LiveRoomInfoProvider.getInstance().hasTemplateLeave = false;
                activity.refreshTempLiveTip("", "", false);
            }

            if (LiveRoomInfoProvider.getInstance().isHostHelper()) {
                activity.mTRTCVideoViewLayout.refreshTemplateLeaveTip("", "", false);
            }

            if (LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                activity.templateLiveMeeting();
            }
        }


        /**
         * ===================== 白板
         */
        //显示白板
        if (cmdId == LiveCustomMessageCode.START_DRAW_POP.getCode()) {
            activity.showDrawPad(false);
        }

        //隐藏白板
        if (cmdId == LiveCustomMessageCode.STOP_DRAW_POP.getCode()) {
            activity.disDrawPad(false);
        }

        // 观众&主播&助手 收到显示白板
        if (cmdId == LiveCustomMessageCode.OPEN_DOCUMENT_WB_ID.getCode()) {
            activity.showFileShow(customMessage.extroId, false);
        }

        // 通知观众端关闭白板
        if (cmdId == LiveCustomMessageCode.CLOSE_DOCUMENT_WB.getCode()) {
            activity.disShowFile();
        }

        // 通知观众端显示文档演示--视频
        if (cmdId == LiveCustomMessageCode.OPEN_DOCUMENT_VIDEO.getCode()) {
            activity.showDoumentVideo(new Gson().fromJson(customMessage.extroId, DoumentVideoSendMsg.class), false);
        }

        // 通知观众端关闭文档演示--视频
        if (cmdId == LiveCustomMessageCode.CLOSE_DOCUMENT_VIDEO.getCode()) {
            activity.disDoumentVideo();
        }

        /**
         * ===================== 引流
         */

        // 通知其他端开启引流
        if (cmdId == LiveCustomMessageCode.SHOWOUTLINK.getCode()) {
            Gson gson = new Gson();
            Type founderListType = new TypeToken<ArrayList<SelectOutLiveModel>>() {
            }.getType();

            try {
                List<SelectOutLiveModel> selectedArray = gson.fromJson(customMessage.extroId, founderListType);
                if (selectedArray == null) {
                    return;
                }

                for (SelectOutLiveModel model : selectedArray) {
                    String outlinkUrl = model.pullFlvUrl;
                    String outlinkRtmp = model.pullRtmpUrl;
                    String streamId = model.streamId;
                    activity.showOuterLive(outlinkUrl, outlinkRtmp, streamId, false);
                }
            } catch (Exception e) {

            }

        }

        // 通知其他端关闭引流
        if (cmdId == LiveCustomMessageCode.STOPOUTLINK.getCode()) {
            activity.disOuterLive(customMessage.extroId, false, null);
        }

        // 直播、同台接听电话
        if (cmdId == LiveCustomMessageCode.CALL_OFF_HOOK.getCode()) {
            activity.callOffHook(customMessage.fromUserId);
        }

        // 直播、同台挂断电话
        if (cmdId == LiveCustomMessageCode.CALL_IDLE.getCode()) {
            activity.callIdle(customMessage.fromUserId);
        }
    }

    /**
     * 自己断开连接回调：此时已经断了35s了
     */
    @Override
    public void onConnectionLost() {
        super.onConnectionLost();
//        BaseLiveAct activity = mContext.get();
//        if (activity == null) {
//            return;
//        }
//        //连麦观众自己异常下麦 需修改同台申请按钮状态 并关闭自己的本地音视频
//        if (LiveRoomInfoProvider.getInstance().isGuest()
//                && activity.mLiveOperationPop.mSameTableState == 1) {
//            mConnectHandler.sendEmptyMessage(1000);
//        }
    }

//    private Handler mConnectHandler = new Handler() {
//        @SuppressLint("HandlerLeak")
//        @Override
//        public void handleMessage(Message msg) {
//            if (msg.what == 1000) {
//                mConnectHandler.sendEmptyMessageDelayed(1001, 1000 * 100);
//            } else if (msg.what == 1001) {
//                BaseLiveAct activity = mContext.get();
//                if (activity == null) {
//                    return;
//                }
//
//                //连麦观众自己异常下麦 需修改同台申请按钮状态 并关闭自己的本地音视频
//                if (LiveRoomInfoProvider.getInstance().isGuest()
//                        && OnlineMembersHelper.getInstance().isMyselfExploring(activity)) {
//                    activity.mLiveOperationPop.mSameTableState = 0;
//                    activity.mLiveOperationPop.refreshSameTableState();
//                    activity.mTrtcCloud.muteLocalAudio(false);
//                    activity.mTrtcCloud.stopLocalPreview();
//                    activity.mTrtcCloud.stopLocalAudio();
//                    activity.mTRTCVideoViewLayout.onMemberLeave(BaseUserInfoCache.getUserId(activity));
//                    OnlineMembersHelper.getInstance().onMemeberLeave(BaseUserInfoCache.getUserId(activity));
//                }
//            }
//        }
//    };


    @Override
    public void onConnectionRecovery() {
        super.onConnectionRecovery();
//        BaseLiveAct activity = mContext.get();
//        if (activity == null) {
//            return;
//        }
//        //连麦观众自己异常下麦 需修改同台申请按钮状态 并关闭自己的本地音视频
//        if (LiveRoomInfoProvider.getInstance().isGuest()
//                && OnlineMembersHelper.getInstance().isMyselfExploring(activity)) {
//            mConnectHandler.removeMessages(1000);
//            mConnectHandler.removeMessages(1001);
//        }
    }

    //只有非官方大会的主播才可调用因为会调结束直播的口
    public void checkliveStatus() {
        LogUtil.i(LiveLogTag.IOLiveTag, "checkliveStatus");
        //重连后检测当前直播间状态
        LiveRoomIOHelper.checkLiveRoom(new IOLiveRoomListener() {
            @Override
            public void onIOError(String error, int errorCode) {
                BaseLiveAct activity = mContext.get();
                if (activity == null) {
                    return;
                }

                LogUtil.i(LiveLogTag.IOLiveTag, "checkliveStatus - StopLive");


                if (activity.isExitingRoom) {
                    return;
                }

                activity.stopLive(false, false);
            }

            @Override
            public void onIOSuccess() {
                LogUtil.i(LiveLogTag.IOLiveTag, "checkliveStatus - onIOSuccess¬");
            }
        });
    }

    /**
     * 旁路直播相关处理
     *
     * @param err
     * @param errMsg
     */
    @Override
    public void onStartPublishCDNStream(int err, String errMsg) {

    }

    @Override
    public void onStopPublishCDNStream(int err, String errMsg) {

    }

    /**
     * 渲染自定义视频回调
     *
     * @param userId
     * @param streamType
     * @param frame
     */
    @Override
    public void onRenderVideoFrame(String userId, int streamType, TRTCCloudDef.TRTCVideoFrame frame) {

    }


    /**
     * 数据统计，暂不做处理
     *
     * @param statics
     */
    @Override
    public void onStatistics(TRTCStatistics statics) {
    }

    /**
     * 跨房连麦，暂不做处理，
     *
     * @param userID
     * @param err
     * @param errMsg
     */
    @Override
    public void onConnectOtherRoom(final String userID, final int err, final String errMsg) {

    }

    @Override
    public void onDisConnectOtherRoom(final int err, final String errMsg) {

    }

}