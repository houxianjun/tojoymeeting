package com.tojoy.cloud.online_meeting.App.Live.Live.Helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.tojoy.tjoybaselib.services.log.BaseLiveLog;

/**
 * @author qll
 * 动态注册广播接收器监听去电信息
 */
public class InnerOutCallReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // 获取播出的去电号码
        String outPhone = getResultData();
        Log.i(BaseLiveLog.BaseLiveTag, "outPhone:" + outPhone);
        TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        mTelephonyManager.listen(new MyPhoneStateListener(), PhoneStateListener.LISTEN_CALL_STATE);

    }
}
