package com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.view;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.fragment.DistributionMyWithdrawalFragment;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.Services.PayAndShare.callback.PayChannelStatusCallBack;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 我的提现-提现账号页面
 */
public class DistributionWithdrawalAccountView extends RelativeLayout {
    Context mContext;
    //立即绑定支付宝渠道
    private TextView bindChannelTV;
    //账户昵称
    private TextView accountNickNameTV;

    private DistributionMyWithdrawalFragment mainFragment;

    public void setMainFragment(DistributionMyWithdrawalFragment mainFragment) {
        this.mainFragment = mainFragment;
    }

    public DistributionWithdrawalAccountView(Context context) {
        super(context);
        initView(context);
    }

    public DistributionWithdrawalAccountView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public void initView(Context context) {
        mContext = context;
        View mView = LayoutInflater.from(context).inflate(R.layout.layout_distribution_withdrawal_account, null);
        initUI(mView);
        this.addView(mView);
    }

    private void initUI(View layout) {
        bindChannelTV = layout.findViewById(R.id.tv_bind_channel);
        bindChannelTV.setOnClickListener(v -> {
            //连续点击限制
            if (!FastClickAvoidUtil.isFastDoubleClick(bindChannelTV.getId())) {
                mainFragment.getPayAliChannel().bingPayChannel((Activity) mContext, new PayChannelStatusCallBack<String>() {
                    @Override
                    public void onFail(String msg) {
                        // Toasty.normal(mContext,msg).show();
                    }

                    @Override
                    public void onSuccess(String content) {

                        queryAliAccountBinding(content);
                    }
                });
            }
        });
        accountNickNameTV = layout.findViewById(R.id.tv_account_nickname);
    }

    /**
     * 更新绑定按钮文案
     *
     * @param status
     * @param name
     */
    public void updateBindStatus(int status, String name) {
        switch (status) {
            //未绑定
            case 0:
                accountNickNameTV.setVisibility(GONE);
                bindChannelTV.setVisibility(VISIBLE);
                break;
            //已绑定
            case 1:
                accountNickNameTV.setVisibility(VISIBLE);
                bindChannelTV.setVisibility(GONE);
                accountNickNameTV.setText(name);
                break;
            default:
                break;
        }
    }

    /**
     * 获取用户绑定
     */
    public void queryAliAccountBinding(String authCode) {
        OMAppApiProvider.getInstance().queryAliAccountBinding(authCode, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(OMBaseResponse response) {
                if (response.isSuccess()) {
                    mainFragment.queryAliAccountBindingStatus();
                } else {

                    Toasty.normal(mContext, response.msg).show();
                }
            }
        });
    }
}
