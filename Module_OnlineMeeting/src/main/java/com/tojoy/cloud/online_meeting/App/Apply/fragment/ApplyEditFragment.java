package com.tojoy.cloud.online_meeting.App.Apply.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.core.LogisticsCenter;
import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.internal.OSSAsyncTask;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.configs.UserPermissions;
import com.tojoy.tjoybaselib.model.meetingperson.AttendeePersonModel;
import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.model.meetingperson.SingleTonModel.MeetingPersonTon;
import com.tojoy.tjoybaselib.services.OnlineMeeting.ApplyStateCode;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.oss.OSSService;
import com.tojoy.tjoybaselib.services.oss.ProgressCallback;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.CustomAlertDialog;
import com.tojoy.tjoybaselib.ui.editView.ClearEmojInputFilter;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.App.Apply.activity.ApplyAct;
import com.tojoy.cloud.online_meeting.App.Apply.adapter.SubmitFileOrCoverAdapter;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApplyJoinLiveModel;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApplyPowerTagModel;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApplyPhotoInfo;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.App.BaseFragment;
import com.tojoy.common.Widgets.CommonParamsPop.CommonParam;
import com.tojoy.common.Widgets.CommonParamsPop.CommonParamsPop;
import com.tojoy.file.browser.FileViewAct;
import com.tojoy.image.photopicker.PickImageHelper;
import com.tojoy.provider.OSSFileNameProvider;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.dmoral.toasty.Toasty;

/**
 * @author qll
 * @date 2019/3/29
 * 开会申请--可编辑
 * ApplyOver（未申请）、ApplyError（驳回，需将上次传的信息回显）、ApplyRecall（撤回申请，需将上次传的信息回显）可编辑
 * 其他：查看详情，只回显数据，不可编辑上传
 */
public class ApplyEditFragment extends BaseFragment implements View.OnClickListener {
    // 封面最多数量
    protected static final int PICK_IMAGE_COUNT = 1;
    // 文件最多数量
    protected static final int PICK_FILE_COUNT = 5;
    // 标签最多可选数量
    protected static final int SELECT_TAG_COUNT = 10;
    // 还可选择的文档数量
    private int multiSelectFileMaxCount = PICK_FILE_COUNT;
    // 还可以选择的封面的数量
    private int multiSelectCoverMaxCount = PICK_IMAGE_COUNT;


    private TagFlowLayout mIdflowlayout;
    private TextView mRoomIdTv, mRoomLookCountTv, mEmsTv, mSubmitLeaderTv, mRoomLookPowerTv, mRoomDrainageTypeTv, mRoomInvitationPeopleTv, mRoomPlayBackPowerTv;
    private EditText mStartLiveReasonEdit;
    private EditText mEditRoomName;
    private EditText mEditRoomPsw;
    private Button mSubmitExamineBt;
    private RecyclerView mRoomCoverUploadRecyclerView, mRoomFileUploadRecyclerView;
    private RelativeLayout mRoomLookCountRlv;
    private RelativeLayout mRoomLookPowerRlv;
    private RelativeLayout mLeaderRlv;
    private RelativeLayout mRoomDrainageTypeRlv;
    private RelativeLayout mRoomInvitationCountRLv;
    private RelativeLayout mRoomPlayBackPowerRlv;
    private SubmitFileOrCoverAdapter mSubmitCoverAdapter;
    private SubmitFileOrCoverAdapter mSubmitFileAdapter;
    private TagAdapter mTagAdapter;


    private List<ApplyPowerTagModel> mTagData = new ArrayList();
    protected List<ApplyPhotoInfo> mCoverInfos = new ArrayList<>();
    protected List<ApplyPhotoInfo> mFileInfos = new ArrayList<>();

    // 开会申请字数超出提示
    private boolean mIsFirstShowToast = true;
    // 会议主题字数超过限制
    private boolean mIsFirstShowToastForName = true;
    // 会议密码字数超过限制
    private boolean mIsFirstShowToastForPws = true;

    private int gridCountHor = 3;

    private ApplyJoinLiveModel applyJoinLiveModel;

    private String mTagId = "";
    private String sumitPicUrl = "";
    private String submitDocUrl = "";
    private String submitDocName = "";
    private String mPowerKey = "";
    private String mDrainageTypeKey = "";
    private String mLookCount = "";
    // 是否生成回放（0：不生成回放、1：生成回放）
    private String mPlaybackKey = "0";

    private String userIds = "";
    private String mobiles = "";

    private String tmpuserIds = "";
    private String tmpmobiles = "";

    //保存参会人字符串相关的对象
    AttendeePersonModel attendeePersonModel = new AttendeePersonModel();

    public void setAttendeePersonModel(AttendeePersonModel attendeePersonModel) {
        this.attendeePersonModel = attendeePersonModel;
    }

    MeetingPersonDepartmentResponse.DepartmentModel departPersonModel;

    public MeetingPersonDepartmentResponse.DepartmentModel getDepartPersonModel() {
        return departPersonModel;
    }

    public void setDepartPersonModel(MeetingPersonDepartmentResponse.DepartmentModel departPersonModel) {
        this.departPersonModel = departPersonModel;
    }

    //已添加的外部人员列表集合
    List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> outSidePersonLists = new ArrayList<>();

    ApplyStateCode stateCode;

    ArrayList<CommonParam> peopleParams = new ArrayList<>();
    ArrayList<CommonParam> peoplePworParams = new ArrayList<>();
    ArrayList<CommonParam> drainageParams = new ArrayList<>();
    ArrayList<CommonParam> playBackPowerParams = new ArrayList<>();

    @SuppressLint("ValidFragment")
    public ApplyEditFragment() {
    }

    public ApplyEditFragment setApplyStateCode(ApplyJoinLiveModel info) {
        this.applyJoinLiveModel = info;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_apply_edit_detail, container, false);
        }
        isCreateView = true;
        return mView;
    }

    /**
     * 初始化本页面涉及弹窗的列表
     */
    private void initListPopParams() {
        // 初始化选择框列表的值
        // 1、初始化选择参会人数列表
        for (int p = 0; p < applyJoinLiveModel.peopleNumList.size(); p++) {
            CommonParam commonParam = new CommonParam(applyJoinLiveModel.peopleNumList.get(p).personNumber);
            commonParam.code = applyJoinLiveModel.peopleNumList.get(p).personNumberId;
            if (!TextUtils.isEmpty(mLookCount) && mLookCount.equals(commonParam.name)) {
                commonParam.isSelected = true;
            } else {
                commonParam.isSelected = false;
            }
            peopleParams.add(commonParam);
        }
        mView.findViewById(R.id.iv_right_room_look_count).setVisibility(peopleParams.size() > 1 ? View.VISIBLE : View.GONE);

        // 2、初始化参会权限列表
        CommonParam peoplePowerParam1 = new CommonParam("内外部人员可参会");
        peoplePowerParam1.code = "1";
        CommonParam peoplePowerParam2 = new CommonParam("仅内部人员可参会");
        peoplePowerParam2.code = "2";
        CommonParam peoplePworParam3 = new CommonParam("仅邀请人员可参会");
        peoplePworParam3.code = "3";

        if ("1".equals(mPowerKey)) {
            peoplePowerParam1.isSelected = true;
        } else if ("2".equals(mPowerKey)) {
            peoplePowerParam2.isSelected = true;
        } else if ("3".equals(mPowerKey)) {
            peoplePworParam3.isSelected = true;
        }
        peoplePworParams.add(peoplePowerParam1);
        peoplePworParams.add(peoplePowerParam2);
        peoplePworParams.add(peoplePworParam3);
        // 3、初始化引流权限列表
        CommonParam commonParam3 = new CommonParam("可被其他会议引用本会议画面");
        commonParam3.code = "1";
        CommonParam commonParam4 = new CommonParam("不可被其他会议引用本会议画面");
        commonParam4.code = "0";
        if ("1".equals(mDrainageTypeKey)) {
            commonParam3.isSelected = true;
        } else if ("0".equals(mDrainageTypeKey)) {
            commonParam4.isSelected = true;
        }
        drainageParams.add(commonParam3);
        drainageParams.add(commonParam4);

        // 4、初始化是否生成回放的权限
        CommonParam commonParam5 = new CommonParam("生成");
        commonParam5.code = "1";
        CommonParam commonParam6 = new CommonParam("不生成");
        commonParam6.code = "0";
        if ("1".equals(mPlaybackKey)) {
            commonParam5.isSelected = true;
        } else if ("0".equals(mPlaybackKey)) {
            commonParam6.isSelected = true;
        }
        playBackPowerParams.add(commonParam5);
        playBackPowerParams.add(commonParam6);
    }

    @Override
    protected void initUI() {
        super.initUI();
        mIdflowlayout = mView.findViewById(R.id.id_flowlayout);
        mRoomIdTv = mView.findViewById(R.id.tv_room_id);
        mRoomLookCountTv = mView.findViewById(R.id.tv_room_look_count);
        mStartLiveReasonEdit = mView.findViewById(R.id.edit_start_live_reason);
        mEmsTv = mView.findViewById(R.id.tv_Ems);
        mSubmitLeaderTv = mView.findViewById(R.id.tv_submit_leader);
        mSubmitExamineBt = mView.findViewById(R.id.bt_submit_examine);
        mRoomCoverUploadRecyclerView = mView.findViewById(R.id.rlv_room_cover_upload);
        mRoomFileUploadRecyclerView = mView.findViewById(R.id.rlv_room_file_upload);
        mRoomLookCountRlv = mView.findViewById(R.id.rlv_room_look_count);
        mRoomLookPowerRlv = mView.findViewById(R.id.rlv_room_look_power);
        mRoomLookPowerTv = mView.findViewById(R.id.tv_room_look_power);
        mLeaderRlv = mView.findViewById(R.id.rlv_leader);
        mRoomDrainageTypeRlv = mView.findViewById(R.id.rlv_room_drainage_type);
        mRoomDrainageTypeTv = mView.findViewById(R.id.tv_room_drainage_type);
        mRoomInvitationCountRLv = mView.findViewById(R.id.rlv_room_invitation_count);
        mEditRoomName = mView.findViewById(R.id.edit_room_name);
        mEditRoomPsw = mView.findViewById(R.id.edit_room_psw);
        mRoomInvitationPeopleTv = mView.findViewById(R.id.tv_room_invitation_count);
        mRoomPlayBackPowerRlv = mView.findViewById(R.id.rlv_room_playback);
        mRoomPlayBackPowerTv = mView.findViewById(R.id.tv_room_playback_power);

        gridCountHor = (AppUtils.getScreenWidth(getContext()) - AppUtils.dip2px(getContext(), 37) * 2) / AppUtils.dip2px(getContext(), 80);
        initListener();
        if (applyJoinLiveModel != null && applyJoinLiveModel.lastParams != null) {
            setJoinApplyInfo(applyJoinLiveModel.lastParams);
        } else {
            setJoinApplyInfo(applyJoinLiveModel);
        }
        stateCode = (applyJoinLiveModel == null || applyJoinLiveModel.getApplyStatus() == null) ? ApplyStateCode.getStateCode(0) : applyJoinLiveModel.getApplyStatus();

        ((ApplyAct) getActivity()).setEditText(mStartLiveReasonEdit);
    }

    @SuppressLint("NewApi")
    private void initListener() {
        InputFilter[] emojiFilters = {new ClearEmojInputFilter()};
        mStartLiveReasonEdit.setFilters(emojiFilters);
        //禁止换行
        mStartLiveReasonEdit.setOnEditorActionListener((v, actionId, event) -> (event.getKeyCode() == KeyEvent.KEYCODE_ENTER));
        mStartLiveReasonEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int length = charSequence.length();
                if (length > 200) {
                    mStartLiveReasonEdit.setText(mStartLiveReasonEdit.getText().subSequence(0, 200));
                    mStartLiveReasonEdit.setSelection(200);
                    if (mIsFirstShowToast) {
                        Toasty.normal(getContext(), "已达最大字数限制").show();
                        mIsFirstShowToast = false;
                    }
                } else if (length < 200) {
                    mIsFirstShowToast = true;
                }

                int canEditCount = 200 - mStartLiveReasonEdit.getText().toString().length();
                if (canEditCount == 0) {
                    mEmsTv.setTextColor(Color.RED);
                } else {
                    mEmsTv.setTextColor(Color.parseColor("#2a62dd"));
                }
                mEmsTv.setText(String.valueOf(canEditCount));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // 判断重置按钮
                checkRestBt();
            }
        });
        mStartLiveReasonEdit.setOnClickListener(v -> ((UI) getActivity()).showKeyboard(true));
        mStartLiveReasonEdit.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                ((UI) getActivity()).showKeyboard(true);
            }
        });

        mEditRoomName.setFilters(emojiFilters);
        mEditRoomName.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEND
                    || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                return true;
            }
            return false;
        });
        mEditRoomName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int length = charSequence.length();
                if (length > 20) {
                    mEditRoomName.setText(mEditRoomName.getText().subSequence(0, 20));
                    mEditRoomName.setSelection(20);
                    if (mIsFirstShowToastForName) {
                        Toasty.normal(getActivity(), "已达最大字数限制").show();
                        mIsFirstShowToastForName = false;
                    }
                } else if (length < 20) {
                    mIsFirstShowToastForName = true;
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                // 判断重置按钮
                checkRestBt();
            }
        });

        mEditRoomPsw.setFilters(emojiFilters);
        mEditRoomPsw.setOnEditorActionListener((v, actionId, event) -> actionId == EditorInfo.IME_ACTION_SEND
                || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER));
        mEditRoomPsw.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int length = charSequence.length();
                if (length > 6) {
                    mEditRoomPsw.setText(mEditRoomPsw.getText().subSequence(0, 6));
                    mEditRoomPsw.setSelection(6);
                    if (mIsFirstShowToastForPws) {
                        Toasty.normal(getActivity(), "已达最大字数限制").show();
                        mIsFirstShowToastForPws = false;
                    }
                } else if (length < 6) {
                    mIsFirstShowToastForPws = true;
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                // 判断重置按钮
                checkRestBt();
            }
        });


        mRoomLookCountRlv.setOnClickListener(this);
        mSubmitExamineBt.setOnClickListener(this);
        mRoomLookPowerRlv.setOnClickListener(this);
        mRoomDrainageTypeRlv.setOnClickListener(this);
        mRoomInvitationCountRLv.setOnClickListener(this);
        mRoomPlayBackPowerRlv.setOnClickListener(this);
    }

    /**
     * 初始化标签适配器
     */
    private void initTagAdatper() {
        mIdflowlayout.setMaxSelectCount(SELECT_TAG_COUNT);
        mIdflowlayout.setOnTagClickListener((view, position, parent) -> {
            if (!mTagData.get(position).getIsSelect() && mIdflowlayout.getSelectedList().size() == SELECT_TAG_COUNT) {
                if (!FastClickAvoidUtil.isDoubleClick(3000)) {
                    // 其实item的id都是相同的，只是控制提示不弹出那么多次
                    // 若已经选择了最多的标签数量再选择则提示用户最多选择多少个标签
                    Toasty.normal(getContext(), "最多选择" + SELECT_TAG_COUNT + "个标签").show();
                }

            }
            return false;
        });
        mTagAdapter = new TagAdapter<ApplyPowerTagModel>(mTagData) {
            @Override
            public View getView(FlowLayout parent, int position, ApplyPowerTagModel tag) {
                TextView tv = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.item_apply_power_tag, mIdflowlayout, false);
                tv.setText(tag.tagName);
                return tv;
            }

            @Override
            public void onSelected(int position, View view) {
                mTagData.get(position).isSelect = "1";
                TextView textViewSelect = (TextView) view;
                textViewSelect.setTextColor(getResources().getColor(R.color.white));
                textViewSelect.setBackgroundResource(R.drawable.shape_apply_power_tag_select);
                // 判断重置按钮
                checkRestBt();
            }

            @Override
            public boolean setSelected(int position, ApplyPowerTagModel cooperativeTagInfo) {
                if (cooperativeTagInfo.getIsSelect()) {
                    return true;
                } else {
                    return super.setSelected(position, cooperativeTagInfo);
                }
            }

            @Override
            public void unSelected(int position, View view) {
                mTagData.get(position).isSelect = "0";
                TextView textViewNormal = (TextView) view;
                textViewNormal.setTextColor(getResources().getColor(R.color.color_b1b5c0));
                textViewNormal.setBackgroundResource(R.drawable.shape_apply_power_tag_nomal);
            }
        };
        Set<Integer> set = new HashSet<>();
        set.clear();
        for (int i = 0; i < mTagData.size(); i++) {
            if (mTagData.get(i).getIsSelect()) {
                set.add(i);
            }
        }
        // 初始化选中的标签
        mTagAdapter.setSelectedList(set);
        mIdflowlayout.setAdapter(mTagAdapter);
    }

    /**
     * 初始化封面上传适配器
     */
    private void initCoverAdapter(ApplyJoinLiveModel info) {
        if (info.picList != null) {
            if (info.picList.size() <= PICK_IMAGE_COUNT) {
                mCoverInfos.addAll(info.picList);
                multiSelectCoverMaxCount = PICK_IMAGE_COUNT - info.picList.size();
            } else {
                // 服务器返回的大于最多显示的张数
                for (int i = 0; i < info.picList.size(); i++) {
                    if (!TextUtils.isEmpty(info.picList.get(i).ossUrl) && info.picList.get(i).ossUrl.length() > 150) {
                        info.picList.remove(i);
                        break;
                    }
                }
                if (info.picList.size() > PICK_IMAGE_COUNT) {
                    for (int j = 0; j < PICK_IMAGE_COUNT; j++) {
                        mCoverInfos.add(info.picList.get(j));
                    }
                } else {
                    mCoverInfos.addAll(info.picList);
                }
            }

        }
        if (mCoverInfos.size() < PICK_IMAGE_COUNT) {
            // 设置默认封面数据加号
            mCoverInfos.add(new ApplyPhotoInfo());
        }

        GridLayoutManager mCoverLayoutManager = new GridLayoutManager(getContext(), gridCountHor) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        mCoverLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRoomCoverUploadRecyclerView.setLayoutManager(mCoverLayoutManager);
        mSubmitCoverAdapter = new SubmitFileOrCoverAdapter(getContext(), mCoverInfos, ApplyAct.TYPE_EDIT, true);
        mRoomCoverUploadRecyclerView.setAdapter(mSubmitCoverAdapter);
        mSubmitCoverAdapter.setOnItemClickListener(new SubmitFileOrCoverAdapter.OnItemClickListener() {
            @Override
            public void onPhotoClick(int index, ImageView imageView) {
                if (TextUtils.isEmpty(mCoverInfos.get(index).getGileUrl())) {
                    //相册选择照片
                    showSelector(R.string.apply_power_room_cover_upload_title, ApplyAct.REQUEST_PICK_ICON);
                } else {
                    ((ApplyAct) getActivity()).showPhotoView(mCoverInfos.get(index), imageView);
                }
            }

            @Override
            public void onPhotoDelete(int index, String path) {
                deletePhotoView(index, 1, path);
            }

            @Override
            public void onUploadStart(int index) {
                uploadPic(index, true);
            }

            @Override
            public void onCancleUpload(int index) {
                cancleOssTask(index, true);

            }
        });
    }

    private void initFileAdapter(ApplyJoinLiveModel info) {
        if (info.docList != null) {
            mFileInfos.addAll(info.docList);
            multiSelectFileMaxCount = PICK_FILE_COUNT - info.docList.size();
        }
        if (mFileInfos.size() < PICK_FILE_COUNT) {
            // 设置默认封面数据加号
            mFileInfos.add(new ApplyPhotoInfo());
        }

        GridLayoutManager mFIleLayoutManager = new GridLayoutManager(getContext(), gridCountHor) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        mFIleLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRoomFileUploadRecyclerView.setLayoutManager(mFIleLayoutManager);
        mSubmitFileAdapter = new SubmitFileOrCoverAdapter(getContext(), mFileInfos, ApplyAct.TYPE_EDIT, false);
        mRoomFileUploadRecyclerView.setAdapter(mSubmitFileAdapter);
        mSubmitFileAdapter.setOnItemClickListener(new SubmitFileOrCoverAdapter.OnItemClickListener() {
            @Override
            public void onPhotoClick(int index, ImageView imageView) {
                if (TextUtils.isEmpty(mFileInfos.get(index).getGileUrl())) {
                    //相册选择照片
                    showFileSelector(R.string.apply_power_room_cover_upload_title);
                } else {
                    // 判断是文件还是图片
                    if (mFileInfos.get(index).getGileUrl().endsWith("ppt") || mFileInfos.get(index).getGileUrl().endsWith("pptx")) {
                        FileViewAct.startActivity(getActivity(), "文档", mFileInfos.get(index).getGileUrl(), false, null);
                    } else {
                        ((ApplyAct) getActivity()).showPhotoView(mFileInfos.get(index), imageView);
                    }
                }
            }

            @Override
            public void onPhotoDelete(int index, String path) {
                deletePhotoView(index, 2, path);
            }

            @Override
            public void onUploadStart(int index) {
                uploadPic(index, false);
            }

            @Override
            public void onCancleUpload(int index) {
                cancleOssTask(index, false);
            }
        });
    }

    /**
     * 选择封面图片
     */
    private void showSelector(int titleId, final int requestCode) {
        PickImageHelper.PickImageOption option = new PickImageHelper.PickImageOption();
        option.titleResId = titleId;
        option.multiSelect = true;
        option.multiSelectMaxCount = multiSelectCoverMaxCount;
        // 是否裁剪
        option.crop = true;
        option.cropOutputImageWidth = 720;
        option.cropOutputImageHeight = 720;

        CustomAlertDialog coverDialog = new CustomAlertDialog(getContext());
        coverDialog.setTitle(uk.co.senab.photoview.R.string.choose);
        coverDialog.addItem(getContext().getString(R.string.input_panel_take), () -> {
            PickImageHelper.clickPickImageCamera(getActivity(), requestCode, option);
        });
        coverDialog.addItem(getContext().getString(uk.co.senab.photoview.R.string.choose_from_photo_album), () -> {
            PickImageHelper.clickPickImage(getActivity(), requestCode, option);
        });
        coverDialog.show();
    }

    /**
     * 删除封面图片
     */
    public void deletePhotoView(int index, int type, String path) {
        if (TextUtils.isEmpty(path)) {
            return;
        }
        if (type == 1) {
            // 封面
            for (int i = 0; i < mCoverInfos.size(); i++) {
                if (path.equals(mCoverInfos.get(i).getFilePath())
                        || path.equals(mCoverInfos.get(i).ossUrl)) {
                    mCoverInfos.remove(index);
                    mSubmitCoverAdapter.notifyDataSetChanged();
                    break;
                }
            }
            checkCoverCount();
        } else if (type == 2) {
            // 文件
            for (int i = 0; i < mFileInfos.size(); i++) {
                if (path.equals(mFileInfos.get(i).getFilePath())
                        || path.equals(mFileInfos.get(i).ossUrl)) {
                    mFileInfos.remove(index);
                    mSubmitFileAdapter.notifyDataSetChanged();
                    break;
                }
            }
            checkFilesCount();
        }

    }

    /**
     * 选择文件
     */
    private void showFileSelector(int titleId) {
        PickImageHelper.PickImageOption option = new PickImageHelper.PickImageOption();
        option.titleResId = titleId;
        option.multiSelect = true;
        option.multiSelectMaxCount = multiSelectFileMaxCount;
        option.crop = false;
        option.cropOutputImageWidth = 720;
        option.cropOutputImageHeight = 720;

        CustomAlertDialog dialog = new CustomAlertDialog(getContext());
        dialog.setTitle(uk.co.senab.photoview.R.string.choose);
        dialog.addItem(getContext().getString(R.string.input_panel_take), () -> {
            PickImageHelper.clickPickImageCamera(getActivity(), 1004, option);
        });
        dialog.addItem(getContext().getString(uk.co.senab.photoview.R.string.choose_from_photo_album), () -> {
            PickImageHelper.clickPickImage(getActivity(), 1004, option);
        });

        dialog.show();
    }

    /**
     * 设置房间主题名称
     *
     * @param roomName
     */
    public void setRoomName(String roomName) {
        if (!TextUtils.isEmpty(roomName)) {
            mEditRoomName.setText(roomName);
            // 判断重置按钮
            checkRestBt();
        }
    }

    /**
     * 设置房间观人数
     *
     * @param count
     */
    public void setRoomLookCount(String count) {
        if (!TextUtils.isEmpty(count)) {
            mRoomLookCountTv.setText(count + "人");
            mLookCount = count;
            if (!BaseUserInfoCache.getUserIsHavePermiss(getContext(), UserPermissions.NO_APPROVE)) {
                ((ApplyAct) getActivity()).getCheckUser(count);
            }
            // 判断重置按钮
            checkRestBt();
        }
    }

    /**
     * 根据所选观看人数显示对应审批人
     */
    public void setCheckUser(String name) {
        mSubmitLeaderTv.setText(name);
    }

    /**
     * 设置参会权限
     * 若选择：3：仅邀请人员可参会
     * 1）无需设置会议密码
     * 2）无【可否被引用画面】选项，不可被其他会议引流
     * 3）无【邀请参会人】设置项。
     * 注意⚠️：用户首次进入开会申请和重置后显示"内外部人员可参会"默认选项
     */
    public void setLookPower(String power, String powerKey) {
        //每次切换权限时，清空上次选中的状态
        ((ApplyAct) getActivity()).tmpHamapA = new HashMap<>();
        userIds = "";
        mobiles = "";

        if (TextUtils.isEmpty(power)) {
            // 如若为空则默认"内外部人员可参会"
            mRoomLookPowerTv.setText("内外部人员可参会");
            mPowerKey = "1";
            if (peoplePworParams != null) {
                for (CommonParam commonParam : peoplePworParams) {
                    commonParam.isSelected = mPowerKey.equals(commonParam.code);
                }
            }
        } else {
            mRoomLookPowerTv.setText(power);
            mPowerKey = powerKey;
            setInvitationPersons(userIds, mobiles, outSidePersonLists);
        }

        if (!TextUtils.isEmpty(mPowerKey) && "3".equals(mPowerKey)) {
            // 仅邀请人员可参会
            mView.findViewById(R.id.rlv_room_psw).setVisibility(View.GONE);
            mView.findViewById(R.id.v_line_psw).setVisibility(View.GONE);
            mEditRoomPsw.setText("");
            mRoomDrainageTypeRlv.setVisibility(View.GONE);
            for (int i = 0; i < drainageParams.size(); i++) {
                if ("0".equals(drainageParams.get(i).code)) {
                    drainageParams.get(i).isSelected = true;
                    mRoomDrainageTypeTv.setText(drainageParams.get(i).name);
                    mDrainageTypeKey = "0";
                } else {
                    drainageParams.get(i).isSelected = false;
                }
            }
            setInvitationPersons(userIds, mobiles);

        } else {
            mView.findViewById(R.id.rlv_room_psw).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.v_line_psw).setVisibility(View.VISIBLE);
            mRoomDrainageTypeRlv.setVisibility(View.VISIBLE);
        }
        // 判断重置按钮
        checkRestBt();
    }

    /**
     * 设置视频引流权限
     */
    public void setDrainageType(String power, String powerKey) {
        mRoomDrainageTypeTv.setText(power);
        mDrainageTypeKey = powerKey;
        // 判断重置按钮
        checkRestBt();
    }

    /**
     * 设置生成回放的权限
     */
    public void setPlayBackPower(String power, String powerKey) {
        if (TextUtils.isEmpty(power)) {
            // 如若为空则默认"不生成"
            mPlaybackKey = "0";
            mRoomPlayBackPowerTv.setText("不生成");
            if (playBackPowerParams != null) {
                for (CommonParam commonParam : playBackPowerParams) {
                    commonParam.isSelected = mPlaybackKey.equals(commonParam.code);
                }
            }
        } else {
            mPlaybackKey = powerKey;
            mRoomPlayBackPowerTv.setText(power);
        }
        // 判断重置按钮
        checkRestBt();
    }

    /**
     * 添加封面图片
     */
    public void addCoverPhotoView(List<ApplyPhotoInfo> list) {
        if (mCoverInfos.size() > PICK_IMAGE_COUNT - 1) {
            mCoverInfos.remove(mCoverInfos.size() - 1);
            mCoverInfos.addAll(list);
        } else {
            mCoverInfos.remove(mCoverInfos.size() - 1);
            mCoverInfos.addAll(list);
        }

        mSubmitCoverAdapter.notifyDataSetChanged();
        // 判断重置按钮
        checkRestBt();
    }

    /**
     * 添加文件
     */
    public void addFileView(String path, String type) {
        ApplyPhotoInfo info = new ApplyPhotoInfo();
        info.setFilePath(path);
        info.setType(type);
        info.ossUrl = "";

        if (mFileInfos.size() > PICK_FILE_COUNT - 1) {
            mFileInfos.remove(mFileInfos.size() - 1);
            mFileInfos.add(info);
        } else {
            mFileInfos.add(mFileInfos.size() - 1, info);
        }
        mSubmitFileAdapter.notifyDataSetChanged();

        // 判断重置按钮
        checkRestBt();
    }

    /**
     * 添加文件
     */
    public void addFileViews(List<ApplyPhotoInfo> list) {
        if (mFileInfos.size() > PICK_FILE_COUNT - 1) {
            mFileInfos.remove(mFileInfos.size() - 1);
            mFileInfos.addAll(list);
        } else {
            mFileInfos.remove(mFileInfos.size() - 1);
            mFileInfos.addAll(list);
        }
        mSubmitFileAdapter.notifyDataSetChanged();

        // 判断重置按钮
        checkRestBt();
    }

    /**
     * 设置页面参数
     *
     * @param info
     */
    public void setJoinApplyInfo(ApplyJoinLiveModel info) {
        if (info == null) {
            return;
        }

        // 初始化标签信息
        if (info.tagList != null && info.tagList.size() > 0) {
            mTagData.addAll(info.tagList);
        }
        if (info.selestTagList != null && info.selestTagList.size() > 0) {
            // 重新编辑，之前有选中的标签，将上次选中的标签显示出来
            for (int i = 0; i < mTagData.size(); i++) {
                for (int j = 0; j < info.selestTagList.size(); j++) {
                    if (mTagData.get(i).tagId.equals(info.selestTagList.get(j).tagId)) {
                        mTagData.get(i).isSelect = "1";
                    }
                }
            }
        }
        if (mTagData != null && mTagData.size() > 0) {
            mView.findViewById(R.id.llv_select_tags).setVisibility(View.VISIBLE);
            initTagAdatper();
        } else {
            mView.findViewById(R.id.llv_select_tags).setVisibility(View.GONE);
        }
        // 房间ID
        mRoomIdTv.setText(TextUtils.isEmpty(info.roomCode) ? " " : info.roomCode);
        mEditRoomName.setText(TextUtils.isEmpty(info.roomName) ? "" : info.roomName);
        mEditRoomPsw.setText(TextUtils.isEmpty(info.roomPassword) ? "" : info.roomPassword);
        if (BaseUserInfoCache.getUserIsHavePermiss(getContext(), UserPermissions.NO_APPROVE) || "1".equals(applyJoinLiveModel.leaderFlag)) {
            mLeaderRlv.setVisibility(View.GONE);
        } else {
            mLeaderRlv.setVisibility(View.VISIBLE);
            mSubmitLeaderTv.setText(TextUtils.isEmpty(info.superName) ? " " : info.superName);
        }
        if (applyJoinLiveModel.peopleNumList != null && applyJoinLiveModel.peopleNumList.size() > 0) {
            // 默认回显不显示上一次的，只显示本次的列表第一个，若只有一个选项，则默认展示该用户可使用的参会人数，不需要再操作选择
            setRoomLookCount(applyJoinLiveModel.peopleNumList.get(0).personNumber);
        } else {
            setRoomLookCount(info.viewNum);
        }
        if (TextUtils.isEmpty(BaseUserInfoCache.getCompanyCode(getActivity()))) {
            // 没有企业则不显示邀请人员信息
            mView.findViewById(R.id.v_line_invitation).setVisibility(View.GONE);
            mRoomInvitationCountRLv.setVisibility(View.GONE);
        } else {
            mView.findViewById(R.id.v_line_invitation).setVisibility(View.VISIBLE);
            mRoomInvitationCountRLv.setVisibility(View.VISIBLE);
        }
        mRoomDrainageTypeTv.setText(info.getDrainageType());
        mDrainageTypeKey = info.drainageType;
        mStartLiveReasonEdit.setText(TextUtils.isEmpty(info.playReason) ? "" : info.playReason);
        setLookPower(info.getAuthType(), info.authType);
        // 默认不生成回放（无论是否申请过，默认均为不生成回放）
        mRoomPlayBackPowerTv.setText("不生成");
        mPlaybackKey = "0";

        initCoverAdapter(info);
        initFileAdapter(info);

        // 判断重置按钮
        checkRestBt();

        initListPopParams();
    }

    public void setInvitationPersons(String ids, String phones) {
        if (TextUtils.isEmpty(ids)) {
            mRoomInvitationPeopleTv.setText("");
        } else {
            String[] persons = ids.split(",");
            mRoomInvitationPeopleTv.setText(persons.length + "人");
        }

    }


    public void setInvitationPersons(String ids, String phones, List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> mlists) {
        userIds = ids;
        mobiles = phones;
        //保存临时
        tmpuserIds = ids;
        tmpmobiles = phones;

        if (attendeePersonModel != null) {
            int personsLengh = 0;
            int ousidelengh = 0;
            if (!TextUtils.isEmpty(attendeePersonModel.getInuserIds())) {
                String[] persons = attendeePersonModel.getInuserIds().split(",");
                personsLengh = persons.length;
            }
            if (!TextUtils.isEmpty(attendeePersonModel.getOutsideMobils())) {
                String[] outsidePersons = attendeePersonModel.getOutsideMobils().split(",");
                ousidelengh = outsidePersons.length;
            }

            if (TextUtils.isEmpty(ids)) {
                mRoomInvitationPeopleTv.setText("");
            } else {
                mRoomInvitationPeopleTv.setText(personsLengh + ousidelengh + "人");
            }

        }
        //赋值已添加的外部联系人
        outSidePersonLists = mlists;
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (getActivity() == null || FastClickAvoidUtil.isFastDoubleClick(i)) {
            return;
        }

        if (i == R.id.rlv_room_look_count) {
            // 设置参会人数
            if (peopleParams == null || peopleParams.size() == 0) {
                // 没有可选人数不弹窗
                return;
            }
            //1.4.3版本修改需求，重置之后显示默认设置人数。如果是单个选项不能弹框选择
            //
            if (peopleParams.size() == 1 && !TextUtils.isEmpty(mLookCount)) {
                // 如果列表只有一个并且页面设置的值不为空，不弹窗（为空可以，重置的情况）
                return;
            }

            // 即使都符合了，判断一下是否是需要的数据（后台返回的乱七八糟的对象可能都对不上）
            CommonParam cPModel = peopleParams.get(0);
            if (cPModel == null || TextUtils.isEmpty(cPModel.name)) {
                // 抽查第一个数据，若对象里面基本的字段都没有就别往下走了
                return;
            }

            new CommonParamsPop(getActivity(), peopleParams, 3, "设置参会人数", "", "", (v2, position, data) -> {
                for (CommonParam commonParam : peopleParams) {
                    commonParam.isSelected = false;
                }
                peopleParams.get(position).isSelected = true;
                setRoomLookCount(peopleParams.get(position).name);
            }).show();
        } else if (i == R.id.rlv_room_look_power) {
            // 设置参会权限
            new CommonParamsPop(getActivity(), peoplePworParams, 2, "设置参会权限", "",
                    "选择任何一种参会权限，都可设置密码", "请前往管理后台导入参会人员", 2, (v2, position, data) -> {
                for (CommonParam commonParam : peoplePworParams) {
                    commonParam.isSelected = false;
                }
                peoplePworParams.get(position).isSelected = true;
                setLookPower(peoplePworParams.get(position).name, peoplePworParams.get(position).code);
            }).show();

        } else if (i == R.id.bt_submit_examine) {
            // 提交并审核按钮
            submitExamineMsg();

        } else if (i == R.id.rlv_room_drainage_type) {
            // 视频引流权限（0 不允许    1允许）
            new CommonParamsPop(getActivity(), drainageParams, 1, "可否被引用画面", "",
                    "如选择可被其他会议引用本会议画面则其他会议可随时引用本会议视频画面", (v2, position, data) -> {
                for (CommonParam commonParam : drainageParams) {
                    commonParam.isSelected = false;
                }
                drainageParams.get(position).isSelected = true;
                setDrainageType(drainageParams.get(position).name, drainageParams.get(position).code);
            }).show();

        } else if (i == R.id.rlv_room_invitation_count) {
            if (TextUtils.isEmpty(mLookCount)) {
                Toasty.normal(getContext(), "请先设置参会人数").show();
                return;
            }
            //页面临时的外部人员列表集合
            List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel> tmpOutSidePersonLists = null;
            if ("2".equals(mPowerKey)) {

                tmpOutSidePersonLists = MeetingPersonTon.getInstance().getOutSidePersonList();

            } else {
                tmpOutSidePersonLists = MeetingPersonTon.getInstance().getOutSidePersonList();
            }

            userIds = tmpuserIds;
            mobiles = tmpmobiles;

            // 邀请参会人员
            Postcard postcard = ARouter.getInstance()
                    .build(RouterPathProvider.MeetingPersonEnterprise);
            LogisticsCenter.completion(postcard);
            Intent intent = new Intent(getActivity(), postcard.getDestination());
            intent.putExtras(postcard.getExtras());
            intent.putExtra("userIds", userIds);
            intent.putExtra("mobiles", mobiles);
            intent.putExtra("max_meeting_person", mLookCount);
            //会议权限类型
            intent.putExtra("meeting_authtype", mPowerKey);
            //参会来源
            intent.putExtra("meeting_from", "ApplyEditFragment");
            //已添加的外部联系人集合
            intent.putExtra("outsideperson", (Serializable) tmpOutSidePersonLists);
            intent.putExtra("departPersonModel", getDepartPersonModel());

            HashMap<String, String> tmp = ((ApplyAct) getActivity()).tmpHamapA;

            intent.putExtra("checkhashmap", tmp);

            intent.putExtra("isBack", ((ApplyAct) getActivity()).isBackFromMeetingPerson);

            getActivity().startActivityForResult(intent, ApplyAct.REQUEST_ROOM_INVITATION_TYPE);//跳转，code要大于0

        } else if (i == R.id.rlv_room_playback) {
            //  是否生成回放
            new CommonParamsPop(getActivity(), playBackPowerParams, 2, "是否生成回放", "",
                    "", (v2, position, data) -> {
                for (CommonParam commonParam : playBackPowerParams) {
                    commonParam.isSelected = false;
                }
                playBackPowerParams.get(position).isSelected = true;
                setPlayBackPower(playBackPowerParams.get(position).name, playBackPowerParams.get(position).code);
            }).show();
        }
    }

    /**
     * 校验是否有未填写的信息
     */
    public void submitExamineMsg() {
        if (TextUtils.isEmpty(mRoomIdTv.getText().toString())) {
            // 判断房间号是否为空
            Toasty.normal(getContext(), "会议ID不能为空").show();
            return;
        }
        if (TextUtils.isEmpty(mEditRoomName.getText().toString().trim())) {
            // 判断房间主题名称是否为空
            Toasty.normal(getContext(), "会议主题不能为空").show();
            return;
        }
        if (!TextUtils.isEmpty(mEditRoomPsw.getText().toString().trim()) && mEditRoomPsw.getText().toString().trim().length() != 6) {
            // 如果设置密码了，判断密码是否为6位数
            Toasty.normal(getContext(), "请设置6位会议密码").show();
            return;
        }
        if (TextUtils.isEmpty(mLookCount)) {
            // 判断观看人数
            Toasty.normal(getContext(), "设置参会人数不能为空").show();
            return;
        }
        if (TextUtils.isEmpty(mRoomLookPowerTv.getText().toString())) {
            // 判断观看权限
            Toasty.normal(getContext(), "设置参会权限不能为空").show();
            return;
        }
        if (TextUtils.isEmpty(mRoomDrainageTypeTv.getText().toString())) {
            // 判断引流权限
            Toasty.normal(getContext(), "可否被引用画面不能为空").show();
            return;
        }
        if (TextUtils.isEmpty(mRoomPlayBackPowerTv.getText().toString())) {
            // 判断是否生成回放权限
            Toasty.normal(getContext(), "是否生成回放不能为空").show();
            return;
        }
        if (mCoverInfos == null || mCoverInfos.size() <= PICK_IMAGE_COUNT - 1) {
            // 判断封面
            Toasty.normal(getContext(), "请上传会议封面").show();
            return;
        } else {
            int coverSize = 0;
            for (int m = 0; m < mCoverInfos.size(); m++) {
                if (!TextUtils.isEmpty(mCoverInfos.get(m).getGileUrl())) {
                    coverSize++;
                }
            }
            if (coverSize < PICK_IMAGE_COUNT) {
                // 判断封面
                Toasty.normal(getContext(), "请上传会议封面").show();
                return;
            }
        }
        if (mFileInfos != null && mFileInfos.size() > 0) {
            // 如果有文档，判断文档的url和名称是否都存在，若缺名称不允许提交
            boolean isAllHaveName = true;
            for (int i = 0; i < mFileInfos.size(); i++) {
                if (!TextUtils.isEmpty(mFileInfos.get(i).getGileUrl())) {
                    // 有图片
                    if (TextUtils.isEmpty(mFileInfos.get(i).name)) {
                        // 名称为空
                        isAllHaveName = false;
                        break;
                    }
                }
            }
            if (!isAllHaveName) {
                Toasty.normal(getContext(), "请输入上传的会议文件名称").show();
                return;
            }
        }
        // 拼接选择标签的参数
        StringBuilder stringBuilder = new StringBuilder();
        for (ApplyPowerTagModel tagInfo : mTagData) {
            if (tagInfo.getIsSelect()) {
                stringBuilder.append(tagInfo.tagId);
                stringBuilder.append(",");
            }
        }
        if (stringBuilder.length() > 0) {
            mTagId = stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1);
        }

        cancleAllTask();
        submitCoverList.clear();
        sumitPicUrl = "";
        // 先过滤掉"+"号图片
        for (int i = 0; i < mCoverInfos.size(); i++) {
            if (!TextUtils.isEmpty(mCoverInfos.get(i).ossUrl)) {
                // 代表是上传过的，直接传给后台不需要再次上传此图片
                sumitPicUrl = sumitPicUrl + mCoverInfos.get(i).ossUrl + ",";
            } else if (!TextUtils.isEmpty(mCoverInfos.get(i).getFilePath()) && TextUtils.isEmpty(mCoverInfos.get(i).ossUrl)) {
                // 本次选中但未上传成功的
                ApplyPhotoInfo info = mCoverInfos.get(i);
                info.indexInfos = i;
                submitCoverList.add(info);
            }
        }
        submitDocName = "";
        // 先过滤掉"+"号图片
        for (int j = 0; j < mFileInfos.size(); j++) {
            if (!TextUtils.isEmpty(mFileInfos.get(j).name)) {
                submitDocName = submitDocName + mFileInfos.get(j).name + ",";
            }
        }

        // 必填信息填写完毕，先上传封面
        if (submitCoverList.size() > 0) {
            sumitPicUrl = "";
            uploadImgToOSS(true);
        } else {
            // 代表照片为上次上传过的，直接上传文件
            submitCoverOver("");
        }
    }

    /**
     * 上传封面成功，返回封面的参数
     */
    public void submitCoverOver(String picUrl) {
        sumitPicUrl = "";
        // 先过滤掉"+"号图片
        for (int i = 0; i < mCoverInfos.size(); i++) {
            if (!TextUtils.isEmpty(mCoverInfos.get(i).ossUrl)) {
                // 代表是上传过的，直接传给后台不需要再次上传此图片
                sumitPicUrl = sumitPicUrl + mCoverInfos.get(i).ossUrl + ",";
            }
        }

        submitDocUrl = "";
        submitFileList.clear();
        // 先过滤掉"+"号图片
        for (int j = 0; j < mFileInfos.size(); j++) {
            if (!TextUtils.isEmpty(mFileInfos.get(j).getFilePath()) && TextUtils.isEmpty(mFileInfos.get(j).ossUrl)) {
                ApplyPhotoInfo info = mFileInfos.get(j);
                info.indexInfos = j;
                submitFileList.add(info);
            }
            if (!TextUtils.isEmpty(mFileInfos.get(j).ossUrl)) {
                submitDocUrl = submitDocUrl + mFileInfos.get(j).ossUrl + ",";
            }
        }
        if (submitFileList.size() > 0) {
            // 有文件上传
            if (getActivity() != null) {
                submitDocUrl = "";
                uploadImgToOSS(false);
            }

        } else {
            // 没有文件上传、直接提交申请
            submitFileOver("");
        }
    }

    /**
     * 上传文档图片完毕
     */
    public void submitFileOver(String docUrl) {
        submitDocUrl = "";
        // 先过滤掉"+"号图片
        for (int j = 0; j < mFileInfos.size(); j++) {
            if (!TextUtils.isEmpty(mFileInfos.get(j).ossUrl)) {
                submitDocUrl = submitDocUrl + mFileInfos.get(j).ossUrl + ",";
            }
        }

        if (getActivity() != null) {
            //仅邀请内部人员
            if (mPowerKey != null && mPowerKey.equals("2")) {
                attendeePersonModel.setOutsideNames("");
                attendeePersonModel.setOutsideMobils("");
            }


            ((ApplyAct) getActivity()).submitLiveApply(mEditRoomName.getText().toString(),
                    mEditRoomPsw.getText().toString(),
                    mLookCount,
                    mStartLiveReasonEdit.getText().toString(),
                    mTagId,
                    submitDocUrl,
                    sumitPicUrl,
                    mPowerKey,
                    mDrainageTypeKey,
                    submitDocName,
                    attendeePersonModel.getInuserIds(),
                    attendeePersonModel.getInmobiles(),
                    mPlaybackKey, attendeePersonModel.getOutsideMobils(), attendeePersonModel.getOutsideNames(), attendeePersonModel.getInnameIds());
        }
    }

    /**
     * 重置按钮可点击颜色的判断
     * 房间ID不可重置不用检测
     */
    public void checkRestBt() {
        if ((mCoverInfos != null && mCoverInfos.size() > 0)
                || (mFileInfos != null && mFileInfos.size() > 0)) {
            checkCoverCount();
            checkFilesCount();
        }
        if (!TextUtils.isEmpty(mEditRoomName.getText().toString().trim())
                || !TextUtils.isEmpty(mLookCount)
                || !TextUtils.isEmpty(mRoomLookPowerTv.getText().toString())
                || !TextUtils.isEmpty(mRoomDrainageTypeTv.getText().toString())
                || !TextUtils.isEmpty(mRoomPlayBackPowerTv.getText().toString())
                || !TextUtils.isEmpty(mEditRoomPsw.getText().toString())
                || !TextUtils.isEmpty(mStartLiveReasonEdit.getText().toString())) {
            // 判断房间主题名称、密码、观看人数、观看权限、引流权限、开播理由、是否生成引流是否为空
            if (getActivity() != null) {
                ((ApplyAct) getActivity()).setRestBtClick(true);
            }

        } else if ((mFileInfos != null && mFileInfos.size() > 1)
                || mIdflowlayout.getSelectedList().size() > 0) {
            // 文字类的均没有值，判断封面、文档、标签的数据
            if (getActivity() != null) {
                ((ApplyAct) getActivity()).setRestBtClick(true);
            }
        } else if ((mCoverInfos != null && mCoverInfos.size() > 0) && !TextUtils.isEmpty(mCoverInfos.get(0).getGileUrl())) {
            // 文字类的均没有值，判断封面、文档、标签的数据
            if (getActivity() != null) {
                ((ApplyAct) getActivity()).setRestBtClick(true);
            }
        } else {
            // 页面无值，不可点击
            if (getActivity() != null) {
                ((ApplyAct) getActivity()).setRestBtClick(false);
            }
        }
    }

    /**
     * 点击重置，清空页面所填写、选中的数据
     */
    public void restEditMsg() {
        // 清空房间主题
        mEditRoomName.setText("");
        // 清空密码
        mEditRoomPsw.setText("");
        // 清空观看人数 1.4.3版本 根据产品需求注释清空观看人数
        //mRoomLookCountTv.setText("");
        //mLookCount = "";
        if (peopleParams != null) {
            for (CommonParam commonParam : peopleParams) {
                commonParam.isSelected = false;
            }
        }
        mRoomDrainageTypeTv.setText("");
        mDrainageTypeKey = "";
        if (drainageParams != null) {
            for (CommonParam commonParam : drainageParams) {
                commonParam.isSelected = false;
            }
        }
        // 邀请人为空
        userIds = "";
        mobiles = "";
        // 参会权限
        setLookPower("", "");
        // 回放是否生成
        setPlayBackPower("", "");
        mRoomInvitationPeopleTv.setText("");
        // 开播理由
        mStartLiveReasonEdit.setText("");
        // 审批人清空
        //mSubmitLeaderTv.setText("");
        // 清空封面
        if ((mCoverInfos != null && mCoverInfos.size() > 0)) {
            mCoverInfos.clear();
            sumitPicUrl = "";
            multiSelectCoverMaxCount = PICK_IMAGE_COUNT;
            // 设置默认封面数据加号
            mCoverInfos.add(new ApplyPhotoInfo());
            mSubmitCoverAdapter.notifyDataSetChanged();
        }

        // 清空文档
        if ((mFileInfos != null && mFileInfos.size() > 0)) {
            mFileInfos.clear();
            submitDocUrl = "";
            submitDocName = "";
            multiSelectFileMaxCount = PICK_FILE_COUNT;
            // 设置默认封面数据加号
            mFileInfos.add(new ApplyPhotoInfo());
            mSubmitFileAdapter.notifyDataSetChanged();
        }

        // 清空标签
        if (mIdflowlayout.getSelectedList().size() > 0) {
            mIdflowlayout.getSelectedList().clear();
            // 重新编辑，之前有选中的标签，将上次选中的标签显示出来
            for (int i = 0; i < mTagData.size(); i++) {
                mTagData.get(i).isSelect = "0";
            }
            Set<Integer> set = new HashSet<>();
            set.clear();
            mTagAdapter.setSelectedList(set);
            mTagAdapter.notifyDataChanged();
        }
        cancleAllTask();

        //清空之前联系人选过的状态
        ((ApplyAct) getActivity()).tmpHamapA = new HashMap<>();
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        }
    };

    Map<String, OSSAsyncTask> mTaskList = new HashMap<>();

    /**
     * 上传该图片
     *
     * @param position
     * @param isCover
     */
    private void uploadPic(int position, boolean isCover) {
        OSSService.getInterface().initOSSService(new OSSService.InitOSSCallBack() {
            @Override
            public void onInitComplete() {
                String filePath;
                if (isCover) {
                    filePath = mCoverInfos.get(position).getFilePath();
                } else {
                    filePath = mFileInfos.get(position).getFilePath();
                }

                String uploadFilePath = OSSFileNameProvider.getImgFileName(filePath, OSSFileNameProvider.MODULE_NetMeetiing, 1.0f);
                String finalFilePath = filePath;
                OSSAsyncTask task = OSSService.getInterface().uploadObject(uploadFilePath, filePath, new ProgressCallback<PutObjectRequest, PutObjectResult>() {
                    @Override
                    public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                        Log.d("OSS-Android-SDK-WRITE", "");
                        mHandler.postDelayed(() -> {
                            if (isCover) {
                                mCoverInfos.get(position).ossUrl = uploadFilePath;
                                mSubmitCoverAdapter.notifyDataSetChanged();
                            } else {
                                mFileInfos.get(position).ossUrl = uploadFilePath;
                                mSubmitFileAdapter.notifyDataSetChanged();
                            }
                        }, 1000);
                    }

                    @Override
                    public void onFailure(PutObjectRequest request, ClientException clientException, ServiceException serviceException) {
                        for (int i = 0; i < mTaskList.size(); i++) {
                            if (mTaskList.containsKey(finalFilePath)) {
                                mHandler.post(() -> Toasty.normal(getContext(), getString(R.string.oss_init_fail)).show());
                                break;
                            }
                        }
                        Log.d("OSS-Android-SDK-WRITE", "");
                    }

                    @Override
                    public void onProgress(PutObjectRequest request, long currentSize, long totalSize) {
                        Log.d("OSS-Android-SDK-WRITE", currentSize + "");
                    }
                }, () -> {
                    cancleOssTask(position, isCover);
                });
                mTaskList.put(filePath, task);
            }

            @Override
            public void onInitFail() {
                Toasty.normal(getContext()
                        , getContext().getString(R.string.oss_init_fail)).show();
            }
        });
    }

    /**
     * 取消该文件上传并删除该图片
     *
     * @param position
     * @param isCover
     */
    private void cancleOssTask(int position, boolean isCover) {
        String filePath;
        if (isCover) {
            filePath = mCoverInfos.get(position).getFilePath();
        } else {
            filePath = mFileInfos.get(position).getFilePath();
        }
        for (int i = 0; i < mTaskList.size(); i++) {
            if (mTaskList.containsKey(filePath)) {
                OSSAsyncTask task = mTaskList.get(filePath);
                task.cancel();
            }
        }
        if (isCover) {
            deletePhotoView(position, 1, filePath);
        } else {
            deletePhotoView(position, 2, filePath);
        }
    }

    /**
     * 取消全部上传任务
     */
    private void cancleAllTask() {
        if (mTaskList.isEmpty()) {
            return;
        }
        for (Map.Entry<String, OSSAsyncTask> entry : mTaskList.entrySet()) {
            entry.getValue().cancel();
        }
        mTaskList.clear();
    }

    List<ApplyPhotoInfo> submitCoverList = new ArrayList<>();
    List<ApplyPhotoInfo> submitFileList = new ArrayList<>();
    int uploadImgSize = 0;

    /**
     * 循环上传多张图片
     */
    private void uploadImgToOSS(boolean isUploadCover) {
        mTaskList.clear();
        cancleAllTask();

        if (isUploadCover) {
            uploadImgSize = submitCoverList.size();

            if (submitCoverList.size() > 0) {//有需要上传的图片
                showProgressHUD(getContext(), "正在上传封面，请稍后！");
                ArrayList uploadResult = new ArrayList<String>();
                for (ApplyPhotoInfo photoInfo : submitCoverList) {
                    String filePath = photoInfo.getFilePath();
                    for (int i = 0; i < mTaskList.size(); i++) {
                        if (mTaskList.containsKey(filePath)) {
                            OSSAsyncTask taskInfo = mTaskList.get(filePath);
                            if (taskInfo.isCompleted()) {
                                // 若正在上传则不重新上传（防止adapter刷新的时候调用）
                                return;
                            }
                        }
                    }
                    String uploadFilePath = OSSFileNameProvider.getImgFileName(filePath, OSSFileNameProvider.MODULE_NetMeetiing, 1.0f);
                    OSSAsyncTask task = OSSService.getInterface().uploadObject(uploadFilePath, filePath, new ProgressCallback<PutObjectRequest, PutObjectResult>() {
                        @Override
                        public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                            uploadResult.add(filePath);
                            mCoverInfos.get(photoInfo.indexInfos).ossUrl = uploadFilePath;
                            if (uploadImgSize == uploadResult.size()) {
                                // 封面已上传完成，可以提交申请了
                                showProgressHUD(getContext(), "封面上传完毕");
                                mHandler.postDelayed(() -> {
                                    submitCoverOver("");
                                    mSubmitCoverAdapter.notifyDataSetChanged();
                                }, 800);
                            }
                        }

                        @Override
                        public void onFailure(PutObjectRequest request, ClientException clientException, ServiceException serviceException) {
                            mHandler.post(() -> Toasty.normal(getContext(), getString(R.string.oss_init_fail)).show());
                            dismissProgressHUD();
                        }

                        @Override
                        public void onProgress(PutObjectRequest request, long currentSize, long totalSize) {
                            Log.d("OSS-Android-SDK-WRITE", currentSize + "");
                        }
                    }, () -> {
                        cancleOssTask(photoInfo.indexInfos, true);
                    });
                    mTaskList.put(filePath, task);
                }
            }
        } else {
            uploadImgSize = submitFileList.size();
            if (uploadImgSize == 0) {
                return;
            }
            showProgressHUD(getContext(), "正在上传文档，请稍后！");
            ArrayList uploadResult = new ArrayList<String>();
            for (ApplyPhotoInfo photoInfo : submitFileList) {
                String filePath = photoInfo.getFilePath();
                String ossPath = OSSFileNameProvider.getFileName(filePath, OSSFileNameProvider.MODULE_NetMeetiing);
                OSSAsyncTask task = OSSService.getInterface().uploadObject(ossPath, filePath, new ProgressCallback<PutObjectRequest, PutObjectResult>() {
                    @Override
                    public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                        uploadResult.add(ossPath);
                        mFileInfos.get(photoInfo.indexInfos).ossUrl = ossPath;
                        Log.d("uploadObject", "OSSAsyncTask-Success" + uploadResult.size());

                        if (uploadResult.size() == uploadImgSize) {
                            // 文件已上传完成，可以提交申请了
                            // 本次为上传文件
                            Log.d("uploadObject", "OSSAsyncTask-Success" + "Over");

                            showProgressHUD(getContext(), "文档上传完毕");
                            mHandler.postDelayed(() -> {
                                submitFileOver("");
                                mSubmitFileAdapter.notifyDataSetChanged();
                            }, 800);

                        }
                    }

                    @Override
                    public void onFailure(PutObjectRequest request, ClientException clientException, ServiceException serviceException) {
                        mHandler.post(() -> Toasty.normal(getContext(), getString(R.string.oss_init_fail)).show());
                        dismissProgressHUD();
                    }

                    @Override
                    public void onProgress(PutObjectRequest request, long currentSize, long totalSize) {
                        Log.d("OSS-Android-SDK-WRITE", currentSize + "");
                    }
                }, () -> {
                    cancleOssTask(photoInfo.indexInfos, false);
                });
                mTaskList.put(filePath, task);
            }
        }
    }

    /**
     * 检测封面已选择的数量
     */
    private void checkCoverCount() {
        // 双重检测封面和文档的数量是否有加号
        if (mCoverInfos != null) {
            if (mCoverInfos.size() > 0) {
                if (!TextUtils.isEmpty(mCoverInfos.get(mCoverInfos.size() - 1).getFilePath())
                        || !TextUtils.isEmpty(mCoverInfos.get(mCoverInfos.size() - 1).ossUrl)) {
                    // 最后一个有数据，判断数量是否为最大，否则添加加号
                    multiSelectCoverMaxCount = PICK_IMAGE_COUNT - mCoverInfos.size();
                    if (mCoverInfos.size() < PICK_IMAGE_COUNT) {
                        mCoverInfos.add(new ApplyPhotoInfo());
                    }
                } else {
                    multiSelectCoverMaxCount = PICK_IMAGE_COUNT - mCoverInfos.size() + 1;
                }
            } else {
                mCoverInfos.add(new ApplyPhotoInfo());
                multiSelectCoverMaxCount = PICK_IMAGE_COUNT;
            }
        }

    }

    /**
     * 检查当前文件已选择的数量
     */
    private void checkFilesCount() {
        if (mFileInfos != null) {
            if (mFileInfos.size() > 0) {
                if (!TextUtils.isEmpty(mFileInfos.get(mFileInfos.size() - 1).getFilePath())
                        || !TextUtils.isEmpty(mFileInfos.get(mFileInfos.size() - 1).ossUrl)) {
                    // 最后一个有数据，判断数量是否为最大，否则添加加号
                    multiSelectFileMaxCount = PICK_FILE_COUNT - mFileInfos.size();
                    if (mFileInfos.size() < PICK_FILE_COUNT) {
                        mFileInfos.add(new ApplyPhotoInfo());
                    }
                } else {
                    multiSelectFileMaxCount = PICK_FILE_COUNT - mFileInfos.size() + 1;
                }
            } else {
                mFileInfos.add(new ApplyPhotoInfo());
                multiSelectFileMaxCount = PICK_FILE_COUNT;
            }
        }
    }

}
