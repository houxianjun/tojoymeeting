package com.tojoy.cloud.online_meeting.App.Home.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.tojoy.tjoybaselib.model.live.GoodsResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.cloud.online_meeting.App.EnterpriseEditionHome.adapter.ProductRecommendListAdapter;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.App.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;


@SuppressLint("ValidFragment")
public class ProductSearchFragment extends BaseFragment {
    private TJRecyclerView mRecyclerView;

    private ProductRecommendListAdapter mAdapter;
    private List<GoodsResponse.DataBean.RecordsBean> mDatas = new ArrayList<>();

    private String mCompanyCode;
    private int mPage;
    private String mSearchKey;

    @SuppressLint("ValidFragment")
    public ProductSearchFragment(String companyCode) {
        this.mCompanyCode = companyCode;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.activity_products_recommend_list, container, false);
        }
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        isCreateView = true;
        return mView;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initUI() {

        mRecyclerView = mView.findViewById(R.id.recyclerView);

        mAdapter = new ProductRecommendListAdapter(getContext(), mDatas);
        mView.findViewById(R.id.rlv_title_layout).setVisibility(View.GONE);
        mView.findViewById(R.id.content_seach).setVisibility(View.GONE);
        mRecyclerView.setLoadMoreViewWithHide();
        mRecyclerView.setEmptyImgAndTip(R.drawable.icon_empty_online_logo, "暂无搜索内容");
        setRightManagerImg(R.drawable.icon_search_company);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setRefreshable(false);
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {

            }

            @Override
            public void onLoadMore() {
            }
        });
    }

    /**
     * 进行搜索
     */
    public void doSearch(String dynamicKey) {
        mPage = 1;
        mSearchKey = dynamicKey;
        mDatas.clear();
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
        loadData();
    }

    /**
     * 搜索产品数据
     */
    private void loadData() {
        mCompanyCode = !TextUtils.isEmpty(mCompanyCode) ? mCompanyCode : BaseUserInfoCache.getCompanyCode(getContext());
        OMAppApiProvider.getInstance().getLiveRoomProductList(mCompanyCode, mSearchKey, mPage
                , new Observer<GoodsResponse>() {
                    @Override
                    public void onCompleted() {
                        mRecyclerView.setRefreshing(false);
                        if (mDatas.size() == 0) {
                            mView.findViewById(R.id.emptyLayout_order).setVisibility(View.VISIBLE);
                        } else {
                            mView.findViewById(R.id.emptyLayout_order).setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mRecyclerView.setRefreshing(false);
                        Toasty.normal(getContext(), "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(GoodsResponse response) {
                        if (response.isSuccess()) {
                            if (mPage == 1) {
                                mDatas.clear();
                            }
                            mDatas.addAll(response.data.records);
                            mRecyclerView.showFooterView();
                            mAdapter.updateData(mDatas);
                            mRecyclerView.setLoadMoreView();
                            mRecyclerView.refreshLoadMoreView(mPage, response.data.records.size());
                        } else {
                            Toasty.normal(getContext(), response.msg).show();
                        }
                    }
                });
    }
}
