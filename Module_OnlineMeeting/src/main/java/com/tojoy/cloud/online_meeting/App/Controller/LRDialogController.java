package com.tojoy.cloud.online_meeting.App.Controller;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishProjectAttachment;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishQRAttachment;
import com.netease.nim.uikit.business.chatroom.viewholder.ChatRoomMsgViewHolderText;
import com.netease.nim.uikit.business.liveroom.LiveRoomHelper;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tojoy.cloud.online_meeting.App.Live.Interaction.LaunchProductTJDialog;
import com.tojoy.common.LiveRoom.LiveSharePopview;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.model.live.LiveShareInfoModel;
import com.tojoy.tjoybaselib.model.shopmall.ShopMallListResponse;
import com.tojoy.tjoybaselib.model.shopmall.ShopMallModel;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigCacheManager;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigConstantKey;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.util.sys.KeyboardControlUtil;
import com.tojoy.cloud.online_meeting.App.Live.ChatRoom.LiveCRMessageFragment;
import com.tojoy.cloud.online_meeting.App.Live.ChatRoom.LiveChatMsgDialog;
import com.tojoy.cloud.online_meeting.App.Live.ExporeOnline.ExporeApiManager;
import com.tojoy.cloud.online_meeting.App.Live.Interaction.CenterQRDialog;
import com.tojoy.cloud.online_meeting.App.Live.Interaction.NoticeDialog;
import com.tojoy.cloud.online_meeting.App.Live.Interaction.StartDealDialog;
import com.tojoy.cloud.online_meeting.App.Live.Interaction.StopLiveDialog;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.SeeLiveAct;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.LiveCustomMessageProvider;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.OnlineMembersHelper;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.netease.nim.uikit.business.liveroom.LiveCustomMessageCode;
import com.tojoy.cloud.online_meeting.App.Live.Live.View.BottomOperationView;
import com.tojoy.cloud.online_meeting.App.Live.Live.View.ShortLivePop;
import com.tojoy.cloud.online_meeting.App.Live.MemberList.LiveRoomMemverListDialog;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;
import com.tojoy.common.LiveRoom.ProjectTJDialog;
import com.tojoy.cloud.online_meeting.App.Live.Interaction.PublistQRListTJDialog;

import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

import static android.view.View.GONE;

/**
 * Created by luuzhu on 2019/7/23.
 * 直播间的dialog控制类
 */

public class LRDialogController {

    private static LRDialogController instance;

    private LRDialogController() {
    }

    public synchronized static LRDialogController getInstance() {
        if (instance == null) {
            instance = new LRDialogController();
        }
        return instance;
    }

//——————————————————————————————————————————————————————————————————————————————————————————————————


    public LiveChatMsgDialog mLiveChatMsgDialog;            //聊天信息列表
    public LiveRoomMemverListDialog mMemverListDialog;      //成员列表
    public StartDealDialog mStartDealDialog;                //发起成交
    public ShortLivePop mShortLivePop;                      //暂离原因list
    public NoticeDialog mNoticeDialog;                      //公告、举报
    public ProjectTJDialog mProjectTJDialog;               //项目推荐
    public PublistQRListTJDialog mPublishQRListTJDialog;    //发布活动（二维码）
    public CenterQRDialog mCenterQRDialog;                  // 接收活动（二维码）中间弹窗

    public LaunchProductTJDialog launchProductTJDialog;

    private TJMakeSureDialog mInviteExploreDialog; //观众 & 助手 收到上麦邀请
    private TJMakeSureDialog mStartExploreDialog;  //观众 & 助手 申请上麦被同意后的弹窗


    public void cleanDialogs() {
        mLiveChatMsgDialog = null;
        mMemverListDialog = null;
        mStartDealDialog = null;
        mShortLivePop = null;
        mNoticeDialog = null;
        mProjectTJDialog = null;
        mInviteExploreDialog = null;
        mStartExploreDialog = null;
        mPublishQRListTJDialog = null;
        mCenterQRDialog = null;
        launchProductTJDialog = null;
    }


    /**
     * 聊天信息列表
     */
    public void showChatRoomMessageDialog(BaseLiveAct act, LiveCRMessageFragment mLiveCRMessageFragment, FrameLayout mChatMsgContainer, BottomOperationView mBottomOperationView) {

        if (mLiveChatMsgDialog != null && mLiveChatMsgDialog.isShowing()) {
            return;
        }

        if (mLiveCRMessageFragment.getMessageRecyclerView() == null) {
            Toasty.normal(act, "聊天室初始化失败，请稍后再试").show();
            return;
        }

        mLiveCRMessageFragment.setChatListType("2");

        act.showBottomView(false);
        mLiveChatMsgDialog = new LiveChatMsgDialog(act);
        mLiveChatMsgDialog.show();

        mChatMsgContainer.removeAllViews();
        mLiveChatMsgDialog.addChatMsgLayout(mLiveCRMessageFragment.getMessageRecyclerView());
        mLiveCRMessageFragment.messageListPanel.refreshMsgBg(ChatRoomMsgViewHolderText.SHOW_BG_STATE_GONE);
        LinearLayoutManager layoutManager = (LinearLayoutManager) mLiveCRMessageFragment.getMessageRecyclerView().getLayoutManager();
        layoutManager.setStackFromEnd(false);
        mLiveCRMessageFragment.messageListPanel.scrollToBottom();

        mLiveChatMsgDialog.setOnDismissListener(dialogInterface -> {

            mLiveCRMessageFragment.setChatListType("1");
            mLiveChatMsgDialog.removeMsgRecycler();
            mLiveCRMessageFragment.inputPanel.hideEmojiLayout();
            new Handler().postDelayed(() -> {
                //先隐藏未读数（等同于清0），再显示底部按钮
                if (mBottomOperationView != null) {
                    act.showBottomView(true);
                    act.addMsgListView();
                    mLiveCRMessageFragment.messageListPanel.scrollToBottom();
                    mLiveCRMessageFragment.inputPanel.messageEditText.setText("");
                }
            }, 400);
        });

        mLiveChatMsgDialog.mTvInput.setOnClickListener(view -> {
            mLiveChatMsgDialog.mTvInput.setVisibility(GONE);
            mLiveChatMsgDialog.mInputContainer.removeAllViews();
            ViewGroup parent = (ViewGroup) mLiveCRMessageFragment.inputPanel.messageActivityBottomLayout.getParent();
            if (parent != null) {
                parent.removeAllViews();
            }
            mLiveChatMsgDialog.mInputContainer.addView(mLiveCRMessageFragment.inputPanel.messageActivityBottomLayout);
            mLiveCRMessageFragment.inputPanel.messageEditText.requestFocus();
            mLiveCRMessageFragment.inputPanel.showInputMethod(mLiveCRMessageFragment.inputPanel.messageEditText);
        });
    }

    /**
     * 成员列表
     */
    public void showMemberList(BaseLiveAct act, OperationCallback operationCallback) {
        if (mMemverListDialog != null && mMemverListDialog.isShowing()) {
            return;
        }
        act.showBottomView(false);
        mMemverListDialog = new LiveRoomMemverListDialog(act, operationCallback);
        mMemverListDialog.show(act.getSupportFragmentManager(), "memberList");
        mMemverListDialog.setOnDismissListener(dialogInterface -> {
            if (mMemverListDialog != null) {
                KeyboardControlUtil.hideKeyboard(mMemverListDialog.etSearch);
            }
            new Handler().postDelayed(() -> act.showBottomView(true), 400);
        });
    }

    public void refreshMemberListDialog() {
        if (mMemverListDialog != null) {
            mMemverListDialog.refreshMemberList();
        }
    }

    /**
     * 发起成交
     */
    public void startDeal(BaseLiveAct act) {
        if (mStartDealDialog != null && mStartDealDialog.isShowing()) {
            return;
        }
        act.showBottomView(false);
        mStartDealDialog = new StartDealDialog(act);
        mStartDealDialog.show();
        mStartDealDialog.setOnDismissListener(dialogInterface -> new Handler().postDelayed(() -> act.showBottomView(true), 400));
    }

    /**
     * 发起代播商品成交
     */
    public void startAgentDeal(BaseLiveAct act) {

        if (launchProductTJDialog != null && launchProductTJDialog.isShowing()) {
            return;
        }
        act.showBottomView(false);
        launchProductTJDialog = new LaunchProductTJDialog(act);
        launchProductTJDialog.show(act.getSupportFragmentManager(), "lauchproductList");
        launchProductTJDialog.setOnDismissListener(dialogInterface -> {

            new Handler().postDelayed(() -> act.showBottomView(true), 400);
        });
    }


    /**
     * 发起活动（二维码）:主播/助手端
     */
    public void startPublishQrList(BaseLiveAct act) {
        if (mPublishQRListTJDialog != null && mPublishQRListTJDialog.isShowing()) {
            return;
        }
        act.showBottomView(false);
        mPublishQRListTJDialog = new PublistQRListTJDialog(act);
        mPublishQRListTJDialog.show();
        mPublishQRListTJDialog.setOnDismissListener(dialogInterface -> new Handler().postDelayed(() -> {
            act.showBottomView(true);
            // 判断有无要展示的活动弹窗
            if (LiveRoomHelper.getInstance().receivedQrBigList != null && LiveRoomHelper.getInstance().receivedQrBigList.size() > 0) {
                showQRCenterList(act, LiveRoomHelper.getInstance().receivedQrBigList.get(0));
            }
        }, 100));

    }

    /**
     * 观众端展示接收到二维码的弹窗
     */
    public void showQRCenterList(BaseLiveAct act, CRMsgLivePublishQRAttachment attachment) {
        if (mCenterQRDialog != null && mCenterQRDialog.isShowing()) {
            return;
        }
        if (attachment == null) {
            return;
        }
        if (LiveRoomInfoProvider.getInstance().isHost() || LiveRoomInfoProvider.getInstance().isHostHelper()) {
            // 如果是主播/助手。判断当前发布列表是否正在展示，如果正在展示则暂不显示中间弹窗
            if (mPublishQRListTJDialog != null && mPublishQRListTJDialog.isShowing()) {
                return;
            }
        }
        mCenterQRDialog = new CenterQRDialog(act, attachment);
        mCenterQRDialog.show();
        mCenterQRDialog.setOnDismissListener(dialogInterface -> new Handler().postDelayed(() -> {
            // 监听弹窗结束关闭的动作（展示下一个弹窗）,递归调用，没有要展示的活动为止
            LiveRoomHelper.getInstance().deleteQRDataBig(attachment);
            // 删除后再判断列表是否有需要展示的数据，有的话继续展示
            if (LiveRoomHelper.getInstance().receivedQrBigList != null && LiveRoomHelper.getInstance().receivedQrBigList.size() > 0) {
                showQRCenterList(act, LiveRoomHelper.getInstance().receivedQrBigList.get(0));
            }
        }, 100));
    }

    /**
     * 暂离list
     */
    public void templeteLeave(BaseLiveAct act) {
        if (mShortLivePop == null) {
            mShortLivePop = new ShortLivePop(act, act);
        }
        if (mShortLivePop.isShowing()) {
            return;
        }
        mShortLivePop.setmShortLeaveCallback(new ShortLivePop.ShortLeaveCallback() {
            @Override
            public void onShortLeave(String showText, String time) {
                //助手 & 主播 发送暂离消息
                LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                        LiveCustomMessageProvider.getTemplateLeaveData(act, showText, time), false);
                //主播 开始暂离
                if (LiveRoomInfoProvider.getInstance().isHost()) {
                    act.templateLive(showText, time);
                } else {
                    //助手显示暂离页面
                    LiveRoomInfoProvider.getInstance().hasTemplateLeave = true;
                    act.refreshTempLiveTip(showText, time, false);
                }
            }

            @Override
            public void onShortLevaeFail() {
                act.showBottomView(true);
            }
        });
        mShortLivePop.refreshLeaveReason();
    }

    /**
     * 举报、公告
     */
    public void showNotice(BaseLiveAct act, String type) {
        if (mNoticeDialog != null && mNoticeDialog.isShowing()) {
            return;
        }
        mNoticeDialog = new NoticeDialog(act, type);
        mNoticeDialog.show();
        mNoticeDialog.setOnDismissListener(dialogInterface -> new Handler().postDelayed(() -> act.showBottomView(true), 400));
    }

    /**
     * 项目推荐
     */
    private boolean isRequesting = false;

    public void showProjectList(BaseLiveAct act, View projectListBtn) {
        if (mProjectTJDialog != null && mProjectTJDialog.isShowing() && isRequesting) {
            return;
        }
        act.showProgressHUD(act, "");
        isRequesting = true;
        getProjectTJData(false, new SeeLiveAct.GotProjectListCallback() {
            @Override
            public void onGot() {
                act.dismissProgressHUD();
                mProjectTJDialog = new ProjectTJDialog(act);
                mProjectTJDialog.show();
                act.showBottomView(false);
                isRequesting = false;
                mProjectTJDialog.setOnDismissListener(dialogInterface -> new Handler().postDelayed(() -> act.showBottomView(true), 400));
            }

            @Override
            public void onError() {
                act.dismissProgressHUD();
            }
        }, projectListBtn);
    }

    /**
     * 获取项目推荐数据,如果有数据,用户端显示项目推荐按钮
     */
    public void getProjectTJData(boolean needHide, SeeLiveAct.GotProjectListCallback callback, View projectListBtn) {
        LiveRoomHelper.getInstance().receivedProjects.clear();

        OMAppApiProvider.getInstance().getGoodsTJ(BaseUserInfoCache.getUserId(projectListBtn.getContext()), LiveRoomInfoProvider.getInstance().liveId, "", "1",
                new Observer<ShopMallListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(ShopMallListResponse response) {
                        if (response.isSuccess()) {
                            List<ShopMallModel> list = response.data.list;
                            int size = list.size();

                            if (needHide) {
                                if (size > 0) {
                                    projectListBtn.setVisibility(View.VISIBLE);
                                }
                            }

                            for (int i = 0; i < size; i++) {
                                ShopMallModel bean = list.get(i);
                                CRMsgLivePublishProjectAttachment attachment = new CRMsgLivePublishProjectAttachment();
                                attachment.setTitle(bean.goodsName);
                                attachment.setProjectId(bean.goodsId);
                                attachment.setCoverUrl(bean.goodsImg1);
                                attachment.setProductType(bean.goodsType);
                                attachment.setIsInterest(bean.isInterested);
                                attachment.setStoreId(bean.storeId);
                                attachment.setIsDeposit(bean.isDeposit);
                                attachment.setAgentCompanyCode(bean.agentCompanyCode);
                                attachment.setAgentId(bean.agentId);
                                attachment.setAgentLive(bean.agentLive);
                                LiveRoomHelper.getInstance().receivedProjects.add(attachment);
                            }

                            if (callback != null) {
                                callback.onGot();
                            }
                        } else {
                            if (callback != null) {
                                callback.onError();
                            }
                            if (!needHide) {
                                Toasty.normal(projectListBtn.getContext(), response.msg).show();
                            }
                        }
                    }
                });
    }


    /**
     * 分享
     */
    public void showShare(BaseLiveAct act) {
        OMAppApiProvider.getInstance().getShareInfo(LiveRoomInfoProvider.getInstance().liveId, new Observer<LiveShareInfoModel>() {
            @Override
            public void onCompleted() {
                act.dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(act, "网络错误，请检查网络").show();
            }

            @Override
            public void onNext(LiveShareInfoModel model) {
                if (model.isSuccess()) {
                    // 允许分享
                    LiveShareInfoModel.DataObjBean.ShareInfoBean shareInfo = model.data.shareInfo;
                    LiveSharePopview wxPopView = new LiveSharePopview(act, act, LiveSharePopview.STYLE_BLACK);
                    wxPopView.setLiveShareData(OSSConfig.getOSSURLedStr(shareInfo.shareImg),
                            shareInfo.shareUrl + ((!TextUtils.isEmpty(shareInfo.shareUrl) && shareInfo.shareUrl.contains("roomLiveId")) ? "" : "?roomLiveId=" + LiveRoomInfoProvider.getInstance().liveId),
                            model.data.liveInfo.title, shareInfo.shareDesc, model.data);
                    wxPopView.show();
                    act.showBottomView(false);
                } else {
                    Toasty.normal(act, model.msg).show();
                }
            }
        });
    }

    /**
     * 退出直播间
     */
    public void quitLive(BaseLiveAct act, String type) {
        StopLiveDialog stopLiveDialog = new StopLiveDialog(act, type);
        stopLiveDialog.show();
    }

    public void dimissAllDialogs() {

        if (mMemverListDialog != null) {
            mMemverListDialog.dismiss();
        }

        if (mStartDealDialog != null) {
            mStartDealDialog.dismiss();
        }

        if (mShortLivePop != null) {
            mShortLivePop.dismiss();
        }

        if (mNoticeDialog != null) {
            mNoticeDialog.dismiss();
        }

        if (mProjectTJDialog != null) {
            mProjectTJDialog.dismiss();
        }

        if (mPublishQRListTJDialog != null) {
            mPublishQRListTJDialog.dismiss();
        }

        if (launchProductTJDialog != null) {
            launchProductTJDialog.dismiss();
        }

    }


    public void showInviteExploreDialog(BaseLiveAct act, String fromUserId, String extroId) {
        if (mInviteExploreDialog != null && mInviteExploreDialog.isShowing()) {
            mInviteExploreDialog.dismissStrongly();
        }
        mInviteExploreDialog = new TJMakeSureDialog(act);
        String title = TextConfigCacheManager.getInstance(act).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1);
        mInviteExploreDialog.setCancelListener(() -> {
                    if (mStartExploreDialog != null) {
                        if (mStartExploreDialog.isShowing()) {
                            mStartExploreDialog.dismiss();
                        }
                    }
                    aggreeOrRefuseInvite(act, LiveCustomMessageCode.REFUSE_INVITE_EXPORE, fromUserId, extroId);
                }
        )
                .setTitleAndCotent("", TextUtils.isEmpty(title) ? "是否现在同意同台互动？" : "是否现在同意" + title + "?")
                .setBtnText("同意", "拒绝")
                .hideTitle();

        mInviteExploreDialog.setEvent(view -> LiveRoomIOHelper.checkMediaPermission(act, new LiveRoomIOHelper.PermissionCallback() {
            @Override
            public void onGranted() {
                mInviteExploreDialog.dismissStrongly();
                doExploreMeNow(act, extroId, true);
                if (mStartExploreDialog != null) {
                    if (mStartExploreDialog.isShowing()) {
                        mStartExploreDialog.dismiss();
                    }
                }

            }

            @Override
            public void onDenied() {
                Toasty.normal(act, "请开启相关权限后重试").show();
            }
        }));

        if (OnlineMembersHelper.getInstance().isMyselfExploring(act)) {
            mInviteExploreDialog.dismiss();
        } else {
            mInviteExploreDialog.showSameTableDialog2(act);
        }
    }


    /**
     * 同意或拒绝邀请
     *
     * @param code
     * @param applyUserId
     * @param applyForId
     */
    private void aggreeOrRefuseInvite(Context context, LiveCustomMessageCode code, String applyUserId, String applyForId) {
        ExporeApiManager.mResponseForApplyOrInviteExplore(code, applyForId, applyUserId, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(OMBaseResponse baseResponse) {
                if (baseResponse.isSuccess()) {

                    //5.19只有拒绝的时候才发消息给主播
                    if (code.getCode() == LiveCustomMessageCode.REFUSE_INVITE_EXPORE.getCode()) {
                        LiveCustomMessageProvider.sendLiveRoomCustomMessage(
                                "",
                                LiveCustomMessageProvider.getRefuseInviteApplyExporeData(context, LiveRoomInfoProvider.getInstance().hostUserId), false);
                    }
                } else {
                    Toasty.normal(context, baseResponse.msg).show();
                }
            }
        });
    }


    /**
     * 同台申请被同意 & 同台邀请同意 并且权限通过后开始直播
     */
    private void doExploreMeNow(BaseLiveAct activity, String extroId, boolean isFromInvite) {
        LogUtil.i(LiveLogTag.BaseLiveTag, "doExploreMeNow" + isFromInvite);
        if (OnlineMembersHelper.getInstance().canOnlineNow()) {
            if (isFromInvite) {

                activity.showProgressHUD(activity, "处理中");

                ExporeApiManager.mResponseForApplyOrInviteExplore(LiveCustomMessageCode.AGREE_INVITE_EXPORE, extroId, "", new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        activity.dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        if (response.isSuccess()) {
                            activity.exploreMe();
                        } else {
                            Toasty.normal(activity, response.msg).show();
                        }
                    }
                });
            } else {
                activity.showProgressHUD(activity, "处理中");
                ExporeApiManager.mStartExploreMe(extroId, new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        activity.dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {
                        activity.dismissProgressHUD();
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        activity.dismissProgressHUD();
                        if (response.isSuccess()) {
                            activity.exploreMe();
                        } else {
                            Toasty.normal(activity, response.msg).show();
                        }
                    }
                });
            }
        } else {
            Toasty.normal(activity, "数量已达上限，无法同台").show();
        }
    }


    public void showStartExploreDialog(BaseLiveAct act, String toUserId, String extroId) {

        if (BaseUserInfoCache.isMySelf(act, toUserId)) {

            String live = "开始互动";

            mStartExploreDialog = new TJMakeSureDialog(act);
            String title = TextConfigCacheManager.getInstance(act).getWidgetTextByKey(TextConfigConstantKey.INTER_ACTION1);
            mStartExploreDialog.setTitleAndCotent("", TextUtils.isEmpty(title) ? "    您的同台互动申请已通过，\n" + "请准备好后互动" : "    您的" + title + "申请已通过，\n" + "请准备好后互动")
                    .setOkText(live).hideTitle().showSameTableDialog(act);
            mStartExploreDialog.setEvent(view -> LiveRoomIOHelper.checkMediaPermission(act, new LiveRoomIOHelper.PermissionCallback() {
                @Override
                public void onGranted() {
                    doExploreMeNow(act, extroId, false);
                    mStartExploreDialog.dismissStrongly();

                    if (mInviteExploreDialog != null) {
                        if (mInviteExploreDialog.isShowing()) {
                            mInviteExploreDialog.dismissStrongly();
                        }
                    }

                }

                @Override
                public void onDenied() {
                    Toasty.normal(act, "请开启相关权限后重试").show();
                }
            }));
        }

        //如果当前主播 & 助手的申请列表在显示 需要刷新状态
        if (!LiveRoomInfoProvider.getInstance().isGuest()) {
            act.refreshOnlineExplorePop(toUserId, true);
        }
    }

    public void refreshHelperMsg() {
        //6.当被取消助手身份的时候 如果当前在发布活动弹窗则需要关闭当前弹窗
        if (mPublishQRListTJDialog != null && mPublishQRListTJDialog.isShowing()) {
            if (LiveRoomInfoProvider.getInstance().isGuest()) {
                mPublishQRListTJDialog.dismiss();
            }
        }
    }

}