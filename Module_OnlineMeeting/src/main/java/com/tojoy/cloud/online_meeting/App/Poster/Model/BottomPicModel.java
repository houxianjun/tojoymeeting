package com.tojoy.cloud.online_meeting.App.Poster.Model;

public class BottomPicModel {
    /**
     * 本地模版背景图片
     */
    public int picSource;
    /**
     * 本地模版配件图片
     */
    public int picParts;
    /**
     * 本地模版配件位置（左、中、右）
     */
    public String partsLocation;

    public boolean isSelected;

}
