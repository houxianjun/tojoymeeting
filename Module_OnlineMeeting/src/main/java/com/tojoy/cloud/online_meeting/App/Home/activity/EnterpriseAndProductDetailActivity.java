package com.tojoy.cloud.online_meeting.App.Home.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.enterprise.ProductDetailResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.util.net.NetWorkUtils;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.Services.PayAndShare.ShareWxPopView;
import com.tojoy.common.Services.Web.BaseOutlinkAct;

import es.dmoral.toasty.Toasty;
import rx.Observer;


/**
 * 商品广告业详情、企业详情页
 */
@Route(path = RouterPathProvider.ONLINE_ENTERPRISE_AND_PRODUCT_DETAIL)
public class EnterpriseAndProductDetailActivity extends BaseOutlinkAct {
    private String mId;
    private String mCoverUrl;
    private String mShareUrl;
    private String mSubTitle;
    private String mTitle;
    //1：产品详情
    private int mEnterType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void loadData() {
        super.loadData();
        mWebView.setWebChromeClient(new CustomWebViewChromeClient(this, mLoaderBar,null));
        if (getIntent().hasExtra("productId")) {
            mId = getIntent().getStringExtra("productId");
            mEnterType = 1;
            requestProductData();
        } else if (getIntent().hasExtra("enterpriseId")) {
            mId = getIntent().getStringExtra("enterpriseId");
            requestEnterpriseData();
            mEnterType = 2;
        }
    }


    /**
     * 产品推荐详情
     */
    private void requestProductData() {
        showProgressHUD(this, "加载中");
        OMAppApiProvider.getInstance().getProjectProductDetail(mId, new Observer<ProductDetailResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(EnterpriseAndProductDetailActivity.this, "网络错误，请检查网络").show();
                dismissProgressHUD();
            }

            @Override
            public void onNext(ProductDetailResponse response) {
                if (response.isSuccess()) {
                    if (response.data != null) {
                        mCoverUrl = response.data.productImg1;
                        mShareUrl = response.data.shareUrl;
                        mSubTitle = response.data.productSubtitle;
                        mTitle = response.data.productName;
                        if ("1".equals(String.valueOf(response.data.isShow))) {
                            if ("1".equals(response.data.isShare)) {
                                setRightManagerImg(R.drawable.icon_share_product);
                            } else {
                                mRightManagerIv.setVisibility(View.GONE);
                            }
                            setTitle(mTitle);
                            mShareUrl = mShareUrl + "?shareType=" + mEnterType + "&title=" + mTitle + "&productId=" + mId + "&desc=" + mSubTitle + "&imgUrl=" + OSSConfig.getOSSURLedStr(mCoverUrl);
                            mWebUrl = mShareUrl + "&clearH5HeaderFooter=1";
                            mWebView.loadUrl(mWebUrl);
                        } else {
                            Toasty.normal(EnterpriseAndProductDetailActivity.this, "产品已下架").show();
                            finish();
                        }
                    }
                } else {
                    Toasty.normal(EnterpriseAndProductDetailActivity.this, response.msg).show();
                }
            }
        });
    }

    /**
     * 企业文化详情
     */
    private void requestEnterpriseData() {
        showProgressHUD(this, "加载中");
        OMAppApiProvider.getInstance().mCorporationDetail(mId, new Observer<ProductDetailResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();

            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(EnterpriseAndProductDetailActivity.this, "网络错误，请检查网络").show();
                dismissProgressHUD();
            }

            @Override
            public void onNext(ProductDetailResponse response) {
                if (response.isSuccess()) {
                    if (response.data != null) {
                        mCoverUrl = response.data.img;
                        mShareUrl = response.data.shareLink;
                        mSubTitle = response.data.subtitle;
                        mTitle = response.data.title;
                        if ("1".equals(String.valueOf(response.data.state + ""))) {
                            if ("1".equals(response.data.share + "")) {
                                setRightManagerImg(R.drawable.icon_share_product);
                            } else {
                                mRightManagerIv.setVisibility(View.GONE);
                            }
                            setTitle(mTitle);
                            mShareUrl = mShareUrl + "?shareType=" + mEnterType + "&title=" + mTitle + "&id=" + mId + "&desc=" + mSubTitle + "&imgUrl=" + OSSConfig.getOSSURLedStr(mCoverUrl);
                            mWebUrl = mShareUrl + "&clearH5HeaderFooter=1";
                            mWebView.loadUrl(mWebUrl);
                        } else {
                            Toasty.normal(EnterpriseAndProductDetailActivity.this, "该内容已下架，请刷新重试").show();
                            finish();
                        }
                    } else {
                        Toasty.normal(EnterpriseAndProductDetailActivity.this, "该内容已下架，请刷新重试").show();
                        finish();
                    }
                } else {
                    Toasty.normal(EnterpriseAndProductDetailActivity.this, response.msg).show();
                }
            }
        });
    }


    /**
     * 点击分享
     */
    @Override
    protected void onRightManagerTitleClick() {
        super.onRightManagerTitleClick();
        if (!NetWorkUtils.isNetworkConnected(this)) {
            Toasty.normal(this, "网络断开，请检查网络").show();
            return;
        }
        ShareWxPopView wxPopView = new ShareWxPopView(this).setOutSideCancelable(true);
        if (mEnterType == 1) {
            mShareUrl = mShareUrl + "?shareType=" + mEnterType + "&title=" + mTitle + "&productId=" + mId + "&desc=" + mSubTitle + "&imgUrl=" + OSSConfig.getOSSURLedStr(mCoverUrl);
        } else if (mEnterType == 2) {
            mShareUrl = mShareUrl + "?shareType=" + mEnterType + "&title=" + mTitle + "&id=" + mId + "&desc=" + mSubTitle + "&imgUrl=" + OSSConfig.getOSSURLedStr(mCoverUrl);
        }
        wxPopView.setLiveShareData(OSSConfig.getOSSURLedStr(mCoverUrl), mShareUrl,
                mTitle, mSubTitle);
        wxPopView.show(true);
    }
}
