package com.tojoy.cloud.online_meeting.App.Live.CollectOuterLive;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.tojoy.tjoybaselib.model.live.OutlinkListResponse;
import com.tojoy.tjoybaselib.model.live.SelectOutLiveResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.KeyboardControlUtil;
import com.tojoy.tjoybaselib.model.live.OnlineMeetingHomeGridModel;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.OnlineMembersHelper;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.Services.EventBus.EndLiveEventBus;
import com.tojoy.common.Services.EventBus.OuterLiveEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import rx.Observer;

public class OuterLiveListAct extends UI {

    RelativeLayout mUnicornProjectRlv;
    TJRecyclerView mRecyclerView;
    RelativeLayout mEmptyRLv;
    ArrayList<OnlineMeetingHomeGridModel> mList = new ArrayList<>();
    OuterLiveAdapter mAdapter;
    EditText mSearchKeyEt;

    String mSearchKey = "";
    boolean isSelecting = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outer_live_layout);
        EventBus.getDefault().register(this);
        initTitle();
        initView();
        initData();
    }

    public static void start(Activity context) {
        Intent intent = new Intent();
        intent.setClass(context, OuterLiveListAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivityForResult(intent, 1008);
    }

    private void initTitle() {
        setStatusBar();
        int statusBarHeight = AppUtils.getStatusBarHeight(this);
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.dip2px(this, 48));
        lps.setMargins(0, statusBarHeight, 0, 0);
        findViewById(R.id.rtl_normal_title).setLayoutParams(lps);
        setStatusBarLightMode();
    }

    private void initView() {
        mSearchKeyEt = (EditText) findViewById(R.id.basequery);
        mSearchKeyEt.clearFocus();

        mEmptyRLv = (RelativeLayout) findViewById(R.id.rlv_empty);
        mUnicornProjectRlv = (RelativeLayout) findViewById(R.id.mUnicornProjectRlv);
        mRecyclerView = new TJRecyclerView(this);
        mRecyclerView.setGridLayoutManager(this, 2);
        mUnicornProjectRlv.addView(mRecyclerView);
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                mList.clear();
                requestListData();
            }

            @Override
            public void onLoadMore() {
                mPage++;
                showProgressHUD(OuterLiveListAct.this, "");
                requestListData();
            }
        });

        mAdapter = new OuterLiveAdapter(this, mList);
        mAdapter.hasHeader = false;
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.updateData(mList);
        mRecyclerView.refreshLoadMoreView(1, mAdapter.getItemCount());

        findViewById(R.id.iv_basetitle_leftimg).setOnClickListener(v -> finish());
        findViewById(R.id.iv_search_cancel).setOnClickListener(v -> mSearchKeyEt.setText(""));

        mSearchKeyEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSearchKey = s.toString().trim();
                requestListData();
                findViewById(R.id.iv_search_cancel).setVisibility(s.toString().length() > 0 ? View.VISIBLE : View.GONE);
                if (s.toString().length() == 0) {
                    KeyboardControlUtil.hideKeyboard(mSearchKeyEt);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
            if ("1".equals(data.isUse)) {
                return;
            }
            if (!OnlineMembersHelper.getInstance().canOnlineNow()) {
                Toasty.normal(this, "数量已达上线，请稍后重试").show();
                return;
            }
            if (isSelecting) {
                showProgressHUD(OuterLiveListAct.this, "处理中");
                return;
            }
            isSelecting = true;
            showProgressHUD(OuterLiveListAct.this, "处理中");

            OMAppApiProvider.getInstance().mSelectAOuterLive(LiveRoomInfoProvider.getInstance().liveId,
                    data.roomLiveId,
                    OnlineMembersHelper.getInstance().getCurrentOnlineStreams(),
                    String.valueOf(OnlineMembersHelper.getInstance().mOnlineMembers.size() + 1),
                    new Observer<SelectOutLiveResponse>() {
                        @Override
                        public void onCompleted() {
                            dismissProgressHUD();
                            isSelecting = false;
                        }

                        @Override
                        public void onError(Throwable e) {
                            dismissProgressHUD();
                            isSelecting = false;
                        }

                        @Override
                        public void onNext(SelectOutLiveResponse response) {
                            if (response.isSuccess()) {
                                if (isFinishing()) {
                                    // 此方法主要针对与在引流列表点击引流后，没有等接口返回就直接finish的情况
                                    EventBus.getDefault().post(new OuterLiveEvent(response.data.respStreamRoomDtoList));
                                } else {
                                    Intent aIntent = new Intent();
                                    aIntent.putParcelableArrayListExtra("selectedList", response.data.respStreamRoomDtoList);
                                    setResult(2008, aIntent);
                                    finish();
                                }
                            } else {
                                if ("120".equals(response.code)) {
                                    OuterLiveLimitPop crPermissionsDialog = new OuterLiveLimitPop(OuterLiveListAct.this);
                                    crPermissionsDialog.setOutliveCount(response.data.count, response.data.delCount);
                                    crPermissionsDialog.show();
                                } else {
                                    Toasty.normal(OuterLiveListAct.this, response.msg).show();
                                }
                            }
                        }
                    });
        });
    }

    protected void initData() {
        mRecyclerView.autoRefresh();
    }

    private void requestListData() {
        OMAppApiProvider.getInstance().mOuterLiveRoomList(LiveRoomInfoProvider.getInstance().liveId, mSearchKey, mPage, new Observer<OutlinkListResponse>() {
            @Override
            public void onCompleted() {
                mRecyclerView.setRefreshing(false);
                if (mList.size() == 0) {
                    mEmptyRLv.setVisibility(View.VISIBLE);
                } else {
                    mEmptyRLv.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(Throwable e) {
                if (mList.size() == 0) {
                    mEmptyRLv.setVisibility(View.VISIBLE);
                } else {
                    mEmptyRLv.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNext(OutlinkListResponse response) {
                if (response.isSuccess() && response.data != null && response.data.list != null) {
                    if (mPage == 1) {
                        mList.clear();
                    }
                    mList.addAll(response.data.list);
                    mAdapter.updateData(mList);
                    mRecyclerView.refreshLoadMoreView(mPage, response.data.list.size());
                } else {
                    mRecyclerView.refreshLoadMoreView(mPage, 0);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEndLiveEvent(EndLiveEventBus event) {
        if (event.isEnd) {
            finish();
        }
    }
}
