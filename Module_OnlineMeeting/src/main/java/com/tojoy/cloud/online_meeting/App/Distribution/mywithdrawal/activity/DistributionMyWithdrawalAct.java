package com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.activity;

import android.os.Bundle;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.fragment.DistributionMyWithdrawalFragment;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.util.sys.ActivityUtils;

/**
 * 分销客 我的提现
 *
 * @author houxianjun
 * @Date 2020/08/21
 */
@Route(path = RouterPathProvider.Distribution_MyWithdrawal_Act)
public class DistributionMyWithdrawalAct extends UI {
    DistributionMyWithdrawalFragment distributionMyWithdrawalFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distribution_mywithdrawal);
        ARouter.getInstance().inject(this);
        setStatusBar();
        setSwipeBackEnable(false);
        initTitle();
        distributionMyWithdrawalFragment = DistributionMyWithdrawalFragment.newInstance();

        ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), distributionMyWithdrawalFragment, R.id.fragmentContainer);
    }

    private void initTitle() {
        setUpNavigationBar();
        setTitle("提现");
        showBack();
    }

    @Override
    protected void onLeftManagerImageClick() {
        this.finish();
    }
}
