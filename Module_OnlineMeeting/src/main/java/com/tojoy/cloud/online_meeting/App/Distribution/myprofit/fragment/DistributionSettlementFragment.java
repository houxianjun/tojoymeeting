package com.tojoy.cloud.online_meeting.App.Distribution.myprofit.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tojoy.cloud.online_meeting.App.Distribution.myprofit.adapter.MyProfitAdapter;
import com.tojoy.tjoybaselib.model.home.MineProfitListDistributionResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.ui.scrollableHelper.ScrollableHelper;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.cloud.online_meeting.R;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 我的收益 已结算／待结算 列表
 *
 * @author houxianjun
 * @Date 2020/07/16
 */
@SuppressLint("ValidFragment")
public class DistributionSettlementFragment extends Fragment implements ScrollableHelper.ScrollableContainer {
    private Context mContext;
    private View mView;
    private TJRecyclerView mRecyclerView;
    private MyProfitAdapter mAdapter;
    List<MineProfitListDistributionResponse.MyProfitRecordsModel> datas = new ArrayList<>();
    int pageNum = 1;
    int pageSize = 10;
    //1 会议列表 0 商品列表
    String status;
    int mPageTotal;

    public DistributionSettlementFragment(String tmpstatus) {
        this.status = tmpstatus;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_distribution_mymember, container, false);
        initUI();
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    @Override
    public View getScrollableView() {
        return mRecyclerView.getRecyclerView();
    }

    private void initUI() {
        mRecyclerView = mView.findViewById(R.id.recyclerView);
        mRecyclerView.setVerticalScrollBarEnabled(false);
        mAdapter = new MyProfitAdapter(getActivity(), datas);
        mRecyclerView.setLoadMoreViewWithHide();
        if ("1".equals(this.status)) {
            mAdapter.setDistributionType("meeting");
            mRecyclerView.setEmptyImgAndTip(R.drawable.icon_no_content, "暂无会议推广记录");
        } else {
            mRecyclerView.setEmptyImgAndTip(R.drawable.icon_no_content, "暂无商品推广记录");
            mAdapter.setDistributionType("shop");
        }
        mRecyclerView.setRefreshable(false);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {

            }

            @Override
            public void onLoadMore() {
                pageNum++;
                initData();

            }
        });

    }

    /**
     * 初始化列表数据
     */
    public void initData() {
        if ("1".equals(status)) {
            OMAppApiProvider.getInstance().queryDistributionCommissionList(pageNum + "", pageSize + "", status,
                    new Observer<MineProfitListDistributionResponse>() {

                        @Override
                        public void onCompleted() {
                            mRecyclerView.stopRefresh();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toasty.normal(getActivity(), "网络错误，请检查网络").show();
                            mRecyclerView.stopRefresh();
                        }

                        @Override
                        public void onNext(MineProfitListDistributionResponse response) {
                            if (response.isSuccess()) {
                                mPageTotal = response.data.total;
                                if (pageNum == 1) {
                                    datas.clear();
                                }
                                if (response.data.records != null) {
                                    datas.addAll(response.data.records);
                                    mAdapter.updateData(datas);
                                    mRecyclerView.refreshLoadMoreView(pageNum, response.data.records.size());

                                } else {
                                    if (pageNum == 1) {
                                        mRecyclerView.refreshLoadMoreView(pageNum, 0);
                                    }
                                }

                            } else {
                                Toasty.normal(getActivity(), response.msg).show();
                            }
                        }
                    });
        } else {
            OMAppApiProvider.getInstance().queryDistributionGoodsdetailList(pageNum + "", pageSize + "",
                    new Observer<MineProfitListDistributionResponse>() {

                        @Override
                        public void onCompleted() {
                            mRecyclerView.stopRefresh();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toasty.normal(getActivity(), "网络错误，请检查网络").show();
                            mRecyclerView.stopRefresh();
                        }

                        @Override
                        public void onNext(MineProfitListDistributionResponse response) {
                            if (response.isSuccess()) {
                                mPageTotal = response.data.total;
                                if (pageNum == 1) {
                                    datas.clear();
                                }
                                if (response.data.records != null) {
                                    datas.addAll(response.data.records);
                                    mAdapter.updateData(datas);
                                    mRecyclerView.refreshLoadMoreView(pageNum, response.data.records.size());

                                } else {
                                    if (pageNum == 1) {
                                        mRecyclerView.refreshLoadMoreView(pageNum, 0);
                                    }
                                }

                            } else {
                                Toasty.normal(getActivity(), response.msg).show();
                            }
                        }
                    });
        }
    }
}
