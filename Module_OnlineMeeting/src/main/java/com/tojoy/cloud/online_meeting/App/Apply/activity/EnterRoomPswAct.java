package com.tojoy.cloud.online_meeting.App.Apply.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.netease.nim.uikit.api.NimUIKit;
import com.netease.nim.uikit.business.chatroom.helper.ChatRoomHelper;
import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.netease.nim.uikit.common.NIMCache;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.user.IMTokenResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.services.permission.annotation.OnMPermissionDenied;
import com.tojoy.tjoybaselib.services.permission.annotation.OnMPermissionGranted;
import com.tojoy.tjoybaselib.services.permission.annotation.OnMPermissionNeverAskAgain;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.util.sys.LiveActAtack;
import com.tojoy.cloud.online_meeting.App.widgets.SecurityCodeView;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.Services.EventBus.LiveRoomEvent;

import org.greenrobot.eventbus.EventBus;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * @author qululu
 * 进入房间前验证密码页面（若设置密码才会进入此页面，否则直接进入直播间）
 */
@Route(path = RouterPathProvider.ONLINE_MEETING_ENTER_ROOM_PSW)
public class EnterRoomPswAct extends UI implements SecurityCodeView.InputCompleteListener {

    private SecurityCodeView editText;
    private TextView mPswErrorHintTv;

    private String mRoomName = "";
    private String mLiveId = "";

    private String mPhoneNum = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_room_psw);
        if (!BaseUserInfoCache.isLogined(this)) {
            ARouter.getInstance().build(RouterPathProvider.Login)
                    .navigation();
            finish();
            return;
        }
        mRoomName = getIntent().getStringExtra("roomName");
        mLiveId = getIntent().getStringExtra("liveId");
        setStatusBar();
        initUI();
        initTitle();
    }

    private void initUI() {
        editText = (SecurityCodeView) findViewById(R.id.edit_security_code);
        mPswErrorHintTv = (TextView) findViewById(R.id.tv_psw_error_hint);
        TextView mRoomTitleTv = (TextView) findViewById(R.id.tv_room_title);
        editText.setInputCompleteListener(this);

        mRoomTitleTv.setText(TextUtils.isEmpty(mRoomName) ? "输入密码" : mRoomName);
        mHandler.postDelayed(() -> {
            editText.getEditText().setVisibility(View.VISIBLE);
            editText.getEditText().requestFocus();

            showKeyboard(true, editText.getEditText());
        }, 500);
    }

    private void initTitle() {
        setUpNavigationBar();
        setTitle("会议密码");
        showBack();
    }

    @Override
    public void inputComplete() {
        if (TextUtils.isEmpty(mLiveId)) {
            return;
        }
        if (ChatRoomHelper.isFromLiveRoom) {
            if (LiveRoomInfoProvider.getInstance().isHost()) {
                Toasty.normal(EnterRoomPswAct.this, "正在直播，请稍后再试").show();
            } else if (LiveRoomInfoProvider.getInstance().liveId.equals(mLiveId)) {
                LiveActAtack.getActivityStack().popAllActivity();
            } else {
                TJMakeSureDialog tjMakeSureDialog = new TJMakeSureDialog(EnterRoomPswAct.this);
                tjMakeSureDialog.setTitleAndCotent("", "您还未离开当前会议\n要加入新的会议么");
                tjMakeSureDialog.setBtnSureText("加入");
                tjMakeSureDialog.setBtnCancelText("不加入");
                tjMakeSureDialog.hideTitle();
                tjMakeSureDialog.setEvent(v1 -> {
                    if (AppConfig.isLoginIM) {
                        enterLiveRoom(editText.getEditContent());
                    } else {
                        loginim();
                    }
                    tjMakeSureDialog.dismissStrongly();
                    //关闭之前打开的
                    EventBus.getDefault().post(new LiveRoomEvent(LiveRoomEvent.LIVE_CLOSE_GUEST, ""));
                });
                tjMakeSureDialog.show();
            }
        } else {
            if (AppConfig.isLoginIM) {
                enterLiveRoom(editText.getEditContent());
            } else {
                loginim();
            }
        }

    }

    private void loginim() {
        OMAppApiProvider.getInstance().getIMToken(BaseUserInfoCache.getUserId(EnterRoomPswAct.this), "0", new Observer<IMTokenResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                statitiscIMLogin("0");
                if (AppConfig.isCanToast()) {
                    Toasty.normal(EnterRoomPswAct.this, "getIMToken error = " + e.getMessage()).show();
                } else {
                    Toasty.normal(EnterRoomPswAct.this, "网络错误，请稍后再试").show();
                }
            }

            @Override
            public void onNext(IMTokenResponse imToken) {
                if (imToken.isSuccess()) {
                    BaseUserInfoCache.setIMAccountHeaderBase(EnterRoomPswAct.this, imToken.data.env);
                    NimUIKit.setIMHeaderStr(imToken.data.env);
                    BaseUserInfoCache.saveIMTokenBase(EnterRoomPswAct.this, imToken.data.token);

                    String account = BaseUserInfoCache.getIMHeaderedAccount(EnterRoomPswAct.this);
                    NimUIKit.login(new LoginInfo(account, imToken.data.token), new RequestCallback<LoginInfo>() {
                        @Override
                        public void onSuccess(LoginInfo param) {
                            NIMCache.setAccount(account);
                            enterLiveRoom(editText.getEditContent());
                            statitiscIMLogin("1");
                        }

                        @Override
                        public void onFailed(int code) {
                            statitiscIMLogin("0");
                            if (AppConfig.isCanToast()) {
                                Toasty.normal(EnterRoomPswAct.this, "登录IM 失败 code = " + code).show();
                            } else {
                                Toasty.normal(EnterRoomPswAct.this, "网络错误，请稍后再试").show();
                            }
                        }

                        @Override
                        public void onException(Throwable exception) {
                            statitiscIMLogin("0");
                            if (AppConfig.isCanToast()) {
                                Toasty.normal(EnterRoomPswAct.this, "登录IM 异常 e = " + exception.getMessage()).show();
                            } else {
                                Toasty.normal(EnterRoomPswAct.this, "网络错误，请稍后再试").show();
                            }
                        }
                    });

                } else {
                    statitiscIMLogin("0");
                    if (AppConfig.isCanToast()) {
                        Toasty.normal(EnterRoomPswAct.this, "getIMToken 异常 = " + imToken.msg).show();
                    } else {
                        Toasty.normal(EnterRoomPswAct.this, "网络错误，请稍后再试").show();
                    }
                }

            }
        });
    }

    private void statitiscIMLogin(String loginIMState) {

        OMAppApiProvider.getInstance().statitiscIMLogin(loginIMState, "enterLivePage", new rx.Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(OMBaseResponse omBaseResponse) {
            }
        });
    }

    @Override
    protected void onLeftManagerImageClick() {
        hideKeyboard(this);
        finish();
    }
    /**
     * 隐藏键盘的方法
     *
     * @param context
     */
    public static void hideKeyboard(Activity context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        // 隐藏软键盘
        assert imm != null;
        imm.hideSoftInputFromWindow(context.getWindow().getDecorView().getWindowToken(), 0);
    }
    /**
     * 进入直播间接口（包括判断密码正确与否与人数是否已满）
     */
    private void enterLiveRoom(String password) {
        //先调一次退出直播间 针对该用户从直播间点主页后跳另外的直播间
        String oldLiveId = getIntent().getStringExtra("oldLiveId");
        if (!TextUtils.isEmpty(oldLiveId) && !oldLiveId.equals(mLiveId)) {
            LiveRoomInfoProvider.getInstance().lastLiveId = oldLiveId;
            LiveRoomInfoProvider.getInstance().lastIMRoomId = getIntent().getStringExtra("oldIMRoomId");
        } else if (!TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().liveId) && !TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().imRoomId)
                && !LiveRoomInfoProvider.getInstance().liveId.equals(mLiveId)) {
            LiveRoomInfoProvider.getInstance().lastLiveId = LiveRoomInfoProvider.getInstance().liveId;
            LiveRoomInfoProvider.getInstance().lastIMRoomId = LiveRoomInfoProvider.getInstance().imRoomId;
        } else {
            LiveRoomInfoProvider.getInstance().lastLiveId = "";
        }
        LiveRoomIOHelper.exitOldUser(BaseUserInfoCache.getUserId(EnterRoomPswAct.this));

        LiveRoomInfoProvider.getInstance().joinPassword = password;
        LiveRoomInfoProvider.getInstance().liveId = mLiveId;
        showProgressHUD(this, "加载中");
        LiveRoomIOHelper.joinLiveRoom(this, new IOLiveRoomListener() {
            @Override
            public void onIOError(String error, int errorCode) {
                dismissProgressHUD();
                editText.clearEditText();
                if (errorCode == 105) {
                    //密码错误
                    mPswErrorHintTv.setText("密码错误，请重试！");
                } else if (errorCode == 302) {
                    mPhoneNum = error;
                    mPswErrorHintTv.setText("密码错误，请重试！");
                    // 输入密码超过5次
                    String content = "请拨打" + mPhoneNum + "转2\n" + "获取直播间密码";
                    new TJMakeSureDialog(EnterRoomPswAct.this, content,
                            v -> {
                                // 拨打
                                checkPersimiss(PERMISSIONS_MAKECALL, PERMISSIONS_MAKECALL_REQUEST_CODE);
                            }).setBtnText("拨打电话", "重新输入").show();

                } else {
                    if (!TextUtils.isEmpty(error)) {
                        Toasty.normal(EnterRoomPswAct.this, error).show();
                    }
                }
            }

            @Override
            public void onIOSuccess() {
                dismissProgressHUD();
                // 密码输入成功，进入直播间，关闭此页面
                finish();
            }
        });

    }

    @Override
    public void deleteContent(boolean isDelete) {
        if (isDelete) {
            mPswErrorHintTv.setText("");
        }
    }

    //拨打电话
    @OnMPermissionGranted(PERMISSIONS_MAKECALL_REQUEST_CODE)
    public void onBasicPermissionSuccessMobile() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mPhoneNum));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @OnMPermissionDenied(PERMISSIONS_MAKECALL_REQUEST_CODE)
    public void onBasicPermissionDenyMobile() {
        Toasty.normal(this, "请设置开启拨打电话权限").show();
    }

    @OnMPermissionNeverAskAgain(PERMISSIONS_MAKECALL_REQUEST_CODE)
    public void onBasicPermissionNerveAskMobile() {
        Toasty.normal(this, "请设置开启拨打电话权限").show();
    }


}
