package com.tojoy.cloud.online_meeting.App.Distribution.mymember.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.cloud.online_meeting.App.Distribution.mymember.fragment.MyMemberFragment;
import com.tojoy.cloud.online_meeting.App.Distribution.view.CommonProfitView;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.home.MineMemberDistributionResponse;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.imageManager.YuanJiaoImageView;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;

import java.util.List;

public class MyMemberAdapter extends BaseRecyclerViewAdapter<MineMemberDistributionResponse.MyMemberRecordsModel> {
    private Context mContext;
    //0 我的会员 1 我的团队
    private String mstatus;

    public MyMemberAdapter(@NonNull Context context, @NonNull String status, @NonNull List<MineMemberDistributionResponse.MyMemberRecordsModel> datas) {
        super(context, datas);
        mContext = context;
        this.mstatus = status;
    }

    @Override
    protected int getViewType(int position, @NonNull MineMemberDistributionResponse.MyMemberRecordsModel data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_distribution_mymember;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new MyMemberVH(mContext, parent, getLayoutResId(viewType));
    }

    private class MyMemberVH extends BaseRecyclerViewHolder<MineMemberDistributionResponse.MyMemberRecordsModel> {

        private YuanJiaoImageView logoImageView;
        private TextView tvMymemberName;
        private TextView tvMymemberBindTime;
        private CommonProfitView viewTodayProfit;
        private CommonProfitView viewTotalProfit;
        private LinearLayout llTeamCount;
        private TextView tvTeamCount;

        MyMemberVH(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            bindViews();
        }

        private void bindViews() {
            logoImageView = itemView.findViewById(R.id.iv_mymember_icon);
            tvMymemberName = itemView.findViewById(R.id.tv_mymember_name);
            tvMymemberBindTime = itemView.findViewById(R.id.tv_mymember_bind_time);
            viewTodayProfit = itemView.findViewById(R.id.view_today_profit);
            viewTotalProfit = itemView.findViewById(R.id.view_total_profit);
            llTeamCount = itemView.findViewById(R.id.ll_team_count);
            tvTeamCount = itemView.findViewById(R.id.tv_team_count);
        }

        @Override
        public void onBindData(MineMemberDistributionResponse.MyMemberRecordsModel data, int position) {

            ImageLoaderManager.INSTANCE.loadHomeHotCompnayIcon(getContext(), logoImageView, data.avatar, R.drawable.img_search_company_default, "#DDE2EB", 1);
            tvMymemberName.setText(data.trueName);
            tvMymemberBindTime.setText("绑定时间：" + data.bindDateTime);
            viewTodayProfit.initData(data.currentDayPrice, "今日成交(元)");
            viewTotalProfit.initData(data.totalPrice, "累计成交(元)");
            if ("1".equals(mstatus)) {
                llTeamCount.setVisibility(View.VISIBLE);
                if (data.count == null) {
                    tvTeamCount.setText("0");
                } else {
                    tvTeamCount.setText(data.count + ">");
                    //跳转至团队列表页面
                    llTeamCount.setOnClickListener(v ->
                            ARouter.getInstance()
                                    .build(RouterPathProvider.Distribution_MyMember_TeamList_Act)
                                    .withString("distributorId", data.distributorId)
                                    .navigation());
                }
            } else {
                llTeamCount.setVisibility(View.GONE);
                tvTeamCount.setText("");
            }
        }
    }

}
