package com.tojoy.cloud.online_meeting.App.Distribution.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.util.sys.AppUtils;

public class PayChannelUnbindDialog {
    private Context mContext;
    private View mView;
    private Dialog alertDialog;

    public PayChannelUnbindDialog(Context context, View.OnClickListener onClickListener) {
        mContext = context;
        initUI();
        initEvent(onClickListener, true);
    }
    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_pay_channel_unbind, null);
    }
    private void initEvent(View.OnClickListener onClickListener, boolean isCanDismiss) {
        mView.findViewById(com.tojoy.tjoybaselib.R.id.rlv_sure).setOnClickListener(v -> {
            if (isCanDismiss) {
                alertDialog.dismiss();
            }
            if (onClickListener != null) {
                onClickListener.onClick(v);
            }
        });
        mView.findViewById(com.tojoy.tjoybaselib.R.id.rlv_cancle).setOnClickListener(v -> {
            if (isCanDismiss) {
                alertDialog.dismiss();
            }
        });
    }
    public void show() {

        if (mView.getParent() != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            parent.removeAllViews();
        }
        alertDialog = new Dialog(mContext, com.tojoy.tjoybaselib.R.style.tjoy_dialog_style);
        alertDialog.setContentView(mView, new ViewGroup.LayoutParams(AppUtils.getWidth(mContext) - AppUtils.dip2px(mContext, 120), ViewGroup.LayoutParams.WRAP_CONTENT));
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        alertDialog.setOnKeyListener((dialog, keyCode, event) -> true);
    }
    /**
     * ****************** UI定制化
     */
    public PayChannelUnbindDialog setTitleAndCotent(String content) {
        ((TextView) mView.findViewById(com.tojoy.tjoybaselib.R.id.tv_content)).setText(content);
        return this;
    }
}
