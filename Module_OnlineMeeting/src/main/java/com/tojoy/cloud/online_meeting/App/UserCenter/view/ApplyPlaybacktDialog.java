package com.tojoy.cloud.online_meeting.App.UserCenter.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.R;


/**
 * Created by luuzhu on 2019/9/23.
 */

public class ApplyPlaybacktDialog {

    private Activity mContext;
    private Dialog mDialog;
    private View mView;

    public ApplyPlaybacktDialog(Context context) {
        mContext = (Activity) context;
        initUI();
    }

    @SuppressLint("WrongConstant")
    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_live_apply_playback, null);
        initView();
    }

    public boolean isShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

    public void show() {

        mDialog = new Dialog(mContext, R.style.CenterProject);
        mDialog.setContentView(mView);

        Window win = mDialog.getWindow();
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = AppUtils.getScreenWidth(mContext) - AppUtils.dip2px(mContext, 90);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.windowAnimations = R.style.CenterInAndOutStyle;
        lp.gravity = Gravity.CENTER;
        win.setAttributes(lp);

        mDialog.setCanceledOnTouchOutside(false);
        try {
            mDialog.show();
            mDialog.setOnKeyListener((dialog, keyCode, event) -> true);
        } catch (Exception e) {

        }
    }

    private void initView() {

        mView.findViewById(R.id.tvIKnow).setOnClickListener(view -> dismiss());

    }
}
