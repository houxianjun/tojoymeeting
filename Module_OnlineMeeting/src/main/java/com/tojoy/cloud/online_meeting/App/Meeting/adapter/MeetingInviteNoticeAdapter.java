package com.tojoy.cloud.online_meeting.App.Meeting.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.time.TimeUtility;
import com.tojoy.tjoybaselib.model.home.MeetingInviteModel;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;

import static com.tojoy.tjoybaselib.util.time.TimeUtility.YEAR_HOUR_Notice;


public class MeetingInviteNoticeAdapter extends BaseRecyclerViewAdapter<MeetingInviteModel> {

    public MeetingInviteNoticeAdapter(@NonNull Context context, @NonNull List<MeetingInviteModel> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull MeetingInviteModel data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_meetinginvite_layout;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new MeetingInviteViewHolder(context, parent, getLayoutResId(viewType));
    }

    /**
     * ViewHolder
     */
    public class MeetingInviteViewHolder extends BaseRecyclerViewHolder<MeetingInviteModel> {

        TextView mTime;
        TextView mContent;
        TextView mCreator;
        ImageView mJoinIv;
        TextView mStatus;

        public MeetingInviteViewHolder(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            mTime = itemView.findViewById(R.id.tv_notice_time);
            mContent = itemView.findViewById(R.id.tv_notice_desc);
            mCreator = itemView.findViewById(R.id.tv_creator);
            mJoinIv = itemView.findViewById(R.id.rlv_join);
            mStatus = itemView.findViewById(R.id.tv_end);
        }

        @Override
        public void onBindData(MeetingInviteModel meetingModel, int position) {
            mTime.setText(TimeUtility.getTimeFormat(Long.parseLong(TextUtils.isEmpty(meetingModel.createDate) ? "0" : meetingModel.createDate), YEAR_HOUR_Notice));
            mContent.setText(meetingModel.messages);
            mCreator.setText("发起人：" + meetingModel.userName);

            if ("1".equals(meetingModel.status)) {
                mJoinIv.setVisibility(View.GONE);
                mStatus.setVisibility(View.VISIBLE);
                mStatus.setText("未开始");
            } else if ("2".equals(meetingModel.status) || "3".equals(meetingModel.status)) {
                mJoinIv.setVisibility(View.VISIBLE);
                mStatus.setVisibility(View.GONE);
            } else {
                mJoinIv.setVisibility(View.GONE);
                mStatus.setVisibility(View.VISIBLE);
                mStatus.setText("已结束");
            }

            itemView.setOnClickListener(v -> mJoinLiveCallback.joinLiveRoom(meetingModel, position));
        }
    }

    public interface JoinLiveCallback {
        void joinLiveRoom(MeetingInviteModel meetingModel, int pos);
    }

    public JoinLiveCallback mJoinLiveCallback;
}
