package com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.activity.DistributionMyWithdrawalAct;
import com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.view.DistributionWithdrawalUnbindView;
import com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.view.DistributionWithdrawalAccountView;
import com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.view.DistributionWithdrawalAmountView;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.Services.PayAndShare.channel.PayAliChannel;
import com.tojoy.common.Services.PayAndShare.callback.PayChannelStatusCallBack;
import com.tojoy.common.Services.PayAndShare.channel.PayChannelFactory;
import com.tojoy.tjoybaselib.model.home.MineProfitInfoDistributionResponse;
import com.tojoy.tjoybaselib.model.home.MineWithdrawalAccountBindResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 支付宝提现页面
 * @author  houxianjun
 * @Date 2020/08/20
 */
public class DistributionMyWithdrawalFragment extends Fragment  {

    private MineWithdrawalAccountBindResponse.AccountBind accountBind;

    private MineProfitInfoDistributionResponse.MineProfitDistributionModel myProfitModel;

    private DistributionWithdrawalAccountView accountView;

    private DistributionWithdrawalAmountView amountView;

    private DistributionWithdrawalUnbindView accountUnBindView;

    public MineWithdrawalAccountBindResponse.AccountBind getAccountBind() {
        return accountBind;
    }

    public void setAccountBind(MineWithdrawalAccountBindResponse.AccountBind accountBind) {
        this.accountBind = accountBind;
    }

    PayAliChannel payAliChannel;

    public PayAliChannel getPayAliChannel() {
        return payAliChannel;
    }

    public DistributionMyWithdrawalFragment() {

    }

    public static DistributionMyWithdrawalFragment newInstance() {

        DistributionMyWithdrawalFragment fragment = new DistributionMyWithdrawalFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        payAliChannel= (PayAliChannel) PayChannelFactory.getPayChannel("ali");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_distribution_mywithdrawal, container, false);

        initUI(layout);

        return layout;

    }


    private void initUI(View layout) {

        accountView = layout.findViewById(R.id.view_account);
        amountView= layout.findViewById(R.id.view_amount);
        accountUnBindView=layout.findViewById(R.id.view_amountrecord);
        accountView.setMainFragment(this);
        amountView.setMainFragment(this);
        accountUnBindView.setMainFragment(this);
    }

    public void initData() {

        OMAppApiProvider.getInstance().queryDistributionMyProfit(new Observer<MineProfitInfoDistributionResponse>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(getActivity(), "网络错误，请检查网络").show();

            }

            @Override
            public void onNext(MineProfitInfoDistributionResponse response) {
                if (response.isSuccess()) {
                    myProfitModel = response.data;
                    updateProfitData(myProfitModel);
                    accountUnBindView.setBindTipsContent(myProfitModel.minMoney);
                } else {
                    Toasty.normal(getActivity(), response.msg).show();
                }
            }
        });

    }


    public void updateProfitData(MineProfitInfoDistributionResponse.MineProfitDistributionModel model) {

        amountView.setAmount(model.cashIncome);
        amountView.setLimitWithdrawalMoney(Double.valueOf(model.minMoney));
    }
    @Override
    public void onResume() {
        super.onResume();
        initData();
        queryAliAccountBindingStatus();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    /**
     * 获取用户绑定状态
     */
    public void queryAliAccountBindingStatus(){

        //查询绑定状态
        payAliChannel.isChannelBind(new PayChannelStatusCallBack<MineWithdrawalAccountBindResponse>() {
            @Override
            public void onFail(String msg) {
                Toasty.normal(getActivity(),msg).show();
            }

            @Override
            public void onSuccess(MineWithdrawalAccountBindResponse response) {

                MineWithdrawalAccountBindResponse mdata =response;
                setAccountBind(mdata.data);
                amountView.updateWithdrawalStatus();
                accountView.updateBindStatus(mdata.data.status,mdata.data.nickName);
                accountUnBindView.updateBindBtnStatus(mdata.data.status);

            }
        });

    }

    public void showLoading(String content){
       if ((DistributionMyWithdrawalAct) getActivity()!=null&&!((DistributionMyWithdrawalAct)getActivity()).isFinishing()){
           ((DistributionMyWithdrawalAct) getActivity()).showProgressHUD(getActivity(),content);
       }
    }

    public void dismissLoading(){
        if ((DistributionMyWithdrawalAct) getActivity()!=null&&!((DistributionMyWithdrawalAct)getActivity()).isFinishing()){
            ((DistributionMyWithdrawalAct) getActivity()).dismissProgressHUD();
        }
    }

}
