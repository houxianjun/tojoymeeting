package com.tojoy.cloud.online_meeting.App.Distribution.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.cloud.online_meeting.R;


/**
 * 公共收益组件使用场景：我的会员item 我的收益页面
 *
 * @author houxianjun
 * @Date 2020.07.08
 */
public class CommonProfitView extends RelativeLayout {

    private TextView tvProfit;
    private TextView tvProfitDescribe;

    public CommonProfitView(Context context) {
        super(context);
        initView(context);
    }

    public CommonProfitView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public void initView(Context context) {
        View mView = LayoutInflater.from(context).inflate(R.layout.view_distribution_commonprofit, null);
        tvProfit = mView.findViewById(R.id.tv_profit);
        tvProfitDescribe = mView.findViewById(R.id.tv_profit_describe);

        this.addView(mView);
    }

    /**
     * 初始化数据
     */
    public void initData(String profit, String describe) {
        tvProfit.setText(profit != null ? profit : "");
        tvProfitDescribe.setText(describe != null ? describe : "");
    }
}
