package com.tojoy.cloud.online_meeting.App.Home.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;

import com.alibaba.android.arouter.launcher.ARouter;
import com.netease.nim.uikit.business.chatroom.helper.ChatRoomHelper;
import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.enterprise.EnterpriseHomeHotLiveListResponse;
import com.tojoy.tjoybaselib.model.live.OnlineMeetingHomeGridModel;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.sys.LiveActAtack;
import com.tojoy.cloud.online_meeting.App.Meeting.adapter.LivecenterMultiAdapter;
import com.tojoy.cloud.online_meeting.App.Meeting.model.MultiAdapterBean;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.App.BaseFragment;
import com.tojoy.common.Services.EventBus.LiveRoomEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import rx.Observer;


public class HotLiveSearchFragment extends BaseFragment {
    private TJRecyclerView mRecyclerView;
    private LivecenterMultiAdapter mAdapter;
    ArrayList<MultiAdapterBean> mSelfLiveList = new ArrayList<>();
    private int mPage;
    private String mSearchKey;
    private String mCompanyCode;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_hot_live_search, container, false);
        }
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        isCreateView = true;
        return mView;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initUI() {
        mRecyclerView = mView.findViewById(R.id.recyclerView);
        mRecyclerView.getRecyclerView().setVerticalScrollBarEnabled(false);
        mRecyclerView.setLoadMoreView();
        mRecyclerView.goneFooterView();
        mRecyclerView.setEmptyImgAndTip(R.drawable.icon_empty_online_logo, "暂无数据");
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                initData();
            }

            @Override
            public void onLoadMore() {
                mPage++;
                initData();
            }
        });
        mAdapter = new LivecenterMultiAdapter(getContext(), mSelfLiveList,2);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.autoRefresh();

        mAdapter.setmOnClickJoinDetailListener((view, position, data) -> {
            if (ChatRoomHelper.isFromLiveRoom) {
                if (LiveRoomInfoProvider.getInstance().isHost()) {
                    Toasty.normal(getContext(), "正在直播，请稍后再试").show();
                } else if (LiveRoomInfoProvider.getInstance().liveId.equals(data.roomLiveId)) {
                    LiveActAtack.getActivityStack().popAllActivity();
                } else {
                    TJMakeSureDialog tjMakeSureDialog = new TJMakeSureDialog(getContext());
                    tjMakeSureDialog.setTitleAndCotent("", "您还未离开当前会议\n要加入新的会议么");
                    tjMakeSureDialog.setBtnSureText("加入");
                    tjMakeSureDialog.setBtnCancelText("不加入");
                    tjMakeSureDialog.hideTitle();
                    tjMakeSureDialog.setEvent(v1 -> {
                        joinThisLiveRoom(data);
                        tjMakeSureDialog.dismissStrongly();
                        //关闭之前打开的
                        EventBus.getDefault().post(new LiveRoomEvent(LiveRoomEvent.LIVE_CLOSE_GUEST, ""));
                    });
                    tjMakeSureDialog.show();
                }
            } else {
                joinThisLiveRoom(data);
            }
        });

        /**
         * 滑动监听，回收播放器资源
         */
        mRecyclerView.getRecyclerView().addOnChildAttachStateChangeListener(new RecyclerView.OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(View view) {
                RelativeLayout mRlvAnimContainer = view.findViewById(R.id.rlv_anim_container);
                if (mRlvAnimContainer == null) {
                    return;
                }
                initAnimationView(mRlvAnimContainer.getChildAt(0));
            }

            @Override
            public void onChildViewDetachedFromWindow(View view) {

            }
        });

    }


    private void joinThisLiveRoom(OnlineMeetingHomeGridModel data) {
        if (!TextUtils.isEmpty(data.hasPassword) && "1".equals(data.hasPassword)) {
            // 房间设置了密码
            // 检验是否要输入密码
            LiveRoomIOHelper.checkEnterLiveRoom(getContext(), data.roomLiveId, new IOLiveRoomListener() {
                @Override
                public void onIOError(String error, int errorCode) {
                    if (errorCode == 301) {
                        ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_ENTER_ROOM_PSW)
                                .withString("roomName", data.title)
                                .withString("liveId", data.roomLiveId)
                                .navigation();
                    } else if (errorCode == 302) {
                    } else {
                        Toasty.normal(getContext(), error).show();
                    }
                }

                @Override
                public void onIOSuccess() {
                    // 不需要输入密码，直接进入,包括判断密码正确与否与人数是否已满
                    joinRoom(data);
                }
            });
        } else {
            joinRoom(data);
        }
    }


    public void joinRoom(OnlineMeetingHomeGridModel data) {
        //先调一次退出直播间 针对该用户从直播间点主页后跳另外的直播间
        if (!TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().liveId) && !TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().imRoomId)
                && !LiveRoomInfoProvider.getInstance().liveId.equals(data.roomLiveId)) {
            LiveRoomInfoProvider.getInstance().lastLiveId = LiveRoomInfoProvider.getInstance().liveId;
            LiveRoomInfoProvider.getInstance().lastIMRoomId = LiveRoomInfoProvider.getInstance().imRoomId;
        } else {
            LiveRoomInfoProvider.getInstance().lastLiveId = "";
        }

        LiveRoomInfoProvider.getInstance().joinPassword = "";
        LiveRoomInfoProvider.getInstance().liveId = data.roomLiveId;
        LiveRoomIOHelper.joinLiveRoom(getContext(), new IOLiveRoomListener() {
            @Override
            public void onIOError(String error, int errorCode) {
                dismissProgressHUD();
                if (!TextUtils.isEmpty(error)) {
                    Toasty.normal(getContext(), error).show();
                }
            }

            @Override
            public void onIOSuccess() {
                dismissProgressHUD();
                LiveRoomIOHelper.exitOldUser(BaseUserInfoCache.getUserId(getContext()));
            }
        });
    }


    public void initAnimationView(View mRootView) {
        View mView1 = mRootView.findViewById(R.id.view1);
        View mView2 = mRootView.findViewById(R.id.view2);
        View mView3 = mRootView.findViewById(R.id.view3);
        View mView4 = mRootView.findViewById(R.id.view4);

        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.0f, 1f, 0.1f, Animation.RELATIVE_TO_PARENT
                , 1.0f, Animation.RELATIVE_TO_PARENT
                , 1.0f);
        scaleAnimation.setDuration(400);
        scaleAnimation.setRepeatMode(Animation.REVERSE);
        scaleAnimation.setRepeatCount(-1);
        mView1.startAnimation(scaleAnimation);


        ScaleAnimation scaleAnimation1 = new ScaleAnimation(1.0f, 1.0f, 1f, 0.1f, Animation.RELATIVE_TO_PARENT
                , 1.0f, Animation.RELATIVE_TO_PARENT
                , 1.0f);
        scaleAnimation1.setDuration(350);
        scaleAnimation1.setRepeatMode(Animation.REVERSE);
        scaleAnimation1.setRepeatCount(-1);
        mView2.startAnimation(scaleAnimation1);


        ScaleAnimation scaleAnimation12 = new ScaleAnimation(1.0f, 1.0f, 1f, 0.1f, Animation.RELATIVE_TO_PARENT
                , 1.0f, Animation.RELATIVE_TO_PARENT
                , 1.0f);
        scaleAnimation12.setDuration(450);
        scaleAnimation12.setRepeatMode(Animation.REVERSE);
        scaleAnimation12.setRepeatCount(-1);
        mView3.startAnimation(scaleAnimation12);


        ScaleAnimation scaleAnimation13 = new ScaleAnimation(1.0f, 1.0f, 1f, 0.1f, Animation.RELATIVE_TO_PARENT
                , 1.0f, Animation.RELATIVE_TO_PARENT
                , 1.0f);
        scaleAnimation13.setDuration(500);
        scaleAnimation13.setRepeatMode(Animation.REVERSE);
        scaleAnimation13.setRepeatCount(-1);
        mView4.startAnimation(scaleAnimation13);


    }

    /**
     * 进行搜索
     */
    public void doSearch(String dynamicKey) {
        mPage = 1;
        mSearchKey = dynamicKey;
        mSelfLiveList.clear();
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
        Bundle arguments = this.getArguments();
        mCompanyCode = arguments.getString("companyCode");
        loadData();
    }


    /**
     * 热门推荐列表
     */
    private void loadData() {
        OMAppApiProvider.getInstance().mHotRoomLive(mCompanyCode, mPage, 20, 2, mSearchKey, new Observer<EnterpriseHomeHotLiveListResponse>() {
            @Override
            public void onCompleted() {
                if (mSelfLiveList.size() == 0) {
                    mView.findViewById(R.id.emptyLayout_order).setVisibility(View.VISIBLE);
                } else {
                    mView.findViewById(R.id.emptyLayout_order).setVisibility(View.GONE);
                }
                mRecyclerView.setRefreshing(false);
            }

            @Override
            public void onError(Throwable e) {
                mRecyclerView.refreshLoadMoreView(1, 0);
                Toasty.normal(getContext(), "网络异常，稍后再试").show();
            }

            @Override
            public void onNext(EnterpriseHomeHotLiveListResponse homeHotLiveListResponse) {
                if (homeHotLiveListResponse.isSuccess() && homeHotLiveListResponse.data.list != null) {
                    mView.findViewById(R.id.emptyLayout_order).setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    if (mSelfLiveList.size() <= 0) {
                        mSelfLiveList.add(new MultiAdapterBean(MultiAdapterBean.CellType.MULTI_CELL,MultiAdapterBean.ResouseType.ROAD_OR_HOT_LIVE_LIST)
                                .setRoadOrHotLiveDataList(homeHotLiveListResponse.data.list));
                    } else {
                        if (mPage == 1) {
                            mSelfLiveList.get(0).roadOrHotLiveDataList.clear();
                        }
                        mSelfLiveList.get(0).roadOrHotLiveDataList.addAll(homeHotLiveListResponse.data.list);
                    }
                    mRecyclerView.refreshLoadMoreView(mPage, homeHotLiveListResponse.data.list.size());
                    mAdapter.updateData(mSelfLiveList);
                } else {
                    Toasty.normal(getContext(), homeHotLiveListResponse.msg).show();
                }
            }
        });
    }

}
