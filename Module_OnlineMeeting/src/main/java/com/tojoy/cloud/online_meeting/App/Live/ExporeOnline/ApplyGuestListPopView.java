package com.tojoy.cloud.online_meeting.App.Live.ExporeOnline;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.tojoy.tjoybaselib.model.live.ApplyGuestInfo;
import com.tojoy.tjoybaselib.model.live.ApplyGuestListResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.ui.dialog.PickerViewAnimateUtil;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.tojoy.cloud.online_meeting.R;

import java.util.ArrayList;

import rx.Observer;

/**
 * 申请同台的用户列表
 */
public class ApplyGuestListPopView {

    private Context context;
    private ViewGroup contentContainer;
    private Animation outAnim;
    private Animation inAnim;
    private boolean isAnim = true;
    private boolean isShowing;
    private int gravity = Gravity.BOTTOM;
    private ViewGroup decorView;
    private ViewGroup rootView;
    private boolean dismissing;
    private OperationCallback mOperationCallback;

    private TJRecyclerView mListView;
    private ArrayList<ApplyGuestInfo> mDatas = new ArrayList<>();
    private ApplyGuestAdapter mAdapter;
    private int mPage = 1;
    private boolean isLastPage = true;

    ApplyGuestListPopView(Context context, OperationCallback operationCallback) {
        this.context = context;
        this.mOperationCallback = operationCallback;
        init();
        initViews();
    }

    protected void init() {
        inAnim = getInAnimation();
        outAnim = getOutAnimation();
    }

    private void initViews() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        //decorView是activity的根View
        if (decorView == null) {
            decorView =((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        }

        //将控件添加到decorView中
        rootView = (ViewGroup) layoutInflater.inflate(R.layout.view_guest_applylist_layout, decorView, false);
        rootView.setLayoutParams(new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        ));
        rootView.setBackgroundColor(context.getResources().getColor(R.color.colorTranslucent));

        contentContainer = rootView.findViewById(R.id.content_container);
        rootView.findViewById(R.id.iv_close).setOnClickListener(v -> dismiss());

        int height = (int) (AppUtils.getWidth(context) * 800.0f / 760.0f);
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height);
        rootView.findViewById(R.id.container_base).setLayoutParams(lps);
        setKeyBackCancelable(true);

        mListView = rootView.findViewById(R.id.apply_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        mListView.getRecyclerView().setLayoutManager(linearLayoutManager);
        mListView.setLoadMoreView();
        mListView.goneFooterView();
        mAdapter = new ApplyGuestAdapter(context, this.mDatas);
        mListView.setAdapter(mAdapter);

        mListView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                mDatas.clear();
                requestApplyExplorerList();
            }

            @Override
            public void onLoadMore() {
                if (!isLastPage && mDatas != null && mDatas.size() >= mPage * AppConfig.PER_PAGE_COUNT) {
                    mPage++;
                    requestApplyExplorerList();
                }
            }
        });
    }

    public void setmOperationCallback(ApplyOperationCallback callback) {
        mAdapter.setmApplyOperationCallback(callback);
    }

    /**
     * 进行操作后刷新一下接口数据
     */
    void refreshApply() {
        mPage = 1;
        mDatas.clear();
        if (mAdapter == null) {
            mAdapter = new ApplyGuestAdapter(context, this.mDatas);
        }
        requestApplyExplorerList();
    }

    void refreshItemStatus(int index, int status) {
        mDatas.get(index).oprationStatus = status;
        mAdapter.notifyItemChanged(index);
    }

    /**
     * 获取真实的请求同台人员列表
     */
    private void requestApplyExplorerList() {
        OMAppApiProvider.getInstance().mApplyExplorerList(LiveRoomInfoProvider.getInstance().liveId, String.valueOf(mPage), String.valueOf(AppConfig.PER_PAGE_COUNT),
                new Observer<ApplyGuestListResponse>() {
                    @Override
                    public void onCompleted() {
                        mListView.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mListView.setRefreshing(false);
                    }

                    @Override
                    public void onNext(ApplyGuestListResponse baseResponse) {
                        if (baseResponse.isSuccess()) {
                            isLastPage = baseResponse.data.isLastPage;
                            mDatas.addAll(baseResponse.data.list);
                            mAdapter.updateData(mDatas);
                            if (mDatas.isEmpty()) {
                                dismiss();
                            }
                        }

                    }
                });
    }

    /**
     * @param v      (是通过哪个View弹出的)
     * @param isAnim 是否显示动画效果
     */
    public void show(View v, boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(View v) {
        show();
    }

    public void show() {
        if (isShowing()) {
            return;
        }
        isShowing = true;
        onAttached(rootView);
        rootView.requestFocus();
        mPage = 1;
        mDatas.clear();
        if (mAdapter == null) {
            mAdapter = new ApplyGuestAdapter(context, this.mDatas);
        }
        mListView.autoRefresh();
    }

    private void onAttached(View view) {
        decorView.addView(view);
        if (isAnim) {
            contentContainer.startAnimation(inAnim);
        }
    }

    public boolean isShowing() {
        return rootView.getParent() != null || isShowing;
    }


    public void dismiss() {
        if (dismissing) {
            return;
        }
        if (isAnim) {
            //消失动画
            outAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    dismissImmediately();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            contentContainer.startAnimation(outAnim);
        } else {
            dismissImmediately();
        }
        dismissing = true;
    }

    private void dismissImmediately() {
        decorView.post(() -> {
            //从根视图移除
            decorView.removeView(rootView);
            isShowing = false;
            dismissing = false;
            mOperationCallback.showBottomView(true);
        });

    }

    private Animation getInAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, true);
        return AnimationUtils.loadAnimation(context, res);
    }

    private Animation getOutAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, false);
        return AnimationUtils.loadAnimation(context, res);
    }

    private void setKeyBackCancelable(boolean isCancelable) {
        ViewGroup view;
        view = rootView;
        view.setFocusable(isCancelable);
        view.setFocusableInTouchMode(isCancelable);
        if (isCancelable) {
            view.setOnKeyListener(onKeyBackListener);
        } else {
            view.setOnKeyListener(null);
        }
    }

    private View.OnKeyListener onKeyBackListener = (v, keyCode, event) -> {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == MotionEvent.ACTION_DOWN
                && isShowing()) {
            dismiss();
            mOperationCallback.showBottomView(true);
            return true;
        }
        return false;
    };

    /**
     * Called when the user touch on black overlay in order to dismiss the dialog
     */
    @SuppressLint("ClickableViewAccessibility")
    private final View.OnTouchListener onCancelableTouchListener = (v, event) -> {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            dismiss();
        }
        return false;
    };
}
