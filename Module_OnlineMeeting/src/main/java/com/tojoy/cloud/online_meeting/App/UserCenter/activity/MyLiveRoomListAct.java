package com.tojoy.cloud.online_meeting.App.UserCenter.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.tojoy.tjoybaselib.model.live.MyLiveRoomInfo;
import com.tojoy.tjoybaselib.model.live.MyLiveRoomListResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.net.NetWorkUtils;
import com.tojoy.tjoybaselib.util.sys.StatusbarUtils;
import com.tojoy.cloud.online_meeting.App.UserCenter.adapter.MyLiveRoomListAdapter;
import com.tojoy.cloud.online_meeting.R;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 我的会议和观看记录
 */

public class MyLiveRoomListAct extends UI {

    private int page = 1;
    private TJRecyclerView mRecyclerView;
    private ArrayList<MyLiveRoomListResponse.DataObjBean.ListBean> mDatas = new ArrayList<>();
    private MyLiveRoomListAdapter mAdapter;
    private String mRoomId;
    private static boolean mIsMyMeeting = true;

    public static void start(Context context, boolean isMyMeeting) {
        Intent intent = new Intent();
        intent.setClass(context, MyLiveRoomListAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mIsMyMeeting = isMyMeeting;
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_live_room_list);
        setStatusBar();
        initTitle();
        StatusbarUtils.setStatusBarDarkMode(this, StatusbarUtils.statusbarlightmode(this));
        initUI();
        mRecyclerView.autoRefresh();
    }

    private void initTitle() {
        setUpNavigationBar();
        showBack();
        if (mIsMyMeeting) {
            setTitle("我的会议");
        } else {
            setTitle("参会记录");
        }
    }

    private void initUI() {
        mRecyclerView = (TJRecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.getRecyclerView().setVerticalScrollBarEnabled(false);
        mRecyclerView.setLoadMoreViewWithHide();
        mRecyclerView.setEmptyImgAndTip(R.drawable.icon_empty_online_logo, mIsMyMeeting ? "您还没有发起过会议" : "暂无参会记录");
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                firstInitData();
            }

            @Override
            public void onLoadMore() {
                if (mDatas.isEmpty()) {
                    return;
                }

                page++;
                if (mIsMyMeeting) {
                    initData();
                } else {
                    getLookLogInfo();
                }

            }
        });

        mAdapter = new MyLiveRoomListAdapter(this, mDatas, mIsMyMeeting);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setmOnItemDelete((position, liveId) -> {
            showProgressHUD(MyLiveRoomListAct.this, "加载中");
            if (mIsMyMeeting) {
                //我的会议
                OMAppApiProvider.getInstance().deleteMyLive(liveId, new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(MyLiveRoomListAct.this, "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        if (response.isSuccess()) {
                            mDatas.remove(position);
                            mAdapter.updateData(mDatas);
                            if (mDatas.isEmpty()) {
                                firstInitData();
                            }
                        } else {
                            Toasty.normal(MyLiveRoomListAct.this, response.msg).show();
                        }
                    }
                });
            } else {
                //参会记录
                OMAppApiProvider.getInstance().removeViewRecord(liveId, new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(MyLiveRoomListAct.this, "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        if (response.isSuccess()) {
                            mDatas.remove(position);
                            mAdapter.updateData(mDatas);
                            if (mDatas.isEmpty()) {
                                firstInitData();
                            }
                        } else {
                            Toasty.normal(MyLiveRoomListAct.this, response.msg).show();
                        }
                    }
                });
            }
        });
    }

    private void firstInitData() {
        page = 1;
        if (mIsMyMeeting) {
            getRoomInfo();
        } else {
            getLookLogInfo();
        }
    }

    /**
     * 先获取房间id 和 直播次数
     */
    private void getRoomInfo() {
        OMAppApiProvider.getInstance().getMyRoomInfo(new Observer<MyLiveRoomInfo>() {
            @Override
            public void onCompleted() {
                mRecyclerView.setRefreshing(false);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(MyLiveRoomInfo response) {
                if (response.isSuccess()) {
                    LiveRoomInfoProvider.getInstance().imCreatorid = response.data.userId;
                    mRoomId = response.data.roomId;
                    LiveRoomInfoProvider.getInstance().roomId = mRoomId;
                    if (!TextUtils.isEmpty(mRoomId)) {
                        initData();
                    }
                } else {
                    mRecyclerView.showFooterView();
                    mRecyclerView.refreshLoadMoreView(1, 0);
                }
            }
        });
    }

    /**
     * 观看记录列表
     */
    private void getLookLogInfo() {
        if (!NetWorkUtils.isNetworkConnected(this)) {
            dismissProgressHUD();
            return;
        }

        OMAppApiProvider.getInstance().mGetViewRecordList(page + "", "10", "", new Observer<MyLiveRoomListResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                mRecyclerView.refreshLoadMoreView(1, 0);
            }

            @Override
            public void onNext(MyLiveRoomListResponse response) {

                if (response.isSuccess()) {
                    List<MyLiveRoomListResponse.DataObjBean.ListBean> list = response.data.list;
                    if (page == 1) {
                        mDatas.clear();
                        mDatas.addAll(list);
                        mAdapter.updateData(mDatas);
                    } else {
                        mDatas.addAll(list);
                        mAdapter.addMore(list);
                    }
                    mRecyclerView.showFooterView();
                    mRecyclerView.refreshLoadMoreView(page, list.size());
                } else {
                    Toasty.normal(MyLiveRoomListAct.this, response.msg).show();
                }
            }
        });
    }

    /**
     * 获取我的房间列表数据
     */
    private void initData() {
        if (TextUtils.isEmpty(mRoomId)) {
            return;
        }
        OMAppApiProvider.getInstance().getMyLiveRoomList(mRoomId, page + "", "10", "", new Observer<MyLiveRoomListResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {
                mRecyclerView.refreshLoadMoreView(1, 0);
            }

            @Override
            public void onNext(MyLiveRoomListResponse response) {

                if (response.isSuccess()) {
                    List<MyLiveRoomListResponse.DataObjBean.ListBean> list = response.data.list;
                    if (page == 1) {
                        mDatas.clear();
                        mDatas.addAll(list);
                        mAdapter.updateData(mDatas);
                    } else {
                        mDatas.addAll(list);
                        mAdapter.addMore(list);
                    }
                    mRecyclerView.showFooterView();
                    mRecyclerView.refreshLoadMoreView(page, list.size());
                } else {
                    Toasty.normal(MyLiveRoomListAct.this, response.msg).show();
                }
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (mDatas.isEmpty()) {
            initData();
        }

    }

}