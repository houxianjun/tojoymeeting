package com.tojoy.cloud.online_meeting.App.Live.Live.Helper;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.tojoy.tjoybaselib.services.log.BaseLiveLog;
import com.tojoy.cloud.online_meeting.App.Live.Live.Model.PhoneStateEvent;

import org.greenrobot.eventbus.EventBus;

public class MyPhoneStateListener extends PhoneStateListener {

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        super.onCallStateChanged(state, incomingNumber);
        switch (state) {
            case TelephonyManager.CALL_STATE_RINGING:
                // 响铃中
                Log.i(BaseLiveLog.BaseLiveTag, "CALL_STATE_RINGING:" + incomingNumber);
                EventBus.getDefault().post(new PhoneStateEvent("CALL_STATE_RINGING"));
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                // 接听：自己呼出去电话也是这个状态
                Log.i(BaseLiveLog.BaseLiveTag, "CALL_STATE_OFFHOOK:" + incomingNumber);
                EventBus.getDefault().post(new PhoneStateEvent("CALL_STATE_OFFHOOK"));
                break;
            case TelephonyManager.CALL_STATE_IDLE:
                // 挂断
                Log.i(BaseLiveLog.BaseLiveTag, "CALL_STATE_IDLE:" + incomingNumber);
                EventBus.getDefault().post(new PhoneStateEvent("CALL_STATE_IDLE"));
                break;
            default:
                break;
        }
    }
}
