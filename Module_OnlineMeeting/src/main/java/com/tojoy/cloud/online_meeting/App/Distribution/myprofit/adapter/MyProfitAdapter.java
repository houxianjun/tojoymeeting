package com.tojoy.cloud.online_meeting.App.Distribution.myprofit.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.model.home.MineProfitListDistributionResponse;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;

import java.util.List;

/**
 * 收益结算列表类：分为会议，商品类型
 *
 * @author houxainjun
 */
public class MyProfitAdapter extends BaseRecyclerViewAdapter<MineProfitListDistributionResponse.MyProfitRecordsModel> {
    Context mcontext;
    /**
     * 列表显示类型 "meeting" 会议，"shop"商品
     */
    String distributionType = "meeting";

    public String getDistributionType() {
        return distributionType;
    }

    public void setDistributionType(String distributionType) {
        this.distributionType = distributionType;
    }

    public MyProfitAdapter(@NonNull Context context, @NonNull List<MineProfitListDistributionResponse.MyProfitRecordsModel> datas) {
        super(context, datas);
        this.mcontext = context;
    }

    @Override
    protected int getViewType(int position, @NonNull MineProfitListDistributionResponse.MyProfitRecordsModel data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_distribution_myprofit;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        MyProfitVh myProfitVh = new MyProfitVh(this.mcontext, parent, getLayoutResId(viewType));
        myProfitVh.setType(getDistributionType());
        return myProfitVh;
    }

    private static class MyProfitVh extends BaseRecyclerViewHolder<MineProfitListDistributionResponse.MyProfitRecordsModel> {
        private TextView tvRoomid;
        private TextView tvPeopleName;
        private TextView tvUpdateDate;
        private TextView tvMoney;
        private TextView tvProfitStatus;
        private Context context;
        String mtype;

        public void setType(String distributionType) {
            this.mtype = distributionType;
        }

        public MyProfitVh(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            this.context = context;
            bindViews();
        }

        private void bindViews() {
            tvRoomid = itemView.findViewById(R.id.tv_profit_order_number);
            tvPeopleName = itemView.findViewById(R.id.tv_profit_order_people);
            tvUpdateDate = itemView.findViewById(R.id.tv_profit_order_date);
            tvMoney = itemView.findViewById(R.id.tv_order_profit_total);
            tvProfitStatus = itemView.findViewById(R.id.tv_profit_status);

        }

        @Override
        public void onBindData(MineProfitListDistributionResponse.MyProfitRecordsModel data, int position) {
            boolean isMeeting = mtype != null && "meeting".equals(mtype);
            //会议类型列表
            if (isMeeting) {
                tvRoomid.setText("会议ID:" + data.roomCode);
                tvPeopleName.setText("参会会员：" + data.trueName);
                if (data.status == 1) {
                    tvUpdateDate.setTextColor(this.context.getResources().getColor(R.color.color_353f5d));
                    tvUpdateDate.setText("结算时间：" + data.updateDate);
                    tvProfitStatus.setText("已结算");
                    tvProfitStatus.setBackground(this.context.getResources().getDrawable(R.drawable.bg_distribution_profit_settled));

                } else {
                    tvUpdateDate.setTextColor(this.context.getResources().getColor(R.color.color_f5A623));
                    tvUpdateDate.setText(data.withdrawalTips);
                    tvProfitStatus.setText("待结算");
                    tvProfitStatus.setBackground(this.context.getResources().getDrawable(R.drawable.bg_distribution_profit_unsettled));

                }

                tvMoney.setText(data.commissionStr + "元");
            } else {
                tvRoomid.setText("商品编码:" + data.code);
                tvPeopleName.setText("商品名称：" + data.name);
                if (data.status == 1) {
                    tvUpdateDate.setTextColor(this.context.getResources().getColor(R.color.color_353f5d));
                    tvUpdateDate.setText("结算时间：" + data.updateDate);
                    tvProfitStatus.setText("已结算");
                    tvProfitStatus.setBackground(this.context.getResources().getDrawable(R.drawable.bg_distribution_profit_settled));
                } else {
                    tvUpdateDate.setTextColor(this.context.getResources().getColor(R.color.color_f5A623));
                    tvUpdateDate.setText(data.withdrawalTips);
                    tvProfitStatus.setText("待结算");
                    tvProfitStatus.setBackground(this.context.getResources().getDrawable(R.drawable.bg_distribution_profit_unsettled));
                }

                tvMoney.setText(data.commissionStr + "元");
            }

        }
    }

}
