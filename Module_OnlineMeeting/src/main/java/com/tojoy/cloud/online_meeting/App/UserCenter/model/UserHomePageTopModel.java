package com.tojoy.cloud.online_meeting.App.UserCenter.model;

import com.tojoy.tjoybaselib.model.live.EndLiveInfoResponse;

/**
 * @author qll
 * @date 2019/4/28
 */
public class UserHomePageTopModel {
    public String hostPic;
    public String roomId;
    public String userName;
    public String duration;
    public String roomCode;

    public EndLiveInfoResponse.EndLiveInfoModel endLiveInfoModel;
}
