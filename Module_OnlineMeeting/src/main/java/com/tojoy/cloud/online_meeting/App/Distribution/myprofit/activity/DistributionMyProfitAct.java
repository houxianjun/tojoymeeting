package com.tojoy.cloud.online_meeting.App.Distribution.myprofit.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.ActivityUtils;
import com.tojoy.tjoybaselib.util.sys.StatusbarUtils;
import com.tojoy.cloud.online_meeting.App.Distribution.myprofit.fragment.DistributionMyProfitFragment;
import com.tojoy.cloud.online_meeting.R;

/**
 * 分销客 我的收益
 *
 * @author houxianjun
 * @Date 2020/07/16
 */
@Route(path = RouterPathProvider.Distribution_MyProfitAct)
public class DistributionMyProfitAct extends UI {
    private RelativeLayout mTitleLayoutRlv;
    DistributionMyProfitFragment distributionMyProfitFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distribution_myprofit);
        ARouter.getInstance().inject(this);
        StatusbarUtils.enableTranslucentStatusbar(this);
        StatusbarUtils.setStatusBarLightMode(this, StatusbarUtils.statusbarlightmode(this));
        setSwipeBackEnable(false);
        initTitle();
        distributionMyProfitFragment = DistributionMyProfitFragment.newInstance(this);
        ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), distributionMyProfitFragment, R.id.fragmentContainer);
    }

    private void initTitle() {
        setUpNavigationBar();
        setTitle("我的收益");
        showBack();
        setAlphaBackground();
        mTitleLayoutRlv = getmTitleContainer();
        //标题右侧提现明细文本
        setRightManagerTitleColor(this.getResources().getColor(R.color.color_ffffff));
        setRightManagerTitle("提现明细");

    }
    @Override
    protected void onRightManagerTitleClick (){
        if (!FastClickAvoidUtil.isDoubleClick()) {
            //跳转至提现明细页面
            ARouter.getInstance().build(RouterPathProvider.Distribution_WithdrawalRecord_Act)
                    .navigation();
        }

    }
    @Override
    protected void onLeftManagerImageClick() {
        this.finish();
    }

    /**
     * 修改标题栏透明度
     *
     * @param alpha
     */
    public void setTitleAlpha(float alpha) {
        //完全不透明，隐藏标题栏
        if (alpha == 0) {
            mTitleLayoutRlv.setVisibility(View.GONE);
        } else {
            mTitleLayoutRlv.setVisibility(View.VISIBLE);
        }
        mTitleLayoutRlv.setAlpha(alpha);
    }

}
