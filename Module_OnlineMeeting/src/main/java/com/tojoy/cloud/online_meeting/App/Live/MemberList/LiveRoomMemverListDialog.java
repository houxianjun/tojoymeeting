package com.tojoy.cloud.online_meeting.App.Live.MemberList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.user.MemberListResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.TJTablayout.SlidingTabLayout;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.ui.editView.ClearEmojInputFilter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.KeyboardControlUtil;
import com.netease.nim.uikit.business.liveroom.FileShowCallback;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.tojoy.cloud.online_meeting.App.UserCenter.adapter.LiveRoomMemberListAdapter;
import com.tojoy.cloud.online_meeting.R;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * Created by luuzhu on 2019/4/20.
 * 直播间观众列表/主播端已入会未入会列表弹窗
 */

@SuppressLint("ValidFragment")
public class LiveRoomMemverListDialog extends DialogFragment implements FileShowCallback {

    private UI mContext;
    private View mView;
    public TJRecyclerView mRecyclerView;
    private View mSearchLayout;
    private View mRlTabs;
    private View mRlTitle;
    private View mEmptyLayout;
    private View mRlGuestTitle;
    private SlidingTabLayout mTabLayout;
    private ViewPager mViewPager;
    private View tvAdd;
    private View tvRequest;
    private View btnLayout;
    private TextView tvMemberCount;

    private String[] mTitles = new String[2];
    private ArrayList<Fragment> mFragments = new ArrayList<>();

    private List<MemberListResponse.DataObjBean.ListBean> mDatas = new ArrayList<>();
    private LiveRoomMemberListAdapter mAdapter;
    public int page = 1;
    public int ownerPage = 1;

    public SecurityEditText etSearch;
    private ImageView ivClear;
    private TextView tvCancle;
    private OperationCallback mOperationCallback;
    private String mSearchKey;
    private DialogInterface.OnDismissListener dismissListener;
    private boolean initOwnerFlag;
    private LiveMemberFragment fragment1;
    private LiveMemberFragment fragment2;
    private FragmentPagerAdapter mViewPagerAdapter;

    public LiveRoomMemverListDialog(Context context, OperationCallback operationCallback) {
        mContext = (UI) context;
        this.mOperationCallback = operationCallback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NORMAL, R.style.LiveDialogMemberList);
        setCancelable(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_fragment_live_member, null);
        initView(mView);
        return mView;
    }

    @Override
    public void onResume() {

        Window win = getDialog().getWindow();
        WindowManager.LayoutParams lp = win.getAttributes();
        win.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.windowAnimations = R.style.BottomInAndOutStyle;
        lp.gravity = Gravity.BOTTOM;
        win.setAttributes(lp);
        getDialog().setCanceledOnTouchOutside(true);

        super.onResume();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView(View view) {

        mRlGuestTitle = mView.findViewById(R.id.rlGuestTitle);
        mView.findViewById(R.id.ivCloseMemberGuest).setOnClickListener(view15 -> dismiss());

        etSearch = mView.findViewById(R.id.etSearch);
        ivClear = mView.findViewById(R.id.ivClear);
        tvCancle = mView.findViewById(R.id.tvCancle);
        mSearchLayout = mView.findViewById(R.id.searchLayout);
        mEmptyLayout = mView.findViewById(R.id.emptyLayout);
        tvMemberCount = mView.findViewById(R.id.tvMemberCount);

        initRecyclerView();

        layoutSearchLayout();

        if (!LiveRoomInfoProvider.getInstance().isGuest() && !LiveRoomInfoProvider.getInstance().isOfficalLive()) {
            // 2020.06.20 ：官方大会也不区分主播、助手都不显示邀请参会人
            initOwner();
            mSearchLayout.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
            mRlGuestTitle.setVisibility(View.GONE);
            tvCancle.setVisibility(View.VISIBLE);
            mRlTabs.setVisibility(View.VISIBLE);
            mRlTitle.setVisibility(View.VISIBLE);
            btnLayout.setVisibility(View.VISIBLE);

            mEmptyLayout.setVisibility(View.GONE);
        } else {
            getData();
        }

        InputFilter[] emojiFilters = {new ClearEmojInputFilter()};
        etSearch.setFilters(emojiFilters);

        ivClear.setOnClickListener(view1 -> {
            etSearch.setText("");
            // 测试提bug：点击清除后需清除页面已搜素出来的数据
            mDatas.clear();
            mAdapter.updateData(mDatas);
            page = 1;
            mEmptyLayout.setVisibility(View.VISIBLE);
        });

        tvCancle.setOnClickListener(view12 -> {
            KeyboardControlUtil.closeKeyboard(etSearch, mContext);
            etSearch.clearFocus();

            if (LiveRoomInfoProvider.getInstance().isGuest() || LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                tvCancle.setVisibility(View.GONE);
                page = 1;
                getData();
            } else {
                mSearchLayout.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.GONE);
                mRlTabs.setVisibility(View.VISIBLE);
                mRlTitle.setVisibility(View.VISIBLE);
                mEmptyLayout.setVisibility(View.GONE);
            }
            etSearch.setText("");

        });

        etSearch.setDelKeyEventListener(() -> {
            String s = etSearch.getText().toString();
            if (!TextUtils.isEmpty(s)) {
                etSearch.setText(s.substring(0, s.length() - 1));
                etSearch.setSelection(etSearch.getText().toString().length());
            }
        });

        //软键盘出来前先降低高度，防止把这个窗顶的太高
        etSearch.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {

                if (!BaseLiveAct.isKeyboardShowing) {
                    ViewGroup.LayoutParams params = mRecyclerView.getLayoutParams();
                    params.height = AppUtils.dip2px(mContext, 200);
                    mRecyclerView.setLayoutParams(params);
                    etSearch.requestFocus();
                }
            }
            return false;
        });

        etSearch.setHint("请输入姓名进行搜索");
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mSearchKey = etSearch.getText().toString();
                if(" ".equals(mSearchKey)) {
                    etSearch.setText("");
                    Toasty.normal(mContext, "请输入搜索内容").show();
                }

                if (TextUtils.isEmpty(mSearchKey)) {
                    if (ivClear.getVisibility() == View.VISIBLE) {
                        ivClear.setVisibility(View.GONE);
                    }
                } else {
                    if (ivClear.getVisibility() == View.GONE) {
                        ivClear.setVisibility(View.VISIBLE);
                    }
                    if (tvCancle.getVisibility() == View.GONE) {
                        tvCancle.setVisibility(View.VISIBLE);
                    }
//                    if (!TextUtils.isEmpty(mSearchKey.trim())) {
//
//                        if (!LiveRoomInfoProvider.getInstance().isGuest() && !LiveRoomInfoProvider.getInstance().isOfficalLive()) {
//                            ownerPage = 1;
//                            ownerSearch();
//                        } else {
//                            page = 1;
//                            doSearch();
//                        }
//                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener(){

            @Override
            public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {

                if(arg1 == EditorInfo.IME_ACTION_SEARCH)

                {
                    mSearchKey = etSearch.getText().toString();

                    if (!TextUtils.isEmpty(mSearchKey.trim())) {

                        if (!LiveRoomInfoProvider.getInstance().isGuest() && !LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                            ownerPage = 1;
                            ownerSearch();
                        } else {
                            page = 1;
                            doSearch();
                        }
                    }
                }

                return false;

            }

        });

    }

    private void layoutSearchLayout() {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mSearchLayout.getLayoutParams();
        if (LiveRoomInfoProvider.getInstance().isGuest() || LiveRoomInfoProvider.getInstance().isOfficalLive()) {
            // 2020.06.20 ：官方大会不区分主播、助手都不显示邀请参会人
            layoutParams.topMargin = AppUtils.dip2px(mContext, 55);
        } else {
            layoutParams.topMargin = AppUtils.dip2px(mContext, 42);
        }
        mSearchLayout.setLayoutParams(layoutParams);
    }

    private void initOwner() {
        initOwnerFlag = true;
        fragment1 = new LiveMemberFragment("1", mOperationCallback, this);
        fragment2 = new LiveMemberFragment("2", mOperationCallback, this);
        mFragments.add(fragment1);
        mFragments.add(fragment2);
        mRlTitle = mView.findViewById(R.id.rlv_title);
        mRlTabs = mView.findViewById(R.id.rlTabs);
        tvAdd = mView.findViewById(R.id.tvAdd);
        tvRequest = mView.findViewById(R.id.tvRequest);
        btnLayout = mView.findViewById(R.id.btnLayout);

        tvAdd.setOnClickListener(view -> {
            ARouter.getInstance().build(RouterPathProvider.MeetingPersonEnterprise)
                    .withString("max_meeting_person", LiveRoomInfoProvider.getInstance().allowViewTotal)
                    .withString("meeting_authtype", LiveRoomInfoProvider.getInstance().authType)
                    .withString("meeting_from", LiveRoomMemverListDialog.class.getName())
                    .navigation();
        });

        tvRequest.setOnClickListener(view -> {

            mContext.showProgressHUD(mContext, "加载中...");

            OMAppApiProvider.getInstance().inviteMember("", LiveRoomInfoProvider.getInstance().liveId, new Observer<OMBaseResponse>() {
                @Override
                public void onCompleted() {
                    mContext.dismissProgressHUD();
                }

                @Override
                public void onError(Throwable e) {
                    mContext.dismissProgressHUD();
                    Toasty.normal(mContext, "网络错误，请检查网络").show();
                }

                @Override
                public void onNext(OMBaseResponse response) {
                    if (response.isSuccess()) {
                        fragment2.refreshData();
                        Toasty.normal(mContext, "邀请成功").show();
                    } else {
                        Toasty.normal(mContext, response.msg).show();
                    }
                }
            });
        });

        mView.findViewById(R.id.ivSearchMemberList).setOnClickListener(view14 -> {
            mSearchLayout.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.VISIBLE);
            mRlTabs.setVisibility(View.GONE);
            mRlTitle.setVisibility(View.GONE);
            etSearch.requestFocus();

            mDatas.clear();
            mAdapter.updateData(mDatas);
        });
        mView.findViewById(R.id.ivCloseMemberList).setOnClickListener(view13 -> dismiss());
        mTabLayout = mView.findViewById(R.id.tabLayot);
        mViewPager = mView.findViewById(R.id.viewPager);
        mTabLayout.setTabSpaceEqual(true);
        mViewPager.setOffscreenPageLimit(2);

        ViewGroup.LayoutParams params = mViewPager.getLayoutParams();
        params.height = (int) (AppUtils.getHeight(mContext) * 0.55);
        mViewPager.setLayoutParams(params);

        mViewPagerAdapter = new FragmentPagerAdapter(getChildFragmentManager()) {

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                return mTitles[position];
            }

            @Override
            public int getCount() {
                return mTitles.length;
            }

            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }
        };
        mViewPager.setAdapter(mViewPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tvRequest.setVisibility(position == 0 ? View.GONE : View.VISIBLE);
                fragment1.refreshData();
                fragment2.refreshData();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mTitles[0] = "已入会";
        mTitles[1] = "未入会";
        mTabLayout.setViewPager(mViewPager, mTitles);
    }

    private void initRecyclerView() {
        mRecyclerView = mView.findViewById(R.id.memberRecycler);
        mRecyclerView.setRefreshable(false);
        mRecyclerView.setLoadMoreView();
        mRecyclerView.refreshLoadMoreView(1, 0);
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
            }

            @Override
            public void onLoadMore() {

                if (!LiveRoomInfoProvider.getInstance().isGuest() && !LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                    // 2020.06.20 ：官方大会不区分主播、助手都不显示邀请参会人
                    ownerPage++;
                    //ownerSearch();
                    ownerSearchPage();
                } else {
                    page++;
                    if (!TextUtils.isEmpty(mSearchKey)) {
                        doPageSearch();
                    } else {
                        mSearchKey="";
                        //getData();
                        doPageSearch();
                    }

                }
            }
        });

        ViewGroup.LayoutParams params = mRecyclerView.getLayoutParams();
        params.height = (int) (AppUtils.getHeight(mContext) * 0.55) + AppUtils.dip2px(mContext, 80);
        mRecyclerView.setLayoutParams(params);

        mAdapter = new LiveRoomMemberListAdapter(mContext, mDatas, mOperationCallback);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setClickHeadIvCallback(new LiveRoomMemberListAdapter.OnClickHeadIvCallback() {

            @Override
            public void onSetHelper(MemberListResponse.DataObjBean.ListBean data, boolean isSetHelper) {
                new TJMakeSureDialog(mContext, (isSetHelper ? "是否设置" : "是否切换") + data.userName + "为助手？",
                        view -> {
                            if (mContext != null) {
                                // 判断当前是否在展示文档和手绘板，正在展示提示"演示中，暂不可切换助手"
                                if (LiveRoomInfoProvider.getInstance().isShowFile || LiveRoomInfoProvider.getInstance().isShowDrawPad) {
                                    Toasty.normal(mContext, "演示中，暂不可切换助手").show();
                                    return;
                                }
                                mContext.showProgressHUD(mContext, "");
                                OMAppApiProvider.getInstance().addLiveHelper(LiveRoomInfoProvider.getInstance().liveId, LiveRoomInfoProvider.getInstance().imRoomId, data.userId, data.userName, new Observer<OMBaseResponse>() {
                                    @Override
                                    public void onCompleted() {
                                        mContext.dismissProgressHUD();
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        mContext.dismissProgressHUD();
                                        Toasty.normal(mContext, "网络异常，请检查网络").show();
                                    }

                                    @Override
                                    public void onNext(OMBaseResponse response) {
                                        if (!response.isSuccess()) {
                                            Toasty.normal(mContext, response.msg).show();
                                        } else {
                                            // 设置成功，刷新页面
                                            new Handler().postDelayed(() -> {
                                                page = 1;
                                                getData();
                                            }, 1000);
                                        }
                                    }
                                });
                            }
                        }).setBtnText("确定", "取消").show();

            }

            @Override
            public void onCancleHelper(MemberListResponse.DataObjBean.ListBean data) {
                new TJMakeSureDialog(mContext, "是否取消助手" + data.userName + "？",
                        view -> {
                            if (mContext != null) {
                                // 判断当前是否在展示文档和手绘板，正在展示提示"演示中，暂不可切换助手"
                                if (LiveRoomInfoProvider.getInstance().isShowFile || LiveRoomInfoProvider.getInstance().isShowDrawPad) {
                                    Toasty.normal(mContext, "演示中，暂不可取消助手").show();
                                    return;
                                }
                                mContext.showProgressHUD(mContext, "");
                                OMAppApiProvider.getInstance().cancleLiveHelper(LiveRoomInfoProvider.getInstance().liveId, LiveRoomInfoProvider.getInstance().imRoomId, data.userId, data.userName, new Observer<OMBaseResponse>() {
                                    @Override
                                    public void onCompleted() {
                                        mContext.dismissProgressHUD();
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        mContext.dismissProgressHUD();
                                        Toasty.normal(mContext, "网络异常，请检查网络").show();
                                    }

                                    @Override
                                    public void onNext(OMBaseResponse response) {
                                        if (!response.isSuccess()) {
                                            Toasty.normal(mContext, response.msg).show();
                                        } else {
                                            // 取消成功，刷新页面
                                            new Handler().postDelayed(() -> {
                                                page = 1;
                                                getData();
                                            }, 1000);
                                        }
                                    }
                                });
                            }
                        }).setBtnText("确定", "取消").show();
            }
        });
    }

    public void changeMemberType() {

        if (LiveRoomInfoProvider.getInstance().isGuest() || LiveRoomInfoProvider.getInstance().isOfficalLive()) {

            page = 1;
            getData();

            mSearchLayout.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.VISIBLE);
            mRlGuestTitle.setVisibility(View.VISIBLE);
            tvCancle.setVisibility(View.GONE);
            mRlTabs.setVisibility(View.GONE);
            mRlTitle.setVisibility(View.GONE);
            btnLayout.setVisibility(View.GONE);
            mEmptyLayout.setVisibility(View.GONE);

        } else {

            if (!initOwnerFlag) {
                initOwner();
            } else {
                fragment1.refreshData();
                fragment2.refreshData();
            }
            mSearchLayout.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
            mRlGuestTitle.setVisibility(View.GONE);
            tvCancle.setVisibility(View.VISIBLE);
            mRlTabs.setVisibility(View.VISIBLE);
            mRlTitle.setVisibility(View.VISIBLE);
            btnLayout.setVisibility(View.VISIBLE);
            mEmptyLayout.setVisibility(View.GONE);
            mDatas.clear();
            mAdapter.updateData(mDatas);
        }
        layoutSearchLayout();
    }

    private void getData() {

        OMAppApiProvider.getInstance().getMemberList(LiveRoomInfoProvider.getInstance().imRoomId, LiveRoomInfoProvider.getInstance().liveId,
                page + "", "20", new Observer<MemberListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(mContext, "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(MemberListResponse response) {
                        if (response.isSuccess()) {

                            if (page == 1) {
                                mDatas.clear();
                                mDatas = response.data.list;
                                tvMemberCount.setText("当前在线人数：" + response.data.total);
                                if(!"3".equals(LiveRoomInfoProvider.getInstance().authType)) {
                                    tvMemberCount.setVisibility(View.VISIBLE);
                                }
                            } else {
                                mDatas.addAll(response.data.list);
                            }
                            mAdapter.setHaveHelper(response.data.hasNextPage);
                            mAdapter.updateData(mDatas);
                            mRecyclerView.refreshLoadMoreView(page, response.data.list.size(), 20);

                            if (!TextUtils.isEmpty(mSearchKey) && mDatas.isEmpty()) {
                                mEmptyLayout.setVisibility(View.VISIBLE);
                            } else {
                                mEmptyLayout.setVisibility(View.GONE);
                            }

                        } else {
                            Toasty.normal(mContext, response.msg).show();
                        }
                    }
                });
    }

    /**
     * 观众端搜索人脉
     */
    private void doSearch() {

        OMAppApiProvider.getInstance().searchMemberList("0", mSearchKey, LiveRoomInfoProvider.getInstance().imRoomId,
                LiveRoomInfoProvider.getInstance().liveId, page + "", "20", new Observer<MemberListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(mContext, "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(MemberListResponse response) {
                        if (response.isSuccess()) {

                            if (page == 1) {
                                mDatas.clear();
                                mDatas = response.data.list;
                            } else {
                                mDatas.addAll(response.data.list);
                            }
                            mAdapter.setHaveHelper(response.data.hasNextPage);
                            mAdapter.updateData(mDatas);
                            mRecyclerView.refreshLoadMoreView(page, response.data.list.size(), 20);

                            if (!TextUtils.isEmpty(mSearchKey) && mDatas.isEmpty()) {
                                mEmptyLayout.setVisibility(View.VISIBLE);
                            } else {
                                mEmptyLayout.setVisibility(View.GONE);
                            }

                        } else {
                            Toasty.normal(mContext, response.msg).show();
                        }
                    }
                });
    }

    /**
     * 观众端分页搜索人脉
     */
    private void doPageSearch(){
        OMAppApiProvider.getInstance().searchPageMemberList("0", mSearchKey, LiveRoomInfoProvider.getInstance().imRoomId,
                LiveRoomInfoProvider.getInstance().liveId, page + "", "20", new Observer<MemberListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(mContext, "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(MemberListResponse response) {
                        if (response.isSuccess()) {

                            if (page == 1) {
                                mDatas.clear();
                                mDatas = response.data.list;
                            } else {
                                mDatas.addAll(response.data.list);
                            }
                            mAdapter.setHaveHelper(response.data.hasNextPage);
                            mAdapter.updateData(mDatas);
                            mRecyclerView.refreshLoadMoreView(page, response.data.list.size(), 20);

                            if (!TextUtils.isEmpty(mSearchKey) && mDatas.isEmpty()) {
                                mEmptyLayout.setVisibility(View.VISIBLE);
                            } else {
                                mEmptyLayout.setVisibility(View.GONE);
                            }

                        } else {
                            Toasty.normal(mContext, response.msg).show();
                        }
                    }
                });

    }

    /**
     * 主播/助手搜索人脉
     */
    private void ownerSearch() {

        OMAppApiProvider.getInstance().searchMember(mSearchKey, LiveRoomInfoProvider.getInstance().liveId,
                ownerPage + "", "20", new Observer<MemberListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(mContext, "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(MemberListResponse response) {
                        if (response.isSuccess()) {

                            if (ownerPage == 1) {
                                mDatas.clear();
                                mDatas = response.data.list;
                            } else {
                                mDatas.addAll(response.data.list);
                            }
                            mAdapter.setHaveHelper(response.data.hasNextPage);
                            mAdapter.updateData(mDatas);
                            mRecyclerView.refreshLoadMoreView(ownerPage, response.data.list.size(), 20);

                            if (!TextUtils.isEmpty(mSearchKey) && mDatas.isEmpty()) {
                                mEmptyLayout.setVisibility(View.VISIBLE);
                            } else {
                                mEmptyLayout.setVisibility(View.GONE);
                            }

                        } else {
                            Toasty.normal(mContext, response.msg).show();
                        }
                    }
                });
    }

    /**
     * 1.4.3 新增主播/助手分页搜索
     */
    private void ownerSearchPage(){
        OMAppApiProvider.getInstance().searchMemberPage(mSearchKey, LiveRoomInfoProvider.getInstance().liveId,
                ownerPage + "", "20", new Observer<MemberListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(mContext, "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(MemberListResponse response) {
                        if (response.isSuccess()) {

                            if (ownerPage == 1) {
                                mDatas.clear();
                                mDatas = response.data.list;
                            } else {
                                mDatas.addAll(response.data.list);
                            }
                            mAdapter.setHaveHelper(response.data.hasNextPage);
                            mAdapter.updateData(mDatas);
                            mRecyclerView.refreshLoadMoreView(ownerPage, response.data.list.size(), 20);

                            if (!TextUtils.isEmpty(mSearchKey) && mDatas.isEmpty()) {
                                mEmptyLayout.setVisibility(View.VISIBLE);
                            } else {
                                mEmptyLayout.setVisibility(View.GONE);
                            }

                        } else {
                            Toasty.normal(mContext, response.msg).show();
                        }
                    }
                });
    }


    public void refreshInviteBtn(boolean isNone) {
        tvRequest.setBackgroundResource(isNone ? R.drawable.bg_btn_grey2 : R.drawable.bg_blue_bt3);
        tvRequest.setClickable(!isNone);
    }

    public void refreshTabTitle(int index, String title) {
        mTitles[index] = title;
        if (!TextUtils.isEmpty(mTitles[0]) && !TextUtils.isEmpty(mTitles[1])) {
            new Handler().postDelayed(() -> mTabLayout.setViewPager(mViewPager, mTitles), 300);
        }
    }

    public void refreshMemberList() {

        new Handler().postDelayed(() -> {
            if(fragment1 != null) {
                fragment1.refreshData();
            }
            if(fragment2 != null) {
                fragment2.refreshData();
            }
        }, 800);
    }


    public void setOnDismissListener(DialogInterface.OnDismissListener listener) {
        this.dismissListener = listener;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (dismissListener != null) {
            dismissListener.onDismiss(dialog);
        }
        super.onDismiss(dialog);
    }

    public boolean isShowing() {
        return isVisible();
    }

    @Override
    public void exitFileShowSuccess() {
        dismiss();
    }

}
