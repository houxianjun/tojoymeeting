package com.tojoy.cloud.online_meeting.App.Meeting.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.enterprise.BrowsedEnterpriseResponse;
import com.tojoy.tjoybaselib.model.enterprise.CorporationMsgResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.ui.recyclerview.decoration.SpacingDecoration;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.App.Meeting.adapter.EnterpriseSearchAdapter;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.App.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

public class EnterPriseSearchFragment extends BaseFragment {

    private TJRecyclerView mRecyclerView;
    private EnterpriseSearchAdapter mAdapter;
    private List<BrowsedEnterpriseResponse.BrowsedEnterpriseModel.EnterprisListBean> mDatas = new ArrayList<>();
    private int mPage = 1;
    private String mSearchKey;
    private View emptyView;
    private TextView mEmptyViewTv;


    protected Handler mHandler;

    private String pagefrom = "";

    @SuppressLint("HandlerLeak")
    private void initHandler() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
            }
        };
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_search_enterprise_layout, container, false);
        }
        isCreateView = true;
        return mView;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initUI() {
        emptyView = mView.findViewById(R.id.emptyLayout_order);
        mEmptyViewTv = mView.findViewById(R.id.tv_empty_text);
        mRecyclerView = mView.findViewById(R.id.recyclerView);
        mRecyclerView.getRecyclerView().setVerticalScrollBarEnabled(false);
        mRecyclerView.setLoadMoreView();

        mEmptyViewTv.setText("无匹配内容，换个关键词试试吧");

        mAdapter = new EnterpriseSearchAdapter(getContext(), mDatas);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.getRecyclerView().addItemDecoration(new SpacingDecoration(AppUtils.dip2px(getContext(), 8), AppUtils.dip2px(getContext(), 0), false));
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                if ("MeetingFragment".equals(pagefrom)) {
                    loadHotCommpanyData();
                } else {
                    loadData();
                }
            }

            @Override
            public void onLoadMore() {
                mPage++;
                if ("MeetingFragment".equals(pagefrom)) {
                    loadHotCommpanyData();
                } else {
                    loadData();
                }
            }
        });

        //点击进入直播间会议
        mAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                getCompanyStatus(data.companyCode);
            }
        });
    }

    /**
     * 进行搜索
     *
     * @param dynamicKey
     */
    public void doSearch(String dynamicKey, String pagefrom) {
        this.pagefrom = pagefrom;
        mPage = 1;
        mSearchKey = dynamicKey;
        mDatas.clear();
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
        if ("MeetingFragment".equals(this.pagefrom)) {
            loadHotCommpanyData();
        } else {

            loadData();
        }
    }


    private void loadData() {
        OMAppApiProvider.getInstance().browsedEnterprise(mSearchKey, mPage, new Observer<BrowsedEnterpriseResponse>() {
            @Override
            public void onCompleted() {
                mRecyclerView.setRefreshing(false);
                if (mDatas.size() == 0) {
                    emptyView.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(Throwable e) {
                mRecyclerView.setRefreshing(false);
                Toasty.normal(getContext(), "网络异常，稍后再试").show();
            }

            @Override
            public void onNext(BrowsedEnterpriseResponse response) {
                if (response.isSuccess()) {
                    if (response.data.list != null && response.data.list.size() != 0) {
                        emptyView.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        if (mPage == 1) {
                            mDatas.clear();
                        }
                        mDatas.addAll(response.data.list);
                        mAdapter.updateData(mDatas);
                        mRecyclerView.refreshLoadMoreView(mPage, response.data.list.size());
                        mRecyclerView.showFooterView();
                    }
                } else {
                    Toasty.normal(getContext(), response.msg).show();
                }

            }
        });

    }


    private void loadHotCommpanyData() {
        OMAppApiProvider.getInstance().getHotCompanyMoreList(mSearchKey, mPage + "", 20 + "", new Observer<BrowsedEnterpriseResponse>() {
            @Override
            public void onCompleted() {
                mRecyclerView.setRefreshing(false);
                if (mDatas.size() == 0) {
                    emptyView.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                } else {

                }
            }

            @Override
            public void onError(Throwable e) {
                mRecyclerView.setRefreshing(false);
                Toasty.normal(getContext(), "网络异常，稍后再试").show();
            }

            @Override
            public void onNext(BrowsedEnterpriseResponse response) {
                if (response.isSuccess()) {
                    if (response.data.records != null && response.data.records.size() != 0) {
                        emptyView.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        if (mPage == 1) {
                            mDatas.clear();
                        }
                        mDatas.addAll(response.data.records);
                        mAdapter.updateData(mDatas);
                        mRecyclerView.refreshLoadMoreView(mPage, response.data.records.size());
                        mRecyclerView.showFooterView();
                    }
                } else {
                    Toasty.normal(getContext(), response.msg).show();
                }

            }
        });

    }


    /**
     * 获取公司是否有定制企业状态
     */
    private void getCompanyStatus(String companyCode) {
        OMAppApiProvider.getInstance().mCorporationQuery(companyCode, false, new rx.Observer<CorporationMsgResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(getContext(), "网络错误，请检查网络").show();
            }

            @Override
            public void onNext(CorporationMsgResponse omBaseResponse) {
                if (omBaseResponse != null && omBaseResponse.isSuccess()) {
                    if ("1".equals(String.valueOf(omBaseResponse.data.customState)) && companyCode != null) {
                        ARouter.getInstance().build(RouterPathProvider.OnlineMeetingCustomizedEnterprisePageAct)
                                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                                .withString("companyCode", companyCode)
                                .withString("sourceWay","2")
                                .navigation();
                    } else {
                        Toasty.normal(getContext(), "抱歉，该企业未配置企业主页").show();
                    }
                } else {
                    Toasty.normal(getContext(), omBaseResponse.msg).show();
                }
            }
        });
    }
}
