package com.tojoy.cloud.online_meeting.App.Live.Live.Helper;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.netease.nim.uikit.business.chatroom.extension.LiveRoomCustomMessageAttachment;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.model.live.DoumentVideoSendMsg;
import com.tojoy.tjoybaselib.model.live.SelectOutLiveModel;
import com.netease.nim.uikit.business.liveroom.LiveCustomMessage;
import com.netease.nim.uikit.business.liveroom.LiveCustomMessageCode;
import com.tojoy.cloud.online_meeting.App.Live.Live.Model.LiveTemplateLeaveMsg;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;
import com.tojoy.common.App.TJBaseApp;
import com.tojoy.common.Services.EventBus.PublicStringEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

/**
 * 直播间里的消息
 */
public class LiveCustomMessageProvider {

    /**
     * 获取消息体
     */
    static LiveCustomMessage getCustomMessageExtra(byte[] message) {
        String json = new String(message);
        try {
            return new Gson().fromJson(json, LiveCustomMessage.class);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 发送直播间自定义消息
     */
    public static void sendLiveRoomCustomMessage(String userId, byte[] message, boolean isResend) {
        /**
         * 聊天室发送消息
         */
        LiveRoomCustomMessageAttachment attachment = new LiveRoomCustomMessageAttachment();
        attachment.setExt(new String(message));
        ChatRoomMessage customMessage = ChatRoomMessageBuilder.createChatRoomCustomMessage(LiveRoomInfoProvider.getInstance().imRoomId, attachment);
        NIMClient.getService(ChatRoomService.class).sendMessage(customMessage, false)
                .setCallback(new RequestCallback<Void>() {
                    @Override
                    public void onSuccess(Void param) {
                        LogUtil.i(LiveLogTag.LRMsgTag, "sendLiveRoomCustomMessage-onSuccess" + LiveRoomInfoProvider.getInstance().imRoomId);
                    }

                    @Override
                    public void onFailed(int code) {
                        LogUtil.i(LiveLogTag.LRMsgTag, "sendLiveRoomCustomMessage-onFailed" + LiveRoomInfoProvider.getInstance().imRoomId);
                        Toasty.normal(TJBaseApp.getInstance(), "网络异常，请稍后再试！").show();
                        if (!isResend) {
                            sendLiveRoomCustomMessage(userId, message, true);
                            EventBus.getDefault().post(new PublicStringEvent(PublicStringEvent.CHAT_ROOM_LIVE_REENTER));
                        }
                    }

                    @Override
                    public void onException(Throwable exception) {
                        Toasty.normal(TJBaseApp.getInstance(), "网络异常，请稍后再试！").show();
                        LogUtil.i(LiveLogTag.LRMsgTag, "sendLiveRoomCustomMessage-onException" + LiveRoomInfoProvider.getInstance().imRoomId);
                    }
                });

    }

    /**
     * 获取申请同台直播的消息
     */
    public static byte[] getApplyExporeData(Context context) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.type = 1;
        liveCustomMessage.msgCode = LiveCustomMessageCode.APPLY_EXPORE.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 获取同意申请同台直播的消息
     */
    public static byte[] getAgreeApplyExporeData(Context context, String fromUserId, String applyId) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.toUserId = fromUserId;
        liveCustomMessage.extroId = applyId;
        liveCustomMessage.type = 1;
        liveCustomMessage.msgCode = LiveCustomMessageCode.AGREE_EXPORE.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 获取拒绝申请同台直播的消息
     */
    public static byte[] getRefuseApplyExporeData(Context context, String fromUserId) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.toUserId = fromUserId;
        liveCustomMessage.type = 1;
        liveCustomMessage.msgCode = LiveCustomMessageCode.REFUSE_EXPORE.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 获取邀请同台直播的消息
     */
    public static byte[] getInviteApplyExporeData(Context context, String inviteUserId, String applyForId) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.toUserId = inviteUserId;
        liveCustomMessage.extroId = applyForId;
        liveCustomMessage.msgCode = LiveCustomMessageCode.INVITE_EXPORE.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 获取同意邀请同台直播的消息
     */
    public static byte[] getAgreeInviteApplyExporeData(Context context, String hostUserId) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.toUserId = hostUserId;
        liveCustomMessage.type = 1;
        liveCustomMessage.msgCode = LiveCustomMessageCode.AGREE_INVITE_EXPORE.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 获取拒绝邀请同台直播的消息
     */
    public static byte[] getRefuseInviteApplyExporeData(Context context, String hostUserId) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.toUserId = hostUserId;
        liveCustomMessage.type = 1;
        liveCustomMessage.msgCode = LiveCustomMessageCode.REFUSE_INVITE_EXPORE.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 获取关闭观众同台
     */
    public static byte[] getCloseExporeSomebodyData(Context context, String guestUserId) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.toUserId = guestUserId;
        liveCustomMessage.msgCode = LiveCustomMessageCode.CLOSE_EXPLORE.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 获取关闭观众视频
     */
    public static byte[] getCloseSomebodyVideoData(Context context, String guestUserId) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.toUserId = guestUserId;
        liveCustomMessage.type = 1;
        liveCustomMessage.msgCode = LiveCustomMessageCode.CLOSE_VIDEO.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 获取开启观众视频
     */
    public static byte[] getOpenSomebodyVideoData(Context context, String guestUserId) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.toUserId = guestUserId;
        liveCustomMessage.type = 1;
        liveCustomMessage.msgCode = LiveCustomMessageCode.OPEN_VIDEO.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 获取关闭观众语音
     */
    public static byte[] getCloseSomebodyAudioData(Context context, String guestUserId) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.toUserId = guestUserId;
        liveCustomMessage.type = 1;
        liveCustomMessage.msgCode = LiveCustomMessageCode.CLOSE_AUDIO.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 获取开启观众语音
     */
    public static byte[] getOpenSomebodyAudioData(Context context, String guestUserId) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.toUserId = guestUserId;
        liveCustomMessage.type = 1;
        liveCustomMessage.msgCode = LiveCustomMessageCode.OPEN_AUDIO.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 获取暂离的接口(暂离发送2003，2001已经舍弃)
     */
    public static byte[] getTemplateLeaveData(Context context, String tempLeaveTip, String time) {
        LiveTemplateLeaveMsg liveTemplateLeaveMsg = new LiveTemplateLeaveMsg();
        liveTemplateLeaveMsg.reason = tempLeaveTip;
        liveTemplateLeaveMsg.time = time;
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.type = 1;
        liveCustomMessage.extroId = new Gson().toJson(liveTemplateLeaveMsg);
        liveCustomMessage.msgCode = LiveCustomMessageCode.TEMP_EXIT_ROOM_NEW.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }


    /**
     * 获取再次开播的接口
     */
    public static byte[] getHostReStartData(Context context) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.type = 1;
        liveCustomMessage.msgCode = LiveCustomMessageCode.RESTART_LIVE.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 获取切换视频消息
     */
    public static byte[] getScreenChangeData(Context context, String userId, int code) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.type = 1;
        liveCustomMessage.extroId = userId;
        liveCustomMessage.msgCode = code;
        return new Gson().toJson(liveCustomMessage).getBytes();
    }


    /**
     * 获取绘画板的消息
     */
    public static byte[] getSwitchShowDrawPop(Context context, boolean isOpen) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.type = 1;
        liveCustomMessage.msgCode = isOpen ? LiveCustomMessageCode.START_DRAW_POP.getCode() : LiveCustomMessageCode.STOP_DRAW_POP.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 开关引流的消息
     */
    public static byte[] getSwitchOutlink(Context context, String outLinkUrlFlv, String outLinkUrlRtmp, String streamId) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.type = 1;
        liveCustomMessage.extroId = streamId;
        liveCustomMessage.outLinkUrlFlv = outLinkUrlFlv;
        liveCustomMessage.outLinkUrlRtmp = outLinkUrlRtmp;
        liveCustomMessage.msgCode = TextUtils.isEmpty(outLinkUrlFlv) ? LiveCustomMessageCode.STOPOUTLINK.getCode() : LiveCustomMessageCode.SHOWOUTLINK.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    public static byte[] getOpenOutlink(Context context, ArrayList<SelectOutLiveModel> outLiveModels) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.type = 1;
        liveCustomMessage.extroId = new Gson().toJson(outLiveModels);
        liveCustomMessage.msgCode = LiveCustomMessageCode.SHOWOUTLINK.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }


    public static byte[] getActivityRefreshData() {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.type = 1;
        liveCustomMessage.msgCode = LiveCustomMessageCode.REFRESH_ACTIVITY.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 获取当前文档演示白板ID变化的接口
     */
    public static byte[] getWBChangeData(Context context, String boardId) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.type = 1;
        liveCustomMessage.extroId = boardId;
        liveCustomMessage.msgCode = LiveCustomMessageCode.OPEN_DOCUMENT_WB_ID.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 通知关闭文档演示白板
     */
    public static byte[] getWBColoseData(Context context) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.type = 1;
        liveCustomMessage.msgCode = LiveCustomMessageCode.CLOSE_DOCUMENT_WB.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 获取当前文档演示视频变化的接口
     */
    public static byte[] getDocumentVideoChangeData(Context context, String videoUrl, String playTime, String status) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.type = 1;
        DoumentVideoSendMsg msg = new DoumentVideoSendMsg();
        msg.url = videoUrl;
        msg.playTime = playTime;
        msg.status = status;
        liveCustomMessage.extroId = new Gson().toJson(msg);
        liveCustomMessage.msgCode = LiveCustomMessageCode.OPEN_DOCUMENT_VIDEO.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 通知关闭文档演示视频
     */
    public static byte[] getDocumentVideoColoseData(Context context, String videoUrl, String playTime, String status) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.type = 1;
        DoumentVideoSendMsg msg = new DoumentVideoSendMsg();
        msg.url = videoUrl;
        msg.playTime = playTime;
        msg.status = status;
        liveCustomMessage.extroId = new Gson().toJson(msg);
        liveCustomMessage.msgCode = LiveCustomMessageCode.CLOSE_DOCUMENT_VIDEO.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }


    /**
     * 通知关闭文档演示视频
     */
    public static byte[] getHasReceiveOnlineTipData(Context context, String toUserId) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.type = 0;
        liveCustomMessage.toUserId = toUserId;
        liveCustomMessage.msgCode = LiveCustomMessageCode.HAS_RECEIVE_TIP.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 获取同台、直播人员接听电话
     */
    public static byte[] getCallOffHookData(Context context) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.type = 1;
        liveCustomMessage.msgCode = LiveCustomMessageCode.CALL_OFF_HOOK.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }

    /**
     * 获取同台、直播人员挂断电话
     */
    public static byte[] getCallIdelData(Context context) {
        LiveCustomMessage liveCustomMessage = new LiveCustomMessage();
        liveCustomMessage.fromUserId = BaseUserInfoCache.getUserId(context);
        liveCustomMessage.fromUserJob = BaseUserInfoCache.getJobName(context);
        liveCustomMessage.fromUserName = BaseUserInfoCache.getUserName(context);
        liveCustomMessage.fromUserHeader = BaseUserInfoCache.getUserHeaderPic(context);
        liveCustomMessage.type = 1;
        liveCustomMessage.msgCode = LiveCustomMessageCode.CALL_IDLE.getCode();
        return new Gson().toJson(liveCustomMessage).getBytes();
    }
}
