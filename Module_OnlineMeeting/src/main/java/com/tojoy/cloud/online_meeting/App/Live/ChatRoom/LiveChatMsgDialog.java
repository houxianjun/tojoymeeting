package com.tojoy.cloud.online_meeting.App.Live.ChatRoom;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.netease.nim.uikit.business.liveroom.LiveRoomHelper;
import com.tojoy.cloud.online_meeting.R;

/**
 * Created by luuzhu on 2019/4/19.
 */

public class LiveChatMsgDialog {

    private Dialog mDialog;

    private Activity mContext;
    private FrameLayout mMessageContainer;
    public TextView mTvInput;
    public FrameLayout mInputContainer;
    private View mView;

    public LiveChatMsgDialog(Context context) {
        mContext = (Activity) context;
        initUI();
    }

    @SuppressLint("WrongConstant")
    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_fragment_live, null);
        initView(mView);
    }

    public void show(){

        mDialog = new Dialog(mContext, R.style.LiveDialogFragmentEnterExitAnim);
        mDialog.setContentView(mView);

        Window win = mDialog.getWindow();
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.windowAnimations = R.style.BottomInAndOutStyle;
        lp.gravity = Gravity.BOTTOM;
        win.setAttributes(lp);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.show();
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener listener){
        mDialog.setOnDismissListener(listener);
    }

    public boolean isShowing(){
        return mDialog != null && mDialog.isShowing();
    }

    private void initView(View view) {
        mMessageContainer = view.findViewById(R.id.messageContainer);
        mTvInput = view.findViewById(R.id.tvInput);
        LiveRoomHelper.getInstance().inputMsgBtn = mTvInput;
        mInputContainer = view.findViewById(R.id.inputContainer);

        view.findViewById(R.id.ivClose).setOnClickListener(view1 -> mDialog.dismiss());
    }


    public void addChatMsgLayout(RecyclerView msgRecyclerView) {
        mMessageContainer.removeAllViews();
        mMessageContainer.addView(msgRecyclerView);
    }

    public void removeMsgRecycler() {
        mMessageContainer.removeAllViews();
    }
}
