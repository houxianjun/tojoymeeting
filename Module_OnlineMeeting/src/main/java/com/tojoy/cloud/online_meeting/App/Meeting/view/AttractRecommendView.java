package com.tojoy.cloud.online_meeting.App.Meeting.view;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.ui.tjrecyclerview.GesturesRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.cloud.online_meeting.App.Meeting.adapter.HomeAttractRecommendAdapter;
import com.tojoy.tjoybaselib.model.home.HomeModelResponse;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.Services.Router.AppRouter;

/**
 * 招商推荐板块
 */
public class AttractRecommendView extends RelativeLayout implements PlateBaseView {

    private Context mContext;
    private View mView;
    private GesturesRecyclerView mHotEnterpriseRecycleview;
    private HomeAttractRecommendAdapter adapter;
    private RelativeLayout mAttractEnterpriseMainRlv;

    private TextView mAttractBusinessTitileTv;
    private TextView mAttractRecommendDescribeTv;


    public AttractRecommendView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        if (mView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mView = aLayoutInflater.inflate(R.layout.view_home_attract_recommend, null);
        }
        initView();
        this.addView(mView);
    }

    @Override
    public void initView() {
        mHotEnterpriseRecycleview = mView.findViewById(R.id.rclv_attract_recommend);
        mAttractEnterpriseMainRlv = mView.findViewById(R.id.rl_attract_recommend_main);
        mAttractBusinessTitileTv = mView.findViewById(R.id.tv_attract_recommend_titile);
        mAttractRecommendDescribeTv = mView.findViewById(R.id.tv_attract_recommend);
    }

    @Override
    public void initData() {
    }

    @Override
    public void initData(HomeModelResponse data) {

        if (data != null && data.dataList != null && data.dataList.size() != 0) {

            mAttractEnterpriseMainRlv.setVisibility(VISIBLE);
            mAttractBusinessTitileTv.setText(data.spaceName);
            mAttractRecommendDescribeTv.setText(data.des);
            adapter = new HomeAttractRecommendAdapter(mContext, data.dataList);

            LinearLayoutManager ms = new LinearLayoutManager(mContext);

            // 设置 recyclerview 布局方式为横向布局
            ms.setOrientation(LinearLayoutManager.HORIZONTAL);

            //LinearLayoutManager 种 含有3 种布局样式  第一个就是最常用的 1.横向 , 2. 竖向,3.偏移
            //给RecyClerView 添加设置好的布局样式
            mHotEnterpriseRecycleview.setLayoutManager(ms);

            mHotEnterpriseRecycleview.setAdapter(adapter);

            initLisenter();
        } else {
            mAttractEnterpriseMainRlv.setVisibility(GONE);
        }
    }

    /**
     * 事件监听
     */
    public void initLisenter() {
        adapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                AppRouter.doHomeAttractJump(mContext, data.jumpType, data.appChannel, data.channelValue, data.h5Url, "");
            }
        });
    }
}
