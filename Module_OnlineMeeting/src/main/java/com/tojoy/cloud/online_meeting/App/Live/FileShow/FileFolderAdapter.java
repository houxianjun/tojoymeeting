package com.tojoy.cloud.online_meeting.App.Live.FileShow;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.live.FileFolderModel;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;

public class FileFolderAdapter extends BaseRecyclerViewAdapter<FileFolderModel> {

    FileFolderAdapter(@NonNull Context context, @NonNull List<FileFolderModel> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull FileFolderModel data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_file_folder_view;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new FileFolderVH(context, parent, getLayoutResId(viewType));
    }

    public void setDatas(List<FileFolderModel> datas) {
        this.datas.clear();
        this.datas.addAll(datas);
        notifyDataSetChanged();
    }

    /**
     * 选择框Cell
     */
    public class FileFolderVH extends BaseRecyclerViewHolder<FileFolderModel> {
        private ImageView mFolderCover;
        private TextView mFolderName;
        Context context;

        FileFolderVH(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            this.context = context;
            mFolderCover = itemView.findViewById(R.id.iv_folder_cover);
            mFolderName = itemView.findViewById(R.id.tv_folder_name);
        }

        @Override
        public void onBindData(FileFolderModel message, int position) {
            if ("-1".equals(message.tagId)) {
                mFolderName.setText("我的");
                mFolderCover.setBackgroundResource(R.drawable.icon_filefolder_my);
            } else {
                mFolderName.setText(message.tagName);
                mFolderCover.setBackgroundResource(R.drawable.icon_filefolder_project);
            }
        }
    }

}