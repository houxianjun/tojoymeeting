package com.tojoy.cloud.online_meeting.App.Live.Interaction;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;

import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishQRAttachment;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.cloud.online_meeting.App.Controller.LRUEController;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.Services.Router.AppRouter;
import com.tojoy.common.Services.PayAndShare.WXShareUtil;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;

import rx.Observer;


/**
 * Created by luuzhu on 2019/4/24.
 * 直播间发项目的中间弹窗
 */

public class CenterQRDialog {

    private BaseLiveAct mContext;
    private Dialog mDialog;
    private View mView;
    private CRMsgLivePublishQRAttachment attachment;

    public CenterQRDialog(Context context, CRMsgLivePublishQRAttachment data) {
        mContext = (BaseLiveAct) context;
        attachment = data;
        initUI();
    }

    @SuppressLint("WrongConstant")
    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_live_qr_center, null);
        initView();
    }

    public boolean isShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

    private void initView() {
        ImageView mBigQRCoverTv = mView.findViewById(R.id.iv_qr_big_cover);
        ImageLoaderManager.INSTANCE.loadWantonlyRadiu(mContext,
                mBigQRCoverTv,
                OSSConfig.getRemoveStylePath(attachment.getPicBig()),
                R.drawable.popup_default_qr_big,
                R.drawable.popup_default_qr_big,
                0,
                true, true, true, true);

        mBigQRCoverTv.setOnClickListener(v -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }
            // 直接跳转
            selectPromotion();
            if ("wechat".equals(attachment.getQrType())){
                String path = attachment.getWechatUrl();
                if (!TextUtils.isEmpty(path) && path.contains("?")) {
                    path = path + "&member_mobile="+ BaseUserInfoCache.getUserMobile(mContext) +"&source=yunqiahui";
                } else {
                    path = path + "?member_mobile="+ BaseUserInfoCache.getUserMobile(mContext) +"&source=yunqiahui";
                }
                WXShareUtil.openToWeixinMini(mContext,attachment.getOpenid(),path);
            } else {
                AppRouter.doJump(mContext,attachment.getQrType(),"","",attachment.getUrl(),"");
            }
        });
        View ueLayout = mView.findViewById(R.id.doUELayout);

        //关闭按钮
        mView.findViewById(R.id.iv_qr_close).setOnClickListener(view -> {
            if (FastClickAvoidUtil.isFastDoubleClick(R.id.iv_qr_close,1000)) {
                return;
            }
            int[] activityIconLocation = (mContext).getQRSmallBannerLocation();
            LRUEController.getInstance().doCloseQRCenterUE(mContext, activityIconLocation, ueLayout, mBigQRCoverTv, new LRUEController.onUEAnimationListener() {
                @Override
                public void onSart() {

                }

                @Override
                public void onEnd() {
                    dismiss();
                }

                @Override
                public void onRepeat() {

                }
            });
        });
    }

    /**
     * 跳转小程序统计
     */
    private void selectPromotion(){
        OMAppApiProvider.getInstance().selectPromotion(attachment.getId(), LiveRoomInfoProvider.getInstance().liveId, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(OMBaseResponse omBaseResponse) {

            }
        });
    }

    public void show() {

        mDialog = null;

        mDialog = new Dialog(mContext, R.style.CenterProject);
        mDialog.setContentView(mView);

        Window win = mDialog.getWindow();
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.windowAnimations = R.style.CenterInAndOutStyle;
        lp.gravity = Gravity.CENTER;
        win.setAttributes(lp);

        mDialog.setCanceledOnTouchOutside(false);
        try {
            mDialog.show();
            mDialog.setOnKeyListener((dialog, keyCode, event) -> true);
        } catch (Exception e) {
            Log.e("TAG", "e === " + e.getMessage());
        }
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener listener) {
        mDialog.setOnDismissListener(listener);
    }
}
