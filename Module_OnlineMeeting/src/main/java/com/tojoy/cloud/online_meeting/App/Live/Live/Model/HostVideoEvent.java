package com.tojoy.cloud.online_meeting.App.Live.Live.Model;

public class HostVideoEvent {
    public String selectUserId;
    public boolean isClose;

    public HostVideoEvent(String selectUserId, boolean isClose) {
        this.selectUserId = selectUserId;
        this.isClose = isClose;
    }
}
