package com.tojoy.cloud.online_meeting.App.Distribution.mymember.activity;

import android.os.Bundle;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.cloud.online_meeting.App.Distribution.mymember.adapter.MyMemberAdapter;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.home.MineMemberDistributionResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 团队列表页面
 */
@Route(path = RouterPathProvider.Distribution_MyMember_TeamList_Act)
public class DistributionTeamListAct extends UI {
    private TJRecyclerView mRecyclerView;
    private MyMemberAdapter mAdapter;
    private int mPage = 1;
    private List<MineMemberDistributionResponse.MyMemberRecordsModel> mDatas = new ArrayList<>();
    private String distributorId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distribution_mymenber_teamlist);
        ARouter.getInstance().inject(this);
        distributorId = getIntent().getStringExtra("distributorId");
        setStatusBar();
        setSwipeBackEnable(false);
        initTitle();
        initUI();
        queryDistributionTeamListData();
    }

    private void initTitle() {
        setUpNavigationBar();
        showBack();
        setTitle("团队人数");

    }

    public void initUI() {
        mRecyclerView = (TJRecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.getRecyclerView().setVerticalScrollBarEnabled(false);
        mAdapter = new MyMemberAdapter(this, "0", mDatas);
        mRecyclerView.setLoadMoreViewWithHide();
        mRecyclerView.setEmptyImgAndTip(R.drawable.icon_no_content, "还没有团队…");
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                queryDistributionTeamListData();
            }

            @Override
            public void onLoadMore() {
                mPage++;
                queryDistributionTeamListData();
            }
        });
    }

    /**
     * 团队列表请求
     */
    public void queryDistributionTeamListData() {
        OMAppApiProvider.getInstance().queryTwoLevelDistributionMyTeamList(mPage, distributorId, new Observer<MineMemberDistributionResponse>() {

            @Override
            public void onCompleted() {
                mRecyclerView.stopRefresh();
            }

            @Override
            public void onError(Throwable e) {
                mRecyclerView.stopRefresh();
            }

            @Override
            public void onNext(MineMemberDistributionResponse response) {
                if (response.isSuccess()) {
                    if (mPage == 1) {
                        mDatas.clear();
                    }
                    mDatas.addAll(response.data.records);
                    mAdapter.updateData(mDatas);
                    mRecyclerView.refreshLoadMoreView(mPage, response.data.records.size());
                } else {
                    Toasty.normal(DistributionTeamListAct.this, response.msg).show();
                }
            }
        });

    }
}
