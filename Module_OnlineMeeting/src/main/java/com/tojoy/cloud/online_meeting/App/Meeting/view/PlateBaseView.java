package com.tojoy.cloud.online_meeting.App.Meeting.view;

import com.tojoy.tjoybaselib.model.home.HomeModelResponse;

/**
 * 负责板块需要实现函数的接口类
 */
public interface PlateBaseView {

   void initView();


   void initData();

   void initData(HomeModelResponse data);

}
