package com.tojoy.cloud.online_meeting.App.Meeting.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.model.home.HomeModelResponse;
import com.tojoy.cloud.online_meeting.App.Meeting.view.CornersGifView;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;

/**
 * 首页热门推荐横向列表适配器
 */
public class HomeAttractRecommendAdapter extends BaseRecyclerViewAdapter<HomeModelResponse.HotEnterPriseModel> {


    public HomeAttractRecommendAdapter(@NonNull Context context, @NonNull List<HomeModelResponse.HotEnterPriseModel> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull HomeModelResponse.HotEnterPriseModel data) {
        return position;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_home_attract_recommend;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new HotEnterpriseVH(context, parent, getLayoutResId(viewType));
    }


    public class HotEnterpriseVH extends BaseRecyclerViewHolder<HomeModelResponse.HotEnterPriseModel> {

        private TextView mRecommendNameTv;
        private CornersGifView mRecommendIconIv;

        public HotEnterpriseVH(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            mRecommendNameTv = itemView.findViewById(R.id.iv_recommend_name);
            mRecommendIconIv = itemView.findViewById(R.id.iv_recommend_icon);
        }

        @Override
        public void onBindData(HomeModelResponse.HotEnterPriseModel data, int position) {
            ImageLoaderManager.INSTANCE.loadVideoScreenshot(context, mRecommendIconIv,
                    OSSConfig.getOSSURLedStr(data.bannerUrl),R.drawable.icon_online_meeting_home_grid_default,AppUtils.dip2px(context, 3f),
                    false, false, false, false,0);
            mRecommendNameTv.setText(data.adName);
        }
    }
}
