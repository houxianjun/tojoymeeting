package com.tojoy.cloud.online_meeting.App.Meeting.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.ui.activity.BaseSearchAct;
import com.tojoy.common.App.BaseFragment;


@Route(path = RouterPathProvider.BrowsedEnterpriseSearchAct)
public class BrowsedEnterpriseSearchActivity extends BaseSearchAct {
    EnterPriseSearchFragment mEnterPriseSearchFragment;

    private FragmentPagerAdapter mAdapter;
    private BaseFragment[] mFragments;
   @Autowired(name="pagefrom")
   String pagefrom;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ARouter.getInstance().inject(this);
        preSearch();
    }

    @Override
    protected void initUI() {
        super.initUI();
        setSearchHint("请输入企业名称");
        hideBottomLine();
    }

    private void initViewPager() {
        mFragments = new BaseFragment[1];
        mEnterPriseSearchFragment = new EnterPriseSearchFragment();
        mFragments[0] = mEnterPriseSearchFragment;

        mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments[position];
            }

            @Override
            public int getCount() {
                return mFragments.length;
            }
        };
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(1);
        mViewPager.setCurrentItem(0);
    }

    @Override
    protected void doSearch() {
        super.doSearch();
        if (mEnterPriseSearchFragment == null) {
            initViewPager();
        }

        // 浏览过的企业搜索
        String searchKey = mSeachEditView.getText().toString().trim();
        ((EnterPriseSearchFragment) mFragments[0]).doSearch(searchKey,pagefrom);
    }

    @Override
    protected void stopSearch() {
        finish();
    }

}
