package com.tojoy.cloud.online_meeting.App.Approval.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.R;

/**
 * @author qululu
 * 审批列表筛选弹窗
 */
public class FliterPopup extends PopupWindow implements View.OnClickListener {

    private Activity mContext;
    private OnSelectListener mOnselectListener;

    public FliterPopup(Context context,OnSelectListener onSelectListener) {
        super(context);
        mContext = (Activity) context;
        mOnselectListener = onSelectListener;
        initUI();
    }

    private void initUI() {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.popup_fliter_menu_layout, null);
        setContentView(mView);
        mView.findViewById(R.id.rlv_1).setOnClickListener(this);
        mView.findViewById(R.id.rlv_2).setOnClickListener(this);
        mView.findViewById(R.id.rlv_3).setOnClickListener(this);
        mView.findViewById(R.id.rlv_4).setOnClickListener(this);
        LinearLayout popup_contianer = mView.findViewById(R.id.popup_contianer);

        // 根据显示的条目控制不同的高度（背景图片太大导致wrap_content有问题）
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) popup_contianer.getLayoutParams();
        params.width = AppUtils.dip2px(mContext,87);
        params.height = AppUtils.dip2px(mContext,148);


        popup_contianer.setLayoutParams(params);


        setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        setAnimationStyle(R.style.pop_anim_alpha);
        setBackgroundDrawable(new ColorDrawable());
        setFocusable(true);
        setOutsideTouchable(true);
        setTouchable(true);
        initListener();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.rlv_1) {
            if (mOnselectListener != null) {
                mOnselectListener.onClick("", "全部");
            }
            dismiss();

        } else if (i == R.id.rlv_2) {
            if (mOnselectListener != null) {
                mOnselectListener.onClick("0", "未处理");
            }
            dismiss();

        } else if (i == R.id.rlv_3) {
            if (mOnselectListener != null) {
                mOnselectListener.onClick("1", "已处理");
            }
            dismiss();

        }  else if (i == R.id.rlv_4) {
            // 三期新需求，增加已撤回状态
            if (mOnselectListener != null) {
                mOnselectListener.onClick("2", "已撤回");
            }
            dismiss();

        }
    }


    /**
     * 显示时屏幕变暗
     */
    public void lightOff() {
        WindowManager.LayoutParams layoutParams = mContext.getWindow().getAttributes();
        layoutParams.alpha = 0.5f;
        mContext.getWindow().setAttributes(layoutParams);
    }

    private void initListener() {
        // 消失时屏幕变亮
        setOnDismissListener(() -> {
            WindowManager.LayoutParams layoutParams = mContext.getWindow().getAttributes();
            layoutParams.alpha = 1.0f;
            mContext.getWindow().setAttributes(layoutParams);
        });
    }

    public interface OnSelectListener{
        void onClick(String status,String title);
    }

}
