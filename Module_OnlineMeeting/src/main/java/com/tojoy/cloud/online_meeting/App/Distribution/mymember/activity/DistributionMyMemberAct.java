package com.tojoy.cloud.online_meeting.App.Distribution.mymember.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.home.MineDistributionResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.TJTablayout.SlidingTabLayout;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.cloud.online_meeting.App.Distribution.mymember.fragment.MyMemberFragment;
import com.tojoy.cloud.online_meeting.R;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 我的会员页面
 */
@Route(path = RouterPathProvider.Distribution_MyMemberAct)
public class DistributionMyMemberAct extends UI {
    /**
     * 分销客状态一级分销客显示我的团队，二级分销客隐藏tab项
     */
    private String status = "";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private SlidingTabLayout mTabLayout;
    private ViewPager mViewPager;
    private String[] mTitles;
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyMemberFragment myMemberFragment;
    private MyMemberFragment myTeamFragment;

    public static void start(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, DistributionMyMemberAct.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distribution_mymenber);
        ARouter.getInstance().inject(this);
        setStatusBar();
        setSwipeBackEnable(false);
        initTitle();
        initUI();
        initData();
        queryDistributionData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initTitle() {
        setUpNavigationBar();
        showBack();

    }

    /**
     * 更新标题显示
     */
    public void updateTitle(String titleMember) {
        //当前用户是二级分销客
        if (BaseUserInfoCache.getDistributorLevel(this) == 2) {
            titleMember = !TextUtils.isEmpty(titleMember) ? "(" + titleMember + "人)" : "";
            setTitle("我的会员" + titleMember);
        } else {
            setTitle("我的会员");
        }
    }

    private void initData() {

        //二级分销客只显示我的会员
        if ("2".equals(status)) {
            mTitles = new String[1];
            mTitles[0] = "我的会员";
            myMemberFragment = new MyMemberFragment("0");
            mFragments.add(myMemberFragment);
            mTabLayout.setVisibility(View.GONE);
        } else {
            //以及分销客显示我的会员，我的团队
            mTabLayout.setVisibility(View.VISIBLE);
            mTitles = new String[2];
            mTitles[0] = "我的会员";
            mTitles[1] = "我的团队";
            myMemberFragment = new MyMemberFragment("0");
            mFragments.add(myMemberFragment);
            myTeamFragment = new MyMemberFragment("1");
            mFragments.add(myTeamFragment);
        }

        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                return mTitles[position];
            }

            @Override
            public int getCount() {
                return mTitles.length;
            }

            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }
        });
        mTabLayout.setViewPager(mViewPager, mTitles);
        mViewPager.setCurrentItem(0);

    }

    /**
     * 更新我的会员项文案
     */
    public void updateTabTitleMymember(String mymemberTotal) {
        String tmpmemberTotal = !TextUtils.isEmpty(mymemberTotal) ? "(" + mymemberTotal + "人)" : "";

        mTitles[0] = "我的会员" + tmpmemberTotal;
        mTabLayout.updateTitles(mTitles);
        mTabLayout.notifyDataSetChanged();
    }

    /**
     * 更新我的团队项文案
     */
    public void updateTabTeamMymember(String myteamTotal) {
        String tmpteamTotal = !TextUtils.isEmpty(myteamTotal) ? "(" + myteamTotal + "人)" : "";
        mTitles[1] = "我的团队" + tmpteamTotal;
        mTabLayout.updateTitles(mTitles);
        mTabLayout.notifyDataSetChanged();
    }


    private void initUI() {
        mTabLayout = (SlidingTabLayout) findViewById(R.id.tabLayot);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mTabLayout.setTabSpaceEqual(true);
        mTabLayout.setTextBold(2);
        mTabLayout.setIndicatorMargin(0, 0, 0, 8);
        mViewPager.setOffscreenPageLimit(2);
        //二级分销客
        if (BaseUserInfoCache.getDistributorLevel(this) == 2) {
            status = "2";
        } else {
            status = "1";
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * 我的收益、我的会员（推广员身份可调用接口）
     */
    public void queryDistributionData() {

        OMAppApiProvider.getInstance().getDistributionMemberInfo(new Observer<MineDistributionResponse>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(DistributionMyMemberAct.this, "网络错误，请检查网络").show();

            }

            @Override
            public void onNext(MineDistributionResponse response) {
                if (response.isSuccess()) {
                    BaseUserInfoCache.setUserDistributionData(DistributionMyMemberAct.this, response.data);
                    if (BaseUserInfoCache.getDistributorLevel(DistributionMyMemberAct.this) == 2) {
                        status = "2";
                        updateTitle(response.data.memberTotal);
                        if (myMemberFragment != null) {
                            myMemberFragment.setmPage(1);
                            myMemberFragment.initData();
                        }
                    } else {
                        status = "1";
                        if (myMemberFragment != null) {
                            myMemberFragment.setmPage(1);
                            myMemberFragment.initData();
                        }
                        if (myTeamFragment != null) {
                            myTeamFragment.setmPage(1);
                            myTeamFragment.initData();
                        } else {
                            //以及分销客显示我的会员，我的团队
                            mTabLayout.setVisibility(View.VISIBLE);
                            mTitles = new String[2];
                            mTitles[0] = "我的会员";
                            mTitles[1] = "我的团队";
                            myTeamFragment = new MyMemberFragment("1");
                            mFragments.add(myTeamFragment);

                            mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {

                                @Nullable
                                @Override
                                public CharSequence getPageTitle(int position) {
                                    return mTitles[position];
                                }

                                @Override
                                public int getCount() {
                                    return mTitles.length;
                                }

                                @Override
                                public Fragment getItem(int position) {
                                    return mFragments.get(position);
                                }
                            });
                            mTabLayout.setViewPager(mViewPager, mTitles);

                        }

                        updateTitle("");
                        updateTabTitleMymember(response.data.memberTotal);
                        updateTabTeamMymember(response.data.teamTotal);
                    }
                } else {
                    Toasty.normal(DistributionMyMemberAct.this, response.msg).show();
                }
            }
        });
    }
}
