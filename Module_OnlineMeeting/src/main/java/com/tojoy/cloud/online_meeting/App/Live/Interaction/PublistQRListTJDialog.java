package com.tojoy.cloud.online_meeting.App.Live.Interaction;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.live.PublishQRListModel;
import com.tojoy.tjoybaselib.model.live.PublishQRListResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.net.NetworkUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.tojoy.cloud.online_meeting.R;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * Created by luuzhu on 2019/4/24.
 * 项目推荐list
 */

public class PublistQRListTJDialog {

    private Dialog mDialog;
    private BaseLiveAct mContext;
    private View mView;
    private TJRecyclerView mRecyclerView;
    private List<PublishQRListModel> mDatas = new ArrayList<>();

    public PublistQRListAdapter adapter;
    private int mPage = 1;

    public PublistQRListTJDialog(Context context) {
        mContext = (BaseLiveAct) context;
        initUI();
        getData();
    }

    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_live_publist_qr_list, null);
        initView();
    }

    private void getData() {
        OMAppApiProvider.getInstance().selectMyCompanyPromotion(LiveRoomInfoProvider.getInstance().liveId,mPage,new Observer<PublishQRListResponse>() {
            @Override
            public void onCompleted() {
                mRecyclerView.setRefreshing(false);
                if (mDatas.size()==0) {
                    mView.findViewById(R.id.rlv_empty_qr).setVisibility(View.VISIBLE);
                }else{
                    mView.findViewById(R.id.rlv_empty_qr).setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(Throwable e) {
                mRecyclerView.setRefreshing(false);
                if (mDatas.size()==0) {
                    mView.findViewById(R.id.rlv_empty_qr).setVisibility(View.VISIBLE);
                }else{
                   mView.findViewById(R.id.rlv_empty_qr).setVisibility(View.GONE);
                }
                Toasty.normal(mContext,"网络异常，请稍后再试").show();
            }

            @Override
            public void onNext(PublishQRListResponse omBaseResponse) {
                if (omBaseResponse.isSuccess()) {
                    if (mPage == 1) {
                        mDatas.clear();
                    }
                    mDatas.addAll(omBaseResponse.data.list);
                    adapter.notifyDataSetChanged();
                    mRecyclerView.showFooterView();
                    mRecyclerView.refreshLoadMoreView(mPage, omBaseResponse.data.list.size());
                } else {
                    Toasty.normal(mContext,omBaseResponse.msg).show();
                }
            }
        });

    }

    public void show() {
        mDialog = new Dialog(mContext, R.style.LiveDialogFragmentEnterExitAnim);
        mDialog.setContentView(mView);

        Window win = mDialog.getWindow();
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.windowAnimations = R.style.BottomInAndOutStyle;
        lp.gravity = Gravity.BOTTOM;
        win.setAttributes(lp);

        mDialog.setCanceledOnTouchOutside(true);
        try {
            mDialog.show();
            mDialog.setOnKeyListener((dialog, keyCode, event) -> true);
        } catch (Exception e) {

        }
    }

    private void initView() {
        ((TextView) mView.findViewById(R.id.tv_title)).setText("发布活动");
        mRecyclerView = mView.findViewById(R.id.recyclerView);
        GridLayoutManager mGridLayoutManager = new GridLayoutManager(mContext, 2, LinearLayoutManager.VERTICAL, false);
        mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int i) {
                if (mDatas.size() == 0) {
                    mRecyclerView.hideFooterView();
                    return 1;
                } else {
                    mRecyclerView.showFooterView();
                    return i == mDatas.size() ? 2 : 1;
                }
            }
        });
        mRecyclerView.getRecyclerView().setVerticalScrollBarEnabled(false);
        mRecyclerView.getRecyclerView().setLayoutManager(mGridLayoutManager);
        mRecyclerView.setLoadMoreViewWithHide();
        mRecyclerView.setEmptyImgAndTip(R.drawable.icon_empty_outer_live, mContext.getString(R.string.instant_no_content));
        adapter = new PublistQRListAdapter();
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                getData();
            }

            @Override
            public void onLoadMore() {
                if (mDatas.isEmpty()) {
                    return;
                }
                mPage++;
                getData();
            }
        });

        mView.findViewById(R.id.iv_close).setOnClickListener(view -> mDialog.dismiss());
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener listener) {
        mDialog.setOnDismissListener(listener);
    }

    public boolean isShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }


    public class PublistQRListAdapter extends RecyclerView.Adapter {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = null;
            itemView = LayoutInflater.from(mContext).inflate(R.layout.item_publish_qr_list_tj, parent, false);
            return new PublistQRVH(itemView);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            PublistQRVH projectVH = (PublistQRVH) holder;
            projectVH.bindData(position);
        }

        @Override
        public int getItemCount() {
            return mDatas == null ? 0 : mDatas.size();
        }
    }

    private class PublistQRVH extends RecyclerView.ViewHolder {

        private ImageView ivCover;
        private TextView tvTitle;
        private TextView tvGone;
        private TextView tvSend;
        private ImageView ivGone;

        boolean isCanClickSend = false;
        boolean isCanClickGone = false;

        PublistQRVH(View itemView) {
            super(itemView);
            ivCover = itemView.findViewById(R.id.iv_cover);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvGone = itemView.findViewById(R.id.tv_gone);
            tvSend = itemView.findViewById(R.id.tv_send);
            ivGone = itemView.findViewById(R.id.iv_gone);

            int width = (AppUtils.getWidth(mContext) - AppUtils.dip2px(mContext, 32) - AppUtils.dip2px(mContext, 2 * 4 + 2 * 8)) / 2;
            int height = (int) (width * (90.0f / 160.f));

            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(width, height);
            lps.setMargins(AppUtils.dip2px(mContext, 4), AppUtils.dip2px(mContext, 14), 0, 0);
            ivCover.setLayoutParams(lps);
        }

        void bindData(int position) {
            PublishQRListModel data = mDatas.get(position);
            tvTitle.setText(data.getName());
            ImageLoaderManager.INSTANCE.loadImage(mContext, ivCover, data.getPicBig(), R.drawable.popup_default_qr_big, 5);

            switch (data.getStatus()) {
                case -1:
                    // 从未发布过（页面显示：隐藏-置灰&&不可点；发布-可点）
                    tvSend.setText("发布");
                    tvGone.setText("隐藏");
                    tvGone.setTextColor(mContext.getResources().getColor(R.color.color_A9A9AB));
                    ivGone.setBackground(mContext.getResources().getDrawable(R.drawable.icon_publish_qr_gone));
                    isCanClickSend = true;
                    isCanClickGone = false;
                    break;
                case 1:
                    // 已经发布过且不是隐藏状态（页面显示：隐藏-高亮&&可点；再次发布-可点）
                    tvSend.setText("再次发布");
                    tvGone.setText("隐藏");
                    tvGone.setTextColor(mContext.getResources().getColor(R.color.color_237bf1));
                    ivGone.setBackground(mContext.getResources().getDrawable(R.drawable.icon_publish_qr_gone_select));
                    isCanClickSend = true;
                    isCanClickGone = true;
                    break;
                case 2:
                    // 已发布过且当前为隐藏状态（页面显示：已隐藏-置灰&&不可点；再次发布-可点）
                    tvSend.setText("再次发布");
                    tvGone.setText("已隐藏");
                    tvGone.setTextColor(mContext.getResources().getColor(R.color.color_A9A9AB));
                    ivGone.setBackground(mContext.getResources().getDrawable(R.drawable.icon_publish_qr_gone));
                    isCanClickSend = true;
                    isCanClickGone = false;
                    break;
                default:
                    // 其他状态均默认未发布过状态（但不可点击）
                    tvSend.setText("发布");
                    tvGone.setText("隐藏");
                    tvGone.setTextColor(mContext.getResources().getColor(R.color.color_A9A9AB));
                    ivGone.setBackground(mContext.getResources().getDrawable(R.drawable.icon_publish_qr_gone));
                    isCanClickSend = false;
                    isCanClickGone = false;
                    break;
            }


            itemView.findViewById(R.id.op_left).setOnClickListener(view -> {

                if (FastClickAvoidUtil.isDoubleClick() || !isCanClickGone) {
                    return;
                }

                sendOrGone(false,data.id);
            });

            //发布
            itemView.findViewById(R.id.op_right).setOnClickListener(view -> {

                if (FastClickAvoidUtil.isDoubleClick() || !isCanClickSend) {
                    return;
                }

                sendOrGone(true,data.id);
            });
        }

        /**
         * 发布/隐藏活动
         */
        private void sendOrGone(boolean isSend,String detailId){
            if (!NetworkUtil.isNetAvailable(mContext)) {
                Toasty.normal(mContext, "网络错误，请检查网络").show();
                return;
            }
            mContext.showProgressHUD(mContext,"");
            OMAppApiProvider.getInstance().pushPromotion(detailId,LiveRoomInfoProvider.getInstance().imRoomId,
                    LiveRoomInfoProvider.getInstance().liveId,isSend ? "1" : "2" ,new Observer<OMBaseResponse>() {
                @Override
                public void onCompleted() {
                    mContext.dismissProgressHUD();
                }

                @Override
                public void onError(Throwable e) {
                    mContext.dismissProgressHUD();
                    Toasty.normal(mContext,"网络异常，请稍后再试").show();
                }

                @Override
                public void onNext(OMBaseResponse omBaseResponse) {
                    if (omBaseResponse.isSuccess()){
                        Toasty.normal(mContext,isSend ? "发布成功" : "隐藏成功").show();
                        // 只对该数据进行状态改变
                        if (isSend) {
                            // 已经发布过且不是隐藏状态（页面显示：隐藏-高亮&&可点；再次发布-可点）
                            tvSend.setText("再次发布");
                            tvGone.setText("隐藏");
                            tvGone.setTextColor(mContext.getResources().getColor(R.color.color_237bf1));
                            ivGone.setBackground(mContext.getResources().getDrawable(R.drawable.icon_publish_qr_gone_select));
                            isCanClickSend = true;
                            isCanClickGone = true;
                        } else {
                            // 已发布过且当前为隐藏状态（页面显示：已隐藏-置灰&&不可点；再次发布-可点）
                            tvSend.setText("再次发布");
                            tvGone.setText("已隐藏");
                            tvGone.setTextColor(mContext.getResources().getColor(R.color.color_A9A9AB));
                            ivGone.setBackground(mContext.getResources().getDrawable(R.drawable.icon_publish_qr_gone));
                            isCanClickSend = true;
                            isCanClickGone = false;
                        }
                    } else {
                        Toasty.normal(mContext,omBaseResponse.msg).show();
                    }
                }
            });
        }
    }
}
