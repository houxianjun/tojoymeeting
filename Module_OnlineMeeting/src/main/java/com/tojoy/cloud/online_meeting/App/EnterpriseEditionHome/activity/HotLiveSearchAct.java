package com.tojoy.cloud.online_meeting.App.EnterpriseEditionHome.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.netease.nim.uikit.business.chatroom.helper.ChatRoomHelper;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.ui.activity.BaseSearchAct;
import com.tojoy.tjoybaselib.util.sys.LiveActAtack;
import com.tojoy.cloud.online_meeting.App.Home.activity.HotLiveSearchFragment;
import com.tojoy.common.App.BaseFragment;


@Route(path = RouterPathProvider.HotLiveSearchAct)
public class HotLiveSearchAct extends BaseSearchAct {
    HotLiveSearchFragment mHotLiveSearchFragment;

    private BaseFragment[] mFragments;

    public static void start(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, HotLiveSearchAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preSearch();

        if (ChatRoomHelper.isFromLiveRoom) {
            LiveActAtack.getActivityStack().pushActivity(this);
        }
    }

    @Override
    protected void initUI() {
        super.initUI();
        setSearchHint("请输入会议ID或标题");
        hideBottomLine();
    }

    private void initViewPager() {
        mFragments = new BaseFragment[1];
        Bundle bundle = new Bundle();
        bundle.putString("companyCode", getIntent().getStringExtra("companyCode"));
        mHotLiveSearchFragment = new HotLiveSearchFragment();
        mHotLiveSearchFragment.setArguments(bundle);
        mFragments[0] = mHotLiveSearchFragment;

        FragmentPagerAdapter mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments[position];
            }

            @Override
            public int getCount() {
                return mFragments.length;
            }
        };
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(1);
        mViewPager.setCurrentItem(0);
    }

    @Override
    protected void doSearch() {
        super.doSearch();
        if (mHotLiveSearchFragment == null) {
            initViewPager();
        }
        String searchKey = mSeachEditView.getText().toString().trim();
        ((HotLiveSearchFragment) mFragments[0]).doSearch(searchKey);
    }

    @Override
    protected void stopSearch() {
        finish();
    }

}
