package com.tojoy.cloud.online_meeting.App.Live.Live.Model;

/**
 * @author qll
 * 电话通话状态的监听
 */
public class PhoneStateEvent {
    public String state;
    public PhoneStateEvent(String s) {
        state = s;
    }
}
