package com.tojoy.cloud.online_meeting.App.Live.ChatRoom;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.netease.nim.uikit.api.NimUIKit;
import com.netease.nim.uikit.api.model.user.PushMsgParser;
import com.netease.nim.uikit.api.model.user.SystemNotice;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLiveOverAttachment;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishDistributionAttachment;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishProjectAttachment;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishQRAttachment;
import com.netease.nim.uikit.business.chatroom.extension.LRClosingMsgAttachment;
import com.netease.nim.uikit.business.chatroom.extension.LRGuestTipsAttachment;
import com.netease.nim.uikit.business.chatroom.extension.LRRemoveMsgAttachment;
import com.netease.nim.uikit.business.chatroom.extension.LiveRoomCustomMessageAttachment;
import com.netease.nim.uikit.business.chatroom.helper.ChatRoomHelper;
import com.netease.nim.uikit.business.chatroom.module.LiveKickedOutBean;
import com.netease.nim.uikit.business.chatroom.module.LiveRoomInputPanel;
import com.netease.nim.uikit.business.chatroom.module.LiveRoomMsgListPanel;
import com.netease.nim.uikit.business.chatroom.module.SenstiveBean;
import com.netease.nim.uikit.business.chatroom.viewholder.ChatRoomMsgViewHolderText;
import com.netease.nim.uikit.business.liveroom.LiveRoomHelper;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachment;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachmentType;
import com.netease.nim.uikit.business.session.actions.BaseAction;
import com.netease.nim.uikit.business.session.module.Container;
import com.netease.nim.uikit.business.session.module.ModuleProxy;
import com.netease.nim.uikit.common.IMLoginManager;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.ResponseCode;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.ChatRoomServiceObserver;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomInfo;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessageExtension;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomNotificationAttachment;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomData;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.constant.NotificationType;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.CustomNotification;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.model.live.SelectOutLiveModel;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.ui.fragment.TFragment;
import com.tojoy.tjoybaselib.util.string.StringUtil;
import com.tojoy.cloud.online_meeting.App.Controller.LRDialogController;
import com.tojoy.cloud.online_meeting.App.Live.Interaction.CenterProjectDialog;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;
import com.tojoy.cloud.online_meeting.App.UserCenter.callback.LiveCRFragmentCallback;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.Services.EventBus.PublicStringEvent;
import com.tojoy.common.Services.Push.SystemNoticeType;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

/**
 * Created by luuzhu on 2019/4/18.
 */

@SuppressLint("ValidFragment")
public class LiveCRMessageFragment extends TFragment implements ModuleProxy {

    protected View rootView;
    public LiveRoomInputPanel inputPanel;
    public LiveRoomMsgListPanel messageListPanel;

    private Activity mContext;

    public CenterProjectDialog mCenterProjectDialog;
    private String mChatListType = "1";
    private boolean hasKickOut;

    private boolean registerObservers;

    private LiveCRFragmentCallback mLiveCRFragmentCallback;

    @SuppressLint("ValidFragment")
    public LiveCRMessageFragment(LiveCRFragmentCallback liveCRFragmentCallback) {
        this.mLiveCRFragmentCallback = liveCRFragmentCallback;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getActivity();
        rootView = inflater.inflate(R.layout.live_room_message_fragment, container, false);
        init();
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    public void init() {
        enterChatRoom();
    }

    public void enterChatRoom() {
        EnterChatRoomData data = new EnterChatRoomData(LiveRoomInfoProvider.getInstance().imRoomId);
        //设置进入聊天室的扩展信息，头像和职位
        Map<String, Object> userInfoMap = new HashMap<>();
        userInfoMap.put("nickName", BaseUserInfoCache.getUserNickName(getContext()));
        userInfoMap.put("headImgUrl", BaseUserInfoCache.getUserHeaderPic(getContext()));
        userInfoMap.put("job", BaseUserInfoCache.getJobName(getContext()));
        userInfoMap.put("company", BaseUserInfoCache.getCompanyName(getContext()));
        data.setExtension(userInfoMap);
        if (!TextUtils.isEmpty(BaseUserInfoCache.getUserNickName(getContext()))) {
            data.setNick(BaseUserInfoCache.getUserNickName(getContext()));
        }

        NIMClient.getService(ChatRoomService.class).enterChatRoomEx(data, 3).setCallback(new RequestCallback<EnterChatRoomResultData>() {
            @Override
            public void onSuccess(EnterChatRoomResultData param) {
                NimUIKit.enterChatRoomSuccess(param, true);
                findViews();
                if (!registerObservers) {
                    registerObservers(true);
                }
                //聊天室名字和公告
                ChatRoomInfo roomInfo = param.getRoomInfo();
                LiveRoomInfoProvider.getInstance().announcement = roomInfo.getAnnouncement();
                LiveRoomInfoProvider.getInstance().isMute = (param.getMember().isTempMuted() || param.getMember().isMuted());
                LiveRoomInfoProvider.getInstance().isMuteAll = param.getRoomInfo().isMute();

                if (AppConfig.isCanToast()) {
                    Toasty.normal(mContext, "进聊天室成功").show();
                }

                if (mLiveCRFragmentCallback != null) {
                    mLiveCRFragmentCallback.onEnterChatRoom();
                }
            }

            @Override
            public void onFailed(int code) {
                statitiscJoinCR();
                LogUtil.i(LiveLogTag.LRMsgTag, "enterChatRoomFail" + code + ";" + LiveRoomInfoProvider.getInstance().imRoomId);
                if (AppConfig.isCanToast()) {
                    Toasty.normal(mContext, "进聊天室SDK出错 code = " + code + "imRoomId:" + LiveRoomInfoProvider.getInstance().imRoomId).show();
                }

                if (code == 1000) {
                    IMLoginManager.getInterface().requestLoginIM(getContext(), "1", new IMLoginManager.LoginSuccessCallback() {
                        @Override
                        public void onLoginSuccess() {
                            if (AppConfig.isCanToast()) {
                                Toasty.normal(mContext, "IM重新登录成功").show();
                            }
                            enterChatRoom();
                        }

                        @Override
                        public void onLoginError(int code) {
                            if (AppConfig.isCanToast()) {
                                Toasty.normal(mContext, "IM重新登录失败：" + code).show();
                            }
                        }
                    });
                }
            }

            @Override
            public void onException(Throwable exception) {
                statitiscJoinCR();
                if (AppConfig.isCanToast()) {
                    Toasty.normal(mContext, "进聊天室SDK异常 e = " + exception.getMessage() + "imRoomId:" + LiveRoomInfoProvider.getInstance().imRoomId).show();
                }
                LogUtil.i(LiveLogTag.LRMsgTag, "enterChatRoomExc" + exception.getMessage() + ";" + LiveRoomInfoProvider.getInstance().imRoomId);
            }
        });
    }

    private void statitiscJoinCR() {

        OMAppApiProvider.getInstance().statitiscIMLogin("2", "enterLivePage", new rx.Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(OMBaseResponse omBaseResponse) {
            }
        });
    }

    private void findViews() {

        Container container = new Container(mContext, LiveRoomInfoProvider.getInstance().imRoomId, SessionTypeEnum.ChatRoom, LiveCRMessageFragment.this);
        if (messageListPanel == null) {
            messageListPanel = new LiveRoomMsgListPanel(container, rootView);
        } else {
            messageListPanel.reload(container);
        }

        if (inputPanel == null) {
            inputPanel = new LiveRoomInputPanel(getActivity(), container, rootView, getActionList(), false);
        } else {
            inputPanel.reload(container, null);
        }
        LiveRoomHelper.getInstance().setInputPanel(inputPanel);
        LiveRoomHelper.getInstance().setMessageListPanel(messageListPanel);
    }

    public void registerObservers(boolean register) {

        registerObservers = register;

        NIMClient.getService(ChatRoomServiceObserver.class).observeReceiveMessage(incomingChatRoomMsg, register);

        //自定义消息监听敏感词结果
        NIMClient.getService(MsgServiceObserve.class).observeCustomNotification((Observer<CustomNotification>) customNotification -> {
            SystemNotice systemNotice = new PushMsgParser(customNotification.getContent().getBytes()).doParser();
            if (systemNotice == null || TextUtils.isEmpty(systemNotice.type)) {
                return;
            }

            String content = customNotification.getContent();
            if (SystemNoticeType.SENSTIVE.equals(systemNotice.type)) {
                //敏感词过滤
                Gson gson = new Gson();
                SenstiveBean senstiveBean = gson.fromJson(content, SenstiveBean.class);
                if (!TextUtils.isEmpty(senstiveBean.messageid)) {
                    //刷新消息列表里的这条消息
                    if (messageListPanel != null) {
                        getHandler().postDelayed(() -> messageListPanel.refreshMsgByUuId(senstiveBean.messageid), 250);
                    }
                }
            } else if (SystemNoticeType.CLOSE_LIVE_STREAM.equals(systemNotice.type)) {
                Log.d("PlayEvent", content);
                Gson gson = new Gson();
                SelectOutLiveModel selectOutLiveModel = gson.fromJson(content, SelectOutLiveModel.class);
                if (selectOutLiveModel != null && mLiveCRFragmentCallback != null) {
                    mLiveCRFragmentCallback.stopSomeLive(selectOutLiveModel.streamId, selectOutLiveModel.roomTitle);
                }
            } else if (SystemNoticeType.OUT_LIVE_ROOM_LIVE_ADD.equals(systemNotice.type)) {
                Log.d("PlayEvent", content);
                Gson gson = new Gson();
                SelectOutLiveModel selectOutLiveModel = gson.fromJson(content, SelectOutLiveModel.class);
                if (selectOutLiveModel != null && mLiveCRFragmentCallback != null) {
                    mLiveCRFragmentCallback.onOutliveRoomLiveAdd(selectOutLiveModel);
                }
            } else if (SystemNoticeType.OUT_LIVE_ROOM_LIVE_DECREASE.equals(systemNotice.type)) {
                Log.d("PlayEvent", content);
                Gson gson = new Gson();
                SelectOutLiveModel selectOutLiveModel = gson.fromJson(content, SelectOutLiveModel.class);
                if (selectOutLiveModel != null && mLiveCRFragmentCallback != null) {
                    mLiveCRFragmentCallback.stopSomeLive(selectOutLiveModel.streamId, selectOutLiveModel.roomTitle);
                }
            } else if (SystemNoticeType.CLOSE_ENTERPRISE_FORBID.equals(systemNotice.type)) {
                //企业禁用
                if (mLiveCRFragmentCallback != null) {
                    mLiveCRFragmentCallback.enterpriseForbidLive();
                }
            } else {//被踢出直播间
                if (content.contains("AddBlack")) {
                    if (!ChatRoomHelper.isFromLiveRoom) {
                        //已经退出了直播间
                        return;
                    }
                    if (hasKickOut) {
                        return;
                    }
                    hasKickOut = true;
                    Gson gson = new Gson();
                    LiveKickedOutBean kickedOutBean = gson.fromJson(content, LiveKickedOutBean.class);
                    if ("AddBlack".equals(kickedOutBean.action)) {
                        if (mLiveCRFragmentCallback != null) {
                            mLiveCRFragmentCallback.onKickedOutCallback();
                        }
                    }
                }
            }

        }, register);
    }

    Observer<List<ChatRoomMessage>> incomingChatRoomMsg = new Observer<List<ChatRoomMessage>>() {
        @Override
        public void onEvent(List<ChatRoomMessage> messages) {
            if (messages == null || messages.isEmpty()) {
                return;
            }
            if (TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().imRoomId) || !LiveRoomInfoProvider.getInstance().imRoomId.equals(messages.get(0).getSessionId())) {
                return;
            }

            //滤掉自己发送的文本消息 / 别的聊天室乱入的消息
            int sizeNew = messages.size();
            for (int i = sizeNew - 1; i >= 0; i--) {
                ChatRoomMessage chatRoomMessage = messages.get(i);
                String fromAccount = chatRoomMessage.getFromAccount();
                Map<String, Object> ext = chatRoomMessage.getRemoteExtension();
                if(ext!=null && !ext.isEmpty()){
                  String source=  (String)ext.get("ext");
                    //消息发送端是1 app 2 小程序
                    if(source!=null&&source.equals("1")){
                        if (!TextUtils.isEmpty(fromAccount)
                                && fromAccount.contains(BaseUserInfoCache.getUserId(mContext))
                                && MsgTypeEnum.text == chatRoomMessage.getMsgType()) {
                            messages.remove(i);
                        }
                    }
                }
                else{
                    if (!TextUtils.isEmpty(fromAccount)
                            && fromAccount.contains(BaseUserInfoCache.getUserId(mContext))
                            && MsgTypeEnum.text == chatRoomMessage.getMsgType()) {
                        messages.remove(i);
                    }
                }
            }

            if (messages.isEmpty()) {
                return;
            }
            //进入的系统消息数
            int enterMsgCount = 0;

            int size = messages.size();
            for (int i = size - 1; i >= 0; i--) {
                MsgAttachment attachment = messages.get(i).getAttachment();
                if (attachment instanceof ChatRoomNotificationAttachment) {
                    //如果是系统通知消息
                    //过滤掉离开聊天室/禁言/解禁言/设为取消管理员的消息
                    ChatRoomNotificationAttachment notificationAttachment = (ChatRoomNotificationAttachment) attachment;
                    LogUtil.i(LiveLogTag.LRMsgTag, notificationAttachment.toString());
                    if (notificationAttachment.getType() == NotificationType.ChatRoomMemberExit//离开
                            || notificationAttachment.getType() == NotificationType.ChatRoomMemberMuteAdd//禁言
                            || notificationAttachment.getType() == NotificationType.ChatRoomMemberMuteRemove//解禁言
                            || notificationAttachment.getType() == NotificationType.ChatRoomMemberTempMuteAdd//临时禁言
                            || notificationAttachment.getType() == NotificationType.ChatRoomMemberTempMuteRemove//解临时禁言
                            || notificationAttachment.getType() == NotificationType.ChatRoomRoomMuted//全体禁言
                            || notificationAttachment.getType() == NotificationType.ChatRoomRoomDeMuted//解除全体禁言
                            || notificationAttachment.getType() == NotificationType.ChatRoomManagerAdd//设管理员
                            || notificationAttachment.getType() == NotificationType.ChatRoomManagerRemove//取消管理员
                            || notificationAttachment.getType() == NotificationType.ChatRoomMemberBlackAdd//被踢出
                            || notificationAttachment.getType() == NotificationType.ChatRoomInfoUpdated) {//聊天室信息变更-公告

                        messages.remove(i);

                        if (notificationAttachment.getType() == NotificationType.ChatRoomManagerAdd) {
                            if (notificationAttachment.getTargets().contains(NimUIKit.getAccount())) {
                                LiveRoomInfoProvider.getInstance().myUserType = "1";
                                EventBus.getDefault().post(new PublicStringEvent(PublicStringEvent.LIVE_GENGLE_CHANGE));
                            }
                        } else if (notificationAttachment.getType() == NotificationType.ChatRoomManagerRemove) {
                            if (notificationAttachment.getTargets().contains(NimUIKit.getAccount())) {
                                LiveRoomInfoProvider.getInstance().myUserType = "2";
                                EventBus.getDefault().post(new PublicStringEvent(PublicStringEvent.LIVE_GENGLE_CHANGE));
                            }
                        } else if (notificationAttachment.getType() == NotificationType.ChatRoomMemberTempMuteAdd
                                || notificationAttachment.getType() == NotificationType.ChatRoomMemberMuteAdd) {
                            if (notificationAttachment.getTargets().contains(NimUIKit.getAccount())) {
                                LiveRoomInfoProvider.getInstance().isMute = true;
                            }
                        } else if (notificationAttachment.getType() == NotificationType.ChatRoomMemberTempMuteRemove
                                || notificationAttachment.getType() == NotificationType.ChatRoomMemberMuteRemove) {
                            if (notificationAttachment.getTargets().contains(NimUIKit.getAccount())) {
                                LiveRoomInfoProvider.getInstance().isMute = false;
                            }
                        } else if (notificationAttachment.getType() == NotificationType.ChatRoomRoomMuted) {
                            //群禁群解
                            LogUtil.i(LiveLogTag.BaseLiveTag, "ChatRoomRoomMuted");
                            LiveRoomInfoProvider.getInstance().isMuteAll = true;
                            if (!LiveRoomInfoProvider.getInstance().isHost()) {
                                LiveRoomHelper.getInstance().sendMuteAllMsg(true);
                            }
                        } else if (notificationAttachment.getType() == NotificationType.ChatRoomRoomDeMuted) {
                            LogUtil.i(LiveLogTag.BaseLiveTag, "ChatRoomRoomDeMuted");
                            LiveRoomInfoProvider.getInstance().isMuteAll = false;
                            if (!LiveRoomInfoProvider.getInstance().isHost()) {
                                LiveRoomHelper.getInstance().sendMuteAllMsg(false);
                            }
                        } else if (notificationAttachment.getType() == NotificationType.ChatRoomInfoUpdated) {
                            NimUIKit.getChatRoomInfo(LiveRoomInfoProvider.getInstance().imRoomId, new RequestCallback<ChatRoomInfo>() {
                                @Override
                                public void onSuccess(ChatRoomInfo param) {

                                    if (!TextUtils.isEmpty(param.getAnnouncement())) {
                                        LiveRoomInfoProvider.getInstance().announcement = param.getAnnouncement();
                                        if (mLiveCRFragmentCallback != null) {
                                            mLiveCRFragmentCallback.onNoticeUpdateCallback();
                                        }
                                    }
                                }

                                @Override
                                public void onFailed(int code) {

                                }

                                @Override
                                public void onException(Throwable exception) {

                                }
                            });
                        }
                    } else if (notificationAttachment.getType() == NotificationType.ChatRoomMemberIn) {
                        //进入，不计未读
                        enterMsgCount++;
                        if (mLiveCRFragmentCallback != null) {
                            String name = "";
                            ChatRoomMessageExtension extension = messages.get(i).getChatRoomMessageExtension();
                            messages.remove(i);
                            Map<String, Object> senderExtension = extension.getSenderExtension();
                            if (senderExtension != null) {
                                name = (String) senderExtension.get("nickName");
                            }
                            LiveRoomInfoProvider.getInstance().joinNamesArr.add(name);
                            mLiveCRFragmentCallback.onMemberJoin(name);
                        }
                    }

                } else if (attachment instanceof CustomAttachment) {
                    //如果是自定义消息
                    CustomAttachment customAttachment = (CustomAttachment) attachment;
                    if (customAttachment.getType() == CustomAttachmentType.LIVE_PUBLISH_PROJECT) {
                        //直播间发布项目
                        messages.remove(i);
                        CRMsgLivePublishProjectAttachment att = (CRMsgLivePublishProjectAttachment) attachment;
                        LogUtil.i(LiveLogTag.LRMsgTag, att.toString());
                        //查询这个项目当前用户感不感兴趣
                        OMAppApiProvider.getInstance().isInterested(att.getProjectId(), BaseUserInfoCache.getUserId(mContext), new rx.Observer<OMBaseResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(OMBaseResponse response) {//状态码返回1表示已经感兴趣过了
                                if ("1".equals(response.code)) {
                                    att.setIsInterest("1");
                                }

                                //如果现有的集合里没有这个项目,就添加
                                ArrayList<CRMsgLivePublishProjectAttachment> projects = LiveRoomHelper.getInstance().receivedProjects;
                                int length = projects.size();
                                boolean isContains = false;
                                CRMsgLivePublishProjectAttachment alreadyAtt = null;
                                for (int k = 0; k < length; k++) {
                                    if (att.getProjectId().equals(projects.get(k).getProjectId())) {
                                        isContains = true;

                                        CRMsgLivePublishProjectAttachment data = projects.get(k);
                                        alreadyAtt = data;
                                        if ("1".equals(att.getIsInterest())) {
                                            alreadyAtt.setIsInterest("1");
                                            data.setIsInterest("1");
                                        }
                                    }
                                }
                                if (!isContains) {
                                    LiveRoomHelper.getInstance().receivedProjects.add(att);
                                }

                                if (LiveRoomHelper.getInstance().receivedProjects.isEmpty()) {
                                    return;
                                }

                                if (LiveRoomHelper.getInstance().productListBtn != null) {
                                    LiveRoomHelper.getInstance().productListBtn.setVisibility(View.VISIBLE);
                                }

                                if (LiveRoomInfoProvider.getInstance().isHost()) {
                                    return;
                                }

                                if (mCenterProjectDialog != null && mCenterProjectDialog.isShowing()) {
                                    if (isContains && alreadyAtt != null) {
                                        mCenterProjectDialog.updata(false, alreadyAtt);
                                    } else {
                                        mCenterProjectDialog.updata(false, att);
                                    }
                                } else {
                                    if (isContains && alreadyAtt != null) {
                                        mCenterProjectDialog = new CenterProjectDialog(mContext, alreadyAtt);
                                    } else {
                                        mCenterProjectDialog = new CenterProjectDialog(mContext, att);
                                    }
                                    mCenterProjectDialog.show();
                                }
                            }
                        });

                    } else if (customAttachment.getType() == CustomAttachmentType.LIVE_PUBLISH_QR) {
                        //直播间发布活动（二维码）
                        messages.remove(i);
                        CRMsgLivePublishQRAttachment att = (CRMsgLivePublishQRAttachment) attachment;
                        LogUtil.i(LiveLogTag.LRMsgTag, att.toString());
                        // 处理Big：
                        boolean isShowCenterQR = false;
                        if (LRDialogController.getInstance().mCenterQRDialog != null && LRDialogController.getInstance().mCenterQRDialog.isShowing()) {
                            isShowCenterQR = true;
                        }
                        if (!LiveRoomInfoProvider.getInstance().isHost()) {
                            // 主播不弹中间的弹窗
                            LiveRoomHelper.getInstance().addQRDataBig(att,isShowCenterQR);
                        }
                        // 处理small:
                        LiveRoomHelper.getInstance().addQRDataSmall(att);

                        if (mLiveCRFragmentCallback != null) {
                            mLiveCRFragmentCallback.onQRDataRefresh(att);
                        }

                    } else if (customAttachment.getType() == CustomAttachmentType.LIVE_PUBLISH_DISTRUBTION) {
                        // 会议被设置为可分销会议
                        messages.remove(i);
                        CRMsgLivePublishDistributionAttachment publishDistributionAttachment = (CRMsgLivePublishDistributionAttachment) attachment;
                        LogUtil.i(LiveLogTag.LRMsgTag, publishDistributionAttachment.toString());
                        if (mLiveCRFragmentCallback != null) {
                            mLiveCRFragmentCallback.onDistributionCallBack(publishDistributionAttachment);
                        }
                    } else if (customAttachment.getType() == CustomAttachmentType.LIVE_OVER_LIVE) {
                        //结束直播
                        messages.remove(i);
                        if (mLiveCRFragmentCallback != null) {
                            getHandler().postDelayed(() -> {
                                mLiveCRFragmentCallback.onEndingLive();

                                CRMsgLiveOverAttachment overAttachment = (CRMsgLiveOverAttachment) attachment;
                                LogUtil.i(LiveLogTag.LRMsgTag, overAttachment.toString());

                                String tipsMsgHost = overAttachment.getTipsMsg();
                                String tipsMsgViewer = overAttachment.getTipsMsgViewer();

                                LiveRoomInfoProvider.getInstance().tipsMsgHost = tipsMsgHost;
                                LiveRoomInfoProvider.getInstance().tipsMsgViewer = tipsMsgViewer;

                                if (!TextUtils.isEmpty(tipsMsgHost) || !TextUtils.isEmpty(tipsMsgViewer)) {
                                    LiveRoomInfoProvider.getInstance().isSysClosedLiveRoom = true;
                                }
                            }, 1000);
                        }
                    } else if (customAttachment.getType() == CustomAttachmentType.LIVE_CUSTOM_MESSAGE) {
                        messages.remove(i);
                        LiveRoomCustomMessageAttachment att = (LiveRoomCustomMessageAttachment) attachment;
                        mLiveCRFragmentCallback.onReceiveLiveRoomCustomMessage(att.getExt());
                    } else if (customAttachment.getType() == CustomAttachmentType.LIVE_HELPER_LEAVE) {
                        //助手离开直播间
                        //只有主播显示此消息
                        if (!LiveRoomInfoProvider.getInstance().isHost()) {
                            messages.remove(i);
                        }
                    } else if (customAttachment.getType() == CustomAttachmentType.LIVE_DRAINAGE) {
                        //直播间被引流
                        if (!LiveRoomInfoProvider.getInstance().isHost()) {
                            messages.remove(i);
                        }
                    } else if (customAttachment.getType() == CustomAttachmentType.LIVE_REMOVE_MESSAGE) {
                        //删除某条消息
                        messages.remove(i);
                        LRRemoveMsgAttachment msgAttachment = (LRRemoveMsgAttachment) attachment;
                        if (!TextUtils.isEmpty(msgAttachment.getMsgIdStr())) {
                            String[] split = msgAttachment.getMsgIdStr().split(",");
                            for (String aSplit : split) {
                                if (!TextUtils.isEmpty(aSplit)) {
                                    messageListPanel.removeMessage(aSplit);
                                }
                            }
                        }
                    } else if (customAttachment.getType() == CustomAttachmentType.LIVE_GUEST_TIPS) {
                        //***点击感兴趣，***要合作项目，***抢占项目区域
                        messages.remove(i);
                        if (mLiveCRFragmentCallback != null) {

                            LRGuestTipsAttachment tipsAttachment = (LRGuestTipsAttachment) attachment;
                            LogUtil.i(LiveLogTag.LRMsgTag, tipsAttachment.toString());
                            String info = tipsAttachment.getInfo();
                            if (!TextUtils.isEmpty(info)) {
                                LiveRoomInfoProvider.getInstance().guestTipsArr.add(info);
                                mLiveCRFragmentCallback.onReceiveGuestTips(info);
                            }
                        }

                    } else if (customAttachment.getType() == CustomAttachmentType.LIVE_CLOSING) {
                        //企业已禁用，60s 后关闭直播

                        LRClosingMsgAttachment msgAttachment = (LRClosingMsgAttachment) attachment;
                        String content = msgAttachment.getContent();
                        new TJMakeSureDialog(mContext, null)
                                .setTitleAndCotent("", TextUtils.isEmpty(content) ? "企业已禁用，60s后关闭会议" : content)
                                .setOkText("知道了").hideTitle().showAlert();
                    } else {
                        //不在已知自定义消息范围内 删除
                        messages.remove(i);
                    }
                }
            }

            if (messages.isEmpty()) {
                return;
            }

            //回调未读消息数
            if (mLiveCRFragmentCallback != null) {

                int length = messages.size();
                if (enterMsgCount > 0) {
                    length -= enterMsgCount;
                }
                if (length > 0
                        && (LiveRoomInfoProvider.getInstance().isCountingUnread || LiveRoomInfoProvider.getInstance().isCountingUnread2)
                        && "1".equals(mChatListType)
                        && ((BaseLiveAct) mContext).mChatMsgContainer.getVisibility() == View.GONE) {
                    mLiveCRFragmentCallback.onUnreadCallBack(length);
                }
            }

            /**
             * 判断当前的大小聊天列表状态,依此决定是否显示消息背景
             */
            int count = messages.size();
            for (int i = 0; i < count; i++) {
                ChatRoomMessage chatRoomMessage = messages.get(i);
                String senstive = "";
                Map<String, Object> msgMap = new HashMap<>();
                Map<String, Object> extension = chatRoomMessage.getLocalExtension();
                if (extension != null && !extension.isEmpty()) {
                    senstive = (String) extension.get(ChatRoomMsgViewHolderText.SENSTIVE);
                }
                if ("1".equals(mChatListType)) {
                    msgMap.put(ChatRoomMsgViewHolderText.SHOW_BG, ChatRoomMsgViewHolderText.SHOW_BG_STATE_SHOW);
                } else {
                    msgMap.put(ChatRoomMsgViewHolderText.SHOW_BG, ChatRoomMsgViewHolderText.SHOW_BG_STATE_GONE);
                }

                msgMap.put(ChatRoomMsgViewHolderText.SENSTIVE, senstive);
                chatRoomMessage.setLocalExtension(msgMap);
            }

            messageComming(messages);
        }
    };

    private void messageComming(List<ChatRoomMessage> messages) {
        getHandler().postDelayed(() -> {
            if (messageListPanel == null) {
                messageComming(messages);
            } else {
                messageListPanel.onIncomingMessage(messages, "1".equals(mChatListType));
            }
        }, 50);
    }

    /************************** Module proxy ***************************/

    @Override
    public boolean sendMessage(IMMessage msg) {
        ChatRoomMessage message = (ChatRoomMessage) msg;

        String content = message.getContent();

        boolean isPhone = StringUtil.isStringContainsPhone(content);
        boolean isHttpUrl = StringUtil.isHttpUrl(content);
        if (isPhone || isHttpUrl) {
            Toasty.normal(mContext, "您输入的内容含有敏感词！").show();
            return false;
        }

        ChatRoomHelper.buildMemberTypeInRemoteExt(message, LiveRoomInfoProvider.getInstance().imRoomId);

        //如果是文字消息，就走后台审核发送消息
        if (message.getMsgType() == MsgTypeEnum.text) {
            LiveRoomHelper.getInstance().sendMsgToSys(mContext, message, false, true);
        } else {
            NIMClient.getService(ChatRoomService.class).sendMessage(message, false)
                    .setCallback(new RequestCallback<Void>() {
                        @Override
                        public void onSuccess(Void param) {
                        }

                        @Override
                        public void onFailed(int code) {
                            if (code == ResponseCode.RES_CHATROOM_MUTED) {
                                Toast.makeText(NimUIKit.getContext(), "用户被禁言", Toast.LENGTH_SHORT).show();
                            } else if (code == ResponseCode.RES_CHATROOM_ROOM_MUTED) {
                                Toast.makeText(NimUIKit.getContext(), "全体禁言", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(NimUIKit.getContext(), "消息发送失败：code:" + code, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onException(Throwable exception) {
                            Toast.makeText(NimUIKit.getContext(), "消息发送失败！", Toast.LENGTH_SHORT).show();
                        }
                    });
        }

        messageListPanel.onMsgSend(message);
        return true;
    }

    @Override
    public void onInputPanelExpand() {
        messageListPanel.scrollToBottom();
    }

    @Override
    public void shouldCollapseInputPanel() {
        inputPanel.collapse(false);
    }

    @Override
    public void onItemFooterClick(IMMessage message) {
    }

    @Override
    public boolean isLongClickEnabled() {
        return !inputPanel.isRecording();
    }

    // 操作面板集合
    protected List<BaseAction> getActionList() {
        List<BaseAction> actions = new ArrayList<>();
        return actions;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (inputPanel != null) {
            inputPanel.onPause();
        }
        if (messageListPanel != null) {
            messageListPanel.onPause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (messageListPanel != null) {
            messageListPanel.onResume();
        }
    }

    public boolean onBackPressed() {
        if (inputPanel != null && inputPanel.collapse(true)) {
            return true;
        }

        return messageListPanel != null && messageListPanel.onBackPressed();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        registerObservers(false);

        if (messageListPanel != null) {
            messageListPanel.onDestroy();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputPanel.onActivityResult(requestCode, resultCode, data);
    }

    public void onLeave() {
        if (inputPanel != null) {
            inputPanel.collapse(false);
        }
    }

    /**
     * 传递当前用户看到的大小聊天列表 1:小  2:大
     *
     * @param type
     */
    public void setChatListType(String type) {
        this.mChatListType = type;
    }

    public RecyclerView getMessageRecyclerView() {
        return messageListPanel == null ? null : messageListPanel.getMessageRecyclerView();
    }
}

