package com.tojoy.cloud.online_meeting.App.Live.Interaction;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.tojoy.cloud.online_meeting.App.Live.MemberList.LiveRoomMemverListDialog;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;
import com.tojoy.cloud.online_meeting.App.UserCenter.adapter.LiveRoomMemberListAdapter;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.model.live.GoodsResponse;
import com.tojoy.tjoybaselib.model.user.MemberListResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.sys.AppUtils;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 直播间-成员列表
 */

@SuppressLint("ValidFragment")
public class LauchProductListFragment extends Fragment {

    private LaunchProductTJDialog liveRoomMemverListDialog;
    private String mType;
    private BaseLiveAct mContext;
    private View mView;
    private TJRecyclerView mRecyclerView;
    private List<GoodsResponse.DataBean.RecordsBean> mDatas;
    private LauchListAdapter mAdapter;
    private RelativeLayout mEmptyTip;
    private int mPage = 1;
    private SparseBooleanArray mBooleanArray;

    /**
     * @param type  1:发布商品  2:代播商品
     * @param liveRoomMemverListDialog
     */
    public LauchProductListFragment(String type, LaunchProductTJDialog liveRoomMemverListDialog) {
        this.mType = type;
        this.liveRoomMemverListDialog = liveRoomMemverListDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = (BaseLiveAct) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_live_lauch_product, container, false);
        init();
        getData();
        return mView;
    }

    private void init() {
        mEmptyTip = mView.findViewById(R.id.rlv_empty);
        mRecyclerView = mView.findViewById(R.id.memberRecycler);
        mRecyclerView.setRefreshable(false);
        mRecyclerView.setLoadMoreView();
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                getData();
            }

            @Override
            public void onLoadMore() {
                mPage++;
                getData();
            }
        });
        mDatas =new ArrayList<GoodsResponse.DataBean.RecordsBean>();
        mAdapter = new LauchListAdapter();
        mRecyclerView.setAdapter(mAdapter);
    }

    private void getData() {
        if ("1".equals(mType)) {
            LogUtil.i(LiveLogTag.BaseLiveTag, "getMemberListOwner--start");
            OMAppApiProvider.getInstance().getLiveRoomProductList(LiveRoomInfoProvider.getInstance().otherCompanyCode, null, mPage, new Observer<GoodsResponse>() {
                @Override
                public void onCompleted() {
                    mContext.dismissProgressHUD();
                }

                @Override
                public void onError(Throwable e) {
                    Toasty.normal(mContext, "网络错误，请检查网络").show();
                    mContext.showBottomView(true);
                }

                @Override
                public void onNext(GoodsResponse response) {
                    if (response.isSuccess()) {
                        if (mPage == 1) {
                            mDatas = response.data.records;
                        } else {
                            mDatas.addAll(response.data.records);
                        }
                        mAdapter.notifyDataSetChanged();
                        mRecyclerView.refreshLoadMoreView(mPage, response.data.records.size());
                        liveRoomMemverListDialog.refreshTabTitle(0, "企业商品（" + response.data.total + "）");
                    } else {
                        Toasty.normal(mContext, response.msg).show();
                    }
                    mEmptyTip.setVisibility(mDatas.isEmpty() ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            LogUtil.i(LiveLogTag.BaseLiveTag, "getUnEnterMemberList--start");
            OMAppApiProvider.getInstance().getLiveRoomAgentProductList(LiveRoomInfoProvider.getInstance().otherCompanyCode, null, mPage, new Observer<GoodsResponse>() {
                @Override
                public void onCompleted() {
                    mContext.dismissProgressHUD();
                }

                @Override
                public void onError(Throwable e) {
                    Toasty.normal(mContext, "网络错误，请检查网络").show();

                }

                @Override
                public void onNext(GoodsResponse response) {
                    if (response.isSuccess()) {
                        if (mPage == 1) {
                            mDatas = response.data.records;
                        } else {
                            mDatas.addAll(response.data.records);
                        }
                        mAdapter.notifyDataSetChanged();
                        mRecyclerView.refreshLoadMoreView(mPage, response.data.records.size());
                        liveRoomMemverListDialog.refreshTabTitle(1, "代播商品（" + response.data.total + "）");
                    } else {


                        Toasty.normal(mContext, response.msg).show();
                    }
                    mEmptyTip.setVisibility(mDatas.isEmpty() ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    public void clearSelect(){
        mAdapter.clearItemChecked(mAdapter.getmLastCheckedPosition());

    }
    /**
     * 发布商品列表适配器
     */
    private class LauchListAdapter extends RecyclerView.Adapter {


        private int mLastCheckedPosition = -1;

        public LauchListAdapter() {
            mBooleanArray = new SparseBooleanArray();
        }

        public void setItemChecked(int position) {
            mBooleanArray.put(position, true);
            if (mLastCheckedPosition > -1) {
                mBooleanArray.put(mLastCheckedPosition, false);
                notifyItemChanged(mLastCheckedPosition);
            }
            notifyDataSetChanged();
            mLastCheckedPosition = position;
        }
        public int getmLastCheckedPosition() {
            return mLastCheckedPosition;
        }
        /**
         * 清理默认选项
         */
        public void clearItemChecked(int position) {
            mBooleanArray.put(position, false);
            notifyItemChanged(mLastCheckedPosition);
            mLastCheckedPosition = -1;

        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_deal_item, parent, false);
            return new LauchListVH(itemView);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            LauchListVH vh = (LauchListVH) holder;
            vh.bindData(position);
        }

        @Override
        public int getItemCount() {
            return mDatas == null ? 0 : mDatas.size();
        }
        private class LauchListVH extends RecyclerView.ViewHolder {

            private CheckBox checkbox;
            private ImageView ivCover;
            private TextView tvInfo;
            private View item;

            LauchListVH(View itemView) {
                super(itemView);
                checkbox = itemView.findViewById(R.id.checkbox);
                tvInfo = itemView.findViewById(R.id.tvInfo);
                ivCover = itemView.findViewById(R.id.ivCover);
                item = itemView;
            }

            void bindData(int position) {
                GoodsResponse.DataBean.RecordsBean bean = mDatas.get(position);
                checkbox.setChecked(mBooleanArray.get(position));
                tvInfo.setText(bean.name);
                ImageLoaderManager.INSTANCE.loadCoverSizedImage(mContext, ivCover, OSSConfig.getOSSURLedStr(bean.liveRoomCoverImgUrl), 0, AppUtils.dip2px(mContext, 116), AppUtils.dip2px(mContext, 116), 0);
                item.setOnClickListener(view -> {
                    //被选中项 == 当前项 则返回
                    if (mLastCheckedPosition == position) {
                        return;
                    }
                    setItemChecked(position);
                    liveRoomMemverListDialog.setSelectBean(bean);
                    if ("1".equals(mType)) {
                        //当前是发布商品列表，则清理代发商品列表被选中
                        liveRoomMemverListDialog.clearOtherSelectStatus(1);
                    }
                    else{
                        liveRoomMemverListDialog.clearOtherSelectStatus(0);
                    }

                });
            }
        }

    }

}
