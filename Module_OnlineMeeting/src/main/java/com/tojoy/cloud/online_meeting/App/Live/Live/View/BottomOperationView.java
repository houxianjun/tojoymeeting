package com.tojoy.cloud.online_meeting.App.Live.Live.View;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.netease.nim.uikit.business.liveroom.LiveRoomHelper;
import com.tojoy.cloud.online_meeting.App.Live.Interaction.LaunchProductTJDialog;
import com.tojoy.cloud.online_meeting.App.Live.MemberList.LiveRoomMemverListDialog;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.enterprise.CorporationMsgResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigCacheManager;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigConstantKey;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.cloud.online_meeting.App.Controller.LRDialogController;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.util.sys.KeyboardControlUtil;

import es.dmoral.toasty.Toasty;

public class BottomOperationView {

    private Context mContext;
    private View mView;

    public View mUnreadLayout;
    public TextView mTvUnread;
    private View mAddMark;
    private TextView mTvDuration;
    private View mIvInteraction;
    private TextView mIvProductList;
    private TextView mTvMemberList;
    private TextView mTvMessage;

    private View flUnreadLayout;
    private TextView mTvShare;
    private TextView mTvCompany;

    private OperationCallback mBottomOperationCallback;

    private final int TIMER = 9999;
    private int mSeconds = 0;
    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == TIMER) {
                mSeconds++;
                mTvDuration.setText(getTimeStr(mSeconds));
                if (mHandler == null) {
                    return;
                }
                mHandler.removeMessages(TIMER);
                mHandler.sendEmptyMessageDelayed(TIMER, 1000);
            }
        }
    };
    //互动的小红点
    private View mInteractionRedDot;

    public BottomOperationView(Context context, OperationCallback bottomOperationCallback, TextView tv) {
        mContext = context;
        mBottomOperationCallback = bottomOperationCallback;
        mSeconds = LiveRoomInfoProvider.getInstance().liveDuration > 0 ? (int) (LiveRoomInfoProvider.getInstance().liveDuration / 1000) : 0;

        mTvDuration = tv;
        mTvDuration.setVisibility(View.VISIBLE);
    }

    public View getView() {
        if (mView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mView = aLayoutInflater.inflate(R.layout.view_live_bottom_opview, null);
        }
        initView();
        return mView;
    }

    private void initView() {

        mUnreadLayout = mView.findViewById(R.id.unreadLayout);
        mTvUnread = mView.findViewById(R.id.tvUnread);
        mAddMark = mView.findViewById(R.id.addMark);
        mIvInteraction = mView.findViewById(R.id.ivInteraction);
        mInteractionRedDot = mView.findViewById(R.id.interactionRedDot);
        mIvProductList = mView.findViewById(R.id.tv_wares);
        mTvMemberList = mView.findViewById(R.id.tv_memberList);
        mTvMessage = mView.findViewById(R.id.tv_message);
        flUnreadLayout = mView.findViewById(R.id.flUnreadLayout);
        mTvShare = mView.findViewById(R.id.tv_share);
        mTvCompany = mView.findViewById(R.id.tv_companyhome);

        mTvMessage.setVisibility(View.VISIBLE);
        flUnreadLayout.setVisibility(View.VISIBLE);


        mTvMemberList.setText(TextUtils.isEmpty(TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.MEETING_BAR3)) ?
                "观众" : TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.MEETING_BAR3));

        mTvMessage.setText(TextUtils.isEmpty(TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.MEETIGN_BAR1)) ?
                "评论" : TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.MEETIGN_BAR1));

        mIvProductList.setText(TextUtils.isEmpty(TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.MEETING_BAR5)) ?
                "商品" : TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.MEETING_BAR5));

        LiveRoomHelper.getInstance().productListBtn = mIvProductList;


        mHandler.removeMessages(TIMER);
        mHandler.sendEmptyMessageDelayed(TIMER, 1000);
        mIvInteraction.setOnClickListener(v -> {
            mBottomOperationCallback.showOperationView();
            mInteractionRedDot.setVisibility(View.GONE);
        });

        if (!TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().isHideShare) && "0".equals(LiveRoomInfoProvider.getInstance().isHideShare)) {
            // 私密会议，要隐藏的
            mTvShare.setVisibility(View.GONE);
        } else if("1".equals(LiveRoomInfoProvider.getInstance().isHideShare)) {
            // 非私密会议，要显示
            mTvShare.setVisibility(View.VISIBLE);
            mTvShare.setText(TextUtils.isEmpty(TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.MEETING_BAR2)) ?
                    "分享" : TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.MEETING_BAR2));
        }

        mTvCompany.setText(TextUtils.isEmpty(TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.MEETING_BAR4)) ?
                "企业" : TextConfigCacheManager.getInstance(mContext).getWidgetTextByKey(TextConfigConstantKey.MEETING_BAR4));

        //分享
        mView.findViewById(R.id.tv_share).setOnClickListener(view -> {
            mBottomOperationCallback.showShareView();
        });

        if (!TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().otherCompanyCode)) {
            OMAppApiProvider.getInstance().mCorporationQuery(LiveRoomInfoProvider.getInstance().otherCompanyCode, false, new rx.Observer<CorporationMsgResponse>() {
                @Override
                public void onCompleted() {
                }

                @Override
                public void onError(Throwable e) {
                }

                @Override
                public void onNext(CorporationMsgResponse response) {
                    if (response.isSuccess()) {
                        if (response.data.customState == 1) {
                            mTvCompany.setVisibility(View.VISIBLE);
                        }
                    } else {
                        if (!TextUtils.isEmpty(response.code) && "-2".equals(response.code)) {
                            Toasty.normal(mContext, response.msg).show();
                        }
                    }
                }
            });
        }

        //企业主页
        mTvCompany.setOnClickListener(view -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }

            ARouter.getInstance().build(RouterPathProvider.OnlineMeetingCustomizedEnterprisePageAct)
                    .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    .withString("companyCode", LiveRoomInfoProvider.getInstance().otherCompanyCode)
                    .withString("isFromLive", "1")
                    .withString("roomLiveId",LiveRoomInfoProvider.getInstance().liveId)
                    .withString("roomId",LiveRoomInfoProvider.getInstance().roomId)
                    .withString("sourceWay","0")
                    .navigation();
        });


        LRDialogController.getInstance().getProjectTJData(true, null, mIvProductList);
        //商品列表
        mIvProductList.setOnClickListener(view -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }
            showCenterListRedDot(false);
            LRDialogController.getInstance().showProjectList((BaseLiveAct) mContext, mIvProductList);

        });

        mTvMessage.setOnClickListener(v -> {
            mUnreadLayout.setVisibility(View.GONE);
            mTvUnread.setText("0");
            mBottomOperationCallback.showChatRoomMessage();
            mBottomOperationCallback.goneUnreadCount();
        });

        mTvMemberList.setOnClickListener((View v) -> {
            mBottomOperationCallback.showMemberList();
        });
    }



    public void showSelf(boolean show) {
        if (show) {
            mView.setVisibility(View.VISIBLE);
        } else {
            mView.setVisibility(View.GONE);
        }
    }


    public void refreshUnreadInfo(int addCount) {
        if (addCount == -1) {
            if (mUnreadLayout.getVisibility() != View.GONE) {
                mUnreadLayout.setVisibility(View.GONE);
            }
            mTvUnread.setText("0");
        } else if (mUnreadLayout.getVisibility() == View.GONE) {
            //首次接到未读
            mUnreadLayout.setVisibility(View.VISIBLE);
            mTvUnread.setText(addCount + "");
        } else {
            int unread = Integer.parseInt(mTvUnread.getText().toString());
            unread += addCount;
            if (unread <= 99) {
                mTvUnread.setText(unread + "");
                mAddMark.setVisibility(View.GONE);
            } else {
                mTvUnread.setText("99");
                mAddMark.setVisibility(View.VISIBLE);
            }
        }
    }

    private String getTimeStr(int duration) {

        int h = duration / 3600;
        int leftSecond = duration % 3600;
        int m = leftSecond / 60;
        int s = leftSecond % 60;

        String hour, min, second;
        if (h < 10) {
            hour = "0" + h;
        } else {
            hour = "" + h;
        }
        if (m < 10) {
            min = "0" + m;
        } else {
            min = "" + m;
        }
        if (s < 10) {
            second = "0" + s;
        } else {
            second = "" + s;
        }

        if ("00".equals(hour)) {
            return min + ":" + second;
        } else {
            return hour + ":" + min + ":" + second;
        }
    }

    public String getLiveDuration() {

        String s = mTvDuration.getText().toString();
        if (s.length() == 5) {
            s = "00:" + s;
        }
        return s;
    }

    public void destory() {
        if (mHandler != null) {
            mHandler = null;
        }
    }

    public int[] getInteractionIconLocation() {
        int[] location = new int[2];
        mIvInteraction.getLocationOnScreen(location);
        return location;
    }

    public View getRedDot() {
        return mInteractionRedDot;
    }

    public int[] getCenterListIconLocation() {
        int[] location = new int[2];
        mIvProductList.getLocationOnScreen(location);
        return location;
    }

    public void showCenterListRedDot(boolean is) {
        mView.findViewById(R.id.centerListRedDot).setVisibility(is ? View.VISIBLE : View.GONE);
    }
}
