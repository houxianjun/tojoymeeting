package com.tojoy.cloud.online_meeting.App.Meeting.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;

/**
 * @author fanxi
 * @date 2020-02-28.
 * description：
 */
public class MeetingPageAdapter extends BaseRecyclerViewAdapter<String> {

    public MeetingPageAdapter(@NonNull Context context, @NonNull List<String> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull String data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_enterprise_goods_layouot;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new MeetingPageHolder(context, getLayoutResId(viewType));
    }

    public static class MeetingPageHolder extends BaseRecyclerViewHolder<String> {

        public MeetingPageHolder(Context context, int layoutResId) {
            super(context, layoutResId);
        }

        @Override
        public void onBindData(String data, int position) {

        }
    }
}
