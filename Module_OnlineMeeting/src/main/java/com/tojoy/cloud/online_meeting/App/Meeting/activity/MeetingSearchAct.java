package com.tojoy.cloud.online_meeting.App.Meeting.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.ui.activity.BaseSearchAct;
import com.tojoy.common.App.BaseFragment;

@Route(path = RouterPathProvider.MeetingSearchAct)
public class MeetingSearchAct extends BaseSearchAct {
    MeetingSearchFragment mMeetingSearchFragment;

    private BaseFragment[] mFragments;

    public static void start(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, MeetingSearchAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preSearch();
    }

    @Override
    protected void initUI() {
        super.initUI();
        setSearchHint("请输入会议ID或标题");
        hideBottomLine();
    }

    private void initViewPager() {
        mFragments = new BaseFragment[1];
        mMeetingSearchFragment = new MeetingSearchFragment();
        mFragments[0] = mMeetingSearchFragment;

        FragmentPagerAdapter mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments[position];
            }

            @Override
            public int getCount() {
                return mFragments.length;
            }
        };
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(1);
        mViewPager.setCurrentItem(0);
    }

    @Override
    protected void doSearch() {
        super.doSearch();
        if (mMeetingSearchFragment == null) {
            initViewPager();
        }
        String searchKey = mSeachEditView.getText().toString().trim();
        ((MeetingSearchFragment) mFragments[0]).doSearch(searchKey);

    }

    @Override
    protected void stopSearch() {
        finish();
    }

}
