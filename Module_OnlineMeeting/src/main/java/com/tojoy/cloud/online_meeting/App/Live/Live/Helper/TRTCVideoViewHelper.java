package com.tojoy.cloud.online_meeting.App.Live.Live.Helper;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tencent.rtmp.ITXLivePlayListener;
import com.tencent.rtmp.TXLiveConstants;
import com.tencent.rtmp.TXLivePlayConfig;
import com.tencent.rtmp.ui.TXCloudVideoView;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.cloud.online_meeting.App.Live.Live.Model.OnlineMember;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;

import java.util.ArrayList;
import java.util.List;

import static com.tencent.rtmp.TXLiveConstants.EVT_PARAM1;
import static com.tencent.rtmp.TXLiveConstants.EVT_PARAM2;
import static com.tencent.rtmp.TXLiveConstants.PLAY_EVT_CHANGE_RESOLUTION;
import static com.tencent.rtmp.TXLiveConstants.PLAY_EVT_RCV_FIRST_I_FRAME;
import static com.tencent.rtmp.TXLiveConstants.RENDER_MODE_ADJUST_RESOLUTION;
import static com.tencent.rtmp.TXLiveConstants.RENDER_MODE_FULL_FILL_SCREEN;

/**
 * 主要处理TRTCVideoViewLayout的一些TXLivePlayer封装
 */
public class TRTCVideoViewHelper {
    private Context mContext;

    private final static int RetryCount = 12;
    private final static int RetryInterval = 5;

    LiveRoomPlayer mLivePlayer1;
    LiveRoomPlayer mLivePlayer2;
    LiveRoomPlayer mLivePlayer3;
    LiveRoomPlayer mLivePlayer4;
    LiveRoomPlayer mLivePlayer5;
    LiveRoomPlayer mLivePlayer6;
    LiveRoomPlayer mLivePlayer7;
    LiveRoomPlayer mLivePlayer8;
    LiveRoomPlayer mLivePlayer9;

    public TRTCVideoViewHelper(Context context) {
        super();
        mContext = context;
    }

    public LiveRoomPlayer getmLivePlayerByUserId(String userId) {
        if (mLivePlayer1 != null && userId.equals(mLivePlayer1.userId)) {
            return mLivePlayer1;
        } else if (mLivePlayer2 != null && userId.equals(mLivePlayer2.userId)) {
            return mLivePlayer2;
        } else if (mLivePlayer3 != null && userId.equals(mLivePlayer3.userId)) {
            return mLivePlayer3;
        } else if (mLivePlayer4 != null && userId.equals(mLivePlayer4.userId)) {
            return mLivePlayer4;
        } else if (mLivePlayer5 != null && userId.equals(mLivePlayer5.userId)) {
            return mLivePlayer5;
        }else if (mLivePlayer6 != null && userId.equals(mLivePlayer6.userId)) {
            return mLivePlayer6;
        } else if (mLivePlayer7 != null && userId.equals(mLivePlayer7.userId)) {
            return mLivePlayer7;
        }else if (mLivePlayer8 != null && userId.equals(mLivePlayer8.userId)) {
            return mLivePlayer8;
        } else if (mLivePlayer9 != null && userId.equals(mLivePlayer9.userId)) {
            return mLivePlayer9;
        } else {
            return null;
        }
    }

    public LiveRoomPlayer getTXLivePlayer(int index,String userId) {
        switch (index) {
            case 0:
                if (mLivePlayer1 == null) {
                    mLivePlayer1 = new LiveRoomPlayer(mContext);
                    if (!TextUtils.isEmpty(userId)) {
                        mLivePlayer1.userId = userId;
                    }
                    configTxLivePlayer(mLivePlayer1);
                }
                return mLivePlayer1;
            case 1:
                if (mLivePlayer2 == null) {
                    mLivePlayer2 = new LiveRoomPlayer(mContext);
                    if (!TextUtils.isEmpty(userId)){
                        mLivePlayer2.userId = userId;

                    }
                    configTxLivePlayer(mLivePlayer2);
                }
                return mLivePlayer2;
            case 2:
                if (mLivePlayer3 == null) {
                    mLivePlayer3 = new LiveRoomPlayer(mContext);
                    if (!TextUtils.isEmpty(userId)){
                        mLivePlayer3.userId = userId;
                    }
                    configTxLivePlayer(mLivePlayer3);
                }
                return mLivePlayer3;
            case 3:
                if (mLivePlayer4 == null) {
                    mLivePlayer4 = new LiveRoomPlayer(mContext);
                    if (!TextUtils.isEmpty(userId)){
                        mLivePlayer4.userId = userId;
                    }
                    configTxLivePlayer(mLivePlayer4);
                }
                return mLivePlayer4;
            case 4:
                if (mLivePlayer5 == null) {
                    mLivePlayer5 = new LiveRoomPlayer(mContext);
                    if (!TextUtils.isEmpty(userId)){
                        mLivePlayer5.userId = userId;
                    }
                    configTxLivePlayer(mLivePlayer5);
                }
                return mLivePlayer5;
            case 5:
                if (mLivePlayer6 == null) {
                    mLivePlayer6 = new LiveRoomPlayer(mContext);
                    if (!TextUtils.isEmpty(userId)){
                        mLivePlayer6.userId = userId;
                    }
                    configTxLivePlayer(mLivePlayer6);
                }
                return mLivePlayer6;
            case 6:
                if (mLivePlayer7 == null) {
                    mLivePlayer7 = new LiveRoomPlayer(mContext);
                    if (!TextUtils.isEmpty(userId)){
                        mLivePlayer7.userId = userId;
                    }
                    configTxLivePlayer(mLivePlayer7);
                }
                return mLivePlayer7;
            case 7:
                if (mLivePlayer8 == null) {
                    mLivePlayer8 = new LiveRoomPlayer(mContext);
                    if (!TextUtils.isEmpty(userId)){
                        mLivePlayer8.userId = userId;
                    }
                    configTxLivePlayer(mLivePlayer8);
                }
                return mLivePlayer8;
            default:
                if (mLivePlayer9 == null) {
                    mLivePlayer9 = new LiveRoomPlayer(mContext);
                    if (!TextUtils.isEmpty(userId)){
                        mLivePlayer9.userId = userId;
                    }
                    configTxLivePlayer(mLivePlayer9);
                }
                return mLivePlayer9;
        }
    }



    public void configTxLivePlayer(LiveRoomPlayer livePlayer) {
        TXLivePlayConfig config = new TXLivePlayConfig();
        config.setEnableMessage(false);
        config.setConnectRetryCount(RetryCount);
        config.setConnectRetryInterval(RetryInterval);
        config.enableAEC(false);
        config.setAutoAdjustCacheTime(true);
        config.setMinAutoAdjustCacheTime(1);
        config.setMaxAutoAdjustCacheTime(1);
        livePlayer.setConfig(config);
        livePlayer.setRenderRotation(TXLiveConstants.RENDER_ROTATION_PORTRAIT);
        livePlayer.enableHardwareDecode(true);
        if (!TextUtils.isEmpty(livePlayer.userId) && (livePlayer.userId.length() > 10 || (LiveRoomInfoProvider.getInstance().isOfficalLive() && LiveRoomInfoProvider.getInstance().isHost(livePlayer.userId)))) {
            livePlayer.setRenderMode(RENDER_MODE_ADJUST_RESOLUTION);
        } else {
            livePlayer.setRenderMode(RENDER_MODE_FULL_FILL_SCREEN);
        }
        livePlayer.setPlayListener(new ITXLivePlayListener() {
            @Override
            public void onPlayEvent(int i, Bundle bundle) {
                if (i == PLAY_EVT_RCV_FIRST_I_FRAME || i == PLAY_EVT_CHANGE_RESOLUTION) {
                    if (!TextUtils.isEmpty(livePlayer.userId) &&
                            (livePlayer.userId.length() > 10 ||
                            (LiveRoomInfoProvider.getInstance().isHost(livePlayer.userId) && LiveRoomInfoProvider.getInstance().isOfficalLive()))) {
                        changeRenderMode(livePlayer, bundle);
                    }
                }
            }

            @Override
            public void onNetStatus(Bundle bundle) {
            }
        });
    }

    /**
     * * 若为引流：
     * 小窗口：
     *      * width > heigh :等比例显示有黑边（横着的流）
     *      * width < heigh :铺满，宽度合适，高度会显示中间那部分（竖着的流）
     * 切大屏幕：无论什么样式均不能有黑边，即只能铺满展示会有裁剪
     * @param player
     * @param bundle
     */
    private void changeRenderMode(LiveRoomPlayer player, Bundle bundle) {
        int width = bundle.getInt(EVT_PARAM1);
        int height = bundle.getInt(EVT_PARAM2);

        OnlineMember onlineMember = OnlineMembersHelper.getInstance().getMemberByUserId(player.userId);
        if (onlineMember != null){
            onlineMember.width = width;
            onlineMember.height = height;
        }

        if ((width > height) || (onlineMember != null && onlineMember.isAtBigScreen)) {
            player.setRenderMode(RENDER_MODE_ADJUST_RESOLUTION);
//            player.isHorizontal = true;
        } else {
            if (!TextUtils.isEmpty(player.userId) && (player.userId.length() > 10 || (LiveRoomInfoProvider.getInstance().isOfficalLive() && LiveRoomInfoProvider.getInstance().isHost(player.userId)))) {
                return;
            }
            player.setRenderMode(RENDER_MODE_FULL_FILL_SCREEN);
//            player.isHorizontal = false;
        }
    }

    public void releaseTXLivePlayer(int index) {
        switch (index) {
            case 0:
                mLivePlayer1 = null;
                break;
            case 1:
                mLivePlayer2 = null;
                break;
            case 2:
                mLivePlayer3 = null;
                break;
            case 3:
                mLivePlayer4 = null;
                break;
            case 4:
                mLivePlayer5 = null;
                break;
            case 5:
                mLivePlayer6 = null;
                break;
            case 6:
                mLivePlayer7 = null;
                break;
            case 7:
                mLivePlayer8 = null;
                break;
            case 8:
                mLivePlayer9 = null;
                break;
            default:
                break;
        }
    }


    //0:没有滚动
    //1:下滑
    //2:上滑
    public void refreshViewAlpha(ArrayList<TXCloudVideoView> mVideoViewList, int state, boolean hasScrollToTop) {

        LogUtil.i(LiveLogTag.TRTCLayout, "refreshViewAlpha" + state);

        switch (state) {
            case 0:
                if (hasScrollToTop) {
                    for (TXCloudVideoView txCloudVideoView : mVideoViewList) {
                        txCloudVideoView.setAlpha(1);
                    }
                } else {
                    for (int i = 0; i < mVideoViewList.size(); i++) {
                        if (i > 4) {
                            mVideoViewList.get(i).setAlpha(0);
                        } else {
                            mVideoViewList.get(i).setAlpha(1);
                        }
                    }
                }
                break;
            case 1:
                for (int i = 0; i < mVideoViewList.size(); i++) {
                    if (i > 4) {
                        ObjectAnimator alphaAnim = ObjectAnimator.ofFloat(mVideoViewList.get(i), "alpha", 1, 0);
                        alphaAnim.setDuration(400);
                        alphaAnim.start();
                    } else {
                        mVideoViewList.get(i).setAlpha(1);
                    }
                }

                break;
            case 2:
                for (int i = 0; i < mVideoViewList.size(); i++) {
                    if (i > 4) {
                        ObjectAnimator alphaAnim = ObjectAnimator.ofFloat(mVideoViewList.get(i), "alpha", 0, 1);
                        alphaAnim.setDuration(400);
                        alphaAnim.start();
                    } else {
                        mVideoViewList.get(i).setAlpha(1);
                    }
                }
                break;
            default:
                break;
        }
    }
}
