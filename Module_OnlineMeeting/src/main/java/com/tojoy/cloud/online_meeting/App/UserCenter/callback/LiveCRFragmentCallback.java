package com.tojoy.cloud.online_meeting.App.UserCenter.callback;

import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishDistributionAttachment;
import com.netease.nim.uikit.business.chatroom.extension.CRMsgLivePublishQRAttachment;
import com.tojoy.tjoybaselib.model.live.SelectOutLiveModel;

/**
 * Created by luuzhu on 2019/4/26.
 * 直播间里的聊天室fragment发给上级activity的回调
 */

public interface LiveCRFragmentCallback {

    /**
     * 未读数回调
     *
     * @param addCount 新增的未读数
     */
    void onUnreadCallBack(int addCount);

    /**
     * 被踢出聊天室回调
     */
    void onKickedOutCallback();

    /**
     * 聊天室公告更新
     */
    void onNoticeUpdateCallback();

    /**
     * 结束直播
     */
    void onEndingLive();

    /**
     * 关闭某个引流
     */
    void stopSomeLive(String streamId, String roomTitle);

    /**
     * 关闭某个引流
     */
    void enterpriseForbidLive();

    /**
     * 收到直播间自定义消息
     */
    void onReceiveLiveRoomCustomMessage(String message);

    /**
     * 用户进入直播间
     */
    void onMemberJoin(String name);

    /**
     * 进入直播间成功 目前用于刷新全员禁言按钮
     */
    void onEnterChatRoom();

    /**
     * 收到引流直播间的流增加
     */
    void onOutliveRoomLiveAdd(SelectOutLiveModel selectOutLiveModel);

    /**
     * ***点击感兴趣，***要合作项目，***抢占项目区域
     * @param info
     */
    void onReceiveGuestTips(String info);

    /**
     * 直播间发布/隐藏了活动，刷新活动弹窗和轮播图
     */
    void onQRDataRefresh(CRMsgLivePublishQRAttachment attachment);

    /**
     * 直播间（本场直播被设置为可推广会议）
     */
    void onDistributionCallBack(CRMsgLivePublishDistributionAttachment attachment);
}
