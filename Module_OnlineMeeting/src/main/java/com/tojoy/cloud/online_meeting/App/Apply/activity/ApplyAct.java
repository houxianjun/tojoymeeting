package com.tojoy.cloud.online_meeting.App.Apply.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.CallSuper;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.sdk.android.oss.internal.OSSAsyncTask;
import com.tojoy.tjoybaselib.model.meetingperson.AttendeePersonModel;
import com.tojoy.tjoybaselib.model.meetingperson.MeetingPersonDepartmentResponse;
import com.tojoy.tjoybaselib.model.meetingperson.SingleTonModel.MeetingPersonTon;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.oss.OSSService;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.util.media.ImageUtil;
import com.tojoy.cloud.online_meeting.App.Apply.fragment.ApplyDetailFragment;
import com.tojoy.cloud.online_meeting.App.Apply.fragment.ApplyEditFragment;
import com.tojoy.cloud.online_meeting.App.Apply.fragment.ApplyResultFragment;
import com.tojoy.tjoybaselib.services.OnlineMeeting.ApplyStateCode;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApplyJoinLiveModel;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApplyJoinLiveResponse;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApplyPhotoInfo;
import com.tojoy.tjoybaselib.model.OnlineMeeting.CheckUserInfoResponse;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.App.BaseFragment;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.image.compressor.UpLoadImageBaseAct;
import com.tojoy.image.photobrowser.PhotoBrowseActivity;
import com.tojoy.image.photobrowser.PhotoBrowseInfo;
import com.tojoy.image.photopicker.Extras;
import com.tojoy.image.photopicker.model.PhotoInfo;
import com.tojoy.image.photopicker.model.PickerContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * @author qululu
 * 开会申请页面（开会申请编辑、开会申请结果公用同一个页面）
 */
@Route(path = RouterPathProvider.ONLINE_MEETING_START_LIVE_BROADCAST_APPLY)
public class ApplyAct extends UpLoadImageBaseAct {
    // 只展示
    public static final String TYPE_SHOW = "show";
    // 可编辑
    public static final String TYPE_EDIT = "edit";
    // 房间名称
    public static final int REQUEST_ROOM_NAME = 1001;
    // 房间封面
    public static final int REQUEST_PICK_ICON = 1003;
    // 房间文档--图片
    public static final int REQUEST_PICK_FILE_IMAGE = 1004;
    // 房间文档视频
    public static final int REQUEST_PICK_FILE_Video = 1005;
    // 邀请参会人员
    public static final int REQUEST_ROOM_INVITATION_TYPE = 1009;

    BaseFragment mFragment;
    // 要显示的类型：可编辑、纯展示
    private String mType;
    // 跳转来源
    private String source;
    private ApplyJoinLiveModel joinLiveModel;
    // 标识是否触发跳转结果页面了
    private boolean isJumpResultOver = false;
    // 是否正在提交申请中
    private boolean isSubmitLiveApplyIng = false;
    // 默认重置按钮是可点击的
    private boolean isCanClickRightManagerTitle = true;

    // 余额不许提醒，只在开播申请编辑页面、开播申请结果页面显示
    private RelativeLayout mServiceHintRlv;
    private TextView mServiceHintTv;


    public HashMap<String, String> tmpHamapA = new HashMap<>();

    //从参会人页面是返回键还是确定键
    public String isBackFromMeetingPerson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_live_broadcast_apply);
        mType = getIntent().getStringExtra("type");
        source = getIntent().getStringExtra("source");
        joinLiveModel = (ApplyJoinLiveModel) getIntent().getSerializableExtra("joinLiveModel");
        setStatusBar();
        initTitle();
        initUI();
        initOss();
    }

    private void initTitle() {
        setUpNavigationBar();
        showBack();
        if (!TextUtils.isEmpty(mType) && TYPE_SHOW.equals(mType)) {
            setTitle("申请处理");
        } else {
            setTitle("开会申请");
        }
    }

    private void initUI() {

        //清空开播外部联系人集合
        MeetingPersonTon.getInstance().clear();

        mServiceHintRlv = (RelativeLayout) findViewById(R.id.rlv_service_hint);
        mServiceHintTv = (TextView) findViewById(R.id.tv_service_hint);
        if (!TextUtils.isEmpty(source) && "menu".equals(source)) {
            // 是首页菜单进来的，需先请求接口，根据状态跳转页面
            requestJoinLiveApply(false);
        } else {
            // 从结果页进入，需重新申请或查看详情
            requestApplyDetailOrAgainApply();
            mServiceHintRlv.setVisibility(View.GONE);
        }
    }

    /**
     * 请求可编辑状态下的开会申请页面参数（菜单进入）
     */
    public void requestJoinLiveApply(boolean isRefresh) {
        showProgressHUD(this, "请稍后...");
        OMAppApiProvider.getInstance().joinLiveApply(new Observer<ApplyJoinLiveResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {
                dismissProgressHUD();
                Toasty.normal(ApplyAct.this, "网络异常，请稍后再试").show();
                mServiceHintRlv.setVisibility(View.GONE);
            }

            @Override
            public void onNext(ApplyJoinLiveResponse baseResponse) {
                if (baseResponse.isSuccess()) {
                    if (isRefresh) {
                        ((ApplyResultFragment) mFragment).setApplyResultInfo(baseResponse.data);
                    } else {
                        addFragment(baseResponse.data);
                    }

                    if (!TextUtils.isEmpty(baseResponse.data.remainingTimeNotify)) {
                        mServiceHintRlv.setVisibility(View.VISIBLE);
                        mServiceHintTv.setText(TextUtils.isEmpty(baseResponse.data.remainingTimeNotify) ?
                                getResources().getString(R.string.apply_online_service_hint) : baseResponse.data.remainingTimeNotify);
                    }
                } else {
                    Toasty.normal(ApplyAct.this, baseResponse.msg).show();
                    mServiceHintRlv.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * 由结果页面进来的，需要重新申请或查看详情
     */
    public void requestApplyDetailOrAgainApply() {
        showProgressHUD(this, "");
        OMAppApiProvider.getInstance().getApplyInfoDetail(new Observer<ApplyJoinLiveResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {
                dismissProgressHUD();
                Toasty.normal(ApplyAct.this, "网络异常，请稍后再试！").show();
            }

            @Override
            public void onNext(ApplyJoinLiveResponse applyJoinLiveResponse) {
                if (applyJoinLiveResponse.isSuccess()) {
                    addFragment(applyJoinLiveResponse.data);
                } else {
                    Toasty.normal(ApplyAct.this, applyJoinLiveResponse.msg).show();
                }
            }
        }, joinLiveModel.applyId);
    }

    private void addFragment(ApplyJoinLiveModel applyJoinLiveModel) {
        // 请求结果后根据来源判断显示哪种状态页面
        if (!TextUtils.isEmpty(mType) && TYPE_SHOW.equals(mType)) {
            // 查看详情
            mFragment = new ApplyDetailFragment().setApplyStateCode(applyJoinLiveModel);
        } else if (!TextUtils.isEmpty(mType) && TYPE_EDIT.equals(mType)) {
            // 重新申请（重新申请时需手动请求一下接口获取当前设置的观看人数审批人）
            mFragment = new ApplyEditFragment().setApplyStateCode(applyJoinLiveModel);
            setRightManagerTitle("重置");
            if (!TextUtils.isEmpty(applyJoinLiveModel.remainingTimeNotify)) {
                mServiceHintRlv.setVisibility(View.VISIBLE);
                mServiceHintTv.setText(TextUtils.isEmpty(applyJoinLiveModel.remainingTimeNotify) ?
                        getResources().getString(R.string.apply_online_service_hint) : applyJoinLiveModel.remainingTimeNotify);
            } else {
                mServiceHintRlv.setVisibility(View.GONE);
            }
        } else if (applyJoinLiveModel.getApplyStatus() == ApplyStateCode.ApplySuccess
                || applyJoinLiveModel.getApplyStatus() == ApplyStateCode.ApplyReview
                || applyJoinLiveModel.getApplyStatus() == ApplyStateCode.ApplyError
                || applyJoinLiveModel.getApplyStatus() == ApplyStateCode.ApplyReCall) {
            // 审核结果页
            mFragment = new ApplyResultFragment().setApplyStateCode(applyJoinLiveModel.getApplyStatus(), applyJoinLiveModel);
        } else {
            // 开会申请编辑页面
            mFragment = new ApplyEditFragment().setApplyStateCode(applyJoinLiveModel);
            setRightManagerTitle("重置");
        }

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        if (mFragment != null) {
            transaction.add(R.id.flv_root, mFragment).commit();
        }
    }

    /**
     * 根据所选人数判断显示对应审批人
     */
    public void getCheckUser(String lookCount) {
        showProgressHUD(this, "");
        OMAppApiProvider.getInstance().mGetCheckUser(new Observer<CheckUserInfoResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {
                dismissProgressHUD();
                Toasty.normal(ApplyAct.this, "网络异常，请稍后再试").show();
            }

            @Override
            public void onNext(CheckUserInfoResponse baseResponse) {
                if (baseResponse != null && baseResponse.data != null && baseResponse.isSuccess()) {
                    if (mFragment != null) {
                        ((ApplyEditFragment) mFragment).setCheckUser(baseResponse.data.name);
                    }
                } else if (baseResponse != null) {
                    Toasty.normal(ApplyAct.this, baseResponse.msg).show();
                }
            }
        }, lookCount);
    }

    /**
     * 提交开会申请接口
     */
    public void submitLiveApply(String roomName, String roomPassword, String viewNum, String playReason, String tagIds, String docUrl, String picUrl,
                                String authType, String drainageType, String docName, String userIds, String mobiles, String isPlayback, String outMobiles, String outNames, String nameIds) {
        if (isSubmitLiveApplyIng) {
            return;
        }
        showProgressHUD(this, "正在提交申请，请稍后！");
        isSubmitLiveApplyIng = true;
        OMAppApiProvider.getInstance().mSubmitLiveApply(new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
                isSubmitLiveApplyIng = false;
            }

            @Override
            public void onError(Throwable e) {
                dismissProgressHUD();
                isSubmitLiveApplyIng = false;
                mHandler.postDelayed(() -> {
                    Toasty.normal(ApplyAct.this, "网络异常，请稍后！").show();
                }, 500);
            }

            @Override
            public void onNext(OMBaseResponse baseResponse) {
                if (baseResponse.isSuccess()) {
                    Toasty.normal(ApplyAct.this, "提交成功！").show();
                    dismissProgressHUD();
                    //清空开播外部联系人集合
                    MeetingPersonTon.getInstance().clear();
                    if (!TextUtils.isEmpty(mType) && TYPE_EDIT.equals(mType)) {
                        mHandler.postDelayed(() -> finish(), 500);
                    } else {
                        // 退出本页面再进入
                        if (!isJumpResultOver) {
                            // 若已经触发跳转结果页面不需再次跳转
                            ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_START_LIVE_BROADCAST_APPLY)
                                    .withString("source", "menu")
                                    .navigation();
                            isJumpResultOver = true;
                            finish();
                        }
                    }
                } else {
                    Toasty.normal(ApplyAct.this, baseResponse.msg).show();
                }
            }
        }, roomName, roomPassword, viewNum, playReason, tagIds, docUrl, picUrl, authType, drainageType, docName, userIds, mobiles,isPlayback,outMobiles,outNames,nameIds);
    }

    /**
     * 撤回申请
     */
    public void reCallApply(String applyId) {
        showProgressHUD(this, "");
        OMAppApiProvider.getInstance().mRevertLiveApply(new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {
                dismissProgressHUD();
                Toasty.normal(ApplyAct.this, "网络异常，请稍后！").show();
            }

            @Override
            public void onNext(OMBaseResponse baseResponse) {
                if (baseResponse.isSuccess()) {
                    ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_START_LIVE_BROADCAST_APPLY)
                            .withString("source", "menu")
                            .navigation();
                    finish();
                } else if ("-23".equals(baseResponse.code)) {
                    new TJMakeSureDialog(ApplyAct.this, "您的开会申请已提交！\n请等待回复！",
                            view -> {
                                // 刷新
                                requestJoinLiveApply(true);
                            }).showAlert();
                } else {
                    Toasty.normal(ApplyAct.this, "撤回失败").show();
                    // 刷新
                    requestJoinLiveApply(true);
                }
            }
        }, applyId);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case REQUEST_PICK_ICON:
                // 上传封面
                List<PhotoInfo> coverPhotos = new ArrayList<>();
                boolean coverLocal = data.getBooleanExtra(Extras.EXTRA_FROM_LOCAL, false);
                if (coverLocal) {
                    // 本地相册
                    coverPhotos = PickerContract.getPhotos(data);
                    if (coverPhotos == null) {
                        return;
                    }
                } else {
                    // 拍照
                    String photoPath = data.getStringExtra(Extras.EXTRA_FILE_PATH);
                    PhotoInfo photoInfo = new PhotoInfo();
                    photoInfo.setFilePath(photoPath);
                    coverPhotos.add(photoInfo);
                }
                List<ApplyPhotoInfo> infos = new ArrayList<>();
                for (int i = 0; i < coverPhotos.size(); i++) {
                    ApplyPhotoInfo info = new ApplyPhotoInfo();
                    info.setFilePath(coverPhotos.get(i).getAbsolutePath());
                    info.setType("image");
                    info.ossUrl = "";
                    infos.add(info);
                }
                if (mFragment != null) {
                    ((ApplyEditFragment) mFragment).addCoverPhotoView(infos);
                }
                break;
            case REQUEST_PICK_FILE_IMAGE:
                // 上传文件--图片
                List<PhotoInfo> photos = new ArrayList<>();
                boolean local = data.getBooleanExtra(Extras.EXTRA_FROM_LOCAL, false);
                if (local) {
                    // 本地相册
                    photos = PickerContract.getPhotos(data);
                    if (photos == null) {
                        return;
                    }
                } else {
                    // 拍照
                    String photoPath = data.getStringExtra(Extras.EXTRA_FILE_PATH);
                    PhotoInfo photoInfo = new PhotoInfo();
                    photoInfo.setFilePath(photoPath);
                    photos.add(photoInfo);
                }
                List<ApplyPhotoInfo> applyPhotoInfos = new ArrayList<>();
                for (int i = 0; i < photos.size(); i++) {
                    ApplyPhotoInfo info = new ApplyPhotoInfo();
                    info.setFilePath(photos.get(i).getAbsolutePath());
                    info.setType("image");
                    info.ossUrl = "";
                    applyPhotoInfos.add(info);
                }
                if (mFragment != null) {
                    ((ApplyEditFragment) mFragment).addFileViews(applyPhotoInfos);
                }
                break;
            case REQUEST_PICK_FILE_Video:
                // 上传文件--视频
                String fileVideopath = filePathFromIntent(data);
                if (mFragment != null) {
                    ((ApplyEditFragment) mFragment).addFileView(fileVideopath, "video");
                }
                break;
            case REQUEST_ROOM_INVITATION_TYPE:
                // 邀请人员
                if (mFragment != null) {
                    ((ApplyEditFragment) mFragment).setAttendeePersonModel((AttendeePersonModel) data.getSerializableExtra("attendeePersonModel"));
                    ((ApplyEditFragment) mFragment).setDepartPersonModel((MeetingPersonDepartmentResponse.DepartmentModel) data.getSerializableExtra("departPersonModel"));
                    ((ApplyEditFragment) mFragment).setInvitationPersons(data.getStringExtra("userIds"), data.getStringExtra("mobiles")
                            , (List<MeetingPersonDepartmentResponse.DepartmentModel.EmployeeModel>) data.getSerializableExtra("attendeeDatas"));
                    isBackFromMeetingPerson = data.getStringExtra("isBack");
                    tmpHamapA = (HashMap<String, String>) data.getSerializableExtra("checkhashmap");
                    if (isBackFromMeetingPerson.equals("back")) {
                        if (tmpHamapA == null) {
                            tmpHamapA = new HashMap<>();
                        }
                    } else {
                        MeetingPersonTon.getInstance().setAllDepartHashMapString(tmpHamapA);

                    }
                }
                break;
            default:
                break;
        }
    }

    /**
     * 获取文件路径
     *
     * @param data intent数据
     * @return
     */
    private String filePathFromIntent(Intent data) {
        Uri uri = data.getData();
        try {
            assert uri != null;
            @SuppressLint("Recycle")
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor == null) {
                //miui 2.3 有可能为null
                return uri.getPath();
            } else {
                cursor.moveToFirst();
                // 文件路径
                return cursor.getString(cursor.getColumnIndex("_data"));
            }
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void uploadImgToOSS() {

    }

    @Override
    public ArrayList<String> getImgsUriList() {
        return null;
    }

    ArrayList<OSSAsyncTask> mTaskList = new ArrayList<>();

    /**
     * 取消所有上传任务
     */
    private void canleAllTask() {
        if (mTaskList.isEmpty()) {
            return;
        }

        for (OSSAsyncTask task : mTaskList) {
            if (!task.isCompleted()) {
                task.cancel();
            }
        }
        mTaskList.clear();
    }

    /**
     * *************************************查看图片、文档相关********************************************
     * 显示封面图片
     */
    public void showPhotoView(ApplyPhotoInfo selectInfo, ImageView img) {
        ArrayList<String> photoStrs = new ArrayList<>();
        photoStrs.add(selectInfo.getGileUrl());
        ArrayList<Rect> rects = new ArrayList<>();
        rects.add(ImageUtil.getDrawableBoundsInView(img));
        PhotoBrowseInfo info = PhotoBrowseInfo.create(photoStrs, rects, 0);
        PhotoBrowseActivity.startToPhotoBrowseActivity(this, info);
    }

    /**
     * 点击非编辑区域收起键盘
     * 获取点击事件
     */
    @CallSuper
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View view = getCurrentFocus();
            if (view != null && !(view instanceof EditText)) {
                if (mStartLiveReasonEdit != null) {
                    mStartLiveReasonEdit.clearFocus();//失去焦点
                }
            }
            if (isShouldHideKeyBord(view, ev)) {
                assert view != null;
                hideSoftInput(view.getWindowToken());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 判定当前是否需要隐藏
     */
    protected boolean isShouldHideKeyBord(View v, MotionEvent ev) {
        if ((v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left + v.getWidth();
            return !(ev.getX() > left && ev.getX() < right && ev.getY() > top && ev.getY() < bottom);
        }
        return false;
    }

    /**
     * 隐藏软键盘
     */
    private void hideSoftInput(IBinder token) {
        if (token != null) {
            InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            assert manager != null;
            manager.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    EditText mStartLiveReasonEdit;

    public void setEditText(EditText edit) {
        mStartLiveReasonEdit = edit;
    }

    public void initOss() {
        OSSService.getInterface().initOSSService(new OSSService.InitOSSCallBack() {
            @Override
            public void onInitComplete() {

            }

            @Override
            public void onInitFail() {

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showKeyboard(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        canleAllTask();
        dismissProgressHUD();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //清空开播外部联系人集合
        MeetingPersonTon.getInstance().clear();
        finish();
    }

    /**
     * 点击重置按钮
     */
    @Override
    protected void onRightManagerTitleClick() {
        super.onRightManagerTitleClick();
        if (!isCanClickRightManagerTitle) {
            return;
        }
        if (mFragment != null) {
            new TJMakeSureDialog(this, "确定清空吗？",
                    v -> {
                        ((ApplyEditFragment) mFragment).restEditMsg();
                        setRestBtClick(false);
                    }).setBtnText("确定", "取消").show();
        }
    }

    /**
     * 设置"重置"字体颜色及可点击状态
     */
    public void setRestBtClick(boolean isClick) {
        isCanClickRightManagerTitle = isClick;
        if (isClick) {
            setRightManagerTitleColor(getResources().getColor(R.color.color_2671e9));
        } else {
            setRightManagerTitleColor(getResources().getColor(R.color.color_969697));
        }
    }
}
