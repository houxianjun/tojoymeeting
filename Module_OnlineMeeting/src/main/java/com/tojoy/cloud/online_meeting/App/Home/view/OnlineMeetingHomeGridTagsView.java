package com.tojoy.cloud.online_meeting.App.Home.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.App.widgets.TagTextView;
import com.tojoy.cloud.online_meeting.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @author qll
 * @date 2019/4/9
 * 网上天洽会首页grid的标签布局
 * 动态测量宽高，变迁显示不下用...代替
 */
public class OnlineMeetingHomeGridTagsView {
    private Context mContext;
    private View mView;
    private LinearLayout mTagsLlv;
    private LinearLayout rootView;
    /**
     * 标签展示行最长的宽度
     */
    private int viewWidth;
    /**
     * 现在已添加标签的长度
     */
    private int nowWidth;

    private List<String> mTags;


    public OnlineMeetingHomeGridTagsView(Context context, List<String> tags, LinearLayout linearLayout) {
        this.mContext = context;
        this.mTags = tags;
        this.rootView = linearLayout;
    }

    public View getView() {
        if (mView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mView = aLayoutInflater.inflate(R.layout.view_online_meeting_home_tags, null);
        }
        initView();
        return mView;
    }

    private void initView() {
        mTagsLlv = mView.findViewById(R.id.llv_tags);
        rootView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                viewWidth = rootView.getMeasuredWidth() - AppUtils.dip2px(mContext, 5);
                addTags();
                rootView.getViewTreeObserver().removeOnPreDrawListener(this);
                return true;
            }
        });
    }

    private void addTags() {
        mTagsLlv.removeAllViews();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(AppUtils.dip2px(mContext, 5), 0, 0, 0);
        for (int i = 0; i < mTags.size(); i++) {
            TagTextView nowTagTv = new TagTextView(mContext);
            nowTagTv.setText(mTags.get(i));
            nowTagTv.measure(0, 0);
            /* 当前新创建还未添加的标签宽度 */
            int textWidth = nowTagTv.getMeasuredWidth();
            if (i % 3 == 0) {
                nowTagTv.setTextColor(mContext.getResources().getColor(R.color.color_2a62dd));
                nowTagTv.setBackgroundResource(R.drawable.shape_online_home_tag_blue_bg);
            } else if (i % 3 == 1) {
                nowTagTv.setTextColor(mContext.getResources().getColor(R.color.color_f2bc16));
                nowTagTv.setBackgroundResource(R.drawable.shape_online_home_tag_yellow_bg);
            } else {
                nowTagTv.setTextColor(mContext.getResources().getColor(R.color.color_15caa6));
                nowTagTv.setBackgroundResource(R.drawable.shape_online_home_tag_green_bg);
            }
            if (i == mTags.size() - 1) {
                // 当前是最后一个
                if ((nowWidth + textWidth + AppUtils.dip2px(mContext, 5)) <= viewWidth) {
                    // 且能显示下，直接添加
                    nowWidth = nowWidth + textWidth + AppUtils.dip2px(mContext, 5);
                    mTagsLlv.addView(nowTagTv);
                }
            } else {
                // 当前不是最后一个
                /* 省略号的宽度 */
                int ellipsizeTvWidth = 0;
                if ((nowWidth + textWidth + AppUtils.dip2px(mContext, 5) + ellipsizeTvWidth) <= viewWidth) {
                    // 添加省略号的宽度仍能显示下，直接添加
                    nowWidth = nowWidth + textWidth + AppUtils.dip2px(mContext, 5);
                    mTagsLlv.addView(nowTagTv);
                } else {
                    break;
                }
            }
        }
    }
}
