package com.tojoy.cloud.online_meeting.App.Poster.Activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tojoy.cloud.online_meeting.App.Poster.Adapter.PosterBottomPicListAdapter;
import com.tojoy.cloud.online_meeting.App.Poster.Model.BottomPicModel;
import com.tojoy.cloud.online_meeting.App.Poster.View.ThumbnailPosterView;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.imageManager.YuanJiaoImageView;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.util.QR.QRCodeUtil;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.string.StringUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.MathematicalUtils;
import com.tojoy.cloud.online_meeting.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @author qll
 * 会议推广海报页面、商品推广海报页面
 */
@Route(path = RouterPathProvider.ONLINE_MEETING_EXTEN_SION_POSTER)
public class MeetingExtenSionPosterAct extends UI {
    /**
     * 会议推广海报
     */
    private RelativeLayout flvRoot;
    private ImageView mRootBgIv;
    private LinearLayout mMeetingPosterDetailLlv;
    private ThumbnailPosterView thumbnailPosterView;
    private ImageView ivPartsRight;
    private ImageView ivPartsLeft;
    private ImageView ivPartsCenter;

    /**
     * 1：会议推广海报；2：商品推广海报
     */
    private int type;

    /**
     * 会议相关参数
     */
    private String mLiveTitle;
    private String mLiveId;
    private String mLiveCoverUrl;
    private String mRoomId;

    /**
     * 商品相关参数
     */
    private String goodsTitle;
    private String goodsPrice;

    public MeetingExtenSionPosterAct() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting_extension_poster);
        setStatusBar();
        type = getIntent().getIntExtra("fromType", 0);
        mLiveTitle = getIntent().getStringExtra("mLiveTitle");
        mLiveId = getIntent().getStringExtra("mLiveId");
        mLiveCoverUrl = getIntent().getStringExtra("mLiveCoverUrl");
        mRoomId = getIntent().getStringExtra("mRoomId");
        goodsTitle = getIntent().getStringExtra("goodsTitle");
        goodsPrice = getIntent().getStringExtra("goodsPrice");
        initUI();
        initTitle();
        initListener();
        initBottomSmallPics();
    }

    private void initUI() {
        flvRoot = (RelativeLayout) findViewById(R.id.flv_root);
        mRootBgIv = (ImageView) findViewById(R.id.iv_root_bg);
        ImageView mLogoIv = (ImageView) findViewById(R.id.iv_logo);
        TextView mMeetingTitleTv = (TextView) findViewById(R.id.tv_meeting_title);
        mMeetingPosterDetailLlv = (LinearLayout) findViewById(R.id.llv_meeting_poster_detail);
        ImageView mMeetingCoverIv = (ImageView) findViewById(R.id.tv_poster_qr);
        RelativeLayout mPosterBottomRlv = (RelativeLayout) findViewById(R.id.rlv_poster_bottom);
        YuanJiaoImageView mIvHead = (YuanJiaoImageView) findViewById(R.id.iv_head);
        TextView mUserName = (TextView) findViewById(R.id.tv_user_name);
        TextView mUserId = (TextView) findViewById(R.id.tv_user_id);
        LinearLayout mPoaterQRLlv = (LinearLayout) findViewById(R.id.llv_poster_qr);
        ImageView mMeetingPosterQRIv = (ImageView) findViewById(R.id.iv_poster_qr);
        TextView mLongClickHintTv = (TextView) findViewById(R.id.tv_long_click_hint);
        ivPartsRight = (ImageView) findViewById(R.id.iv_parts_right);
        ivPartsLeft = (ImageView) findViewById(R.id.iv_parts_left);
        ivPartsCenter = (ImageView) findViewById(R.id.iv_parts_center);

        FrameLayout mThumbnailFlv = (FrameLayout) findViewById(R.id.flv_thumbnail);
        thumbnailPosterView = new ThumbnailPosterView(this);
        mThumbnailFlv.addView(thumbnailPosterView.getView());

        thumbnailPosterView.goneThumbnailPoster();

        // 设计图尺寸（375 * 812），实际机型整体按照高度的比例进行缩放
        double hScale = MathematicalUtils.div(AppUtils.getHeight(this), AppUtils.dip2px(this, 812), 2);
        int mBigW = (int) (MathematicalUtils.div(320, 375, 2) * AppUtils.getWidth(this) * hScale);
        int marginTop = (int) (AppUtils.dip2px(this, 75) * hScale + AppUtils.getStatusBarHeight(this));
        int marginLeft = (int) MathematicalUtils.div(AppUtils.getWidth(this) - mBigW, 2, 2);
        // 顶部logo的布局处理
        RelativeLayout.LayoutParams logoParams = (RelativeLayout.LayoutParams) mLogoIv.getLayoutParams();
        logoParams.setMargins(marginLeft, marginTop, 0, (int) (AppUtils.dip2px(this, 10) * hScale));
        mLogoIv.setLayoutParams(logoParams);
        mLogoIv.setImageResource(R.drawable.icon_meeting_extension_poster_logo);
        // 中间小窗口的整体布局
        RelativeLayout.LayoutParams mBigPosterParams = (RelativeLayout.LayoutParams) mMeetingPosterDetailLlv.getLayoutParams();
        mBigPosterParams.width = mBigW;
        mMeetingPosterDetailLlv.setLayoutParams(mBigPosterParams);
        // 中间小窗口会议、产品封面图片
        LinearLayout.LayoutParams imageParams = (LinearLayout.LayoutParams) mMeetingCoverIv.getLayoutParams();
        imageParams.width = mBigW;
        imageParams.height = mBigW;
        mMeetingCoverIv.setLayoutParams(imageParams);
        ImageLoaderManager.INSTANCE.loadImage(this, mMeetingCoverIv, OSSConfig.getOSSURLedStr(mLiveCoverUrl), 0);
        // 中间小窗口底部二维码和信息布局
        LinearLayout.LayoutParams mPosterBottomParams = (LinearLayout.LayoutParams) mPosterBottomRlv.getLayoutParams();
        mPosterBottomParams.height = (int) (AppUtils.dip2px(this, 105) * hScale);
        mPosterBottomRlv.setLayoutParams(mPosterBottomParams);

        // 二维码处理
        RelativeLayout.LayoutParams mPosterQRParams = (RelativeLayout.LayoutParams) mPoaterQRLlv.getLayoutParams();
        mPosterQRParams.setMargins(0, 0, (int) (AppUtils.dip2px(this, 22) * hScale), 0);
        mPoaterQRLlv.setLayoutParams(mPosterQRParams);
        LinearLayout.LayoutParams posterQrIvParams = (LinearLayout.LayoutParams) mMeetingPosterQRIv.getLayoutParams();
        posterQrIvParams.width = (int) (AppUtils.dip2px(this, 72) * hScale);
        posterQrIvParams.height = (int) (AppUtils.dip2px(this, 72) * hScale);
        mMeetingPosterQRIv.setLayoutParams(posterQrIvParams);
        mMeetingPosterQRIv.setImageBitmap(QRCodeUtil.createQRCode(BaseUserInfoCache.getUserInviteUrl(this), (int) (AppUtils.dip2px(this, 72) * hScale)));
        mLongClickHintTv.setTextSize((float) (12 * hScale));

        // 配件布局
        RelativeLayout.LayoutParams ivPartsLeftParams = (RelativeLayout.LayoutParams) ivPartsLeft.getLayoutParams();
        ivPartsLeftParams.width = (int) (AppUtils.getWidth(this) - marginLeft - AppUtils.dip2px(this, 22 + 72) * hScale);
        ivPartsLeft.setLayoutParams(ivPartsLeftParams);

        if (type == 1) {
            // 会议
            // 顶部标题
            mMeetingTitleTv.setTextSize((float) (21 * hScale));
            mMeetingTitleTv.setText(mLiveTitle);
            // 若是会议，中间小窗口底部展示头像、姓名、会议id
            RelativeLayout.LayoutParams headIvParams = (RelativeLayout.LayoutParams) mIvHead.getLayoutParams();
            headIvParams.width = (int) (AppUtils.dip2px(this, 41) * hScale);
            headIvParams.height = (int) (AppUtils.dip2px(this, 41) * hScale);
            headIvParams.setMargins((int) (AppUtils.dip2px(this, 6.5f) * hScale), 0, (int) (AppUtils.dip2px(this, 9) * hScale), 0);
            mIvHead.setLayoutParams(headIvParams);
            mIvHead.setType(1);
            mIvHead.setBorderRadius((int) (41 * hScale));
            mIvHead.setWidthHeight((int) (41 * hScale), (int) (41 * hScale));
            ImageLoaderManager.INSTANCE.loadHeadImage(this, mIvHead, BaseUserInfoCache.getUserHeaderPic(this), R.drawable.icon_dorecord);
            mIvHead.setVisibility(View.VISIBLE);

            mUserName.setTextSize((float) (16 * hScale));
            mUserName.setMaxLines(1);
            mUserName.setText(BaseUserInfoCache.getUserName(this));
            mUserId.setTextSize((float) (13 * hScale));
            mUserId.setTextColor(getResources().getColor(R.color.color_91969e));
            mUserId.setText("ID  " + mRoomId);
            findViewById(R.id.tv_pirce_symbol).setVisibility(View.GONE);

        } else if (type == 2) {
            // 商品
            // 顶部标题
            mMeetingTitleTv.setTextSize((float) (18 * hScale));
            String title = BaseUserInfoCache.getUserName(this) + "给你推荐了一个好东西";
            mMeetingTitleTv.setText(StringUtil.setDifferentTextSize(title, (float) MathematicalUtils.div(18, 14, 2), 0, title.length() - 10));
            LinearLayout llvMeetingOrGoodsMsg = (LinearLayout) findViewById(R.id.llv_meeting_or_goods_msg);
            llvMeetingOrGoodsMsg.setPadding(AppUtils.dip2px(this,13.5f),0,0,0);
            mIvHead.setVisibility(View.GONE);
            mUserName.setTextSize((float) (12.5 * hScale));
            mUserName.setText(goodsTitle);
            mUserName.setMaxLines(2);
            mUserId.setTextSize((float) (16 * hScale));
            mUserId.setTextColor(getResources().getColor(R.color.color_fff4372d));
            if (!TextUtils.isEmpty(goodsPrice)) {
                findViewById(R.id.tv_pirce_symbol).setVisibility(View.VISIBLE);
                int dollIndex = goodsPrice.indexOf(".");
                mUserId.setText(StringUtil.setDifferentTextSize(goodsPrice, (float) MathematicalUtils.div(25, 16, 2), 0, dollIndex + 1));
            }
        }
    }

    private void initTitle() {
        setUpNavigationBar();
        setAlphaBackground();
        showBack();
    }

    private void initListener() {
        mMeetingPosterDetailLlv.setOnLongClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                thumbnailPosterView.showThumbnailPoster(flvRoot);
            }
            return false;
        });
    }

    private void initBottomSmallPics() {
        RecyclerView recycleBottomSmallPic = (RecyclerView) findViewById(R.id.recycle_bottom_small_pic);
        List<BottomPicModel> pics = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            BottomPicModel model = new BottomPicModel();
            model.isSelected = i == 0;
            if (i == 0) {
                model.picSource = R.drawable.bg_poster_template_one;
                model.picParts = R.drawable.bg_poster_template_one_parts;
                model.partsLocation = "right";
                // 推广码整体背景图片
                mRootBgIv.setImageResource(R.drawable.bg_poster_template_one);
                ivPartsRight.setImageResource(R.drawable.bg_poster_template_one_parts);
                ivPartsRight.setVisibility(View.VISIBLE);
            } else if (i == 1) {
                model.picSource = R.drawable.bg_poster_template_two;
                model.picParts = R.drawable.bg_poster_template_two_parts;
                model.partsLocation = "center";
            } else if (i == 2) {
                model.picSource = R.drawable.bg_poster_template_three;
            } else if (i == 3) {
                model.picSource = R.drawable.bg_poster_template_four;
                model.picParts = R.drawable.bg_poater_template_four_parts;
                model.partsLocation = "left";
            } else {
                model.picSource = R.drawable.bg_poster_template_five;
            }

            pics.add(model);
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recycleBottomSmallPic.setLayoutManager(linearLayoutManager);
        recycleBottomSmallPic.setItemViewCacheSize(30);

        PosterBottomPicListAdapter mAdapter = new PosterBottomPicListAdapter(this, pics);
        mAdapter.setHasStableIds(true);
        recycleBottomSmallPic.setAdapter(mAdapter);
        mAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
            for (BottomPicModel filePageModel : pics) {
                filePageModel.isSelected = false;
            }
            pics.get(position).isSelected = true;
            mAdapter.notifyDataSetChanged();

            mRootBgIv.setImageResource(data.picSource);
            if (data.picParts != 0) {
                // 此模版有配件展示，判断展示的位置
                if ("left".equals(data.partsLocation)) {
                    ivPartsLeft.setVisibility(View.VISIBLE);
                    ivPartsRight.setVisibility(View.GONE);
                    ivPartsCenter.setVisibility(View.GONE);
                    ivPartsLeft.setImageResource(data.picParts);
                } else if ("center".equals(data.partsLocation)) {
                    ivPartsLeft.setVisibility(View.GONE);
                    ivPartsRight.setVisibility(View.GONE);
                    ivPartsCenter.setVisibility(View.VISIBLE);
                    ivPartsCenter.setImageResource(data.picParts);
                } else if ("right".equals(data.partsLocation)) {
                    ivPartsLeft.setVisibility(View.GONE);
                    ivPartsRight.setVisibility(View.VISIBLE);
                    ivPartsCenter.setVisibility(View.GONE);
                    ivPartsRight.setImageResource(data.picParts);
                } else {
                    ivPartsRight.setVisibility(View.GONE);
                    ivPartsLeft.setVisibility(View.GONE);
                    ivPartsCenter.setVisibility(View.GONE);
                }
            } else {
                ivPartsRight.setVisibility(View.GONE);
                ivPartsLeft.setVisibility(View.GONE);
                ivPartsCenter.setVisibility(View.GONE);
            }
        });
    }

}