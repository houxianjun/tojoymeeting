package com.tojoy.cloud.online_meeting.App.Live.Interaction;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.editView.ClearEmojInputFilter;
import com.tojoy.tjoybaselib.ui.editView.LRNotiveEmojInputFilter;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.tojoy.cloud.online_meeting.R;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * Created by luuzhu on 2019/4/26.
 * 公告
 */

public class NoticeDialog {

    public String mType;
    private Dialog mDialog;
    private Activity mContext;
    private View mView;
    private EditText mEtNotice;
    private TextView mTvPublish;
    private View mSeizeView;
    private TextView tvTextNum;

    private boolean retryHide = false;
    //隐藏公告
    private View tvHidden;

    /**
     * @param context
     * @param type    1:公告  2:举报
     */
    public NoticeDialog(Context context, String type) {
        mContext = (Activity) context;
        this.mType = type;
        initUI();
    }

    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_live_notice, null);
        initView();
        initListener();
    }

    public void initView() {
        TextView mTvTitle = mView.findViewById(R.id.tvTitle);
        mTvPublish = mView.findViewById(R.id.tvPublish);
        mEtNotice = mView.findViewById(R.id.etNotice);
        mSeizeView = mView.findViewById(R.id.seizeView);
        TextView mTvNotice = mView.findViewById(R.id.tvNotice);
        tvTextNum = mView.findViewById(R.id.tvTextNum);
        tvHidden = mView.findViewById(R.id.tvHidden);

        InputFilter[] emojiFilters = {new ClearEmojInputFilter()};
        InputFilter[] noticeEmojiFilters = {new LRNotiveEmojInputFilter()};
        if ("1".equals(mType)) {
            mEtNotice.setFilters(noticeEmojiFilters);
        } else {
            mEtNotice.setFilters(emojiFilters);
        }

        if ("1".equals(mType)) {
            //公告
            mTvTitle.setText("公告");
            mTvPublish.setText("发布");
            mEtNotice.setHint("请在此输入文字…");
            if ("2".equals(LiveRoomInfoProvider.getInstance().myUserType)) {
                mTvPublish.setVisibility(View.GONE);
                mEtNotice.setVisibility(View.GONE);
                tvTextNum.setVisibility(View.GONE);
                tvHidden.setVisibility(View.GONE);
                mTvNotice.setVisibility(View.VISIBLE);
            } else {
                mTvPublish.setVisibility(View.VISIBLE);
                mEtNotice.setVisibility(View.VISIBLE);
                tvTextNum.setVisibility(View.VISIBLE);
                mTvNotice.setVisibility(View.GONE);
                tvHidden.setVisibility(View.VISIBLE);
            }
            if (TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().announcement)) {
                if ("2".equals(LiveRoomInfoProvider.getInstance().myUserType)) {
                    mTvNotice.setText("主持人还没有发布公告");
                }
            } else {
                mEtNotice.setText(LiveRoomInfoProvider.getInstance().announcement);
                mTvNotice.setText(LiveRoomInfoProvider.getInstance().announcement);
                // 防止从后台拿到的数据大于最大字符数量，插入光标报越界异常
                String mEtNoticemsg = mEtNotice.getText().toString();
                mEtNotice.setSelection(mEtNoticemsg.length());
                tvTextNum.setText(mEtNotice.getText().toString().length() + "/50");
            }
        } else if ("2".equals(mType)) {
            //举报
            mTvTitle.setText("举报");
            mTvPublish.setText("提交");
            mTvPublish.setVisibility(View.VISIBLE);
            mEtNotice.setVisibility(View.VISIBLE);
            tvTextNum.setVisibility(View.VISIBLE);
            mTvNotice.setVisibility(View.GONE);
            mEtNotice.setEnabled(true);
            mEtNotice.setHint("请输入内容");
            tvHidden.setVisibility(View.GONE);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initListener() {
        mEtNotice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String content = mEtNotice.getText().toString();
                if (content.length() > 50) {
                    Toasty.normal(mContext, "字数超过限制").show();
                    try {
                        mEtNotice.setText(content.substring(0, 50));
                        mEtNotice.setSelection(50);
                    } catch (Exception e) {
                        mEtNotice.setText(content.substring(0, 49));
                        mEtNotice.setSelection(49);
                    }
                }
                tvTextNum.setText(mEtNotice.getText().toString().length() + "/50");
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mTvPublish.setOnClickListener(view -> {
            if (TextUtils.isEmpty(mEtNotice.getText().toString().trim())) {
                return;
            }

            if ("发布".equals(mTvPublish.getText().toString())) {
                OMAppApiProvider.getInstance().livePublishNotice(LiveRoomInfoProvider.getInstance().imRoomId, LiveRoomInfoProvider.getInstance().liveId,
                        mEtNotice.getText().toString(), "1", new Observer<OMBaseResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                Toasty.normal(mContext, "网络错误，请检查网络").show();
                            }

                            @Override
                            public void onNext(OMBaseResponse response) {
                                if (response.isSuccess()) {
                                    Toasty.normal(mContext, "发布成功").show();
                                } else {
                                    Toasty.normal(mContext, response.msg).show();
                                }
                            }
                        });

            } else if ("提交".equals(mTvPublish.getText().toString())) {
                OMAppApiProvider.getInstance().liveReport(LiveRoomInfoProvider.getInstance().liveId, BaseUserInfoCache.getUserId(mContext),
                        LiveRoomInfoProvider.getInstance().imCreatorid, mEtNotice.getText().toString(), new Observer<OMBaseResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                Toasty.normal(mContext, "网络错误，请检查网络").show();
                            }

                            @Override
                            public void onNext(OMBaseResponse response) {
                                if (response.isSuccess()) {
                                    Toasty.normal(mContext, "举报成功").show();
                                } else {
                                    Toasty.normal(mContext, response.msg).show();
                                }
                            }
                        });
            }
            mDialog.dismiss();
        });


        tvHidden.setOnClickListener(view -> {
            if (TextUtils.isEmpty(mEtNotice.getText().toString().trim())) {
                return;
            }
            OMAppApiProvider.getInstance().livePublishNotice(LiveRoomInfoProvider.getInstance().imRoomId, LiveRoomInfoProvider.getInstance().liveId,
                    mEtNotice.getText().toString(), "0", new Observer<OMBaseResponse>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            if (retryHide) {
                                Toasty.normal(mContext, "网络错误，请检查网络").show();
                            } else {
                                retryHide = true;
                                tvHidden.performClick();
                            }
                        }

                        @Override
                        public void onNext(OMBaseResponse response) {
                            if (!response.isSuccess()) {
                                if (retryHide) {
                                    Toasty.normal(mContext, response.msg).show();
                                } else {
                                    retryHide = true;
                                    tvHidden.performClick();
                                }
                            } else {
                                Toasty.normal(mContext, "隐藏成功").show();
                            }
                        }
                    });

            mDialog.dismiss();
        });


        mView.findViewById(R.id.ivClose).setOnClickListener(view -> mDialog.dismiss());

        mEtNotice.setOnTouchListener((view, motionEvent) -> {
            if (mEtNotice.isEnabled() && !BaseLiveAct.isKeyboardShowing) {
                update(false);
            }

            return false;
        });

    }

    public void setOnDismissListener(DialogInterface.OnDismissListener listener) {
        mDialog.setOnDismissListener(listener);
    }

    public boolean isShowing() {
        return mDialog != null && mDialog.isShowing();
    }


    public void update(boolean isShowSeize) {
        mSeizeView.setVisibility(isShowSeize ? View.VISIBLE : View.GONE);

        if (LiveRoomInfoProvider.getInstance().isGuest() && "1".equals(mType)) {
            mTvPublish.setVisibility(View.GONE);
            tvHidden.setVisibility(View.GONE);
        } else {
            mTvPublish.setVisibility(isShowSeize ? View.VISIBLE : View.GONE);
            if ("2".equals(mType)) {
                // 举报无隐藏
                tvHidden.setVisibility(View.GONE);
            } else {
                tvHidden.setVisibility(isShowSeize ? View.VISIBLE : View.GONE);
            }
        }
    }

    public void show() {

        mDialog = new Dialog(mContext, R.style.LiveDialogFragmentEnterExitAnim);
        mDialog.setContentView(mView);

        Window win = mDialog.getWindow();
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.windowAnimations = R.style.BottomInAndOutStyle;
        lp.gravity = Gravity.BOTTOM;
        win.setAttributes(lp);

        mDialog.setCanceledOnTouchOutside(true);
        mDialog.show();
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }
}
