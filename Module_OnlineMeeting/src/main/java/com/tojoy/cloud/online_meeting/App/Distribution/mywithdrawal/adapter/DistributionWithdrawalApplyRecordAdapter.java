package com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.home.MineDistributionApplyRecordResponse;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;

import java.util.List;

public class DistributionWithdrawalApplyRecordAdapter extends BaseRecyclerViewAdapter<MineDistributionApplyRecordResponse.ApplyModel> {
    Context mcontext;

    public DistributionWithdrawalApplyRecordAdapter(@NonNull Context context, @NonNull List<MineDistributionApplyRecordResponse.ApplyModel> datas) {
        super(context, datas);
        this.mcontext = context;
    }

    @Override
    protected int getViewType(int position, @NonNull MineDistributionApplyRecordResponse.ApplyModel data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_distribution_withdrawal_apply_record;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new RecordVH(this.mcontext, parent, getLayoutResId(viewType));
    }

    private static class RecordVH extends BaseRecyclerViewHolder<MineDistributionApplyRecordResponse.ApplyModel> {
        private RelativeLayout recordMainRlv;
        private TextView mWithdrawalDateNameTv;
        private TextView mProfitWithdrawalAcountTv;
        private TextView mProfitWithdrawalStatusTv;
        Context context;
        String mtype;

        public void setType(String distributionType) {
            this.mtype = distributionType;
        }

        public RecordVH(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            this.context = context;
            bindViews();
        }

        private void bindViews() {
            recordMainRlv = itemView.findViewById(R.id.rlv_record_main);
            mWithdrawalDateNameTv = itemView.findViewById(R.id.tv_withdrawal_date_name);
            mProfitWithdrawalAcountTv = itemView.findViewById(R.id.tv_profit_withdrawal_acount);
            mProfitWithdrawalStatusTv = itemView.findViewById(R.id.tv_profit_withdrawal_status);
        }

        @Override
        public void onBindData(MineDistributionApplyRecordResponse.ApplyModel data, int position) {
            try {
                recordMainRlv.setOnClickListener(v -> {
                    if (!FastClickAvoidUtil.isDoubleClick()) {
                        ARouter.getInstance().build(RouterPathProvider.Distribution_WithdrawalDetail_Act)
                                .withString("id", data.id)
                                .navigation();
                    }
                });
                mWithdrawalDateNameTv.setText(data.createDate);
                mProfitWithdrawalAcountTv.setText("-" + data.amount);
                //1未支付，2已支付, 3，4支付失败'
                if ("2".equals(data.status)) {
                    mProfitWithdrawalStatusTv.setTextColor(getContext().getResources().getColor(R.color.color_1083EA));
                    mProfitWithdrawalStatusTv.setText("交易完成");
                } else if ("4".equals(data.status)) {
                    mProfitWithdrawalStatusTv.setTextColor(getContext().getResources().getColor(R.color.color_fb4f65));
                    mProfitWithdrawalStatusTv.setText("交易失败");
                } else {
                    mProfitWithdrawalStatusTv.setTextColor(getContext().getResources().getColor(R.color.color_f5A623));
                    mProfitWithdrawalStatusTv.setText("提现中");
                }
            } catch (Exception ex) {
                //后面可以加上报日志
            }
        }
    }
}
