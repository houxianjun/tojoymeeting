package com.tojoy.cloud.online_meeting.App.EnterpriseEditionHome.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.netease.nim.uikit.business.chatroom.helper.ChatRoomHelper;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.enterprise.CorporationMsgResponse;
import com.tojoy.tjoybaselib.model.enterprise.EnterpriseBannerModel;
import com.tojoy.tjoybaselib.model.enterprise.EnterpriseBannerResponse;
import com.tojoy.tjoybaselib.model.enterprise.EnterpriseHomeHotLiveListResponse;
import com.tojoy.tjoybaselib.model.live.OnlineMeetingHomeGridModel;
import com.tojoy.tjoybaselib.model.live.ProductListResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.services.permission.annotation.OnMPermissionGranted;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.LiveActAtack;
import com.tojoy.cloud.online_meeting.App.EnterpriseEditionHome.adapter.CustomizedEnterprisePageAdapter;
import com.tojoy.cloud.online_meeting.App.EnterpriseEditionHome.view.CustomizedEnterpriseHeaderView;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.live.floatpalyer.FloatEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

@Route(path = RouterPathProvider.OnlineMeetingCustomizedEnterprisePageAct)
public class CustomizedEnterprisePageAct extends UI {
    //控件
    private TJRecyclerView mRecyclerView;
    private CustomizedEnterprisePageAdapter mAdapter;
    private CustomizedEnterpriseHeaderView mHeaderView;
    private TextView mTvContent;

    /**
     * banner数据
     */
    private List<EnterpriseBannerModel> mBannerList = new ArrayList<>();
    /**
     * 产品推荐列表数据
     */
    private List<ProductListResponse.ProductDataBean> mProductRecommendList = new ArrayList<>();
    /**
     * 热门推荐列表
     */
    private List<OnlineMeetingHomeGridModel> mHotRecommendList = new ArrayList<>();

    //热门推荐默认标题
    private String mHotRecommendTitle = "热门推荐";
    //产品推荐默认标题
    private String mProductRecommendTitle = "产品推荐";

    //是否展示热门推荐
    private boolean isShowHotRecommend;
    //是否展示产品推荐
    private boolean isShowProductRecommend;

    //公司码
    private String mCompanyCode;
    //联系客服页面
    private String mCompanyServiceMobile;

    /**
     * 若来源为直播间，需暂时记录直播间信息
     */
    private String roomLiveId;
    private String roomId;
    private String isFromLive;
    private String sourceWay;


    /**
     * 只有从首页我的页面跳转到企业定制页面才调用该方法
     * @param context
     * @param companyCode
     */
    public static void start(Context context, String companyCode) {
        Intent intent = new Intent();
        intent.putExtra("companyCode", companyCode);
        intent.setClass(context, CustomizedEnterprisePageAct.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customized_enterprise_page);
        isFromLive = getIntent().getStringExtra("isFromLive");
        sourceWay = getIntent().getStringExtra("sourceWay");
        if (!TextUtils.isEmpty(isFromLive) && "1".equals(isFromLive)) {
            // 从直播间跳过来的，打开小窗口播放
            EventBus.getDefault().post(new FloatEvent(true));
        }
        roomLiveId = getIntent().getStringExtra("roomLiveId");
        roomId = getIntent().getStringExtra("roomId");
        initTitle();
        initView();

        if (ChatRoomHelper.isFromLiveRoom) {
            LiveActAtack.getActivityStack().pushActivity(this);
        }
    }

    protected void initTitle() {
        setStatusBar();
        setUpNavigationBar();
        showBack();
        hideBottomLine();
        setRightManagerTitleTopDrowable("联系企业", R.drawable.icon_business_phone);
        if (mRightManagerIv != null) {
            mRightManagerIv.setVisibility(View.GONE);
        }
    }

    /**
     * 初始化控件
     */
    private void initView() {
        mCompanyCode = getIntent().getStringExtra("companyCode");
        mCompanyCode = TextUtils.isEmpty(mCompanyCode) ? BaseUserInfoCache.getCompanyCode(this) : mCompanyCode;

        mRecyclerView = (TJRecyclerView) findViewById(R.id.reycler_view);
        mTvContent = (TextView) findViewById(R.id.tv_empty_text);
        mRecyclerView.getRecyclerView().setVerticalScrollBarEnabled(false);
        mRecyclerView.getRecyclerView().setHorizontalScrollBarEnabled(false);

        mAdapter = new CustomizedEnterprisePageAdapter(this, mProductRecommendList);
        mHeaderView = new CustomizedEnterpriseHeaderView(this);
        mRecyclerView.addHeaderView(mHeaderView.getView());
        mRecyclerView.setAdapter(mAdapter);
        mHeaderView.setCompanyCode(mCompanyCode);
        mHeaderView.setPageSource(isFromLive,roomLiveId,roomId,sourceWay);
        mAdapter.setPageSource(isFromLive,roomLiveId,roomId,sourceWay);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.getRecyclerView().setLayoutManager(linearLayoutManager);
        mRecyclerView.setLoadMoreViewWithHide();

        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                getCompanyInfo();
            }

            @Override
            public void onLoadMore() {
            }
        });

        mRecyclerView.autoRefresh();
    }

    /**
     * 获取企业信息接口
     */
    private void getCompanyInfo() {
        OMAppApiProvider.getInstance().mCorporationQuery(mCompanyCode, true, new Observer<CorporationMsgResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                mRecyclerView.setRefreshing(false);
                Toasty.normal(CustomizedEnterprisePageAct.this, "网络异常，稍后再试").show();
            }

            @Override
            public void onNext(CorporationMsgResponse omBaseResponse) {
                if (omBaseResponse.isSuccess()) {
                    isDefaultOrEmptyCompanyPage(omBaseResponse.data.customState != 1);
                    mCompanyServiceMobile = omBaseResponse.data.phone;
                    if (!TextUtils.isEmpty(mCompanyServiceMobile)) {
                        if (mRightManagerTv != null) {
                            mRightManagerTv.setVisibility(View.VISIBLE);
                        }
                    } else {
                        if (mRightManagerTv != null) {
                            mRightManagerTv.setVisibility(View.GONE);
                        }
                    }
                    setTitle(omBaseResponse.data.name);
                    for (int i = 0; i < omBaseResponse.data.corporationNavs.size(); i++) {
                        if ("RECOMMEND".equals(omBaseResponse.data.corporationNavs.get(i).code)) {
                            // 热门直播
                            mHotRecommendTitle = omBaseResponse.data.corporationNavs.get(i).title;
                            isShowHotRecommend = omBaseResponse.data.corporationNavs.get(i).getState();
                            mHeaderView.setLiveListTitle(mHotRecommendTitle);
                        } else if ("PRODUCT_RECOMMEND".equals(omBaseResponse.data.corporationNavs.get(i).code)) {
                            // 产品推荐
                            mProductRecommendTitle = omBaseResponse.data.corporationNavs.get(i).title;
                            isShowProductRecommend = omBaseResponse.data.corporationNavs.get(i).getState();
                            mHeaderView.setProductListTitle(mProductRecommendTitle);
                        }
                    }
                } else {
                    Toasty.normal(CustomizedEnterprisePageAct.this, omBaseResponse.msg).show();
                    isDefaultOrEmptyCompanyPage(true);
                    mRecyclerView.setRefreshing(false);
                }
            }
        });
    }

    /**
     * 是否显示默认定制首页、空视图
     */
    private void isDefaultOrEmptyCompanyPage(boolean isDefault) {
        if (isDefault) {
            findViewById(R.id.emptyLayout_order).setVisibility(View.VISIBLE);
            mTvContent.setText(getString(R.string.no_company_config));
            mRecyclerView.setVisibility(View.GONE);
        } else {
            findViewById(R.id.emptyLayout_order).setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            // 只有定制首页开启则请求下面数据
            requestBannerData();
        }
    }

    /**
     * banner数据
     */
    private void requestBannerData() {
        OMAppApiProvider.getInstance().mGetCorporationBanner(mCompanyCode, new Observer<EnterpriseBannerResponse>() {
            @Override
            public void onCompleted() {
                requestHotRecommentData();
            }

            @Override
            public void onError(Throwable e) {
                mBannerList.clear();
                requestHotRecommentData();
                mHeaderView.setBannerData(null);
            }

            @Override
            public void onNext(EnterpriseBannerResponse omBaseResponse) {
                if (omBaseResponse.isSuccess()) {
                    if (omBaseResponse.data != null && omBaseResponse.data.list != null && omBaseResponse.data.list.size() != 0) {
                        mBannerList.clear();
                        mBannerList.addAll(omBaseResponse.data.list);
                        mHeaderView.setBannerData(mBannerList);
                    } else {
                        mHeaderView.setBannerData(null);
                    }
                } else {
                    mHeaderView.setBannerData(null);
                    Toasty.normal(CustomizedEnterprisePageAct.this, omBaseResponse.msg != null ? omBaseResponse.msg : "").show();
                }
            }
        });
    }

    /**
     * 热门推荐列表
     */
    private void requestHotRecommentData() {
        OMAppApiProvider.getInstance().mHotRoomLive(mCompanyCode, 1, 2, 1, "", new Observer<EnterpriseHomeHotLiveListResponse>() {
            @Override
            public void onCompleted() {
                requestProductRecommendData();

            }

            @Override
            public void onError(Throwable e) {
                requestProductRecommendData();
                Toasty.normal(CustomizedEnterprisePageAct.this, "网络异常，稍后再试").show();
            }

            @Override
            public void onNext(EnterpriseHomeHotLiveListResponse homeHotLiveListResponse) {
                if (homeHotLiveListResponse.isSuccess()) {
                    if (homeHotLiveListResponse.data != null && isShowHotRecommend) {
                        mHotRecommendList.clear();
                        mHotRecommendList.addAll(homeHotLiveListResponse.data.list);
                        mHeaderView.setHotLiveListData(mHotRecommendList);
                    } else {
                        mHotRecommendList.clear();
                        mHeaderView.setHotLiveListData(null);
                    }
                } else {
                    Toasty.normal(CustomizedEnterprisePageAct.this, homeHotLiveListResponse.msg).show();
                }
            }
        });
    }

    /**
     * 请求产品推荐列表数据
     */
    private void requestProductRecommendData() {

        OMAppApiProvider.getInstance().getProductList(mCompanyCode, new Observer<ProductListResponse>() {
            @Override
            public void onCompleted() {
                mRecyclerView.setRefreshing(false);
                if (mBannerList.size() == 0 && mHotRecommendList.size() == 0 && mProductRecommendList.size() == 0) {
                    mRecyclerView.setEmptyImgAndTip(R.drawable.icon_no_content, getString(R.string.instant_search_no_content));
                    findViewById(R.id.emptyLayout_order).setVisibility(View.VISIBLE);
                    mTvContent.setText("暂无内容");
                } else {
                    findViewById(R.id.emptyLayout_order).setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(Throwable e) {
                mRecyclerView.setRefreshing(false);
            }

            @Override
            public void onNext(ProductListResponse omBaseResponse) {
                if (omBaseResponse.isSuccess()) {
                    if (omBaseResponse.data != null && omBaseResponse.data.size() != 0 && isShowProductRecommend) {
                        if (mPage == 1) {
                            mProductRecommendList.clear();
                        }

                        mProductRecommendList.addAll(omBaseResponse.data);
                        mAdapter.updateData(mProductRecommendList);
                        mHeaderView.setProductTitleVisible(true);
                        mRecyclerView.refreshLoadMoreView(mPage, omBaseResponse.data.size());
                    } else {
                        if (mPage == 1) {
                            mProductRecommendList.clear();
                            mAdapter.updateData(mProductRecommendList);
                            mHeaderView.setProductTitleVisible(false);
                            mRecyclerView.refreshLoadMoreView(mPage, 0);
                        }
                    }
                } else {
                    Toasty.normal(CustomizedEnterprisePageAct.this, omBaseResponse.msg != null ? omBaseResponse.msg : "").show();
                }
            }
        });
    }


    /**
     * 联系企业
     */
    @Override
    protected void onRightManagerTitleClick() {
        super.onRightManagerTitleClick();
        if (FastClickAvoidUtil.isDoubleClick()) {
            return;
        }
        if (LiveRoomInfoProvider.getInstance().isHost() && ChatRoomHelper.isFromLiveRoom) {
            Toasty.normal(this, "正在直播，请稍后再试").show();
            return;
        }
        checkPersimiss(PERMISSIONS_MAKECALL, PERMISSIONS_MAKECALL_REQUEST_CODE);
    }

    //拨打电话
    @OnMPermissionGranted(PERMISSIONS_MAKECALL_REQUEST_CODE)
    public void onBasicPermissionSuccessMobile() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mCompanyServiceMobile));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
