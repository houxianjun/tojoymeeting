package com.tojoy.cloud.online_meeting.App.Live.Live.Activity;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.google.gson.Gson;
import com.netease.nim.uikit.business.chatroom.helper.ChatRoomHelper;
import com.netease.nim.uikit.business.liveroom.IOLiveRoomListener;
import com.netease.nim.uikit.business.liveroom.LiveRoomIOHelper;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tencent.rtmp.ui.TXCloudVideoView;
import com.tencent.trtc.TRTCCloudDef;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.Model.Router.AppRouterJumpModel;
import com.tojoy.common.Services.EventBus.LiveRoomEvent;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import es.dmoral.toasty.Toasty;

import static com.tencent.trtc.TRTCCloudDef.TRTCRoleAnchor;

/**
 * 直播页面
 */
@Route(path = RouterPathProvider.ONLINE_MEETING_LIVE_ROOM)
public class LiveAct extends BaseLiveAct {

    @SuppressLint("StaticFieldLeak")
    public static LiveAct SINGTON;
    private String pushLiveId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mAppScence = TRTCCloudDef.TRTC_APP_SCENE_VIDEOCALL;
        super.onCreate(savedInstanceState);
        SINGTON = this;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String pushJson = intent.getStringExtra("liveJson");
        if (!TextUtils.isEmpty(pushJson)) {
            if (!BaseUserInfoCache.isLogined(this)) {
                ARouter.getInstance().build(RouterPathProvider.Login)
                        .navigation();
                finish();
            } else {
                // TODO: 2020/7/3  需要判断此push是不是发给自己的
                doPush(pushJson);
            }
        }
    }

    @Override
    protected void doPush(String pushJson) {
        if (pushJson.startsWith("{") && pushJson.endsWith("}")) {
            AppRouterJumpModel model = new Gson().fromJson(pushJson, AppRouterJumpModel.class);
            pushLiveId = model.roomLiveId;
        } else {
            pushLiveId = pushJson;
        }

        boolean isSameRoom = pushLiveId.equals(LiveRoomInfoProvider.getInstance().liveId);

        if (ChatRoomHelper.isFromLiveRoom) {
            if (!isSameRoom) {
                Toasty.normal(LiveAct.this, "正在直播，请稍后再试").show();
            }
        } else {
            joinLive();
        }

    }

    private void joinLive() {
        LiveRoomInfoProvider.getInstance().liveId = pushLiveId;
        LiveRoomInfoProvider.getInstance().joinPassword = "";
        LiveRoomIOHelper.joinLiveRoom(LiveAct.this, new IOLiveRoomListener() {
            @Override
            public void onIOError(String error, int errorCode) {
                finish();
                if (!TextUtils.isEmpty(error)) {
                    Toasty.normal(LiveAct.this, error).show();
                }
            }

            @Override
            public void onIOSuccess() {
                loginTXLiveRoom();
            }
        });
    }

    @Override
    protected void enterRoom() {
        LogUtil.i(LiveLogTag.BaseLiveTag, "enterRoom");
        TXCloudVideoView localVideoView = mTRTCVideoViewLayout.getCloudVideoViewByIndex(0);
        localVideoView.setUserId(mTrtcParams.userId);
        localVideoView.setVisibility(View.VISIBLE);
        findViewById(R.id.rlv_close).setVisibility(View.VISIBLE);

        mTrtcParams.role = TRTCRoleAnchor;

        // 开启视频采集预览
        if (!LiveRoomInfoProvider.getInstance().isOfficalLive()) {
            mTrtcCloud.startLocalPreview(true, localVideoView);
            mTrtcCloud.startLocalAudio();
        }

        //主播端采用视频通话模式
        mTrtcCloud.enterRoom(mTrtcParams, TRTCCloudDef.TRTC_APP_SCENE_VIDEOCALL);
    }

    @Override
    protected void initVideoViewLayout() {
        super.initVideoViewLayout();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPublicStringEvent(LiveRoomEvent event) {
        if(LiveRoomEvent.LIVE_CLOSE_ALL.equals(event.getType())) {
            finish();
        }
    }
}
