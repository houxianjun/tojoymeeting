package com.tojoy.cloud.online_meeting.App.Meeting.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.enterprise.CorporationMsgResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.GesturesRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.cloud.online_meeting.App.Meeting.adapter.HomeHotEnterpriseAdapter;
import com.tojoy.tjoybaselib.model.home.HomeModelResponse;
import com.tojoy.cloud.online_meeting.R;

import es.dmoral.toasty.Toasty;

/**
 * 热门企业板块
 */
public class HotEnterpriseView extends RelativeLayout implements PlateBaseView {
    private Context mContext;
    private View mView;
    //版本总布局组件
    private RelativeLayout mHotEnterpriseMainRlv;
    //热门企业板块标题总布局
    private RelativeLayout mTodayBusinessTitleRlv;
    //标题
    private TextView mTodayBusinessTitileTv;
    //板块描述
    private TextView mHotEnterpriseDescribeTv;
    //横向滑动RecyclerView
    private GesturesRecyclerView mHotEnterpriseRecyclerview;
    //列表适配器
    private HomeHotEnterpriseAdapter adapter;

    //更多企业页面title
    String title = "";

    public HotEnterpriseView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;

        if (mView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mView = aLayoutInflater.inflate(R.layout.view_home_hot_enterprise, null);
        }

        initView();
        this.addView(mView);
    }

    public View getView() {
        if (mView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mView = aLayoutInflater.inflate(R.layout.view_home_hot_enterprise, null);
        }
        initView();
        return mView;
    }

    @Override
    public void initView() {
        mHotEnterpriseMainRlv = mView.findViewById(R.id.rl_hot_enterprise_main);
        mTodayBusinessTitleRlv = mView.findViewById(R.id.rlv_today_business_title);
        mTodayBusinessTitileTv = mView.findViewById(R.id.tv_today_business_titile);
        mHotEnterpriseDescribeTv = mView.findViewById(R.id.tv_hot_enterprise_describe);
        mHotEnterpriseRecyclerview = mView.findViewById(R.id.rclv_hot_enterprise);
    }

    @Override
    public void initData() {

    }

    @Override
    public void initData(HomeModelResponse data) {
        //数据不为空，则显示板块
        if (data != null && data.dataList != null && data.dataList.size() != 0) {
            if (data.spaceName != null) {
                title = data.spaceName;
            }
            mHotEnterpriseMainRlv.setVisibility(VISIBLE);
            mTodayBusinessTitileTv.setText(data.spaceName);
            mHotEnterpriseDescribeTv.setText(data.des);
            adapter = new HomeHotEnterpriseAdapter(mContext, data.dataList);

            LinearLayoutManager ms = new LinearLayoutManager(mContext);
            // 设置 recyclerview 布局方式为横向布局
            ms.setOrientation(LinearLayoutManager.HORIZONTAL);
            //LinearLayoutManager 种 含有3 种布局样式  第一个就是最常用的 1.横向 , 2. 竖向,3.偏移
            //给RecyClerView 添加设置好的布局样式
            mHotEnterpriseRecyclerview.setLayoutManager(ms);
            mHotEnterpriseRecyclerview.setAdapter(adapter);
            initListener();
        }
        //没有数据，不展示该板块
        else {
            mHotEnterpriseMainRlv.setVisibility(GONE);
        }
    }

    public void initListener() {
        mTodayBusinessTitleRlv.setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                ARouter.getInstance().build(RouterPathProvider.BrowsedEnterprise)
                        .withString("pagefrom", "MeetingFragment")
                        .withString("title", title).navigation();
            }
        });
        adapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                getCompanyStatus(data.channelValue);
            }
        });
    }

    /**
     * 获取公司是否有定制企业状态
     */
    private void getCompanyStatus(String companyCode) {
        OMAppApiProvider.getInstance().mCorporationQuery(companyCode, false, new rx.Observer<CorporationMsgResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(mContext, "网络错误，请检查网络").show();
            }

            @Override
            public void onNext(CorporationMsgResponse omBaseResponse) {
                if (omBaseResponse != null && omBaseResponse.isSuccess()) {
                    if ("1".equals(String.valueOf(omBaseResponse.data.customState)) && companyCode != null) {
                        ARouter.getInstance().build(RouterPathProvider.OnlineMeetingCustomizedEnterprisePageAct)
                                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                                .withString("companyCode", companyCode)
                                .withString("sourceWay", "2")
                                .navigation();
                    } else {
                        Toasty.normal(mContext, "抱歉，该企业未配置企业主页").show();
                    }
                } else {
                    Toasty.normal(mContext, omBaseResponse.msg).show();
                }

            }
        });
    }
}
