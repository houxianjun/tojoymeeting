package com.tojoy.cloud.online_meeting.App.Meeting.view;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.cloud.online_meeting.R;

import butterknife.ButterKnife;

public class NewsHeaderView {

    private Context mContext;
    private View mView;

    private TextView mTimeStampTv;
    private TextView mMsgContent;
    private TextView mUnreadCount;
    private RelativeLayout mUnreadLayout;

    public NewsHeaderView(Context context) {
        mContext = context;
    }

    public View getView() {
        if (mView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mView = aLayoutInflater.inflate(R.layout.headerview_news_layout, null);
        }
        ButterKnife.bind(this, mView);
        initUI();
        return mView;
    }

    private void initUI() {
        mTimeStampTv = mView.findViewById(R.id.tv_notice_time);
        mMsgContent = mView.findViewById(R.id.tv_notice_desc);
        mUnreadLayout = mView.findViewById(R.id.rlv_needpay_redtip);
        mUnreadCount = mView.findViewById(R.id.tv_needpay_num);

        mView.setOnClickListener(v -> {
            ARouter.getInstance().build(RouterPathProvider.SystemNotice)
                    .navigation();
            new Handler().postDelayed(() -> mUnreadLayout.setVisibility(View.GONE), 300);
        });
    }

    public void refresh(int unReadCount, String content, String timeStamp) {
        if (TextUtils.isEmpty(content)) {
            mTimeStampTv.setVisibility(View.GONE);
            mMsgContent.setText("暂无系统消息");
        } else {
            mMsgContent.setText(content);
            mTimeStampTv.setText(timeStamp);
            mTimeStampTv.setVisibility(View.VISIBLE);
        }

        if (unReadCount > 0) {
            mUnreadLayout.setVisibility(View.VISIBLE);
            mUnreadCount.setText(String.valueOf(unReadCount));
        } else {
            mUnreadLayout.setVisibility(View.GONE);
        }
    }

}
