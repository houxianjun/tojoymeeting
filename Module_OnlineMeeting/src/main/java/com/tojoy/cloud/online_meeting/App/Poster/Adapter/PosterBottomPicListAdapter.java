package com.tojoy.cloud.online_meeting.App.Poster.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.tojoy.cloud.online_meeting.App.Poster.Model.BottomPicModel;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.sys.AppUtils;

import java.util.List;


public class PosterBottomPicListAdapter extends BaseRecyclerViewAdapter<BottomPicModel> {

    public PosterBottomPicListAdapter(@NonNull Context context, @NonNull List<BottomPicModel> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull BottomPicModel data) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return 0;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new FileListVH(context, parent);
    }

    class FileListVH extends BaseRecyclerViewHolder<BottomPicModel> {
        Context mContext;
        RelativeLayout mContainer;
        ImageView mPreviewImg;
        ImageView mSelectedIv;

        FileListVH(Context context, ViewGroup parent) {
            super(context, parent, R.layout.item_bottom_poster_pic_view);
            this.mContext = context;
            mContainer = itemView.findViewById(R.id.rlv_filecontainer);
            mPreviewImg = itemView.findViewById(R.id.iv_preview);
            mSelectedIv = itemView.findViewById(R.id.iv_pic_selected);
        }

        @Override
        public void onBindData(BottomPicModel data, int position) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mContainer.getLayoutParams();

            if (data.isSelected) {
                params.height = AppUtils.dip2px(mContext,93);
                params.width = AppUtils.dip2px(mContext,70);
                mContainer.setBackgroundResource(R.drawable.bg_bottom_poster_pic_selected);
                mSelectedIv.setVisibility(View.VISIBLE);
            } else {
                params.height = AppUtils.dip2px(mContext,78);
                params.width = AppUtils.dip2px(mContext,58);
                mContainer.setBackgroundResource(0);
                mSelectedIv.setVisibility(View.GONE);
            }

            mContainer.setLayoutParams(params);
            mPreviewImg.setImageResource(data.picSource);
        }
    }
}
