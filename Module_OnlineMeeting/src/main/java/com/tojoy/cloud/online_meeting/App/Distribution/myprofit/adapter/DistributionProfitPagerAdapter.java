package com.tojoy.cloud.online_meeting.App.Distribution.myprofit.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tojoy.cloud.online_meeting.App.Distribution.myprofit.fragment.DistributionSettlementFragment;
import com.tojoy.tjoybaselib.ui.scrollableHelper.ScrollableHelper;

import java.util.List;

public class DistributionProfitPagerAdapter extends FragmentPagerAdapter {
    private final int PAGE_COUNT = 2;
    private String[] tableTitle = new String[] {"会议", "商品"};
    private List<DistributionSettlementFragment> mFragmentTab;

    public DistributionProfitPagerAdapter(FragmentManager fm, Context context, List<DistributionSettlementFragment> fragmentList) {
        super(fm);
        mFragmentTab = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentTab.get(position);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tableTitle[position];
    }

    public ScrollableHelper.ScrollableContainer getCurrentFragmentTab(int currentIndex){
        return mFragmentTab.get(currentIndex);
    }

}
