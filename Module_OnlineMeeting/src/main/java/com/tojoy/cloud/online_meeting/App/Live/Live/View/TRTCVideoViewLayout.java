package com.tojoy.cloud.online_meeting.App.Live.Live.View;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tencent.rtmp.TXLivePlayer;
import com.tencent.rtmp.ui.TXCloudVideoView;
import com.tencent.trtc.TRTCCloud;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.model.live.LiveUserInfoResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.net.NetWorkUtils;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.App.Config.LiveRoomConfig;
import com.tojoy.cloud.online_meeting.App.Live.ExporeOnline.ExploreOperationMenu;
import com.tojoy.cloud.online_meeting.App.Live.ExporeOnline.ExporeApiManager;
import com.tojoy.cloud.online_meeting.App.Live.Live.Activity.BaseLiveAct;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.LiveCustomMessageProvider;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.LiveRoomPlayer;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.OnlineMembersHelper;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.TRTCVideoViewHelper;
import com.tojoy.cloud.online_meeting.App.Live.Live.Helper.ViewComparator;
import com.netease.nim.uikit.business.liveroom.LiveCustomMessageCode;
import com.tojoy.cloud.online_meeting.App.Live.Live.Model.OnlineMember;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;
import com.tojoy.cloud.online_meeting.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;

import es.dmoral.toasty.Toasty;
import jp.wasabeef.glide.transformations.BlurTransformation;
import rx.Observer;

import static com.tencent.rtmp.TXLiveConstants.RENDER_MODE_ADJUST_RESOLUTION;
import static com.tencent.rtmp.TXLiveConstants.RENDER_MODE_FULL_FILL_SCREEN;
import static com.tojoy.cloud.online_meeting.App.Live.Live.Helper.ViewComparator.timeTag;

public class TRTCVideoViewLayout extends RelativeLayout {
    private final static String TAG = TRTCVideoViewLayout.class.getSimpleName();

    private final int liveUrlTag = R.string.str_tag_liveUrl;
    private final int outlivePlayerIndex = R.string.outlivePlayerIndex;
    private Context mContext;
    private ArrayList<TXCloudVideoView> mVideoViewList = new ArrayList<>(LiveRoomConfig.ROOM_MAX_USER);
    private RelativeLayout mLayout;
    private RelativeLayout mArrowContainer;
    private ImageView mArrowImg;

    private LinearLayout mGuestContainer;

    public ExploreOperationMenu mExploreOperationMenu;

    private int parentLeftMargin;
    private int leftMargin;
    private int subWidth;
    private int topMargin;
    private int lineHeight;
    private int operationHeight;
    private int arrowLineHeight;

    boolean isInBlock = false;

    private OperationCallback mOperationCallback;

    RelativeLayout.LayoutParams mBigParams;
    RelativeLayout.LayoutParams mSubParams1;
    RelativeLayout.LayoutParams mSubParams2;
    RelativeLayout.LayoutParams mSubParams3;
    RelativeLayout.LayoutParams mSubParams4;
    RelativeLayout.LayoutParams mSubParams5;
    RelativeLayout.LayoutParams mSubParams6;
    RelativeLayout.LayoutParams mSubParams7;
    RelativeLayout.LayoutParams mSubParams8;
    RelativeLayout.LayoutParams mGuestLayoutParams;
    RelativeLayout.LayoutParams mArrowLayoutParams;


    //
    ImageView mBlurCover;
    RelativeLayout mOpenHostVideo;

    public static final int DELAY_TIME = 500;

    public TRTCVideoViewHelper mTRTCVideoViewHelper;

    public TRTCVideoViewLayout(Context context) {
        super(context);
        initView(context);
    }

    public TRTCVideoViewLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public TRTCVideoViewLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public void setOperationCallback(OperationCallback operationCallback) {
        this.mOperationCallback = operationCallback;
    }

    private void initView(Context context) {
        mContext = context;
        LayoutInflater.from(context).inflate(R.layout.trtc_roomview, this);
        mLayout = findViewById(R.id.ll_mainview);
        mTRTCVideoViewHelper = new TRTCVideoViewHelper(context);
        initViews();
    }

    private void initViews() {
        // 左右距两边的间距(保持跟页面两边的按钮边距对齐)
        parentLeftMargin = AppUtils.dip2px(mContext, 10);
        // 同台的边距
        leftMargin = AppUtils.dip2px(mContext, 8);
        // 每个同台的宽高
        subWidth = (AppUtils.getWidth(mContext) - leftMargin * 3 - parentLeftMargin * 2) / 4;
        // 操作台的高度
        operationHeight = AppUtils.dip2px(mContext, 70);
        // 上间距
        topMargin = subWidth - operationHeight;
        // 第一行与第二行的高度
        lineHeight = AppUtils.dip2px(mContext, 8);
        // 上下箭头的高度
        arrowLineHeight = AppUtils.dip2px(mContext, 12);

        //大布局
        mBigParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        changeLayoutParams(false, 0);

        //底部游客
        mGuestContainer = new LinearLayout(mContext);
        mGuestContainer.setVisibility(GONE);

        mArrowContainer = new RelativeLayout(mContext);
        mArrowContainer.setVisibility(GONE);
        mArrowContainer.setId(R.id.id_video_arrow_container);
        mArrowContainer.setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isFastDoubleClick(R.id.id_video_arrow_container)) {
                mExploreOperationMenu.getView().setVisibility(GONE);
                mOperationCallback.hideTip();
                LogUtil.i(LiveLogTag.TRTCLayout, "ArrowClick");
                if (hasScrollToTop) {
                    scrollDown(null);
                } else {
                    scrollToUp(null);
                }
                if (mOperationCallback != null) {
                    mOperationCallback.setBottomGraientHeight(hasScrollToTop);
                }
            }
        });

        mArrowImg = new ImageView(mContext);
        mArrowImg.setImageResource(R.drawable.icon_outlive_arrowup);
        mArrowContainer.addView(mArrowImg);
        LayoutParams arrowLps = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        arrowLps.addRule(CENTER_HORIZONTAL);
        arrowLps.setMargins(0, AppUtils.dip2px(mContext, 4), 0, 0);
        mArrowImg.setLayoutParams(arrowLps);

        mLayout.removeAllViews();

        for (int i = 0; i < LiveRoomConfig.ROOM_MAX_USER; i++) {
            TXCloudVideoView cloudVideoView = new TXCloudVideoView(mContext);
            cloudVideoView.setId(1000 + i);
            cloudVideoView.setClickable(true);
            cloudVideoView.setTag(i);
            cloudVideoView.setTag(outlivePlayerIndex, i);
            cloudVideoView.setBackgroundColor(Color.BLACK);
            cloudVideoView.setRenderMode(RENDER_MODE_FULL_FILL_SCREEN);
            LayoutInflater.from(mContext).inflate(R.layout.trtc_videoview_toolbar, cloudVideoView);

            FrameLayout.LayoutParams lps = new FrameLayout.LayoutParams(subWidth, subWidth);
            lps.gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
            cloudVideoView.findViewById(R.id.default_userheader).setLayoutParams(lps);

            mVideoViewList.add(i, cloudVideoView);

            if (i == 1) {
                mLayout.addView(mGuestContainer);
                mLayout.addView(mArrowContainer);
            }
            mLayout.addView(cloudVideoView);

            if (i == 0) {
                cloudVideoView.setVisibility(VISIBLE);
                cloudVideoView.setLayoutParams(mBigParams);
                mGuestContainer.setLayoutParams(mGuestLayoutParams);
                mArrowContainer.setLayoutParams(mArrowLayoutParams);
                mBlurCover = cloudVideoView.findViewById(R.id.img_blur_cover);
                mOpenHostVideo = cloudVideoView.findViewById(R.id.rlv_live_end_tip);
                if (mBlurCover != null) {
                    String url = OSSConfig.getOSSURLedStr(LiveRoomInfoProvider.getInstance().coverOne);
                    Glide.with(mContext)
                            .load(url)
                            .apply(RequestOptions.bitmapTransform(new BlurTransformation(10, 10))
                                    .placeholder(0))
                            .into(mBlurCover);
                }

                mOpenHostVideo.setOnClickListener(v -> startLiveVideo(getCloudVideoViewByUseId(LiveRoomInfoProvider.getInstance().hostUserId)));
            } else {
                updateViewWithStatus(cloudVideoView, 0);
                cloudVideoView.setLayoutParams(getSubLayoutParams(i));
                cloudVideoView.setVisibility(GONE);
            }

            if (i > 4) {
                cloudVideoView.setAlpha(0);
            }

            cloudVideoView.setOnClickListener(v -> onCloudViewClicked(v));
        }

        mExploreOperationMenu = new ExploreOperationMenu(mContext);
        mLayout.addView(mExploreOperationMenu.getView());
    }

    /**
     * ==================== 进入房间成功
     */
    public void onRoomEnter() {
        try {
            updateLayoutFloat();
        } catch (Exception e) {
            LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "onRoomEnter Exception");
        }
    }

    /**
     * =================== 官方大会支持
     */
    public void startMeeting() {

        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "startMeeting");

        TXCloudVideoView videoView = getCloudVideoViewByUseId(LiveRoomInfoProvider.getInstance().hostUserId);
        if (videoView != null) {
            videoView.setVisibility(VISIBLE);
        }

        View container = getCloudVideoViewByIndex(0).findViewById(R.id.layout_toolbar);
        container.setVisibility(VISIBLE);
        container.bringToFront();

        View hostNo = container.findViewById(R.id.shortLeaveLayout);
        hostNo.setVisibility(GONE);

        LiveRoomPlayer meetingTXLivePlayer = mTRTCVideoViewHelper.getTXLivePlayer(0,LiveRoomInfoProvider.getInstance().hostUserId);
        meetingTXLivePlayer.userId = LiveRoomInfoProvider.getInstance().hostUserId;
        meetingTXLivePlayer.setPlayerView(getCloudVideoViewByIndex(0));
        meetingTXLivePlayer.startPlay(LiveRoomInfoProvider.getInstance().officalLiveUrl, TXLivePlayer.PLAY_TYPE_LIVE_FLV);

        if (LiveRoomInfoProvider.getInstance().isTemplateLeaveWhenInit()) {
            meetingTXLivePlayer.setVolume(0);
            getCloudVideoViewByIndex(0).setVisibility(GONE);
        } else {
            getCloudVideoViewByIndex(0).setVisibility(VISIBLE);
        }
    }

    /**
     * =================== 引流处理
     */
    public void startOutLive(String liveUrlFlv, String liveUrlRtmp, String streamId, boolean isInit) {
        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "streamId:" + streamId + "isInit:" + isInit);

        TXCloudVideoView videoView = onMemberEnter(streamId);
        videoView.setTag(liveUrlTag, liveUrlFlv);

        //5.19 同台/引流人数已满，再有人误入需要处理一下
        if (videoView == null) {
            return;
        }

        int selectedIndex = (int) videoView.getTag(outlivePlayerIndex);
        LiveRoomPlayer selectIndexTXLivePlayer = mTRTCVideoViewHelper.getTXLivePlayer(selectedIndex,streamId);
        selectIndexTXLivePlayer.userId = streamId;
        selectIndexTXLivePlayer.setPlayerView(videoView);
        selectIndexTXLivePlayer.startPlay(liveUrlFlv, TXLivePlayer.PLAY_TYPE_LIVE_FLV);
        selectIndexTXLivePlayer.setMute(true);
    }

    public void disOuterLive(String userId) {
        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "disOuterLive:" + userId);

        TXCloudVideoView cloudVideoView = getCloudVideoViewByUseId(userId);

        if (cloudVideoView == null) {
            return;
        }

        cloudVideoView.setTag(liveUrlTag, "");

        if (cloudVideoView == null) {
            return;
        }

        int index = (int) cloudVideoView.getTag(outlivePlayerIndex);
        LiveRoomPlayer indexTXLivePlayer = mTRTCVideoViewHelper.getTXLivePlayer(index,userId);
        indexTXLivePlayer.stopPlay(false);
        indexTXLivePlayer.setPlayerView(null);
        mTRTCVideoViewHelper.releaseTXLivePlayer(index);
        onMemberLeave(userId);
    }

    /**
     * =================== 暂离处理
     */
    public void refreshTemplateLeaveTip(String tip, String time, boolean isInit) {
        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "refreshTemplateLeaveTip:" + isInit);
        LiveRoomInfoProvider.getInstance().templateLeaveTip = "";
        ((TextView) mVideoViewList.get(0).findViewById(R.id.tvShortLeaveText)).setText(tip);
        TextView tvAutoStopTip = mVideoViewList.get(0).findViewById(R.id.tvAutoStopTip);
        if (!TextUtils.isEmpty(time) && !LiveRoomInfoProvider.getInstance().isGuest()) {
            // 时间不为空且是助手或主播，才显示，否则不显示
            tvAutoStopTip.setVisibility(VISIBLE);
            tvAutoStopTip.setText("您在" + time + "分钟内不回来会议视频会直接关闭哦");
        } else {
            tvAutoStopTip.setVisibility(INVISIBLE);
        }
        if (isInit) {
            TXCloudVideoView videoView = getCloudVideoViewByUseId(LiveRoomInfoProvider.getInstance().hostUserId);
            if (videoView != null) {
                videoView.findViewById(R.id.layout_toolbar).setVisibility(VISIBLE);
                videoView.setVisibility(VISIBLE);
                updateViewWithStatus(videoView, 3);
            }

            //5.23 移动处理ConcurrentModificationException：此时主播不在大屏，并且自己是主播，将主播切到大屏显示
            if (getCurrentBigIndex() != 0 && LiveRoomInfoProvider.getInstance().isHost()) {
                ExporeApiManager.mSetBigScreenUser(LiveRoomInfoProvider.getInstance().hostUserId);
                LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                        LiveCustomMessageProvider.getScreenChangeData(mContext, "", LiveCustomMessageCode.HOST_VIDEO_UP.getCode()), false);
            }
        }

        if (LiveRoomInfoProvider.getInstance().isOfficalLive() || LiveRoomInfoProvider.getInstance().isGuest()) {
            updateViewWithStatus(mVideoViewList.get(0), 3);
        }
    }

    /**
     * ================== 进出用户时做布局的获取与处理
     */
    //同台 & 引流 用户进入
    public TXCloudVideoView onMemberEnter(String userId) {

        if (TextUtils.isEmpty(userId)) {
            return null;
        }

        LogUtil.i(LiveLogTag.TRTCLayout, "onMemberEnter" + userId);

        TXCloudVideoView videoView = null;
        if (!isInBlock) {
            isInBlock = true;
        }
        for (int i = 0; i < mVideoViewList.size(); i++) {
            TXCloudVideoView renderView = mVideoViewList.get(i);

            if (renderView != null) {
                String vUserId = renderView.getUserId();

                //已经有对应User的视频窗口
                if (userId.equalsIgnoreCase(vUserId)) {
                    LogUtil.i(LiveLogTag.TRTCLayout, "onMemberEnter-equalsIgnoreCase" + userId);
                    isInBlock = false;
                    return renderView;
                }

                //没有该用户对应的窗口 则赋值一下
                if (videoView == null && TextUtils.isEmpty(vUserId)) {
                    renderView.setUserId(userId);
                    renderView.setTag(timeTag, System.currentTimeMillis());
                    videoView = renderView;
                }
            }
        }
        isInBlock = false;

        //5.19 同台/引流人数已满，再有人误入需要处理一下
        if (videoView == null) {
            return null;
        }

        updateViewWithStatus(videoView, 2);
        videoView.setVisibility(VISIBLE);

        try {
            updateLayoutFloat();
        } catch (Exception e) {
            LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "onMemberLeave:Exception");
        }

        refreshHostUI();

        mOperationCallback.checkShowTip1(false);

        return videoView;
    }

    //同台 & 引流 用户离开
    public void onMemberLeave(String userId) {
        LogUtil.i(LiveLogTag.TRTCLayout, "onMemberLeave" + userId);

        if (!isInBlock) {
            isInBlock = true;
        }

        for (int i = 0; i < mVideoViewList.size(); i++) {
            TXCloudVideoView renderView = mVideoViewList.get(i);
            if (renderView != null && null != renderView.getUserId()) {
                if (renderView.getUserId().equals(userId)) {
                    renderView.setUserId(null);
                    renderView.setVisibility(GONE);
                    updateViewWithStatus(renderView, 0);

                    //当前退出的用户在大屏上时需切换
                    if (getCurrentBigIndex() == i && i > 0) {
                        //5.17：当是大会视频的时候 需要开启声音,并且需要回到首页的时候
                        if (LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                            LogUtil.i(LiveLogTag.TRTCLayout, "onMemberLeave_isOfficalLive" + true);
                            muteGuestAudio(LiveRoomInfoProvider.getInstance().hostUserId, false);
                        }
                        LogUtil.i(LiveLogTag.TRTCLayout, "onMemberLeave_OnbigUp" + userId);
                        bigUp();
                        if (LiveRoomInfoProvider.getInstance().isHost()) {
                            ExporeApiManager.mSetBigScreenUser(LiveRoomInfoProvider.getInstance().hostUserId);
                            LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                                    LiveCustomMessageProvider.getScreenChangeData(mContext, "", LiveCustomMessageCode.HOST_VIDEO_UP.getCode()), false);
                        }
                    }
                }
            }
        }

        isInBlock = false;

        if (mExploreOperationMenu != null) {
            mExploreOperationMenu.getView().setVisibility(GONE);
            mOperationCallback.hideTip();
        }

        try {
            updateLayoutFloat();
        } catch (Exception e) {
            LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "onMemberLeave:updateLayoutFloat Exception");
        }

        new Handler().postDelayed(() -> refreshHostUI(), 500);
    }

    /**
     * 判断本地的视频窗口，主播是否在大屏
     *
     * @return
     */
    public boolean checkBigUpIsHostNow() {
        if (mVideoViewList != null && mVideoViewList.size() > 0) {
            TXCloudVideoView renderView = mVideoViewList.get(0);
            if (renderView != null
                    && renderView.getUserId() != null
                    && renderView.getUserId().equals(LiveRoomInfoProvider.getInstance().hostUserId)
                    && getCurrentBigIndex() == 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    /**
     * ==========================
     * ========================== 小窗弹窗操作：关闭同台、切换音视频、切换大小屏
     * ==========================
     */

    private void onCloudViewClicked(View v) {
        String vUserId = ((TXCloudVideoView) v).getUserId();

        if (TextUtils.isEmpty(vUserId)) {
            return;
        }
        LogUtil.i(LiveLogTag.LRMsgTag, "onCloudViewClicked--vUserId:" + vUserId);

        //观众并且不是点击自己
        if (LiveRoomInfoProvider.getInstance().isGuest() && !BaseUserInfoCache.isMySelf(mContext, vUserId)) {
            return;
        }

        if (hideMenuView()) {
            LogUtil.i(LiveLogTag.LRMsgTag, "onCloudViewClicked--hideMenuView,vUserId:" + vUserId);
            return;
        }

        mExploreOperationMenu.refreshUIByUserId(vUserId);

        LayoutParams layoutParams = (LayoutParams) v.getLayoutParams();
        if (layoutParams.width == subWidth) {
            LayoutParams childLps = new LayoutParams(AppUtils.dip2px(mContext, 100), ViewGroup.LayoutParams.WRAP_CONTENT);
            childLps.addRule(ALIGN_PARENT_BOTTOM);
            int subLeftMargin = layoutParams.leftMargin + (subWidth - AppUtils.dip2px(mContext, 100)) / 2;
            int subBottomMargin = layoutParams.bottomMargin + subWidth;
            childLps.setMargins(subLeftMargin, 0, 0, subBottomMargin);

            mOperationCallback.hideTip();
            mExploreOperationMenu.getView().setLayoutParams(childLps);
            mExploreOperationMenu.getView().setVisibility(VISIBLE);
            mExploreOperationMenu.getView().bringToFront();
            mExploreOperationMenu.setmExploreOperationCallback(new ExploreOperationMenu.ExploreOperationCallback() {
                @Override
                public void doSwitchScreen() {
                    mExploreOperationMenu.getView().setVisibility(GONE);
                    changeToBig(v, v == mVideoViewList.get(0));
                    mOperationCallback.checkShowTip2();
                }

                @Override
                public void doStopInvite() {
                    mExploreOperationMenu.getView().setVisibility(GONE);

                    if (LiveRoomInfoProvider.getInstance().isHost()) { //主播
                        ExporeApiManager.mStopExploreSomebody(mContext, ((TXCloudVideoView) v).getUserId());
                    } else if (LiveRoomInfoProvider.getInstance().isHostHelper()) {
                        if (!BaseUserInfoCache.isMySelf(mContext, ((TXCloudVideoView) v).getUserId())) {
                            ExporeApiManager.mStopExploreSomebody(mContext, ((TXCloudVideoView) v).getUserId());
                        } else {
                            closeMySelfExplore(mContext);
                        }
                    } else {
                        closeMySelfExplore(mContext);
                    }
                }

                @Override
                public void doStopVideo() {
                    mExploreOperationMenu.getView().setVisibility(GONE);

                    if (LiveRoomInfoProvider.getInstance().isHost(((TXCloudVideoView) v).getUserId()) && LiveRoomInfoProvider.getInstance().hasTemplateLeave) {
                        Toasty.normal(mContext, "暂离中").show();
                        return;
                    }

                    try {
                        ((BaseLiveAct) mContext).showProgressHUD(mContext, "");
                    } catch (Exception e) {

                    }

                    if (!NetWorkUtils.isNetworkConnected(mContext)) {
                        Toasty.normal(mContext, "当前网络不佳").show();
                    }
                    ExporeApiManager.mControlGuestMediaStatusWithObserver(((TXCloudVideoView) v).getUserId(), "0", "1", getCallIngStateOfNow(((TXCloudVideoView) v)),new Observer<OMBaseResponse>() {
                        @Override
                        public void onCompleted() {
                            try {
                                ((BaseLiveAct) mContext).dismissProgressHUD();
                            } catch (Exception e) {

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            try {
                                ((BaseLiveAct) mContext).dismissProgressHUD();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                            if (!NetWorkUtils.isNetworkConnected(mContext)) {
                                Toasty.normal(mContext, "当前网络不佳").show();
                            } else {
                                Toasty.normal(mContext, "网络异常，请稍后再试！").show();
                            }
                        }

                        @Override
                        public void onNext(OMBaseResponse response) {
                            if (!response.isSuccess()) {
                                Toasty.normal(mContext, response.msg).show();
                            } else {
                                LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                                        LiveCustomMessageProvider.getCloseSomebodyVideoData(mContext, ((TXCloudVideoView) v).getUserId()), false);
                                OnlineMembersHelper.getInstance().doStopVideo(((TXCloudVideoView) v).getUserId());
                                muteGuestVideo(((TXCloudVideoView) v).getUserId(), true);

//                                if (LiveRoomInfoProvider.getInstance().isHost(((TXCloudVideoView) v).getUserId())) {
//                                    if (LiveRoomInfoProvider.getInstance().isHost()) {
//                                        TRTCCloud.sharedInstance(mContext).muteLocalVideo(true);
//                                        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "muteLocalVideo--doStopVideo");
//                                    }
//                                }
                                if (BaseUserInfoCache.isMySelf(mContext,((TXCloudVideoView) v).getUserId())) {
                                    TRTCCloud.sharedInstance(mContext).stopLocalPreview();
                                    LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "stopLocalPreview--doStopVideo");
                                    TRTCCloud.sharedInstance(mContext).muteLocalVideo(true);
                                    LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "muteLocalVideo--doStopVideo");
                                } else {
                                    TRTCCloud.sharedInstance(mContext).stopRemoteView(((TXCloudVideoView) v).getUserId());
                                    LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "stopRemoteView--doStopVideo");
                                }
                                if (mExploreOperationMenu != null) {
                                    mExploreOperationMenu.refreshUIByUserId(((TXCloudVideoView) v).getUserId());
                                }
                            }
                        }
                    });
                }

                @Override
                public void doStartVideo() {
                    startLiveVideo(v);
                }

                @Override
                public void doStopOutlive() {
                    mExploreOperationMenu.getView().setVisibility(GONE);
                    mOperationCallback.disOuterLive(((TXCloudVideoView) v).getUserId(), true, null);
                    mExploreOperationMenu.getView().setVisibility(GONE);
                }

                @Override
                public void doStopAudio() {
                    mExploreOperationMenu.getView().setVisibility(GONE);

                    if (LiveRoomInfoProvider.getInstance().isHost(((TXCloudVideoView) v).getUserId()) && LiveRoomInfoProvider.getInstance().hasTemplateLeave) {
                        Toasty.normal(mContext, "暂离中").show();
                        return;
                    }

                    try {
                        ((BaseLiveAct) mContext).showProgressHUD(mContext, "");
                    } catch (Exception e) {

                    }
                    if (!NetWorkUtils.isNetworkConnected(mContext)) {
                        Toasty.normal(mContext, "当前网络不佳").show();
                    }
                    ExporeApiManager.mControlGuestMediaStatusWithObserver(((TXCloudVideoView) v).getUserId(), "0", "2", getCallIngStateOfNow(((TXCloudVideoView) v)), new Observer<OMBaseResponse>() {
                        @Override
                        public void onCompleted() {
                            try {
                                ((BaseLiveAct) mContext).dismissProgressHUD();
                            } catch (Exception e) {

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            try {
                                ((BaseLiveAct) mContext).dismissProgressHUD();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                            if (!NetWorkUtils.isNetworkConnected(mContext)) {
                                Toasty.normal(mContext, "当前网络不佳").show();
                            } else {
                                Toasty.normal(mContext, "网络异常，请稍后再试！").show();
                            }
                        }

                        @Override
                        public void onNext(OMBaseResponse response) {
                            if (response.isSuccess()) {

                                LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                                        LiveCustomMessageProvider.getCloseSomebodyAudioData(mContext, ((TXCloudVideoView) v).getUserId()), false);
                                OnlineMembersHelper.getInstance().doStopAudio(((TXCloudVideoView) v).getUserId());
                                muteGuestAudio(((TXCloudVideoView) v).getUserId(), true);

                                if (LiveRoomInfoProvider.getInstance().isHost() && LiveRoomInfoProvider.getInstance().isHost(((TXCloudVideoView) v).getUserId())) {
                                    //20200301:做200m延时处理要不然小概率开启语音失败，这里加了200ms延时，为了保障在下边的开启接口成功后，会再开启一次
                                    new Handler().postDelayed(() -> TRTCCloud.sharedInstance(mContext).muteLocalAudio(true), 200);
                                }
                                if (mExploreOperationMenu != null) {
                                    mExploreOperationMenu.refreshUIByUserId(((TXCloudVideoView) v).getUserId());
                                }

                            } else {
                                Toasty.normal(mContext, response.msg).show();
                            }
                        }
                    });
                }

                @Override
                public void doStartAudio() {
                    LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "startLiveAudio--doStartAudio==click");
                    startLiveAudio(v);
                }
            });
        }
    }


    /**
     * 切换大屏
     *
     * @param v
     * @param thisIsHost
     */
    private void changeToBig(View v, boolean thisIsHost) {
        try {
            ((BaseLiveAct) mContext).showProgressHUD(mContext, "");
        } catch (Exception e) {

        }

        String operateUserId = thisIsHost ? LiveRoomInfoProvider.getInstance().hostUserId : ((TXCloudVideoView) v).getUserId();

        ExporeApiManager.mSetBigScreenUserWithObserver(operateUserId, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
                try {
                    ((BaseLiveAct) mContext).dismissProgressHUD();
                } catch (Exception e) {

                }
            }

            @Override
            public void onError(Throwable e) {
                try {
                    ((BaseLiveAct) mContext).dismissProgressHUD();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                Toasty.normal(mContext, "网络异常，请稍后再试！").show();
            }

            @Override
            public void onNext(OMBaseResponse response) {
                if (response.isSuccess()) {
                    if (thisIsHost) {
                        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "bigUp--doSwitchScreen:==0,mSetBigScreenUserWithObserverSuccess");
                        bigUp();
                        LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                                LiveCustomMessageProvider.getScreenChangeData(mContext, "", LiveCustomMessageCode.HOST_VIDEO_UP.getCode()), false);

                        //四期 主播上屏的时候 如果没有开启语音
                        if (!OnlineMembersHelper.getInstance().isHostAudioOn && !LiveRoomInfoProvider.getInstance().hasTemplateLeave) {
                            LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "startLiveAudio--doSwitchScreen:==0,mSetBigScreenUserWithObserverSuccess");
                            startLiveAudio(v);
                        }
                    } else {
                        bigDown((TXCloudVideoView) v);
                        LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                                LiveCustomMessageProvider.getScreenChangeData(mContext, operateUserId, LiveCustomMessageCode.HOST_VIDEO_DOWN.getCode()), false);

                        //切换到大屏的时候，把对应的同台 或者 引流 开启声音
                        if (LiveRoomInfoProvider.getInstance().isHost(operateUserId) && LiveRoomInfoProvider.getInstance().hasTemplateLeave) {
                            // 若被切换的人是主播，则判断当前主播的暂离状态(主播暂离中不开启语音)
                            return;
                        }
                        {
                            LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                                    LiveCustomMessageProvider.getOpenSomebodyAudioData(mContext, operateUserId), false);
                            LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "startLiveAudio--doSwitchScreen:!=0,mSetBigScreenUserWithObserverSuccess");
                            OnlineMembersHelper.getInstance().doStartAudio(operateUserId);
                            ExporeApiManager.mControlGuestMediaStatus(operateUserId, "1", "2",getCallIngStateOfNow(((TXCloudVideoView) v)));
                            muteGuestAudio(operateUserId, false);
                        }
                    }
                } else {
                    Toasty.normal(mContext, response.msg).show();
                }
            }
        });
    }

    /**
     * 开启某个窗口的视频
     *
     * @param v
     */
    private void startLiveVideo(View v) {
        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "startLiveVideo");
        mExploreOperationMenu.getView().setVisibility(GONE);

        if (LiveRoomInfoProvider.getInstance().isHost(((TXCloudVideoView) v).getUserId()) && LiveRoomInfoProvider.getInstance().hasTemplateLeave) {
            Toasty.normal(mContext, "暂离中").show();
            return;
        }

        try {
            ((BaseLiveAct) mContext).showProgressHUD(mContext, "");
        } catch (Exception e) {

        }
        if (!NetWorkUtils.isNetworkConnected(mContext)) {
            Toasty.normal(mContext, "当前网络不佳").show();
        }
        ExporeApiManager.mControlGuestMediaStatusWithObserver(((TXCloudVideoView) v).getUserId(), "1", "1",getCallIngStateOfNow(((TXCloudVideoView) v)), new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
                try {
                    ((BaseLiveAct) mContext).dismissProgressHUD();
                } catch (Exception e) {

                }
            }

            @Override
            public void onError(Throwable e) {
                try {
                    ((BaseLiveAct) mContext).dismissProgressHUD();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                if (!NetWorkUtils.isNetworkConnected(mContext)) {
                    Toasty.normal(mContext, "当前网络不佳").show();
                } else {
                    Toasty.normal(mContext, "网络异常，请稍后再试！").show();
                }
            }

            @Override
            public void onNext(OMBaseResponse response) {
                if (!response.isSuccess()) {
                    Toasty.normal(mContext, response.msg).show();
                } else {
                    LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                            LiveCustomMessageProvider.getOpenSomebodyVideoData(mContext, ((TXCloudVideoView) v).getUserId()), false);
                    OnlineMembersHelper.getInstance().doStartVideo(((TXCloudVideoView) v).getUserId());
                    muteGuestVideo(((TXCloudVideoView) v).getUserId(), false);

//                    LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "startRemoteView--doStartVideo");
//                    if (LiveRoomInfoProvider.getInstance().isHost(((TXCloudVideoView) v).getUserId()) && LiveRoomInfoProvider.getInstance().isHost()) {
//                        TRTCCloud.sharedInstance(mContext).muteLocalVideo(false);
//                        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "muteLocalVideo--doStartVideo");
//                    }
                    if (BaseUserInfoCache.isMySelf(mContext,((TXCloudVideoView) v).getUserId())) {
                        TRTCCloud.sharedInstance(mContext).startLocalPreview(LiveRoomInfoProvider.getInstance().isFrontCamera,(TXCloudVideoView) v);
                        TRTCCloud.sharedInstance(mContext).muteLocalVideo(false);
                        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "muteLocalVideo--doStartVideo");
                        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "startLocalPreview--doStartVideo");
                    } else {
                        TRTCCloud.sharedInstance(mContext).startRemoteView(((TXCloudVideoView) v).getUserId(), (TXCloudVideoView) v);
                        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "startRemoteView--doStartVideo");
                    }

                    if (mExploreOperationMenu != null) {
                        mExploreOperationMenu.refreshUIByUserId(((TXCloudVideoView) v).getUserId());
                    }
                }
            }
        });
    }

    /**
     * 开启某个窗口的音频
     *
     * @param v
     */
    public void startLiveAudio(View v) {
        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "startLiveAudio");
        mExploreOperationMenu.getView().setVisibility(GONE);

        if (LiveRoomInfoProvider.getInstance().isHost(((TXCloudVideoView) v).getUserId()) && LiveRoomInfoProvider.getInstance().hasTemplateLeave) {
            Toasty.normal(mContext, "暂离中").show();
            return;
        }

        try {
            ((BaseLiveAct) mContext).showProgressHUD(mContext, "");
        } catch (Exception e) {

        }

        if (!NetWorkUtils.isNetworkConnected(mContext)) {
            Toasty.normal(mContext, "当前网络不佳").show();
        }
        ExporeApiManager.mControlGuestMediaStatusWithObserver(((TXCloudVideoView) v).getUserId(), "1", "2",getCallIngStateOfNow(((TXCloudVideoView) v)), new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
                try {
                    ((BaseLiveAct) mContext).dismissProgressHUD();
                } catch (Exception e) {

                }
            }

            @Override
            public void onError(Throwable e) {
                try {
                    ((BaseLiveAct) mContext).dismissProgressHUD();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                if (!NetWorkUtils.isNetworkConnected(mContext)) {
                    Toasty.normal(mContext, "当前网络不佳").show();
                } else {
                    Toasty.normal(mContext, "网络异常，请稍后再试！").show();
                }
            }

            @Override
            public void onNext(OMBaseResponse response) {
                if (!response.isSuccess()) {
                    Toasty.normal(mContext, response.msg).show();
                } else {
                    LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                            LiveCustomMessageProvider.getOpenSomebodyAudioData(mContext, ((TXCloudVideoView) v).getUserId()), false);
                    OnlineMembersHelper.getInstance().doStartAudio(((TXCloudVideoView) v).getUserId());
                    muteGuestAudio(((TXCloudVideoView) v).getUserId(), false);

                    if (LiveRoomInfoProvider.getInstance().isHost() && LiveRoomInfoProvider.getInstance().isHost(((TXCloudVideoView) v).getUserId())) {
                        //20200301:做200m延时处理要不然小概率开启语音失败，这里加了200ms延时，为了保障在下边的开启接口成功后，会再开启一次
                        new Handler().postDelayed(() -> TRTCCloud.sharedInstance(mContext).muteLocalAudio(false), 200);
                    }

                    if (mExploreOperationMenu != null) {
                        mExploreOperationMenu.refreshUIByUserId(((TXCloudVideoView) v).getUserId());
                    }
                }
            }
        });
    }

    public boolean hideMenuView() {
        if (mExploreOperationMenu.getView().getVisibility() == VISIBLE) {
            mExploreOperationMenu.getView().setVisibility(GONE);
            return true;
        }
        return false;
    }

    //助手 & 观众 关闭自己的同台
    private void closeMySelfExplore(Context context) {
        try {
            ((BaseLiveAct) context).showProgressHUD(context, "");
        } catch (Exception e) {

        }

        OMAppApiProvider.getInstance().mStopExploreSomebody(BaseUserInfoCache.getUserId(mContext),
                LiveRoomInfoProvider.getInstance().liveId, new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        try {
                            ((BaseLiveAct) context).dismissProgressHUD();
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(mContext, "网络异常，请稍后再试！").show();
                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        if (response.isSuccess()) {
                            mOperationCallback.disExploreMe();
                        } else {
                            Toasty.normal(mContext, response.msg).show();
                        }
                    }
                });
    }

    //切换同台观众的视频
    public void muteGuestVideo(String userId, boolean isClosed) {
        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "muteGuestVideo:userId:" + userId + isClosed);

        TXCloudVideoView cloudVideoView = getCloudVideoViewByUseId(userId);
        if (cloudVideoView == null) {
            return;
        }

        if (LiveRoomInfoProvider.getInstance().hasTemplateLeave && LiveRoomInfoProvider.getInstance().isHost(userId)) {
            updateViewWithStatus(cloudVideoView, 3);
        } else {
            updateViewWithStatus(cloudVideoView, isClosed ? 1 : 2);
        }

        if (LiveRoomInfoProvider.getInstance().isHost(userId)) {
            refreshHostUI();
        }
    }

    //切换引流的音频
    public void muteGuestAudio(String userId, boolean isClosed) {
        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "muteGuestAudio:userId:" + userId + isClosed);

        // 控制引流及大会视频音量
        boolean isMeetingHome = LiveRoomInfoProvider.getInstance().isOfficalLive() && LiveRoomInfoProvider.getInstance().isHost(userId);
        if (isMeetingHome) {
            mTRTCVideoViewHelper.getTXLivePlayer(0,userId).setMute(isClosed);
            LiveRoomInfoProvider.getInstance().isAudioOpen = !isClosed;
        } else if (!TextUtils.isEmpty(userId) && userId.length() >= 10) {
            TXCloudVideoView cloudVideoView = getCloudVideoViewByUseId(userId);
            if (cloudVideoView == null) {
                return;
            }

            int outliveIndex = (int) cloudVideoView.getTag(outlivePlayerIndex);
            mTRTCVideoViewHelper.getTXLivePlayer(outliveIndex,userId).setMute(isClosed);
        }
    }

    //大小屏切换
    public void bigUp() {
        LogUtil.i(LiveLogTag.TRTCLayout, "bigUp");
        if (isInBlock) {
            new Handler().postDelayed(() -> bigUp(), 500);
        } else {
            //如果主播音频关闭了，走一下开启(只有主播、助手自己才能触发主屏幕是自己的情况下声音开启)
            if (!OnlineMembersHelper.getInstance().isHostAudioOn
                    && !LiveRoomInfoProvider.getInstance().isGuest()
                    && !LiveRoomInfoProvider.getInstance().hasTemplateLeave) {
                LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "startLiveAudio--bigUp");
                startLiveAudio(getCloudVideoViewByUseId(LiveRoomInfoProvider.getInstance().hostUserId));
            }

            //暂存主播View
            ArrayList<TXCloudVideoView> temp = new ArrayList<>();
            temp.add(mVideoViewList.get(0));

            //设置主播大屏Params 并移除
            mVideoViewList.get(0).setLayoutParams(mBigParams);
            mVideoViewList.remove(0);

            //对剩余的View排序
            Collections.sort(mVideoViewList, new ViewComparator());

            for (int i = 0; i < mVideoViewList.size(); i++) {
                mVideoViewList.get(i).setLayoutParams(getSubLayoutParams(i + 1));
            }

            mVideoViewList.addAll(0, temp);
            mVideoViewList.get(0).bringToFront();

            refreshHostViewAfterBigViewChange(mVideoViewList.get(0));

            mGuestContainer.bringToFront();
            mArrowContainer.bringToFront();
            for (int i = 0; i < mVideoViewList.size(); i++) {
                if (i >= 1) {
                    mVideoViewList.get(i).bringToFront();
                }
                updateCloudViewOfCallPhone(mVideoViewList.get(i),mVideoViewList.get(i).getUserId());
                changeOuterLiveRenderModel(mVideoViewList.get(i));
            }

            if (mExploreOperationMenu != null) {
                mExploreOperationMenu.getView().setVisibility(GONE);
            }

            mTRTCVideoViewHelper.refreshViewAlpha(mVideoViewList, 0, hasScrollToTop);

            mOperationCallback.changeVideoQst();

        }
    }

    public void bigDown(TXCloudVideoView v) {
        if (v == null || TextUtils.isEmpty(v.getUserId())) {
            return;
        }

        LogUtil.i(LiveLogTag.TRTCLayout, "bigDown:" + v.getUserId());
        if (isInBlock) {
            new Handler().postDelayed(() -> bigDown(v), 500);
        } else {
            ArrayList<TXCloudVideoView> temp = new ArrayList<>();
            temp.add(mVideoViewList.get(0));
            temp.add(v);

            mVideoViewList.get(0).setLayoutParams(mSubParams1);
            v.setLayoutParams(mBigParams);

            mVideoViewList.remove(0);
            mVideoViewList.remove(v);

            Collections.sort(mVideoViewList, new ViewComparator());

            for (int i = 0; i < mVideoViewList.size(); i++) {
                mVideoViewList.get(i).setLayoutParams(getSubLayoutParams(i + 2));
            }

            mVideoViewList.addAll(0, temp);
            mVideoViewList.get(1).bringToFront();

            refreshHostViewAfterBigViewChange(mVideoViewList.get(0));

            mGuestContainer.bringToFront();
            mArrowContainer.bringToFront();
            for (int i = 0; i < mVideoViewList.size(); i++) {
                if (i != 1) {
                    mVideoViewList.get(i).bringToFront();
                }
                updateCloudViewOfCallPhone(mVideoViewList.get(i),mVideoViewList.get(i).getUserId());
                changeOuterLiveRenderModel(mVideoViewList.get(i));
            }

            if (mExploreOperationMenu != null) {
                mExploreOperationMenu.getView().setVisibility(GONE);
            }

            mTRTCVideoViewHelper.refreshViewAlpha(mVideoViewList, 0, hasScrollToTop);


            if (BaseUserInfoCache.isMySelf(mContext, v.getUserId())) {
                mOperationCallback.changeVideoQst();
            } else if (LiveRoomInfoProvider.getInstance().isHost()) {
                mOperationCallback.changeVideoQst();
            }

//            for (int i = 0; i < mVideoViewList.size(); i++) {
////                updateCloudViewOfCallPhone(mVideoViewList.get(i),mVideoViewList.get(i).getUserId());
//            }
        }

    }

    /**
     * 改变引流窗体的渲染模式
     * 若为引流：
     * width > heigh :等比例显示有黑边
     * width < heigh :铺满，宽度合适，高度会显示中间那部分
     */
    private void changeOuterLiveRenderModel(TXCloudVideoView v) {
        if (v == null || TextUtils.isEmpty(v.getUserId())) {
            return;
        }

        int index = getIndexBySelectUserId(v.getUserId());

        if (TextUtils.isEmpty(mVideoViewList.get(index).getUserId())) {
            return;
        }
        LiveRoomPlayer livePlayer = mTRTCVideoViewHelper.getmLivePlayerByUserId(mVideoViewList.get(index).getUserId());
//        LiveRoomPlayer currentTXLivePlayer = mTRTCVideoViewHelper.getTXLivePlayer(index);
        if (livePlayer == null || !mVideoViewList.get(index).getUserId().equals(livePlayer.userId)) {
            return;
        }
//        currentTXLivePlayer.userId = mVideoViewList.get(index).getUserId();
//        currentTXLivePlayer.setPlayerView(mVideoViewList.get(index));


        if (livePlayer.userId.length() > 10 || (LiveRoomInfoProvider.getInstance().isOfficalLive() && LiveRoomInfoProvider.getInstance().isHost(livePlayer.userId))) {
            // 引流窗口,判断此为大窗口还是小窗口
            OnlineMember onlineMember = OnlineMembersHelper.getInstance().getMemberByUserId(livePlayer.userId);
            if (onlineMember == null) {
                return;
            }
            if (getCurrentBigIndex() == index) {
                // 大屏幕显示
                onlineMember.isAtBigScreen = true;
            } else {
                // 小屏幕显示
                onlineMember.isAtBigScreen = true;
            }
//
//            if(onlineMember.width != 0){
//                if ((onlineMember.width > onlineMember.height)/** || player.isAtBigScreen**/) {
                    livePlayer.setRenderMode(RENDER_MODE_ADJUST_RESOLUTION);
//                } else {
//                    livePlayer.setRenderMode(RENDER_MODE_FULL_FILL_SCREEN);
//                }
//            }
        } else {
            // 非引流窗口，无论大、小屏幕均为铺满展示
            livePlayer.setRenderMode(RENDER_MODE_FULL_FILL_SCREEN);
        }

    }

    private void refreshHostViewAfterBigViewChange(TXCloudVideoView cloudVideoView) {
        LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "refreshHostViewAfterBigViewChange");

        if (!LiveRoomInfoProvider.getInstance().isHost(cloudVideoView.getUserId())) {
            return;
        }
        if (LiveRoomInfoProvider.getInstance().hasTemplateLeave) {
            updateViewWithStatus(cloudVideoView, 3);
            OMAppApiProvider.getInstance().getMemberInfo(BaseUserInfoCache.getUserId(mContext), LiveRoomInfoProvider.getInstance().liveId, new Observer<LiveUserInfoResponse>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                }

                @Override
                public void onNext(LiveUserInfoResponse response) {
                    if (response.isSuccess()) {
                        if (!"3".equals(response.data.roomLive.status)) {
                            LiveRoomInfoProvider.getInstance().hasTemplateLeave = false;
                            updateViewWithStatus(cloudVideoView, 2);
                        }
                    }
                }
            });
        } else {
            updateViewWithStatus(cloudVideoView, 2);
        }
        refreshHostUI();
    }


    /**
     * ==========================
     * ========================== 更新单个小窗里的内容及布局
     * ==========================
     */

    /**
     * 供外部调用，当某个小窗视频可见或者不可见的时候，或者更新当前主播暂离状态的时候
     * 通过userID判断当前是哪个弹窗
     * 通过bHasVideo判断是否有视频显示
     * <p>
     * 调用位置：
     * 1.TRTCCloudListenerImpl.onUserVideoAvailable:收到某个用户视频可见&不可见的时候的回调
     * 2.BaseLiveAct.onEnterRoomSuccess: 非主播进入房间后1.5s后未发现主播视频
     * 3.BaseLiveAct.templateLive:进入暂离状态
     * 4.BaseLiveAct.finishTempleteLeave:结束暂离状态
     * 5.BaseLiveAct.templateLiveMeeting:大会进入/结束暂离状态
     *
     * @param userID
     * @param bHasVideo
     */
    public void updateVideoStatus(String userID, boolean bHasVideo) {
        LogUtil.i(LiveLogTag.TRTCLayout, "updateVideoStatus:" + userID + bHasVideo);

        if (TextUtils.isEmpty(userID)) {
            return;
        }

        if (!isInBlock) {
            isInBlock = true;
        }
        for (int i = 0; i < mVideoViewList.size(); i++) {
            if (i < mVideoViewList.size()) {
                TXCloudVideoView videoView = mVideoViewList.get(i);
                String tempUserID = videoView.getUserId();
                if (!TextUtils.isEmpty(tempUserID) && tempUserID.equals(userID)) {
                    if (bHasVideo) {
                        updateViewWithStatus(videoView, 2);
                    } else {
                        if (LiveRoomInfoProvider.getInstance().isHost(tempUserID)) {
                            updateViewWithStatus(videoView, LiveRoomInfoProvider.getInstance().hasTemplateLeave ? 3 : 1);
                        } else {
                            updateViewWithStatus(videoView, 1);
                        }
                    }
//                    updateCloudViewOfCallPhone(videoView,videoView.getUserId());
                }
            }
        }
        isInBlock = false;
    }

    /**
     * 助手身份取消时，如是暂离状态，刷新显示样式
     */
    public void refreshTemplateLeaveView() {
        updateViewWithStatus(mVideoViewList.get(0), 3);
    }

    /**
     * 供内部调用
     * state对应使用场景：
     * 0：初始化或小窗释放后的重置
     * 1：显示非主播的视频不可见状态：手动关闭同台用户视频、同台用户因网络问题视频不可见
     * 2：视频窗口可见：新用户或同台进入、用户视频可见的回调、打开同台用户的视频
     * 3：暂离：主动暂离、看不到主播视频回调时的暂离、进入1.5s后看不到主播视频后的暂离
     *
     * @param cloudVideoView
     * @param state
     */
    public boolean isChangingOfficeMeetingStatus = false;

    private void updateViewWithStatus(TXCloudVideoView cloudVideoView, int state) {
        if (cloudVideoView == null) {
            return;
        }

        LogUtil.i(LiveLogTag.TRTCLayout, "switchVideoContent:" + cloudVideoView.getUserId() + ",state--" + state);

        View layoutToolbar = cloudVideoView.findViewById(R.id.layout_toolbar);
        View defaultHeader = cloudVideoView.findViewById(R.id.layout_no_video);
        View userheader = cloudVideoView.findViewById(R.id.default_userheader);
        View templateLeaveBig = cloudVideoView.findViewById(R.id.shortLeaveLayout);
        View templateLeaveSmall = cloudVideoView.findViewById(R.id.rlExploreSomebody);
        View isHostRl = cloudVideoView.findViewById(R.id.rlIsHost);
        TextView isHostIv = cloudVideoView.findViewById(R.id.ivIsHost);
        // 新增拨打电话蒙层
        View callOffHookView = cloudVideoView.findViewById(R.id.rlv_call_off_hook);
        ImageView callOffHookIv = cloudVideoView.findViewById(R.id.iv_lice_call_off_hook);
        TextView callOffHookTv = cloudVideoView.findViewById(R.id.tv_lice_call_off_hook);
        callOffHookView.setVisibility(GONE);

        layoutToolbar.setVisibility(VISIBLE);
        defaultHeader.setVisibility(GONE);
        userheader.setVisibility(GONE);
        templateLeaveSmall.setVisibility(GONE);
        templateLeaveBig.setVisibility(GONE);

        switch (state) {
            case 0://初始化状态：将用户头像显示成黑色半透明背景
                defaultHeader.setVisibility(VISIBLE);
                userheader.setBackground(mContext.getResources().getDrawable(R.drawable.bg_black_50));
                userheader.setVisibility(VISIBLE);
                layoutToolbar.bringToFront();
                if (LiveRoomInfoProvider.getInstance().isHost(cloudVideoView.getUserId())) {
                    if (!LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                        // 官方大会
                        refreshCallPhoneState(cloudVideoView.getUserId(),LiveRoomInfoProvider.getInstance().isCallPhoneIng(),cloudVideoView);
                    }
                } else {
                    // 同台其他人员
                    OnlineMember onlineMember = OnlineMembersHelper.getInstance().getMemberByUserId(cloudVideoView.getUserId());
                    refreshCallPhoneState(cloudVideoView.getUserId(),
                            (onlineMember != null && onlineMember.isCallPhoneIng),
                            cloudVideoView);
                }
                break;

            case 1: //观众上麦，视频关闭
                defaultHeader.setVisibility(VISIBLE);
                userheader.setBackground(mContext.getResources().getDrawable(R.mipmap.icon_no_video));
                userheader.setVisibility(VISIBLE);
                layoutToolbar.bringToFront();
                if (LiveRoomInfoProvider.getInstance().isHost(cloudVideoView.getUserId())) {
                    if (!LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                        // 官方大会
                        refreshCallPhoneState(cloudVideoView.getUserId(),LiveRoomInfoProvider.getInstance().isCallPhoneIng(),cloudVideoView);
                    }
                } else {
                    // 同台其他人员
                    OnlineMember onlineMember = OnlineMembersHelper.getInstance().getMemberByUserId(cloudVideoView.getUserId());
                    refreshCallPhoneState(cloudVideoView.getUserId(),
                            (onlineMember != null && onlineMember.isCallPhoneIng),
                            cloudVideoView);
                }
                break;

            case 2://观众 & 主播都正常在线
                //兼容大会
                defaultHeader.setVisibility(GONE);
                userheader.setVisibility(GONE);
                if (LiveRoomInfoProvider.getInstance().isOfficalLive() && LiveRoomInfoProvider.getInstance().isHost(cloudVideoView.getUserId())) {
                    getCloudVideoViewByIndex(0).setVisibility(VISIBLE);
                    layoutToolbar.setVisibility(VISIBLE);
                    layoutToolbar.bringToFront();
                    if (isChangingOfficeMeetingStatus) {
                        isChangingOfficeMeetingStatus = false;
                        mTRTCVideoViewHelper.getTXLivePlayer(0,cloudVideoView.getUserId()).setMute(false);
                    }
                }

                if (LiveRoomInfoProvider.getInstance().isHost(cloudVideoView.getUserId()) && getCurrentBigIndex() != 0) {//如果主播在小屏
                    isHostRl.setVisibility(VISIBLE);
                    isHostIv.setVisibility(VISIBLE);

                    new Handler().postDelayed(() -> {
                        layoutToolbar.bringToFront();
                        if (LiveRoomInfoProvider.getInstance().isHost(cloudVideoView.getUserId())) {
                            if (!LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                                // 官方大会
                                refreshCallPhoneState(cloudVideoView.getUserId(),LiveRoomInfoProvider.getInstance().isCallPhoneIng(),cloudVideoView);
                            }
                        }
                        isHostRl.bringToFront();
                        isHostIv.bringToFront();
                    }, DELAY_TIME);
                } else {
                    isHostRl.setVisibility(GONE);
                    isHostIv.setVisibility(GONE);
                    if (LiveRoomInfoProvider.getInstance().isHost(cloudVideoView.getUserId())) {
                        if (!LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                            // 官方大会
                            refreshCallPhoneState(cloudVideoView.getUserId(),LiveRoomInfoProvider.getInstance().isCallPhoneIng(),cloudVideoView);
                        }
                    } else {
                        // 同台其他人员
                        OnlineMember onlineMember = OnlineMembersHelper.getInstance().getMemberByUserId(cloudVideoView.getUserId());
                        refreshCallPhoneState(cloudVideoView.getUserId(),
                                (onlineMember != null && onlineMember.isCallPhoneIng),cloudVideoView);
                    }
                }
                break;
            case 3://主播暂离
                layoutToolbar.bringToFront();
                callOffHookView.setVisibility(GONE);
                //预防暂离做处理
                if (!LiveRoomInfoProvider.getInstance().hasTemplateLeave) {
                    return;
                }

                if (!LiveRoomInfoProvider.getInstance().isHost(cloudVideoView.getUserId())) {
                    return;
                }

                //7.31 增加暂离二次监测
                OMAppApiProvider.getInstance().getMemberInfo(BaseUserInfoCache.getUserId(mContext), LiveRoomInfoProvider.getInstance().liveId, new Observer<LiveUserInfoResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(LiveUserInfoResponse response) {
                        if (response.isSuccess()) {
                            if (!"3".equals(response.data.roomLive.status)) {
                                LiveRoomInfoProvider.getInstance().hasTemplateLeave = false;
                            }
                        }
                    }
                });


                if (getCurrentBigIndex() == 0) {//如果主播在大屏
                    templateLeaveBig.setVisibility(VISIBLE);
                    templateLeaveSmall.setVisibility(GONE);
                    isHostRl.setVisibility(GONE);
                    isHostIv.setVisibility(GONE);

                    if (LiveRoomInfoProvider.getInstance().isGuest()) {
                        cloudVideoView.findViewById(R.id.tvLiveGoOn).setVisibility(GONE);
                        cloudVideoView.findViewById(R.id.tvAutoStopTip).setVisibility(GONE);
                    } else {
                        ((TextView) cloudVideoView.findViewById(R.id.tvLiveGoOn)).setText("继续开会");
                        cloudVideoView.findViewById(R.id.tvLiveGoOn).setVisibility(VISIBLE);
                        if (!LiveRoomInfoProvider.getInstance().isGuest()) {
                            cloudVideoView.findViewById(R.id.tvAutoStopTip).setVisibility(VISIBLE);
                        }
                        TextView tvAutoStopTip = mVideoViewList.get(0).findViewById(R.id.tvAutoStopTip);
                        if (!TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().templateLeaveTime) && !LiveRoomInfoProvider.getInstance().isGuest()) {
                            // 时间不为空且是助手或主播，才显示，否则不显示
                            tvAutoStopTip.setVisibility(VISIBLE);
                            tvAutoStopTip.setText("您在" + LiveRoomInfoProvider.getInstance().templateLeaveTime + "分钟内不回来会议视频会直接关闭哦");
                        } else {
                            tvAutoStopTip.setVisibility(INVISIBLE);
                        }
                        cloudVideoView.findViewById(R.id.tvLiveGoOn).setOnClickListener(v -> {
                            LiveCustomMessageProvider.sendLiveRoomCustomMessage("",
                                    LiveCustomMessageProvider.getHostReStartData(mContext), false);
                            mOperationCallback.finishTempleteLeave();
                        });
                    }
                } else {//如果主播在小屏
                    templateLeaveBig.setVisibility(GONE);
                    templateLeaveSmall.setVisibility(VISIBLE);
                    isHostRl.setVisibility(VISIBLE);
                    isHostIv.setVisibility(VISIBLE);
                    TextView exploreSomebodyTipsTv = cloudVideoView.findViewById(R.id.tvExploreSomebodyTips);
                    exploreSomebodyTipsTv.setTextColor(getResources().getColor(R.color.color_cbcbcb));
                    exploreSomebodyTipsTv.setText("暂离");

                    new Handler().postDelayed(() -> {
                        layoutToolbar.bringToFront();
                        defaultHeader.bringToFront();
                        userheader.bringToFront();
                        callOffHookView.bringToFront();
                        callOffHookIv.bringToFront();
                        callOffHookTv.bringToFront();
                        templateLeaveSmall.bringToFront();
                        isHostRl.bringToFront();
                        isHostIv.bringToFront();
                    }, DELAY_TIME);
                }

                //判断针对大会的暂离状态需要隐藏视频播放器及关闭播放器声音
                if (LiveRoomInfoProvider.getInstance().isOfficalLive()) {
                    isChangingOfficeMeetingStatus = false;
                    mTRTCVideoViewHelper.getTXLivePlayer(0,"").setMute(true);
                }
                break;
            default:
                break;
        }
    }

    /**
     * 刷新接电话的蒙层状态
     * @param selectUserId
     * @param isCallIng
     */
    public void refreshCallPhoneState(String selectUserId ,boolean isCallIng,TXCloudVideoView cloudVideoView){
        if (TextUtils.isEmpty(selectUserId) || cloudVideoView == null || TextUtils.isEmpty(cloudVideoView.getUserId())) {
            return;
        }
        View layoutToolbar = cloudVideoView.findViewById(R.id.layout_toolbar);
        View defaultHeader = cloudVideoView.findViewById(R.id.layout_no_video);
        View userheader = cloudVideoView.findViewById(R.id.default_userheader);
        View templateLeaveBig = cloudVideoView.findViewById(R.id.shortLeaveLayout);
        View templateLeaveSmall = cloudVideoView.findViewById(R.id.rlExploreSomebody);
        View isHostRl = cloudVideoView.findViewById(R.id.rlIsHost);
        TextView isHostIv = cloudVideoView.findViewById(R.id.ivIsHost);

        View callOffHookView = cloudVideoView.findViewById(R.id.rlv_call_off_hook);
        ImageView callOffHookIv = cloudVideoView.findViewById(R.id.iv_lice_call_off_hook);
        TextView callOffHookTv = cloudVideoView.findViewById(R.id.tv_lice_call_off_hook);
        if (isCallIng) {
            if (getCurrentBigIndex() == getIndexBySelectUserId(selectUserId)) {
                //如果在大屏
                callOffHookIv.setVisibility(VISIBLE);
                callOffHookTv.setTextSize(16);
                callOffHookTv.setText(mContext.getString(R.string.live_call_phone_hint_bigup));
            } else {
                // 小窗口
                callOffHookIv.setVisibility(GONE);
                callOffHookTv.setTextSize(10);
                callOffHookTv.setText(mContext.getString(R.string.live_call_phone_hint_bigdown));
            }
            callOffHookView.setVisibility(VISIBLE);
            new Handler().postDelayed(() -> {
                layoutToolbar.bringToFront();
                defaultHeader.bringToFront();
                userheader.bringToFront();
                callOffHookView.bringToFront();
                callOffHookIv.bringToFront();
                callOffHookTv.bringToFront();
                isHostRl.bringToFront();
                isHostIv.bringToFront();
            }, 500);
        } else {
            callOffHookView.setVisibility(GONE);
            LogUtil.i(LiveLogTag.TRTCLayout, "refreshCallPhoneState:" + "\nlayoutToolbar:--"+ layoutToolbar.getVisibility()
                + "\ndefaultHeader:--" + defaultHeader.getVisibility()
                +"\nuserheader:--" + defaultHeader.getVisibility());

//            Toasty.normal(mContext,"layoutToolbar:--"+ layoutToolbar. ()
//                + "\ndefaultHeader:--" + defaultHeader.getVisibility()
//                +"\nuserheader:--" + defaultHeader.getVisibility()).show();
        }
    }

    /**
     * 更新当前同台、主播是否在拨打电话
     * state:
     * true：同台用户正在接听电话，此同台人员窗口需显示拨打电话的提示
     * false：同台用户已经挂断电话，此同台影院窗口需隐藏拨打电话的提示
     */
    public void updateCloudViewOfCallPhone(TXCloudVideoView cloudVideoView, String selectUserId) {
        if (cloudVideoView == null || TextUtils.isEmpty(selectUserId)) {
            return;
        }

        if (LiveRoomInfoProvider.getInstance().isHost(selectUserId) && LiveRoomInfoProvider.getInstance().isOfficalLive()) {
            // 官方大会直播中，且此人是obs主播，不触发回调
            return;
        }

        View layoutToolbar = cloudVideoView.findViewById(R.id.layout_toolbar);
        View defaultHeader = cloudVideoView.findViewById(R.id.layout_no_video);
        View userheader = cloudVideoView.findViewById(R.id.default_userheader);
        View templateLeaveBig = cloudVideoView.findViewById(R.id.shortLeaveLayout);
        View templateLeaveSmall = cloudVideoView.findViewById(R.id.rlExploreSomebody);
        View isHostRl = cloudVideoView.findViewById(R.id.rlIsHost);
        TextView isHostIv = cloudVideoView.findViewById(R.id.ivIsHost);

        View callOffHookView = cloudVideoView.findViewById(R.id.rlv_call_off_hook);
        ImageView callOffHookIv = cloudVideoView.findViewById(R.id.iv_lice_call_off_hook);
        TextView callOffHookTv = cloudVideoView.findViewById(R.id.tv_lice_call_off_hook);
        callOffHookView.setVisibility(GONE);

        if (LiveRoomInfoProvider.getInstance().isHost(selectUserId)) {
            LogUtil.i(LiveLogTag.TRTCLayout, "updateCloudViewOfCallPhone:" + selectUserId + ",state:" + LiveRoomInfoProvider.getInstance().isCallPhoneIng);
            // 主播
            if (LiveRoomInfoProvider.getInstance().isCallPhoneIng()) {
                callOffHookView.setVisibility(VISIBLE);

                if (getCurrentBigIndex() == getIndexBySelectUserId(selectUserId)) {
                    //如果在大屏
                    callOffHookIv.setVisibility(VISIBLE);
                    callOffHookTv.setTextSize(16);
                    callOffHookTv.setText(mContext.getString(R.string.live_call_phone_hint_bigup));
                } else {
                    // 小窗口
                    callOffHookIv.setVisibility(GONE);
                    callOffHookTv.setTextSize(10);
                    callOffHookTv.setText(mContext.getString(R.string.live_call_phone_hint_bigdown));
                }

                new Handler().postDelayed(() -> {
                    layoutToolbar.bringToFront();
                    defaultHeader.bringToFront();
                    userheader.bringToFront();
                    callOffHookView.bringToFront();
                    callOffHookIv.bringToFront();
                    callOffHookTv.bringToFront();
                    isHostRl.bringToFront();
                    isHostIv.bringToFront();
                }, 500);
            } else {
                callOffHookView.setVisibility(GONE);
            }
        } else {
            // 同台人员
            OnlineMember onlineMember = OnlineMembersHelper.getInstance().getMemberByUserId(selectUserId);
            LogUtil.i(LiveLogTag.TRTCLayout, "updateCloudViewOfCallPhone:" + selectUserId + ",state:" + (onlineMember != null ? "" : onlineMember.isCallPhoneIng + ""));
            if (onlineMember != null && onlineMember.isCallPhoneIng) {
                callOffHookView.setVisibility(VISIBLE);
                if (getCurrentBigIndex() == getIndexBySelectUserId(selectUserId)) {
                    //如果在大屏
                    callOffHookIv.setVisibility(VISIBLE);
                    callOffHookTv.setTextSize(16);
                    callOffHookTv.setText(mContext.getString(R.string.live_call_phone_hint_bigup));
                } else {
                    // 小窗口
                    callOffHookIv.setVisibility(GONE);
                    callOffHookTv.setTextSize(10);
                    callOffHookTv.setText(mContext.getString(R.string.live_call_phone_hint_bigdown));
                }

                new Handler().postDelayed(() -> {
                    layoutToolbar.bringToFront();
                    defaultHeader.bringToFront();
                    userheader.bringToFront();
                    callOffHookView.bringToFront();
                    callOffHookIv.bringToFront();
                    callOffHookTv.bringToFront();
                    isHostRl.bringToFront();
                    isHostIv.bringToFront();
                }, 500);
            } else {
                callOffHookView.setVisibility(GONE);
            }
        }
    }

    /**
     * 获取此窗口当前的电话接听状态
     * @param v
     * @return
     */
    public String getCallIngStateOfNow(TXCloudVideoView v){
        // 设置其他状态时要把此人当前的接打挂断状态传过去
        String isCallIng = "";
        if (LiveRoomInfoProvider.getInstance().isHost(v.getUserId())) {
            // 主播
            isCallIng = LiveRoomInfoProvider.getInstance().isCallPhoneIng ? "1" : "0";
        } else {
            // 同台人员
            OnlineMember onlineMember = OnlineMembersHelper.getInstance().getMemberByUserId(v.getUserId());
            if (onlineMember != null) {
                isCallIng = onlineMember.isCallPhoneIng ? "1" : "0";
            }
        }
        return isCallIng;
    }

    //四期检测毛玻璃
    public void refreshHostUI() {
        if (!OnlineMembersHelper.getInstance().isHostVideoOn) {
            try {
            TXCloudVideoView cloudVideoView = getCloudVideoViewByUseId(LiveRoomInfoProvider.getInstance().hostUserId);
            if(cloudVideoView!=null){
                View layoutToolbar = cloudVideoView.findViewById(R.id.layout_toolbar);
                View defaultHeader = cloudVideoView.findViewById(R.id.layout_no_video);
                View userheader = cloudVideoView.findViewById(R.id.default_userheader);

                userheader.setBackground(mContext.getResources().getDrawable(R.mipmap.icon_no_video));
//                layoutToolbar.bringToFront();

                //如果在大屏显示呢 则显示第一个封面并且毛玻璃处理
                //因为毛玻璃化比较耗性能，所以在进入直播间后就进行膜玻璃化，之后只做显引处理
                boolean checkIsBigUpHost = checkBigUpIsHostNow();
                mBlurCover.setVisibility(checkIsBigUpHost ? VISIBLE : GONE);
                defaultHeader.setVisibility(checkIsBigUpHost ? GONE : VISIBLE);
                userheader.setVisibility(checkIsBigUpHost ? GONE : VISIBLE);
                mOpenHostVideo.setVisibility(checkIsBigUpHost ? VISIBLE : GONE);
                if (LiveRoomInfoProvider.getInstance().isGuest()) {
                    mOpenHostVideo.setVisibility(GONE);
                }
            }
            }catch (Exception ex){
              LogUtil.e(LiveLogTag.TRTCLayout,ex.getStackTrace()+ex.getMessage());
            }
        } else {
            mBlurCover.setVisibility(GONE);
            mOpenHostVideo.setVisibility(GONE);
        }
    }

    /**
     * ==========================
     * ========================== 更新主页面布局
     * ==========================
     */

    /**
     * 调用时机：
     * 1.初始化页面：OnEnterRoom
     * 2.新增引流/同台：OnMemberEnter
     * 3.删除引流/同台：OnMemberLeave
     */
    int templateBigIndex = 0;

    private void updateLayoutFloat() throws Exception {
        LogUtil.i(LiveLogTag.TRTCLayout, "updateLayoutFloat");

        //1.先设置底部小窗及Container是否可见
        ArrayList<String> onlines = new ArrayList<>();
        if (!isInBlock) {
            isInBlock = true;
        }
        for (int i = 1; i < mVideoViewList.size(); i++) {
            TXCloudVideoView cloudVideoView = mVideoViewList.get(i);
            //1.cloudVideo对应的UserId不为空
            //2.不是主播的视频窗
            if (!TextUtils.isEmpty(LiveRoomInfoProvider.getInstance().hostUserId)
                    && !LiveRoomInfoProvider.getInstance().hostUserId.equals(cloudVideoView.getUserId())
                    && !TextUtils.isEmpty(cloudVideoView.getUserId())) {
                onlines.add(cloudVideoView.getUserId());
            }
        }
        mGuestContainer.setVisibility(onlines.size() > 0 ? VISIBLE : GONE);
        mArrowContainer.setVisibility(onlines.size() > 4 ? VISIBLE : GONE);
        isInBlock = false;

        //2.重新排序
        int bigIndex = getCurrentBigIndex();
        templateBigIndex = bigIndex;
        TXCloudVideoView bigCloudView = mVideoViewList.get(bigIndex);
        try {
            if (bigIndex == 0) {
                Collections.sort(mVideoViewList.subList(1, LiveRoomConfig.ROOM_MAX_USER), new ViewComparator());
            } else {
                //如果当前主播被切换到底部,当前大屏用户及主播用户不参与排序，排序完成之后再添加
                mVideoViewList.remove(bigIndex);
                //去除当前在大屏幕上，排序，设置
                Collections.sort(mVideoViewList.subList(1, LiveRoomConfig.ROOM_MAX_USER - 1), new ViewComparator());
            }
        } catch (Exception e) {
            LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "sort_ERROR");
        }


        //3.重新设置各个小窗的LayoutParams
        // 如果涉及到需自由弹出/关闭抽屉，动画执行完毕后再更新LayoutParams
        if (onlines.size() != OnlineMembersHelper.getInstance().mOnlineMembers.size()) {//小窗数量有变化
            if (onlines.size() == 5 && OnlineMembersHelper.getInstance().mOnlineMembers.size() == 4) {//在阈值处增加
                updateLayoutParams(bigIndex, bigCloudView);
            } else if (onlines.size() == 4 && OnlineMembersHelper.getInstance().mOnlineMembers.size() == 5) {//在阈值处减少
                if (hasScrollToTop) {
                    scrollDown(() -> updateLayoutParams(bigIndex, bigCloudView));
                } else {
                    updateLayoutParams(bigIndex, bigCloudView);
                }
            } else {
                updateLayoutParams(bigIndex, bigCloudView);
            }
        } else {
            updateLayoutParams(bigIndex, bigCloudView);
        }
    }

    /**
     * 排序完成之后重新布局 --- 针对小窗有增减
     * updateLayoutFloat的子方法：主要为了适配动画执行完之后调用，所以拆分出来
     *
     * @param bigIndex：当前大屏的Index
     * @param bigCloud：当前大屏的View
     * @apiNote 获取当前的大屏信息，方便设置完其他View后再次插入到mVideoViewList里
     */
    private void updateLayoutParams(int bigIndex, TXCloudVideoView bigCloud) {
        templateBigIndex = 0;

        try {
            boolean isBigAtUp = (bigIndex == 0);
            isInBlock = true;

            //针对非主播的小窗进行重新设置Params
            for (int i = 1; i < mVideoViewList.size(); i++) {
                TXCloudVideoView cloudVideoView = mVideoViewList.get(i);
                cloudVideoView.setClickable(true);
                cloudVideoView.setLayoutParams(getSubLayoutParams(isBigAtUp ? i : (i + 1)));
                mLayout.bringChildToFront(cloudVideoView);
            }

            //当主播在下边的时候 需要对主播设置Params
            if (!isBigAtUp) {
                mVideoViewList.get(0).setLayoutParams(mSubParams1);
            }

            isInBlock = false;
            if (!isBigAtUp) {
                mVideoViewList.add(1, bigCloud);
            }

            mTRTCVideoViewHelper.refreshViewAlpha(mVideoViewList, 0, hasScrollToTop);
        } catch (ConcurrentModificationException e) {
            LogUtil.i(LiveLogTag.TRTCVideoLayoutTag, "ConcurrentModificationException:resetViewListLayout");
        }
    }

    /**
     * ==========================
     * ========================== 基础布局服务方法
     * ==========================
     */
    private int getCurrentBigIndex() {
        if (!isInBlock) {
            isInBlock = true;
        }
        for (int i = 0; i < mVideoViewList.size(); i++) {
            if (mVideoViewList.get(i).getLayoutParams().width == LayoutParams.MATCH_PARENT) {
                isInBlock = false;
                return i;
            }
        }
        isInBlock = false;
        return 0;
    }

    private int getIndexBySelectUserId(String selectUserId){
        int index = 0;
        for (int i = 0; i < mVideoViewList.size(); i++) {
            String tempUserID = mVideoViewList.get(i).getUserId();
            if (tempUserID.equals(selectUserId)) {
                index = i;
                break;
            }
        }

        return index;
    }

    public TXCloudVideoView getCloudVideoViewByIndex(int index) {
        return mVideoViewList.get(index);
    }

    public TXCloudVideoView getCloudVideoViewByUseId(String userId) {
        if (!isInBlock) {
            isInBlock = true;
        }
        for (TXCloudVideoView videoView : mVideoViewList) {
            String tempUserID = videoView.getUserId();
            if (tempUserID != null && tempUserID.equalsIgnoreCase(userId)) {
                isInBlock = false;
                return videoView;
            }
        }
        isInBlock = false;
        return null;
    }

    private RelativeLayout.LayoutParams getSubLayoutParams(int index) {
        if (index == 1) {
            return mSubParams1;
        }

        if (index == 2) {
            return mSubParams2;
        }

        if (index == 3) {
            return mSubParams3;
        }

        if (index == 4) {
            return mSubParams4;
        }

        if (index == 5) {
            return mSubParams5;
        }

        if (index == 6) {
            return mSubParams6;
        }

        if (index == 7) {
            return mSubParams7;
        }

        return mSubParams8;
    }

    public boolean hasScrollToTop = false;

    private void scrollToUp(AnimationCallBack animationCallBack) {
        LogUtil.i(LiveLogTag.TRTCLayout, "scrollToUp");

        if (mExploreOperationMenu != null) {
            mExploreOperationMenu.getView().setVisibility(GONE);
        }

        ValueAnimator animator = ValueAnimator.ofFloat(0f, subWidth);
        animator.setDuration(400);
        animator.addUpdateListener(animation -> {
            float value = (float) animation.getAnimatedValue();
            changeLayoutParams(true, value);
            refreshLayoutParamsOnScrolling();
            if (value > subWidth / 2) {
                mArrowImg.setImageResource(R.drawable.icon_outlive_arrowdown);
            }
        });

        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                hasScrollToTop = true;
                if (animationCallBack != null) {
                    animationCallBack.onAnimationEnd();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        animator.start();

        mTRTCVideoViewHelper.refreshViewAlpha(mVideoViewList, 2, hasScrollToTop);
    }

    private void scrollDown(AnimationCallBack animationCallBack) {

        LogUtil.i(LiveLogTag.TRTCLayout, "scrollDown");

        if (mExploreOperationMenu != null) {
            mExploreOperationMenu.getView().setVisibility(GONE);
        }

        ValueAnimator animator = ValueAnimator.ofFloat(subWidth, 0);
        animator.setDuration(400);
        animator.addUpdateListener(animation -> {
            float value = (float) animation.getAnimatedValue();
            changeLayoutParams(true, value);
            refreshLayoutParamsOnScrolling();
            if (value < subWidth / 2) {
                mArrowImg.setImageResource(R.drawable.icon_outlive_arrowup);
            }
        });
        animator.start();
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                hasScrollToTop = false;
                if (animationCallBack != null) {
                    animationCallBack.onAnimationEnd();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        mTRTCVideoViewHelper.refreshViewAlpha(mVideoViewList, 1, hasScrollToTop);
    }

    private void refreshLayoutParamsOnScrolling() {
        boolean needCheckTemplateBigIndex = templateBigIndex > 0;

        int currentBigIndex = templateBigIndex > 0 ? templateBigIndex : getCurrentBigIndex();

        if (currentBigIndex == 0) {
            for (int i = 1; i < mVideoViewList.size(); i++) {
                mVideoViewList.get(i).setLayoutParams(getSubLayoutParams(i));
            }
        } else {
            for (int i = 0; i < mVideoViewList.size(); i++) {
                if (needCheckTemplateBigIndex) {
                    if (i == 0) {
                        mVideoViewList.get(0).setLayoutParams(getSubLayoutParams(1));
                    } else {
                        mVideoViewList.get(i).setLayoutParams(getSubLayoutParams(i + 1));
                    }
                } else {
                    if (i == currentBigIndex) {
                        continue;
                    }

                    if (i == 0) {
                        mVideoViewList.get(0).setLayoutParams(getSubLayoutParams(1));
                    } else if (i < currentBigIndex) {
                        mVideoViewList.get(i).setLayoutParams(getSubLayoutParams(i + 1));
                    } else {
                        mVideoViewList.get(i).setLayoutParams(getSubLayoutParams(i));
                    }
                }
            }
        }

        mGuestContainer.setLayoutParams(mGuestLayoutParams);
        mArrowContainer.setLayoutParams(mArrowLayoutParams);
    }

    private void changeLayoutParams(boolean isScrolling, float flingHeight) {
        float floatHeight = isScrolling ? flingHeight : 0;

        mSubParams1 = new RelativeLayout.LayoutParams(subWidth, subWidth);
        mSubParams1.setMargins(parentLeftMargin, 0, 0, (int) (operationHeight + floatHeight));
        mSubParams1.addRule(ALIGN_PARENT_BOTTOM);

        mSubParams2 = new RelativeLayout.LayoutParams(subWidth, subWidth);
        mSubParams2.setMargins(leftMargin + parentLeftMargin + subWidth, 0, 0, (int) (operationHeight + floatHeight));
        mSubParams2.addRule(ALIGN_PARENT_BOTTOM);

        mSubParams3 = new RelativeLayout.LayoutParams(subWidth, subWidth);
        mSubParams3.setMargins(leftMargin * 2 + parentLeftMargin + 2 * subWidth, 0, 0, (int) (operationHeight + floatHeight));
        mSubParams3.addRule(ALIGN_PARENT_BOTTOM);

        mSubParams4 = new RelativeLayout.LayoutParams(subWidth, subWidth);
        mSubParams4.setMargins(leftMargin * 3 + parentLeftMargin + 3 * subWidth, 0, 0, (int) (operationHeight + floatHeight));
        mSubParams4.addRule(ALIGN_PARENT_BOTTOM);

        mSubParams5 = new RelativeLayout.LayoutParams(subWidth, subWidth);
        mSubParams5.setMargins(parentLeftMargin, 0, 0, (int) (-(topMargin + lineHeight) + floatHeight));
        mSubParams5.addRule(ALIGN_PARENT_BOTTOM);

        mSubParams6 = new RelativeLayout.LayoutParams(subWidth, subWidth);
        mSubParams6.setMargins(leftMargin + parentLeftMargin + subWidth, 0, 0, (int) (-(topMargin + lineHeight) + floatHeight));
        mSubParams6.addRule(ALIGN_PARENT_BOTTOM);

        mSubParams7 = new RelativeLayout.LayoutParams(subWidth, subWidth);
        mSubParams7.setMargins(leftMargin * 2 + parentLeftMargin + 2 * subWidth, 0, 0, (int) (-(topMargin + lineHeight) + floatHeight));
        mSubParams7.addRule(ALIGN_PARENT_BOTTOM);

        mSubParams8 = new RelativeLayout.LayoutParams(subWidth, subWidth);
        mSubParams8.setMargins(leftMargin * 3 + parentLeftMargin + 3 * subWidth, 0, 0, (int) (-(topMargin + lineHeight) + floatHeight));
        mSubParams8.addRule(ALIGN_PARENT_BOTTOM);

        mGuestLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                (int) (subWidth + AppUtils.dip2px(mContext, 100) + floatHeight));
        mGuestLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        mArrowLayoutParams = new RelativeLayout.LayoutParams(AppUtils.dip2px(mContext, 82), AppUtils.dip2px(mContext, 22));
        mArrowLayoutParams.setMargins(0, 0, 0, (int) (operationHeight + subWidth + floatHeight + arrowLineHeight - 0.5));
        mArrowLayoutParams.addRule(ALIGN_PARENT_BOTTOM);
        mArrowLayoutParams.addRule(CENTER_HORIZONTAL);
    }

    /**
     * ==========================
     * ========================== 生命周期方法
     * ==========================
     */
    public void onPause() {
        LogUtil.i(LiveLogTag.TRTCLayout, "onPause");

        for (int i = 0; i < mVideoViewList.size(); i++) {
            LiveRoomPlayer livePlayer = mTRTCVideoViewHelper.getTXLivePlayer((int) mVideoViewList.get(i).getTag(outlivePlayerIndex),mVideoViewList.get(i).getUserId());
            if (livePlayer != null) {
                try {
                    mTRTCVideoViewHelper.getTXLivePlayer(i,mVideoViewList.get(i).getUserId()).pause();
                } catch (Exception e) {

                }
            }
        }
    }

    public void onResume() {
        LogUtil.i(LiveLogTag.TRTCLayout, "onResume");

        for (int i = 0; i < mVideoViewList.size(); i++) {
            LiveRoomPlayer livePlayer = mTRTCVideoViewHelper.getTXLivePlayer((int) mVideoViewList.get(i).getTag(outlivePlayerIndex),mVideoViewList.get(i).getUserId());

            if (livePlayer != null) {
                try {
                    mTRTCVideoViewHelper.getTXLivePlayer(i,mVideoViewList.get(i).getUserId()).resume();
                } catch (Exception e) {

                }
            }
        }
    }

    public void onDestory() {
        LogUtil.i(LiveLogTag.TRTCLayout, "onDestory");

        for (int i = 0; i < mVideoViewList.size(); i++) {
            LiveRoomPlayer txLivePlayer = mTRTCVideoViewHelper.getTXLivePlayer(i,mVideoViewList.get(i).getUserId());
            if (txLivePlayer != null && txLivePlayer.isPlaying()) {
                txLivePlayer.stopPlay(false);
                txLivePlayer.setPlayerView(null);
            }
            mTRTCVideoViewHelper.releaseTXLivePlayer(i);
            mVideoViewList.get(i).setUserId("");
        }
    }

    /**
     * ==========================
     * ========================== Callbacks
     * ==========================
     */
    private interface AnimationCallBack {
        void onAnimationEnd();
    }
}
