package com.tojoy.cloud.online_meeting.App.Distribution.view.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.configs.UrlProvider;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * @author houxianjun
 * @Date 2020/07/15
 */
public class ApplyDistributionPopView extends PopupWindow {
    private Activity mContext;
    private ImageView mPopClose;
    private TextView applyPopleNameTv;
    private TextView applyPoplePhoneTv;
    private CheckBox selectedLookCb;
    private ImageView commitIv;
    private TextView distributionManagerAgreeTv;

    public ApplyDistributionPopView(Context context) {
        super(context);
        mContext = (Activity) context;
        initUI();
    }

    private void initUI() {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.popup_apply_distribution, null);
        setContentView(mView);
        mPopClose = mView.findViewById(R.id.popClose);
        applyPopleNameTv = mView.findViewById(R.id.tv_apply_pople_name);
        applyPoplePhoneTv = mView.findViewById(R.id.tv_apply_pople_phone);
        selectedLookCb = mView.findViewById(R.id.cb_selected_look);
        commitIv = mView.findViewById(R.id.iv_commit);
        distributionManagerAgreeTv = mView.findViewById(R.id.tv_distribution_manager_agree);
        setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        setAnimationStyle(R.style.distribution_pop_anim_scale_alpha);
        setBackgroundDrawable(new ColorDrawable());
        setFocusable(true);
        setOutsideTouchable(false);
        setTouchable(true);
        setData();
        initListener();
    }

    private void setData() {
        applyPopleNameTv.setText(BaseUserInfoCache.getUserName(mContext));
        applyPoplePhoneTv.setText(BaseUserInfoCache.getUserMobile(mContext));
    }

    /**
     * 显示时屏幕变暗
     */
    public void lightOff() {
        WindowManager.LayoutParams layoutParams = mContext.getWindow().getAttributes();
        layoutParams.alpha = 0.5f;
        mContext.getWindow().setAttributes(layoutParams);
        //showCloseIcon();
    }


    private void initListener() {
        /**
         * 消失时屏幕变亮
         */
        setOnDismissListener(() -> {
            WindowManager.LayoutParams layoutParams = mContext.getWindow().getAttributes();
            layoutParams.alpha = 1.0f;
            mContext.getWindow().setAttributes(layoutParams);
        });
        mPopClose.setOnClickListener(view -> {
            dismiss();
        });

        selectedLookCb.setOnCheckedChangeListener((buttonView, isChecked) -> {
            updateCheckBox(isChecked);
        });
        /**
         * 查看协议
         */
        distributionManagerAgreeTv.setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                ARouter.getInstance().build(RouterPathProvider.WEBNew)
                        .withString("url", UrlProvider.DISTRIBUTION_AGREEMENT)
                        .withString("title", "")
                        .navigation();
            }
        });

        commitIv.setOnClickListener(view -> {
            if (!selectedLookCb.isChecked()) {
                Toasty.normal(mContext, "需同意推广大使合作协议").show();
                return;
            }

            OMAppApiProvider.getInstance().queryApplyForDistributor(new Observer<OMBaseResponse>() {
                @Override
                public void onCompleted() {
                    dismiss();
                }

                @Override
                public void onError(Throwable e) {
                    dismiss();
                }

                @Override
                public void onNext(OMBaseResponse omBaseResponse) {
                    dismiss();
                    if (omBaseResponse.isSuccess()) {
                        Toasty.normal(mContext, omBaseResponse.msg).show();

                    } else {
                        Toasty.normal(mContext, omBaseResponse.msg).show();

                    }
                }
            });
        });
    }

    private void updateCheckBox(boolean isAgree) {
        if (isAgree) {
            selectedLookCb.setChecked(true);
        } else {
            selectedLookCb.setChecked(false);
        }
    }
}
