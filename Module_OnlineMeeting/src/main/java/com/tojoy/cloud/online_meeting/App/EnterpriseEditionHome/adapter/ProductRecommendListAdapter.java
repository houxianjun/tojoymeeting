package com.tojoy.cloud.online_meeting.App.EnterpriseEditionHome.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.common.LiveRoom.MeetingExtensionPopView;
import com.tojoy.common.Services.Distribution.DistrbutionHelper;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.live.GoodsResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.string.StringUtil;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * @author fanxi
 * @date 2020-03-09.
 * description：
 */
public class ProductRecommendListAdapter extends BaseRecyclerViewAdapter<GoodsResponse.DataBean.RecordsBean> {

    private Context mContext;
    /**
     * 若来源为直播间，需暂时记录直播间信息
     */
    private String roomLiveId;
    private String roomId;
    private String isFromLive;
    private String sourceWay;

    public ProductRecommendListAdapter(@NonNull Context context, @NonNull List<GoodsResponse.DataBean.RecordsBean> datas) {
        super(context, datas);
        mContext = context;
    }

    /**
     * 设置页面来源（直播间/其他）
     */
    public void setPageSource(String isFrom,String roomLiveId, String roomId,String sourceWay) {
        this.roomId = roomId;
        this.roomLiveId = roomLiveId;
        this.isFromLive = isFrom;
        this.sourceWay=sourceWay;
    }
    @Override
    protected int getViewType(int position, @NonNull GoodsResponse.DataBean.RecordsBean data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_enterprise_goods_layouot;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        CustomizedEnterprisePageHolder holder = new CustomizedEnterprisePageHolder(context, getLayoutResId(viewType));
        return holder;
    }

    public class CustomizedEnterprisePageHolder extends BaseRecyclerViewHolder<GoodsResponse.DataBean.RecordsBean> {
        private TextView mCommodityNameTv, mCommodityPriceTv, tvCommodityReactFloat;
        private ImageView mCommdityIconIv;
        private View rootView;
        //新增加的属性
        private TextView mGoodIntroduce;
        private TextView mTvNum;
        private TextView mDistributionMoneyTv;
        private LinearLayout mDistributionLlv;

        CustomizedEnterprisePageHolder(Context context, int layoutResId) {
            super(context, layoutResId);
            rootView = itemView;
            mCommodityNameTv = itemView.findViewById(R.id.tvCommodityName);
            mCommodityPriceTv = itemView.findViewById(R.id.tvCommodityReactPrice);
            mCommdityIconIv = itemView.findViewById(R.id.ivCommdityIcon);
            mGoodIntroduce = itemView.findViewById(R.id.tvCommodityIntroduce);
            mTvNum = itemView.findViewById(R.id.tv_watch_num);
            tvCommodityReactFloat = itemView.findViewById(R.id.tvCommodityReactFloat);
            mDistributionMoneyTv = itemView.findViewById(R.id.tv_distribution_money);
            mDistributionLlv = itemView.findViewById(R.id.llv_distribution_goods);
        }

        @Override
        public void onBindData(GoodsResponse.DataBean.RecordsBean data, int position) {
            mCommodityNameTv.setText(data.name);
            mGoodIntroduce.setText(data.subhead);

            if (TextUtils.isEmpty(data.imgurl)) {
                ImageLoaderManager.INSTANCE.loadImage(mContext, mCommdityIconIv, data.liveRoomCoverImgUrl,
                        R.drawable.nim_image_default, 2);
            } else {
                ImageLoaderManager.INSTANCE.loadImage(mContext, mCommdityIconIv, data.imgurl,
                        R.drawable.nim_image_default, 2);
            }
            mTvNum.setText(data.visitNum);

            String price = TextUtils.isEmpty(data.salePrice) ? "0" : data.salePrice;
            mCommodityPriceTv.setText(StringUtil.getFormatedIntString(price));
            tvCommodityReactFloat.setText(StringUtil.getFormatedMinString(price));

            if (DistrbutionHelper.isShowDistribution(context,data.ifDistribution,data.companyCode)) {
                mDistributionLlv.setVisibility(View.VISIBLE);
                mDistributionMoneyTv.setText(DistrbutionHelper.getMaxBudgetText(context
                        ,data.level1Commission
                        ,data.level2Commission
                        ,data.level1CommissionEmployee
                        ,data.level2CommissionEmployee
                        ,data.companyCode,false));
            } else {
                mDistributionLlv.setVisibility(View.GONE);
            }
            MeetingExtensionPopView popView = new MeetingExtensionPopView(context);
            mDistributionLlv.setOnClickListener(v -> {
                if (!FastClickAvoidUtil.isFastDoubleClick(mDistributionLlv.getId())) {
                    if (DistrbutionHelper.isNeedJumpPop(context,data.level1CommissionEmployee,data.level2CommissionEmployee,data.companyCode)) {
                        if (popView.isShowing()) {
                            return;
                        }
                        popView.setGoodsData(data.id
                                ,data.name
                                ,!TextUtils.isEmpty(data.imgurl) ? data.imgurl : data.liveRoomCoverImgUrl
                                ,DistrbutionHelper.getDistributionMoney(context
                                        ,data.level1Commission
                                        ,data.level2Commission
                                        ,data.level1CommissionEmployee
                                        ,data.level2CommissionEmployee
                                        ,data.companyCode)
                                ,data.minPerson
                                ,data.minViewTime
                                ,data.salePrice);
                        popView.show();
                    } else {
                        // 判断二维码内容是否为空，为空生成海报没有意义
                        if (!TextUtils.isEmpty(BaseUserInfoCache.getUserInviteUrl(context))) {
                            ARouter.getInstance()
                                    .build(RouterPathProvider.ONLINE_MEETING_EXTEN_SION_POSTER)
                                    .withInt("fromType", 2)
                                    .withString("goodsId",data.id)
                                    .withString("goodsTitle",data.name)
                                    .withString("goodsPrice",data.salePrice)
                                    .withString("mLiveCoverUrl",!TextUtils.isEmpty(data.imgurl) ? data.imgurl : data.liveRoomCoverImgUrl)
                                    .navigation();
                        } else {
                            Toasty.normal(context, context.getResources().getString(R.string.extension_qr_error)).show();
                        }
                    }
                }
            });

            rootView.setOnClickListener(v -> {
                if (FastClickAvoidUtil.isFastDoubleClick(rootView.getId())) {
                    return;
                }
                if (!TextUtils.isEmpty(isFromLive) && "1".equals(isFromLive)) {
                    // 从直播间跳过来的，来源记录为app直播间
                    RouterJumpUtil.startGoodsDetailFromLiveRoom(data.id, roomId,roomLiveId,sourceWay!=null?sourceWay:"0","","","0");
                } else {
                    RouterJumpUtil.startGoodsDetails(data.id,sourceWay!=null?sourceWay:"2","","","0");
                }
            });

        }
    }
}
