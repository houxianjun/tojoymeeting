package com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.fragment.DistributionMyWithdrawalFragment;
import com.tojoy.cloud.online_meeting.App.Distribution.view.dialog.PayChannelUnbindDialog;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 我的提现-接触绑定
 */
public class DistributionWithdrawalUnbindView extends RelativeLayout {
    private Context mContext;
    private DistributionMyWithdrawalFragment mainFragment;
    private TextView unbindChannelTv;
    private TextView bindTipsTv;

    public void setMainFragment(DistributionMyWithdrawalFragment mainFragment) {
        this.mainFragment = mainFragment;
    }

    public DistributionWithdrawalUnbindView(Context context) {
        super(context);
        initView(context);
    }

    public DistributionWithdrawalUnbindView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public void initView(Context context) {
        mContext = context;
        View mView = LayoutInflater.from(context).inflate(R.layout.layout_distribution_withdrawal_record, null);
        initUI(mView);
        this.addView(mView);
    }

    public void initUI(View layout) {
        bindTipsTv = layout.findViewById(R.id.tv_bind_tips);
        unbindChannelTv = layout.findViewById(R.id.tv_unbind_channel);
        unbindChannelTv.setOnClickListener(v -> {
            //连续点击限制
            if (!FastClickAvoidUtil.isFastDoubleClick(unbindChannelTv.getId())) {
                new PayChannelUnbindDialog(mContext
                        , view -> queryAliAccountUnBinding())
                        .setTitleAndCotent("是否解除绑定？解除后无法提现～")
                        .show();
            }
        });

    }

    /**
     * 提现文案
     */
    public void setBindTipsContent(String limitMoney) {
        String explain_content = "<html>\n" +
                "\n" +
                "<body>\n" +
                "\n" +
                "<p style=\"color:#ffb1b5c0; font-size:14sp\">温馨提示：</p>\n\n" +
                "<p style=\"color:#ffb1b5c0; font-size:14sp\">1、提现每笔" + limitMoney + "元起提，每个账户提现次数为1次/天</p>\n" +
                "<p style=\"color:#ffb1b5c0; font-size:14sp\">2、根据《反洗钱法》规定，禁止洗钱、信用卡套现、虚假交易等行为。平台对有上述可疑行为的用户发起的提现将采取延迟付款或终止该账户的使用</p>\n" +
                "<p style=\"color:#ffb1b5c0; font-size:14sp\">3、用户提现时佣金个税代收代缴提示</p>\n" +
                "<p style=\"color:#ffb1b5c0; font-size:13sp\">4、如有疑问，请联系在线客服</p>\n" + " \n" +
                "</body>\n" +
                "\n" +
                "</html>";
        bindTipsTv.setText(Html.fromHtml(explain_content));
    }

    public void updateBindBtnStatus(int status) {
        switch (status) {
            //未绑定
            case 0:
                unbindChannelTv.setVisibility(GONE);
                break;
            //已绑定
            case 1:
                unbindChannelTv.setVisibility(VISIBLE);
                break;
            default:
                break;
        }
    }

    /**
     * 解除用户绑定
     */
    public void queryAliAccountUnBinding() {
        String id = mainFragment.getAccountBind().id;
        OMAppApiProvider.getInstance().queryAliAccountUnBinding(id, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(OMBaseResponse response) {
                if (response.isSuccess()) {
                    mainFragment.queryAliAccountBindingStatus();
                } else {
                    Toasty.normal(mContext, response.msg).show();
                }
            }
        });
    }
}
