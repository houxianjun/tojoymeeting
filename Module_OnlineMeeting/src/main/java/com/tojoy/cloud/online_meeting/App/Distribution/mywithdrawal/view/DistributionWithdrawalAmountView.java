package com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.view;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.fragment.DistributionMyWithdrawalFragment;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.Services.PayAndShare.callback.PayChannelStatusCallBack;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.home.MineDistributionWithdarwalApplyResponse;
import com.tojoy.tjoybaselib.model.home.MineWithdrawalAccountBindResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 我的提现-提现金额
 */
public class DistributionWithdrawalAmountView extends RelativeLayout {
    private static int counter = 0;

    private Context mContext;

    private TextView mCanWithdrawalAmountTv;

    private TextView mWithdrawalNextTv;

    private EditText channelMoneyEt;

    private DistributionMyWithdrawalFragment mainFragment;

    //最低提现额度
    private Double mlimitwithdrawalMoney = 0.00;

    public void setLimitWithdrawalMoney(Double limitwithdrawalMoney) {
        this.mlimitwithdrawalMoney = limitwithdrawalMoney;
    }

    public DistributionWithdrawalAmountView(Context context) {
        super(context);
        initView(context);
    }

    public DistributionWithdrawalAmountView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public void initView(Context context) {
        mContext = context;
        View mView = LayoutInflater.from(context).inflate(R.layout.layout_distribution_withdrawal_amount, null);
        initUI(mView);
        this.addView(mView);
    }

    public void setMainFragment(DistributionMyWithdrawalFragment mainFragment) {
        this.mainFragment = mainFragment;
    }

    public void initUI(View layout) {
        mWithdrawalNextTv = layout.findViewById(R.id.tv_widthdrawal_next);

        mWithdrawalNextTv.setOnClickListener(v -> {
            //连续点击限制
            if (!FastClickAvoidUtil.isFastDoubleClick(mWithdrawalNextTv.getId())) {
                String amount = channelMoneyEt.getText().toString();
                MineWithdrawalAccountBindResponse.AccountBind accountBind = mainFragment.getAccountBind();

                //已绑定
                if (accountBind.isBinding()) {
                    if (TextUtils.isEmpty(amount)) {
                        Toasty.normal(mContext, "提现金额不能为空").show();
                        return;
                    }
                    if (".".equals(amount.substring(amount.length() - 1))) {
                        //补全小数点后两位
                        amount = amount + "00";
                        channelMoneyEt.setText(amount);
                    }

                    if (mlimitwithdrawalMoney > Double.parseDouble(amount)) {
                        Toasty.normal(mContext, "您未达到最低提现额度，暂时不能提现！").show();
                        return;
                    }

                    querytDistributionWithdrawApply(amount);
                } else {
                    mainFragment.getPayAliChannel().bingPayChannel((Activity) mContext, new PayChannelStatusCallBack<String>() {
                        @Override
                        public void onFail(String msg) {

                            // Toasty.normal(mContext,msg).show();
                        }

                        @Override
                        public void onSuccess(String content) {

                            queryAliAccountBinding(content);
                        }
                    });
                }

            }
        });

        channelMoneyEt = layout.findViewById(R.id.et_channel_money);
        channelMoneyEt.addTextChangedListener(textWatcher);
        mCanWithdrawalAmountTv = layout.findViewById(R.id.tv_can_withdrawal_amount);
    }

    //可提现金额
    public void setAmount(String amount) {
        mCanWithdrawalAmountTv.setText("(可提现金额：" + amount + "元)");
    }

    //根据绑定状态更新按钮文案
    public void updateWithdrawalStatus() {
        MineWithdrawalAccountBindResponse.AccountBind accountBind = mainFragment.getAccountBind();
        //已绑定
        if (accountBind.isBinding()) {
            mWithdrawalNextTv.setText("申请提现");
        } else {
            mWithdrawalNextTv.setText("绑定支付宝并提现");
        }
    }

    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            counter = 0;
            String mobileNum = channelMoneyEt.getText().toString().trim();
            if (TextUtils.isEmpty(mobileNum)) {
                return;
            }
            afterTextChangedLogic(mobileNum);
        }
    };

    /**
     * 计算个税金额
     * @param money
     */
//    private void  calculationRate(String money){
//
//        // 计算输入金额对应分钟数计算公式：购买分钟数=购买金额／运营后台自定义产品单价；
//        DecimalFormat dF = new DecimalFormat("0.00");
//        if(money.contains(".")&&money.substring(money.length()).equals(".")){
//            return ;
//        }
//        else{
//            //输入金额
//            Double inputMoney =Double.valueOf(money);
//
//            //已提现金额是否到达扣个税限额，未超过
//            if(withdrawnMoney<ratelimitMoney){
//                //差多少到扣个税的额度
//                Double imbalanceMoney =ratelimitMoney-withdrawnMoney;
//                Double tmp=0.0;
//                if(inputMoney>imbalanceMoney){
//                    tmp=inputMoney-imbalanceMoney;
//                }
//                money = dF.format(Double.valueOf(tmp) * rate);
//                calculationRateTv.setText(money);
//            }
//            //已超过
//            else{
//
//                money = dF.format(Double.valueOf(money) * rate);
//                calculationRateTv.setText(money);
//            }
//
//        }
//
//    }


    /**
     * 输入文字的展示逻辑
     *
     * @param tmpinput
     */
    private void afterTextChangedLogic(String tmpinput) {
        int len = tmpinput.length();
        //输入金额为长度为1
        if (len == 1) {
            //有小数点
            if (tmpinput.contains(".")) {
                setInputContent("");
            }
        } else if (len >= 2) {
            int index = tmpinput.indexOf(".");
            //有小数点
            if (tmpinput.contains(".")) {
                int tmpcounter = countStr(tmpinput, ".");
                if (tmpcounter >= 2) {
                    tmpinput = tmpinput.substring(0, len - 1);
                    setInputContent(tmpinput);
                } else if (index < len - 3) {
                    setInputContent(tmpinput.substring(0, len - 1));
                }
            } else if (Double.parseDouble(tmpinput.substring(0, 1)) == 0) {
                String tmp = tmpinput.substring(0, 1);
                setInputContent(tmp);
            }
        }
        //重置小数点个数
        counter = 0;
    }

    /**
     * 递归判断str1中包含str2的个数
     */
    private int countStr(String str1, String str2) {
        if (!str1.contains(str2)) {
            return 0;
        } else if (str1.contains(str2)) {
            counter++;
            countStr(str1.substring(str1.indexOf(str2) +
                    str2.length()), str2);
            return counter;
        }
        return 0;
    }

    /**
     * 设置输入框文本内容以及光标位置
     */
    public void setInputContent(String content) {
        channelMoneyEt.setText(content);
        channelMoneyEt.setSelection(channelMoneyEt.getText().toString().length());
    }

    /**
     * 提现申请
     */
    public void querytDistributionWithdrawApply(String amount) {
        mainFragment.showLoading("正在申请...");
        MineWithdrawalAccountBindResponse.AccountBind accountBind = mainFragment.getAccountBind();
        OMAppApiProvider.getInstance().querytDistributionWithdrawApply(amount, accountBind.aliUserId, "ALI_PAY", accountBind.nickName, new Observer<MineDistributionWithdarwalApplyResponse>() {

            @Override
            public void onCompleted() {
                mainFragment.dismissLoading();
            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(mContext, "网络错误，请检查网络").show();
                mainFragment.dismissLoading();
            }

            @Override
            public void onNext(MineDistributionWithdarwalApplyResponse response) {
                mainFragment.dismissLoading();
                if (response.isSuccess()) {
                    ARouter.getInstance().build(RouterPathProvider.Distribution_WithdrawalDetail_Act)
                            .withString("id", response.data.id)
                            .navigation();
                    Toasty.normal(mContext, response.msg).show();
                } else {
                    Toasty.normal(mContext, response.msg).show();
                }
            }
        });

    }

    /**
     * 获取用户绑定
     */
    public void queryAliAccountBinding(String authCode) {
        OMAppApiProvider.getInstance().queryAliAccountBinding(authCode, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(OMBaseResponse response) {
                if (response.isSuccess()) {
                    mainFragment.queryAliAccountBindingStatus();
                } else {
                    Toasty.normal(mContext, response.msg).show();
                }
            }
        });
    }
}
