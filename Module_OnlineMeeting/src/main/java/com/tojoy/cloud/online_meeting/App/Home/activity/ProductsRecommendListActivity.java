package com.tojoy.cloud.online_meeting.App.Home.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;

import com.tojoy.tjoybaselib.model.live.GoodsResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.cloud.online_meeting.App.EnterpriseEditionHome.adapter.ProductRecommendListAdapter;
import com.tojoy.cloud.online_meeting.R;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 产品推荐列表
 */
public class ProductsRecommendListActivity extends UI {
    private TJRecyclerView mRecyclerView;
    private ImageView mIvTopIcon;
    private ProductRecommendListAdapter mAdapter;
    private List<GoodsResponse.DataBean.RecordsBean> mDatas = new ArrayList<>();
    private String mCompanyCode;
    private LinearLayoutManager mLinearLayoutManager;

    /**
     * 若来源为直播间，需暂时记录直播间信息
     */
    private String roomLiveId;
    private String roomId;
    private String isFromLive;
    private String sourceWay;

    public static void start(Context context, String pageTitle, String code, String isFrom,String roomLiveId, String roomId,String sourceWay) {
        Intent intent = new Intent();
        intent.setClass(context, ProductsRecommendListActivity.class);
        intent.putExtra("pageTitle", pageTitle);
        intent.putExtra("code", code);
        intent.putExtra("isFromLive",isFrom);
        intent.putExtra("roomLiveId",roomLiveId);
        intent.putExtra("roomId",roomId);
        intent.putExtra("sourceWay",sourceWay);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_recommend_list);
        isFromLive = getIntent().getStringExtra("isFromLive");
        roomLiveId = getIntent().getStringExtra("roomLiveId");
        roomId = getIntent().getStringExtra("roomId");
        sourceWay=getIntent().getStringExtra("sourceWay");
        initView();
        initTitle();
        initData();
    }


    private void initTitle() {
        setStatusBar();
        setUpNavigationBar();
        setTitle(TextUtils.isEmpty(getIntent().getStringExtra("pageTitle")) ? "产品推荐" : getIntent().getStringExtra("pageTitle"));
        setRightManagerTitleColor(getResources().getColor(com.netease.nim.uikit.R.color.color_353f5d));
        showBack();
        hideBottomLine();

    }

    private void initView() {
        mRecyclerView = (TJRecyclerView) findViewById(R.id.recyclerView);
        mIvTopIcon = (ImageView) findViewById(R.id.iv_top);
        mAdapter = new ProductRecommendListAdapter(this, mDatas);
        mRecyclerView.setLoadMoreViewWithHide();
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setEmptyImgAndTip(R.drawable.icon_empty_online_logo, getString(R.string.instant_no_content));
        mAdapter.setPageSource(isFromLive,roomLiveId,roomId,sourceWay);

        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.getRecyclerView().setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                loadData();
            }

            @Override
            public void onLoadMore() {
                mPage++;
                loadData();
            }
        });

        // 点击搜索对产品列表进行搜索操作
        findViewById(R.id.content_seach).setOnClickListener(v -> {
            ARouter.getInstance().build(RouterPathProvider.GoodsSearchAct)
                    .withString("companyCode", mCompanyCode)
                    .navigation();
        });
        // 监听recyclerview的滑动控制icon的显隐
        mRecyclerView.getRecyclerView().addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && mIvTopIcon.getVisibility() == View.GONE) {
                    //上翻
                    mIvTopIcon.setVisibility(View.VISIBLE);
                }

                int firstItemPosition = mLinearLayoutManager.findFirstVisibleItemPosition();
                if (firstItemPosition == 0) {
                    int childCount = recyclerView.getChildCount();
                    View firstChild = recyclerView.getChildAt(0);
                    View lastChild = recyclerView.getChildAt(childCount - 1);
                    int top = firstChild.getTop();
                    int bottom = lastChild.getBottom();
                    //recycleView显示itemView的有效区域的top坐标Y
                    int topEdge = recyclerView.getPaddingTop();
                    //recycleView显示itemView的有效区域的bottom坐标Y
                    int bottomEdge = recyclerView.getHeight() - recyclerView.getPaddingBottom();
                    //第一个view的顶部大于top边界值,说明第一个view已经完全显示在顶部
                    //同时最后一个view的底部应该小于bottom边界值,说明最后一个view的底部已经超出显示范围,部分或者完全移出了界面
                    if (top >= topEdge && bottom > bottomEdge) {
                        mIvTopIcon.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }
        });

        // 向上悬浮icon的点击事件
        mIvTopIcon.setOnClickListener(v -> {
            mIvTopIcon.setVisibility(View.GONE);
            mRecyclerView.getRecyclerView().smoothScrollToPosition(0);

        });

        mRecyclerView.autoRefresh();
    }


    private void initData() {
        mCompanyCode = !TextUtils.isEmpty(getIntent().getStringExtra("code")) ? getIntent().getStringExtra("code") : BaseUserInfoCache.getCompanyCode(this);
    }

    private void loadData() {
        OMAppApiProvider.getInstance().getLiveRoomProductList(mCompanyCode, null, mPage, new Observer<GoodsResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                mRecyclerView.refreshLoadMoreView(1, 0);
            }

            @Override
            public void onNext(GoodsResponse response) {
                if (response.isSuccess() && response.data != null) {
                    if (mPage == 1) {
                        mDatas.clear();
                        mDatas.addAll(response.data.records);
                        mAdapter.updateData(mDatas);
                    } else {
                        mDatas.addAll(response.data.records);
                        mAdapter.addMore(response.data.records);
                    }
                    mRecyclerView.showFooterView();
                    mRecyclerView.refreshLoadMoreView(mPage, response.data.records.size());
                } else {
                    Toasty.normal(ProductsRecommendListActivity.this, response.msg).show();
                }
            }
        });
    }

}
