package com.tojoy.cloud.online_meeting.App.Live.Live.Helper;

import android.text.TextUtils;

import com.tencent.rtmp.ui.TXCloudVideoView;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.App.TJBaseApp;

import java.util.Comparator;

public class ViewComparator implements Comparator<TXCloudVideoView> {

    public static final int timeTag = R.string.str_tag_pos;

    @Override
    public int compare(TXCloudVideoView view1, TXCloudVideoView view2) {

        //都为空
        if (TextUtils.isEmpty(view1.getUserId()) && TextUtils.isEmpty(view2.getUserId())) {
            return 0;
        }

        //判空：非空往前排
        if (!TextUtils.isEmpty(view1.getUserId()) && TextUtils.isEmpty(view2.getUserId())) {
            return -1;
        } else if (TextUtils.isEmpty(view1.getUserId()) && !TextUtils.isEmpty(view2.getUserId())) {
            return 1;
        }


        //判类型
        if (view1.getUserId().length() < 10) {
            if (view2.getUserId().length() > 10) {
                //前同台后引流，需要调换位置
                return -1;
            } else {
                //都是同台
                //判自己 先自己后他人<多么自私的需求>
                try {
                    if (BaseUserInfoCache.isMySelf(TJBaseApp.getInstance(), view1.getUserId())) {
                        return -1;
                    } else if (BaseUserInfoCache.isMySelf(TJBaseApp.getInstance(), view2.getUserId())) {
                        return 1;
                    } else {
                        if ((long) view1.getTag(timeTag) > (long) view2.getTag(timeTag)) {
                            return 1;
                        } else if ((long) view1.getTag(timeTag) < (long) view2.getTag(timeTag)) {
                            return -1;
                        } else {
                            return 0;
                        }
                    }
                } catch (Exception e) {
                    return 0;
                }
            }
        } else {
            if (view2.getUserId().length() < 10) {
                //前引流后同台
                return 1;
            } else {
                //都是引流 ：更改 后来的放后边
                if ((long) view1.getTag(timeTag) > (long) view2.getTag(timeTag)) {
                    return 1;
                } else if ((long) view1.getTag(timeTag) < (long) view2.getTag(timeTag)) {
                    return -1;
                } else {
                    return 0;
                }
            }
        }
    }
}