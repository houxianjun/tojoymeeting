package com.tojoy.cloud.online_meeting.App.Distribution.mywithdrawal.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.home.MineDistributionWithdarwalDetailResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;

import rx.Observer;

/**
 * 分销客 提现明细
 *
 * @author houxianjun
 * @Date 2020/07/16
 */
@Route(path = RouterPathProvider.Distribution_WithdrawalDetail_Act)
public class DistributionWithdrawalDetailAct extends UI {
    private TextView mWithdrawalAcountTv;
    private TextView mWithdrawalStatusTv;
    private TextView mWithrawalChannelContentTv;
    private TextView mWithrawalIncomeContentTv;
    private TextView mWithrawalAcountContentTv;
    private TextView mWithrawalApplydateContentTv;
    private TextView mMainWithrawalAcountdateContentTv;
    private TextView mWithrawalNumbersContentTv;
    private RelativeLayout mMainWithrawalAcountdateRlv;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distribution_withdrawal_detail);
        ARouter.getInstance().inject(this);
        id = getIntent().getStringExtra("id");
        setStatusBar();
        setSwipeBackEnable(false);
        initTitle();
        initUI();
        querytDistributionWithdrawApplyDetail();
    }
    private void initTitle() {
        setUpNavigationBar();
        showBack();
        setTitle("账单详情");
        setLeftImage(R.drawable.icon_withdrawal_detail_close);
    }

    public void initUI(){
        mWithdrawalAcountTv =(TextView) findViewById(R.id.tv_withdrawal_acount);
        mWithdrawalStatusTv =(TextView) findViewById(R.id.tv_withdrawal_status);
        mWithrawalChannelContentTv =(TextView) findViewById(R.id.tv_withrawal_channel_content);
        mWithrawalIncomeContentTv =(TextView) findViewById(R.id.tv_withrawal_income_content);
        mWithrawalAcountContentTv =(TextView) findViewById(R.id.tv_withrawal_acount_content);
        mWithrawalApplydateContentTv =(TextView) findViewById(R.id.tv_withrawal_applydate_content);
        mMainWithrawalAcountdateContentTv =(TextView) findViewById(R.id.tv_main_withrawal_acountdate_content);
        mWithrawalNumbersContentTv =(TextView) findViewById(R.id.tv_withrawal_numbers_content);
        mMainWithrawalAcountdateRlv =(RelativeLayout) findViewById(R.id.rl_main_withrawal_acountdate);
    }

    /**
     * 提现列表请求
     */
    public void querytDistributionWithdrawApplyDetail(){
        OMAppApiProvider.getInstance().querytDistributionWithdrawApplyDetail(id, new Observer<MineDistributionWithdarwalDetailResponse>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(MineDistributionWithdarwalDetailResponse response) {
                try {
                    mWithdrawalAcountTv.setText("-"+response.data.amount);
                    mWithrawalChannelContentTv.setText(response.data.withdrawalAccountNo);
                    mWithrawalIncomeContentTv.setText("¥ "+response.data.serviceFee+"元");
                    mWithrawalAcountContentTv.setText("¥ "+response.data.receivedAmount+"元");
                    mWithrawalApplydateContentTv.setText(response.data.createDate);
                    mWithrawalNumbersContentTv.setText(response.data.orderNo);
                    if(response.data.status.equals("2")){
                        mWithdrawalStatusTv.setText("提现申请交易完成");
                        mMainWithrawalAcountdateRlv.setVisibility(View.VISIBLE);
                        mWithdrawalStatusTv.setTextColor(DistributionWithdrawalDetailAct.this.getResources().getColor(R.color.color_1083EA));
                        mMainWithrawalAcountdateContentTv.setText(response.data.successDate);
                    }
                    else if(response.data.status.equals("4")){

                        mWithdrawalStatusTv.setText(response.data.failReason);
                        mMainWithrawalAcountdateRlv.setVisibility(View.GONE);
                        mMainWithrawalAcountdateContentTv.setText("");
                        mWithdrawalStatusTv.setTextColor(DistributionWithdrawalDetailAct.this.getResources().getColor(R.color.color_fb4f65));
                    }
                    else{
                        mMainWithrawalAcountdateRlv.setVisibility(View.GONE);
                        mMainWithrawalAcountdateContentTv.setText("");
                        mWithdrawalStatusTv.setText("提现中");
                        mWithdrawalStatusTv.setTextColor(DistributionWithdrawalDetailAct.this.getResources().getColor(R.color.color_f5A623));
                    }
                }catch (Exception ex){
                 //后面可以加上报日志
                }

            }
        });

    }
}
