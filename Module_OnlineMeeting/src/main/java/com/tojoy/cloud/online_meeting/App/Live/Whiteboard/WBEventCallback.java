package com.tojoy.cloud.online_meeting.App.Live.Whiteboard;

public interface WBEventCallback {
    void onInitWBFail(int initType);
    void onInitWBSuccess(int initType);
}
