package com.tojoy.cloud.online_meeting.App.Live.CollectOuterLive;

import android.app.Dialog;
import android.content.Context;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.R;

public class NewOutlivePop {
    private Context mContext;
    private View mView;
    private Dialog alertDialog;
    private View.OnClickListener makeSureClick;

    public NewOutlivePop(Context context, View.OnClickListener makeSureClick) {
        mContext = context;
        this.makeSureClick = makeSureClick;
        initUI();
    }


    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_new_outlive_layout, null);
        mView.findViewById(R.id.layout_sure).setOnClickListener(v -> {
            if (makeSureClick != null) {
                makeSureClick.onClick(v);
            }
            dismiss();
        });

        mView.findViewById(R.id.layout_cancle).setOnClickListener(v -> dismiss());
    }

    public void show() {
        int width = AppUtils.getWidth(mContext) - AppUtils.dip2px(mContext, 114);
        int height = 300 * width / 510;
        alertDialog = new Dialog(mContext, com.tojoy.tjoybaselib.R.style.tjoy_dialog_style);
        alertDialog.setContentView(mView, new ViewGroup.LayoutParams(width, height));
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.setCanceledOnTouchOutside(false);
        try {
            alertDialog.show();
            alertDialog.setOnKeyListener((dialog, keyCode, event) -> true);
        } catch (Exception e) {

        }
    }

    public boolean isShowing() {
        if (alertDialog == null) {
            return false;
        }
        return alertDialog.isShowing();
    }

    public void dismiss() {
        if (isShowing()) {
            alertDialog.dismiss();
        }
    }

    public void setType(String type, String userName1, String userName2) {
        TextView title = mView.findViewById(R.id.tv_title_left);
        String titleString;
        if ("1".equals(type)) {
            titleString = "<font color='#353f5e'>" + userName2 + "和主播" + userName1 + "开启同台啦，\n是否重新引流？</font>";
        } else {
            titleString = "<font color='#353f5e'>主播" + userName1 + "增加引流视频，\n是否重新引流？</font>";
        }
        title.setText(Html.fromHtml(titleString));
    }
}
