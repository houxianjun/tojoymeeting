package com.tojoy.cloud.online_meeting.App.UserCenter.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.user.MemberListResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.services.imageManager.YuanJiaoImageView;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * Created by luuzhu on 2019/4/20.
 */

public class LiveRoomMemberListAdapter extends BaseRecyclerViewAdapter<MemberListResponse.DataObjBean.ListBean> {
    OperationCallback mOperationCallback;
    private boolean isHaveHelper;

    public LiveRoomMemberListAdapter(@NonNull Context context, @NonNull List<MemberListResponse.DataObjBean.ListBean> datas, OperationCallback operationCallback) {
        super(context, datas);
        this.mOperationCallback = operationCallback;
    }

    public void setHaveHelper(boolean haveHelper) {
        isHaveHelper = haveHelper;
    }

    @Override
    protected int getViewType(int position, @NonNull MemberListResponse.DataObjBean.ListBean data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_live_room_member_list;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new LiveMemberListVH(context, parent, getLayoutResId(viewType));
    }


    private class LiveMemberListVH extends BaseRecyclerViewHolder<MemberListResponse.DataObjBean.ListBean> {

        private YuanJiaoImageView mIvHead;
        private TextView mTvLabel;
        private TextView mTvName;
        private TextView mTvSetHelper;
        private TextView mTvCancelHelper;
        private TextView mTvSwitchHelper;

        private TextView tvMemberState;

        public LiveMemberListVH(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            bindViews(itemView);
        }


        private void bindViews(View itemView) {
            mIvHead = itemView.findViewById(R.id.ivHead);
            mTvLabel = itemView.findViewById(R.id.tv_label);
            mTvName = itemView.findViewById(R.id.tvName);
            mTvSetHelper = itemView.findViewById(R.id.tv_set_helper);
            mTvCancelHelper = itemView.findViewById(R.id.tv_cancle_helper);
            mTvSwitchHelper = itemView.findViewById(R.id.tv_switch_helper);
            tvMemberState = itemView.findViewById(R.id.tvMemberState);
        }

        @Override
        public void onBindData(@NonNull MemberListResponse.DataObjBean.ListBean data, int position) {

            try {

                if (data == null) {
                    return;
                }
                String type = "";

                if (data.userType == null) {
                    type = "";
                } else {
                    type = data.userType;
                }

                //这个是助手，但是未入会
                if ("1".equals(type) && !"1".equals(data.online)) {
                    type = "2";
                }

                if ("0".equals(type)) {
                    // 主播
                    mTvLabel.setVisibility(View.VISIBLE);
                    mTvLabel.setText("主持人");
                    mTvLabel.setTextSize(12);
                    mTvLabel.setBackgroundResource(R.drawable.bg_member_search_host_label);
                    mTvLabel.setTextColor(context.getResources().getColor(R.color.white));
                    mTvLabel.setCompoundDrawables(null,null,null,null);
                    mTvSetHelper.setVisibility(View.GONE);
                    mTvCancelHelper.setVisibility(View.GONE);
                    mTvSwitchHelper.setVisibility(View.GONE);
                    tvMemberState.setVisibility(View.GONE);
                } else if ("1".equals(type)) {
                    // 助手
                    mTvLabel.setVisibility(View.VISIBLE);
                    mTvLabel.setText("助手");
                    mTvLabel.setTextSize(12);
                    mTvLabel.setTextColor(context.getResources().getColor(R.color.white));
                    mTvLabel.setCompoundDrawables(null,null,null,null);
                    mTvLabel.setBackgroundResource(R.drawable.bg_member_search_helper_label);
                    mTvSetHelper.setVisibility(View.GONE);
                    mTvSwitchHelper.setVisibility(View.GONE);
                    tvMemberState.setVisibility(View.GONE);
                    if (LiveRoomInfoProvider.getInstance().isHost()) {
                        // 如果我是主播显示取消助手按钮，否则隐藏
                        mTvCancelHelper.setVisibility(View.VISIBLE);
                    } else {
                        mTvCancelHelper.setVisibility(View.GONE);
                    }
                }
              else {
                    mTvLabel.setVisibility(View.GONE);
                    if ("1".equals(data.online)) {
                        //已入会
                        tvMemberState.setVisibility(View.GONE);
                        //主播端显示规则
                        if (LiveRoomInfoProvider.getInstance().isHost()) {


                            //小程序用户 增加字段 wchat=1 app=0 显示小程序标签
                            if(data.getFromwechat().equals("1")&&data.getFromapp().equals("0")){

                                showWeChatLabel(mTvLabel);
                                mTvCancelHelper.setVisibility(View.GONE);
                                mTvSetHelper.setVisibility(View.GONE);
                                mTvSwitchHelper.setVisibility(View.GONE);
                            }
                            //来源不是小程序用户走之前的逻辑。app显示设置助手，取消助手，邀请按钮
                            else{
                                mTvLabel.setVisibility(View.GONE);
                                // 我是主播显示设置助手和切换助手的操作
                                if (isHaveHelper) {
                                    // 直播间有助手（可以切换助手)
                                    mTvCancelHelper.setVisibility(View.GONE);
                                    mTvSetHelper.setVisibility(View.GONE);
                                    mTvSwitchHelper.setVisibility(View.VISIBLE);
                                } else {
                                    // 直播间没助手（可以取消助手)
                                    mTvCancelHelper.setVisibility(View.GONE);
                                    mTvSetHelper.setVisibility(View.VISIBLE);
                                    mTvSwitchHelper.setVisibility(View.GONE);
                                }
                            }

                        } else {

                            if(data.getFromwechat().equals("1")&&data.getFromapp().equals("0")){
                                showWeChatLabel(mTvLabel);

                            }
                            mTvCancelHelper.setVisibility(View.GONE);
                            mTvSetHelper.setVisibility(View.GONE);
                            mTvSwitchHelper.setVisibility(View.GONE);
                        }
                    } else {
                        //未入会
                        mTvCancelHelper.setVisibility(View.GONE);
                        mTvSetHelper.setVisibility(View.GONE);
                        mTvSwitchHelper.setVisibility(View.GONE);

                        if ("1".equals(data.isRegister)) {
                            //已注册
                            tvMemberState.setVisibility(View.VISIBLE);
                            if ("0".equals(data.invite)) {
                                //可邀请
                                tvMemberState.setText("邀请");
                                tvMemberState.setBackgroundResource(R.drawable.bg_btn_blue);
                            } else {
                                tvMemberState.setText("邀请中...");
                                tvMemberState.setBackgroundResource(R.drawable.bg_btn_grey);
                            }
                        } else if ("0".equals(data.isRegister)){
                            tvMemberState.setVisibility(View.VISIBLE);
                            tvMemberState.setText("未注册");
                            tvMemberState.setBackgroundResource(R.drawable.bg_btn_grey);
                        }
                    }
                }

                mTvName.setText(data.userName);
                mIvHead.setType(1);
                mIvHead.setBorderRadius(50);
                mIvHead.setWidthHeight(34, 34);
                ImageLoaderManager.INSTANCE.loadHeadImage(context, mIvHead, data.userAvatar, R.drawable.icon_dorecord);

                mTvSetHelper.setOnClickListener(v -> {
                    // 设置助手
                    if (mClickHeadIvCallback != null) {
                        if (!FastClickAvoidUtil.isDoubleClick()) {
                            mClickHeadIvCallback.onSetHelper(data, true);
                        }
                    }
                });

                mTvSwitchHelper.setOnClickListener(v -> {
                    // 切换助手
                    if (mClickHeadIvCallback != null) {
                        if (!FastClickAvoidUtil.isDoubleClick()) {
                            mClickHeadIvCallback.onSetHelper(data, false);
                        }
                    }
                });

                mTvCancelHelper.setOnClickListener(v -> {
                    // 取消助手
                    if (mClickHeadIvCallback != null) {
                        if (!FastClickAvoidUtil.isDoubleClick()) {
                            mClickHeadIvCallback.onCancleHelper(data);
                        }
                    }
                });

            } catch (Exception ex) {
                Log.d(LiveRoomMemberListAdapter.class.getName(), "LiveRoomMemberListAdapter onBindData =" + ex.getStackTrace() + ex.getMessage());
            }

            tvMemberState.setOnClickListener(view -> {
                if (FastClickAvoidUtil.isDoubleClick()) {
                    return;
                }
                if ("邀请".equals(tvMemberState.getText().toString())) {

                    ((UI) (context)).showProgressHUD(context, "加载中...");

                    OMAppApiProvider.getInstance().inviteMember(data.userId, LiveRoomInfoProvider.getInstance().liveId, new Observer<OMBaseResponse>() {
                        @Override
                        public void onCompleted() {
                            ((UI) (context)).dismissProgressHUD();
                        }

                        @Override
                        public void onError(Throwable e) {
                            ((UI) (context)).dismissProgressHUD();
                            Toasty.normal(context, "网络错误，请检查网络").show();
                        }

                        @Override
                        public void onNext(OMBaseResponse response) {
                            if (response.isSuccess()) {
                                if ("1".equals(data.isRegister)) {//已注册
                                    tvMemberState.setText("邀请中...");
                                } else {
                                    tvMemberState.setText("未注册");
                                }
                                data.invite = "1";
                                tvMemberState.setBackgroundResource(R.drawable.bg_btn_grey);
                                Toasty.normal(context, "邀请成功").show();
                            } else {
                                Toasty.normal(context, response.msg).show();
                            }
                        }
                    });
                }
            });
        }
    }

    /**
     * 显示小程序标签
     */
    public void showWeChatLabel(TextView mTvLabel){
        //小程序用户 增加字段 wchat=1 app=0 显示小程序标签
            Drawable rightDrawable = context.getResources().getDrawable(R.drawable.icon_member_list_applets);
            rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());
            mTvLabel.setVisibility(View.VISIBLE);
            mTvLabel.setTextSize(13);
            mTvLabel.setBackgroundResource(R.color.transparent);
            mTvLabel.setTextColor(context.getResources().getColor(R.color.color_f5A623));
            mTvLabel.setCompoundDrawables(rightDrawable,null,null,null);
            mTvLabel.setText(" 小程序");

    }


    public void setClickHeadIvCallback(OnClickHeadIvCallback callback) {
        this.mClickHeadIvCallback = callback;
    }

    private OnClickHeadIvCallback mClickHeadIvCallback;

    public interface OnClickHeadIvCallback {

        void onSetHelper(MemberListResponse.DataObjBean.ListBean data, boolean isSetHelper);

        void onCancleHelper(MemberListResponse.DataObjBean.ListBean data);
    }

}
