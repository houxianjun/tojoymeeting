package com.tojoy.cloud.online_meeting.App.Live.Live.Model;

/**
 * @author qll
 * @date 2019/11/25
 */
public class LiveTemplateLeaveMsg {
    public String time;
    public String reason;
}
