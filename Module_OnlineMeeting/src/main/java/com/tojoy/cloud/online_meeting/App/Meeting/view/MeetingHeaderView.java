package com.tojoy.cloud.online_meeting.App.Meeting.view;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.configs.UserPermissions;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.ui.Banner.Banner;
import com.tojoy.tjoybaselib.model.home.HomeModelResponse;
import com.tojoy.common.Services.Router.AppRouter;
import com.tojoy.common.Widgets.AnimationIndicator.MeetingHomeAnimationIndicator;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import rx.Observer;

public class MeetingHeaderView {
    private Context mContext;
    public View mView;
    private Banner mBanner;
    private MeetingHomeAnimationIndicator mAnimationIndicator;
    RelativeLayout mIndicatorContainer;

    private RelativeLayout flowResourceRlv;
    private ImageView mFlowResourceIv, mProjectServiceIv, mSaleOnlineIv;

    //热门企业
    HotEnterpriseView hotEnterpriseView;

    //招商推荐
    AttractRecommendView attractRecommendView;

    // banner数据列表
    private List<HomeModelResponse.HotEnterPriseModel> mBannerList = new ArrayList<>();
    // banner图片数据
    private List<String> mBannerImageList = new ArrayList<>();

    // H5资源位的数据
    private List<HomeModelResponse.HotEnterPriseModel> mH5ResourceList = new ArrayList<>();


    private int flowWith;
    private int flowHeight;

    public MeetingHeaderView(Activity context) {
        mContext = context;
    }

    public View getView() {
        if (mView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mView = aLayoutInflater.inflate(R.layout.header_meeting_home_layout, null);
            ButterKnife.bind(this, mView);
        }
        initView();
        return mView;
    }

    private void initView() {
        mBanner = mView.findViewById(R.id.iv_banner);
        mIndicatorContainer = mView.findViewById(R.id.indicator_container);
        flowResourceRlv = mView.findViewById(R.id.rlv_flow_resource);
        mFlowResourceIv = mView.findViewById(R.id.iv_flow_resource);
        mProjectServiceIv = mView.findViewById(R.id.iv_project_service);
        mSaleOnlineIv = mView.findViewById(R.id.iv_sale_online);
        View spaceView = mView.findViewById(R.id.view_space);

        RelativeLayout.LayoutParams spaceParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,AppUtils.getStatusBarHeight(mContext));
        spaceView.setLayoutParams(spaceParams);

        // 设置banner
        int widht = AppUtils.getWidth(mContext) - AppUtils.dip2px(mContext, 35);
        int height = 170 * widht / 340;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mBanner.getLayoutParams();
        params.height = height;
        params.width = widht;
        mBanner.setLayoutParams(params);
        mBanner.setDelayTime(5000);
        mBanner.setIndicatorHeight(mContext, 0);
        //设置图片加载器
        mBanner.setImageLoader(new MeetingHomeBannerImageLoader());
        if (mAnimationIndicator == null) {
            mAnimationIndicator = new MeetingHomeAnimationIndicator(mContext);
            mAnimationIndicator.setIndicatorCoolor();
            RelativeLayout.LayoutParams indicatorLps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.dip2px(mContext, 7));
            indicatorLps.setMargins(0, 0, 0, 0);
            mIndicatorContainer.addView(mAnimationIndicator.getView(), indicatorLps);
        }

        // 设置招商、加入会议
        RelativeLayout.LayoutParams enterParams = (RelativeLayout.LayoutParams) mView.findViewById(R.id.llv_enter).getLayoutParams();
        int enterWith = (AppUtils.getWidth(mContext) - AppUtils.dip2px(mContext, 45)) / 2;
        enterParams.height = 54 * enterWith / 165;
        mView.findViewById(R.id.llv_enter).setLayoutParams(enterParams);

        // 设置流量资源、路演服务、招商等布局
        LinearLayout.LayoutParams flowParams = (LinearLayout.LayoutParams) flowResourceRlv.getLayoutParams();
        flowWith = (AppUtils.getWidth(mContext) - AppUtils.dip2px(mContext, 45)) / 2;
        flowHeight = 136 * flowWith / 165;
        flowParams.height = flowHeight;
        flowResourceRlv.setLayoutParams(flowParams);

        //热门会议
        hotEnterpriseView = mView.findViewById(R.id.view_home_enterprise);
        attractRecommendView = mView.findViewById(R.id.view_home_attract_recommend);
        initListener();
    }

    /**
     * 设置banner的数据
     */
    public void setBannerData(HomeModelResponse bannerData) {
        if (bannerData != null && bannerData.dataList != null && bannerData.dataList.size() != 0) {
            mBannerList.clear();
            mBannerList.addAll(bannerData.dataList);
            mBannerImageList.clear();
            for (HomeModelResponse.HotEnterPriseModel data : bannerData.dataList) {
                mBannerImageList.add(data.bannerUrl);
            }
            mBanner.setVisibility(View.VISIBLE);
            mIndicatorContainer.setVisibility(View.VISIBLE);
            initBanner();
        } else {
            mBanner.setVisibility(View.GONE);
            mIndicatorContainer.setVisibility(View.GONE);
        }
    }

    /**
     * 初始化banner
     */
    private void initBanner() {
        //设置图片集合
        mBanner.setImages(mBannerImageList);
        //banner设置方法全部调用完毕时最后调用
        mBanner.start();

        mAnimationIndicator.createIndicator(mBannerImageList.size());
        //页面改变的导航
        mBanner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                mAnimationIndicator.selectPos(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        mBanner.setOnBannerListener(position -> {
            if (FastClickAvoidUtil.isFastDoubleClick(mBanner.getId())) {
                return;
            }
            HomeModelResponse.HotEnterPriseModel model = mBannerList.get(position);
            if (model == null) {
                return;
            }

            AppRouter.doJump(mContext,model.jumpType,model.appChannel,model.channelValue,model.h5Url,"");
        });
    }

    /**
     * 设置H5资源位置数据
     */
    public void setH5ResourceData(HomeModelResponse h5ResourceData) {
        if (h5ResourceData != null && h5ResourceData.dataList != null && h5ResourceData.dataList.size() > 0) {
            mH5ResourceList.clear();
            mH5ResourceList.addAll(h5ResourceData.dataList);
            mView.findViewById(R.id.rlv_h5).setVisibility(View.VISIBLE);
            for (int i = 0; i < mH5ResourceList.size(); i++) {
                if (i == 0) {
                    ImageLoaderManager.INSTANCE.loadSingleConorSizedImageAddPlaceHolder(mContext,
                            mFlowResourceIv,
                            OSSConfig.getRemoveStylePath( mH5ResourceList.get(i).bannerUrl),
                            AppUtils.dip2px(mContext, 3), flowWith, flowHeight, 0, RoundedCornersTransformation.CornerType.ALL);
                } else if (i == 1) {
                    ImageLoaderManager.INSTANCE.loadSingleConorSizedImageAddPlaceHolder(mContext,
                            mProjectServiceIv,
                            OSSConfig.getRemoveStylePath( mH5ResourceList.get(i).bannerUrl),
                            AppUtils.dip2px(mContext, 3), flowWith, (flowHeight - AppUtils.dip2px(mContext,10))/2  , 0, RoundedCornersTransformation.CornerType.ALL);
                } else if (i == 2) {
                    ImageLoaderManager.INSTANCE.loadSingleConorSizedImageAddPlaceHolder(mContext,
                            mSaleOnlineIv,
                            OSSConfig.getRemoveStylePath(mH5ResourceList.get(i).bannerUrl),
                            AppUtils.dip2px(mContext, 3), flowWith, (flowHeight - AppUtils.dip2px(mContext,10))/2, 0, RoundedCornersTransformation.CornerType.ALL);
                }
            }
        } else {
            mView.findViewById(R.id.rlv_h5).setVisibility(View.GONE);
        }
    }

    /**
     * 设置热门企业列表数据
     */
    public void setHotCompanyData(HomeModelResponse data){
        if (data != null && data.dataList != null && data.dataList.size() > 0) {
            hotEnterpriseView.setVisibility(View.VISIBLE);
            hotEnterpriseView.initData(data);
        } else {
            hotEnterpriseView.setVisibility(View.GONE);
        }
    }

    /**
     * 设置招商推荐列表数据
     */
    public void setRecommendInvestmentData(HomeModelResponse data){
        if (data != null && data.dataList != null && data.dataList.size() > 0) {
            attractRecommendView.setVisibility(View.VISIBLE);
            attractRecommendView.initData(data);
        } else {
            attractRecommendView.setVisibility(View.GONE);
        }

    }

    private void initListener() {
        mView.findViewById(R.id.tv_startmeeting).setOnClickListener(v -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }
            // 判断无企业则跳转企业入驻
            if (!BaseUserInfoCache.isHasCompany(mContext)) {
                AppRouter.doJump(mContext,AppRouter.SkipType_Native,AppRouter.KEY_APPLY_ENTERPRISE,"","","");
            } else if (BaseUserInfoCache.getUserIsHavePermiss(mContext,UserPermissions.STARTAUTH)) {
                if (BaseUserInfoCache.getUserIsHavePermiss(mContext,UserPermissions.OBSMANAGER)) {
                    OMAppApiProvider.getInstance().queryLiveStatus(new Observer<OMBaseResponse>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(OMBaseResponse omBaseResponse) {
                            if (omBaseResponse.isSuccess()) {
                                ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_START_LIVE_BROADCAST_APPLY)
                                        .withString("source", "menu")
                                        .navigation();
                            } else {
                                Toasty.normal(mContext,omBaseResponse.msg).show();
                            }
                        }
                    });
                } else {
                    ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_START_LIVE_BROADCAST_APPLY)
                            .withString("source", "menu")
                            .navigation();
                }

            } else {
                Toasty.normal(mContext, "抱歉，您暂无开会权限").show();
            }
        });
        mView.findViewById(R.id.tv_addmeeting).setOnClickListener(v -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }
            ARouter.getInstance().build(RouterPathProvider.JOIN_MEETING).navigation();
        });

        flowResourceRlv.setOnClickListener(v -> {
            // 跳转H5流量资源
            if (mH5ResourceList == null || mH5ResourceList.size() <= 0) {
                return;
            }
            HomeModelResponse.HotEnterPriseModel model = mH5ResourceList.get(0);
            if (model != null) {
                // 进行跳转
                AppRouter.doJump(mContext,model.jumpType,model.appChannel,model.channelValue,model.h5Url,"");
            }
        });

        mView.findViewById(R.id.rlv_project_service).setOnClickListener(v -> {
            // 跳转H5项目服务路演
            if (mH5ResourceList == null || mH5ResourceList.size() <= 1) {
                return;
            }
            HomeModelResponse.HotEnterPriseModel model = mH5ResourceList.get(1);
            if (model != null) {
                // 进行跳转
                AppRouter.doJump(mContext,model.jumpType,model.appChannel,model.channelValue,model.h5Url,"");
            }
        });
        mView.findViewById(R.id.rlv_sale_online).setOnClickListener(v -> {
            // 跳转H5项目服务路演
            if (mH5ResourceList == null || mH5ResourceList.size() <= 2) {
                return;
            }
            HomeModelResponse.HotEnterPriseModel model = mH5ResourceList.get(2);
            if (model != null) {
                // 进行跳转
                AppRouter.doJump(mContext,model.jumpType,model.appChannel,model.channelValue,model.h5Url,"");

            }
        });
    }
}
