package com.tojoy.cloud.online_meeting.App.Live.FileShow;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.model.live.FileModel;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;


public class FileBottomListAdapter extends BaseRecyclerViewAdapter<FileModel> {

    FileBottomListAdapter(@NonNull Context context, @NonNull List<FileModel> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull FileModel data) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return 0;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new FileListVH(context, parent);
    }

    class FileListVH extends BaseRecyclerViewHolder<FileModel> {
        Context mContext;
        RelativeLayout mContainer;
        ImageView mPreviewImg;
        ImageView mIcon;

        FileListVH(Context context, ViewGroup parent) {
            super(context, parent, R.layout.item_file_list_view);
            this.mContext = context;
            mContainer = itemView.findViewById(R.id.rlv_filecontainer);
            mPreviewImg = itemView.findViewById(R.id.iv_preview);
            mIcon = itemView.findViewById(R.id.iv_icon);
        }

        @Override
        public void onBindData(FileModel data, int position) {
            if (data.isSelected) {
                mContainer.setBackgroundResource(R.drawable.bg_filelist_selected);
            } else {
                mContainer.setBackgroundResource(0);
            }

            if (!TextUtils.isEmpty(data.type) && "4".equals(data.type)) {
                // 显示默认图标
                mIcon.setVisibility(View.VISIBLE);
                mIcon.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                mPreviewImg.setBackgroundColor(Color.parseColor("#ffffff"));
                mPreviewImg.setScaleType(ImageView.ScaleType.CENTER_CROP);
                ImageLoaderManager.INSTANCE.loadVideoScreenshot(context,mPreviewImg,
                        "",R.drawable.icon_online_meeting_home_grid_default,0,
                        true, true, true, true,0);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mIcon.getLayoutParams();
                params.setMargins(AppUtils.dip2px(context,10),0,AppUtils.dip2px(context,10),0);
                mIcon.setLayoutParams(params);
                Glide.with(context)
                        .load("pdf")
                        .apply(new RequestOptions().placeholder(R.drawable.icon_file_pdf))
                        .into(mIcon);
            } else if (!TextUtils.isEmpty(data.type) && "2".equals(data.type)) {
                // 显示默认图标
                mIcon.setVisibility(View.VISIBLE);
                mIcon.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                mPreviewImg.setBackgroundColor(Color.TRANSPARENT);
                mPreviewImg.setScaleType(ImageView.ScaleType.CENTER_CROP);
                // 设置第一帧图片的作为背景
                ImageLoaderManager.INSTANCE.loadVideoScreenshot(context,mPreviewImg,
                        OSSConfig.getOSSURLedStr(data.url),R.drawable.icon_online_meeting_home_grid_default,0,
                        true, true, true, true,0);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mIcon.getLayoutParams();
                params.setMargins(AppUtils.dip2px(context,10),0,AppUtils.dip2px(context,10),0);
                mIcon.setLayoutParams(params);
                Glide.with(context)
                        .load("video")
                        .apply(new RequestOptions().placeholder(R.drawable.icon_online_start_video))
                        .into(mIcon);
            } else {
                mPreviewImg.setBackgroundColor(Color.TRANSPARENT);
                mPreviewImg.setScaleType(ImageView.ScaleType.CENTER_CROP);
                mIcon.setVisibility(View.GONE);
                //  防止闪烁
                ImageLoaderManager.INSTANCE.loadHeadImage(mContext,mPreviewImg,OSSConfig.getOSSURLedStr(data.url),R.drawable.icon_online_meeting_home_grid_default);
            }
        }
    }
}
