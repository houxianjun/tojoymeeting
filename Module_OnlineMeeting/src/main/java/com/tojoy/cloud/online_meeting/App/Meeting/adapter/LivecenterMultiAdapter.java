package com.tojoy.cloud.online_meeting.App.Meeting.adapter;

/**
 * Created by chengyanfang on 2018/4/3.
 */

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import com.tojoy.tjoybaselib.model.live.OnlineMeetingHomeGridModel;
import com.tojoy.tjoybaselib.ui.recyclerview.decoration.SpacingDecoration;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.cloud.online_meeting.App.Home.adapter.RoadOrHotLiveListAdapter;
import com.tojoy.cloud.online_meeting.App.Meeting.model.MultiAdapterBean;
import com.tojoy.cloud.online_meeting.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 瀑布流的显示
 */

public class LivecenterMultiAdapter extends BaseRecyclerViewAdapter<MultiAdapterBean> {
    MeetingCenterAdapter mLiveAdapter;

    RoadOrHotLiveListAdapter mRoadOrHotLiveListAdapter;
    /**
     * 仅针对热门直播推荐的数据
     * 1：定制首页热门直播间过来需要做特殊处理
     * 2：热门列表
     */
    private int mEnterType;

    Context mContext;

    public LivecenterMultiAdapter(@NonNull Context context, @NonNull List<MultiAdapterBean> datas) {
        super(context, datas);
        this.mContext = context;
    }

    public LivecenterMultiAdapter(@NonNull Context context, @NonNull List<MultiAdapterBean> datas,int mEnterType) {
        super(context, datas);
        this.mContext = context;
        this.mEnterType = mEnterType;
    }

    @Override
    protected int getViewType(int position, @NonNull MultiAdapterBean data) {
        return data.resouseType.ordinal();
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return 0;
    }


    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        switch (MultiAdapterBean.ResouseType.values()[viewType]) {
            case CENTER_LIVE_LIST:
                return new MultiRecyclerVH(context, parent);
            case ROAD_OR_HOT_LIVE_LIST:
                return new RoadOrHotRecyclerVH(context, parent);
            default:
                return new MultiRecyclerVH(context, parent);
        }
    }

    class MultiRecyclerVH extends BaseRecyclerViewHolder<MultiAdapterBean> {
        RecyclerView mItemRecycler;
        List<OnlineMeetingHomeGridModel> mSelfLiveList = new ArrayList<>();

        public MultiRecyclerVH(Context context, ViewGroup viewGroup) {
            super(context, viewGroup, R.layout.item_arrangebarcontent_layout);
            mItemRecycler = (RecyclerView) findView(mItemRecycler, R.id.item_recycler);
            mItemRecycler.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
            mLiveAdapter = new MeetingCenterAdapter(mContext, mSelfLiveList);
            mItemRecycler.setAdapter(mLiveAdapter);
            mItemRecycler.addItemDecoration(new SpacingDecoration(AppUtils.dip2px(getContext(), 8), AppUtils.dip2px(getContext(), 0), false));

            mLiveAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
                if (!FastClickAvoidUtil.isDoubleClick()) {
                    if (mOnClickJoinDetailListener!= null) {
                        mOnClickJoinDetailListener.onClick(v,position,data);
                    }
                }
            });
        }

        @Override
        public void onBindData(MultiAdapterBean data, int position) {
            mSelfLiveList.clear();
            mSelfLiveList.addAll(data.liveDataList);
            mLiveAdapter.updateData(mSelfLiveList);
        }
    }

    class RoadOrHotRecyclerVH extends BaseRecyclerViewHolder<MultiAdapterBean> {
        RecyclerView mItemRecycler;
        List<OnlineMeetingHomeGridModel> mDataList = new ArrayList<>();

        public RoadOrHotRecyclerVH(Context context, ViewGroup viewGroup) {
            super(context, viewGroup, R.layout.item_arrangebarcontent_layout);
            mItemRecycler = (RecyclerView) findView(mItemRecycler, R.id.item_recycler);
            mItemRecycler.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
            mRoadOrHotLiveListAdapter = new RoadOrHotLiveListAdapter(mContext, mDataList,mEnterType);
            mItemRecycler.setAdapter(mRoadOrHotLiveListAdapter);
            mItemRecycler.addItemDecoration(new SpacingDecoration(AppUtils.dip2px(getContext(), 6), AppUtils.dip2px(getContext(), 6), false));

            mRoadOrHotLiveListAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
                if (!FastClickAvoidUtil.isDoubleClick()) {
                    if (mOnClickJoinDetailListener!= null) {
                        mOnClickJoinDetailListener.onClick(v,position,data);
                    }
                }
            });
        }

        @Override
        public void onBindData(MultiAdapterBean data, int position) {
            mDataList.clear();
            mDataList.addAll(data.roadOrHotLiveDataList);
            mRoadOrHotLiveListAdapter.updateData(mDataList);
        }
    }


    public interface onClickJoinDetailListener{
        void onClick(View view,int position,OnlineMeetingHomeGridModel data);
    }

    private onClickJoinDetailListener mOnClickJoinDetailListener;

    public void setmOnClickJoinDetailListener(onClickJoinDetailListener listener){
        this.mOnClickJoinDetailListener = listener;
    }
}
