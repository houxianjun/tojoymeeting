package com.tojoy.cloud.online_meeting.App.Live.MemberList;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tojoy.tjoybaselib.model.user.MemberListResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.netease.nim.uikit.business.liveroom.OperationCallback;
import com.tojoy.cloud.online_meeting.App.Log.LiveLogTag;
import com.tojoy.cloud.online_meeting.App.UserCenter.adapter.LiveRoomMemberListAdapter;
import com.tojoy.cloud.online_meeting.R;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * 直播间-成员列表
 */

@SuppressLint("ValidFragment")
public class LiveMemberFragment extends Fragment {

    private LiveRoomMemverListDialog liveRoomMemverListDialog;
    private String mType;
    private OperationCallback mOperationCallback;
    private UI mContext;
    private View mView;
    private TJRecyclerView mRecyclerView;
    private List<MemberListResponse.DataObjBean.ListBean> mDatas = new ArrayList<>();
    private LiveRoomMemberListAdapter mAdapter;
    public int page = 1;
    private int unEnterMemberCount;

    /**
     * @param type               1:已入会  2:未入会
     * @param mOperationCallback
     * @param liveRoomMemverListDialog
     */
    public LiveMemberFragment(String type, OperationCallback mOperationCallback, LiveRoomMemverListDialog liveRoomMemverListDialog) {
        this.mOperationCallback = mOperationCallback;
        this.mType = type;
        this.liveRoomMemverListDialog = liveRoomMemverListDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = (UI) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_live_member, container, false);
        init();
        getData();
        return mView;
    }

    private void init() {

        mRecyclerView = mView.findViewById(R.id.memberRecycler);
        mRecyclerView.setRefreshable(false);
        mRecyclerView.setLoadMoreView();
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                page = 1;
                getData();
            }

            @Override
            public void onLoadMore() {
                page++;
                getData();
            }
        });

        mAdapter = new LiveRoomMemberListAdapter(mContext, mDatas, mOperationCallback);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setClickHeadIvCallback(new LiveRoomMemberListAdapter.OnClickHeadIvCallback() {

            @Override
            public void onSetHelper(MemberListResponse.DataObjBean.ListBean data, boolean isSetHelper) {
                new TJMakeSureDialog(mContext, (isSetHelper ? "是否设置" : "是否切换") + data.userName + "为助手？",
                        view -> {
                            if (mContext != null) {
                                // 判断当前是否在展示文档和手绘板，正在展示提示"演示中，暂不可切换助手"
                                if (LiveRoomInfoProvider.getInstance().isShowFile || LiveRoomInfoProvider.getInstance().isShowDrawPad) {
                                    Toasty.normal(mContext, "演示中，暂不可切换助手").show();
                                    return;
                                }
                                mContext.showProgressHUD(mContext, "");
                                OMAppApiProvider.getInstance().addLiveHelper(LiveRoomInfoProvider.getInstance().liveId, LiveRoomInfoProvider.getInstance().imRoomId, data.userId, data.userName, new Observer<OMBaseResponse>() {
                                    @Override
                                    public void onCompleted() {
                                        mContext.dismissProgressHUD();
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        mContext.dismissProgressHUD();
                                        Toasty.normal(mContext, "网络异常，请检查网络").show();
                                    }

                                    @Override
                                    public void onNext(OMBaseResponse response) {
                                        if (!response.isSuccess()) {
                                            Toasty.normal(mContext, response.msg).show();
                                        } else {
                                            // 设置成功，刷新页面
                                            new Handler().postDelayed(() -> {
                                                page = 1;
                                                getData();
                                            }, 1000);
                                        }
                                    }
                                });
                            }
                        }).setBtnText("确定", "取消").show();

            }

            @Override
            public void onCancleHelper(MemberListResponse.DataObjBean.ListBean data) {
                new TJMakeSureDialog(mContext, "是否取消助手" + data.userName + "？",
                        view -> {
                            if (mContext != null) {
                                // 判断当前是否在展示文档和手绘板，正在展示提示"演示中，暂不可切换助手"
                                if (LiveRoomInfoProvider.getInstance().isShowFile || LiveRoomInfoProvider.getInstance().isShowDrawPad) {
                                    Toasty.normal(mContext, "演示中，暂不可取消助手").show();
                                    return;
                                }
                                mContext.showProgressHUD(mContext, "");
                                OMAppApiProvider.getInstance().cancleLiveHelper(LiveRoomInfoProvider.getInstance().liveId, LiveRoomInfoProvider.getInstance().imRoomId, data.userId, data.userName, new Observer<OMBaseResponse>() {
                                    @Override
                                    public void onCompleted() {
                                        mContext.dismissProgressHUD();
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        mContext.dismissProgressHUD();
                                        Toasty.normal(mContext, "网络异常，请检查网络").show();
                                    }

                                    @Override
                                    public void onNext(OMBaseResponse response) {
                                        if (!response.isSuccess()) {
                                            Toasty.normal(mContext, response.msg).show();
                                        } else {
                                            // 取消成功，刷新页面
                                            new Handler().postDelayed(() -> {
                                                page = 1;
                                                getData();
                                            }, 1000);
                                        }
                                    }
                                });
                            }
                        }).setBtnText("确定", "取消").show();
            }
        });
    }

    private void getData() {
        if ("1".equals(mType)) {
            LogUtil.i(LiveLogTag.BaseLiveTag, "getMemberListOwner--start");
            OMAppApiProvider.getInstance().getMemberListOwner(LiveRoomInfoProvider.getInstance().imRoomId, LiveRoomInfoProvider.getInstance().liveId,
                    page + "", "20",LiveRoomInfoProvider.getInstance().applyId, new Observer<MemberListResponse>() {

                        @Override
                        public void onCompleted() {
                            LogUtil.i(LiveLogTag.BaseLiveTag, "getMemberListOwner--Completed");
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toasty.normal(mContext, "网络错误，请检查网络").show();
                            mRecyclerView.refreshLoadMoreView(page, 0);
                            LogUtil.i(LiveLogTag.BaseLiveTag, "getMemberListOwner--Error:" + e.getMessage());
                        }

                        @Override
                        public void onNext(MemberListResponse response) {
                            LogUtil.i(LiveLogTag.BaseLiveTag, "getMemberListOwner--Next：responseCode:" + response.code + ",responseTips:" + response.data);
                            if (response.isSuccess()) {

                                if (page == 1) {
                                    mDatas.clear();
                                    mDatas = response.data.list;
                                } else {
                                    mDatas.addAll(response.data.list);
                                }
                                mAdapter.setHaveHelper(response.data.hasNextPage);
                                mAdapter.updateData(mDatas);
                                mRecyclerView.refreshLoadMoreView(page, response.data.list.size(), 20);
                                LogUtil.i(LiveLogTag.BaseLiveTag, "getMemberListOwner--Next");

                                liveRoomMemverListDialog.refreshTabTitle(0, "已入会（" + response.data.total + "）");

                            } else {
                                Toasty.normal(mContext, response.msg).show();
                            }
                        }
                    });
        } else {
            LogUtil.i(LiveLogTag.BaseLiveTag, "getUnEnterMemberList--start");
            OMAppApiProvider.getInstance().getUnEnterMemberList(LiveRoomInfoProvider.getInstance().imRoomId, LiveRoomInfoProvider.getInstance().liveId,
                    page + "", "20", new Observer<MemberListResponse>() {

                        @Override
                        public void onCompleted() {
                            LogUtil.i(LiveLogTag.BaseLiveTag, "getUnEnterMemberList--Completed");
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toasty.normal(mContext, "网络错误，请检查网络").show();
                            mRecyclerView.refreshLoadMoreView(page, 0);
                            LogUtil.i(LiveLogTag.BaseLiveTag, "getUnEnterMemberList--Error:" + e.getMessage());
                        }

                        @Override
                        public void onNext(MemberListResponse response) {
                            LogUtil.i(LiveLogTag.BaseLiveTag, "getUnEnterMemberList--Next：responseCode:" + response.code + ",responseTips:" + response.data);
                            if (response.isSuccess()) {

                                unEnterMemberCount = 0;

                                try {
                                    unEnterMemberCount = Integer.parseInt(response.data.total);
                                } catch (NumberFormatException e) {
                                }

                                if (page == 1) {
                                    mDatas.clear();
                                    mDatas = response.data.list;
                                } else {
                                    mDatas.addAll(response.data.list);
                                }
                                mAdapter.setHaveHelper(response.data.hasNextPage);
                                mAdapter.updateData(mDatas);
                                mRecyclerView.refreshLoadMoreView(page, response.data.list.size(), 20);

                                liveRoomMemverListDialog.refreshInviteBtn(unEnterMemberCount == 0);

                                LogUtil.i(LiveLogTag.BaseLiveTag, "getUnEnterMemberList--Next");

                                liveRoomMemverListDialog.refreshTabTitle(1, "未入会（" + response.data.total + "）");

                            } else {
                                Toasty.normal(mContext, response.msg).show();
                            }
                        }
                    });
        }


    }

    public void refreshData() {
        page = 1;
        getData();
    }
}
