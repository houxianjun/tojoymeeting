package com.tojoy.cloud.online_meeting.App.Apply.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.model.OnlineMeeting.ApplyResultLeaderInfo;
import com.tojoy.cloud.online_meeting.R;

import java.util.List;

/**
 * @author qululu
 * 搜索结果列表适配器
 */
public class ApplyResultLeaderAdapter extends BaseRecyclerViewAdapter<ApplyResultLeaderInfo> {
    private String mType;
    private String mApplyStatus;

    public ApplyResultLeaderAdapter(@NonNull Context context, @NonNull List<ApplyResultLeaderInfo> datas, String type) {
        super(context, datas);
        this.mType = type;
    }

    public void setApplyStatus(String status){
        this.mApplyStatus = status;
    }
    @Override
    protected int getViewType(int position, @NonNull ApplyResultLeaderInfo data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        if (!TextUtils.isEmpty(mType) && "approval".equals(mType)) {
            return R.layout.item_approval_leader_layout;
        }
        return R.layout.item_apply_result_leader_layout;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new ApplyResultLeaderInfoViewHolder(context, parent, getLayoutResId(viewType));
    }

    public class ApplyResultLeaderInfoViewHolder extends BaseRecyclerViewHolder<ApplyResultLeaderInfo> {

        TextView mLeaderNameTv;
        TextView mApproveTimeTv;

        ApplyResultLeaderInfoViewHolder(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            mLeaderNameTv = itemView.findViewById(R.id.tv_leader_name);
            mApproveTimeTv = itemView.findViewById(R.id.tv_approve_time);
        }

        @Override
        public void onBindData(ApplyResultLeaderInfo data, int position) {
            mLeaderNameTv.setText(TextUtils.isEmpty(data.getName()) ? "" : data.getName());
            if (!TextUtils.isEmpty(mType) && "approval".equals(mType)) {
                int index = getNowCheckIndex();
                if (!TextUtils.isEmpty(mApplyStatus) && "5".equals(mApplyStatus)) {
                    mLeaderNameTv.setTextColor(Color.parseColor("#b1b5c0"));
                    mApproveTimeTv.setTextColor(Color.parseColor("#b1b5c0"));
                } else {
                    if (index != -1 && index == position) {
                        mLeaderNameTv.setTextColor(Color.parseColor("#2a62dd"));
                        mApproveTimeTv.setTextColor(Color.parseColor("#2a62dd"));
                    } else {
                        mLeaderNameTv.setTextColor(Color.parseColor("#b1b5c0"));
                        mApproveTimeTv.setTextColor(Color.parseColor("#b1b5c0"));
                    }
                }

                // 领导审批列表
                if ("1".equals(data.status) || "4".equals(data.status)) {
                    // 未审批
                    mApproveTimeTv.setVisibility(View.GONE);
                } else if ("3".equals(data.status)) {
                    // 已同意
                    mApproveTimeTv.setVisibility(View.VISIBLE);
                    mApproveTimeTv.setText(data.getAuditDate());
                } else if ("2".equals(data.status)) {
                    // 已驳回
                    mLeaderNameTv.setTextColor(Color.parseColor("#2a62dd"));
                    mApproveTimeTv.setTextColor(Color.parseColor("#2a62dd"));
                    mApproveTimeTv.setVisibility(View.VISIBLE);
                    mApproveTimeTv.setText(data.getAuditDate());
                }

            } else {

                // 服务秘书审批申请结果页面
                if ("1".equals(data.status) || "4".equals(data.status)) {
                    // 未审批
                    mLeaderNameTv.setTextColor(Color.parseColor("#7b8090"));
                    mApproveTimeTv.setVisibility(View.GONE);
                } else if ("3".equals(data.status)) {
                    // 已同意
                    mLeaderNameTv.setTextColor(Color.parseColor("#2a62dd"));
                    mApproveTimeTv.setTextColor(Color.parseColor("#2a62dd"));
                    mApproveTimeTv.setVisibility(View.VISIBLE);
                    mApproveTimeTv.setText(data.getAuditDate());
                } else if ("2".equals(data.status)) {
                    // 已驳回
                    mLeaderNameTv.setTextColor(Color.parseColor("#2a62dd"));
                    mApproveTimeTv.setTextColor(Color.parseColor("#2a62dd"));
                    mApproveTimeTv.setVisibility(View.VISIBLE);
                    mApproveTimeTv.setText(data.getAuditDate());
                }
            }
        }

        /**
         * 获取当前审批过的节点
         * 即未审批的高亮显示
         */
        private int getNowCheckIndex(){
            int index = -1;
            for (int i = 0 ; i < datas.size(); i++) {
                ApplyResultLeaderInfo leaderInfo = datas.get(i);
                if ("1".equals(leaderInfo.status) || "5".equals(leaderInfo.status)) {
                    // 只要状态是未审批
                    index = i;
                    break;
                }
            }
            return index;
        }
    }
}
