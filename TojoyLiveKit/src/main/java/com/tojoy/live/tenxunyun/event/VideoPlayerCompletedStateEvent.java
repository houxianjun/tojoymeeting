package com.tojoy.live.tenxunyun.event;

/**
 * @author fanxi
 * @date 2019/7/16.
 * description：
 */
public class VideoPlayerCompletedStateEvent {
    public int  PlayingState;
    public VideoPlayerCompletedStateEvent(int  playingState) {
        PlayingState = playingState;
    }
}
