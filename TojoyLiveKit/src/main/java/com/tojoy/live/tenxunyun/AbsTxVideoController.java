package com.tojoy.live.tenxunyun;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.tojoy.live.tenxunyun.listener.OnClickStartIconListener;
import com.tojoy.live.tenxunyun.listener.OnClickVideoScreenListener;
import com.tojoy.live.tenxunyun.listener.OnGetProgressListener;
import com.tojoy.live.tenxunyun.listener.OnPlayOrPauseListener;
import com.tojoy.live.tenxunyun.listener.OnPlayerIsPlayingListener;
import com.tojoy.live.tenxunyun.listener.OnSeekBarDragListener;
import com.tojoy.live.tenxunyun.listener.OnSimpleCLickListener;
import com.tojoy.live.tenxunyun.listener.OnTopVideoListener;
import com.tojoy.live.tenxunyun.listener.OnVideoBackListener;
import com.tojoy.live.tenxunyun.listener.OnVideoCompletedListener;
import com.tojoy.live.tenxunyun.listener.OnVideoPlayModeChangeListener;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledExecutorService;

/**
 * @author fanxi
 * @date 2019/8/8.
 * description：
 */
public abstract class AbsTxVideoController extends FrameLayout implements View.OnTouchListener {
    private Context mContext;
    private Timer mUpdateProgressTimer;
    private TimerTask mUpdateProgressTimerTask;
    private ScheduledExecutorService pool;
    protected int mCurrentMode;


    public AbsTxVideoController(Context context) {
        super(context);
        mContext = context;
        this.setOnTouchListener(this);
    }

    public AbsTxVideoController(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        this.setOnTouchListener(this);
    }


    /**
     * 开启更新进度的计时器。
     */
    protected void startUpdateProgressTimer() {
        cancelUpdateProgressTimer();
        if (mUpdateProgressTimer == null) {
            mUpdateProgressTimer = new Timer();
        }
        if (mUpdateProgressTimerTask == null) {
            mUpdateProgressTimerTask = new TimerTask() {
                @Override
                public void run() {
                    //在子线程中更新进度，包括进度条进度，展示的当前播放位置时长，总时长等。
                    AbsTxVideoController.this.post(() -> updateProgress());
                }
            };
        }
        mUpdateProgressTimer.schedule(mUpdateProgressTimerTask, 0, 1000);
    }


    /**
     * 取消更新进度的计时器。
     */
    protected void cancelUpdateProgressTimer() {
        if (mUpdateProgressTimer != null) {
            mUpdateProgressTimer.cancel();
            mUpdateProgressTimer = null;
        }
        if (mUpdateProgressTimerTask != null) {
            mUpdateProgressTimerTask.cancel();
            mUpdateProgressTimerTask = null;
        }
    }




    /**
     * 更新进度，包括进度条进度，展示的当前播放位置时长，总时长等。
     */
    protected abstract void updateProgress();

    /**
     * 获取播放百分比
     */
    protected OnGetProgressListener mOnGetProgressListener;

    public void setOnGetProgressListener(OnGetProgressListener mOnGetProgressListener) {
        this.mOnGetProgressListener = mOnGetProgressListener;
    }

    /**
     * 判断视屏播放是否是播放中的状态
     */
    protected OnPlayerIsPlayingListener mOnPlayerIsPlayingListener;

    public void setOnPlayerIsPlayingListener(OnPlayerIsPlayingListener onPlayerIsPlayingListener) {
        this.mOnPlayerIsPlayingListener = onPlayerIsPlayingListener;
    }

    /**
     * 视频单机双击的监听
     */
    protected OnSimpleCLickListener mOnSimpleCLickListener;

    public void setOnSimpleCLickListener(OnSimpleCLickListener onSimpleCLickListener) {
        this.mOnSimpleCLickListener = onSimpleCLickListener;
    }


    /**
     * 视频播放结束回调
     */
    protected OnVideoCompletedListener mOnVideoCompletedListener;

    public void setOnVideoCompletedListener(OnVideoCompletedListener onVideoCompletedListener) {
        this.mOnVideoCompletedListener = onVideoCompletedListener;
    }

    protected OnSeekBarDragListener mOnSeekBarDragListener;

    public void setOnSeekBarDragListener(OnSeekBarDragListener onSeekBarDragListener) {
        this.mOnSeekBarDragListener = onSeekBarDragListener;
    }

    /**
     * 播放开始监听事件
     */
    protected OnClickStartIconListener mOnPlayStartListener;

    public void setOnPlayStartListener(OnClickStartIconListener listener) {
        this.mOnPlayStartListener = listener;
    }

    /**
     * 点击视频播放屏幕监听
     */
    protected OnClickVideoScreenListener mOnClickVideoScreenListener;

    public void setOnClickVideoScreenListener(OnClickVideoScreenListener listener) {
        this.mOnClickVideoScreenListener = listener;
    }


    /**
     * 监听横竖屏模式改变
     */
    protected OnVideoPlayModeChangeListener mOnVideoPlayModeChangeListener;

    public void setOnVideoPlayModeChangeListener(OnVideoPlayModeChangeListener listener) {
        this.mOnVideoPlayModeChangeListener = listener;
    }

    /**
     * 当视频退出全屏或者退出小窗口后，再次点击返回键
     * 让用户自己处理返回键事件的逻辑
     */
    protected OnVideoBackListener mBackListener;

    public void setOnVideoBackListener(OnVideoBackListener listener) {
        this.mBackListener = listener;
    }

    /**
     * 播放暂停监听事件
     */
    protected OnPlayOrPauseListener mOnPlayOrPauseListener;

    public void setOnPlayOrPauseListener(OnPlayOrPauseListener listener) {
        this.mOnPlayOrPauseListener = listener;
    }

    /**
     * 竖屏情况下点击退出视屏按钮
     */
    protected OnTopVideoListener mOnExitVideoListener;

    public void setOnTopVideoListener(OnTopVideoListener mOnExitVideoListener) {
        this.mOnExitVideoListener = mOnExitVideoListener;
    }

}
