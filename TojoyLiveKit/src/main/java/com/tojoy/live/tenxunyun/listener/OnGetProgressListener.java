package com.tojoy.live.tenxunyun.listener;

public interface OnGetProgressListener {
    void getProgress(double progress);
}
