package com.tojoy.live.tenxunyun;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tencent.rtmp.TXLivePlayer;
import com.tencent.rtmp.TXVodPlayer;
import com.tencent.rtmp.ui.TXCloudVideoView;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.live.tenxunyun.event.VideoPlayerCompletedStateEvent;
import com.vhall.uilibs.R;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.tencent.rtmp.TXLiveConstants.RENDER_MODE_FULL_FILL_SCREEN;

/**
 * @author fanxi
 * @date 2019/8/8.
 * description： 控件初始化、控件点击事件、进度条拖拽事件
 * 视频播放状态、视频播放模式
 */
public class TxBaseVideoView extends AbsTxVideoController implements View.OnClickListener {
    protected Context mContext;
    protected ImageView mImage;
    protected ImageView mCenterStart;
    protected LinearLayout mTop;
    protected TextView mBack;
    protected TextView mTitle;
    protected LinearLayout mLlHorizontal;
    protected TextView mTime;
    protected LinearLayout mBottom;
    protected ImageView mRestartPause;
    protected TextView mPosition;
    protected TextView mDuration;
    protected SeekBar mSeek;
    protected ImageView mFullScreen;
    protected TextView mLength;
    protected LinearLayout mLoading;
    protected LinearLayout mChangePosition;
    protected TextView mChangePositionCurrent;
    protected ProgressBar mChangePositionProgress;
    protected LinearLayout mError;
    protected TextView mRetry;
    protected LinearLayout mCompleted;
    protected TextView mReplay;
    protected TextView mShare;
    protected ImageView mIvBack;
    protected RelativeLayout mRlImgBack;
    protected TextView mErrorTip;
    protected RelativeLayout mRlTopContainer;
    protected ImageView mIvTopRight;
    protected CountDownTimer mDismissTopBottomCountDownTimer;//倒计时
    protected TXCloudVideoView mTxCloudVideoView;

    //这个是time时间不操作界面，则自动隐藏顶部和底部视图布局
    protected long time;
    public boolean hideSeekBar = false;
    protected boolean mHideFullScreen = false;
    protected boolean topBottomVisible = false;
    protected boolean mIsShowRestartOrPauseIcon = true;
    protected String fromType = "";

    //详情页点击屏幕不能播放
    protected boolean mScreenclickable = true;
    protected String mVideoBackGroundType = "";
    protected boolean mIsShowIvBack = false;
    protected boolean mIsChatRoomVideoStyle = false;
    protected GestureDetector gestureDetector;

    /** 标识是否是直播间文档演示用户端播放视频 */
    public boolean isOnineClientClick = false;

    /** 标识是否是网上天洽会可以控制端观看视频 */
    public boolean isOnineCanControl = false;

    /** 是否是直播间的（直播间播放视频不显示top布局）*/
    public boolean mIsLivingRoom = false;

    /** 是否一直显示顶部布局（回放视频页面需要一直显示顶部）*/
    public boolean isAlwaysShowTop = false;

    /**
     * 视频播放类型  直播1 或是 回放2
     */
    protected int mVideoType = -1;
    protected TXLivePlayer mTXLivePlayer;
    protected TXVodPlayer mTXVodPlayer;
    protected String mVideoUrl;

    public ImageView mIvHome;
    public ImageView mCickList;


    public TxBaseVideoView(@NonNull Context context) {
        super(context);
        mContext = context;
        initView();
    }

    public TxBaseVideoView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView();
    }

    /**
     * 初始化操作
     */
    private void initView() {
        LayoutInflater.from(mContext).inflate(R.layout.custom_tx_video_player, this, true);
        mCenterStart = findViewById(R.id.center_start);
        mImage = findViewById(R.id.image);

        mTop = findViewById(R.id.top);
        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);

        mLlHorizontal = findViewById(R.id.ll_horizontal);
        mTime = findViewById(R.id.time);

        mBottom = findViewById(R.id.bottom);
        mRestartPause = findViewById(R.id.restart_or_pause);
        mPosition = findViewById(R.id.position);
        mDuration = findViewById(R.id.duration);
        mSeek = findViewById(R.id.seek);
        mFullScreen = findViewById(R.id.full_screen);
        mLength = findViewById(R.id.length);
        mLoading = findViewById(R.id.loading);
        mChangePosition = findViewById(R.id.change_position);
        mChangePositionCurrent = findViewById(R.id.change_position_current);
        mChangePositionProgress = findViewById(R.id.change_position_progress);

        mError = findViewById(R.id.error);
        mRetry = findViewById(R.id.retry);
        mCompleted = findViewById(R.id.completed);
        mReplay = findViewById(R.id.replay);
        mShare = findViewById(R.id.share);
        /**
         * 新增控件
         */
        mErrorTip = findViewById(R.id.errot_tip);
        mRlImgBack = findViewById(R.id.rlv_iv_back);

        mIvBack = findViewById(R.id.iv_back);
        mIvTopRight = findViewById(R.id.iv_right);
        mRlTopContainer = findViewById(R.id.rlv_top_contaner);
        mTxCloudVideoView = findViewById(R.id.video_container);

        mIvHome = findViewById(R.id.iv_home);
        mCickList =  findViewById(R.id.iv_click_list);

        initWidgetVisible();
        initListener();
    }

    /**
     * 初始化控件可见
     */
    private void initWidgetVisible() {
        mLoading.setVisibility(GONE);
        mCenterStart.setVisibility(VISIBLE);
        mReplay.setVisibility(GONE);
        setTopBottomVisible(false);
        mImage.setVisibility(VISIBLE);
    }


    /**
     * 初始化控件事件
     */
    private void initListener() {
        mCenterStart.setOnClickListener(this);
        mImage.setOnClickListener(this);
        mBack.setOnClickListener(this);
        mRestartPause.setOnClickListener(this);
        mFullScreen.setOnClickListener(this);
        mRetry.setOnClickListener(this);
        mReplay.setOnClickListener(this);
        mShare.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
        mRlImgBack.setOnClickListener(this);
        mIvTopRight.setOnClickListener(this);
        mIvHome.setOnClickListener(this);
        mCickList.setOnClickListener(this);
        mSeek.setOnSeekBarChangeListener(seekBarChangeListener);
        this.setOnClickListener(this);
        this.setFocusableInTouchMode(false);
        //屏幕单双点击事件
        doubleOrSingleClickScreen();
    }


    /**
     * 播放器进度条更新播放进度
     */
    @Override
    protected void updateProgress() {
        if (mVideoType == 2 && mTXVodPlayer != null) {
            //获取当前播放的位置，毫秒
            long position = (long) (mTXVodPlayer.getCurrentPlaybackTime() * 1000);
            //获取播放总时长，毫秒
            long duration = (long) (mTXVodPlayer.getDuration() * 1000);
            //获取视频缓冲百分比
            int bufferPercentage = (int) (mTXVodPlayer.getPlayableDuration() * 100 / mTXVodPlayer.getDuration());
            mSeek.setSecondaryProgress(bufferPercentage);
            int progress = (int) (100f * position / duration);
            mSeek.setProgress(progress);
            mPosition.setText(VideoPlayerUtils.formatTime(position));
            mDuration.setText(VideoPlayerUtils.formatTime(duration));

            // 更新时间
            mTime.setText(new SimpleDateFormat("HH:mm", Locale.CHINA).format(new Date()));
            mErrorTip.setText("视频已结束");
            // 判断是否有权限
            if (mOnGetProgressListener != null) {
                mOnGetProgressListener.getProgress(progress / 100f);
            }
        }
    }

    /**
     * 屏幕单双击事件处理
     */
    @SuppressLint("ClickableViewAccessibility")
    private void doubleOrSingleClickScreen() {
        if (mScreenclickable) {
            setOnTouchListener((v, event) -> gestureDetector.onTouchEvent(event));
        }
        gestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {//单击事件
                if (!isOnineClientClick) {
                    topBottomVisible = !topBottomVisible;
                    setTopBottomVisible(topBottomVisible);
                }

                if (isOnineCanControl && mOnSimpleCLickListener != null) {
                    mOnSimpleCLickListener.onSingleClick();
                }
                return super.onSingleTapConfirmed(e);
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {//双击事件
                if (isOnineCanControl && mOnSimpleCLickListener != null) {
                    mOnSimpleCLickListener.onDoubleClick();
                }
                if (!isOnineClientClick) {
                    if (VideoPlayerUtils.isConnected(mContext)) {
                        //重新播放或者暂停

                        if (mVideoType == 1 && mTXLivePlayer != null) {
                            //直播
                            if (mTXLivePlayer.isPlaying()) {
                                pauseVideo();
                                if (mOnPlayOrPauseListener != null) {
                                    mOnPlayOrPauseListener.onPlayOrPauseClick(false);
                                }

                            } else {
                                if (mOnPlayOrPauseListener != null) {
                                    mOnPlayOrPauseListener.onPlayOrPauseClick(true);
                                }
                                restartVideo();
                            }

                        } else if (mVideoType == 2 && mTXVodPlayer != null) {
                            //回放
                            if (mTXVodPlayer.isPlaying()) {
                                pauseVideo();
                                if (mOnPlayOrPauseListener != null) {
                                    mOnPlayOrPauseListener.onPlayOrPauseClick(false);
                                }

                            } else {
                                if (mOnPlayOrPauseListener != null) {
                                    mOnPlayOrPauseListener.onPlayOrPauseClick(true);
                                }
                                restartVideo();
                            }

                        }

                    } else {
                        Toast.makeText(mContext, "请检测是否有网络", Toast.LENGTH_SHORT).show();
                    }
                }

                return super.onDoubleTap(e);
            }

            /**
             * * 双击手势过程中发生的事件，包括按下、移动和抬起事件
             * * @param e
             * * @return */
            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                return super.onDoubleTapEvent(e);
            }
        });
    }

    /**
     * 拖动进度条播放
     */
    private SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            if (mVideoType == 2 && mTXVodPlayer != null) {
                //回放
                long position = (long) (mTXVodPlayer.getDuration() * seekBar.getProgress() / 100f);

                if (!mTXVodPlayer.isPlaying()) {
                    mTXVodPlayer.resume();
                    mTXVodPlayer.seek(position);
                } else {
                    mTXVodPlayer.seek(position);
                }
                if (mOnSeekBarDragListener != null) {
                    mOnSeekBarDragListener.onSeekBarDragFinished(position);
                }
                startDismissTopBottomTimer();
            }
        }
    };


    /**
     * 播放状态改变
     *
     * @param playState
     */
    protected void onPlayStateChanged(int playState) {
        switch (playState) {
            //播放准备就绪
            case ConstantKeys.CurrentState.STATE_PREPARED:
                startUpdateProgressTimer();
                mImage.setVisibility(GONE);
                mRestartPause.setImageResource(R.drawable.ic_player_pause);
                mCenterStart.setVisibility(GONE);
                break;
            //正在播放
            case ConstantKeys.CurrentState.STATE_PLAYING:
                mLoading.setVisibility(View.GONE);
                mRestartPause.setImageResource(R.drawable.ic_player_pause);
                startDismissTopBottomTimer();
                mImage.setVisibility(GONE);
                mCenterStart.setVisibility(GONE);
                if (mOnPlayerIsPlayingListener != null) {
                    mOnPlayerIsPlayingListener.onPlaying(true);
                }


                break;
            //暂停播放
            case ConstantKeys.CurrentState.STATE_PAUSED:
                mLoading.setVisibility(View.GONE);
                mRestartPause.setImageResource(R.drawable.ic_player_start);
                cancelDismissTopBottomTimer();
                if (mOnPlayerIsPlayingListener != null) {
                    mOnPlayerIsPlayingListener.onPlaying(false);
                }
                mCenterStart.setVisibility(VISIBLE);
                break;

            //播放完成
            case ConstantKeys.CurrentState.STATE_COMPLETED:
                EventBus.getDefault().post(new VideoPlayerCompletedStateEvent(1));
                initWidgetVisible();
                if (mOnVideoCompletedListener != null) {
                    mOnVideoCompletedListener.onVideoCompleted();
                }

                if (hideSeekBar) {
                    mCompleted.setVisibility(View.GONE);
                } else {
                    if (mIsChatRoomVideoStyle) {
                        mCenterStart.setVisibility(mIsShowRestartOrPauseIcon ? VISIBLE : GONE);
                        mCompleted.setVisibility(View.GONE);
                    } else {
                        mCompleted.setVisibility(View.VISIBLE);
                    }
                }

                if (mOnPlayerIsPlayingListener != null) {
                    mOnPlayerIsPlayingListener.onPlaying(false);
                }
                break;
            default:
                break;
        }
    }

    /**
     * 控件点击事件
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        if (isOnineClientClick) {
            return;
        }
        if (v == mCenterStart) {
            mLoading.setVisibility(VISIBLE);
            mCenterStart.setVisibility(GONE);


            //聊天室里面点击播放按钮播放
            if (mIsChatRoomVideoStyle && !(ConstantKeys.ONLINE_MEETING_LIVE_REPLAY).equals(fromType)) {
                restartVideo();
                mCenterStart.setVisibility(GONE);
            }

            //非认证点击视屏播放
            if ((ConstantKeys.ONLINE_MEETING_LIVE_REPLAY).equals(fromType) || (ConstantKeys.PLAYER_ISLIVING_LIST).equals(fromType)) {
                restartVideo();
                return;
            }
            // 开始播放
            startVideo();
            if (mOnPlayOrPauseListener != null) {
                mOnPlayOrPauseListener.onPlayOrPauseClick(true);
            }
            mCenterStart.setVisibility(GONE);
            if (mOnPlayStartListener != null) {
                mOnPlayStartListener.onPlayStartClick(true);
            }
        } else if (v == mBack) {
            //退出，执行返回逻辑
            //如果是全屏，则先退出全屏
            if (mVideoType == 1 && mTXLivePlayer != null) {
                //直播
                ((Activity) mContext).finish();
            } else if (mVideoType == 2 && mTXVodPlayer != null) {
                //回放
                ((Activity) mContext).finish();
            }
        } else if (v == mRlImgBack) {
            //竖屏点击关闭视屏按钮
            if (mOnExitVideoListener != null) {
                mOnExitVideoListener.clickExitVideoBackIm();
            }
            ((Activity) mContext).finish();
        } else if (v == mRestartPause) {
            if (VideoPlayerUtils.isConnected(mContext)) {
                if (mVideoType == 1 && mTXLivePlayer != null) {
                    //直播
                    if (!mTXLivePlayer.isPlaying()) {
                        if (mOnPlayOrPauseListener != null) {
                            mOnPlayOrPauseListener.onPlayOrPauseClick(true);
                        }
                        mTXLivePlayer.resume();
                        mRestartPause.setImageResource(R.drawable.ic_player_pause);

                    } else {
                        mTXLivePlayer.pause();
                        if (mOnPlayOrPauseListener != null) {
                            mOnPlayOrPauseListener.onPlayOrPauseClick(false);
                        }
                        mRestartPause.setImageResource(R.drawable.ic_player_start);
                    }

                } else if (mVideoType == 2 && mTXVodPlayer != null) {
                    //回放
                    if (!mTXVodPlayer.isPlaying()) {
                        if (mOnPlayOrPauseListener != null) {
                            mOnPlayOrPauseListener.onPlayOrPauseClick(true);
                        }
                        mTXVodPlayer.resume();
                        mRestartPause.setImageResource(R.drawable.ic_player_pause);
                    } else {
                        mTXVodPlayer.pause();
                        if (mOnPlayOrPauseListener != null) {
                            mOnPlayOrPauseListener.onPlayOrPauseClick(false);
                        }
                        mRestartPause.setImageResource(R.drawable.ic_player_start);
                    }
                }

            } else {
                Toast.makeText(mContext, "请检测是否有网络", Toast.LENGTH_SHORT).show();
            }
        } else if (v == mFullScreen) {
        } else if (v == mRetry) {

            if (VideoPlayerUtils.isConnected(mContext)) {
                if (mVideoType == 1 && mTXLivePlayer != null && !mTXLivePlayer.isPlaying()) {
                    //直播
                    mTXLivePlayer.resume();
                } else if (mVideoType == 2 && mTXVodPlayer != null && !mTXVodPlayer.isPlaying()) {
                    //回放
                    mTXVodPlayer.resume();
                }
            } else {
                Toast.makeText(mContext, "请检测是否有网络", Toast.LENGTH_SHORT).show();
            }
        } else if (v == mReplay) {
            //重新播放
            if (VideoPlayerUtils.isConnected(mContext)) {
                mRetry.performClick();
            } else {
                Toast.makeText(mContext, "请检测是否有网络", Toast.LENGTH_SHORT).show();
            }
        } else if (v == this) {
            if (mOnClickVideoScreenListener != null) {
                mOnClickVideoScreenListener.onClickVideoScreen();
            }
        } else if (v == mIvTopRight) {
            //点击分享
            if (mOnExitVideoListener != null) {
                mOnExitVideoListener.clickShareImg();
            }
        } else if (v == mIvHome) {
            //点击主页
            if (mOnExitVideoListener != null) {
                mOnExitVideoListener.clickEnterpriseHome();
            }
        } else if (v == mCickList) {
            //点击列表布局
            if (mOnExitVideoListener != null) {
                mOnExitVideoListener.clickLeftList();
            }
        } else if (v == mIvBack) {
            if (mOnExitVideoListener != null) {
                mOnExitVideoListener.clickExitVideoBackIm();
            }
        }

    }

    /**
     * 视频播放模式
     *
     * @param playMode
     */
    public void onPlayModeChanged(int playMode) {
        switch (playMode) {
            //普通模式
            case ConstantKeys.PlayMode.MODE_NORMAL:
                mBack.setVisibility(View.GONE);
                mFullScreen.setImageResource(R.drawable.ic_player_fullscreen);
                if (!mHideFullScreen) {
                    mFullScreen.setVisibility(View.VISIBLE);
                }
                mLlHorizontal.setVisibility(View.GONE);
                mRlImgBack.setVisibility(GONE);
                if (mIsShowIvBack) {
                    mRlImgBack.setVisibility(VISIBLE);

                }
                if (mIsChatRoomVideoStyle) {
                    mIvTopRight.setVisibility(VISIBLE);
                    mFullScreen.setVisibility(GONE);
                }
                EventBus.getDefault().post("MODE_NORMAL");
                if (mIsChatRoomVideoStyle) {
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mTop.getLayoutParams();
                    layoutParams.width = AppUtils.getScreenWidth(mContext);
                    mTop.setLayoutParams(layoutParams);
                }

                break;
            //全屏模式
            case ConstantKeys.PlayMode.MODE_FULL_SCREEN:
                mBack.setVisibility(View.VISIBLE);
                if (!mHideFullScreen) {
                    mFullScreen.setVisibility(View.VISIBLE);
                }
                mFullScreen.setImageResource(R.drawable.ic_player_shrink);
                mLlHorizontal.setVisibility(View.GONE);

                mRlImgBack.setVisibility(GONE);
                if (mIsShowIvBack) {
                    mRlImgBack.setVisibility(VISIBLE);
                }
                EventBus.getDefault().post("MODE_FULL_SCREEN");
                if (mIsChatRoomVideoStyle) {
                    mIvTopRight.setVisibility(GONE);
                    RelativeLayout.LayoutParams layoutParamsFull = (RelativeLayout.LayoutParams) mTop.getLayoutParams();
                    layoutParamsFull.width = AppUtils.getScreenHeight(mContext);
                    mTop.setLayoutParams(layoutParamsFull);
                }
                break;
            //小窗口模式
            case ConstantKeys.PlayMode.MODE_TINY_WINDOW:
                mBack.setVisibility(View.GONE);
                mRlImgBack.setVisibility(GONE);
                break;
            default:
                break;
        }
    }


    /**
     * 设置top、bottom的显示和隐藏
     *
     * @param visible true显示，false隐藏.
     */
    public void setTopBottomVisible(boolean visible) {

        if (isAlwaysShowTop) {
            mTop.setVisibility(View.VISIBLE);
        } else {
            mTop.setVisibility((visible && !mIsLivingRoom) ? View.VISIBLE : View.GONE);
        }
        mBottom.setVisibility(visible ? View.VISIBLE : View.GONE);

        if (visible) {
            if (mVideoType == 1 && mTXLivePlayer != null) {
                //直播
                if (mTXLivePlayer.isPlaying()) {
                    startDismissTopBottomTimer();
                }
            } else if (mVideoType == 2 && mTXVodPlayer != null) {
                //回放
                if (mTXVodPlayer.isPlaying()) {
                    startDismissTopBottomTimer();
                }
            }
        } else {
            cancelDismissTopBottomTimer();
        }
        setLiveSeekType(hideSeekBar);
        if (mHideFullScreen) {
            mFullScreen.setVisibility(GONE);
        }
    }

    /**
     * 开启top、bottom自动消失的timer
     * 比如，视频常用功能，当用户5秒不操作后，自动隐藏头部和顶部
     */
    private void startDismissTopBottomTimer() {
        if (time == 0) {
            time = 8000;
        }
        cancelDismissTopBottomTimer();
        if (mDismissTopBottomCountDownTimer == null) {
            mDismissTopBottomCountDownTimer = new CountDownTimer(time, time) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    setTopBottomVisible(false);
                }
            };
        }
        mDismissTopBottomCountDownTimer.start();
    }

    /**
     * 取消top、bottom自动消失的timer
     */
    private void cancelDismissTopBottomTimer() {
        if (mDismissTopBottomCountDownTimer != null) {
            mDismissTopBottomCountDownTimer.cancel();
        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }


    /**
     * 进入全屏模式
     */
    public void enterFullScreen() {
        if (mCurrentMode == ConstantKeys.PlayMode.MODE_FULL_SCREEN) {
            return;
        }
        // 隐藏ActionBar、状态栏，并横屏
        VideoPlayerUtils.hideActionBar(mContext);
        VideoPlayerUtils.scanForActivity(mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        ViewGroup contentView = VideoPlayerUtils.scanForActivity(mContext).findViewById(android.R.id.content);
        if (mCurrentMode == ConstantKeys.PlayMode.MODE_TINY_WINDOW) {
            contentView.removeView(mTxCloudVideoView);
        } else {
            this.removeView(mTxCloudVideoView);
        }
        contentView.removeView(mTxCloudVideoView);
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        contentView.addView(mTxCloudVideoView, params);
        mCurrentMode = ConstantKeys.PlayMode.MODE_FULL_SCREEN;
        if (mVideoType == 1 && mTXLivePlayer != null) {
            mTXLivePlayer.setRenderMode(RENDER_MODE_FULL_FILL_SCREEN);
        } else if (mVideoType == 2 && mTXVodPlayer != null) {
            mTXVodPlayer.setRenderMode(RENDER_MODE_FULL_FILL_SCREEN);
        }
        onPlayModeChanged(mCurrentMode);
    }


    private void setLiveSeekType(boolean ishide) {
        if (ishide && !mIsChatRoomVideoStyle) {
            mSeek.setVisibility(GONE);
            mDuration.setVisibility(GONE);
            mPosition.setVisibility(GONE);
        } else {
            mSeek.setVisibility(VISIBLE);
            mDuration.setVisibility(VISIBLE);
            mPosition.setVisibility(VISIBLE);

        }
    }
    /**
     * ===============================================外部调用方法===============================================
     */

    /**
     * 视频暂停的方法
     */
    public void pauseVideo() {
        if (mVideoType == 1 && mTXLivePlayer != null) {
            //直播
            mTXLivePlayer.pause();
        } else if (mVideoType == 2 && mTXVodPlayer != null) {
            //回放
            mTXVodPlayer.pause();
        }
        onPlayStateChanged(ConstantKeys.CurrentState.STATE_PAUSED);

    }

    /**
     * 重新播放视频
     */
    public void restartVideo() {
        if (mVideoType == 1 && mTXLivePlayer != null) {
            //直播
            mTXLivePlayer.resume();
        } else if (mVideoType == 2 && mTXVodPlayer != null) {
            //回放
            mTXVodPlayer.resume();
        }
        onPlayStateChanged(ConstantKeys.CurrentState.STATE_PLAYING);

    }

    /**
     * 设置视频播放
     */
    public void startVideo() {
        if (mVideoType == 1 && mTXLivePlayer != null) {
            //直播
            mVideoUrl = mVideoUrl.substring(0, mVideoUrl.lastIndexOf("."));
            mVideoUrl = mVideoUrl + ".flv";
            mTXLivePlayer.startPlay(mVideoUrl, TXLivePlayer.PLAY_TYPE_LIVE_FLV);
        } else if (mVideoType == 2 && mTXVodPlayer != null) {
            //回放
            mTXVodPlayer.startPlay(mVideoUrl);
        }
    }

    /**
     * 只有回放才可设置视频播放位置
     */
    public void startVodVideo(float startTime) {
        if (mVideoType == 2 && mTXVodPlayer != null) {
            //回放
            mTXVodPlayer.resume();
            // 获取视频总时长，若播放位置大于视频总时长则显示最后的位置
            float duration = mTXVodPlayer.getDuration();
            mTXVodPlayer.seek(startTime > duration ? (long) duration : startTime);
        }
    }

    public long getVodCurrentTime() {
        if (mTXVodPlayer != null) {
            return (long) mTXVodPlayer.getCurrentPlaybackTime();
        } else {
            return 0;
        }
    }

    /**
     * 私密会议需求：新增隐藏／显示分享按钮方法
     */
    public void setTopShareBtnVisibility(int visibility) {
        mIvTopRight.setVisibility(visibility);
    }
}
