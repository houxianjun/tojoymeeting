package com.tojoy.live.tenxunyun.listener;


/**
 * 进度条拖动的listener
 */
public interface OnSeekBarDragListener {
    /**
     * 手指拖动结束的回调
     */
    void onSeekBarDragFinished(long positon);
}
