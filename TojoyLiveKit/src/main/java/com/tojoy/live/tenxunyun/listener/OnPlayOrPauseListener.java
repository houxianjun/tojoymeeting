package com.tojoy.live.tenxunyun.listener;

/**
 * 播放暂停点击事件抽象接口
 */
public interface OnPlayOrPauseListener {

    void onPlayOrPauseClick(boolean isStart);

}
