package com.tojoy.live.tenxunyun.listener;

/**
 * @author fanxi
 * @date 2019/7/11.
 * description：
 */
public interface OnVideoCompletedListener {
    void onVideoCompleted();
}
