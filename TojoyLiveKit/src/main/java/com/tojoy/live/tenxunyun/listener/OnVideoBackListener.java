package com.tojoy.live.tenxunyun.listener;

/**
 * 点击事件抽象接口
 */
public interface OnVideoBackListener {

    void onBackClick();

}
