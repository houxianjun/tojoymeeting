package com.tojoy.live.tenxunyun.listener;

/**
 * @author fanxi
 * @date 2019/7/12.
 * description：
 */
public interface OnClickVideoScreenListener {
    void onClickVideoScreen();
}
