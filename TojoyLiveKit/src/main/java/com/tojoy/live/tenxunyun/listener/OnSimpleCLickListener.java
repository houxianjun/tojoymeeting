package com.tojoy.live.tenxunyun.listener;

/**
 * 单机双击的监听
 */
public interface OnSimpleCLickListener {

    void onSingleClick();

    void onDoubleClick();
}
