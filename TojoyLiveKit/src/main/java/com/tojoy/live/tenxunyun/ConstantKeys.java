package com.tojoy.live.tenxunyun;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 常量
 */
public class ConstantKeys {

    public static final String PLAYER_ISLIVING_LIST="livingPlayerList";
    public static final String ONLINE_MEETING_LIVE_REPLAY="LiveReplay";//网洽会回放视屏
    //直播类型
    public static final String LIVING_VIDEO="1";
    //白色的视屏类型type
    public static final String WHITE_BG_TYPE="1";
    /**
     * 播放模式
     * -1               播放错误
     * 0                播放未开始
     * 1                播放准备中
     * 2                播放准备就绪
     * 3                正在播放
     * 4                暂停播放
     * 5                正在缓冲(播放器正在播放时，缓冲区数据不足，进行缓冲，缓冲区数据足够后恢复播放)
     * 6                正在缓冲(播放器正在播放时，缓冲区数据不足，进行缓冲，此时暂停播放器，继续缓冲，缓冲区数据足够后恢复暂停
     * 7                播放完成
     */
    public @interface CurrentState{
        int STATE_ERROR = -1;
        int STATE_IDLE = 0;
        int STATE_PREPARING = 1;
        int STATE_PREPARED = 2;
        int STATE_PLAYING = 3;
        int STATE_PAUSED = 4;
        int STATE_BUFFERING_PLAYING = 5;
        int STATE_BUFFERING_PAUSED = 6;
        int STATE_COMPLETED = 7;
    }

    /**
     * 播放模式，普通模式，小窗口模式，正常模式三种其中一种
     */
    @Retention(RetentionPolicy.SOURCE)
    public @interface PlayMode {
        int MODE_NORMAL = 1001;
        int MODE_FULL_SCREEN = 1002;
        int MODE_TINY_WINDOW = 1003;
    }
}
