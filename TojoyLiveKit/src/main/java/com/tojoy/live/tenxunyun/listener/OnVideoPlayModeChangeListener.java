package com.tojoy.live.tenxunyun.listener;

/**
 * @author fanxi
 * @date 2019/7/1.
 * description：监听视屏横竖屏的回调
 */
public interface OnVideoPlayModeChangeListener {
    void onVideoPlayModeChange(int playMode);
}
