package com.tojoy.live.tenxunyun.event;

/**
 * 判断视屏是否是播放的状态 1代表播放
 */
public class VideoPlayerIsPlayingStateEvent {
    public int  PlayingState;

    public VideoPlayerIsPlayingStateEvent(int  playingState) {
        PlayingState = playingState;
    }
}
