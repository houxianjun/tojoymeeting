package com.tojoy.live.tenxunyun;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.tencent.rtmp.ITXLivePlayListener;
import com.tencent.rtmp.ITXVodPlayListener;
import com.tencent.rtmp.TXLiveConstants;
import com.tencent.rtmp.TXLivePlayConfig;
import com.tencent.rtmp.TXLivePlayer;
import com.tencent.rtmp.TXVodPlayConfig;
import com.tencent.rtmp.TXVodPlayer;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.live.tenxunyun.event.VideoPlayerIsPlayingStateEvent;
import com.vhall.uilibs.R;

import org.greenrobot.eventbus.EventBus;

import es.dmoral.toasty.Toasty;

import static com.tencent.rtmp.TXLiveConstants.EVT_PARAM1;
import static com.tencent.rtmp.TXLiveConstants.EVT_PARAM2;
import static com.tencent.rtmp.TXLiveConstants.PLAY_EVT_CHANGE_RESOLUTION;
import static com.tencent.rtmp.TXLiveConstants.PLAY_EVT_PLAY_BEGIN;
import static com.tencent.rtmp.TXLiveConstants.PLAY_EVT_PLAY_END;
import static com.tencent.rtmp.TXLiveConstants.PLAY_EVT_RCV_FIRST_I_FRAME;
import static com.tencent.rtmp.TXLiveConstants.PLAY_EVT_VOD_PLAY_PREPARED;
import static com.tencent.rtmp.TXLiveConstants.RENDER_MODE_ADJUST_RESOLUTION;
import static com.tencent.rtmp.TXLiveConstants.RENDER_MODE_FULL_FILL_SCREEN;

/**
 * @author fanxi
 * @date 2019/8/13.
 * description： 控制ui样式
 */
public class TxVideoPlayer extends TxBaseVideoView {

    public TxVideoPlayer(@NonNull Context context) {
        super(context);
    }

    public TxVideoPlayer(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TXVodPlayer getVoPlayer() {
        return mTXVodPlayer;
    }

    public TXLivePlayer getLivePlayer() {
        return mTXLivePlayer;
    }


    /**
     * 判断是否是直播，直播seekbar隐藏
     *
     * @param
     */
    public void setLivingType(String type) {
        if (ConstantKeys.LIVING_VIDEO.equals(type)) {
            hideSeekBar = true;
        } else {
            hideSeekBar = false;
        }
    }


    /**
     * 设置视屏背景颜色
     *
     * @param type 1代表白色
     */
    public void setVideoPlayType(String type) {
        if (type.equals(ConstantKeys.WHITE_BG_TYPE)) {
            mVideoBackGroundType = type;
        }
    }

    /**
     * 自动已全屏的返回键样式
     *
     * @param drawableRes
     */
    public void setFullScreenBackImage(int drawableRes) {
        mIvBack.setBackgroundResource(drawableRes);
        mIsShowIvBack = true;
        mRlImgBack.setVisibility(VISIBLE);
        mRlTopContainer.setVisibility(VISIBLE);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(AppUtils.getScreenWidth(mContext), AppUtils.dip2px(mContext, 80));
        mTop.setLayoutParams(layoutParams);

    }

    /**
     * 自动已全屏的返回键样式
     *
     * @param drawableRes
     */
    public void setTopRightImg(int drawableRes) {
        mIvTopRight.setBackgroundResource(drawableRes);
        //网洽会分享、非认证回放限制功能
        fromType = ConstantKeys.ONLINE_MEETING_LIVE_REPLAY;
        mIvTopRight.setVisibility(VISIBLE);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(AppUtils.getScreenWidth(mContext), AppUtils.dip2px(mContext, 80));
        mTop.setLayoutParams(layoutParams);

    }


    //是否是聊天室视屏播放样式
    public void setChatRoomStyle(boolean isChatRoomVideoStyle) {
        this.mIsChatRoomVideoStyle = isChatRoomVideoStyle;
    }

    //是否是直播间
    public void setLivingRoom(boolean isLiving) {
        this.mIsLivingRoom = isLiving;
    }


    /**
     * 设置视屏左下角播放暂停按钮是否显示（网洽会官方大会隐藏按钮）
     *
     * @param isShowRestartOrPauseIcon
     */
    public void showRestartOrPauseIcon(boolean isShowRestartOrPauseIcon) {
        mIsShowRestartOrPauseIcon = isShowRestartOrPauseIcon;
        mRestartPause.setVisibility(isShowRestartOrPauseIcon ? VISIBLE : GONE);
        mCenterStart.setVisibility(isShowRestartOrPauseIcon ? VISIBLE : GONE);
    }

    /**
     * 设置视屏背景是否显示阴影
     *
     * @param isShow
     */
    public void setVideoTopGreyBg(boolean isShow) {
        if (isShow) {
            mTop.setBackground(mContext.getResources().getDrawable(R.drawable.player_mask_top));
        } else {
            mTop.setBackground(null);

        }
    }


    public void releaseVideoSource() {
        if (mTXLivePlayer != null) {
            mTXLivePlayer.pause();
            // 代表清除最后一帧画面
            mTXLivePlayer.stopPlay(false);
            mTXLivePlayer = null;

        }
        if (mTXVodPlayer != null) {
            mTXVodPlayer.pause();
            // 代表清除最后一帧画面
            mTXVodPlayer.stopPlay(false);
            mTXVodPlayer = null;

        }
        if (mTxCloudVideoView != null) {
            mTxCloudVideoView.onDestroy();
        }
    }

    /**
     * 释放掉播放器的监听，暂时只对直播间文档演示
     */
    public void releaseVideoListener() {
        // 释放播放/暂停的监听
        mOnPlayOrPauseListener = null;
        // 释放底部拖动进度条的监听
        mOnSeekBarDragListener = null;
        // 释放双击/单机的监听
        mOnSimpleCLickListener = null;
    }

    /**
     * 自定义  隐藏时间
     */
    public void hideLiveTime() {
        mLength.setVisibility(GONE);
    }


    boolean isNeetShowStart = false;

    public void setNeetShowStart(boolean neetShowStart) {
        this.isNeetShowStart = neetShowStart;
    }

    /**
     * 设置错误提示
     */
    public void setErrorTip(String errorTip) {
        mErrorTip.setText(errorTip);
    }

    /**
     * 是否展示全屏按钮
     *
     * @param isShowFull
     */
    public void showFullScreenIcon(boolean isShowFull) {
        if (isShowFull) {
            mFullScreen.setVisibility(VISIBLE);
        } else {
            mFullScreen.setVisibility(GONE);
        }
    }


    /**
     * 设置视屏封面
     */
    public TxVideoPlayer setCoverUrl(String coverUrl) {
        ImageLoaderManager.INSTANCE.loadImage(mContext,
                mImage,
                coverUrl,
                0);
        return this;
    }


    /**
     * 设置视频播放地址
     *
     * @param videoUrl
     * @param videoType
     * @return
     */
    public TxVideoPlayer setVideoUrl(String videoUrl, int videoType) {
        this.mVideoType = videoType;
        this.mVideoUrl = videoUrl;
        initVideoPlayer();
        if (TextUtils.isEmpty(mVideoUrl)) {
            Toasty.normal(mContext, "视频播放地址不能为空").show();
            return this;
        }
        return this;

    }

    public String getVideoUrl() {
        return TextUtils.isEmpty(mVideoUrl) ? "" : mVideoUrl;
    }


    /**
     * 初始化视频播放器
     */
    private void initVideoPlayer() {
        if (mVideoType == 1) {
            if (mTXLivePlayer == null) {
                //直播
                mTXLivePlayer = new TXLivePlayer(mContext);
                TXLivePlayConfig config = new TXLivePlayConfig();
                config.setEnableMessage(false);
                config.enableAEC(false);
                config.setConnectRetryCount(10);
                config.setConnectRetryInterval(5);
                config.setAutoAdjustCacheTime(true);
                config.setMinAutoAdjustCacheTime(1);
                config.setMaxAutoAdjustCacheTime(1);
                mTXLivePlayer.setConfig(config);
                mTXLivePlayer.setRenderRotation(TXLiveConstants.RENDER_ROTATION_PORTRAIT);
                mTXLivePlayer.enableHardwareDecode(true);
                mTXLivePlayer.setRenderMode(RENDER_MODE_ADJUST_RESOLUTION);
                mTXLivePlayer.setPlayerView(mTxCloudVideoView);

            }
            mTXLivePlayer.setPlayListener(new ITXLivePlayListener() {
                @Override
                public void onPlayEvent(int i, Bundle bundle) {
                    if (i == PLAY_EVT_VOD_PLAY_PREPARED) {
                        onPlayStateChanged(ConstantKeys.CurrentState.STATE_PREPARED);
                    }
                    if (i == PLAY_EVT_PLAY_BEGIN) {
                        onPlayStateChanged(ConstantKeys.CurrentState.STATE_PLAYING);

                    }

                    if (i == PLAY_EVT_PLAY_END) {
                        onPlayStateChanged(ConstantKeys.CurrentState.STATE_COMPLETED);
                    }

//                    if (i == PLAY_EVT_CHANGE_RESOLUTION) {
//                        int width = bundle.getInt(EVT_PARAM1);
//                        int height = bundle.getInt(EVT_PARAM2);
//                        if (width > height) {
//                            mTXLivePlayer.setRenderMode(RENDER_MODE_ADJUST_RESOLUTION);
//                        } else {
//                            mTXLivePlayer.setRenderMode(RENDER_MODE_FULL_FILL_SCREEN);
//                        }
//                    }
                }

                @Override
                public void onNetStatus(Bundle bundle) {

                }
            });
        } else if (mVideoType == 2) {
            if (mTXVodPlayer == null) {
                //点播
                mTXVodPlayer = new TXVodPlayer(mContext);
                TXVodPlayConfig vodConfig = new TXVodPlayConfig();
                vodConfig.setConnectRetryCount(10);
                vodConfig.setConnectRetryInterval(5);
                mTXVodPlayer.setConfig(vodConfig);
                mTXVodPlayer.setRenderRotation(TXLiveConstants.RENDER_ROTATION_PORTRAIT);
                mTXVodPlayer.enableHardwareDecode(true);
                mTXVodPlayer.setRenderMode(RENDER_MODE_ADJUST_RESOLUTION);
                mTXVodPlayer.setPlayerView(mTxCloudVideoView);
            }
            mTXVodPlayer.setVodListener(new ITXVodPlayListener() {
                @Override
                public void onPlayEvent(TXVodPlayer txVodPlayer, int i, Bundle bundle) {
                    if (i == PLAY_EVT_VOD_PLAY_PREPARED) {
                        onPlayStateChanged(ConstantKeys.CurrentState.STATE_PREPARED);
                    }
                    if (i == PLAY_EVT_PLAY_BEGIN) {
                        onPlayStateChanged(ConstantKeys.CurrentState.STATE_PLAYING);
                        // 权限版本修复的bug18636，主播暂停后，观众进入直播间换面黑屏。方案添加了播放成功事件回调监听。
                        // 回调后将监听置空
                        // 要在播放成功事件（PLAY_EVT_PLAY_BEGIN）之后调用TXVodPlayer.seek()才有效。播放器没有播放成功，无数据源信息，设置播放器进度是无效的。seek会重复触发播放事件的PLAY_EVT_PLAY_BEGIN事件，请注意不要反复seek。
                        if (mfirstFrameCallBack != null) {
                            mfirstFrameCallBack.firstFrame();
                            mfirstFrameCallBack = null;
                        }
                    }

                    if (i == PLAY_EVT_PLAY_END) {
                        onPlayStateChanged(ConstantKeys.CurrentState.STATE_COMPLETED);
                    }
                    if (i == PLAY_EVT_RCV_FIRST_I_FRAME) {//首帧画面
                        EventBus.getDefault().post(new VideoPlayerIsPlayingStateEvent(1));
                    }
/* 代码在修复bug 14903 注释掉，与小程序显示规则同步，使用有黑边的画面模式。
   影响范围：回放视频，直播间视频文件播放，参会记录，我的会议
 */

//                    if (i == PLAY_EVT_CHANGE_RESOLUTION) {
//                        int width = bundle.getInt(EVT_PARAM1);
//                        int height = bundle.getInt(EVT_PARAM2);
//                        if (width > height) {
//                            mTXVodPlayer.setRenderMode(RENDER_MODE_ADJUST_RESOLUTION);
//                        } else {
//                            mTXVodPlayer.setRenderMode(RENDER_MODE_FULL_FILL_SCREEN);
//                        }
//                    }
                }

                @Override
                public void onNetStatus(TXVodPlayer txVodPlayer, Bundle bundle) {

                }
            });
        }
    }

    public void autoStart() {
        mCenterStart.performClick();
    }

    /**
     * 判断视频是否正在播放
     *
     * @return true表示正在播放
     */
    public boolean isPlaying() {
        if (mVideoType == 1 && mTXLivePlayer != null) {
            return mTXLivePlayer.isPlaying();
        } else if (mVideoType == 2) {
            return mTXVodPlayer.isPlaying();

        }
        return false;
    }

    /**
     * 是否隐藏全屏播放按钮
     */

    public void isShowFullScreenLayout(boolean isShow) {
        mHideFullScreen = !isShow;
        mFullScreen.setVisibility(mHideFullScreen ? GONE : VISIBLE);

    }


    /**
     * 初始时是否显示中间的播放按钮-针对直播间文档演示视频
     *
     * @param isInitShowCenterStart
     */
    public void isInitShowCenterStart(boolean isInitShowCenterStart) {
        mCenterStart.setVisibility(isInitShowCenterStart ? GONE : VISIBLE);

    }

    /**
     * 初始时是否显示中间的播放按钮-针对直播间文档演示视频
     *
     * @param isAlwaysShow
     */
    public void isAlwaysShowTop(boolean isAlwaysShow) {
        isAlwaysShowTop = isAlwaysShow;
        if (mTop != null) {
            mTop.setVisibility(VISIBLE);
        }
    }

    /**
     * 播放器播放成功的回调
     */
    FirstFrameCallBack mfirstFrameCallBack;

    public void setFirstFrameCallBack(FirstFrameCallBack firstFrameCallBack) {
        this.mfirstFrameCallBack = firstFrameCallBack;
    }

    public interface FirstFrameCallBack {
        void firstFrame();
    }
}
