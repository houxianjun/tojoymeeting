package com.tojoy.live.tenxunyun.listener;

/**
 * 专用做独角兽详情页的按钮和视屏的按钮的状态同步
 */
public interface OnPlayerIsPlayingListener {
    void onPlaying(boolean b);
}
