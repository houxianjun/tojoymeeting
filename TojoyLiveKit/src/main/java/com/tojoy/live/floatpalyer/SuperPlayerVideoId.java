package com.tojoy.live.floatpalyer;

public class SuperPlayerVideoId {
    public String fileId;
    public String timeout;
    public int exper = -1;
    public String us;
    public String sign;

    public SuperPlayerVideoId() {
    }

    public String toString() {
        return "SuperPlayerVideoId{, fileId='" + this.fileId + '\'' + ", timeout='" + this.timeout + '\'' + ", exper=" + this.exper + ", us='" + this.us + '\'' + ", sign='" + this.sign + '\'' + '}';
    }
}
