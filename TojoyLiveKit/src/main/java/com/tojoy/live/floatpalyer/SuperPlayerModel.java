package com.tojoy.live.floatpalyer;

import java.util.List;

public class SuperPlayerModel {
    public int appId;
    public String title = "";
    public SuperPlayerVideoId videoId;
    public String url = "";
    public String qualityName = "原画";
    public List<SuperPlayerURL> multiURLs;
    public int playDefaultIndex;

    public SuperPlayerModel() {
    }

    public static class SuperPlayerURL {
        public String qualityName = "原画";
        public String url = "";

        public SuperPlayerURL(String url, String qualityName) {
            this.qualityName = qualityName;
            this.url = url;
        }

        public SuperPlayerURL() {
        }
    }
}
