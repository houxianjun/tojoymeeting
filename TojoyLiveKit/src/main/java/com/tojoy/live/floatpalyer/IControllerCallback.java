package com.tojoy.live.floatpalyer;

public interface IControllerCallback {
    void onSwitchPlayMode(int var1);

    void onBackPressed(int var1);

    void onFloatPositionChange(int var1, int var2);

    void onPause();

    void onResume();

    void onSeekTo(int var1);

    void onResumeLive();

    void onDanmuToggle(boolean var1);

    void onSnapshot();

    void onQualityChange(TCVideoQuality var1);

    void onSpeedChange(float var1);

    void onMirrorToggle(boolean var1);

    void onHWAccelerationToggle(boolean var1);
}
