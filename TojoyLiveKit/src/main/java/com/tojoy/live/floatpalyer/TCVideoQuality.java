package com.tojoy.live.floatpalyer;

public class TCVideoQuality {

    public int index;
    public String name;
    public String title;
    public int bitrate;
    public String url;

    public TCVideoQuality() {
    }

    public TCVideoQuality(int index, String title, String url) {
        this.index = index;
        this.title = title;
        this.url = url;
    }
}
