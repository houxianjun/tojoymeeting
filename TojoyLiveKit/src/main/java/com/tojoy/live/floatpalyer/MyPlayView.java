package com.tojoy.live.floatpalyer;

import android.app.Activity;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.tencent.liteav.basic.log.TXCLog;
import com.tencent.rtmp.ITXLivePlayListener;
import com.tencent.rtmp.ITXVodPlayListener;
import com.tencent.rtmp.TXBitrateItem;
import com.tencent.rtmp.TXLivePlayConfig;
import com.tencent.rtmp.TXLivePlayer;
import com.tencent.rtmp.TXVodPlayConfig;
import com.tencent.rtmp.TXVodPlayer;
import com.tencent.rtmp.ui.TXCloudVideoView;
import com.vhall.uilibs.R;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class MyPlayView extends RelativeLayout implements ITXVodPlayListener, ITXLivePlayListener {
    private static final String TAG = "SuperPlayerView";
    public static boolean isFloating;
    private Context mContext;
    private ViewGroup mRootView;
    private TXCloudVideoView mTXCloudVideoView;
    private TCControllerFloat mControllerFloat;
    private LayoutParams mLayoutParamWindowMode;
    private LayoutParams mLayoutParamFullScreenMode;
    private android.widget.RelativeLayout.LayoutParams mVodControllerWindowParams;
    private android.widget.RelativeLayout.LayoutParams mVodControllerFullScreenParams;
    private WindowManager mWindowManager;
    private android.view.WindowManager.LayoutParams mWindowParams;
    private SuperPlayerModel mCurrentModel;
    private IPlayInfoProtocol mCurrentProtocol;
    private TXVodPlayer mVodPlayer;
    private TXVodPlayConfig mVodPlayConfig;
    private TXLivePlayer mLivePlayer;
    private TXLivePlayConfig mLivePlayConfig;
    private OnSuperPlayerViewCallback mPlayerViewCallback;
    private TCNetWatcher mWatcher;
    private String mCurrentPlayVideoURL;
    private int mCurrentPlayType;
    private int mCurrentPlayMode = 1;
    private int mCurrentPlayState = 1;
    private boolean mIsMultiBitrateStream;
    private boolean mIsPlayWithFileId;
    private long mReportLiveStartTime = -1L;
    private long mReportVodStartTime = -1L;
    private boolean mDefaultQualitySet;
    private boolean mLockScreen = false;
    private boolean mChangeHWAcceleration;
    private int mSeekPos;
    private long mMaxLiveProgressTime;
    private MyPlayView.PLAYER_TYPE mCurPlayType;
    private final int OP_SYSTEM_ALERT_WINDOW;
    public IControllerCallback mControllerCallback;

    public MyPlayView(Context context) {
        super(context);
        this.mCurPlayType = MyPlayView.PLAYER_TYPE.PLAYER_TYPE_NULL;
        this.OP_SYSTEM_ALERT_WINDOW = 24;
        this.mControllerCallback = new NamelessClass_1();
        this.initView(context);
    }

    public MyPlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mCurPlayType = MyPlayView.PLAYER_TYPE.PLAYER_TYPE_NULL;
        this.OP_SYSTEM_ALERT_WINDOW = 24;
        this.mControllerCallback = new NamelessClass_1();
        this.initView(context);
    }

    public MyPlayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mCurPlayType = MyPlayView.PLAYER_TYPE.PLAYER_TYPE_NULL;
        this.OP_SYSTEM_ALERT_WINDOW = 24;
        this.mControllerCallback = new NamelessClass_1();
        this.initView(context);
    }

    class NamelessClass_1 implements IControllerCallback {
        NamelessClass_1() {
        }

        @Override
        public void onSwitchPlayMode(int requestPlayMode) {
            if (MyPlayView.this.mCurrentPlayMode != requestPlayMode) {
                if (!MyPlayView.this.mLockScreen) {
                    if (requestPlayMode == 2) {
                        MyPlayView.this.fullScreen(true);
                    } else {
                        MyPlayView.this.fullScreen(false);
                    }

                    MyPlayView.this.mControllerFloat.hide();
                    if (requestPlayMode == 2) {
                        if (MyPlayView.this.mLayoutParamFullScreenMode == null) {
                            return;
                        }

                        MyPlayView.this.setLayoutParams(MyPlayView.this.mLayoutParamFullScreenMode);
                        MyPlayView.this.rotateScreenOrientation(1);
                        if (MyPlayView.this.mPlayerViewCallback != null) {
                            MyPlayView.this.mPlayerViewCallback.onStartFullScreenPlay();
                        }
                    } else {
                        Intent intent;
                        if (requestPlayMode == 1) {
                            if (MyPlayView.this.mCurrentPlayMode == 3) {
                                try {
                                    Context viewContext = MyPlayView.this.getContext();
                                    intent = null;
                                    if (!(viewContext instanceof Activity)) {
                                        return;
                                    }

                                    intent = new Intent(MyPlayView.this.getContext(), viewContext.getClass());
                                    MyPlayView.this.mContext.startActivity(intent);
                                    MyPlayView.this.pause();
                                    if (MyPlayView.this.mLayoutParamWindowMode == null) {
                                        return;
                                    }

                                    MyPlayView.this.mWindowManager.removeView(MyPlayView.this.mControllerFloat);
                                    if (MyPlayView.this.mCurrentPlayType == 1) {
                                        MyPlayView.this.mVodPlayer.setPlayerView(MyPlayView.this.mTXCloudVideoView);
                                    } else {
                                        MyPlayView.this.mLivePlayer.setPlayerView(MyPlayView.this.mTXCloudVideoView);
                                    }

                                    MyPlayView.this.resume();
                                } catch (Exception var6) {
                                    var6.printStackTrace();
                                }
                            } else if (MyPlayView.this.mCurrentPlayMode == 2) {
                                if (MyPlayView.this.mLayoutParamWindowMode == null) {
                                    return;
                                }

                                MyPlayView.this.setLayoutParams(MyPlayView.this.mLayoutParamWindowMode);
                                MyPlayView.this.rotateScreenOrientation(2);
                                if (MyPlayView.this.mPlayerViewCallback != null) {
                                    MyPlayView.this.mPlayerViewCallback.onStopFullScreenPlay();
                                }
                            }
                        } else if (requestPlayMode == 3) {
                            TXCLog.i("SuperPlayerView", "requestPlayMode Float :" + Build.MANUFACTURER);
                            SuperPlayerGlobalConfig prefs = SuperPlayerGlobalConfig.getInstance();
                            if (!prefs.enableFloatWindow) {
                                return;
                            }

                            if (Build.VERSION.SDK_INT >= 23) {
                                if (!Settings.canDrawOverlays(MyPlayView.this.mContext)) {
                                    intent = new Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION");
                                    intent.setData(Uri.parse("package:" + MyPlayView.this.mContext.getPackageName()));
                                    MyPlayView.this.mContext.startActivity(intent);
                                    return;
                                }
                            } else if (!MyPlayView.this.checkOp(MyPlayView.this.mContext, 24)) {
                                return;
                            }

                            MyPlayView.this.pause();
                            MyPlayView.this.mWindowManager = (WindowManager) MyPlayView.this.mContext.getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
                            MyPlayView.this.mWindowParams = new android.view.WindowManager.LayoutParams();

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                // 8.0新特性
                                MyPlayView.this.mWindowParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
                            } else {
                                MyPlayView.this.mWindowParams.type = WindowManager.LayoutParams.TYPE_PHONE;
                            }

                            MyPlayView.this.mWindowParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                                    | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                                    | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
//                            MyPlayView.this.mWindowParams.flags = 40;
                            MyPlayView.this.mWindowParams.format = -3;
                            MyPlayView.this.mWindowParams.gravity = 51;
                            SuperPlayerGlobalConfig.TXRect rect = prefs.floatViewRect;
                            MyPlayView.this.mWindowParams.x = rect.x;
                            MyPlayView.this.mWindowParams.y = rect.y;
                            MyPlayView.this.mWindowParams.width = rect.width;
                            MyPlayView.this.mWindowParams.height = rect.height;

                            try {
                                MyPlayView.this.mWindowManager.addView(MyPlayView.this.mControllerFloat, MyPlayView.this.mWindowParams);
                            } catch (Exception var5) {
                                return;
                            }

                            TXCloudVideoView videoView = MyPlayView.this.mControllerFloat.getFloatVideoView();
                            if (videoView != null) {
                                if (MyPlayView.this.mCurrentPlayType == 1) {
                                    MyPlayView.this.mVodPlayer.setPlayerView(videoView);
                                } else {
                                    MyPlayView.this.mLivePlayer.setPlayerView(videoView);
                                }

                                MyPlayView.this.resume();
                            }

                            TCLogReport.getInstance().uploadLogs("floatmode", 0L, 0);
                        }
                    }

                    MyPlayView.this.mCurrentPlayMode = requestPlayMode;
                }
            }
        }

        @Override
        public void onBackPressed(int playMode) {
            switch (playMode) {
                case 1:
                    if (MyPlayView.this.mPlayerViewCallback != null) {
                        MyPlayView.this.mPlayerViewCallback.onClickSmallReturnBtn();
                    }

                    if (MyPlayView.this.mCurrentPlayState == 1) {
                        this.onSwitchPlayMode(3);
                    }
                    break;
                case 2:
                    this.onSwitchPlayMode(1);
                    break;
                case 3:
                    try {
                        MyPlayView.this.mWindowManager.removeView(MyPlayView.this.mControllerFloat);
                        if (MyPlayView.this.mPlayerViewCallback != null) {
                            MyPlayView.this.mPlayerViewCallback.onClickFloatCloseBtn();
                        }
                    } catch (Exception e) {

                    }
                    default:
                        break;
            }

        }

        @Override
        public void onFloatPositionChange(int x, int y) {
            MyPlayView.this.mWindowParams.x = x;
            MyPlayView.this.mWindowParams.y = y;
            MyPlayView.this.mWindowManager.updateViewLayout(MyPlayView.this.mControllerFloat, MyPlayView.this.mWindowParams);
        }

        @Override
        public void onPause() {
            if (MyPlayView.this.mCurrentPlayType == 1) {
                if (MyPlayView.this.mVodPlayer != null) {
                    MyPlayView.this.mVodPlayer.pause();
                }
            } else {
                if (MyPlayView.this.mLivePlayer != null) {
                    MyPlayView.this.mLivePlayer.pause();
                }

                if (MyPlayView.this.mWatcher != null) {
                    MyPlayView.this.mWatcher.stop();
                }
            }

            MyPlayView.this.updatePlayState(2);
        }

        @Override
        public void onResume() {
            if (MyPlayView.this.mCurrentPlayState == 4) {
                if (MyPlayView.this.mCurPlayType == MyPlayView.PLAYER_TYPE.PLAYER_TYPE_LIVE) {
                    if (TCUrlUtil.isRTMPPlay(MyPlayView.this.mCurrentPlayVideoURL)) {
                        MyPlayView.this.playLiveURL(MyPlayView.this.mCurrentPlayVideoURL, 0);
                    } else if (TCUrlUtil.isFLVPlay(MyPlayView.this.mCurrentPlayVideoURL)) {
                        MyPlayView.this.playTimeShiftLiveURL(MyPlayView.this.mCurrentModel);
                        if (MyPlayView.this.mCurrentModel.multiURLs != null && !MyPlayView.this.mCurrentModel.multiURLs.isEmpty()) {
                            MyPlayView.this.startMultiStreamLiveURL(MyPlayView.this.mCurrentPlayVideoURL);
                        }
                    }
                } else {
                    MyPlayView.this.playVodURL(MyPlayView.this.mCurrentPlayVideoURL);
                }
            } else if (MyPlayView.this.mCurrentPlayState == 2) {
                if (MyPlayView.this.mCurrentPlayType == 1) {
                    if (MyPlayView.this.mVodPlayer != null) {
                        MyPlayView.this.mVodPlayer.resume();
                    }
                } else if (MyPlayView.this.mLivePlayer != null) {
                    MyPlayView.this.mLivePlayer.resume();
                }
            }

            MyPlayView.this.updatePlayState(1);
        }

        @Override
        public void onSeekTo(int position) {
            if (MyPlayView.this.mCurrentPlayType == 1) {
                if (MyPlayView.this.mVodPlayer != null) {
                    MyPlayView.this.mVodPlayer.seek(position);
                }
            } else {
                MyPlayView.this.updatePlayType(3);
                TCLogReport.getInstance().uploadLogs("timeshift", 0L, 0);
                if (MyPlayView.this.mLivePlayer != null) {
                    MyPlayView.this.mLivePlayer.seek(position);
                }

                if (MyPlayView.this.mWatcher != null) {
                    MyPlayView.this.mWatcher.stop();
                }
            }

        }

        @Override
        public void onResumeLive() {
            if (MyPlayView.this.mLivePlayer != null) {
                MyPlayView.this.mLivePlayer.resumeLive();
            }

            MyPlayView.this.updatePlayType(2);
        }

        @Override
        public void onDanmuToggle(boolean isOpen) {

        }

        @Override
        public void onSnapshot() {
            if (MyPlayView.this.mCurrentPlayType == 1) {
                if (MyPlayView.this.mVodPlayer != null) {
                    MyPlayView.this.mVodPlayer.snapshot(new TXLivePlayer.ITXSnapshotListener() {
                        @Override
                        public void onSnapshot(Bitmap bmp) {
                        }
                    });
                }
            } else if (MyPlayView.this.mCurrentPlayType == 3) {
            } else if (MyPlayView.this.mLivePlayer != null) {
                MyPlayView.this.mLivePlayer.snapshot(new TXLivePlayer.ITXSnapshotListener() {
                    @Override
                    public void onSnapshot(Bitmap bmp) {
                    }
                });
            }

        }

        @Override
        public void onQualityChange(TCVideoQuality quality) {
            if (MyPlayView.this.mCurrentPlayType == 1) {
                if (MyPlayView.this.mVodPlayer != null) {
                    if (quality.index == -1) {
                        float currentTime = MyPlayView.this.mVodPlayer.getCurrentPlaybackTime();
                        MyPlayView.this.mVodPlayer.stopPlay(true);
                        TXCLog.i("SuperPlayerView", "onQualitySelect quality.url:" + quality.url);
                        MyPlayView.this.mVodPlayer.setStartTime(currentTime);
                        MyPlayView.this.mVodPlayer.startPlay(quality.url);
                    } else {
                        TXCLog.i("SuperPlayerView", "setBitrateIndex quality.index:" + quality.index);
                        MyPlayView.this.mVodPlayer.setBitrateIndex(quality.index);
                    }
                }
            } else if (MyPlayView.this.mLivePlayer != null && !TextUtils.isEmpty(quality.url)) {
                int result = MyPlayView.this.mLivePlayer.switchStream(quality.url);
            }

            TCLogReport.getInstance().uploadLogs("change_resolution", 0L, 0);
        }

        @Override
        public void onSpeedChange(float speedLevel) {
            if (MyPlayView.this.mVodPlayer != null) {
                MyPlayView.this.mVodPlayer.setRate(speedLevel);
            }

            TCLogReport.getInstance().uploadLogs("change_speed", 0L, 0);
        }

        @Override
        public void onMirrorToggle(boolean isMirror) {
            if (MyPlayView.this.mVodPlayer != null) {
                MyPlayView.this.mVodPlayer.setMirror(isMirror);
            }

            if (isMirror) {
                TCLogReport.getInstance().uploadLogs("mirror", 0L, 0);
            }

        }

        @Override
        public void onHWAccelerationToggle(boolean isAccelerate) {
            if (MyPlayView.this.mCurrentPlayType == 1) {
                MyPlayView.this.mChangeHWAcceleration = true;
                if (MyPlayView.this.mVodPlayer != null) {
                    MyPlayView.this.mVodPlayer.enableHardwareDecode(isAccelerate);
                    MyPlayView.this.mSeekPos = (int) MyPlayView.this.mVodPlayer.getCurrentPlaybackTime();
                    TXCLog.i("SuperPlayerView", "save pos:" + MyPlayView.this.mSeekPos);
                    MyPlayView.this.stopPlay();
                    MyPlayView.this.playModeVideo(MyPlayView.this.mCurrentProtocol);
                }
            } else if (MyPlayView.this.mLivePlayer != null) {
                MyPlayView.this.mLivePlayer.enableHardwareDecode(isAccelerate);
                MyPlayView.this.playWithModel(MyPlayView.this.mCurrentModel);
            }

            if (isAccelerate) {
                TCLogReport.getInstance().uploadLogs("hw_decode", 0L, 0);
            } else {
                TCLogReport.getInstance().uploadLogs("soft_decode", 0L, 0);
            }

        }
    }

    private void initView(Context context) {
        this.mContext = context;
        this.mRootView = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.super_vod_player_view, (ViewGroup) null);
        this.mTXCloudVideoView = (TXCloudVideoView) this.mRootView.findViewById(R.id.cloud_video_view);
        this.mControllerFloat = (TCControllerFloat) this.mRootView.findViewById(R.id.controller_float);
        this.mVodControllerWindowParams = new android.widget.RelativeLayout.LayoutParams(-1, -1);
        this.mVodControllerFullScreenParams = new android.widget.RelativeLayout.LayoutParams(-1, -1);
        this.mControllerFloat.setCallback(this.mControllerCallback);
        this.removeAllViews();
        this.mRootView.removeView(this.mTXCloudVideoView);
        this.mRootView.removeView(this.mControllerFloat);
        this.addView(this.mTXCloudVideoView);
        this.post(new Runnable() {
            @Override
            public void run() {
                if (MyPlayView.this.mCurrentPlayMode == 1) {
                    MyPlayView.this.mLayoutParamWindowMode = (LayoutParams) MyPlayView.this.getLayoutParams();
                }

                try {
                    Class parentLayoutParamClazz = MyPlayView.this.getLayoutParams().getClass();
                    Constructor constructor = parentLayoutParamClazz.getDeclaredConstructor(Integer.TYPE, Integer.TYPE);
                    MyPlayView.this.mLayoutParamFullScreenMode = (LayoutParams) constructor.newInstance(-1, -1);
                } catch (Exception var3) {
                    var3.printStackTrace();
                }

            }
        });
        TCLogReport.getInstance().setAppName(context);
        TCLogReport.getInstance().setPackageName(context);
    }

    private void initVodPlayer(Context context) {
        if (this.mVodPlayer == null) {
            this.mVodPlayer = new TXVodPlayer(context);
            SuperPlayerGlobalConfig config = SuperPlayerGlobalConfig.getInstance();
            this.mVodPlayConfig = new TXVodPlayConfig();
            File sdcardDir = context.getExternalFilesDir((String) null);
            if (sdcardDir != null) {
                this.mVodPlayConfig.setCacheFolderPath(sdcardDir.getPath() + "/txcache");
            }

            this.mVodPlayConfig.setMaxCacheItems(config.maxCacheItem);
            this.mVodPlayer.setConfig(this.mVodPlayConfig);
            this.mVodPlayer.setRenderMode(config.renderMode);
            this.mVodPlayer.setVodListener(this);
            this.mVodPlayer.enableHardwareDecode(config.enableHWAcceleration);
        }
    }

    private void initLivePlayer(Context context) {
        if (this.mLivePlayer == null) {
            this.mLivePlayer = new TXLivePlayer(context);
            SuperPlayerGlobalConfig config = SuperPlayerGlobalConfig.getInstance();
            this.mLivePlayConfig = new TXLivePlayConfig();
            this.mLivePlayer.setConfig(this.mLivePlayConfig);
            this.mLivePlayer.setRenderMode(config.renderMode);
            this.mLivePlayer.setRenderRotation(0);
            this.mLivePlayer.setPlayListener(this);
            this.mLivePlayer.enableHardwareDecode(config.enableHWAcceleration);
        }
    }

    public void playWithModel(final SuperPlayerModel model) {
        this.mCurrentModel = model;
        this.stopPlay();
        this.initLivePlayer(this.getContext());
        this.initVodPlayer(this.getContext());
        TCPlayInfoParams params = new TCPlayInfoParams();
        params.appId = model.appId;
        if (model.videoId != null) {
            params.fileId = model.videoId.fileId;
            params.timeout = model.videoId.timeout;
            params.us = model.videoId.us;
            params.exper = model.videoId.exper;
            params.sign = model.videoId.sign;
        }

        this.mCurrentProtocol = new TCPlayInfoProtocolV2(params);
        if (model.videoId != null) {
            this.mCurrentProtocol.sendRequest(new IPlayInfoRequestCallback() {
                @Override
                public void onSuccess(IPlayInfoProtocol protocol, TCPlayInfoParams param) {
                    TXCLog.i("SuperPlayerView", "onSuccess: protocol params = " + param.toString());
                    MyPlayView.this.mReportVodStartTime = System.currentTimeMillis();
                    MyPlayView.this.mVodPlayer.setPlayerView(MyPlayView.this.mTXCloudVideoView);
                    MyPlayView.this.playModeVideo(MyPlayView.this.mCurrentProtocol);
                    MyPlayView.this.updatePlayType(1);
                    String title = !TextUtils.isEmpty(model.title) ? model.title : (MyPlayView.this.mCurrentProtocol.getName() != null && !TextUtils.isEmpty(MyPlayView.this.mCurrentProtocol.getName()) ? MyPlayView.this.mCurrentProtocol.getName() : "");
                    MyPlayView.this.updateTitle(title);
                    MyPlayView.this.updateVideoProgress(0L, 0L);
                }

                @Override
                public void onError(int errCode, String message) {
                    TXCLog.i("SuperPlayerView", "onFail: errorCode = " + errCode + " message = " + message);
                }
            });
        } else {
            String videoURL = null;
            List<TCVideoQuality> videoQualities = new ArrayList();
            TCVideoQuality defaultVideoQuality = null;
            if (model.multiURLs != null && !model.multiURLs.isEmpty()) {
                int i = 0;

                SuperPlayerModel.SuperPlayerURL superPlayerURL;
                for (Iterator var7 = model.multiURLs.iterator(); var7.hasNext(); videoQualities.add(new TCVideoQuality(i++, superPlayerURL.qualityName, superPlayerURL.url))) {
                    superPlayerURL = (SuperPlayerModel.SuperPlayerURL) var7.next();
                    if (i == model.playDefaultIndex) {
                        videoURL = superPlayerURL.url;
                    }
                }

                defaultVideoQuality = (TCVideoQuality) videoQualities.get(model.playDefaultIndex);
            } else if (!TextUtils.isEmpty(model.url)) {
                videoQualities.add(new TCVideoQuality(0, model.qualityName, model.url));
                defaultVideoQuality = (TCVideoQuality) videoQualities.get(0);
                videoURL = model.url;
            }

            if (TextUtils.isEmpty(videoURL)) {
                Toast.makeText(this.getContext(), "播放视频失败，播放连接为空", Toast.LENGTH_SHORT).show();
                return;
            }

            if (TCUrlUtil.isRTMPPlay(videoURL)) {
                this.mReportLiveStartTime = System.currentTimeMillis();
                this.mLivePlayer.setPlayerView(this.mTXCloudVideoView);
                this.playLiveURL(videoURL, 0);
            } else if (TCUrlUtil.isFLVPlay(videoURL)) {
                this.mReportLiveStartTime = System.currentTimeMillis();
                this.mLivePlayer.setPlayerView(this.mTXCloudVideoView);
                this.playTimeShiftLiveURL(model);
                if (model.multiURLs != null && !model.multiURLs.isEmpty()) {
                    this.startMultiStreamLiveURL(videoURL);
                }
            } else {
                this.mReportVodStartTime = System.currentTimeMillis();
                this.mVodPlayer.setPlayerView(this.mTXCloudVideoView);
                this.playVodURL(videoURL);
            }

            boolean isLivePlay = TCUrlUtil.isRTMPPlay(videoURL) || TCUrlUtil.isFLVPlay(videoURL);
            this.updatePlayType(isLivePlay ? 2 : 1);
            this.updateTitle(model.title);
            this.updateVideoProgress(0L, 0L);
        }

    }

    private void playModeVideo(IPlayInfoProtocol protocol) {
        this.playVodURL(protocol.getUrl());
    }

    private void playLiveURL(String url, int playType) {
        this.mCurrentPlayVideoURL = url;
        if (this.mLivePlayer != null) {
            this.mLivePlayer.setPlayListener(this);
            int result = this.mLivePlayer.startPlay(url, playType);
            if (result != 0) {
                TXCLog.e("SuperPlayerView", "playLiveURL videoURL:" + url + ",result:" + result);
            } else {
                this.mCurrentPlayState = 1;
                this.mCurPlayType = MyPlayView.PLAYER_TYPE.PLAYER_TYPE_LIVE;
                TXCLog.e("SuperPlayerView", "playLiveURL mCurrentPlayState:" + this.mCurrentPlayState);
            }
        }

    }

    private void playVodURL(String url) {
        if (url != null && !"".equals(url)) {
            this.mCurrentPlayVideoURL = url;
            if (url.contains(".m3u8")) {
                this.mIsMultiBitrateStream = true;
            }

            if (this.mVodPlayer != null) {
                this.mDefaultQualitySet = false;
                this.mVodPlayer.setAutoPlay(true);
                this.mVodPlayer.setVodListener(this);
                int ret = this.mVodPlayer.startPlay(url);
                if (ret == 0) {
                    this.mCurrentPlayState = 1;
                    this.mCurPlayType = MyPlayView.PLAYER_TYPE.PLAYER_TYPE_VOD;
                    TXCLog.e("SuperPlayerView", "playVodURL mCurrentPlayState:" + this.mCurrentPlayState);
                }
            }

            this.mIsPlayWithFileId = false;
        }
    }

    private void playTimeShiftLiveURL(SuperPlayerModel model) {
        String liveURL = model.url;
        String bizid = liveURL.substring(liveURL.indexOf("//") + 2, liveURL.indexOf("."));
        String domian = SuperPlayerGlobalConfig.getInstance().playShiftDomain;
        String streamid = liveURL.substring(liveURL.lastIndexOf("/") + 1, liveURL.lastIndexOf("."));
        int appid = model.appId;
        TXCLog.i("SuperPlayerView", "bizid:" + bizid + ",streamid:" + streamid + ",appid:" + appid);
        this.playLiveURL(liveURL, 1);

        try {
            int bizidNum = Integer.valueOf(bizid);
            this.mLivePlayer.prepareLiveSeek(domian, bizidNum);
        } catch (NumberFormatException var8) {
            var8.printStackTrace();
            TXCLog.e("SuperPlayerView", "playTimeShiftLiveURL: bizidNum 错误 = %s " + bizid);
        }

    }

    private void startMultiStreamLiveURL(String url) {
        this.mLivePlayConfig.setAutoAdjustCacheTime(false);
        this.mLivePlayConfig.setMaxAutoAdjustCacheTime(5.0F);
        this.mLivePlayConfig.setMinAutoAdjustCacheTime(5.0F);
        this.mLivePlayer.setConfig(this.mLivePlayConfig);
        if (this.mWatcher == null) {
            this.mWatcher = new TCNetWatcher(this.mContext);
        }

        this.mWatcher.start(url, this.mLivePlayer);
    }

    private void updateTitle(String title) {
    }

    private void updateVideoProgress(long current, long duration) {
    }

    private void updatePlayType(int playType) {
        this.mCurrentPlayType = playType;
    }

    private void updatePlayState(int playState) {
        this.mCurrentPlayState = playState;
    }

    public void onResume() {

        this.resume();
    }

    private void resume() {
        if (this.mCurrentPlayType == 1 && this.mVodPlayer != null) {
            this.mVodPlayer.resume();
        }

    }

    public void onPause() {
        this.pause();
    }

    private void pause() {
        if (this.mCurrentPlayType == 1 && this.mVodPlayer != null) {
            this.mVodPlayer.pause();
        }

    }

    public void resetPlayer() {
        this.stopPlay();
    }

    private void stopPlay() {
        if (this.mVodPlayer != null) {
            this.mVodPlayer.setVodListener((ITXVodPlayListener) null);
            this.mVodPlayer.stopPlay(false);
        }

        if (this.mLivePlayer != null) {
            this.mLivePlayer.setPlayListener((ITXLivePlayListener) null);
            this.mLivePlayer.stopPlay(false);
            this.mTXCloudVideoView.removeVideoView();
        }

        if (this.mWatcher != null) {
            this.mWatcher.stop();
        }

        this.mCurrentPlayState = 2;
        TXCLog.e("SuperPlayerView", "stopPlay mCurrentPlayState:" + this.mCurrentPlayState);
        this.reportPlayTime();
    }

    private void reportPlayTime() {
        long reportEndTime;
        long diff;
        if (this.mReportLiveStartTime != -1L) {
            reportEndTime = System.currentTimeMillis();
            diff = (reportEndTime - this.mReportLiveStartTime) / 1000L;
            TCLogReport.getInstance().uploadLogs("superlive", diff, 0);
            this.mReportLiveStartTime = -1L;
        }

        if (this.mReportVodStartTime != -1L) {
            reportEndTime = System.currentTimeMillis();
            diff = (reportEndTime - this.mReportVodStartTime) / 1000L;
            TCLogReport.getInstance().uploadLogs("supervod", diff, this.mIsPlayWithFileId ? 1 : 0);
            this.mReportVodStartTime = -1L;
        }

    }

    public void setPlayerViewCallback(OnSuperPlayerViewCallback callback) {
        this.mPlayerViewCallback = callback;
    }

    private void fullScreen(boolean isFull) {
        if (this.getContext() instanceof Activity) {
            Activity activity = (Activity) this.getContext();
            View decorView;
            if (isFull) {
                decorView = activity.getWindow().getDecorView();
                if (decorView == null) {
                    return;
                }

                if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
                    decorView.setSystemUiVisibility(8);
                } else if (Build.VERSION.SDK_INT >= 19) {
                    int uiOptions = 4102;
                    decorView.setSystemUiVisibility(uiOptions);
                }
            } else {
                decorView = activity.getWindow().getDecorView();
                if (decorView == null) {
                    return;
                }

                if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
                    decorView.setSystemUiVisibility(0);
                } else if (Build.VERSION.SDK_INT >= 19) {
                    decorView.setSystemUiVisibility(0);
                }
            }
        }

    }


    private void rotateScreenOrientation(int orientation) {
        switch (orientation) {
            case 1:
//                ((Activity) this.mContext).setRequestedOrientation(0);
                break;
            case 2:
//                ((Activity) this.mContext).setRequestedOrientation(1);
        }

    }

    public void onPlayEvent(TXVodPlayer player, int event, Bundle param) {
        if (event != 2005) {
            String playEventLog = "TXVodPlayer onPlayEvent event: " + event + ", " + param.getString("EVT_MSG");
            TXCLog.d("SuperPlayerView", playEventLog);
        }

        switch (event) {
            case 2003:
                if (this.mChangeHWAcceleration) {
                    TXCLog.i("SuperPlayerView", "seek pos:" + this.mSeekPos);
                    this.mControllerCallback.onSeekTo(this.mSeekPos);
                    this.mChangeHWAcceleration = false;
                }
                break;
            case 2004:
                this.updatePlayState(1);
                break;
            case 2005:
                int progress = param.getInt("EVT_PLAY_PROGRESS_MS");
                int duration = param.getInt("EVT_PLAY_DURATION_MS");
                this.updateVideoProgress((long) (progress / 1000), (long) (duration / 1000));
                break;
            case 2006:
                this.updatePlayState(4);
            case 2007:
            case 2008:
            case 2009:
            case 2010:
            case 2011:
            case 2012:
            default:
                break;
            case 2013:
                this.updatePlayState(1);
                if (this.mIsMultiBitrateStream) {
                    List<TXBitrateItem> bitrateItems = this.mVodPlayer.getSupportedBitrates();
                    if (bitrateItems == null || bitrateItems.size() == 0) {
                        return;
                    }

                    Collections.sort(bitrateItems);
                    List<TCVideoQuality> videoQualities = new ArrayList();
                    int size = bitrateItems.size();

                    TXBitrateItem bitrateItem;
                    TCVideoQuality defaultVideoQuality;
                    for (int i = 0; i < size; ++i) {
                        bitrateItem = (TXBitrateItem) bitrateItems.get(i);
                        defaultVideoQuality = TCVideoQualityUtil.convertToVideoQuality(bitrateItem, i);
                        videoQualities.add(defaultVideoQuality);
                    }

                    if (!this.mDefaultQualitySet) {
                        TXBitrateItem defaultItem = (TXBitrateItem) bitrateItems.get(bitrateItems.size() - 1);
                        this.mVodPlayer.setBitrateIndex(defaultItem.index);
                        bitrateItem = (TXBitrateItem) bitrateItems.get(bitrateItems.size() - 1);
                        defaultVideoQuality = TCVideoQualityUtil.convertToVideoQuality(bitrateItem, bitrateItems.size() - 1);
                        this.mDefaultQualitySet = true;
                    }
                }
                break;
        }

        if (event < 0) {
            this.mVodPlayer.stopPlay(true);
            this.updatePlayState(2);
            Toast.makeText(this.mContext, param.getString("EVT_MSG"), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onNetStatus(TXVodPlayer player, Bundle status) {
    }

    @Override
    public void onPlayEvent(int event, Bundle param) {
        if (event != 2005) {
            String playEventLog = "TXLivePlayer onPlayEvent event: " + event + ", " + param.getString("EVT_MSG");
            TXCLog.d("SuperPlayerView", playEventLog);
        }

        switch (event) {
            case -2307:
                Toast.makeText(this.mContext, "清晰度切换失败", Toast.LENGTH_SHORT).show();
                break;
            case -2301:
            case 2006:
                if (this.mCurrentPlayType == 3) {
                    this.mControllerCallback.onResumeLive();
                    Toast.makeText(this.mContext, "时移失败,返回直播", Toast.LENGTH_SHORT).show();
                    this.updatePlayState(1);
                } else {
                    this.stopPlay();
                    this.updatePlayState(4);
                    if (event == -2301) {
                        Toast.makeText(this.mContext, "网络不给力,点击重试", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this.mContext, param.getString("EVT_MSG"), Toast.LENGTH_SHORT).show();
                    }
                }
            case 2003:
            default:
                break;
            case 2004:
                this.updatePlayState(1);
                if (this.mWatcher != null) {
                    this.mWatcher.exitLoading();
                }
                break;
            case 2005:
                int progress = param.getInt("EVT_PLAY_PROGRESS_MS");
                this.mMaxLiveProgressTime = (long) progress > this.mMaxLiveProgressTime ? (long) progress : this.mMaxLiveProgressTime;
                this.updateVideoProgress((long) (progress / 1000), this.mMaxLiveProgressTime / 1000L);
                break;
            case 2007:
            case 2103:
                this.updatePlayState(3);
                if (this.mWatcher != null) {
                    this.mWatcher.enterLoading();
                }
                break;
            case 2013:
                this.updatePlayState(1);
                break;
            case 2015:
                Toast.makeText(this.mContext, "清晰度切换成功", Toast.LENGTH_SHORT).show();
                break;
        }

    }

    @Override
    public void onNetStatus(Bundle status) {
    }

    public void requestPlayMode(int playMode) {
        if (playMode == 1) {
            if (this.mControllerCallback != null) {
                this.mControllerCallback.onSwitchPlayMode(1);
            }
        } else if (playMode == 3) {
            if (this.mPlayerViewCallback != null) {
                this.mPlayerViewCallback.onStartFloatWindowPlay();
            }

            if (this.mControllerCallback != null) {
                this.mControllerCallback.onSwitchPlayMode(3);
            }
        }

    }

    private boolean checkOp(Context context, int op) {
        if (Build.VERSION.SDK_INT >= 19) {
            AppOpsManager manager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);

            try {
                Method method = AppOpsManager.class.getDeclaredMethod("checkOp", Integer.TYPE, Integer.TYPE, String.class);
                return 0 == (Integer) method.invoke(manager, op, Binder.getCallingUid(), context.getPackageName());
            } catch (Exception var5) {
                TXCLog.e("SuperPlayerView", Log.getStackTraceString(var5));
            }
        }

        return true;
    }

    public int getPlayMode() {
        return this.mCurrentPlayMode;
    }

    public int getPlayState() {
        return this.mCurrentPlayState;
    }

    public void release() {
        if (this.mControllerFloat != null) {
            this.mControllerFloat.release();
        }

    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();

        try {
            this.release();
        } catch (Exception var2) {
            TXCLog.e("SuperPlayerView", Log.getStackTraceString(var2));
        } catch (Error var3) {
            TXCLog.e("SuperPlayerView", Log.getStackTraceString(var3));
        }

    }

    public interface OnSuperPlayerViewCallback {
        void onStartFullScreenPlay();

        void onStopFullScreenPlay();

        void onClickFloatCloseBtn();

        void onClickSmallReturnBtn();

        void onStartFloatWindowPlay();
    }

    private static enum PLAYER_TYPE {
        PLAYER_TYPE_NULL,
        PLAYER_TYPE_VOD,
        PLAYER_TYPE_LIVE;

        private PLAYER_TYPE() {
        }
    }
}
