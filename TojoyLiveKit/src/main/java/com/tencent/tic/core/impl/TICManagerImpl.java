package com.tencent.tic.core.impl;

import android.content.Context;

import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMGroupAddOpt;
import com.tencent.imsdk.TIMGroupManager;
import com.tencent.imsdk.TIMLogLevel;
import com.tencent.imsdk.TIMManager;
import com.tencent.imsdk.TIMSdkConfig;
import com.tencent.imsdk.TIMValueCallBack;
import com.tencent.tic.core.TICClassroomOption;
import com.tencent.tic.core.TICManager;
import com.tencent.tic.core.impl.utils.CallbackUtil;
import com.tencent.liteav.basic.log.TXCLog;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.EnvironmentInstance;

public class TICManagerImpl  extends TICManager{

    private final static String TAG = "TICManager";
    private final static String COMPAT_SAAS_CHAT = "_chat";
    int mDisableModule = TICDisableModule.TIC_DISABLE_MODULE_NONE;


    private final static byte[] SYNC = new byte[1];

    private TICClassroomOption classroomOption;
/////////////////////////////////////////////////////////////////////////////////
//
//                      （一）初始和终止接口函数
//
/////////////////////////////////////////////////////////////////////////////////
    private static volatile TICManager instance;
    public static TICManager sharedInstance() {
        if (instance == null) {
            synchronized (SYNC) {
                if (instance == null) {
                    instance = new TICManagerImpl();
                }
            }
        }
        return instance;
    }

    private TICManagerImpl() {
        TXCLog.i(TAG, "TICManager: constructor ");
    }

    @Override
    public int init(Context context, int appId) {
        int result = init(context, appId, mDisableModule);
        return result;
    }


    @Override
    public int init(Context context, int appId, int disableModule) {

        TXCLog.i(TAG, "TICManager: init, context:" + context + " appid:" + appId);
        TICReporter.updateAppId(appId);
        TICReporter.report(TICReporter.EventId.initSdk_start);

        //1、 TIM SDK初始化
        TIMSdkConfig timSdkConfig = new TIMSdkConfig(appId)
                .enableLogPrint(true)
                //TODO::在正式发布时，设置TIMLogLevel.OFF
                .setLogLevel(TIMLogLevel.OFF);
        TIMManager.getInstance().init(context, timSdkConfig);

        TICReporter.report(TICReporter.EventId.initSdk_end);
        return 0;
    }

    /////////////////////////////////////////////////////////////////////////////////
    //
    //                      （二）TIC登录/登出/创建销毁课堂/进入退出课堂接口函数
    //
    /////////////////////////////////////////////////////////////////////////////////
    @Override
    public void login(final String userId, final String userSig, final TICCallback callBack) {

        TXCLog.i(TAG, "TICManager: login userid:" + userId + " sig:" + userSig);
        TICReporter.updateRoomId(0);
        TICReporter.updateUserId(userId);
        TICReporter.report(TICReporter.EventId.login_start);

        TIMManager.getInstance().login(userId, userSig, new TIMCallBack() {
            @Override
            public void onSuccess() {
                TXCLog.i(TAG, "TICManager: login onSuccess:" + userId);
                TICReporter.report(TICReporter.EventId.login_end);
                if (null != callBack) {
                    callBack.onSuccess("");
                }
            }

            @Override
            public void onError(int errCode, String errMsg) {
                TXCLog.i(TAG, "TICManager: login onError:" + errCode + " msg:"  +errMsg);
                TICReporter.report(TICReporter.EventId.login_end, errCode, errMsg);
                if (null != callBack) {
                    callBack.onError(MODULE_IMSDK, errCode, "login failed: " + errMsg);
                }
            }
        });
    }

    @Override
    public void logout(final TICCallback callback) {
        TXCLog.i(TAG, "TICManager: logout callback:" + callback);
        TICReporter.report(TICReporter.EventId.logout_start);
        TIMManager.getInstance().logout(new TIMCallBack() {
            @Override
            public void onSuccess() {
                TXCLog.i(TAG, "TICManager: logout onSuccess");
                TICReporter.report(TICReporter.EventId.logout_end);
                if (null != callback) {
                    callback.onSuccess("");
                }
            }
            @Override
            public void onError(int errCode, String errMsg) {
                TXCLog.i(TAG, "TICManager: logout onError:" + errCode + " msg:" + errMsg);
                TICReporter.report(TICReporter.EventId.logout_end, errCode, errMsg);
                if (null != callback) {
                    callback.onError(MODULE_IMSDK, errCode, "logout failed: " + errMsg);
                }
            }
        });
    }

    @Override
    public void createClassroom(final int classId, final int scene, final TICCallback callback) {
        TXCLog.i(TAG, "TICManager: createClassroom classId:" + classId + " scene:" + scene + " callback:" + callback);
        TICReporter.report(TICReporter.EventId.createGroup_start);
        // 为了减少用户操作成本（收到群进出等通知需要配置工单才生效）群组类型由ChatRoom改为Public
        final String groupId = String.valueOf(classId);
        final String groupName = "interact group";
        // 因云洽会app非正式环境没有购买聊天室，所以无论课堂场景是什么均设置为公开群；正式环境还是原有逻辑不变
        final String groupType = (AppConfig.environmentInstance == EnvironmentInstance.FORMAL) ? (((scene == TICClassScene.TIC_CLASS_SCENE_LIVE) ? "AVChatRoom": "Public")) : "Public" ;
//        final String groupType = ((scene == TICClassScene.TIC_CLASS_SCENE_LIVE) ? "AVChatRoom": "Public");

        TIMGroupManager.CreateGroupParam param = new TIMGroupManager.CreateGroupParam(groupType, groupName);
        param.setGroupId(groupId);
        param.setAddOption(TIMGroupAddOpt.TIM_GROUP_ADD_ANY);
        TIMGroupManager.getInstance().createGroup(param, new TIMValueCallBack<String>() {

            @Override
            public void onSuccess(String s) {
                TXCLog.i(TAG, "TICManager: createClassroom onSuccess:" + classId + " msg:" + s);
                TICReporter.report(TICReporter.EventId.createGroup_end);
                if (null != callback) {
                    callback.onSuccess(classId);
                }
            }

            @Override
            public void onError(int errCode, String errMsg) {
                if (null != callback) {
                    if (errCode == 10025) {
                        // 群组ID已被使用，并且操作者为群主，可以直接使用。
                        TXCLog.i(TAG, "TICManager: createClassroom 10025 onSuccess:" + classId);
                        callback.onSuccess(classId);
                    } else {
                        TXCLog.i(TAG, "TICManager: createClassroom onError:" + errCode + " msg:" + errMsg);
                        TICReporter.report(TICReporter.EventId.createGroup_end, errCode, errMsg);
                        callback.onError(MODULE_IMSDK, errCode,  errMsg);
                    }
                }
            }
        });
    }

    @Override
    public void destroyClassroom(final int classId, final TICCallback callback) {

        TXCLog.i(TAG, "TICManager: destroyClassroom classId:" + classId + " callback:" + callback);
        TICReporter.report(TICReporter.EventId.deleteGroup_start);
        final String groupId = String.valueOf(classId);

        TIMGroupManager.getInstance().deleteGroup(groupId, new TIMCallBack() {
            @Override
            public void onError(int errorCode, String errInfo) {
                TXCLog.i(TAG, "TICManager: destroyClassroom onError:" + errorCode + " msg:" + errInfo);
                TICReporter.report(TICReporter.EventId.deleteGroup_end, errorCode, errInfo);
                CallbackUtil.notifyError(callback, MODULE_IMSDK, errorCode, errInfo);
            }

            @Override
            public void onSuccess() {
                TICReporter.report(TICReporter.EventId.deleteGroup_end);
                TXCLog.i(TAG, "TICManager: destroyClassroom onSuccess" );

            }
        });

    }

    @Override
    public void joinClassroom(final TICClassroomOption option, final TICCallback callback) {

        if (option == null || option.getClassId() < 0) {
            TXCLog.i(TAG, "TICManager: joinClassroom Para Error");
            CallbackUtil.notifyError(callback, MODULE_TIC_SDK, Error.ERR_INVALID_PARAMS, Error.ERR_MSG_INVALID_PARAMS);
            return;
        }

        TXCLog.i(TAG, "TICManager: joinClassroom classId:" + option.toString() + " callback:" + callback);

        classroomOption = option;

        final int classId = classroomOption.getClassId();
        String groupId = String.valueOf(classId);
        final String desc = "board group";

        TICReporter.updateRoomId(classId);
        TICReporter.report(TICReporter.EventId.joinGroup_start);
        TIMGroupManager.getInstance().applyJoinGroup(groupId, desc + groupId, new TIMCallBack() {
            @Override
            public void onSuccess() {

                TXCLog.i(TAG, "TICManager: joinClassroom onSuccess ");
                TICReporter.report(TICReporter.EventId.joinGroup_end);
                onJoinClassroomSuccessfully(callback);
            }

            @Override
            public void onError(int errCode, String errMsg) {
                if (callback != null) {
                    if (errCode == 10013) {
                        //you are already group member.
                        TXCLog.i(TAG, "TICManager: joinClassroom 10013 onSuccess");
                        TICReporter.report(TICReporter.EventId.joinGroup_end);
                        onJoinClassroomSuccessfully(callback);
                    } else {
                        TXCLog.i(TAG, "TICManager: joinClassroom onError:" + errCode + "|" + errMsg);
                        TICReporter.report(TICReporter.EventId.joinGroup_end, errCode, errMsg);
                        callback.onError(MODULE_IMSDK, errCode, errMsg);
                    }
                }
            }
        });

        if (classroomOption.compatSaas) {
            groupId += COMPAT_SAAS_CHAT;
            TIMGroupManager.getInstance().applyJoinGroup(groupId, desc + groupId, new TIMCallBack() {
                @Override
                public void onSuccess() {
                    TXCLog.i(TAG, "TICManager: joinClassroom compatSaas onSuccess ");
                }

                @Override
                public void onError(int errCode, String errMsg) {
                    if (callback != null) {
                        if (errCode == 10013) {
                            //you are already group member.
                            TXCLog.i(TAG, "TICManager: joinClassroom compatSaas 10013 onSuccess");
                        } else {
                            TXCLog.i(TAG, "TICManager: joinClassroom compatSaas onError:" + errCode + "|" + errMsg);
                        }
                    }
                }
            });
        }

    }

    @Override
    public void quitClassroom(boolean clearBoard, final TICCallback callback) {
        TXCLog.i(TAG, "TICManager: quitClassroom " + clearBoard + "|" + callback);

        if (classroomOption == null) {
            TXCLog.e(TAG, "TICManager: quitClassroom para Error.");
            CallbackUtil.notifyError(callback, MODULE_TIC_SDK, Error.ERR_NOT_IN_CLASS, Error.ERR_MSG_NOT_IN_CLASS);
            return;
        }

        TICReporter.report(TICReporter.EventId.quitGroup_start);
        //3、im退房间
        int classId = classroomOption.getClassId();
        String groupId = String.valueOf(classId);
        TIMGroupManager.getInstance().quitGroup(groupId, new TIMCallBack() {//NOTE:在被挤下线时，不会回调
            @Override
            public void onError(int errorCode, String errInfo) {
                TXCLog.e(TAG, "TICManager: quitClassroom onError, err:" + errorCode + " msg:" + errInfo);
                TICReporter.report(TICReporter.EventId.quitGroup_end, errorCode, errInfo);
                if (callback != null) {
                    if (errorCode == 10009) {
                        callback.onSuccess(0);
                    }
                    else {
                        callback.onError(MODULE_IMSDK, errorCode, errInfo);
                    }
                }
            }

            @Override
            public void onSuccess() {
                TXCLog.e(TAG, "TICManager: quitClassroom onSuccess");
                TICReporter.report(TICReporter.EventId.quitGroup_end);
                CallbackUtil.notifySuccess(callback, 0);
            }
        });

        if (classroomOption.compatSaas) {
            groupId += COMPAT_SAAS_CHAT;

            TIMGroupManager.getInstance().quitGroup(groupId, new TIMCallBack() {
                //NOTE:在被挤下线时，不会回调
                @Override
                public void onError(int errorCode, String errInfo) {
                    TXCLog.e(TAG, "TICManager: quitClassroom compatSaas, err:" + errorCode + " msg:" + errInfo);
                }

                @Override
                public void onSuccess() {
                    TXCLog.e(TAG, "TICManager: quitClassroom onSuccess compatSaas");
                }
            });
        }

        //
        releaseClass();
    }

    private void onJoinClassroomSuccessfully(final TICCallback callback) {
        if (classroomOption == null || classroomOption.getClassId() < 0) {
            CallbackUtil.notifyError(callback, MODULE_TIC_SDK, Error.ERR_INVALID_PARAMS, Error.ERR_MSG_INVALID_PARAMS);
            return;
        }
    }

    private void releaseClass() {
        classroomOption = null;
    }
}
