package com.tojoy.file.browser;

import android.content.Context;

/**
 * Created by chengyanfang on 2018/5/15.
 */

public interface IMFileProvider {

    // 请求文件列表，展示完文件后发送
    void requestFileList(Context context, Callback callback);

    // 打开文件
    void openFile(Context context, String fileName, String filePath);

    // 发送文件的回调
    interface Callback {
        void onSuccess(String fileName, String filePath);
    }

}
