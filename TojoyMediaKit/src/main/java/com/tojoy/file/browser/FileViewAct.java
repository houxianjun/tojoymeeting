package com.tojoy.file.browser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;

import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.hud.ProgressHUD;
import com.tojoy.tjoybaselib.util.file.FileUtil;
import com.tojoy.tjoybaselib.util.string.MD5;
import com.tojoy.file.downloader.FileDownloader;
import com.tojoy.file.widgets.X5Webview.SuperFileView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.senab.photoview.R;

/**
 * Created by chengyanfang on 2018/3/13.
 */

public class FileViewAct extends UI {

    SuperFileView mSuperFileView;

    private static IMFileProvider.Callback callback;

    String mFileName, mFilePath;

    public static void startActivity(Context activity, String fileName, String filePath, boolean canSend, IMFileProvider.Callback callback) {
        Intent intent = new Intent();
        intent.setClass(activity, FileViewAct.class);
        intent.putExtra("fileName", fileName);
        intent.putExtra("canSend", canSend);
        intent.putExtra("filePath", filePath);
        activity.startActivity(intent);
        FileViewAct.callback = callback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fileview_layout);
        setStatusBar();
        ButterKnife.bind(this);

        mFilePath = getIntent().getStringExtra("filePath");
        mFileName = getIntent().getStringExtra("fileName");

        initTitle();
        initUI();
    }


    /**
     * 设置Nav
     */
    private void initTitle() {
        setUpNavigationBar();
        setTitle(mFileName);
        showBack();
        if (getIntent().getBooleanExtra("canSend", false)) {
            setRightManagerTitle("发送");
        }
    }


    /**
     * 配置UI元素
     */
    private void initUI() {
        mSuperFileView = (SuperFileView) findViewById(R.id.fileview);

        mSuperFileView.setOnGetFilePathListener(mSuperFileView2 -> {
            if (mFilePath.contains("http")) {
                downLoadFromNet(mFilePath, mSuperFileView2);
            } else {
                mSuperFileView.displayFile(new File(mFilePath));
            }
        });

        mSuperFileView.show();
    }


    protected ProgressHUD mProgressHUD;

    public void showProgressHUD(Activity context, String showMessage, boolean cancleAble) {
        if (context.isFinishing()) {
            return;
        }
        mHandler.post(() ->
                mProgressHUD = ProgressHUD.show(context, showMessage, cancleAble, null));
    }

    @Override
    public void dismissProgressHUD() {
        try {
            if (mProgressHUD != null)
                mHandler.post(() ->
                        mProgressHUD.dismiss());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void downLoadFromNet(final String url, final SuperFileView mSuperFileView) {
        //1.网络下载、存储路径、
        File cacheFile = getCacheFile(url);
        if (cacheFile.exists()) {
            if (cacheFile.length() <= 0) {
                cacheFile.delete();
            } else {
                mSuperFileView.displayFile(cacheFile);
            }
            return;
        }

        showProgressHUD(this, "加载中", false);

        FileDownloader.loadFile(url, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                boolean flag;
                InputStream is = null;
                byte[] buf = new byte[2048];
                int len = 0;
                FileOutputStream fos = null;

                try {

                    ResponseBody responseBody = response.body();
                    is = responseBody.byteStream();
                    long total = responseBody.contentLength();

                    File file1 = getCacheDir(url);
                    if (!file1.exists()) {
                        file1.mkdirs();
                    }

                    File fileN = getCacheFile(url);

                    if (!fileN.exists()) {
                        boolean mkdir = fileN.createNewFile();
                    }
                    fos = new FileOutputStream(fileN);
                    long sum = 0;
                    while ((len = is.read(buf)) != -1) {
                        fos.write(buf, 0, len);
                        sum += len;
                        int progress = (int) (sum * 1.0f / total * 100);
                    }
                    fos.flush();
                    //2.ACache记录文件的有效期
                    mSuperFileView.displayFile(fileN);

                } catch (Exception e) {

                } finally {
                    mHandler.post(() -> dismissProgressHUD());
                    try {
                        if (is != null)
                            is.close();
                    } catch (IOException e) {
                    }
                    try {
                        if (fos != null)
                            fos.close();
                    } catch (IOException e) {
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                File file = getCacheFile(url);
                if (!file.exists()) {
                    file.delete();
                }
            }
        });

    }

    @Override
    protected void onRightManagerTitleClick() {
        super.onRightManagerTitleClick();
        callback.onSuccess(mFileName, mFilePath);
//        ActivityStack.getActivityStack().popAllActivity();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSuperFileView != null) {
            mSuperFileView.destroyDrawingCache();
            mSuperFileView.onStopDisplay();
            mSuperFileView = null;
        }
    }


    public File getCacheDir(String url) {
        return new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/007/");

    }

    public File getCacheFile(String url) {
        File cacheFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/007/"
                + MD5.encode(url) + "." + FileUtil.getFilevExtension(url));
        return cacheFile;
    }

}
