package com.tojoy.file.downloader;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by chengyanfang on 2018/3/13.
 */

public class FileDownloader {

    public static void loadFile(String url, Callback<ResponseBody> callback) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.baidu.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LoadFileApi mLoadFileApi = retrofit.create(LoadFileApi.class);
        Call<ResponseBody> call = mLoadFileApi.loadPdfFile(url);
        call.enqueue(callback);
    }
}


interface LoadFileApi {
    @GET
    Call<ResponseBody> loadPdfFile(@Url String fileUrl);
}
