package com.tojoy.provider;


import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.util.string.MD5;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by chengyanfang on 2017/9/29.
 *
 */

public class OSSFileNameProvider {

    public static final String MODULE_Dynamic = "dynamic";
    public static final String MODULE_USERCENTER = "usercenter";
    public static final String MODULE_NetMeetiing = "netMeeting";

    /**
     * 获取上传图片文件名
     *
     * @return
     */
    public static String getImgFileName(String filePath, String functionModuleName, float whRatio) {
        Date date = new Date();
        String objectName =
                "tojoy/"+OSSConfig.getOSSRootFileName()
                        + functionModuleName + "/"
                        + new SimpleDateFormat("yyyyMM/dd").format(date)
                        + "/image/"
                        + MD5.encode(filePath) + System.currentTimeMillis() + "whRatio=" + whRatio
                        + ".png";
        return objectName;
    }

    /***
     * 获取ppt上传文件名
     */
    public static String getFileName(String filePath,String moduleName) {
        String suffix = ".png";
        if(OSSFileNameProvider.MODULE_NetMeetiing.equals(moduleName)){
            if (filePath.endsWith("ppt") || filePath.endsWith("pptx")){
                suffix = ".ppt";
            }
        }
        Date date = new Date();
        String objectName =
                "tojoy/"+OSSConfig.getOSSRootFileName() + moduleName + "/"
                        + new SimpleDateFormat("yyyyMM/dd").format(date)
                        + "/ppt/"
                        + MD5.encode(filePath) + System.currentTimeMillis()
                        + suffix;
        return objectName;
    }


    /**
     * 获取上传图片文件名
     *
     * @param filePath
     * @param whRatio
     * @return
     */
    public static String getImgFileName(String filePath, float whRatio) {
        Date date = new Date();
        String objectName =
                "tojoy/" + OSSConfig.getOSSRootFileName() + "dynamic/"
                        + new SimpleDateFormat("yyyyMM/dd").format(date)
                        + "/image/"
                        + MD5.encode(filePath) + System.currentTimeMillis() + "whRatio=" + whRatio
                        + ".png";
        return objectName;
    }
}
