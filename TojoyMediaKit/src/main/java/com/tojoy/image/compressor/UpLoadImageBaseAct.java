package com.tojoy.image.compressor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.util.time.TimeUtil;
import com.tojoy.image.photopicker.Extras;
import com.tojoy.image.photopicker.RequestCode;
import com.tojoy.image.photopicker.model.PhotoInfo;
import com.tojoy.image.photopicker.model.PickerContract;

import java.util.ArrayList;
import java.util.List;

public abstract class UpLoadImageBaseAct extends UI {

    private final String TAG = "Compress--Test--Log";

    private long serviceStartTime;
    protected CompressingReciver reciver;

    protected ArrayList<LGImgCompressor.CompressResult> compressedImgsResult = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUploadReceiver();
    }

    /**
     * 压缩完成通知接收器
     */
    private class CompressingReciver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive:" + Thread.currentThread().getId());
            int flag = intent.getIntExtra(Constanse.KEY_COMPRESS_FLAG, -1);
            Log.d(TAG, " flag:" + flag);
            if (flag == Constanse.FLAG_BEGAIIN) {
                serviceStartTime = System.currentTimeMillis();
                Log.d(TAG, "onCompressServiceStart,time:" + TimeUtil.getBeijingNowTimeString("yyyy-MM-dd HH:mm:ss"));
                return;
            }

            if (flag == Constanse.FLAG_END) {
                compressedImgsResult = (ArrayList<LGImgCompressor.CompressResult>) intent.getSerializableExtra(Constanse.KEY_COMPRESS_RESULT);
                uploadImgToOSS();

                Log.d(TAG, compressedImgsResult.size() + "compressed done");
                Log.d(TAG, "compress " + compressedImgsResult.size() + " files used total time:" + (System.currentTimeMillis() - serviceStartTime));
            }
        }
    }

    /**
     * 初始化上传配置
     */
    private void initUploadReceiver() {
        reciver = new CompressingReciver();
        IntentFilter intentFilter = new IntentFilter(Constanse.ACTION_COMPRESS_BROADCAST);
        registerReceiver(reciver, intentFilter);
    }


    /**
     * 开始进行压缩
     */
    protected void startCompress() {
        ArrayList<String> compressFiles = getImgsUriList();
        if (compressFiles.size() == 0) {//如果是转发过来的图片 不做压缩处理 直接上传图片到OSS
            uploadImgToOSS();
        } else {//发布动态图片 对图片进行压缩
            for (int i = 0; i < compressFiles.size(); ++i) {
                String path = compressFiles.get(i);
                CompressServiceParam param = new CompressServiceParam();
                param.setSrcImageUri(path);
                LGImgCompressorIntentService.startActionCompress(this, param);
            }
        }

    }

    public abstract void uploadImgToOSS();


    public abstract ArrayList<String> getImgsUriList();


    /**
     * 表单页面图片选择器回调
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RequestCode.PICK_IMAGE:
                onPickImageActivityResult(data, 0);
                break;
            case RequestCode.PICK_COVER_IMAGE:
                onPickImageActivityResult(data, 1);
                break;
        }
    }

    protected void onPickImageActivityResult(Intent data, int type) {
        if (data == null) {
            return;
        }
        List<PhotoInfo> photos = new ArrayList<>();
        boolean local = data.getBooleanExtra(Extras.EXTRA_FROM_LOCAL, false);
        if (local) {
            // 本地相册
            photos = PickerContract.getPhotos(data);
            if (photos == null) {
                return;
            }
        } else {
            // 拍照
            String photoPath = data.getStringExtra(Extras.EXTRA_FILE_PATH);
            PhotoInfo photoInfo = new PhotoInfo();
            photoInfo.setFilePath(photoPath);
            photos.add(photoInfo);
        }

        if (type == 0) {
            addPhotoView(photos);
        } else {
            addCover(photos.get(0));
        }

    }

    protected void addPhotoView(List<PhotoInfo> photoInfos) {

    }

    protected void addCover(PhotoInfo photoInfo) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (reciver != null) {
            unregisterReceiver(reciver);
        }
    }

}
