package com.tojoy.image.photopicker;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.tojoy.image.photopicker.activity.PickImageActivity;
import com.tojoy.tjoybaselib.ui.dialog.CustomAlertDialog;
import com.tojoy.tjoybaselib.util.storage.StorageType;
import com.tojoy.tjoybaselib.util.storage.StorageUtil;
import com.tojoy.tjoybaselib.util.string.StringUtil;

import uk.co.senab.photoview.R;

/**
 * Created by huangjun on 2015/9/22.
 */
public class PickImageHelper {

    public static class PickImageOption {
        /**
         * 图片选择器标题
         */
        public int titleResId = R.string.choose;

        /**
         * 是否多选
         */
        public boolean multiSelect = true;

        /**
         * 最多选多少张图（多选时有效）
         */
        public int multiSelectMaxCount = 9;

        /**
         * 是否进行图片裁剪(图片选择模式：false / 图片裁剪模式：true)
         */
        public boolean crop = false;

        /**
         * 图片裁剪的宽度（裁剪模式时有效）
         */
        public int cropOutputImageWidth = 720;

        /**
         * 图片裁剪的高度（裁剪模式时有效）
         */
        public int cropOutputImageHeight = 720;

        /**
         * 图片选择保存路径
         */
        public String outputPath = StorageUtil.getWritePath(StringUtil.get32UUID() + ".jpg", StorageType.TYPE_TEMP);
    }

    /**
     * 打开图片选择器
     */
    public static void pickImage(final Context context, final int requestCode, final PickImageOption option) {
        if (context == null) {
            return;
        }

        CustomAlertDialog dialog = new CustomAlertDialog(context);
        dialog.setTitle(R.string.choose);

        dialog.addItem(context.getString(R.string.input_panel_take), () -> {
            int from = PickImageActivity.FROM_CAMERA;
            if (!option.crop) {
                PickImageActivity.start((Activity) context, requestCode, from, option.outputPath, option.multiSelect, 1,
                        true, false, 0, 0);
            } else {
                PickImageActivity.start((Activity) context, requestCode, from, option.outputPath, false, 1,
                        false, true, option.cropOutputImageWidth, option.cropOutputImageHeight);
            }

        });

        dialog.addItem(context.getString(R.string.choose_from_photo_album), () -> {
            int from = PickImageActivity.FROM_LOCAL;
            if (!option.crop) {
                PickImageActivity.start((Activity) context, requestCode, from, option.outputPath, option.multiSelect,
                        option.multiSelectMaxCount, true, false, 0, 0);
            } else {
                PickImageActivity.start((Activity) context, requestCode, from, option.outputPath, false, 1,
                        false, true, option.cropOutputImageWidth, option.cropOutputImageHeight);
            }

        });

        dialog.show();
    }


    /**
     * 打开图片选择器
     */
    public static void pickImageDireact(final Context context, final int requestCode, final PickImageOption option, int type) {
        if (context == null) {
            return;
        }

        if (type == 1) {
            int from = PickImageActivity.FROM_CAMERA;
            if (!option.crop) {
                PickImageActivity.start((Activity) context, requestCode, from, option.outputPath, option.multiSelect, 1,
                        true, false, 0, 0);
            } else {
                PickImageActivity.start((Activity) context, requestCode, from, option.outputPath, false, 1,
                        false, true, option.cropOutputImageWidth, option.cropOutputImageHeight);
            }
        } else {
            int from = PickImageActivity.FROM_LOCAL;
            if (!option.crop) {
                PickImageActivity.start((Activity) context, requestCode, from, option.outputPath, option.multiSelect,
                        option.multiSelectMaxCount, true, false, 0, 0);
            } else {
                PickImageActivity.start((Activity) context, requestCode, from, option.outputPath, false, 1,
                        false, true, option.cropOutputImageWidth, option.cropOutputImageHeight);
            }
        }
    }


    /**
     * 打开图片选择器
     */
    public static void pickImage(final Context context, final int requestCode, final PickImageOption option, DialogInterface.OnCancelListener cancelListener) {
        if (context == null) {
            return;
        }

        CustomAlertDialog dialog = new CustomAlertDialog(context);
        dialog.setTitle(option.titleResId);

        dialog.addItem(context.getString(R.string.input_panel_take), () -> {
            int from = PickImageActivity.FROM_CAMERA;
            if (!option.crop) {
                PickImageActivity.start((Activity) context, requestCode, from, option.outputPath, option.multiSelect, 1,
                        true, false, 0, 0);
            } else {
                PickImageActivity.start((Activity) context, requestCode, from, option.outputPath, false, 1,
                        false, true, option.cropOutputImageWidth, option.cropOutputImageHeight);
            }

        });

        dialog.addItem(context.getString(R.string.choose_from_photo_album), () -> {
            int from = PickImageActivity.FROM_LOCAL;
            if (!option.crop) {
                PickImageActivity.start((Activity) context, requestCode, from, option.outputPath, option.multiSelect,
                        option.multiSelectMaxCount, true, false, 0, 0);
            } else {
                PickImageActivity.start((Activity) context, requestCode, from, option.outputPath, false, 1,
                        false, true, option.cropOutputImageWidth, option.cropOutputImageHeight);
            }

        });

        dialog.setOnCancelListener(cancelListener);

        dialog.show();
    }

    public static void clickPickImage(final Activity context, final int requestCode, final PickImageOption option){
        int from = PickImageActivity.FROM_LOCAL;
        if (!option.crop) {
            PickImageActivity.start((Activity) context, requestCode, from, option.outputPath, option.multiSelect,
                    option.multiSelectMaxCount, true, false, 0, 0);
        } else {
            PickImageActivity.start((Activity) context, requestCode, from, option.outputPath, false, 1,
                    false, true, option.cropOutputImageWidth, option.cropOutputImageHeight);
        }
    }

    public static void clickPickImageCamera(final Activity context, final int requestCode, final PickImageOption option){
        int from = PickImageActivity.FROM_CAMERA;
        if (!option.crop) {
            PickImageActivity.start((Activity) context, requestCode, from, option.outputPath, option.multiSelect, 1,
                    true, false, 0, 0);
        } else {
            PickImageActivity.start((Activity) context, requestCode, from, option.outputPath, false, 1,
                    false, true, option.cropOutputImageWidth, option.cropOutputImageHeight);
        }
    }

    public static void clickPickVideo(final Activity context, final int requestCode, final PickImageOption option){
        // 视频
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("video/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        context.startActivityForResult(intent, requestCode);
    }
}
