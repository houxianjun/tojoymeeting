package com.tojoy.image.photopicker;

/**
 * Created by zhoujianghua on 2015/8/11.
 */
public interface Extras {

    String EXTRA_FILE_PATH = "file_path";

    String EXTRA_DATA = "data";

    String EXTRA_FROM = "from";

    // 选择图片
    String EXTRA_NEED_CROP = "need-crop";

    String EXTRA_OUTPUTX = "outputX";

    String EXTRA_OUTPUTY = "outputY";

    String EXTRA_FROM_LOCAL = "from_local";

    String EXTRA_SRC_FILE = "src-file";

    String EXTRA_RETURN_DATA = "return-data";

    // 参数
    String EXTRA_ACCOUNT = "account";
    String EXTRA_TYPE = "type";
    String EXTRA_ANCHOR = "anchor";

    String EXTRA_CUSTOMIZATION = "customization";
    String EXTRA_BACK_TO_CLASS = "backToClass";

    String EXTRA_TEAMTITLE = "extra_teamtitle";

    //图片选自器
    String EXTRA_PHOTO_LISTS = "photo_list";
    String EXTRA_SELECTED_IMAGE_LIST = "selected_image_list";
    String EXTRA_MUTI_SELECT_MODE = "muti_select_mode";
    String EXTRA_MUTI_SELECT_SIZE_LIMIT = "muti_select_size_limit";
    String EXTRA_SUPPORT_ORIGINAL = "support_original";
    String EXTRA_IS_ORIGINAL = "is_original";
    String EXTRA_PREVIEW_CURRENT_POS = "current_pos";
    String EXTRA_PREVIEW_IMAGE_BTN_TEXT = "preview_image_btn_text";
    String EXTRA_SCALED_IMAGE_LIST = "scaled_image_list";
    String EXTRA_ORIG_IMAGE_LIST = "orig_image_list";
    String EXTRA_NEED_SHOW_SEND_ORIGINAL = "need_show_send_original_image";

    //用户基础信息
    String EXTRA_NAVTITLE = "extra_navtitle";
    String EXTRA_NICKNAME = "extra_nickname";
    String EXTRA_HEADIMG = "extra_headimg";
    String EXTRA_JOBNAME = "extra_jobname";
    String EXTRA_COMPANY = "extra_company";
    String EXTRA_TAGTYPE = "extra_tagtype";
    String EXTRA_TAGLIST = "extra_taglist";
    String EXTRA_TEAMID = "extra_teamid";
    String EXTRA_FROMTYPE = "extra_fromtype";
    String EXTRA_USERINFO = "extra_userinfo";
    String EXTRA_EDITCONTENT = "extra_editcontent";
    String EXTRA_EDITHINT = "extra_edithint";
    String EXTRA_WALLET_TYPE = "extra_wallet_type";
    String EXTRA_FIRST_SELECT = "extra_first_select";
    //增添个人主页修改爱好
    String EXTRS_HOBBY_USERCENTER= "extrs_hobby_usercenter";

    //业务模块
    String EXTRA_MEETINGID = "extra_meetingID";
    String EXTRA_MEETINGPRICE = "extra_meetingPrice";

}
