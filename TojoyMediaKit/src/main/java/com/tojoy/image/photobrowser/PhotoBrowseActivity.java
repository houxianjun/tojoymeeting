package com.tojoy.image.photobrowser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.viewpager.HackyViewPager;
import com.tojoy.tjoybaselib.ui.dotindicator.DotIndicator;
import com.tojoy.tjoybaselib.util.media.ImageUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.image.widget.gallery.GalleryPhotoView;
import com.tojoy.image.widget.photoview.PhotoView;

import java.util.LinkedList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import uk.co.senab.photoview.R;

/**
 * Created by chengyanfang on 2017/9/7.
 *
 * @function:图片浏览器
 */

public class PhotoBrowseActivity extends UI {

    private static final String TAG = "PhotoBrowseActivity";

    private HackyViewPager photoViewpager;
    private View blackBackground;
    private DotIndicator dotIndicator;//小圆点指示器

    private List<GalleryPhotoView> viewBuckets;
    private PhotoBrowseInfo photoBrowseInfo;
    private InnerPhotoViewerAdapter adapter;

    ProgressBar mProgressBar;
    private int count;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBar();
        setContentView(R.layout.activity_photo_browse);
        preInitData();
        initView();

        setSwipeBackEnable(false);
    }


    private void preInitData() {
        photoBrowseInfo = getIntent().getParcelableExtra("photoinfo");

        int size = photoBrowseInfo.getPhotoUrls().size();
        count = 0;
        for (int i = 0; i < size; i++) {
            if (!TextUtils.isEmpty(photoBrowseInfo.getPhotoUrls().get(i))) {
                count++;
            }
        }
        viewBuckets = new LinkedList<>();
        final int photoCount = count;
        for (int i = 0; i < photoCount; i++) {
            GalleryPhotoView photoView = new GalleryPhotoView(this);
            photoView.setCleanOnDetachedFromWindow(false);
            photoView.setOnViewTapListener((view, x, y) -> finish());
            viewBuckets.add(photoView);
        }
    }

    private void initView() {
        photoViewpager = (HackyViewPager) findViewById(R.id.photo_viewpager);
        mProgressBar = (ProgressBar) findViewById(R.id.loading);
        blackBackground = findViewById(R.id.v_background);
        dotIndicator = (DotIndicator) findViewById(R.id.dot_indicator);

        dotIndicator.init(this, count);
        dotIndicator.setCurrentSelection(photoBrowseInfo.getCurrentPhotoPosition());

        adapter = new InnerPhotoViewerAdapter(this);
        photoViewpager.setAdapter(adapter);
        photoViewpager.setLocked(count == 1);
        photoViewpager.setCurrentItem(photoBrowseInfo.getCurrentPhotoPosition());
        photoViewpager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                dotIndicator.setCurrentSelection(position);
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (!AppUtils.isListEmpty(viewBuckets)) {
            for (PhotoView photoView : viewBuckets) {
                photoView.destroy();
            }
        }
        Glide.get(this).clearMemory();
        super.onDestroy();
    }

    //=============================================================Tools method
    public static void startToPhotoBrowseActivity(Activity from, @NonNull PhotoBrowseInfo info) {
        if (info == null || !info.isValided()) return;
        Intent intent = new Intent(from, PhotoBrowseActivity.class);
        intent.putExtra("photoinfo", info);
        from.startActivity(intent);
        //禁用动画
        from.overridePendingTransition(0, 0);
    }

    @Override
    public void finish() {
        mProgressBar.setVisibility(View.GONE);

        if(photoViewpager.getCurrentItem() >= viewBuckets.size()) {
            super.finish();
            return;
        }

        final GalleryPhotoView currentPhotoView = viewBuckets.get(photoViewpager.getCurrentItem());
        if (currentPhotoView == null) {
            super.finish();
            return;
        }
        final Rect endRect = photoBrowseInfo.getViewLocalRects().get(photoViewpager.getCurrentItem());
        currentPhotoView.playExitAnima(endRect, blackBackground, () -> {
            PhotoBrowseActivity.super.finish();
            overridePendingTransition(0, 0);
        });
    }




    //=============================================================InnerAdapter

    private class InnerPhotoViewerAdapter extends PagerAdapter {
        private Context context;
        private boolean isFirstInitlize;


        public InnerPhotoViewerAdapter(Context context) {
            this.context = context;
            isFirstInitlize = true;
        }

        @Override
        public int getCount() {
            return count;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            GalleryPhotoView photoView = viewBuckets.get(position);
            String photoUrl = photoBrowseInfo.getPhotoUrls().get(position);

            Glide.with(context).asBitmap().load(photoUrl).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    photoView.setImageBitmap(resource);
                }
            });

            container.addView(photoView);


            if (!TextUtils.isEmpty(photoUrl)) {
                photoView.setOnLongClickListener(v -> {
                    SavePhotoPopup savePhotoPopup = new SavePhotoPopup(PhotoBrowseActivity.this);
                    savePhotoPopup.setOnSavePictureCallback(() ->
                            {
                                new Thread(() -> {
                                    try {
                                        Bitmap bitmap = Glide.with(context)
                                                .asBitmap()
                                                .load(photoUrl)
                                                .apply(new RequestOptions().centerCrop())
                                                .into(500,500).get();
                                        ImageUtil.saveImageToGallery(context, bitmap);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }).start();

                                Toasty.normal(context, "保存图片成功,请到相册查看!").show();
                            }

                    );
                    savePhotoPopup.showPopupWindow();
                    return false;
                });

            }
            return photoView;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, final int position, final Object object) {
            if (isFirstInitlize && object instanceof GalleryPhotoView && position == photoBrowseInfo.getCurrentPhotoPosition()) {
                isFirstInitlize = false;
                final GalleryPhotoView targetView = (GalleryPhotoView) object;
                final Rect startRect = photoBrowseInfo.getViewLocalRects().get(position);
                targetView.playEnterAnima(startRect, null);
            }
            super.setPrimaryItem(container, position, object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }

    public interface OnLongClickListener {
        void onClickAt(Bitmap bitmap);
    }
}
