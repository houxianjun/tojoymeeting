package com.tojoy.image.photobrowser;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

import es.dmoral.toasty.Toasty;
import razerdp.basepopup.BasePopupWindow;
import uk.co.senab.photoview.R;

/**
 * Created by chengyanfang on 2017/10/23.
 */

public class SavePhotoPopup extends BasePopupWindow implements View.OnClickListener {

    private TextView mSaveTv;
    private TextView mScanner;
    private TextView mCancel;

    Activity mContext;

    public SavePhotoPopup(Activity context) {
        super(context);
        mSaveTv = (TextView) findViewById(R.id.save);
        mScanner = (TextView) findViewById(R.id.scanner);
        mCancel = (TextView) findViewById(R.id.cancel);
        setViewClickListener(this, mSaveTv, mCancel, mScanner);
        mContext = context;
    }

    @Override
    public View initAnimaView() {
        return findViewById(R.id.popup_container);
    }

    @Override
    protected Animation initShowAnimation() {
        return getTranslateAnimation(300, 0, 300);
    }

    @Override
    protected Animation initExitAnimation() {
        return getTranslateAnimation(0, 300, 300);
    }

    @Override
    public View onCreatePopupView() {
        return createPopupById(R.layout.popup_save_photo);
    }

    @Override
    public View getClickToDismissView() {
        return getPopupWindowView();
    }

    public void showScanner(OnScannerClickCallback onScannerClickCallback) {
        this.onScannerClickCallback = onScannerClickCallback;
        mScanner.setVisibility(View.VISIBLE);
    }

    public void showPopupWindow() {
        super.showPopupWindow();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.save) {

            if (mOnSavePictureCallback != null) {
                mOnSavePictureCallback.onClickSaveBitmap();
            }
            dismiss();
        }

        if (v.getId() == R.id.scanner) {
            if (onScannerClickCallback != null) {
                onScannerClickCallback.onClickScanner();
            }
            dismiss();
        }

        if (v.getId() == R.id.cancel) {
            dismiss();
        }
    }

    private OnSavePictureCallback mOnSavePictureCallback;

    public void setOnSavePictureCallback(OnSavePictureCallback onSavePictureCallback) {
        mOnSavePictureCallback = onSavePictureCallback;
    }

    public interface OnSavePictureCallback {
        void onClickSaveBitmap();
    }


    private OnScannerClickCallback onScannerClickCallback;

    public interface OnScannerClickCallback {
        void onClickScanner();
    }
}
