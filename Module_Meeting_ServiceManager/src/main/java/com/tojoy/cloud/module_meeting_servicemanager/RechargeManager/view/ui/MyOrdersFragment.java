package com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.view.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.model.servicemanager.OrderModel;
import com.tojoy.tjoybaselib.model.servicemanager.RechargeCancelOrderResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.live.LiveRoomInfoProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.time.TimeUtil;
import com.tojoy.cloud.module_meeting_servicemanager.R;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;

import static com.tojoy.tjoybaselib.util.router.RouterJumpUtil.SKIP_FROM_RECHARGE_RECORD;

@SuppressLint("ValidFragment")
public class MyOrdersFragment extends Fragment {

    /**
     * null：全部 1：待付款 3：已完成
     */
    private String mOrderStatus;
    private View mView;
    private TJRecyclerView mRecyclerView;
    private MyOrdersAdapter mAdapter;
    private int mPage = 1;
    private List<OrderModel.DataBean.ListBean> mDatas = new ArrayList<>();

    public MyOrdersFragment(String orderStatus) {
        this.mOrderStatus = orderStatus;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.sm_fragment_my_orders, container, false);
        initUI();

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPage = 1;
        initData();

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            mPage = 1;
            initData();
        }
    }

    private void initData() {

        OMAppApiProvider.getInstance().queryOrderList("10", mPage + "", mOrderStatus, LiveRoomInfoProvider.getInstance().comCode,
                new Observer<OrderModel>() {

                    @Override
                    public void onCompleted() {
                        mRecyclerView.stopRefresh();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toasty.normal(getActivity(), "网络错误，请检查网络").show();
                        mRecyclerView.stopRefresh();
                    }

                    @Override
                    public void onNext(OrderModel response) {
                        if (response.isSuccess()) {
                            if (mPage == 1) {
                                mDatas.clear();
                            }
                            mDatas.addAll(response.data.list);
                            mAdapter.updateData(mDatas);
                            mRecyclerView.refreshLoadMoreView(mPage, response.data.list.size());

                        } else {
                            Toasty.normal(getActivity(), response.msg).show();
                        }
                    }
                });
    }

    private void initUI() {
        mRecyclerView = mView.findViewById(R.id.recyclerView);
        mRecyclerView.getRecyclerView().setVerticalScrollBarEnabled(false);
        mAdapter = new MyOrdersAdapter(getActivity(), mDatas);
        mRecyclerView.setLoadMoreViewWithHide();
        mRecyclerView.setEmptyImgAndTip(R.drawable.icon_empty_order, "暂无订单");
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                initData();
            }

            @Override
            public void onLoadMore() {
                mPage++;
                initData();
            }
        });
    }

    private class MyOrdersAdapter extends BaseRecyclerViewAdapter<OrderModel.DataBean.ListBean> {

        MyOrdersAdapter(@NonNull Context context, @NonNull List<OrderModel.DataBean.ListBean> datas) {
            super(context, datas);
        }

        @Override
        protected int getViewType(int position, @NonNull OrderModel.DataBean.ListBean data) {
            return 0;
        }

        @Override
        protected int getLayoutResId(int viewType) {
            return R.layout.sm_item_my_orders;
        }

        @Override
        protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
            return new MyOrdersVH(getActivity(), parent, getLayoutResId(viewType));
        }
    }

    private class MyOrdersVH extends BaseRecyclerViewHolder<OrderModel.DataBean.ListBean> {

        private TextView tvOrderNo;
        private TextView tvOrderState;
        private TextView tvOrderTitle;
        private TextView tvTimeCount;
        private TextView tvOrderDate;
        private TextView tvPrice;
        private RelativeLayout btnLayout;
        private TextView tvCancle;
        private TextView tvPay;

        MyOrdersVH(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            bindViews();
        }

        private void bindViews() {
            tvOrderNo = itemView.findViewById(R.id.tvOrderNo);
            tvOrderState = itemView.findViewById(R.id.tvOrderState);
            tvOrderTitle = itemView.findViewById(R.id.tvOrderTitle);
            tvTimeCount = itemView.findViewById(R.id.tvTimeCount);
            tvOrderDate = itemView.findViewById(R.id.tvOrderDate);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            btnLayout = itemView.findViewById(R.id.btnLayout);
            tvCancle = itemView.findViewById(R.id.tvCancle);
            tvPay = itemView.findViewById(R.id.tvPay);
        }

        @Override
        public void onBindData(OrderModel.DataBean.ListBean data, int position) {
            tvOrderNo.setText("订单编号：" + data.orderCode);
            String state;
            if (data.orderStatus == 1) {
                state = "等待买家付款";
                btnLayout.setVisibility(View.VISIBLE);
            } else if (data.orderStatus == 2) {
                state = "交易关闭";
                btnLayout.setVisibility(View.GONE);
            } else {
                state = "交易完成";
                btnLayout.setVisibility(View.GONE);
            }
            tvOrderState.setText(state);
            tvOrderTitle.setText(data.feeName);
            tvTimeCount.setText(data.getServiceTime() + "分钟");
            tvOrderDate.setText(TimeUtil.getLoneTimeStr(data.orderDate));
            tvPrice.setText("合计：￥" + data.orderPrice);

            tvCancle.setOnClickListener(v -> new TJMakeSureDialog(getActivity(), "确定取消该订单？", v1 -> {
                ((UI) (getActivity())).showProgressHUD(getActivity(), "加载中");
                OMAppApiProvider.getInstance().cancelOrder(data.id, new Observer<RechargeCancelOrderResponse>() {
                    @Override
                    public void onCompleted() {
                        ((UI) (getActivity())).dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ((UI) (getActivity())).dismissProgressHUD();
                        Toasty.normal(getActivity(), "网络错误，请检查网络").show();
                    }

                    @Override
                    public void onNext(RechargeCancelOrderResponse response) {
                        if (response.isSuccess()) {
                            refreshCancel();
                            String tabType = "";
                            if (TextUtils.isEmpty(mOrderStatus)) {
                                tabType = "1";
                            }
                            ((PurchaseRecordAct) (getActivity())).onOrderCancel(tabType);
                        }
                        Toasty.normal(getActivity(), response.getData()).show();
                    }
                });
            }).setBtnText("确定", "取消").show());

            tvPay.setOnClickListener(v -> {
                if (!FastClickAvoidUtil.isFastDoubleClick(tvPay.getId())) {
                    RouterJumpUtil.startOrderPayDetailAct(data.id + "", data.orderCode, data.orderPrice, SKIP_FROM_RECHARGE_RECORD);
                }
            });
        }
    }

    public void refreshCancel() {
        mPage = 1;
        initData();
    }
}
