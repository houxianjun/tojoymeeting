package com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.precenter;



public interface OnGetDataFinishedListener {

    void onSuccess();

    void onCompleted();

    void onError(Throwable e);

    void toast(String msg);
}
