package com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.cloud.module_meeting_servicemanager.R;

import java.util.List;

/**
 * 充值点数页面：固定金额适配器，从服务端返回数据
 */
public class FixRechargeAcountAdapter extends BaseRecyclerViewAdapter<String> {

    private SparseBooleanArray mBooleanArray;

    private int mLastCheckedPosition = -1;

    private Context context;

    public FixRechargeAcountAdapter(@NonNull Context context, @NonNull List<String> datas) {
        super(context, datas);
        this.context = context;
        mBooleanArray = new SparseBooleanArray(datas.size());
    }

    public void setItemChecked(int position) {
        mBooleanArray.put(position, true);
        if (mLastCheckedPosition > -1) {
            mBooleanArray.put(mLastCheckedPosition, false);
            notifyItemChanged(mLastCheckedPosition);
        }
        notifyDataSetChanged();
        mLastCheckedPosition = position;
    }

    /**
     * 清理默认选项
     */
    public void clearItemChecked(int position) {
        mBooleanArray.put(position, false);
        notifyItemChanged(mLastCheckedPosition);
        mLastCheckedPosition = -1;

    }

    public int getmLastCheckedPosition() {
        return mLastCheckedPosition;
    }

    /**
     * 设置默认项
     */
    public void normalItemChecked() {
        mBooleanArray.put(0, true);
        notifyItemChanged(0);
        mLastCheckedPosition = 0;
    }

    @Override
    protected int getViewType(int position, @NonNull String data) {
        return position;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_adapter_fix_acount;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new ACountViewHoder(context, getLayoutResId(viewType));
    }

    public class ACountViewHoder extends BaseRecyclerViewHolder<String> {

        TextView tvFixAcount;

        TextView tvFixAcountUnit;

        RelativeLayout rlFixAcountMain;

        @SuppressLint("ResourceAsColor")
        @Override
        public void onBindData(String data, int position) {
            if (!mBooleanArray.get(position)) {
                //没有选中
                rlFixAcountMain.setBackgroundResource(R.drawable.sm_fix_amount_normal_bg);
                tvFixAcountUnit.setTextColor(context.getResources().getColor(R.color.color_353f5c));
                tvFixAcount.setTextColor(context.getResources().getColor(R.color.color_353f5c));
                tvFixAcount.setText(data);
            } else {
                rlFixAcountMain.setBackgroundResource(R.drawable.sm_fix_amount_click_bg);
                tvFixAcountUnit.setTextColor(context.getResources().getColor(R.color.white));
                tvFixAcount.setTextColor(context.getResources().getColor(R.color.white));
                tvFixAcount.setText(data);
            }
        }

        ACountViewHoder(Context context, int layoutResId) {
            super(context, layoutResId);
            tvFixAcount = (TextView) findViewById(R.id.tv_fix_acount);
            tvFixAcountUnit = (TextView) findViewById(R.id.tv_fix_acount_unit);
            rlFixAcountMain = (RelativeLayout) findViewById(R.id.rl_fix_acount_main);
        }
    }
}
