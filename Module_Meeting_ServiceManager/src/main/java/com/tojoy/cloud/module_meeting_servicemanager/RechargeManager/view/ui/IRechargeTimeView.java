package com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.view.ui;


import android.content.Context;
import android.view.View;

import com.tojoy.tjoybaselib.model.servicemanager.RechargeProductResponse;
import com.tojoy.tjoybaselib.model.servicemanager.RemainTimeResponse;


public interface IRechargeTimeView {

    /**
     * 获取全局上下文
     * @return context
     */
    Context getContext();


    /**
     *获取剩余分钟数，固定充值金额，上下限信息
     */
    void getSurplusTimeDetail();


    /**
     * 获取充值产品详情
     */
    void getRechargeProductDetail();

    /**
     *
     * @param rechargeTimeEntry
     */
    void getRechargeProductDetailSuccess(RechargeProductResponse.RechargeProductEntry rechargeTimeEntry);


    void getRechargeProductDetailError();

    void getSurplusTimeDetailonSuccess(RemainTimeResponse.RemainTimeEntry entry);

    /**
     *
     */
    void getSurplusTimeDetailonError();

    /**
     *
     * @param content
     */
    void setInputContent(String content);

    /**
     * 立即购买去支付
     * @param view
     */
    void goPayMoney(View view);


    /**
     * 请求订单接口
     * @param serviceTime
     * @param orderPrice
     * @param feeName
     * @param feePrice
     * @param feeCode
     */
    void queryRechargesaveorder(String serviceTime,String orderPrice
            ,String feeName,String feePrice,String feeCode);

    /**
     * 前往购买记录页面
     * @param view
     */
    void goPurchaseRecord(View view);

    /**
     * 前往使用明细／消费明细
     * @param view
     */
    void goConsumptionDetails(View view);

    /**
     *
     */
    void hideLoadingView();

    /**
     * Toast显示
     * @param msg
     */
    void toastShow(String msg);


    void showSureDialog();
}
