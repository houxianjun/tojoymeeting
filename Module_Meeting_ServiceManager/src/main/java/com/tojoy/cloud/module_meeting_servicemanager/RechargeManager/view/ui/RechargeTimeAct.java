package com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.view.ui;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.android.arouter.utils.TextUtils;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.servicemanager.RechargeProductResponse;
import com.tojoy.tjoybaselib.model.servicemanager.RemainTimeResponse;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.ui.recyclerview.decoration.SpacingDecoration;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRecyclerViewItemClickListener;

import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.StatusbarUtils;
import com.tojoy.cloud.module_meeting_servicemanager.R;
import com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.precenter.IRechargeTimePresenter;
import com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.precenter.RechargeTimePresenterImpl;
import com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.view.adapter.FixRechargeAcountAdapter;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.OrderChangeEvent;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * 充值时间页面：购买分钟数
 *
 * @author houxianjun by 2020.4.10
 */
@Route(path = RouterPathProvider.ServiceManager_RechargeTimeAct)
public class RechargeTimeAct extends UI implements IRechargeTimeView, OnRecyclerViewItemClickListener<String>, TextWatcher {
    TextView tvTitle;
    ImageView ivLeftimg;
    TextView tvDetail;
    /**
     * 全局布局组件
     */
    RelativeLayout rlMainLayout;
    /**
     * 限时免费布局
     */
    RelativeLayout rlFeeMain;
    /**
     * 限时免费文案
     */
    TextView tvFeeContent;
    /**
     * 限时免费图标
     */
    TextView tvFeeIcon;
    TextView tvSurplusTimeContent;
    TextView tvSurplusTimeUnit;
    /**
     * 显示剩余分钟数组件
     */
    TextView tvSurplusTime;
    /**
     * 显示固定金额列表
     */
    RecyclerView recyclerView;
    /**
     * 显示自定义购买时间提示
     */
    TextView tvPayTimeTips;
    /**
     * 输入自定义购买分钟数
     */
    EditText etInputAmount;
    /**
     * 立即购买按钮
     */
    TextView btnPayMoney;
    /**
     * 查看点数明细
     */
    TextView tvPayDetailed;
    /**
     * 剩余分钟数
     */
    private String surplusTimeString;

    /**
     * 固定金额列表适配器
     */
    private FixRechargeAcountAdapter fixRechargeAcountAdapter;

    /**
     * 当前固定金额选中位置索引
     */
    private int mCurrentPosition = -1;

    IRechargeTimePresenter timeRechargePresenter = null;

    RechargeProductResponse.RechargeProductEntry rechargeProductEntry = null;
    /**
     * 缓存数据
     */
    RechargeProductResponse.RechargeProductEntry cacheRechargeProductEntry = null;

    private TJMakeSureDialog mTjMakeSureDialog;

    /**
     * 下单总金额
     */
    Double orderTotalPrice;

    String serviceTotalTime;

    /**
     * 是否显示限时免费文案内容
     */
    volatile boolean isShowFeeContent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_servicemanager_rechargetime_layout);
        StatusbarUtils.enableTranslucentStatusbar(this);
        StatusbarUtils.setStatusBarLightMode(this, StatusbarUtils.statusbarlightmode(this));
        setSwipeBackEnable(true);
        timeRechargePresenter = new RechargeTimePresenterImpl(this);
        initTitle();
        initUi();
        getRechargeProductDetail();
    }

    @Override
    public void onResume() {
        super.onResume();
        //获取剩余分钟数，接口配置相关信息
        getSurplusTimeDetail();
    }

    private void initTitle() {
        int statusBarHeight = AppUtils.getStatusBarHeight(this);
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AppUtils.dip2px(this, 48));
        lps.setMargins(0, statusBarHeight, 0, 0);
        if (findViewById(R.id.rlv_title) != null) {
            findViewById(R.id.rlv_title).setLayoutParams(lps);
        }
        setTitle();
        setLeftBack();
    }

    public void setTitle() {
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(this.getResources().getText(R.string.sm_mine_meeting_service_manager));
        tvDetail = (TextView) findViewById(R.id.tv_detail);
        tvDetail.setVisibility(View.GONE);
    }

    public void setLeftBack() {
        ivLeftimg = (ImageView) findViewById(R.id.iv_leftimg);
        ivLeftimg.setOnClickListener(v -> onBackPressed());
    }

    private void initUi() {
        rlMainLayout = (RelativeLayout) findViewById(R.id.rl_main_layout);
        rlFeeMain = (RelativeLayout) findViewById(R.id.rl_fee_main);
        tvFeeIcon = (TextView) findViewById(R.id.tv_fee);
        tvFeeContent = (TextView) findViewById(R.id.tv_fee_content);

        tvSurplusTimeContent = (TextView) findViewById(R.id.tv_surplus_time_content);
        tvSurplusTimeContent.setVisibility(View.GONE);

        tvSurplusTimeUnit = (TextView) findViewById(R.id.tv_surplus_time_unit);
        tvSurplusTimeUnit.setVisibility(View.GONE);

        //显示剩余分钟数组件
        tvSurplusTime = (TextView) findViewById(R.id.tv_surplus_time);

        //显示固定金额列表
        recyclerView = (RecyclerView) findViewById(R.id.recylerview_fixed_amount);

        //显示自定义购买时间提示
        tvPayTimeTips = (TextView) findViewById(R.id.tv_pay_time_tips);

        //输入自定义购买分钟数
        etInputAmount = (EditText) findViewById(R.id.et_input_amount);

        //立即购买按钮
        btnPayMoney = (TextView) findViewById(R.id.tv_pay_money);

        //查看点数明细
        tvPayDetailed = (TextView) findViewById(R.id.tv_pay_detailed);

        //下划线
        tvPayDetailed.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);

        //首次进入页面，隐藏整体布局。初始化后，页面显示
        rlMainLayout.setVisibility(View.GONE);
    }

    public void initCacheData() {
        List<String> list = new ArrayList<String>();
        list.add("100");
        list.add("null");
        list.add("3000");
        list.add("50000");
        RechargeProductResponse response = new RechargeProductResponse();
        cacheRechargeProductEntry = response.getData();
        cacheRechargeProductEntry.setProducttList(list);
        cacheRechargeProductEntry.setMoneyCeiling(2000);
        cacheRechargeProductEntry.setMoneyFloor(50000);
        cacheRechargeProductEntry.setFeeCode("Live");
        cacheRechargeProductEntry.setFeeName("直播充值");
    }

    private void initData(RechargeProductResponse.RechargeProductEntry rechargeProductEntry) {
        //显示标题栏明细文字入口
        tvDetail.setVisibility(View.VISIBLE);
        tvSurplusTimeContent.setVisibility(View.VISIBLE);
        tvSurplusTimeUnit.setVisibility(View.VISIBLE);
        //首次进入页面，隐藏整体布局。初始化后，页面显示
        rlMainLayout.setVisibility(View.VISIBLE);
        // 固定金额数组
        List<String> fixmoneys = rechargeProductEntry.getProducttList();
        if (fixmoneys != null && fixmoneys.size() > 0) {
            mCurrentPosition = 0;
        }
        fixRechargeAcountAdapter = new FixRechargeAcountAdapter(this, fixmoneys);
        fixRechargeAcountAdapter.setOnRecyclerViewItemClickListener(this);
        fixRechargeAcountAdapter.normalItemChecked();
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        //设置RecycleView显示的方向是水平还是垂直 GridLayout.HORIZONTAL水平  GridLayout.VERTICAL默认垂直
        gridLayoutManager.setOrientation(GridLayout.VERTICAL);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(fixRechargeAcountAdapter);
        recyclerView.addItemDecoration(new SpacingDecoration(AppUtils.dip2px(RechargeTimeAct.this, 15), AppUtils.dip2px(RechargeTimeAct.this, 15), false));
        // 购买分钟数上限
        String payLimeUpLimit = rechargeProductEntry.getMoneyCeiling() + "";
        if ("0".equals(payLimeUpLimit)) {
            tvPayTimeTips.setText(RechargeTimeAct.this.getResources().getString(R.string.sm_mine_recharge_pay_noup_tips));
        } else {
            tvPayTimeTips.setText(String.format(RechargeTimeAct.this.getResources().getString(R.string.sm_mine_recharge_pay_time_tips), payLimeUpLimit) + "");

        }
        etInputAmount.addTextChangedListener(this);
        etInputAmount.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                //重置固定列表选择的postion值
                mCurrentPosition = -1;
                fixRechargeAcountAdapter.clearItemChecked(fixRechargeAcountAdapter.getmLastCheckedPosition());
            }
        });
    }

    private void initUiFail() {
        if (cacheRechargeProductEntry == null) {
            initCacheData();
        }
        rechargeProductEntry = cacheRechargeProductEntry;
        initData(rechargeProductEntry);
    }

    /**
     * 固定金额列表监听
     */
    @Override
    public void onItemClick(View v, int position, String data) {
        //清空文本框
        setInputContent("");
        etInputAmount.clearFocus();
        AppUtils.hideInputMethod(etInputAmount);
        //被选中项 == 当前项 则返回
        if (mCurrentPosition == position) {
            return;
        }
        //被选中项为其他项
        mCurrentPosition = position;
        //更新ui
        fixRechargeAcountAdapter.setItemChecked(position);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    /**
     * 输入框输入内容变化监听
     */
    @Override
    public void afterTextChanged(Editable s) {
        String tmpput = s.toString();
        if (tmpput.length() > 0) {
            //重置固定列表选择的postion值
            mCurrentPosition = -1;
            fixRechargeAcountAdapter.clearItemChecked(fixRechargeAcountAdapter.getmLastCheckedPosition());
            timeRechargePresenter.afterTextChanged(tmpput, rechargeProductEntry.getMoneyCeiling() + "", rechargeProductEntry.getMoneyFloor() + "");
        }
    }

    /**
     * 设置输入框文本内容以及光标位置
     */
    @Override
    public void setInputContent(String content) {
        etInputAmount.setText(content);
        etInputAmount.setSelection(etInputAmount.getText().toString().length());
    }

    /**
     * 立即购买
     */
    @Override
    public void goPayMoney(View view) {
        if (!FastClickAvoidUtil.isDoubleClick()) {
            // 赋值订单金额，根据互斥原则
            String editextContent = etInputAmount.getText().toString();
            // 输入金额取值
            if (!TextUtils.isEmpty(editextContent)) {
                orderTotalPrice = Double.valueOf(editextContent);
            } else if (mCurrentPosition != -1) {
                // 固定列表是否选择，如选则使用固定列表的数值
                if (rechargeProductEntry.getProducttList() != null && rechargeProductEntry.getProducttList().size() > 0) {
                    if (rechargeProductEntry.getProducttList().get(mCurrentPosition) != null && !rechargeProductEntry.getProducttList().get(mCurrentPosition).equals("null")) {
                        orderTotalPrice = Double.valueOf(rechargeProductEntry.getProducttList().get(mCurrentPosition));
                    } else {
                        // 后端配置固定列表金额字段为null时，默认金额为0
                        orderTotalPrice = 0.00;
                    }
                }
            } else {
                // 都没有选择，则默认为0提示输入金额
                orderTotalPrice = 0.00;
                toastShow(this.getResources().getText(R.string.sm_mine_recharge_ed_input_hint).toString());
                return;
            }
            // 立即购买需要判断上下限逻辑
            timeRechargePresenter.goPayMoney(new DecimalFormat("0.00").format(orderTotalPrice), rechargeProductEntry.getMoneyCeiling(), rechargeProductEntry.getMoneyFloor());
        }
    }

    /**
     * 显示确认购买分钟数弹框
     */
    @Override
    public void showSureDialog() {
        // 计算输入金额对应分钟数计算公式：购买分钟数=购买金额／运营后台自定义产品单价；
        DecimalFormat dF = new DecimalFormat("0.00");
        if (rechargeProductEntry.getPrice() != null && !"null".equals(rechargeProductEntry.getPrice()) && rechargeProductEntry.getPrice().doubleValue() != 0) {
            serviceTotalTime = dF.format(Double.valueOf(orderTotalPrice / rechargeProductEntry.getPrice().doubleValue()));
        } else {
            //后端单价没有配置，或配置为0则充值分钟数为0.0分钟
            serviceTotalTime = "0.00";
        }

        String content = String.format(RechargeTimeAct.this.getResources().getString(R.string.sm_mine_recharge_recharge_money), new DecimalFormat("0.00").format(orderTotalPrice), serviceTotalTime) + "";

        if (mTjMakeSureDialog == null || !mTjMakeSureDialog.isShowing()) {
            mTjMakeSureDialog = new TJMakeSureDialog(RechargeTimeAct.this, content,
                    v -> {
                        //获取订单
                        String serviceTime = serviceTotalTime + "";
                        String orderPrice = orderTotalPrice + "";
                        String feeName = rechargeProductEntry.getFeeName();
                        String feePrice = rechargeProductEntry.getPrice() + "";
                        String feeCode = rechargeProductEntry.getFeeCode();

                        //调用订单下单／保存接口
                        queryRechargesaveorder(serviceTime, orderPrice, feeName, feePrice, feeCode);

                        mTjMakeSureDialog.dismiss();

                    }).setBtnText(getApplicationContext().getResources().getString(R.string.sm_mine_recharge_dialog_sure),
                    getApplicationContext().getResources().getString(R.string.sm_mine_recharge_dialog_cancle));
            mTjMakeSureDialog.show();
        }
    }

    /**
     * 请求订单/保存订单接口
     */
    @Override
    public void queryRechargesaveorder(String serviceTime, String orderPrice, String feeName, String feePrice, String feeCode) {
        showProgressHUD(RechargeTimeAct.this, getApplicationContext().getResources().getString(R.string.sm_mine_recharge_dialog_loading));
        //获取订单成功后，会直接在该方法中跳转至支付收银台页面
        timeRechargePresenter.queryRechargesaveorder(serviceTime, orderPrice, feeName, feePrice, feeCode);
    }


    /**
     * 前往购买记录页面
     */
    @Override
    public void goPurchaseRecord(View view) {
        if (!FastClickAvoidUtil.isDoubleClick()) {
            ARouter.getInstance().build(RouterPathProvider.ServiceManager_PurchaseRecordAct).navigation();
        }
    }

    /**
     * 前往使用明细／消费明细
     *
     * @param view
     */
    @Override
    public void goConsumptionDetails(View view) {
        if (!FastClickAvoidUtil.isDoubleClick()) {
            ARouter.getInstance().build(RouterPathProvider.ServiceManager_ConsumptionDetailsAct).navigation();
        }
    }

    /**
     * 外部调用ui隐藏加载中提示
     */
    @Override
    public void hideLoadingView() {
        if (isShowProgressHUD()) {
            dismissProgressHUD();
        }
    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }

    /**
     * 获取剩余分钟数接口
     */
    @Override
    public void getSurplusTimeDetail() {
        timeRechargePresenter.getRemainingTime();
    }

    /**
     * 进入页面初始化时会被调用，接口返回数据后刷新页面。展示
     */
    @Override
    public void getRechargeProductDetail() {
        showProgressHUD(RechargeTimeAct.this, getApplicationContext().getResources().getString(R.string.sm_mine_recharge_dialog_loading));
        timeRechargePresenter.getSurplusTimeDetail();
    }

    /**
     * 充值产品详情初始化数据成功
     */
    @Override
    public void getRechargeProductDetailSuccess(RechargeProductResponse.RechargeProductEntry tmprechargeProductEntry) {
        if (isShowProgressHUD()) {
            dismissProgressHUD();
        }
        rechargeProductEntry = tmprechargeProductEntry;
        //保存缓存数据
        cacheRechargeProductEntry = rechargeProductEntry;
        initData(rechargeProductEntry);
    }

    @Override
    public void getRechargeProductDetailError() {
        if (isShowProgressHUD()) {
            dismissProgressHUD();
        }
        initUiFail();
    }

    /**
     * 剩余充值金额获取成功
     */
    @Override
    public void getSurplusTimeDetailonSuccess(RemainTimeResponse.RemainTimeEntry entry) {
        if (entry != null && entry.getRemainingTime() != null) {
            surplusTimeString = entry.getRemainingTime();
            tvSurplusTime.setText(surplusTimeString + "");
            //免费
            if (entry.getBilling() == 0) {
                rlFeeMain.setVisibility(View.VISIBLE);
                tvFeeIcon.setOnClickListener(v -> {
                    if (isShowFeeContent) {
                        isShowFeeContent = false;
                        tvFeeContent.setVisibility(View.GONE);
                    } else {
                        isShowFeeContent = true;
                        if (entry.getServeEndMessage() != null && !TextUtils.isEmpty(entry.getServeEndMessage())) {
                            tvFeeContent.setVisibility(View.VISIBLE);
                            tvFeeContent.setText(entry.getServeEndMessage());
                        } else {
                            tvFeeContent.setVisibility(View.GONE);
                        }
                    }
                });
            } else {
                //计费
                rlFeeMain.setVisibility(View.INVISIBLE);
            }
        } else {
            tvSurplusTime.setText("--");
        }
    }

    /**
     * 剩余初始化数据失败
     */
    @Override
    public void getSurplusTimeDetailonError() {
        if (surplusTimeString != null) {
            tvSurplusTime.setText(surplusTimeString);
        } else {
            tvSurplusTime.setText("--");
        }
    }

    /**
     * Toast提示框
     *
     * @param msg
     */
    @Override
    public void toastShow(String msg) {
        Toasty.normal(RechargeTimeAct.this, msg).show();
    }

    /**
     * 订单充值事件回调，回到该页面重新获取剩余分钟数
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderChange(OrderChangeEvent event) {
        getSurplusTimeDetail();
    }
}
