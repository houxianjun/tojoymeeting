package com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.model;

import com.tojoy.tjoybaselib.model.servicemanager.RechargeProductResponse;
import com.tojoy.tjoybaselib.model.servicemanager.RechargeSaveOrderResponse;
import com.tojoy.tjoybaselib.model.servicemanager.RemainTimeResponse;

import rx.Observer;

public interface IRechargeTimeModel {


    /**
     * 固定充值金额，上下限信息，产品单价
     * 会议服务管理产品接口
     * @param observer
     */
    void queryManagemenProduct(Observer<RechargeProductResponse> observer);

    /**
     * 获取剩余分钟数
     * @param companyCode 企业编码
     * @param observer
     */
    void queryPlatCorporationRemaining(String companyCode,Observer<RemainTimeResponse> observer);


    /**
     * 会议服务管理：充值下单接口
     * @param serviceTime 10000
     * @param orderPrice  2000
     * @param feeName     测3434343434
     * @param feePrice    0.2
     * @param feeCode     Live
     * @param observer
     */
    void queryRechargesaveorder(String serviceTime,String orderPrice
            ,String feeName,String feePrice,String feeCode,Observer<RechargeSaveOrderResponse> observer);

//    /**
//     * 会议服务管理：充值支付接口
//     * @param userOrderNo
//     * @param paySource
//     * @param observer
//     */
//    void queryRechargePlatformAliWxPay(String userOrderNo,String paySource
//            ,Observer<RechargeProductResponse> observer);
    /**
     *会议服务管理：充值取消订单接口
     * @param id
     * @param observer
     */
    void queryRechargeCancelOrder(String id
            ,Observer<RechargeProductResponse> observer);
//    /**
//     * 会议服务管理：充值订单剩余有效时间
//     * @param userOrderNo
//     * @param observer
//     */
//    void queryRechargeUserPaySurplusTime(String userOrderNo
//            ,Observer<RechargeProductResponse> observer);
//
//    /**
//     * 会议服务管理：充值订单详情接口，返回订单状态
//     * @param id
//     * @param observer
//     */
//    void queryRechargeOrderDetail(String id
//            ,Observer<RechargeProductResponse> observer);
}
