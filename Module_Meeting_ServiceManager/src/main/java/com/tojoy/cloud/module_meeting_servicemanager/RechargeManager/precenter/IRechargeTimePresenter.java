package com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.precenter;

import android.app.Activity;
import android.widget.EditText;

public interface IRechargeTimePresenter {

    /**
     *  固定充值金额，上下限信息
     */
    void getSurplusTimeDetail();

    /**
     * 获取剩余分钟数
     */
    void getRemainingTime();


    /**
     * 会议服务管理：充值下单接口
     * @param serviceTime 10000
     * @param orderPrice  2000
     * @param feeName     测3434343434
     * @param feePrice    0.2
     * @param feeCode     Live
     */

     void queryRechargesaveorder(String serviceTime, String orderPrice, String feeName, String feePrice, String feeCode);
    /**
     * 会议服务管理：充值支付接口
     * @param userOrderNo
     * @param paySource
     */

   //  void queryRechargePlatformAliWxPay(String userOrderNo, String paySource);
    /**
     *会议服务管理：充值取消订单接口
     * @param id
     */

     void queryRechargeCancelOrder(String id);
    /**
     * 会议服务管理：充值订单剩余有效时间
     * @param userOrderNo
     */

   //  void queryRechargeUserPaySurplusTime(String userOrderNo);
    /**
     * 会议服务管理：充值订单详情接口，返回订单状态
     * @param id
     */

    // void queryRechargeOrderDetail(String id);


    /**
     * 立即购买
     */
    void goPayMoney(String inputContent,int up,int down);


    /**
     *
     * @param inputContent
     * @param up 上限
     * @param down 下限
     */
    void afterTextChanged(String inputContent,String up,String down);

    /**
     * 隐藏输入框系统软键盘
     * @param context
     * @param editText
     */
    void hideSoftInput(Activity context, EditText editText);
}
