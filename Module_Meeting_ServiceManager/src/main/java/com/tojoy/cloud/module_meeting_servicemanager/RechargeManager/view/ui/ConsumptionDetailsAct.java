package com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.view.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.servicemanager.UseDataResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.time.TimeUtil;
import com.tojoy.cloud.module_meeting_servicemanager.R;
import com.tojoy.tjoybaselib.configs.UrlProvider;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;


/**
 * 使用明细页面
 */
@Route(path = RouterPathProvider.ServiceManager_ConsumptionDetailsAct)
public class ConsumptionDetailsAct extends UI {
    private TJRecyclerView mRecyclerView;
    private List<UseDataResponse.DataBean.ListBean> mDatas = new ArrayList<>();
    private ConsumptionDetailsAdapter mAdapter;
    private int mPage = 1;

    public static void start(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, ConsumptionDetailsAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicemanager_consumption_details_layout);
        setStatusBar();
        setSwipeBackEnable(false);
        initTitle();
        initUI();
        initData();
    }

    private void initTitle() {
        setUpNavigationBar();
        setTitle(this.getResources().getText(R.string.sm_mine_recharge_consumption_details));
        showBack();
        setRightManagerTitle("说明");
        changeRightManagerTitleStatus(true);
    }

    @Override
    protected void onRightManagerTitleClick() {
        if (!FastClickAvoidUtil.isDoubleClick()) {
            ARouter.getInstance().build(RouterPathProvider.WEBNew)
                    .withString("url", UrlProvider.Usage_Details)
                    .withString("title", "")
                    .navigation();
        }
    }

    private void initUI() {
        mRecyclerView = (TJRecyclerView) findViewById(R.id.recyclerView);
        mAdapter = new ConsumptionDetailsAdapter(this, mDatas);
        mRecyclerView.setLoadMoreViewWithHide();
        mRecyclerView.setEmptyImgAndTip(R.drawable.icon_empty_online_logo, "暂无数据");
        mRecyclerView.autoRefresh();
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                initData();
            }

            @Override
            public void onLoadMore() {
                mPage++;
                initData();
            }
        });
    }


    private void initData() {
        OMAppApiProvider.getInstance().useData(mPage + "", "20", new Observer<UseDataResponse>() {
            @Override
            public void onCompleted() {
                mRecyclerView.stopRefresh();
            }

            @Override
            public void onError(Throwable e) {
                Toasty.normal(ConsumptionDetailsAct.this, "网络错误，请检查网络").show();
            }

            @Override
            public void onNext(UseDataResponse response) {
                if (response.isSuccess()) {
                    List<UseDataResponse.DataBean.ListBean> list = response.data.list;
                    if (mPage == 1) {
                        mDatas.clear();
                        mDatas.addAll(list);
                        mAdapter.updateData(mDatas);
                    } else {
                        mDatas.addAll(list);
                        mAdapter.addMore(mDatas);
                    }
                    mRecyclerView.refreshLoadMoreView(mPage, list.size());

                } else {
                    Toasty.normal(ConsumptionDetailsAct.this, response.msg).show();
                }
            }
        });

    }

    private class ConsumptionDetailsAdapter extends BaseRecyclerViewAdapter<UseDataResponse.DataBean.ListBean> {

        ConsumptionDetailsAdapter(@NonNull Context context, @NonNull List<UseDataResponse.DataBean.ListBean> datas) {
            super(context, datas);
        }

        @Override
        protected int getViewType(int position, @NonNull UseDataResponse.DataBean.ListBean data) {
            return 0;
        }

        @Override
        protected int getLayoutResId(int viewType) {
            return R.layout.sm_item_consumption_detail;
        }

        @Override
        protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
            return new ConsumptionDetailsVH(ConsumptionDetailsAct.this, parent, getLayoutResId(viewType));
        }
    }

    private class ConsumptionDetailsVH extends BaseRecyclerViewHolder<UseDataResponse.DataBean.ListBean> {

        private TextView tvTitle;
        private TextView tvTime;
        private TextView tvMeetingID;
        private TextView tvChargingTime;
        private TextView tvStartTime;

        ConsumptionDetailsVH(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            bindViews();
        }

        private void bindViews() {

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvMeetingID = itemView.findViewById(R.id.tvMeetingID);
            tvChargingTime = itemView.findViewById(R.id.tvChargingTime);
            tvStartTime = itemView.findViewById(R.id.tvStartTime);
        }

        @Override
        public void onBindData(UseDataResponse.DataBean.ListBean data, int position) {

            tvTitle.setText(data.feeName);
            tvTime.setText(data.getTotalTime());
            tvMeetingID.setText("会议ID：" + data.meetingId);
            tvChargingTime.setText("扣费时间：" + TimeUtil.getLoneTimeStr(data.deductionFeeTime));

            if (TimeUtil.isSameDay(data.meetingStartTime, data.meetingEndTime)) {

                String endTimeStr = TimeUtil.getLoneTimeStr(data.meetingEndTime);

                tvStartTime.setText("起始时间：" + TimeUtil.getLoneTimeStr(data.meetingStartTime)
                        + "-" + endTimeStr.split(" ")[1]);
            } else {
                tvStartTime.setText("起始时间：" + TimeUtil.getLoneTimeStr(data.meetingStartTime)
                        + "-" + TimeUtil.getLoneTimeStr(data.meetingEndTime));
            }
        }
    }

}
