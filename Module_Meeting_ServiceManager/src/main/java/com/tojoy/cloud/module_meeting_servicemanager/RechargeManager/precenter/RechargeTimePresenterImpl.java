package com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.precenter;


import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;

import com.tojoy.tjoybaselib.BaseLibKit;
import com.tojoy.tjoybaselib.model.servicemanager.RechargeProductResponse;
import com.tojoy.tjoybaselib.model.servicemanager.RechargeSaveOrderResponse;
import com.tojoy.tjoybaselib.model.servicemanager.RemainTimeResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.cloud.module_meeting_servicemanager.R;
import com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.model.IRechargeTimeModel;
import com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.model.RechargeTimeModelImpl;
import com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.view.ui.IRechargeTimeView;

import java.lang.reflect.Method;

import rx.Observer;

import static com.tojoy.tjoybaselib.util.router.RouterJumpUtil.SKIP_FROM_RECHARGE_TIME;

public class RechargeTimePresenterImpl implements IRechargeTimePresenter {

    /**
     * 数据变化，页面展现逻辑处理类
     */
    private IRechargeTimeView miRechargeTimeView;

    /**
     * 网络请求逻辑处理类
     */
    private IRechargeTimeModel miRechargeTimeModel;

    private Context mContext;

    public RechargeTimePresenterImpl(IRechargeTimeView iRechargeTimeView) {
        this.miRechargeTimeView = iRechargeTimeView;
        miRechargeTimeModel = new RechargeTimeModelImpl();
        mContext = this.miRechargeTimeView.getContext();
    }

    /**
     * 获取初始化信息：固定充值金额，上下限金额,产品单价
     */
    @Override
    public void getSurplusTimeDetail() {
        miRechargeTimeModel.queryManagemenProduct(new Observer<RechargeProductResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                //接口异常，使用缓存数据
                miRechargeTimeView.getRechargeProductDetailError();
                miRechargeTimeView.toastShow(mContext.getResources().getText(R.string.sm_mine_recharge_network_timeout).toString());
            }

            @Override
            public void onNext(RechargeProductResponse entry) {
                if (entry != null && entry.isSuccess() && entry.getData() != null) {
                    miRechargeTimeView.getRechargeProductDetailSuccess(entry.getData());
                } else {
                    miRechargeTimeView.getRechargeProductDetailError();
                    miRechargeTimeView.toastShow((entry != null && !TextUtils.isEmpty(entry.msg)) ? entry.msg : mContext.getResources().getText(R.string.sm_mine_recharge_service_error).toString());
                }
            }
        });
    }

    /**
     * 获取剩余分钟数
     */
    @Override
    public void getRemainingTime() {
        String companyCode = BaseUserInfoCache.getCompanyCode(BaseLibKit.getContext());
        miRechargeTimeModel.queryPlatCorporationRemaining(companyCode, new Observer<RemainTimeResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                miRechargeTimeView.getSurplusTimeDetailonError();
                miRechargeTimeView.toastShow(mContext.getResources().getText(R.string.sm_mine_recharge_network_timeout).toString());
            }

            @Override
            public void onNext(RemainTimeResponse entry) {
                if (entry != null && entry.isSuccess() && entry.getData() != null) {
                    miRechargeTimeView.getSurplusTimeDetailonSuccess(entry.getData());
                } else {
                    miRechargeTimeView.getSurplusTimeDetailonError();
                    miRechargeTimeView.toastShow((entry != null && !TextUtils.isEmpty(entry.msg)) ? entry.msg : mContext.getResources().getText(R.string.sm_mine_recharge_service_error).toString());
                }
            }
        });
    }

    /**
     * @param serviceTime 10000
     * @param orderPrice  2000
     * @param feeName     测3434343434
     * @param feePrice    0.2
     * @param feeCode     Live
     */
    @Override
    public void queryRechargesaveorder(String serviceTime, String orderPrice, String feeName, String feePrice, String feeCode) {
        /**
         * code 返回状态
         * 2, "当前服务价格变更为￥"
         * 10, "购买金额大于金额上限:
         * 11, "购买金额小于金额下限:
         * 12, "应付金额不正确"
         */
        miRechargeTimeModel.queryRechargesaveorder(serviceTime, orderPrice, feeName, feePrice, feeCode, new Observer<RechargeSaveOrderResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                miRechargeTimeView.hideLoadingView();
                miRechargeTimeView.toastShow(mContext.getResources().getText(R.string.sm_mine_recharge_network_timeout).toString());
            }

            @Override
            public void onNext(RechargeSaveOrderResponse rechargeProductResponse) {
                if (rechargeProductResponse != null && rechargeProductResponse.isSuccess()) {
                    miRechargeTimeView.hideLoadingView();
                    RechargeSaveOrderResponse.RechargeSaveOrderEntry entry = rechargeProductResponse.getData();
                    RouterJumpUtil.startOrderPayDetailAct(entry.getOrderId(), entry.getOrderCode(), orderPrice, SKIP_FROM_RECHARGE_TIME);
                } else {
                    miRechargeTimeView.hideLoadingView();
                    miRechargeTimeView.toastShow((rechargeProductResponse != null && !TextUtils.isEmpty(rechargeProductResponse.msg)) ? rechargeProductResponse.msg : mContext.getResources().getText(R.string.sm_mine_recharge_service_error).toString());
                }
            }
        });
    }

    /**
     * @param id
     */
    @Override
    public void queryRechargeCancelOrder(String id) {

    }

    @Override
    public void goPayMoney(String inputContent, int up, int down) {
        if (Double.valueOf(inputContent) <= 0) {
            miRechargeTimeView.toastShow(mContext.getResources().getText(R.string.sm_mine_recharge_ed_input_hint).toString());
            return;
        }
        //判断输入的金额格式是否正确
        if (!TextUtils.isEmpty(inputContent)) {
            //有小数点
            if (inputContent.contains(".")) {
                //且小数点不最后一位,判断上下限
                if (inputContent.lastIndexOf(".") != inputContent.length() - 1) {
                    //上限不为0走判断逻辑,后端可能没有设置上下限，返回数据是null,则默认是为0
                    if (!upperAndLowerLimitLogic(inputContent, up, down)) {
                        return;
                    }
                }
                //是最后一位则需要提示用户输入正确格式
                else {
                    miRechargeTimeView.toastShow(mContext.getResources().getText(R.string.sm_mine_recharge_ed_input_hint).toString());
                    return;
                }
            } else {
                //没有小数点
                if (Double.valueOf(inputContent) == 0) {
                    miRechargeTimeView.toastShow(mContext.getResources().getText(R.string.sm_mine_recharge_ed_input_hint).toString());
                    return;
                }
                // 上限不为0走判断逻辑,后端可能没有设置上下限，返回数据是null,则默认是为0
                if (!upperAndLowerLimitLogic(inputContent, up, down)) {
                    return;
                }
            }
        }
        //显示确认弹框
        miRechargeTimeView.showSureDialog();
    }

    /**
     * 输入内容是否在后台配置的上下限范围内，在则返回true继续执行，否则返回false不往下执行
     */
    private boolean upperAndLowerLimitLogic(String inputContent, int up, int down) {
        //上限不为0走判断逻辑,后端可能没有设置上下限，返回数据是null,则默认是为0
        if ((double) up != 0) {
            if (Double.valueOf(inputContent) > (double) up) {
                miRechargeTimeView.toastShow(mContext.getResources().getText(R.string.sm_mine_recharge_up_limit).toString() + up +
                        mContext.getResources().getText(R.string.sm_mine_recharge_unit).toString());
                return false;
            } else if (Double.valueOf(inputContent) < (double) down) {
                miRechargeTimeView.toastShow(mContext.getResources().getText(R.string.sm_mine_recharge_lower_limit).toString() + down +
                        mContext.getResources().getText(R.string.sm_mine_recharge_unit).toString());
                return false;
            }
        }
        return true;
    }

    private static int counter = 0;

    /**
     * 文本框输入内容变化
     */
    @Override
    public void afterTextChanged(String inputContent, String up, String down) {
        counter = 0;
        //判断输入的金额格式是否正确
        if (!TextUtils.isEmpty(inputContent)) {
            afterTextChangedLogic(inputContent);
        }
    }

    /**
     * 输入文字的展示逻辑
     *
     * @param tmpinput
     */
    private void afterTextChangedLogic(String tmpinput) {
        int len = tmpinput.length();
        //输入金额为长度为1
        if (len == 1) {
            //有小数点
            if (tmpinput.contains(".")) {
                miRechargeTimeView.setInputContent("");
            }
        } else if (len >= 2) {
            int index = tmpinput.indexOf(".");
            //有小数点
            if (tmpinput.contains(".")) {
                int tmpcounter = countStr(tmpinput, ".");
                if (tmpcounter >= 2) {
                    tmpinput = tmpinput.substring(0, len - 1);
                    miRechargeTimeView.setInputContent(tmpinput);
                } else if (index < len - 3) {
                    miRechargeTimeView.setInputContent(tmpinput.substring(0, len - 1));
                }
            } else if (Double.valueOf(tmpinput.substring(0, 1)) == 0) {
                String tmp = tmpinput.substring(0, 1);
                miRechargeTimeView.setInputContent(tmp);
            }
        }
        //重置小数点个数
        counter = 0;
    }

    /**
     * 递归判断str1中包含str2的个数
     */
    private int countStr(String str1, String str2) {
        if (!str1.contains(str2)) {
            return 0;
        } else if (str1.contains(str2)) {
            counter++;
            countStr(str1.substring(str1.indexOf(str2) +
                    str2.length()), str2);
            return counter;
        }
        return 0;
    }

    /**
     * editText隐藏系统软键盘
     */
    @Override
    public void hideSoftInput(Activity context, EditText editText){
        Class<EditText> cls = EditText.class;
        Method method;
        try {
            method = cls.getMethod("setShowSoftInputOnFocus", boolean.class);
            method.setAccessible(true);
            method.invoke(editText, false);
        } catch (Exception e) {
        }
        try {
            method = cls.getMethod("setSoftInputShownOnFocus", boolean.class);
            method.setAccessible(true);
            method.invoke(editText, false);
        } catch (Exception e) {
        }
    }

}
