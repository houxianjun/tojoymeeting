package com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.view.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.TJTablayout.SlidingTabLayout;
import com.tojoy.cloud.module_meeting_servicemanager.R;
import com.tojoy.cloud.module_shop.Order.OrderDetail.Model.OrderChangeEvent;
import com.tojoy.cloud.module_shop.Order.utils.OrderConstants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;


/**
 * 会议服务管理：购买记录-我的订单页面
 */
@Route(path = RouterPathProvider.ServiceManager_PurchaseRecordAct)
public class PurchaseRecordAct extends UI {

    private ViewPager mViewPager;
    private String[] mTitles;
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyOrdersFragment allOrderFragment;
    private MyOrdersFragment noPayFragment;
    @Autowired(name = "isPaySuccess")
    public String isPaySuccess;

    public static void start(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, PurchaseRecordAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicemanager_purchase_record_layout);
        ARouter.getInstance().inject(this);
        EventBus.getDefault().register(this);
        setStatusBar();
        setSwipeBackEnable(false);
        initTitle();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initTitle() {
        setUpNavigationBar();
        setTitle(this.getResources().getText(R.string.sm_mine_purchase_record));
        showBack();

        initData();
        initUI();
    }

    private void initData() {
        mTitles = new String[3];
        mTitles[0] = "全部";
        mTitles[1] = "待付款";
        mTitles[2] = "已完成";

        allOrderFragment = new MyOrdersFragment("");
        mFragments.add(allOrderFragment);
        noPayFragment = new MyOrdersFragment("1");
        mFragments.add(noPayFragment);
        mFragments.add(new MyOrdersFragment("3"));
    }

    private void initUI() {
        SlidingTabLayout mTabLayout = (SlidingTabLayout) findViewById(R.id.tabLayot);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mTabLayout.setTabSpaceEqual(true);
        mTabLayout.setTextBold(2);
        mTabLayout.setIndicatorMargin(0, 0, 0, 8);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                return mTitles[position];
            }

            @Override
            public int getCount() {
                return mTitles.length;
            }

            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }
        });
        mTabLayout.setViewPager(mViewPager, mTitles);
        mViewPager.setCurrentItem(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void onOrderCancel(String tabType) {
        if ("1".equals(tabType)) {
            noPayFragment.refreshCancel();
        } else {
            allOrderFragment.refreshCancel();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderChange(OrderChangeEvent event) {
        switch (event.changeType) {
            case OrderConstants.PAY_STATUS_SUCCESS:
                mViewPager.setCurrentItem(0);
                break;
        }
    }

}