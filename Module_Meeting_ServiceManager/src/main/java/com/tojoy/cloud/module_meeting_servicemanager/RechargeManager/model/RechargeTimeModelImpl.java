package com.tojoy.cloud.module_meeting_servicemanager.RechargeManager.model;

import com.tojoy.tjoybaselib.model.servicemanager.RechargeProductResponse;
import com.tojoy.tjoybaselib.model.servicemanager.RechargeSaveOrderResponse;
import com.tojoy.tjoybaselib.model.servicemanager.RemainTimeResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;

import rx.Observer;

public class RechargeTimeModelImpl implements IRechargeTimeModel {
    /**
     * 固定充值金额，上下限信息，产品单价
     * 会议服务管理产品接口
     * @param observer
     */
    @Override
    public void queryManagemenProduct(Observer<RechargeProductResponse> observer) {

        OMAppApiProvider.getInstance().queryManagemenProduct(observer);

    }
    /**
     * 获取剩余分钟数
     * @param companyCode 企业编码
     * @param observer
     */
    @Override
    public void queryPlatCorporationRemaining(String companyCode, Observer<RemainTimeResponse> observer) {
        OMAppApiProvider.getInstance().queryRechargePlatCorporationRemaining(companyCode,observer);
    }
    /**
     * 会议服务管理：充值下单接口
     * @param serviceTime 10000
     * @param orderPrice  2000
     * @param feeName     测3434343434
     * @param feePrice    0.2
     * @param feeCode     Live
     * @param observer
     */
    @Override
    public void queryRechargesaveorder(String serviceTime, String orderPrice, String feeName, String feePrice, String feeCode, Observer<RechargeSaveOrderResponse> observer) {
        OMAppApiProvider.getInstance().queryRechargesaveorder(serviceTime, orderPrice, feeName, feePrice, feeCode, observer);
    }

    /**
     *会议服务管理：充值取消订单接口
     * @param id
     * @param observer
     */
    @Override
    public void queryRechargeCancelOrder(String id, Observer<RechargeProductResponse> observer) {
        OMAppApiProvider.getInstance().queryRechargeCancelOrder(id, observer);
    }

}
