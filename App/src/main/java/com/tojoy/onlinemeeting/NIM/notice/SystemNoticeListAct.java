package com.tojoy.onlinemeeting.NIM.notice;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.RelativeLayout;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.netease.nim.uikit.api.model.user.SystemMsgExtraPraser;
import com.netease.nim.uikit.api.model.user.SystemNotice;
import com.netease.nim.uikit.api.model.user.SystenMsgExtra;
import com.netease.nim.uikit.business.liveroom.JoinLiveRoomHelper;
import com.netease.nim.uikit.business.liveroom.JoinLiveRoomListener;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.onlinemeeting.R;
import com.tojoy.onlinemeeting.Service.SystemNotice.SystemMsgDBManager;
import com.tojoy.common.Services.Push.SystemNoticeType;
import com.tojoy.common.Services.Router.AppRouter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 消息页面
 * Created by daibin on 2019/11/13
 */

@Route(path = RouterPathProvider.SystemNotice)
public class SystemNoticeListAct extends UI {

    @BindView(R.id.container_layout)
    RelativeLayout mContainerLayout;

    int mPage = 1;
    TJRecyclerView mRecyclerView;
    SystemNoticeAdapter mAdapter;

    public ArrayList<SystemNotice> mSystemNoticeList = new ArrayList<>();
    public ArrayList<SystenMsgExtra> mSystemMsgExtraList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_activity_recycler_layout);
        setStatusBar();
        ButterKnife.bind(this);

        initTitle();
        initViews();
        initData();

        SystemMsgDBManager.getInterface().updateNoticeReadedByType(this, SystemNoticeType.SYSTEM_NOTICE);
    }

    /**
     * 设置Title
     *
     * @return
     */
    private void initTitle() {
        setUpNavigationBar();
        setTitle("系统消息");
        hideBottomLine();
        showBack();
    }


    /**
     * 配置UI控件
     */
    private void initViews() {
        mRecyclerView = new TJRecyclerView(this);
        mRecyclerView.setLoadMoreViewWithHide();
        mRecyclerView.setEmptyImgAndTip(com.tojoy.cloud.online_meeting.R.drawable.icon_empty_online_logo, "暂无消息");
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                requestSystemMsgs();
            }

            @Override
            public void onLoadMore() {
                if (mSystemNoticeList.isEmpty()) {
                    return;
                }
                mPage++;
                requestSystemMsgs();
            }
        });
        mAdapter = new SystemNoticeAdapter(this, mSystemNoticeList);
        mRecyclerView.setAdapter(mAdapter);
        mContainerLayout.addView(mRecyclerView);

        if (mRightManagerTv != null) {
            mRightManagerTv.setOnClickListener(v -> {//清空系统消息
                mPage = 1;
                requestSystemMsgs();
            });
        }

        mAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }

            if ("h5".equals(mSystemMsgExtraList.get(position).module)) {
                AppRouter.doJump(this, AppRouter.SkipType_H5, "", "", mSystemMsgExtraList.get(position).moduleDetail, "");
            } else if ("netMeetingCheck".equals(mSystemMsgExtraList.get(position).module)) {
                ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_MY_APPROVAL_LIST)
                        .navigation();
            } else if ("netMeetingApply".equals(mSystemMsgExtraList.get(position).module)) {
                ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_START_LIVE_BROADCAST_APPLY)
                        .withString("source", "menu")
                        .navigation();
            } else if ("netMeetingLiveRoom".equals(mSystemMsgExtraList.get(position).module)) {
                JoinLiveRoomHelper.goToLiveRoom(SystemNoticeListAct.this, mSystemMsgExtraList.get(position).moduleDetail, "",new JoinLiveRoomListener() {
                    @Override
                    public void onJoinError(String error, int errorCode) {
                    }

                    @Override
                    public void onJoinSuccess() {
                    }
                });
            } else if ("meetingServiceManager".equals(mSystemMsgExtraList.get(position).module)) {
                // 跳转会议服务页面进行余额充值
                ARouter.getInstance().build(RouterPathProvider.ServiceManager_RechargeTimeAct).navigation();
            }

        });
    }

    private void initData() {
        requestSystemMsgs();
    }

    private void requestSystemMsgs() {
        List<SystemNotice> resultList = SystemMsgDBManager.getInterface().getNoticeListByType(this, mPage, SystemNoticeType.SYSTEM_NOTICE);

        if (mPage == 1) {
            mSystemNoticeList.clear();
            mSystemMsgExtraList.clear();
        }
        //扩展数据
        for (SystemNotice notice : resultList) {
            if (!TextUtils.isEmpty(notice.extra)) {
                mSystemMsgExtraList.add(new SystemMsgExtraPraser(notice.extra.getBytes()).prase());
            }
        }
        mAdapter.setmSystemMsgExtraList(mSystemMsgExtraList);

        //内容数据
        mSystemNoticeList.addAll(resultList);

        if (mPage == 1) {
            mAdapter.updateData(mSystemNoticeList);
        } else {
            mAdapter.addMore(mSystemNoticeList);
        }

        mRecyclerView.showFooterView();
        mRecyclerView.refreshLoadMoreView(mPage, resultList.size());
    }

}
