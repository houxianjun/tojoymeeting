package com.tojoy.onlinemeeting.NIM.session;

import android.content.Context;

import com.netease.nim.uikit.api.NimUIKit;
import com.netease.nim.uikit.api.model.session.SessionEventListener;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachParser;
import com.netease.nim.uikit.business.p2p.extension.CustomAttachment;
import com.netease.nim.uikit.business.p2p.viewholder.MsgViewHolderDefCustom;
import com.netease.nim.uikit.business.p2p.viewholder.MsgViewHolderTip;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.model.IMMessage;

/**
 * Created by chengyanfang on 2017/8/23.
 *
 * @version V1.0.0
 * @function:UIKit自定义消息界面用法展示类
 */

public class SessionHelper {

    public static void init() {
        // 注册自定义消息附件解析器
        NIMClient.getService(MsgService.class).registerCustomAttachmentParser(new CustomAttachParser());
        // 注册各种扩展消息类型的显示ViewHolder
        registerViewHolders();
        // 设置会话中点击事件响应处理
        setSessionClickListener();
    }

    /**
     * 注册各种扩展消息类型的显示ViewHolder
     */
    private static void registerViewHolders() {
        NimUIKit.registerMsgItemViewHolder(CustomAttachment.class, MsgViewHolderDefCustom.class);

        NimUIKit.registerTipMsgViewHolder(MsgViewHolderTip.class);

    }

    /**
     * 设置会话中点击事件响应处理
     */
    private static void setSessionClickListener() {
        SessionEventListener listener = new SessionEventListener() {
            @Override
            public void onAvatarClicked(Context context, IMMessage message) {
            }

            @Override
            public void onAvatarLongClicked(Context context, IMMessage message) {
                // 一般用于群组@功能，或者弹出菜单，做拉黑，加好友等功能
            }

            @Override
            public void onAckMsgClicked(Context context, IMMessage message) {
            }
        };

        NimUIKit.setSessionListener(listener);
    }
}
