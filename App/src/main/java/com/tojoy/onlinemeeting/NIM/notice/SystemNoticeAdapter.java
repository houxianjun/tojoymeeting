package com.tojoy.onlinemeeting.NIM.notice;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.netease.nim.uikit.api.model.user.SystemNotice;
import com.netease.nim.uikit.api.model.user.SystenMsgExtra;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.util.time.TimeUtility;
import com.tojoy.onlinemeeting.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.tojoy.tjoybaselib.util.time.TimeUtility.YEAR_HOUR_Notice;

/**
 * Created by chengyanfang on 2017/10/24.
 */

public class SystemNoticeAdapter extends BaseRecyclerViewAdapter<SystemNotice> {

    public ArrayList<SystenMsgExtra> mSystemMsgExtraList = new ArrayList<>();

    public SystemNoticeAdapter(@NonNull Context context, @NonNull List<SystemNotice> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull SystemNotice data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_system_notce;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        SystemNoticeViewHolder systemNoticeViewHolder = new SystemNoticeViewHolder(context, parent, getLayoutResId(viewType));
        return systemNoticeViewHolder;
    }


    public void setmSystemMsgExtraList(ArrayList<SystenMsgExtra> systemMsgExtraList) {
        this.mSystemMsgExtraList = systemMsgExtraList;
    }

    @Override
    protected void onBindData(BaseRecyclerViewHolder<SystemNotice> holder, SystemNotice data, int position) {
        super.onBindData(holder, data, position);
    }

    /**
     * ViewHolder
     */
    public class SystemNoticeViewHolder extends BaseRecyclerViewHolder<SystemNotice> {

        @BindView(R.id.tv_notice_time)
        TextView systemTime;

        @BindView(R.id.tv_notice_desc)
        TextView content;

        public SystemNoticeViewHolder(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onBindData(SystemNotice systemNotice, int position) {
            systemTime.setText(TimeUtility.getTimeFormat(Long.parseLong(systemNotice.timestamp), YEAR_HOUR_Notice));

            if (!TextUtils.isEmpty(systemNotice.content)) {

                if ("meetingServiceManager".equals(mSystemMsgExtraList.get(position).module)) {
                    SpannableString spannableString = new SpannableString(systemNotice.content + "去购买 >");
                    ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.parseColor("#2476ed"));
                    spannableString.setSpan(colorSpan, spannableString.length() - 5, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                    content.setText(spannableString);
                } else {
                    content.setText(systemNotice.content);
                }

                content.setVisibility(View.VISIBLE);
            } else {
                content.setVisibility(View.GONE);
            }

        }
    }
}
