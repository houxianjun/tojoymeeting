package com.tojoy.onlinemeeting.NIM.reminder;

/**
 * Created by chengyanfang on 2017/8/29.
 */

public class ReminderId {
    final static public int SESSION = 0;
    final static public int CONTACT = 1;

    final static public int GROUP = 3;
    final static public int FRIEND = 4;
}
