package com.tojoy.onlinemeeting.NIM.reminder;

/**
 * Created by chengyanfang on 2017/8/29.
 */

public class SystemMessageUnreadManager {
    private static SystemMessageUnreadManager instance = new SystemMessageUnreadManager();

    public static SystemMessageUnreadManager getInstance() {
        return instance;
    }

    private int sysMsgUnreadCount = 0;

    public int getSysMsgUnreadCount() {
        return sysMsgUnreadCount;
    }

    public synchronized void setSysMsgUnreadCount(int unreadCount) {
        this.sysMsgUnreadCount = unreadCount;
    }

    //=======================================有关群的系统通知数===============================
    private int sysMsgUnreadCount_group = 0;

    public int getSysMsgUnreadCount_group() {
        return sysMsgUnreadCount_group;
    }

    public synchronized void setSysMsgUnreadCount_group(int unreadCount) {
        this.sysMsgUnreadCount_group = unreadCount;
    }


    //=======================================有关个人（加删好友）的系统通知数===============================
    private int sysMsgUnreadCount_friend = 0;

    public int getSysMsgUnreadCount_friend() {
        return sysMsgUnreadCount_friend;
    }

    public synchronized void setSysMsgUnreadCount_friend(int unreadCount) {
        this.sysMsgUnreadCount_friend = unreadCount;
    }

}
