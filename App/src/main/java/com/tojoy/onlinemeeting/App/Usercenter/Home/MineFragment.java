package com.tojoy.onlinemeeting.App.Usercenter.Home;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.configs.UserRoles;
import com.tojoy.tjoybaselib.model.home.HomeReadCountResponse;
import com.tojoy.tjoybaselib.model.home.MineDistributionResponse;
import com.tojoy.tjoybaselib.model.user.LoginResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.media.ImageUtil;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.onlinemeeting.App.Usercenter.Setting.Controller.EditMyBaseinfoAct;
import com.tojoy.onlinemeeting.App.Usercenter.Setting.Controller.SettingAct;
import com.tojoy.onlinemeeting.R;
import com.tojoy.onlinemeeting.Service.Address.Controller.AdressManagerActivity;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;
import com.tojoy.cloud.online_meeting.App.Distribution.view.dialog.ApplyDistributionPopView;
import com.tojoy.cloud.online_meeting.App.EnterpriseEditionHome.activity.CustomizedEnterprisePageAct;
import com.tojoy.cloud.online_meeting.App.UserCenter.activity.MyLiveRoomListAct;
import com.tojoy.common.App.BaseFragment;
import com.tojoy.image.photobrowser.PhotoBrowseActivity;
import com.tojoy.image.photobrowser.PhotoBrowseInfo;

import java.util.ArrayList;

import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * Created by luuzhu on 2018/9/11.
 * V3.0.0 我的
 */
public class MineFragment extends BaseFragment {
    private RelativeLayout mHeaderBgRlv;
    private RelativeLayout mHeaderLableRlv;
    private ImageView mHeaderImg;
    private TextView mNickName;
    private TextView tvUserId, mTvUserIdTop;
    private TextView mTvCompanyName;
    private TextView mPowerUnReadTv;

    // 展示我的会员数据
    TextView myVipUpdateTodayTv;
    TextView myVipTeamCountTv;
    TextView myVipAllCountTv;

    // 展示我的收益数据
    TextView myProfitTodayTv;
    TextView myProfitMonthTv;
    TextView myProfitAllTv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_mine_layout, container, false);
        }
        ButterKnife.bind(mView);
        return mView;
    }

    @Override
    protected void lazyLoad() {
        super.lazyLoad();
        updateUserInfo();
        updateDistributionData();
    }

    @SuppressLint("HardwareIds")
    @Override
    protected void initUI() {
        super.initUI();

        mHeaderImg = mView.findViewById(R.id.iv_head);
        mNickName = mView.findViewById(R.id.tv_name);
        tvUserId = mView.findViewById(R.id.tv_user_id);
        mTvCompanyName = mView.findViewById(R.id.tv_company_name);
        mTvUserIdTop = mView.findViewById(R.id.tv_user_id_top);
        mPowerUnReadTv = mView.findViewById(R.id.tv_power_unread_msg_count);
        mHeaderBgRlv = mView.findViewById(R.id.rl_head);
        mHeaderLableRlv = mView.findViewById(R.id.rlv_header_lable);
        // 展示我的会员数据
        myVipUpdateTodayTv = mView.findViewById(R.id.tv_my_vip_update_today);
        myVipTeamCountTv = mView.findViewById(R.id.tv_my_vip_team_count);
        myVipAllCountTv = mView.findViewById(R.id.tv_my_vip_all_count);

        // 展示我的收益数据
        myProfitTodayTv = mView.findViewById(R.id.tv_my_profit_today);
        myProfitMonthTv = mView.findViewById(R.id.tv_my_profit_month);
        myProfitAllTv = mView.findViewById(R.id.tv_my_profit_all);

        //除头像区域
        mView.findViewById(R.id.rlv_userinfo).setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                EditMyBaseinfoAct.start(getActivity());
            }
        });

        //头像点击
        mHeaderImg.setOnClickListener(v -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }

            if ("http://".equals(UserInfoManager.getUserHeaderPic(getActivity()))
                    || "https://".equals(UserInfoManager.getUserHeaderPic(getActivity()))) {
                return;
            }
            ArrayList<String> imgs = new ArrayList<>();
            imgs.add(UserInfoManager.getUserHeaderPic(getActivity()));

            ArrayList<Rect> rects = new ArrayList<>();
            rects.add(ImageUtil.getDrawableBoundsInView(mHeaderImg));

            PhotoBrowseInfo info = PhotoBrowseInfo.create(imgs, rects, 0);
            PhotoBrowseActivity.startToPhotoBrowseActivity(getActivity(), info);
        });

        mView.findViewById(R.id.my_order).setOnClickListener(v -> {
            RouterJumpUtil.startOrderListAct(0);
        });
        //跳转至会议服务管理页面：充值
        mView.findViewById(R.id.mine_meeting_service_manager).setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                ARouter.getInstance().build(RouterPathProvider.ServiceManager_RechargeTimeAct).navigation();
            }
        });
        mView.findViewById(R.id.my_meeting).setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                MyLiveRoomListAct.start(getActivity(), true);
            }
        });
        mView.findViewById(R.id.meeting_record).setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                MyLiveRoomListAct.start(getActivity(), false);
            }
        });
        mView.findViewById(R.id.my_enter).setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                CustomizedEnterprisePageAct.start(getContext(), BaseUserInfoCache.getCompanyCode(getContext()));
            }
        });
        mView.findViewById(R.id.my_enter_viewrecord).setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                ARouter.getInstance().build(RouterPathProvider.BrowsedEnterprise).navigation();
            }
        });
        mView.findViewById(R.id.enter_manager).setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                if (UserInfoManager.isHasCompany(getContext())) {
                    //企业管理页面跳转
                    ARouter.getInstance().build(RouterPathProvider.EnterpriseStructHome)
                            .navigation();
                } else {
                    //申请企业入驻页面
                    ARouter.getInstance().build(RouterPathProvider.ApplyEnterprise)
                            .navigation();
                }
            }
        });
        mView.findViewById(R.id.enter_address).setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                AdressManagerActivity.start(getContext(), "3");
            }
        });
        mView.findViewById(R.id.enter_setting).setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                SettingAct.start(getActivity());
            }
        });

        mView.findViewById(R.id.my_power).setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                ARouter.getInstance().build(RouterPathProvider.ONLINE_MEETING_MY_APPROVAL_LIST)
                        .navigation();
            }
        });

        mView.findViewById(R.id.rlv_my_vip).setOnClickListener(v -> {
            // 我的会员
            if (!FastClickAvoidUtil.isDoubleClick()) {
                ARouter.getInstance().build(RouterPathProvider.Distribution_MyMemberAct)
                        .navigation();
            }
        });
        mView.findViewById(R.id.rlv_my_profit).setOnClickListener(v -> {
            // 我的收益
            if (!FastClickAvoidUtil.isDoubleClick()) {
                ARouter.getInstance().build(RouterPathProvider.Distribution_MyProfitAct)
                        .navigation();
            }
        });
        mView.findViewById(R.id.rlv_apply_extension_bt).setOnClickListener(v -> {
            // 申请成为推广大使
            if (!FastClickAvoidUtil.isDoubleClick()) {
                ApplyDistributionPopView applyDistributionPopView = new ApplyDistributionPopView(getActivity());
                applyDistributionPopView.showAtLocation(mView.findViewById(R.id.rlv_apply_extension_bt), Gravity.CENTER, 0, 0);
                applyDistributionPopView.lightOff();
            }
        });
        mView.findViewById(R.id.rlv_my_extension_code_bt).setOnClickListener(v -> {
            // 我的推广码
            if (!FastClickAvoidUtil.isDoubleClick()) {
                // 判断二维码内容是否为空，为空生成海报没有意义
                if (!TextUtils.isEmpty(BaseUserInfoCache.getUserInviteUrl(getContext()))) {
                    ARouter.getInstance()
                            .build(RouterPathProvider.OnlineMeetingMyExtenSionPoster)
                            .withInt("fromType", 0)
                            .navigation();
                } else {
                    Toasty.normal(getContext(), getActivity().getResources().getString(com.tojoy.cloud.online_meeting.R.string.extension_qr_error)).show();
                }
            }
        });
        mView.findViewById(R.id.rlv_promoter_msg_manager).setOnClickListener(v -> {
            // 推广员信息
            if (!FastClickAvoidUtil.isDoubleClick()) {
                ARouter.getInstance().build(RouterPathProvider.Distribution_Information_Entry_Act)
                        .withString("from_page","homepage")
                        .navigation();
            }
        });
    }

    public void updateUserInfo() {
        if (mHeaderImg == null) {
            return;
        }
        //根据是否分销客的身份显示背景
        if (BaseUserInfoCache.getiSDistributor(getContext())) {
            // 推广员
            mHeaderBgRlv.setBackgroundResource(R.drawable.mine_head_yello_bg);
            mHeaderLableRlv.setVisibility(View.VISIBLE);
            mView.findViewById(R.id.rlv_apply_extension_bt).setVisibility(View.GONE);
            mView.findViewById(R.id.rlv_my_extension_code_bt).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.llv_my_vip).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.llv_my_profit).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.rlv_promoter_msg_manager).setVisibility(View.VISIBLE);
        } else {
            // 普通用户
            mHeaderBgRlv.setBackgroundResource(R.drawable.bg_mine_head_img);
            mHeaderLableRlv.setVisibility(View.GONE);
            mView.findViewById(R.id.rlv_apply_extension_bt).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.rlv_my_extension_code_bt).setVisibility(View.GONE);
            mView.findViewById(R.id.llv_my_vip).setVisibility(View.GONE);
            mView.findViewById(R.id.llv_my_profit).setVisibility(View.GONE);
            mView.findViewById(R.id.rlv_promoter_msg_manager).setVisibility(View.GONE);
        }

        ImageLoaderManager.INSTANCE.loadHeadImage(getContext(), mHeaderImg, UserInfoManager.getUserHeaderPic(getContext()),
                R.drawable.icon_dorecord);

        mTvUserIdTop.setText("ID" + UserInfoManager.getRoomCode(getActivity()));
        tvUserId.setText("ID" + UserInfoManager.getRoomCode(getActivity()));
        mNickName.setText(UserInfoManager.getUserNickName(getActivity()));

        ImageLoaderManager.INSTANCE.loadHeadImage(getContext(), mHeaderImg, UserInfoManager.getUserHeaderPic(getContext()),
                R.drawable.icon_dorecord);

        if (UserInfoManager.isHasCompany(getContext())) {
            mView.findViewById(R.id.my_enter).setVisibility(View.VISIBLE);
        } else {
            mView.findViewById(R.id.my_enter).setVisibility(View.GONE);
        }

        if ("1".equals(UserInfoManager.getCheckAuth(getContext()))) {
            mView.findViewById(R.id.my_power).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.my_meeting).setBackgroundResource(R.drawable.bg_ffffff_cen);
        } else {
            mView.findViewById(R.id.my_power).setVisibility(View.GONE);
            mView.findViewById(R.id.my_meeting).setBackgroundResource(R.drawable.bg_ffffff_top);
        }

        //根据是否有公司名判断布局展示与否
        if (TextUtils.isEmpty(BaseUserInfoCache.getCompanyName(getContext()))) {
            mTvCompanyName.setVisibility(View.GONE);
            tvUserId.setVisibility((!TextUtils.isEmpty(UserInfoManager.getRoomCode(getActivity())) && !"0".equals(UserInfoManager.getRoomCode(getActivity()))) ? View.VISIBLE : View.GONE);
            mTvUserIdTop.setVisibility(View.GONE);
        } else {
            mTvCompanyName.setVisibility(View.VISIBLE);
            mTvCompanyName.setText(BaseUserInfoCache.getCompanyName(getContext()));
            tvUserId.setVisibility(View.GONE);
            mTvUserIdTop.setVisibility((!TextUtils.isEmpty(UserInfoManager.getRoomCode(getActivity())) && !"0".equals(UserInfoManager.getRoomCode(getActivity()))) ? View.VISIBLE : View.GONE);
        }

        //根据用户角色判断是否显示企业服务管理
        if (BaseUserInfoCache.getUserIsHaveRoles(getActivity(), UserRoles.SUPERADMIN)) {
            mView.findViewById(R.id.mine_meeting_service_manager).setVisibility(View.VISIBLE);
        }

    }

    /**
     * 更新会员数据
     */
    public void updateDistributionData() {
        MineDistributionResponse.MineDistributionModel model = BaseUserInfoCache.getUserDistributiionData(getContext());
        //根据是否分销客的身份显示背景
        if (BaseUserInfoCache.getiSDistributor(getContext())) {
            // 推广员
            mView.findViewById(R.id.llv_my_vip).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.llv_my_profit).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.rlv_promoter_msg_manager).setVisibility(View.VISIBLE);
            if (model != null) {
                // 判断是否有公司，有公司：不显示推广码
                mView.findViewById(R.id.rlv_my_extension_code_bt).setVisibility(View.VISIBLE);
                mView.findViewById(R.id.rlv_apply_extension_bt).setVisibility(View.GONE);
                // 判断，一级分销客(显示我的团队)；二级分销客(不显示我的团队)；
                if (model.level == 1) {
                    mView.findViewById(R.id.llv_my_vip_team_count).setVisibility(View.VISIBLE);
                } else {
                    mView.findViewById(R.id.llv_my_vip_team_count).setVisibility(View.GONE);
                }
                // 展示我的会员数据
                myVipUpdateTodayTv.setText(model.getTodayIncrease());
                myVipTeamCountTv.setText(model.getTeamMembers());
                myVipAllCountTv.setText(model.getAllMembers());

                // 展示我的收益数据
                myProfitTodayTv.setText(model.getTodayIncome());
                myProfitMonthTv.setText(model.getMonthIncome());
                myProfitAllTv.setText(model.getTotalIncome());

            } else {
                mView.findViewById(R.id.rlv_my_extension_code_bt).setVisibility(View.GONE);
            }
        } else {
            // 普通用户
            mView.findViewById(R.id.llv_my_vip).setVisibility(View.GONE);
            mView.findViewById(R.id.llv_my_profit).setVisibility(View.GONE);
            mView.findViewById(R.id.rlv_my_extension_code_bt).setVisibility(View.GONE);
            mView.findViewById(R.id.rlv_promoter_msg_manager).setVisibility(View.GONE);
        }
    }


    @Override
    protected void initData() {
        super.initData();
    }

    @Override
    public void onResume() {
        super.onResume();
        queryNotReadCount();
    }

    /**
     * 检查权限处理和系统消息是否有未读消息个数
     */
    private void queryNotReadCount() {
        if ("1".equals(BaseUserInfoCache.getCheckAuth(getContext()))) {
            OMAppApiProvider.getInstance().showNum(new rx.Observer<HomeReadCountResponse>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onNext(HomeReadCountResponse omBaseResponse) {
                    if (omBaseResponse.isSuccess()) {
                        updateReadCount(omBaseResponse.data.count);
                    }
                }
            });
        } else {
            updateReadCount(0);
        }
    }

    // 权限审批消息未读数量
    private int mUnReadCount = 0;

    public void updateReadCount(int count) {
        this.mUnReadCount = count;
        if (mUnReadCount > 0) {
            mPowerUnReadTv.setVisibility(View.VISIBLE);
            mPowerUnReadTv.setText(mUnReadCount > 99 ? "99+" : String.valueOf(mUnReadCount));
        } else {
            mPowerUnReadTv.setVisibility(View.GONE);
        }
    }

}


