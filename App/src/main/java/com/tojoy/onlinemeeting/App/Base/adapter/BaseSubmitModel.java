package com.tojoy.onlinemeeting.App.Base.adapter;

import com.tojoy.tjoybaselib.net.BaseResponse;

/**
 * Created by chengyanfang on 2017/9/20.
 */

public class BaseSubmitModel extends BaseResponse {
    //枚举出类型
    public enum CellType {
        BLANKVIEW,
        SELETETYPE,
        SELETETYPETHREE
    }

    public String title;
    public String hint;
    public String content;
    public String typeContent;
    public boolean isChecked;
    public CellType type;
    public boolean showDivider = true;
    public boolean showRedTip = false;

    //隐藏类型内容
    public boolean hideTypeContent;

    public BaseSubmitModel setHideTypeContent(boolean hideTypeContent) {
        this.hideTypeContent = hideTypeContent;
        return this;
    }

    /**
     *
     */
    public String selectHint;

    public BaseSubmitModel setSelectHint(String selectHint) {
        this.selectHint = selectHint;
        return this;
    }


    /**
     * 设置输入框的Hint
     */
    public BaseSubmitModel setHint(String hint) {
        this.hint = hint;
        return this;
    }

    public boolean isShowMustInput;

    /**
     * 空行Cell构造
     *
     * @param type
     */
    public BaseSubmitModel(CellType type) {
        this.type = type;
    }

    /**
     * SectionHeader构造
     *
     * @param type
     * @param title
     */
    public BaseSubmitModel(CellType type, String title) {
        this.type = type;
        this.title = title;
    }

    /**
     * 选择Cell
     *
     * @param type
     * @param title
     * @param typeContent
     */
    public BaseSubmitModel(CellType type, String title, String typeContent) {
        this.type = type;
        this.title = title;
        this.typeContent = typeContent;
    }

    //获取右边是否为灰色状态且不可点
    public boolean hideRightArrow;

    public BaseSubmitModel setHideRightArrow(boolean hideRightArrow) {
        this.hideRightArrow = hideRightArrow;
        return this;
    }


    public String id;

    public BaseSubmitModel setId(String id) {
        this.id = id;
        return this;
    }


    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
