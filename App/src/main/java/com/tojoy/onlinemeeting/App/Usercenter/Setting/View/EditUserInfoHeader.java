package com.tojoy.onlinemeeting.App.Usercenter.Setting.View;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;
import com.tojoy.onlinemeeting.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by chengyanfang on 2017/10/14.
 */

public class EditUserInfoHeader {

    Context mContext;

    View mView;

    @BindView(R.id.head_image)
    ImageView mHeaderImg;

    @BindView(R.id.arrow)
    ImageView mArrowImg;


    public EditUserInfoHeader(Context aContext) {
        this.mContext = aContext;
    }

    public View getView() {
        if (mView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mView = aLayoutInflater.inflate(R.layout.headerview_useredit_layout, null);
        }

        ButterKnife.bind(this, mView);

        initUI();
        return mView;
    }

    @SuppressWarnings("rawtypes")
    private void initUI() {
        ImageLoaderManager.INSTANCE.loadImage(mContext, mHeaderImg, UserInfoManager.getUserHeaderPic(mContext), R.drawable.icon_dorecord, 40);
        mArrowImg.setVisibility(View.VISIBLE);
    }

    /**
     * 更新用户信息
     *
     * @param headerPic
     */
    public void updateUserInfoHeader(String headerPic) {
        ImageLoaderManager.INSTANCE.loadImage(mContext, mHeaderImg, headerPic, R.drawable.icon_dorecord, 40);
    }

}
