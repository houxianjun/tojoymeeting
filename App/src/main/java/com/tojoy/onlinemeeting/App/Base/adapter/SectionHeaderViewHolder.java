package com.tojoy.onlinemeeting.App.Base.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.tojoy.onlinemeeting.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by chengyanfang on 2017/9/20.
 */

public class SectionHeaderViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_title)
    TextView title;

    public SectionHeaderViewHolder(View arg0, Context aContext) {
        super(arg0);
        ButterKnife.bind(this, itemView);
    }
}