package com.tojoy.onlinemeeting.App.Application;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.PowerManager;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.netease.nim.uikit.api.NimUIKit;
import com.netease.nim.uikit.api.UIKitOptions;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nim.uikit.common.NIMCache;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;
import com.netease.nim.uikit.common.UserPreferences;
import com.tojoy.onlinemeeting.NIM.session.NIMFileProvider;
import com.tojoy.onlinemeeting.NIM.session.SessionHelper;
import com.tojoy.common.App.TJBaseApp;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.EnvironmentInstance;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;

import java.util.List;

/**
 * Created by chengyanfang on 2017/8/22.
 */

public class NIMApp extends TJBaseApp {

    private static final String TAG = "NIMApp";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //开启应用级缓存
        NIMCache.setContext(this);

        NIMClient.init(this, getLoginInfo(), NimSDKOptionConfig.getSDKOptions(this));

        if (inMainProcess()) {

            // 初始化UIKit模块
            initUIKit();

            // 初始化消息提醒
            NIMClient.toggleNotification(UserPreferences.getNoticeContentToggle());

            // 云信sdk相关业务初始化
            NimInitManager.getInstance().init(true);

            NIMClient.getService(MsgServiceObserve.class)
                    .observeReceiveMessage(incomingMessageObserver, true);
        }
    }


    Observer<List<IMMessage>> incomingMessageObserver =
            (Observer<List<IMMessage>>) messages -> {
                KeyguardManager km = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
                if (km.inKeyguardRestrictedInputMode()) {
                    PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
                    if (!pm.isScreenOn()) {
                        @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP |
                                PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "bright");
                        wl.acquire();
                        wl.release();
                    }
                }
            };

    /**
     * 如果已经存在用户登录信息，返回LoginInfo用来自动登录，否则返回null即可
     *
     * @return 登录账号信息
     */
    private LoginInfo getLoginInfo() {
        String account = UserInfoManager.getIMHeaderedAccount(this);
        final String token = UserInfoManager.getIMToken(this);

        if (!TextUtils.isEmpty(account) && !TextUtils.isEmpty(token)) {
            NIMCache.setAccount(account.toLowerCase());
            return new LoginInfo(account, token);
        } else {
            return null;
        }
    }

    /**
     * 判断是否是主进程
     *
     * @return
     */
    public boolean inMainProcess() {
        String packageName = getPackageName();
        String processName = getProcessName(this);
        return packageName.equals(processName);
    }

    /**
     * 初始化UIKIT
     */
    private void initUIKit() {
        // 初始化，使用 uikit 默认的用户信息提供者
        NimUIKit.init(this, buildUIKitOptions());

        // 设置文件提供者
        NimUIKit.setIMFileProvider(new NIMFileProvider());

        // 会话窗口的定制初始化。
        SessionHelper.init();

        //设置IMHeader
        NimUIKit.setIMHeaderStr(UserInfoManager.getIMAccountHeader(this));
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.fontScale != 1)//非默认值
            getResources();
        super.onConfigurationChanged(newConfig);
    }

    private UIKitOptions buildUIKitOptions() {
        UIKitOptions options = new UIKitOptions();
        // 设置app图片/音频/日志等缓存目录
        options.appCacheDir = NimSDKOptionConfig.getAppCacheDir(this) + "/app";
        return options;
    }

    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        if (res.getConfiguration().fontScale != 1) {//非默认值
            Configuration newConfig = new Configuration();
            newConfig.setToDefaults();//设置默认
            res.updateConfiguration(newConfig, res.getDisplayMetrics());
        }
        return res;
    }

    /**
     * 获取当前进程名
     *
     * @param context
     * @return 进程名
     */
    public static final String getProcessName(Context context) {
        String processName = null;

        // ActivityManager
        ActivityManager am = ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE));

        while (true) {
            for (ActivityManager.RunningAppProcessInfo info : am.getRunningAppProcesses()) {
                if (info.pid == android.os.Process.myPid()) {
                    processName = info.processName;
                    break;
                }
            }

            // go home
            if (!TextUtils.isEmpty(processName)) {
                return processName;
            }

            // take a rest and again
            try {
                Thread.sleep(100L);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

}
