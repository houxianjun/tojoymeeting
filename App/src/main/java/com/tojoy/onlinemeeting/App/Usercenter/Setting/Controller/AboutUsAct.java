package com.tojoy.onlinemeeting.App.Usercenter.Setting.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.onlinemeeting.BuildConfig;
import com.tojoy.onlinemeeting.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MPW on 2017/10/31.
 */

public class AboutUsAct extends UI {
    @BindView(R.id.aboutUs_version)
    TextView mTvVersion;

    @BindView(R.id.aboutUs_version_two)
    TextView mTvVersionTwo;


    public static void start(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, AboutUsAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        setStatusBar();
        ButterKnife.bind(this);
        initTitle();
        initUI();
    }

    private void initTitle() {
        setUpNavigationBar();
        showBack();
        findViewById(com.tojoy.tjoybaselib.R.id.base_title_line).setVisibility(View.GONE);
    }

    private void initUI() {
        mTvVersion.setText("直播招商云");
        mTvVersionTwo.setText("版本号" + BuildConfig.VERSION_NAME);
    }
}
