package com.tojoy.onlinemeeting.App.Home.Controller;

import com.netease.nim.uikit.common.IMLoginManager;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.ResponseCode;
import com.netease.nimlib.sdk.StatusCode;
import com.netease.nimlib.sdk.auth.AuthServiceObserver;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.SystemMessageObserver;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.ui.drop.DropCover;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.onlinemeeting.NIM.reminder.ReminderManager;
import com.tojoy.onlinemeeting.NIM.reminder.SystemMessageUnreadManager;
import com.tojoy.onlinemeeting.Service.SystemNotice.SystemMsgDBManager;
import com.tojoy.common.Services.Push.SystemNoticeType;

import java.util.List;

/**
 * 初始化IM消息监听器注册
 */
public class HomeIMInitor {

    ReminderManager.UnreadNumChangedCallback mUnreadNumChangedCallbackl;

    DropCover mCover;

    HomeTabAct mHomeTabAct;

    public HomeIMInitor(HomeTabAct homeTabAct, DropCover cover, ReminderManager.UnreadNumChangedCallback unreadNumChangedCallbackl) {
        mUnreadNumChangedCallbackl = unreadNumChangedCallbackl;
        mCover = cover;
        mHomeTabAct = homeTabAct;
    }


    /**
     * 注册监听
     */
    public void registerObserver(boolean isInit) {
        registerObservers(isInit);
        registerMsgUnreadInfoObserver(isInit);
        registerSystemMessageObservers(isInit);
        NIMClient.getService(MsgServiceObserve.class);
    }


    /**
     * 注册未读消息数量观察者
     */
    private void registerMsgUnreadInfoObserver(boolean register) {
        if (register) {
            ReminderManager.getInstance().registerUnreadNumChangedCallback(mUnreadNumChangedCallbackl);
        } else {
            ReminderManager.getInstance().unregisterUnreadNumChangedCallback(mUnreadNumChangedCallbackl);
        }
    }

    /**
     * 注册/注销系统消息未读数变化
     *
     * @param register
     */
    public void registerSystemMessageObservers(boolean register) {
        NIMClient.getService(SystemMessageObserver.class).observeUnreadCountChange(sysMsgUnreadCountChangedObserver, register);
    }


    private Observer<Integer> sysMsgUnreadCountChangedObserver = (Observer<Integer>) unreadCount -> onReceiveNewSystemMessage(SystemMsgDBManager.getInterface().getUnReadNoticeCntByType(mHomeTabAct, SystemNoticeType.SYSTEM_NOTICE));


    /**
     * 查询系统消息未读数
     */
    private void onReceiveNewSystemMessage(int unread) {
        SystemMessageUnreadManager.getInstance().setSysMsgUnreadCount(unread);
        ReminderManager.getInstance().updateContactUnreadNum(unread);
    }


    /**
     * 获取最近消息列表
     * 用来刷新小红点数目
     */
    public void requestRecentMessage() {
        // 查询最近联系人列表数据
        NIMClient.getService(MsgService.class).queryRecentContacts().setCallback(new RequestCallbackWrapper<List<RecentContact>>() {
            @Override
            public void onResult(int code, List<RecentContact> recents, Throwable exception) {
                if (code != ResponseCode.RES_SUCCESS || recents == null) {
                    return;
                }
                //加上系统通知
                int unreadSystemMsgNum = SystemMsgDBManager.getInterface().getUnReadNoticeCntByType(mHomeTabAct,SystemNoticeType.SYSTEM_NOTICE);
                ReminderManager.getInstance().updateSessionUnreadNum(unreadSystemMsgNum);
            }
        });
    }

    /**
     * 监听在线状态
     */
    private void registerObservers(boolean register) {
        NIMClient.getService(AuthServiceObserver.class).observeOnlineStatus((Observer<StatusCode>) code -> {

            //登录成功
            if (code == StatusCode.LOGINED) {

                AppConfig.isLoginIM = true;

            } else {

                if (code == StatusCode.PWD_ERROR) { //密码错误: Token错误  重新获取登录

                    IMLoginManager.getInterface().requestLoginIM(mHomeTabAct, "1", new IMLoginManager.LoginSuccessCallback() {
                        @Override
                        public void onLoginSuccess() {
                        }

                        @Override
                        public void onLoginError(int code) {
                        }
                    });

                } else if (code == StatusCode.KICKOUT) { //被踢，去登录页
//                    doLiveSomething();
                    RouterJumpUtil.startLoginActivity(true);
                    AppConfig.KICK_OUT = true;
                }

                AppConfig.isLoginIM = false;
            }

        }, register);
    }

}
