package com.tojoy.onlinemeeting.App.Usercenter.Login;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.netease.nimlib.sdk.NimIntent;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.permission.annotation.OnMPermissionDenied;
import com.tojoy.tjoybaselib.services.permission.annotation.OnMPermissionGranted;
import com.tojoy.tjoybaselib.services.permission.annotation.OnMPermissionNeverAskAgain;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.util.file.FileUtil;
import com.tojoy.tjoybaselib.util.log.LogUtil;
import com.tojoy.tjoybaselib.util.media.ImageUtil;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.sys.DeviceInfoUtil;
import com.tojoy.onlinemeeting.App.Application.App;
import com.tojoy.onlinemeeting.App.Home.Controller.HomeTabAct;
import com.tojoy.onlinemeeting.Service.SplashAd.ScreenAdvertisInfoManager;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;
import com.tojoy.onlinemeeting.R;
import com.tojoy.common.Services.Push.SystemNoticeType;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by chengyanfang on 2017/8/22.
 *
 * @function 首次打开欢迎页面
 * 处理从Notification 跳转进来的处理
 */

public class LaunchAct extends UI {

    //广告图片
    @BindView(R.id.image_screen_advertis)
    ImageView mAdvertisImage;

    //跳过倒计时
    @BindView(R.id.start_skip_count_down)
    TextView mTimeText;


    @BindView(R.id.start_skip)
    FrameLayout mRelativeLayoutSkip;


    //倒计时 计时器
    private AdvertDownTimer mAdvertDownTimer;

    //广告图片
    private Bitmap mBitmap;

    private String saveImageDir;

    private String imageFileName;
    private String imagePath;

    TJMakeSureDialog makeSureDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!this.isTaskRoot()) { // 判断当前activity是不是所在任务栈的根
            Intent intent = getIntent();
            if (intent != null) {
                String action = intent.getAction();
                boolean isFinish = intent.getBooleanExtra("isFinish",false);
                if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) &&(isFinish || Intent.ACTION_MAIN.equals(action))) {
                    finish();
                    return;
                }
            }
        }
        setContentView(R.layout.activity_launch_layout);
        setStatusBar();
        setSwipeBackEnable(false);
        ButterKnife.bind(this);

//        //切换环境强制退出
//        if (UserInfoManager.needChangeLoginStateWithEvn(this)) {
//            UserInfoManager.logout();
//        }
        // 从堆栈恢复，不再重复解析之前的intent
        if (savedInstanceState != null) {
            setIntent(new Intent());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (App.firstEnter) {
            if (BaseUserInfoCache.isAgreed(this)) {
                new Handler().postDelayed(() -> showSplashView(), 1000);
            } else {
                if (makeSureDialog != null && makeSureDialog.isShowing()) {
                    return;
                }
                showFirstAgreeDialog();
            }
        } else {
            onIntent(new Intent());
        }
    }


    private void showFirstAgreeDialog() {
        if (makeSureDialog == null) {
            makeSureDialog = new TJMakeSureDialog(this, true, v -> {
                BaseUserInfoCache.putIsAgreed(LaunchAct.this);
                new Handler().postDelayed(() -> showSplashView(), 1000);
            }).setTitleAndCotent("温馨提示", "     欢迎您使用“直播招商云”软件及相关服务！\n" +
                    "      “直播招商云”软件服务由天九共享网络科技集团有限公司（以下简称“我们”）开发并发布。我们依据法律法规收集、使用个人信息。" +
                    "在使用“直播招商云”软件及相关服务前，请您务必仔细阅读并理解我们的《用户协议》及《隐私政策》。您一旦选择“同意”，即意味着您授" +
                    "权我们收集、保存、使用、共享、披露及保护您的信息。").setBtnText("同意", "不同意").setCancelListener(() -> showSecondAgreeDialog());

        }
        makeSureDialog.show(80);
    }

    private void showSecondAgreeDialog() {
        new TJMakeSureDialog(this, false, v -> showFirstAgreeDialog()).setTitleAndCotent("同意隐私政策方可继续", "      您需要同意隐私政策，我们才能继续为您提供服务。")
                .setBtnText("再次查看", "退出应用").setCancelListener(() -> {
            android.os.Process.killProcess(android.os.Process.myPid());
        }).show();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    /**
     * 处理Intent跳转
     * 1.新开App->Icon:直接跳转到引导页
     * 2.新开App->通知：解析Intent
     * 3.App已开->通知：解析Intent
     */
    private void onIntent(Intent intent2) {
        Intent intent = getIntent();
        if (intent == null) {
            if (App.firstEnter) {
                forwardToGuideAct(null);
            } else {
                //没有解析到Intent,而且App已开，从哪来回哪去
                finish();
            }
        } else {
            if (intent.hasExtra(NimIntent.EXTRA_NOTIFY_CONTENT)) {
                ArrayList<IMMessage> messages = (ArrayList<IMMessage>) intent.getSerializableExtra(NimIntent.EXTRA_NOTIFY_CONTENT);
                if (messages == null) {
                    forwardToGuideAct(null);
                } else if (messages.size() > 1) {
                    forwardToGuideAct(new Intent().putExtra(NimIntent.EXTRA_NOTIFY_CONTENT, ""));
                } else {
                    forwardToGuideAct(new Intent().putExtra(NimIntent.EXTRA_NOTIFY_CONTENT, messages.get(0)));
                }
            } else if (intent.hasExtra(SystemNoticeType.SYSTEM_NOTICE)
                    || intent.hasExtra(SystemNoticeType.ACTIVIT_NOTICE)) {
                forwardToGuideAct(intent);
            } else if (App.firstEnter) {
                forwardToGuideAct(null);
            } else {
                forwardToGuideAct(intent2);
            }
        }
    }

    private void forwardToGuideAct(Intent intent) {
        if (UserInfoManager.isLogined(LaunchAct.this)) {
            if (BaseUserInfoCache.getIsFirstVersion(LaunchAct.this)) {
                BaseUserInfoCache.saveIsFirstVersion(LaunchAct.this);
                RouterJumpUtil.startLoginActivity();
            } else {
                HomeTabAct.start(LaunchAct.this, intent);
            }
        } else {
            RouterJumpUtil.startLoginActivity();
        }
        finish();
    }


    /***
     * 广告倒计时
     */
    class AdvertDownTimer extends CountDownTimer {
        public AdvertDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        public void onFinish() {
            mTimeText.setText("0s 跳过");
        }

        public void onTick(long millisUntilFinished) {
            mTimeText.setText(millisUntilFinished / 1000 + "s 跳过");
        }
    }

    /**
     * 首次进入，打开欢迎界面
     */
    private void showSplashView() {
        //获取保存图片的文件目录
        saveImageDir = ScreenAdvertisInfoManager.AD_IMAGE_DIR;
        //获取文件名
        imageFileName = ImageUtil.getImgName(ScreenAdvertisInfoManager.getImageUrl(this));
        //图片路径
        imagePath = saveImageDir + "/" + imageFileName;
        if (FileUtil.isFileExit(saveImageDir, imageFileName) && !TextUtils.isEmpty(imageFileName) && UserInfoManager.isLogined(this)) {//如果文件存在存在  动态获取权限
            checkPersimiss(PREMISSIONS_EXTERNAL_STORAGE, PERMISSIONS_EXTERNAL_STORAGE_REQUEST_CODE);
        } else {// 图片没有 不存在
            forwardToGuideAct(null);
        }
    }


    /***
     * 广告倒计
     */
    private void showAdvert() {
        //创建倒计时类
        mAdvertDownTimer = new AdvertDownTimer(4000, 1000);
        mAdvertDownTimer.start();
        //倒计时接结束后跳如HomeActivitu
        Handler handler = new Handler();
        Runnable runnable = new PostDelayedRunable();
        handler.postDelayed(runnable, 3000);
        //点击广告跳转  移除runable
        mAdvertisImage.setOnClickListener(v -> {
            if (UserInfoManager.isLogined(LaunchAct.this)) {
                if (BaseUserInfoCache.getIsFirstVersion(LaunchAct.this)) {
                    BaseUserInfoCache.saveIsFirstVersion(LaunchAct.this);
                    RouterJumpUtil.startLoginActivity();
                } else {
                    HomeTabAct.startForAdvert(LaunchAct.this, HomeTabAct.FROM_ADVERT);
                }
            } else {
                RouterJumpUtil.startLoginActivity();
            }
            mAdvertDownTimer.cancel();
            handler.removeCallbacks(runnable);
            finish();
        });
        //点击跳过 移除runable
        mTimeText.setOnClickListener(v -> {
            handler.removeCallbacks(runnable);
            onIntent(new Intent());
        });
    }

    class PostDelayedRunable implements Runnable {
        @Override
        public void run() {
            onIntent(new Intent());
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //关闭计时器
        if (mAdvertDownTimer != null) {
            mAdvertDownTimer.cancel();
        }

        if(makeSureDialog != null) {
            makeSureDialog.dismiss();
        }
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.clear();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return true;
    }

    /**
     * 基本权限管理
     */
    @OnMPermissionGranted(PERMISSIONS_EXTERNAL_STORAGE_REQUEST_CODE)
    public void onBasicPermissionSuccess() {
        DeviceInfoUtil.getSerialNum(App.getInstance());
        try {
            mBitmap = BitmapFactory.decodeFile(imagePath);
        } catch (OutOfMemoryError e) {
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                mBitmap = BitmapFactory.decodeFile(imagePath, options);
            } catch (Exception e2) {
                onIntent(new Intent());
            }
        }

        long nowTime = System.currentTimeMillis();

        LogUtil.e("nowTime", nowTime + "");
        if (BaseUserInfoCache.isAgreed(this) && mBitmap != null && nowTime < ScreenAdvertisInfoManager.getEndTime(LaunchAct.this)) {
            mTimeText.setVisibility(View.VISIBLE);
            mRelativeLayoutSkip.setVisibility(View.VISIBLE);
            mTimeText.setText("3s 跳过");
            mAdvertisImage.setVisibility(View.VISIBLE);
            mAdvertisImage.setImageBitmap(mBitmap);
            showAdvert();
        } else {
            onIntent(new Intent());
        }
    }

    @OnMPermissionDenied(PERMISSIONS_EXTERNAL_STORAGE_REQUEST_CODE)
    @OnMPermissionNeverAskAgain(PERMISSIONS_EXTERNAL_STORAGE_REQUEST_CODE)
    public void onBasicPermissionFailed() {
        onIntent(new Intent());
    }
}
