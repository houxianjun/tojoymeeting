package com.tojoy.onlinemeeting.App.Usercenter.Setting.View;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.tojoy.tjoybaselib.ui.editView.ClearEmojInputFilter;
import com.tojoy.tjoybaselib.util.string.StringUtil;
import com.tojoy.onlinemeeting.R;

/**
 * Created by chengyanfang on 2017/10/17.
 */

public class HelpFeedbackFooterView {

    Context mContext;
    View mView;


    EditText mEditFeedback;
    TextView mTvEms;
    OnSubmitClickListener mSubmitClickListener;

    private final int LIMIT_LENGTH = 100;

    public HelpFeedbackFooterView(Context context, OnSubmitClickListener submitClickListener) {
        mContext = context;
        mSubmitClickListener = submitClickListener;
    }

    public View getView() {
        if (mView == null) {
            LayoutInflater aLayoutInflater = LayoutInflater.from(mContext);
            this.mView = aLayoutInflater.inflate(R.layout.view_helpfeedback_footer_layout, null);
        }
        initUI();
        return mView;
    }

    private void initUI() {
        mEditFeedback = mView.findViewById(R.id.edit_helpFeedback);
        mTvEms = mView.findViewById(R.id.tvEms);

        InputFilter[] emojiFilters = {new ClearEmojInputFilter()};
        mEditFeedback.setFilters(emojiFilters);

        View rlBtn = mView.findViewById(R.id.rlv_helpFeedback_bt);
        TextView tvBtn = mView.findViewById(R.id.tv_helpFeedback_bt);
        rlBtn.setClickable(false);
        rlBtn.setOnClickListener(v ->
                mSubmitClickListener.onSubmitFeedback(mEditFeedback.getText().toString()));

        mEditFeedback.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @SuppressLint("NewApi")
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int length = charSequence.length();
                if (!TextUtils.isEmpty(charSequence.toString().trim()) && length > 0) {
                    rlBtn.setClickable(true);
                    tvBtn.setTextColor(tvBtn.getContext().getResources().getColor(R.color.color_2476ED));
                } else {
                    rlBtn.setClickable(false);
                    tvBtn.setTextColor(tvBtn.getContext().getResources().getColor(R.color.color_E0E2E7));
                }

                if (length > LIMIT_LENGTH) {
                    mEditFeedback.setText(mEditFeedback.getText().subSequence(0, LIMIT_LENGTH));
                    mEditFeedback.setSelection(LIMIT_LENGTH);
                } else {
                    StringUtil.setColorText(mTvEms.getContext(), 0, (mEditFeedback.getText().length() + "/100").length() - 4, mEditFeedback.getText().length() + "/100", mTvEms);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public interface OnSubmitClickListener {
        void onSubmitFeedback(String feedback);
    }
}
