package com.tojoy.onlinemeeting.App.Usercenter.Login;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.oss.OSSService;
import com.tojoy.onlinemeeting.App.Home.Controller.HomeTabAct;
import com.tojoy.onlinemeeting.R;
import com.tojoy.onlinemeeting.Service.EventBus.TakePhotoEventBus;
import com.netease.nim.uikit.common.IMLoginManager;
import com.tojoy.common.Services.ParamsPicker.BasePickerAct;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

/**
 * Created by jiangguoqiu on 2018/4/16.
 * 登入注册 上传照片 跟登入IM 抽取
 */
public class LoginRegisterBaseAct extends BasePickerAct {
    protected ArrayList<String> headerImgs = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    /**
     * 处理登录成功
     */
    protected void loginSuccess() {
        IMLoginManager.getInterface().requestLoginIM(this, "0", new IMLoginManager.LoginSuccessCallback() {
            @Override
            public void onLoginSuccess() {
                handleLoginSucess(true);
                statitiscIMLogin("1");
            }

            @Override
            public void onLoginError(int code) {
                handleLoginSucess(false);
                statitiscIMLogin("0");
            }
        });
    }

    private void handleLoginSucess( boolean isLoginIMSucess) {
        dismissProgressHUD();
        if(!isLoginIMSucess) {
            BaseUserInfoCache.logout();
            Toasty.normal(this, "获取IM_Token失败，请稍后再试").show();
            return;
        }
        forwardToHomeAct();
    }

    private void statitiscIMLogin(String loginIMState) {
        OMAppApiProvider.getInstance().statitiscIMLogin(loginIMState, "enterLivePage", new rx.Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(OMBaseResponse omBaseResponse) {
            }
        });
    }

    /**
     * 最后跳转到首页
     */
    protected void forwardToHomeAct() {
        HomeTabAct.start(LoginRegisterBaseAct.this, null);
        getHandler().postDelayed(this::finish, 250);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTakePhotoEventBus(TakePhotoEventBus event) {
        showProgressHUD(this, "处理中");
        headerImgs.clear();
        headerImgs.add(event.imagePath);
        OSSService.getInterface().initOSSService(new OSSService.InitOSSCallBack() {
            @Override
            public void onInitComplete() {
                startCompress();
            }

            @Override
            public void onInitFail() {
                Toasty.normal(LoginRegisterBaseAct.this, getString(R.string.oss_init_fail)).show();
            }
        });
    }

    /**
     * EVENT BUS
     */
    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public ArrayList<String> getImgsUriList() {
        return headerImgs;
    }

}
