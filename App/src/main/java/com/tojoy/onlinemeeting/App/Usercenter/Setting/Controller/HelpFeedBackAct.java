package com.tojoy.onlinemeeting.App.Usercenter.Setting.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.WindowManager;

import com.tojoy.tjoybaselib.net.HttpErrorUtil;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.onlinemeeting.App.Base.adapter.BaseSubmitModel;
import com.tojoy.onlinemeeting.App.Base.adapter.SubmitAdapter;
import com.tojoy.onlinemeeting.App.Usercenter.Setting.View.HelpFeedbackFooterView;
import com.tojoy.onlinemeeting.R;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * Created by MPW on 2017/10/30.
 */

public class HelpFeedBackAct extends UI {
    RecyclerView mTJRecyclerView;
    SubmitAdapter mAdapter;
    ArrayList<BaseSubmitModel> mDatas = new ArrayList<>();
    HelpFeedbackFooterView mFooterView;

    public static void start(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, HelpFeedBackAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_publish_cooperation_layout);
        setStatusBar();
        initTitle();
        initUI();
        initData();
    }

    private void initTitle() {
        setUpNavigationBar();
        setTitle("帮助与反馈");
        showBack();
    }

    private void initUI() {
        mTJRecyclerView = (RecyclerView) findViewById(R.id.container_layout);
        mTJRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mFooterView = new HelpFeedbackFooterView(this, feedback -> submitFeedback(feedback));
    }


    private void initData() {
        mDatas.add(new BaseSubmitModel(BaseSubmitModel.CellType.BLANKVIEW));
        mAdapter = new SubmitAdapter(this, mDatas);
        mTJRecyclerView.setAdapter(mAdapter);
        mAdapter.setFooterView(mFooterView.getView());
    }

    private void submitFeedback(String feedback) {
        if (TextUtils.isEmpty(feedback)) {
            Toasty.normal(HelpFeedBackAct.this, "请写下您的宝贵意见！").show();
            return;
        }
        showProgressHUD(this, "反馈中...");
        OMAppApiProvider.getInstance().leaveMsgSave(feedback, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                dismissProgressHUD();
                HttpErrorUtil.error(HelpFeedBackAct.this, e);
            }

            @Override
            public void onNext(OMBaseResponse baseResponse) {
                dismissProgressHUD();
                if (baseResponse.isSuccess()) {
                    Toasty.success(HelpFeedBackAct.this, "提交成功").show();
                    HelpFeedBackAct.this.finish();
                } else {
                    Toasty.normal(HelpFeedBackAct.this, baseResponse.msg).show();
                }
            }
        });
    }


}
