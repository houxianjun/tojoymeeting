package com.tojoy.onlinemeeting.App.Usercenter.Setting.Controller;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.RelativeLayout;

import com.alibaba.android.arouter.launcher.ARouter;
import com.netease.nim.uikit.common.NIMCache;
import com.tojoy.onlinemeeting.App.Usercenter.Login.LogoutHelper;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.EnvironmentInstance;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.meetingperson.SingleTonModel.MeetingPersonTon;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.permission.annotation.OnMPermissionGranted;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.onlinemeeting.App.Base.adapter.BaseSubmitModel;
import com.tojoy.onlinemeeting.App.Base.adapter.SubmitAdapter;
import com.tojoy.onlinemeeting.R;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;
import com.tojoy.tjoybaselib.configs.UrlProvider;
import com.tojoy.common.Services.EventBus.PublicStringEvent;
import com.tojoy.tjoybaselib.util.sys.ActivityStack;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import rx.Observer;


/**
 * Created by chengyanfang on 2017/10/17.
 */

public class SettingAct extends UI {

    RecyclerView mTJRecyclerView;
    SubmitAdapter mAdapter;
    RelativeLayout mRlvLogout;

    ArrayList<BaseSubmitModel> mDatas = new ArrayList<>();

    private String mPhoneNum = "4008958818";

    public static void start(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, SettingAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_layout);
        setStatusBar();
        initTitle();
        initUI();
        initData();
    }


    private void initTitle() {
        setUpNavigationBar();
        setTitle("设置");
        showBack();
    }

    TJMakeSureDialog dialog;

    private void initUI() {
        mTJRecyclerView = (RecyclerView) findViewById(R.id.container_layout);
        mRlvLogout = (RelativeLayout) findViewById(R.id.rlv_logout_bt);
        mTJRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRlvLogout.setOnClickListener(view -> {
            dialog = new TJMakeSureDialog(SettingAct.this, v1 -> {
                pushLogout();
            }).setTitleAndCotent("退出登录", "确认退出当前账号").hideTitle();
            dialog.setSameColor();
            dialog.show();
        });
    }

    private void pushLogout() {

        showProgressHUD(this, "退出中");
        LogoutHelper.loginOutClearCache(SettingAct.class,new LogoutHelper.LoginOutListener() {
            @Override
            public void loginOutSuccess() {
                dismissProgressHUD();
                RouterJumpUtil.startLoginActivity();
                SettingAct.this.finish();
            }
        });

    }

//    private void doLogout() {
//
//        OMAppApiProvider.getInstance().logout(new Observer<OMBaseResponse>() {
//            @Override
//            public void onCompleted() {
//
//            }
//
//            @Override
//            public void onError(Throwable e) {
//
//            }
//
//            @Override
//            public void onNext(OMBaseResponse omBaseResponse) {
//
//            }
//        });
//
//        AppConfig.isShowing = false;
//        dismissProgressHUD();
//        AppConfig.isLoginIM = false;
//        EventBus.getDefault().post(new PublicStringEvent(PublicStringEvent.LOGOUT_SUCCESS));
//        UserInfoManager.logout();
//
//    }

    private void initData() {
        mDatas.add(new BaseSubmitModel(BaseSubmitModel.CellType.SELETETYPE, "联系客服").setHideTypeContent(true));
        mDatas.add(new BaseSubmitModel(BaseSubmitModel.CellType.SELETETYPE, "帮助与反馈").setHideTypeContent(true));
//        mDatas.add(new BaseSubmitModel(BaseSubmitModel.CellType.SELETETYPE, "上传日志").setHideTypeContent(true));
        mDatas.add(new BaseSubmitModel(BaseSubmitModel.CellType.SELETETYPE, "关于我们").setHideTypeContent(true));
        mDatas.add(new BaseSubmitModel(BaseSubmitModel.CellType.SELETETYPE, "直播招商云用户服务协议").setHideTypeContent(true));
        mDatas.add(new BaseSubmitModel(BaseSubmitModel.CellType.SELETETYPE, "直播招商云隐私政策").setHideTypeContent(true));

        //正式环境
        if(AppConfig.environmentInstance != EnvironmentInstance.FORMAL){
            mDatas.add(new BaseSubmitModel(BaseSubmitModel.CellType.SELETETYPE, "切换环境").setHideTypeContent(true));
        }
        mAdapter = new SubmitAdapter(this, mDatas);
        mTJRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(index -> {
            if (FastClickAvoidUtil.isDoubleClick()) {
                return;
            }
            String title = mDatas.get(index).title;
            if ("联系客服".equals(title)) {
                if (mPhoneNum != null) {
                    dialog = new TJMakeSureDialog(SettingAct.this, v1 -> {
                    }).setCallPhonecontent(mPhoneNum);
                    dialog.setBtnText("呼叫", "取消");
                    dialog.setEvent(v -> {
                        if (mPhoneNum != null) {
                            callSectoryPhone(mPhoneNum);
                        }
                        dialog.dismiss();
                    });
                    dialog.setSameColor();
                    dialog.show();
                }
            } else if ("关于我们".equals(title)) {
                AboutUsAct.start(SettingAct.this);
            } else if ("直播招商云用户服务协议".equals(title)) {
                ARouter.getInstance().build(RouterPathProvider.WEBNew)
                        .withString("url", UrlProvider.USERCENTER_RegistPolicy)
                        .withString("title", "")
                        .navigation();
            } else if ("直播招商云隐私政策".equals(title)) {
                ARouter.getInstance().build(RouterPathProvider.WEBNew)
                        .withString("url", UrlProvider.USERCENTER_Privacy)
                        .withString("title", "")
                        .navigation();
            } else if ("帮助与反馈".equals(title)) {
                HelpFeedBackAct.start(SettingAct.this);
            }  else if ("上传日志".equals(title)) {
                UploadLogFileAct.start(SettingAct.this);
            }
            else if ("切换环境".equals(title)) {
                EnvironmentSwitchAct.start(SettingAct.this);
            }
        });
    }

    public void callSectoryPhone(String phoneNum) {
        mPhoneNum = phoneNum;
        checkPersimiss(PERMISSIONS_MAKECALL, PERMISSIONS_MAKECALL_REQUEST_CODE);
    }


    //拨打电话
    @OnMPermissionGranted(PERMISSIONS_MAKECALL_REQUEST_CODE)
    public void onBasicPermissionSuccessMobile() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mPhoneNum));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
