package com.tojoy.onlinemeeting.App.Home.Controller;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.netease.nim.uikit.api.model.user.SystemNotice;
import com.netease.nim.uikit.business.liveroom.JoinLiveRoomHelper;
import com.netease.nim.uikit.business.liveroom.JoinLiveRoomListener;
import com.tojoy.tjoybaselib.model.live.CheckLiveStatusResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.util.time.TimeUtil;
import com.tojoy.onlinemeeting.Service.SystemNotice.SystemMsgDBManager;
import com.tojoy.cloud.online_meeting.App.Meeting.adapter.MeetingInviteNoticeAdapter;
import com.tojoy.tjoybaselib.model.home.MeetingInviteModel;
import com.tojoy.tjoybaselib.model.home.MeetingInviteNoticeResponse;
import com.tojoy.cloud.online_meeting.App.Meeting.view.NewsHeaderView;
import com.tojoy.cloud.online_meeting.R;
import com.tojoy.common.App.BaseFragment;
import com.tojoy.common.Services.Push.SystemNoticeType;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import rx.Observer;


/**
 * Tab-消息Fragment
 */
public class NewsFragment extends BaseFragment {

    private int mPage;
    private TJRecyclerView mRecyclerView;
    private NewsHeaderView mHeaderview;
    private MeetingInviteNoticeAdapter mAdapter;
    private ArrayList<MeetingInviteModel> mModelList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_news, container, false);
        }
        isCreateView = true;
        return mView;
    }

    @Override
    protected void lazyLoad() {
        mRecyclerView.autoRefresh();
        int count = SystemMsgDBManager.getInterface().getUnReadNoticeCntByType(getActivity(), SystemNoticeType.SYSTEM_NOTICE);
        String content = "";
        String time = "";
        if (count > 0) {
            List<SystemNotice> resultList = SystemMsgDBManager.getInterface().getNoticeListByType(getActivity(), 1, SystemNoticeType.SYSTEM_NOTICE);
            if (resultList.size() > 0) {
                content = resultList.get(0).content;
                time = TimeUtil.getDateTimeString(Long.parseLong(resultList.get(0).timestamp), "yyyy-MM-dd HH:mm:ss");
            }
        } else {
            List<SystemNotice> resultList = SystemMsgDBManager.getInterface().getNoticeListByType(getContext(), mPage, SystemNoticeType.SYSTEM_NOTICE);
            if (resultList.size() > 0) {
                content = resultList.get(0).content;
                time = TimeUtil.getDateTimeString(Long.parseLong(resultList.get(0).timestamp), "yyyy-MM-dd HH:mm:ss");
            }
        }
        refresh(count, content, time);
    }


    @Override
    protected void initUI() {
        super.initUI();
        setUpNavigationBar(mView.findViewById(R.id.rlv_title_layout));
        setNavTitle("消息");
        hideBottomLine();
        mRecyclerView = mView.findViewById(R.id.news_recycleview);
        mRecyclerView.setLoadMoreViewWithHide();
        mRecyclerView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                mPage = 1;
                getData();
            }

            @Override
            public void onLoadMore() {
                if (mModelList.isEmpty()) {
                    return;
                }
                mPage++;
                getData();
            }
        });

        mHeaderview = new NewsHeaderView(getContext());
        mRecyclerView.addHeaderView(mHeaderview.getView());
        setAdapter();
    }

    public void refresh(int unReadCount, String content, String timeStamp) {
        if (mRecyclerView != null) {
            mPage = 1;
            getData();
        }

        if (mHeaderview != null) {
            mHeaderview.refresh(unReadCount, content, timeStamp);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mPage == 1) {
            getData();
        }
    }

    @Override
    protected void initData() {
        super.initData();
    }

    private void getData() {
        OMAppApiProvider.getInstance().inviteMeRecordList(String.valueOf(mPage), new Observer<MeetingInviteNoticeResponse>() {
            @Override
            public void onCompleted() {
                mRecyclerView.setRefreshing(false);
            }

            @Override
            public void onError(Throwable e) {
                mRecyclerView.refreshLoadMoreView(1, 0);
            }

            @Override
            public void onNext(MeetingInviteNoticeResponse response) {
                if (response.isSuccess()) {
                    if (mPage == 1) {
                        mModelList.clear();
                        mModelList.addAll(response.data.list);
                        mAdapter.updateData(mModelList);
                    } else {
                        mModelList.addAll(response.data.list);
                        mAdapter.addMore(response.data.list);
                    }
                    mRecyclerView.showFooterView();
                    mRecyclerView.refreshLoadMoreView(mPage, response.data.list.size());
                } else {
                    Toasty.normal(getContext(), response.responseTips).show();
                }
            }
        });
    }

    private boolean isJoin = false;

    private void setAdapter() {
        if (mAdapter == null) {
            mAdapter = new MeetingInviteNoticeAdapter(getContext(), mModelList);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.mJoinLiveCallback = (meetingModel, pos) -> {
                if ("1".equals(meetingModel.status)) {
                    // 会议未开始，没生成roomLiveId就不往下走流程
                    Toasty.normal(getContext(), "会议还未开始").show();
                } else {
                    if (isJoin) {
                        return;
                    }

                    isJoin = true;

                    showProgressHUD(getContext(), "加载中");

                    OMAppApiProvider.getInstance().getLiveRoomStatus(meetingModel.roomLiveId, new Observer<CheckLiveStatusResponse>() {
                        @Override
                        public void onCompleted() {
                            dismissProgressHUD();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toasty.normal(getContext(), "网络异常，请稍后再试").show();
                            isJoin = false;
                        }

                        @Override
                        public void onNext(CheckLiveStatusResponse response) {
                            if (response.isSuccess()) {
                                if ("1".equals(response.data.status)) {
                                    Toasty.normal(getContext(), "会议还未开始").show();
                                    isJoin = false;
                                } else if ("2".equals(response.data.status) || "3".equals(response.data.status) || "4".equals(response.data.status)) {
                                    meetingModel.status = response.data.status;
                                    mAdapter.notifyItemChanged(pos);
                                    skipToELive(meetingModel, response.data.title);
                                }
                            } else {
                                Toasty.normal(getContext(), response.msg).show();
                                isJoin = false;
                            }
                        }
                    });
                }

            };
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 跳转至直播&回放&结束页面
     *
     * @param meetingInviteModel
     */
    private void skipToELive(MeetingInviteModel meetingInviteModel, String title) {
        JoinLiveRoomHelper.goToLiveRoom(getActivity(), meetingInviteModel.roomLiveId, title, new JoinLiveRoomListener() {
            @Override
            public void onJoinError(String error, int errorCode) {
            }

            @Override
            public void onJoinSuccess() {
            }
        });

        new Handler().postDelayed(() -> isJoin = false, 800);
    }

}
