package com.tojoy.onlinemeeting.App.Home.Controller;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.igexin.sdk.PushManager;
import com.netease.nim.uikit.api.model.main.LoginSyncDataStatusObserver;
import com.netease.nimlib.sdk.NimIntent;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tojoy.common.Services.Router.PushNotificationRouter;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.PushConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.home.HomePopResponse;
import com.tojoy.tjoybaselib.model.home.ScreenAdvertisResponse;
import com.tojoy.tjoybaselib.model.pay.CheckDeviceResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.Bugly.BuglyScenceManger;
import com.tojoy.tjoybaselib.services.NetChangeReceiver.NetChangeReceiver;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.onlineMeetingVersionTextConfig.TextConfigCacheManager;
import com.tojoy.tjoybaselib.services.update.UpdateEvent;
import com.tojoy.tjoybaselib.services.update.UpdateManager;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.drop.DropCover;
import com.tojoy.tjoybaselib.util.file.FileUtil;
import com.tojoy.tjoybaselib.util.log.LogUtil;
import com.tojoy.tjoybaselib.util.media.ImageUtil;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.string.StringUtil;
import com.tojoy.tjoybaselib.util.sys.ActivityStack;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.tjoybaselib.util.sys.KeyboardControlUtil;
import com.tojoy.tjoybaselib.util.sys.StatusbarUtils;
import com.tojoy.onlinemeeting.App.Application.App;
import com.tojoy.onlinemeeting.App.Home.Views.HomePoPView;
import com.tojoy.onlinemeeting.R;
import com.tojoy.onlinemeeting.Service.Address.Helper.AddressCheckService;
import com.tojoy.onlinemeeting.Service.Push.GTPushIntentService;
import com.tojoy.onlinemeeting.Service.Push.GTPushService;
import com.tojoy.onlinemeeting.Service.SplashAd.ScreenAdvertisInfoManager;
import com.tojoy.common.Services.EventBus.PublicStringEvent;
import com.tojoy.common.Services.Router.AppRouter;
import com.yanzhenjie.permission.AndPermission;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by chengyanfang on 2017/8/22.
 */

@Route(path = RouterPathProvider.APP_HOME)
public class HomeTabAct extends UI {

    public static final String FROM_ADVERT = "FROM_ADVERT";

    HomeTabPresent mHomeTabPresent;

    HomeIMInitor mHomeIMInitor;

    HomePoPView myPopupWindow;

    NetChangeReceiver mNetChangeReceiver;

    private Bitmap mBitmap;

    public static void start(Context context) {
        start(context, null);
    }

    public static void start(Context context, Intent extras) {
        Intent intent = new Intent();
        intent.setClass(context, HomeTabAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        if (extras != null) {
            intent.putExtras(extras);
        }
        context.startActivity(intent);
    }

    public static void startForAdvert(Context context, String fromAdvert) {
        Intent intent = new Intent();
        intent.setClass(context, HomeTabAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("fromAdvert", fromAdvert);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        //Tabbar设置
        setContentView(R.layout.activity_home_tab_layout);
        StatusbarUtils.setStatusBarDarkMode(this, StatusbarUtils.statusbarlightmode(this));
        setSwipeBackEnable(false);

        mHomeTabPresent = new HomeTabPresent(HomeTabAct.this, findView(R.id.rootLayout), findView(R.id.tabhost), getSupportFragmentManager());
        queryNotReadCount();

        AddressCheckService.checkAddressVersion(this);
        initData();

        //弹窗处理
        getHandler().postDelayed(this::startUpAppCountByUser, 1000);

        initTempIdentity();
        EventBus.getDefault().register(this);
        Log.d("HomeTabAct","HomeTabAct onCreate");
        BuglyScenceManger.getInstance().setCarshUserDate(this);
    }



    private void initData() {
        //处理广告的本地跳转
        onParseIntentForAdvert();

        //处理通知消息过来的
        onParseIntent();

        initScreenAdvertisData();

        mHomeIMInitor = new HomeIMInitor(this, (DropCover) findViewById(R.id.unread_cover), item -> mHomeTabPresent.refreshUnReadTip(item));
        App.firstEnter = false;
        //注册未读消息、消息数目、新消息到达的监听
        mHomeIMInitor.registerObserver(true);

        //同步数据
        syncIMData();

        //请求最近未读的消息
        mHomeIMInitor.requestRecentMessage();

        //注册网络更改广播
        registerNetBroadCast();

        //获取app内控件文案
        TextConfigCacheManager.getInstance(this).updateOMVersionTextOfWidget();

        LogUtil.d("timeStamp", System.currentTimeMillis() + "");

    }


    /**
     * 开屏广告处理
     */
    private void initScreenAdvertisData() {
        int width = AppUtils.getWidth(HomeTabAct.this);
        int height = AppUtils.getHeight(HomeTabAct.this);
        OMAppApiProvider.getInstance().openScreenAdvertis(width, height, new rx.Observer<ScreenAdvertisResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(ScreenAdvertisResponse baseResponse) {
                String saveImageDir = ScreenAdvertisInfoManager.AD_IMAGE_DIR;
                String imageFileName = ImageUtil.getImgName(ScreenAdvertisInfoManager.getImageFileName(HomeTabAct.this));
                if (baseResponse.isSuccess()) {
                    if (baseResponse.data == null || StringUtil.isEmpty(baseResponse.data.imgUrl)) {
                        //后台传的url为空
                        FileUtil.removeFile(saveImageDir);
                    } else {
                        //根据本地保存的url 是否为空 和 新的一样判断是否新保存图片
                        String remoteImgFileName = ImageUtil.getImgName(baseResponse.data.imgUrl);
                        if (!imageFileName.equals(remoteImgFileName)) {
                            try {
                                FileUtil.removeFile(saveImageDir);
                            } catch (Exception ex) {
                                Log.d(HomeTabAct.class.getName(), ex.getStackTrace() + ex.getMessage());
                            }

                        }
                        ScreenAdvertisInfoManager.setmScreenAdvertisInfo(HomeTabAct.this, baseResponse.data);
                        saveAdData();

                        Message msg = Message.obtain();
                        //消息的标识
                        msg.what = 1;
                        // 通过Handler发送消息到其绑定的消息队列
                        workHandler.sendMessage(msg);

                    }
                }
            }
        });
    }

    /**
     * 启动页广告相关下载／保存处理异步线程
     */
    Handler workHandler;
    HandlerThread mHandlerThread;

    private void saveAdData() {
        mHandlerThread = new HandlerThread("workThread");
        mHandlerThread.start();
        workHandler = new Handler(mHandlerThread.getLooper()){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                mBitmap = ImageUtil.getImageFromNet(ScreenAdvertisInfoManager.getImageUrl(HomeTabAct.this));
                if (mBitmap != null) {
                    checkStroragePermission(HomeTabAct.this, new PermissionCallback() {
                        @Override
                        public void onGranted() {
                            String saveImageDir = ScreenAdvertisInfoManager.AD_IMAGE_DIR;
                            String imageFileName = ImageUtil.getImgName(ScreenAdvertisInfoManager.getImageFileName(HomeTabAct.this));
                            ImageUtil.saveImageToGallery(mBitmap, saveImageDir, imageFileName);
                        }

                        @Override
                        public void onDenied() {
                        }
                    });

                }
                // 退出消息循环
                mHandlerThread.quit();
                // 防止Handler内存泄露 清空消息队列
                workHandler.removeCallbacks(null);

            }
        };
    }

    /**
     * 检测手机存储权限
     *
     * @param mContext
     * @param permissionCallback
     */
    public static void checkStroragePermission(Context mContext, PermissionCallback permissionCallback) {
        AndPermission.with(mContext)
                .permission(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                )
                .rationale((context, permissions, executor) -> {
                    executor.execute();
                })
                // 用户给权限了
                .onGranted(permissions -> permissionCallback.onGranted()
                )
                // 用户拒绝权限，包括不再显示权限弹窗也在此列
                .onDenied(permissions -> {
                    // 判断用户是不是不再显示权限弹窗了，是的话进入权限设置页
                    if (AndPermission.hasAlwaysDeniedPermission(mContext, permissions)) {
                        // 打开权限设置页
                        AndPermission.permissionSetting(mContext).execute();
                    } else {
                        permissionCallback.onDenied();
                    }
                })
                .start();
    }

    /**
     * 权限验证状态回调接口
     * by houxianjun
     */
    public interface PermissionCallback {
        /**
         * 同意
         */
        void onGranted();

        /**
         * 拒绝
         */
        void onDenied();
    }


    /**
     * 用户启动APP累计次数(每个自然日)；并处理广告弹窗
     */
    private void startUpAppCountByUser() {
        OMAppApiProvider.getInstance().startUpAppCountByUser(new rx.Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
                if (!AppConfig.isHomePopShown) {
                    AppConfig.isHomePopShown = true;
                    getHandler().post(() -> showPop());
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(OMBaseResponse omBaseResponse) {

            }
        });
    }

    private void showPop() {
        OMAppApiProvider.getInstance().homePop(new rx.Observer<HomePopResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(HomePopResponse response) {
                if (needUpgrade) {
                    needUpgrade = false;
                    return;
                }
                if (response.isSuccess() && !TextUtils.isEmpty(response.data.adUrl) && needShowPop) {
                    myPopupWindow = new HomePoPView(HomeTabAct.this, response.data);
                    myPopupWindow.showAtLocation(findView(R.id.rootLayout), Gravity.CENTER, 0, 0);
                    myPopupWindow.lightOff();
                    needShowPop = false;
                } else {

                }
            }
        });

    }


    /**
     * 同步IM数据
     */
    private void syncIMData() {
        // 等待同步数据完成
        boolean syncCompleted = LoginSyncDataStatusObserver.getInstance().observeSyncDataCompletedEvent((Observer<Void>) v -> dismissProgressHUD());
        if (!syncCompleted) {
            showProgressHUD(HomeTabAct.this, getString(R.string.prepare_data));
        }
    }

    /**
     * ***************************************** 处理通知跳转 *****************************************
     */
    @Override
    protected void onNewIntent(Intent intent) {
        if (intent.hasExtra(NimIntent.EXTRA_NOTIFY_CONTENT)) {
            ArrayList<IMMessage> messages = (ArrayList<IMMessage>) intent.getSerializableExtra(NimIntent.EXTRA_NOTIFY_CONTENT);
            if (messages != null) {
                if (messages.size() > 1) {
                    intent.putExtra(NimIntent.EXTRA_NOTIFY_CONTENT, "");
                } else {
                    intent.putExtra(NimIntent.EXTRA_NOTIFY_CONTENT, messages.get(0));
                }
            }
        }
        setIntent(intent);
        onParseIntent();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.clear();
    }

    /**
     * 处理跳转过来的消息
     */
    private void onParseIntent() {
        Intent intent = getIntent();
        //个推
        if (intent.hasExtra("detail") && intent.hasExtra("module") && intent.hasExtra("action")) {
            String detail = intent.getStringExtra("detail");
            String module = intent.getStringExtra("module");
            String action = intent.getStringExtra("action");
            getHandler().postDelayed(() -> PushNotificationRouter.doPushJump(HomeTabAct.this, module, detail, action), 200);
        }

        Uri uri = intent.getData();
        if (uri != null) {
            String p = uri.getQueryParameter(PushConfig.QUERY_PARAMETER);
        }
    }

    private void onParseIntentForAdvert() {
        String fromType = getIntent().getStringExtra("fromAdvert");
        if (FROM_ADVERT.equals(fromType)) {
            AppRouter.doJumpAd(this, ScreenAdvertisInfoManager.getImageJumpType(this),
                    ScreenAdvertisInfoManager.getImageElementKey(this)
                    , ScreenAdvertisInfoManager.getImageelColumnId(this)
                    , ScreenAdvertisInfoManager.getImageLink(this)
                    , ""
                    , ScreenAdvertisInfoManager.getImageAdLinkId(this)
                    , ScreenAdvertisInfoManager.getImageAdLinkLiveId(this)
                    , ScreenAdvertisInfoManager.getImageIsPw(this));
            hasSkipToAd = true;
        }
    }

    /**
     * 注册网络变化广播
     */
    private void registerNetBroadCast() {
        mNetChangeReceiver = new NetChangeReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        this.registerReceiver(mNetChangeReceiver, filter);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPublicStringEvent(PublicStringEvent publicStringEvent) {
        if (publicStringEvent.eventTitle.equals(PublicStringEvent.LOGOUT_SUCCESS)) {
            mHomeTabPresent.setCurrentTab(0);
        }

        if (publicStringEvent.eventTitle.equals(PublicStringEvent.INVITE_JOIN_MEETING)) {
            if (mHomeTabPresent != null) {
                mHomeTabPresent.refreshInviteMessage();
            }
        }

        if (publicStringEvent.eventTitle.equals(PublicStringEvent.ROLE_CHANGE)) {
            mHomeTabPresent.onRefreshUserRole();
        }
    }

    boolean needUpgrade = false;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateEvent(UpdateEvent updateEvent) {
        if (updateEvent.needUpgrade) {
            needUpgrade = true;
            if (myPopupWindow != null) {
                needUpgrade = false;
                myPopupWindow.dismiss();
            }
        } else if (updateEvent.showPop) {
            showPop();
        }
    }


    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mHomeTabPresent != null) {
            if (mHomeTabPresent.currentTabIndex == 3) {
                setStatusBarLightMode();
            } else {
                setStatusBarDarkMode();
                if (mHomeTabPresent.currentTabIndex == 0){
                    // 首页每次可见都请求一下强更接口
                    UpdateManager.getInstance().checkUpdate(this, false);
                }
            }
        }

        //注册推送服务
        PushManager.getInstance().initialize(this.getApplicationContext(), GTPushService.class);
        PushManager.getInstance().registerPushIntentService(this.getApplicationContext(), GTPushIntentService.class);

        if (AppConfig.KICK_OUT) {
            RouterJumpUtil.startLoginActivity(true);
            EventBus.getDefault().post(new PublicStringEvent(PublicStringEvent.LOGOUT_SUCCESS));
        }

        KeyboardControlUtil.closeKeyboard((EditText) findViewById(R.id.edit), this);

        if (hasSkipToAd && !needShowPop) {
            hasSkipToAd = false;
            needShowPop = true;
            showPop();
        }
        mHomeTabPresent.onRefreshUserRole();
    }

    /**
     * 检查权限处理和系统消息是否有未读消息个数
     */
    private void queryNotReadCount() {
        mHomeTabPresent.refreshInviteMessage();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.d("HomeTabAct","HomeTabAct onDestroy");
        mHomeIMInitor.registerObserver(false);
        if (myPopupWindow != null) {
            myPopupWindow.dismiss();
        }
        if (mNetChangeReceiver != null) {
            unregisterReceiver(mNetChangeReceiver);
        }
        EventBus.getDefault().unregister(this);
    }


    /**
     * 更新deviceID
     */
    private void initTempIdentity() {
        if (TextUtils.isEmpty(BaseUserInfoCache.getDeviceId(this))) {
            OMAppApiProvider.getInstance().getDeviceId(this, new rx.Observer<CheckDeviceResponse>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                }

                @Override
                public void onNext(CheckDeviceResponse checkDeviceResponse) {
                    if (checkDeviceResponse.isSuccess() && checkDeviceResponse.data != null) {
                        BaseUserInfoCache.setDeviceId(HomeTabAct.this, checkDeviceResponse.data.deviceId);
                    }
                }
            });
        } else {
            OMAppApiProvider.getInstance().updateDeviceId(this, BaseUserInfoCache.getDeviceId(this), new rx.Observer<CheckDeviceResponse>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onNext(CheckDeviceResponse checkDeviceResponse) {
                }
            });
        }
    }

    boolean needShowPop = true;
    boolean hasSkipToAd = false;

    @Override
    protected void onPause() {
        super.onPause();
        needShowPop = false;
    }
}
