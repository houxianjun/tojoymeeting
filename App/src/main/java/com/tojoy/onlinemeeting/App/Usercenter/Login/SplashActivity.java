package com.tojoy.onlinemeeting.App.Usercenter.Login;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.viewpager.VerticleViewPager;
import com.tojoy.tjoybaselib.ui.viewpager.VerticleViewPagerAdapter;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.onlinemeeting.App.Home.Controller.HomeTabAct;
import com.tojoy.tjoybaselib.ui.CircleIndicator.CircleIndicator;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;
import com.tojoy.onlinemeeting.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 */
public class SplashActivity extends UI {

    private VerticleViewPager mViewPager;
    private ImageView mIvNext;
    //暂时不要删除
    private CircleIndicator mIndicator;
    private ImageButton mIbInto;
    private Animation animationBottom;
    private int preIndex = 0;
    private int DSI = 0;

    Timer mTimer;
    Handler mTimerHandler;

    final int skipSecond = 4;

    int mSecond = skipSecond;

    public static void start(Context context) {
        if (context == null) return;
        Intent intent = new Intent(context, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setStatusBar();
        setSwipeBackEnable(false);
        mViewPager = (VerticleViewPager) findViewById(R.id.splash_vp);
        mIndicator = (CircleIndicator) findViewById(R.id.splash_indicator);
        mIbInto = (ImageButton) findViewById(R.id.splash_into);
        mIvNext = (ImageView) findViewById(R.id.t1_next);
        mViewPager.setOffscreenPageLimit(2);
        initEvent();
        animal(preIndex);
        startCountDown();
    }

    private void initEvent() {
        mViewPager.setAdapter(new SplashPagerAdapter(this));
        //  mIndicator.setViewPager(mViewPager);
        mViewPager.setOnPageChangeListener(new VerticleViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {


            }

            @Override
            public void onPageScrollStateChanged(int state) {


                switch (state) {
                    case VerticleViewPager.SCROLL_STATE_DRAGGING:
                        DSI += 1;
                        stopCountDown();
                        break;
                    case VerticleViewPager.SCROLL_STATE_SETTLING:
                        DSI += 2;
                        break;
                    case VerticleViewPager.SCROLL_STATE_IDLE:
                        if (mViewPager.getCurrentItem() == mViewPager.getAdapter().getCount() - 1 && DSI == 1) {
                            skipTonNext();
                        }
                        DSI = 0;
                        startCountDown();
                        break;
                    default:
                        break;
                }

            }
        });
    }

    private static class SplashPagerAdapter extends VerticleViewPagerAdapter {
        private Context mContext;
        private int[] pics0 = {R.drawable.welcome_bg};
        private int[] pics1 = {R.drawable.welcome_bg};


        public SplashPagerAdapter(Context contexts) {
            this.mContext = contexts;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.view_splash_pic, container, false);
            ImageView background = view.findViewById(R.id.iv_item_background);
            if (position == 0) {
                background.setImageResource(pics0[0]);
            } else if (position == 1) {
                background.setImageResource(pics1[0]);
            }
            container.addView(view);
            return view;
        }
    }

    @Override
    public void finish() {
        super.finish();
        if (mTimer != null) {
            mTimer.cancel();
        }
        overridePendingTransition(0, 0);
    }

    private void skipTonNext() {
        stopCountDown();
        if (UserInfoManager.isLogined(SplashActivity.this)) {
            HomeTabAct.start(SplashActivity.this);
            finish();
        } else {
            RouterJumpUtil.startLoginActivity();
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        skipTonNext();
        return super.onKeyDown(keyCode, event);
    }

    private void animal(int position) {
        switch (position) {
            case 0:
                if (preIndex > position) {
                }
                animationBottom = AnimationUtils.loadAnimation(this,
                        R.anim.tutorail_bottom);
                mIvNext.startAnimation(animationBottom);
                break;
            default:
                break;
        }
        preIndex = position;
    }


    /**
     * 倒计时
     */
    @SuppressLint("HandlerLeak")
    private void startCountDown() {
        if (mTimer != null) {
            mTimer.cancel();
        }

        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Message message = new Message();
                message.what = 1;
                mTimerHandler.sendMessage(message);
            }
        }, 1000, 1000);
        mTimerHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        if (mSecond <= 0) {
                            mTimer.cancel();
                            mSecond = skipSecond;
                            autoSkip();
                            return;
                        }
                        mSecond--;
                        break;
                    default:
                        break;
                }
                super.handleMessage(msg);
            }
        };
    }


    private void stopCountDown() {
        if (mTimer != null) {
            mTimer.cancel();
        }

        mSecond = skipSecond;
    }

    private void autoSkip() {
        int currentIndex = mViewPager.getCurrentItem();

        if (currentIndex < 1) {
            mViewPager.setCurrentItem(currentIndex + 1);
            startCountDown();
        } else {
            skipTonNext();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopCountDown();
    }

}
