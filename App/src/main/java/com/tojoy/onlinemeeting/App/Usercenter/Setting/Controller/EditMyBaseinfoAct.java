package com.tojoy.onlinemeeting.App.Usercenter.Setting.Controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RelativeLayout;

import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.model.user.UserProfileModel;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.services.oss.OSSService;
import com.tojoy.tjoybaselib.services.oss.ProgressCallback;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.onlinemeeting.App.Base.adapter.BaseSubmitModel;
import com.tojoy.onlinemeeting.App.Base.adapter.SubmitAdapter;
import com.tojoy.onlinemeeting.App.Usercenter.Setting.Model.EditMyInfoModel;
import com.tojoy.onlinemeeting.App.Usercenter.Setting.View.EditUserInfoHeader;
import com.tojoy.onlinemeeting.R;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;
import com.tojoy.cloud.online_meeting.App.Apply.activity.ApplyAct;
import com.tojoy.cloud.online_meeting.App.Apply.activity.EditRoomNameAct;
import com.tojoy.common.Services.ParamsPicker.BasePickerAct;
import com.tojoy.image.photopicker.Extras;
import com.tojoy.image.photopicker.PickImageHelper;
import com.tojoy.provider.OSSFileNameProvider;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * Created by chengyanfang on 2017/10/14.
 * 个人中心界面
 */

public class EditMyBaseinfoAct extends BasePickerAct {
    private static final int REQUEST_PICK_ICON = 104;
    RelativeLayout mContainerLayout;
    TJRecyclerView mRefreshableRecyclerView;
    SubmitAdapter mAdapter;
    EditUserInfoHeader mEditUserInfoHeader;

    ArrayList<BaseSubmitModel> mDatas = new ArrayList<>();

    boolean isCanEdit;

    EditMyInfoModel editMyInfoModel;

    public static void start(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, EditMyBaseinfoAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_edit_userinfo_layout);
        setStatusBar();
        editMyInfoModel = new EditMyInfoModel();
        isCanEdit = TextUtils.isEmpty(UserInfoManager.getCompanyCode(this));
        initTitle();
        initUI();
        initData();
    }


    private void initTitle() {
        setUpNavigationBar();
        setTitle("个人信息");
        showBack();
    }

    private void initUI() {
        mContainerLayout = (RelativeLayout) findViewById(R.id.container_layout);
        mRefreshableRecyclerView = new TJRecyclerView(this);
        mRefreshableRecyclerView.setRefreshable(false);
        mContainerLayout.addView(mRefreshableRecyclerView);

        mEditUserInfoHeader = new EditUserInfoHeader(this);
        mRefreshableRecyclerView.addHeaderView(mEditUserInfoHeader.getView());
        mEditUserInfoHeader.getView().setOnClickListener(v -> showSelector(R.string.set_head_image, REQUEST_PICK_ICON));
    }

    private void initData() {
        mDatas.add(new BaseSubmitModel(BaseSubmitModel.CellType.BLANKVIEW));
        String name = "";
        if (!TextUtils.isEmpty(BaseUserInfoCache.getUserName(this))) {
            name = BaseUserInfoCache.getUserName(this);
        } else {
            name = "去编辑";
        }

        //姓名
        if (!isCanEdit) {
            mDatas.add(new BaseSubmitModel(BaseSubmitModel.CellType.SELETETYPE, getString(R.string.user_name), UserInfoManager.getUserNickName(this))
                    .setSelectHint(name)
                    .setHideRightArrow(true));
        } else {
            mDatas.add(new BaseSubmitModel(BaseSubmitModel.CellType.SELETETYPETHREE, getString(R.string.user_name), UserInfoManager.getUserNickName(this))
                    .setSelectHint(name)
                    .setHideRightArrow(false));
        }

        //账号
        mDatas.add(new BaseSubmitModel(BaseSubmitModel.CellType.SELETETYPE, getString(R.string.user_account), UserInfoManager.getUserMobile(this)).setHideRightArrow(true));
        if (!isCanEdit) {
            //企业归属
            mDatas.add(new BaseSubmitModel(BaseSubmitModel.CellType.SELETETYPE, getString(R.string.user_enterprise), UserInfoManager.getCompanyName(this)).setHideRightArrow(true));
        }

        mAdapter = new SubmitAdapter(this, mDatas);
        mRefreshableRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(index -> {
            if (index == 1 && isCanEdit) {
                startActivityForResult(new Intent(this, EditRoomNameAct.class)
                                .putExtra("title", "请输入姓名")
                                .putExtra("type", "editUserName")
                                .putExtra("loaclData", UserInfoManager.getUserName(EditMyBaseinfoAct.this))
                        , ApplyAct.REQUEST_ROOM_NAME);
            }
        });
    }


    /**
     * 选择图片
     */
    private void showSelector(int titleId, final int requestCode) {
        PickImageHelper.PickImageOption option = new PickImageHelper.PickImageOption();
        option.titleResId = titleId;
        option.multiSelect = false;
        option.crop = true;
        option.cropOutputImageWidth = 720;
        option.cropOutputImageHeight = 720;
        PickImageHelper.pickImage(EditMyBaseinfoAct.this, requestCode, option);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case REQUEST_PICK_ICON:
                String path = data.getStringExtra(Extras.EXTRA_FILE_PATH);
                OSSService.getInterface().initOSSService(new OSSService.InitOSSCallBack() {
                    @Override
                    public void onInitComplete() {
                        uploadImgToOSS(path);
                    }

                    @Override
                    public void onInitFail() {
                        Toasty.normal(EditMyBaseinfoAct.this, getString(R.string.oss_init_fail)).show();
                    }
                });
                break;
            default:
                break;
        }
    }


    /**
     * 上传图片至OSS
     */
    private void uploadImgToOSS(String path) {
        showProgressHUD(this, "修改中");
        String ossPath = OSSFileNameProvider.getImgFileName(path, OSSFileNameProvider.MODULE_USERCENTER, 1);
        OSSService.getInterface().uploadObject(ossPath, path, new ProgressCallback<PutObjectRequest, PutObjectResult>() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                updateHeaderImg(ossPath);
            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientException, ServiceException serviceException) {
                mHandler.post(() -> Toasty.normal(EditMyBaseinfoAct.this, getString(R.string.oss_init_fail)).show());
                dismissProgressHUD();
            }

            @Override
            public void onProgress(PutObjectRequest request, long currentSize, long totalSize) {
                Log.d("OSS-Android-SDK-WRITE", currentSize + "");
            }
        }, () -> uploadImgToOSS(path));
    }


    /**
     * 修改个人头像
     */
    private void updateHeaderImg(String path) {
        editMyInfoModel.headPortrait = path;
        String finalPath = path;
        OMAppApiProvider.getInstance().mEditUerImage(BaseUserInfoCache.getUserId(this), path, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {
                dismissProgressHUD();
                Toasty.normal(EditMyBaseinfoAct.this, "网络错误，请检查网络").show();
            }

            @Override
            public void onNext(OMBaseResponse baseResponse) {
                if (baseResponse.isSuccess()) {
                    Toasty.normal(EditMyBaseinfoAct.this, "修改成功").show();
                    mEditUserInfoHeader.updateUserInfoHeader(OSSConfig.getOSSURLedStr(finalPath));
                    UserInfoManager.saveAvatar(EditMyBaseinfoAct.this, OSSConfig.getOSSURLedStr(finalPath));
                } else {
                    Toasty.normal(EditMyBaseinfoAct.this, baseResponse.msg).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mDatas.size() > 0 && !TextUtils.isEmpty(BaseUserInfoCache.getUserName(this))) {
            mDatas.get(1).typeContent = BaseUserInfoCache.getUserName(this);
            mAdapter.notifyDataSetChanged();
        }
    }

}
