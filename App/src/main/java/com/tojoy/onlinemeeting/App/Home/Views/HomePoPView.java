package com.tojoy.onlinemeeting.App.Home.Views;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.model.home.HomePopResponse;
import com.tojoy.tjoybaselib.services.imageManager.ImageLoaderManager;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.onlinemeeting.R;
import com.tojoy.common.Services.Router.AppRouter;

/**
 * Created by luuzhu on 2018/6/21.
 * 首页广告pop
 */

public class HomePoPView extends PopupWindow {

    private double aspectRatio;
    private String url;
    private View mView;
    private Activity mContext;
    private LinearLayout mAdLayout;
    private ImageView mPopClose;
    private TranslateAnimation mAnim;//关闭按钮动画
    private HomePopResponse.DataObjBean mDataObjBean;

    public HomePoPView(Context context, HomePopResponse.DataObjBean dataObjBean) {
        super(context);
        mContext = (Activity) context;
        this.url = dataObjBean.adUrl;
        this.aspectRatio = dataObjBean.aspectRatio;
        this.mDataObjBean = dataObjBean;
        initUI();
    }

    private void initUI() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.popup_home, null);
        setContentView(mView);
        mAdLayout = mView.findViewById(R.id.adLayout);
        mPopClose = mView.findViewById(R.id.popClose);
        setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        setAnimationStyle(R.style.pop_anim_scale_alpha);
        setBackgroundDrawable(new ColorDrawable());
        setFocusable(true);
        setOutsideTouchable(false);
        setTouchable(true);
        mAnim = new TranslateAnimation(0, 0, -200, 0);
        setData();
        initListener();
    }

    private void setData() {

        String picPath = OSSConfig.getOSSURLedStr(url);
        ImageView imageView = new ImageView(mContext);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        //============计算宽高   手机宽度*0.88
        layoutParams.width = (int) (AppUtils.getWidth(mContext) * 0.88);
        layoutParams.height = (int) (layoutParams.width / aspectRatio);
        imageView.setLayoutParams(layoutParams);

        ImageLoaderManager.INSTANCE.loadImage(mContext, imageView, OSSConfig.getRemoveStylePath(OSSConfig.getOSSURLedStr(picPath)), AppUtils.dip2px(mContext,5));
        mAdLayout.addView(imageView);

    }

    /**
     * 显示时屏幕变暗
     */
    public void lightOff() {
        WindowManager.LayoutParams layoutParams = mContext.getWindow().getAttributes();
        layoutParams.alpha = 0.5f;
        mContext.getWindow().setAttributes(layoutParams);
        showCloseIcon();
    }

    private void showCloseIcon() {
        new Handler().postDelayed(() -> {
            mAnim.setInterpolator(new BounceInterpolator());
            mAnim.setDuration(500);
            mPopClose.startAnimation(mAnim);
            mPopClose.setVisibility(View.VISIBLE);
        }, 600);
    }

    private void initListener() {
        /**
         * 消失时屏幕变亮
         */
        setOnDismissListener(() -> {
            WindowManager.LayoutParams layoutParams = mContext.getWindow().getAttributes();
            layoutParams.alpha = 1.0f;
            mContext.getWindow().setAttributes(layoutParams);
        });
        mPopClose.setOnClickListener(view -> dismiss());

        mAdLayout.setOnClickListener(view ->{
            AppRouter.doJumpAd(mContext, mDataObjBean.adLinkType,
                    mDataObjBean.adLinkKey
                    , ""
                    , mDataObjBean.adLinkUrl
                    ,  ""
                    , mDataObjBean.adLinkId
                    , mDataObjBean.adLinkLiveId
                    , mDataObjBean.isPw());
            dismiss();
        });
    }
}
