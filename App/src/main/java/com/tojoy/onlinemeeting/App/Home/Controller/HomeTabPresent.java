package com.tojoy.onlinemeeting.App.Home.Controller;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import com.netease.nim.uikit.api.model.user.SystemNotice;
import com.netease.nim.uikit.business.session.audio.MessageAudioControl;

import com.tojoy.common.Services.EventBus.PublicStringEvent;
import com.tojoy.tjoybaselib.model.home.MineDistributionResponse;
import com.tojoy.tjoybaselib.model.user.LoginResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.util.net.NetWorkUtils;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.sys.StatusbarUtils;
import com.tojoy.tjoybaselib.util.time.TimeUtil;
import com.tojoy.onlinemeeting.App.Usercenter.Home.MineFragment;
import com.tojoy.onlinemeeting.NIM.reminder.ReminderItem;
import com.tojoy.onlinemeeting.R;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;
import com.tojoy.onlinemeeting.Service.SystemNotice.SystemMsgDBManager;
import com.tojoy.cloud.online_meeting.App.Meeting.activity.MeetingCenterFragment;
import com.tojoy.cloud.online_meeting.App.Meeting.activity.MeetingFragment;
import com.tojoy.common.Services.Push.SystemNoticeType;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import rx.Observer;


/**
 * 承载首页的Tab及Fragment处理
 */
public class HomeTabPresent implements TabHost.OnTabChangeListener {

    Activity mContext;
    int currentTabIndex = 0;

    int currentStr = R.string.tab_home;

    FragmentTransaction mFragmentTransaction;
    FragmentManager mFragmentManager;
    TabHost mTabHost;

    MeetingFragment mMeetingFragment;
    NewsFragment mNewsFragment;
    MeetingCenterFragment mMeetingCenterFragment;
    MineFragment mUsercenterFragment;
    //缓存页
    MeetingFragment mCacheMeetingFragment;
    NewsFragment mCacheNewsFragment;
    MeetingCenterFragment mCacheMeetingCenterFragment;
    MineFragment mCacheUsercenterFragment;

    TextView homeTv, meetingCenterTv, userCenterTv, shopTv;
    ImageView homeIv, meetingCenterIv, userCenterIv, shopIv;
    View mParentView;

    //小红点
    RelativeLayout mRedtipLayout;
    TextView mRedTipTv;

    private View homeView;
    private View meetingCenterView;
    private View usercenterView;
    private View shopView;

    public HomeTabPresent(Activity activity, View view, TabHost tabHost, FragmentManager fragmentManager) {
        mContext = activity;
        mTabHost = tabHost;
        mFragmentManager = fragmentManager;
        mParentView = view;
        initFragment();
        initTab();
    }

    private void initFragment() {
        this.mMeetingFragment = (MeetingFragment) Fragment
                .instantiate(mContext, MeetingFragment.class.getName(), null);
        this.mNewsFragment = (NewsFragment) Fragment.instantiate(mContext, NewsFragment.class.getName(), null);
        this.mMeetingCenterFragment = (MeetingCenterFragment) Fragment.instantiate(mContext, MeetingCenterFragment.class.getName(), null);
        this.mUsercenterFragment = (MineFragment) Fragment
                .instantiate(mContext, MineFragment.class.getName(), null);
    }

    private void initTab() {
        mTabHost.setup();
        mTabHost.setBackgroundResource(R.drawable.tojoy_tab_bg);
        mTabHost.setOnTabChangedListener(this);

        FrameLayout layout = (FrameLayout) this.mTabHost.getChildAt(0);
        TabWidget tw = (TabWidget) layout.getChildAt(2);

        homeView = LayoutInflater.from(mContext)
                .inflate(R.layout.tab_home_layout, tw, false);
        meetingCenterView = LayoutInflater.from(mContext)
                .inflate(R.layout.tab_meeting_center_layout, tw, false);
        shopView = LayoutInflater.from(mContext)
                .inflate(R.layout.tab_shop_layout, tw, false);
        usercenterView = LayoutInflater.from(mContext)
                .inflate(R.layout.tab_usercenter_layout, tw, false);

        homeTv = homeView.findViewById(R.id.erp_title);
        meetingCenterTv = meetingCenterView.findViewById(R.id.meeting_center_title);
        shopTv = shopView.findViewById(R.id.shop_title);
        userCenterTv = usercenterView.findViewById(R.id.usercenter_title);

        homeIv = homeView.findViewById(R.id.recommend_img);
        meetingCenterIv = meetingCenterView.findViewById(R.id.meeting_center_img);
        shopIv = shopView.findViewById(R.id.shop_img);
        userCenterIv = usercenterView.findViewById(R.id.usercenter_img);

        mRedtipLayout = shopView.findViewById(R.id.rlv_needpay_redtip);
        mRedTipTv = shopView.findViewById(R.id.tv_needpay_num);

        addTab(tw, R.string.tab_home, homeView);
        addTab(tw, R.string.tab_meeting_center, meetingCenterView);
        addTab(tw, R.string.tab_new, shopView);
        addTab(tw, R.string.tab_usercenter, usercenterView);

        mTabHost.setCurrentTab(0);
    }

    private void addTab(TabWidget tw, int strId, View aView) {
        TabHost.TabSpec home = mTabHost.newTabSpec(mContext.getString(strId));
        home.setIndicator(aView);
        home.setContent(tag -> {
            View v = new View(mContext);
            return v;
        });
        mTabHost.addTab(home);
    }

    @Override
    public void onTabChanged(String tabId) {
        NetWorkUtils.checkNet(mContext);
        this.mFragmentTransaction = this.mFragmentManager.beginTransaction();
        this.getCacheFragement();
        if (this.isEquStr(tabId, R.string.tab_home)) {
            this.setCurrentFragment(mCacheMeetingFragment,
                    mMeetingFragment, R.string.tab_home);
            currentStr = R.string.tab_home;

            StatusbarUtils.setStatusBarDarkMode(mContext, StatusbarUtils.statusbarlightmode(mContext));
            mTabHost.setBackgroundResource(R.drawable.tojoy_tab_bg);

            homeTv.setTextColor(mContext.getResources().getColor(R.color.color_1290FF));
            meetingCenterTv.setTextColor(mContext.getResources().getColor(R.color.color_91969e));
            shopTv.setTextColor(mContext.getResources().getColor(R.color.color_91969e));
            userCenterTv.setTextColor(mContext.getResources().getColor(R.color.color_91969e));

            homeIv.setBackgroundResource(R.drawable.tj_tab_icon_home_select);
            meetingCenterIv.setBackgroundResource(R.drawable.tj_tab_icon_meeting_normal);
            shopIv.setBackgroundResource(R.drawable.tj_tab_icon_shop_normal);
            userCenterIv.setBackgroundResource(R.drawable.tj_tab_icon_me_normal);

            currentTabIndex = 0;
        } else if (this.isEquStr(tabId, R.string.tab_meeting_center)) {
            StatusbarUtils.setStatusBarDarkMode(mContext, StatusbarUtils.statusbarlightmode(mContext));

            this.setCurrentFragment(mCacheMeetingCenterFragment,
                    mMeetingCenterFragment, R.string.tab_meeting_center);

            currentStr = R.string.tab_meeting_center;
            homeTv.setTextColor(mContext.getResources().getColor(R.color.color_91969e));
            meetingCenterTv.setTextColor(mContext.getResources().getColor(R.color.color_1290FF));
            shopTv.setTextColor(mContext.getResources().getColor(R.color.color_91969e));
            userCenterTv.setTextColor(mContext.getResources().getColor(R.color.color_91969e));

            homeIv.setBackgroundResource(R.drawable.tj_tab_icon_home_normal);
            meetingCenterIv.setBackgroundResource(R.drawable.tj_tab_icon_meeting_select);
            shopIv.setBackgroundResource(R.drawable.tj_tab_icon_shop_normal);
            userCenterIv.setBackgroundResource(R.drawable.tj_tab_icon_me_normal);
            mTabHost.setBackgroundResource(R.drawable.tojoy_tab_bg);

            MessageAudioControl.getInstance(mContext).stopAudio();
            currentTabIndex = 1;

        } else if (this.isEquStr(tabId, R.string.tab_new)) {
            StatusbarUtils.setStatusBarDarkMode(mContext, StatusbarUtils.statusbarlightmode(mContext));

            this.setCurrentFragment(mCacheNewsFragment,
                    mNewsFragment, R.string.tab_new);

            currentStr = R.string.tab_new;

            homeTv.setTextColor(mContext.getResources().getColor(R.color.color_91969e));
            meetingCenterTv.setTextColor(mContext.getResources().getColor(R.color.color_91969e));
            shopTv.setTextColor(mContext.getResources().getColor(R.color.color_1290FF));
            userCenterTv.setTextColor(mContext.getResources().getColor(R.color.color_91969e));

            homeIv.setBackgroundResource(R.drawable.tj_tab_icon_home_normal);
            meetingCenterIv.setBackgroundResource(R.drawable.tj_tab_icon_meeting_normal);
            shopIv.setBackgroundResource(R.drawable.tj_tab_icon_shop_select);
            userCenterIv.setBackgroundResource(R.drawable.tj_tab_icon_me_normal);
            mTabHost.setBackgroundResource(R.drawable.tojoy_tab_bg);

            MessageAudioControl.getInstance(mContext).stopAudio();
            currentTabIndex = 2;

        } else if (this.isEquStr(tabId, R.string.tab_usercenter)) {
            StatusbarUtils.enableTranslucentStatusbar(mContext);
            StatusbarUtils.setStatusBarLightMode(mContext, StatusbarUtils.statusbarlightmode(mContext));
            this.setCurrentFragment(mCacheUsercenterFragment,
                    mUsercenterFragment, R.string.tab_usercenter);

            currentStr = R.string.tab_usercenter;

            homeTv.setTextColor(mContext.getResources().getColor(R.color.color_91969e));
            meetingCenterTv.setTextColor(mContext.getResources().getColor(R.color.color_91969e));
            shopTv.setTextColor(mContext.getResources().getColor(R.color.color_91969e));
            userCenterTv.setTextColor(mContext.getResources().getColor(R.color.color_1290FF));

            homeIv.setBackgroundResource(R.drawable.tj_tab_icon_home_normal);
            meetingCenterIv.setBackgroundResource(R.drawable.tj_tab_icon_meeting_normal);
            shopIv.setBackgroundResource(R.drawable.tj_tab_icon_shop_normal);
            userCenterIv.setBackgroundResource(R.drawable.tj_tab_icon_me_select);
            mTabHost.setBackgroundResource(R.drawable.tojoy_tab_bg);

            MessageAudioControl.getInstance(mContext).stopAudio();
            currentTabIndex = 3;
        }

        this.mFragmentTransaction.commitAllowingStateLoss();
        this.mFragmentManager.executePendingTransactions();
    }

    private void getCacheFragement() {
        this.mCacheMeetingFragment = (MeetingFragment) getCacheFragment(R.string.tab_home);
        this.mCacheUsercenterFragment = (MineFragment) getCacheFragment(R.string.tab_usercenter);
        this.mCacheNewsFragment = (NewsFragment) getCacheFragment(R.string.tab_new);
        this.mCacheMeetingCenterFragment = (MeetingCenterFragment) getCacheFragment(R.string.tab_meeting_center);

        this.hideFragment(mCacheMeetingFragment);
        this.hideFragment(mCacheNewsFragment);
        this.hideFragment(mCacheUsercenterFragment);
        this.hideFragment(mCacheMeetingCenterFragment);
    }

    private Fragment getCacheFragment(int resId) {
        return this.mFragmentManager.findFragmentByTag(mContext.getString(resId));
    }

    private void hideFragment(Fragment aHideFragment) {
        if (aHideFragment != null) {
            this.mFragmentTransaction.hide(aHideFragment);
        }
    }

    private boolean isEquStr(String tabId, int resId) {
        return TextUtils.equals(tabId, mContext.getString(resId));
    }

    private void setCurrentFragment(Fragment aCache, Fragment aCurrt, int resId) {
        if (aCache == null) {
            this.mFragmentTransaction.add(R.id.realtabcontent, aCurrt,
                    mContext.getString(resId));
        } else {
            mFragmentTransaction.show(aCache);
        }
    }


    /**
     * 设置当前选中栏目
     *
     * @param tabIndex
     */
    public void setCurrentTab(int tabIndex) {
        mTabHost.setCurrentTab(tabIndex);
        BaseUserInfoCache.setCurrentTabIndex(tabIndex, mContext);
    }

    /**
     * 更改用户角色
     */
    public void onRefreshUserRole() {
        OMAppApiProvider.getInstance().mGetUserInfo(UserInfoManager.getUserId(mContext), new Observer<LoginResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(LoginResponse baseResponse) {
                if (baseResponse.isSuccess()) {
                    // 没保存之前先判断一下自己分销客的身份是否改变（如果改变 && 正在直播间  需要展示分销按钮）
                    UserInfoManager.saveUserInfo(mContext, baseResponse.data);
                    if (mUsercenterFragment != null) {
                        mUsercenterFragment.updateUserInfo();
                    }
                    //根据是否分销客的身份显示背景
                    if (BaseUserInfoCache.getiSDistributor(mContext)) {
                        queryDistributionData();
                    }

                    if (mMeetingCenterFragment != null) {
                        mMeetingCenterFragment.refreshData();
                    }
                    // 只要身份改变，需要发送eventBus（在直播间内的话动态显示/隐藏）
                    EventBus.getDefault().post(new PublicStringEvent(PublicStringEvent.DISTRIBUTOR_CHANGE));
                } else {
                    if ("20106".equals(baseResponse)) {
                        RouterJumpUtil.startLoginActivity();
                    }
                }
            }
        });
    }

    /**
     * 我的收益、我的会员（推广员身份可调用接口）
     */
    private void queryDistributionData() {
        OMAppApiProvider.getInstance().getDistributionMemberInfo(new rx.Observer<MineDistributionResponse>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(MineDistributionResponse response) {
                if (response.isSuccess()) {
                    BaseUserInfoCache.setUserDistributionData(mContext, response.data);
                    if (mUsercenterFragment != null) {
                        mUsercenterFragment.updateDistributionData();
                    }
                }
            }
        });
    }


    public void refreshInviteMessage() {
        if (mNewsFragment != null) {
            int count = SystemMsgDBManager.getInterface().getUnReadNoticeCntByType(mContext, SystemNoticeType.SYSTEM_NOTICE);
            String content = "";
            String time = "";
            if (count > 0) {
                List<SystemNotice> resultList = SystemMsgDBManager.getInterface().getNoticeListByType(mContext, 1, SystemNoticeType.SYSTEM_NOTICE);
                if (resultList.size() > 0) {
                    content = resultList.get(0).content;
                    time = TimeUtil.getDateTimeString(Long.parseLong(resultList.get(0).timestamp), "yyyy-MM-dd HH:mm");
                }
            }
            mNewsFragment.refresh(count, content, time);
        }
    }

    public void refreshUnReadTip(ReminderItem item) {
        if (item.unread() > 0) {
            refreshInviteMessage();
            mRedtipLayout.setVisibility(View.VISIBLE);
            mRedTipTv.setText(String.valueOf(item.unread()));
        } else {
            mRedtipLayout.setVisibility(View.GONE);
        }
    }

}
