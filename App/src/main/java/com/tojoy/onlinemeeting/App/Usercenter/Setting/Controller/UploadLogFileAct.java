package com.tojoy.onlinemeeting.App.Usercenter.Setting.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.tojoy.onlinemeeting.App.Usercenter.Setting.View.UploadLogDatePickview;
import com.tojoy.onlinemeeting.R;
import com.tojoy.tjoybaselib.ui.activity.UI;

/**
 * 上传日志页面（只显示近一个月的文件夹，提交过多无用文件）
 * @author qululu
 */
public class UploadLogFileAct extends UI {
    private TextView tvSelectLogDate;

    private UploadLogDatePickview uploadLogDatePickview;

    public static void start(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, UploadLogFileAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_upload_log_file_layout);
        setStatusBar();
        initTitle();
        initUI();
        initData();
    }

    private void initTitle() {
        setUpNavigationBar();
        setTitle("上传日志");
        showBack();
    }

    private void initUI() {
        tvSelectLogDate = (TextView) findViewById(R.id.tv_select_log_date);
        uploadLogDatePickview = new UploadLogDatePickview(this);
    }


    private void initData() {
        tvSelectLogDate.setOnClickListener(v -> {
            uploadLogDatePickview.show();
        });
    }

    private void submitLogFile(String feedback) {

    }


}
