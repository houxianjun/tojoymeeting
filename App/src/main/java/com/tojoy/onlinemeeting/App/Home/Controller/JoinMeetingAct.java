package com.tojoy.onlinemeeting.App.Home.Controller;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.netease.nim.uikit.business.liveroom.JoinLiveRoomHelper;
import com.netease.nim.uikit.business.liveroom.JoinLiveRoomListener;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.sys.KeyboardControlUtil;
import com.tojoy.onlinemeeting.R;
import com.netease.nim.uikit.common.IMLoginManager;

import es.dmoral.toasty.Toasty;

/**
 * 加入会议页面
 */
@Route(path = RouterPathProvider.JOIN_MEETING)
public class JoinMeetingAct extends UI {
    private EditText mEtMeetingID;
    private TextView mTvJoin;
    private String mMeetingID;
    private boolean isKeyboardShowing;

    public static void start(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, JoinMeetingAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_meeting);

        setStatusBar();
        initView();
        initListener();
    }

    private void initView() {
        setUpNavigationBar();
        setTitle("加入会议");
        showBack();
        mEtMeetingID = (EditText) findViewById(R.id.etMeetingID);
        mHandler.postDelayed(() -> {
            mEtMeetingID.requestFocus();
            KeyboardControlUtil.openKeyboard(mEtMeetingID, JoinMeetingAct.this);
        }, 500);
        mTvJoin = (TextView) findViewById(R.id.tvJoin);
    }

    private void initListener() {
        JoinMeetingTextWatcher watcher = new JoinMeetingTextWatcher();
        mEtMeetingID.addTextChangedListener(watcher);

        mTvJoin.setOnClickListener(view -> {
            if (FastClickAvoidUtil.isFastDoubleClick(mTvJoin.getId(), 3000)) {
                return;
            }
            if (!TextUtils.isEmpty(mMeetingID)) {
                KeyboardControlUtil.closeKeyboard(mEtMeetingID, JoinMeetingAct.this);
                join();
            }
        });

        KeyboardControlUtil.observerKeyboardVisibleChange(this, (keyboardHeight, isVisible) -> {
            isKeyboardShowing = isVisible;
        });
    }

    @Override
    protected void onLeftManagerImageClick() {
        super.onLeftManagerImageClick();
        KeyboardControlUtil.closeKeyboard(mEtMeetingID, JoinMeetingAct.this);
        mHandler.postDelayed(this::finish, 200);
    }

    private void join() {
        showProgressHUD(this, "加载中");
        if (AppConfig.isLoginIM) {
            joinMeeting();
        } else {
            IMLoginManager.getInterface().requestLoginIM(this, "0", new IMLoginManager.LoginSuccessCallback() {
                @Override
                public void onLoginSuccess() {
                    joinMeeting();
                    statitiscIMLogin("1");
                }

                @Override
                public void onLoginError(int code) {
                    statitiscIMLogin("0");
                    dismissProgressHUD();
                    Toasty.normal(JoinMeetingAct.this, "网络异常，请稍后再试").show();
                }
            });
        }
    }

    private void statitiscIMLogin(String loginIMState) {
        OMAppApiProvider.getInstance().statitiscIMLogin(loginIMState, "enterLivePage", new rx.Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(OMBaseResponse omBaseResponse) {
            }
        });
    }

    private void joinMeeting() {
        JoinLiveRoomHelper.joinLiveRoomForRoomId(JoinMeetingAct.this, mMeetingID, new JoinLiveRoomListener() {
            @Override
            public void onJoinError(String error, int errorCode) {
                if (error.contains("官方会议管理员")) {
                    Toasty.normal(JoinMeetingAct.this, error).show();
                }
                dismissProgressHUD();
            }

            @Override
            public void onJoinSuccess() {
                dismissProgressHUD();
                finish();
            }
        });
    }

    private class JoinMeetingTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            mMeetingID = mEtMeetingID.getText().toString().trim();

            if (!TextUtils.isEmpty(mMeetingID)) {
                mTvJoin.setBackgroundResource(R.drawable.bg_join_meeting_select);
                mTvJoin.setTextColor(Color.WHITE);
            } else {
                mTvJoin.setBackgroundResource(R.drawable.bg_join_meeting_normal);
                mTvJoin.setTextColor(Color.parseColor("#aeaeae"));
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()) {
            KeyboardControlUtil.closeKeyboard(mEtMeetingID, JoinMeetingAct.this);
        }
    }

    @Override
    public void onBackPressed() {
        if (isKeyboardShowing) {
            KeyboardControlUtil.closeKeyboard(mEtMeetingID, this);
        } else {
            super.onBackPressed();
        }
    }
}
