package com.tojoy.onlinemeeting.App.Base.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by chengyanfang on 2017/9/20.
 */

public class BlankViewHolder extends RecyclerView.ViewHolder  {

    public BlankViewHolder(View arg0, Context aContext) {
        super(arg0);
        ButterKnife.bind(this,itemView);
    }
}