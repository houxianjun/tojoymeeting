package com.tojoy.onlinemeeting.App.Usercenter.Login;

import android.Manifest;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.netease.nim.uikit.api.NimUIKit;
import com.netease.nim.uikit.common.NIMCache;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.auth.AuthService;
import com.netease.nimlib.sdk.auth.ClientType;
import com.tencent.trtc.TRTCCloud;
import com.tojoy.onlinemeeting.Service.Push.PushNotificationManager;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.meetingperson.SingleTonModel.MeetingPersonTon;
import com.tojoy.tjoybaselib.model.user.LoginResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.permission.MPermission;
import com.tojoy.tjoybaselib.services.permission.annotation.OnMPermissionGranted;
import com.tojoy.tjoybaselib.services.update.UpdateManager;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.util.media.FastClickAvoidUtil;
import com.tojoy.tjoybaselib.util.net.NetWorkUtils;
import com.tojoy.tjoybaselib.util.string.RegixUtil;
import com.tojoy.tjoybaselib.util.string.StringUtil;
import com.tojoy.tjoybaselib.util.sys.ActivityStack;
import com.tojoy.tjoybaselib.util.sys.DeviceInfoUtil;
import com.tojoy.onlinemeeting.App.Application.App;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;
import com.tojoy.onlinemeeting.R;
import com.tojoy.tjoybaselib.configs.UrlProvider;
import com.tojoy.common.Services.EventBus.PublicStringEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import rx.Observer;

/**
 * Created by chengyanfang on 2017/8/28.
 *
 * @function:登录/注册界面
 */

@Route(path = RouterPathProvider.Login)
public class LoginActivity extends LoginRegisterBaseAct {

    @BindView(R.id.base_container_layout)
    RelativeLayout mBaseContainerLayout;

    @BindView(R.id.et_username)
    EditText mMobileEt;

    @BindView(R.id.et_password)
    EditText mCheckNum;

    @BindView(R.id.rlv_login_bt)
    RelativeLayout mLoginBt;

    //发送&倒计时
    @BindView(R.id.tv_right_manager)
    TextView mRightManagerTv;

    // 天九云协议
    @BindView(R.id.tv_login_agree)
    TextView mLoginAgree;

    //隐私政策
    @BindView(R.id.tvPrivacy)
    TextView tvPrivacy;

    @BindView(R.id.phone_number_clean)
    RelativeLayout mRlvCleanBut;

    @BindView(R.id.cb_login_agree)
    CheckBox cbLoginAgree;

    private static final String KICK_OUT = "KICK_OUT";
    private TJMakeSureDialog mTjMakeSureDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);
        setStatusBar();
        checkPersimiss(PREMISSIONS_EXTERNAL_STORAGE, PERMISSIONS_EXTERNAL_STORAGE_REQUEST_CODE);
        requestBasicPermission();
        onParseIntent();
        setupLoginPanel();
        setSwipeBackEnable(false);
        doTokenInvalid();

        try {
            TRTCCloud.sharedInstance(this).exitRoom();
        } catch (Exception e) {

        }
//        NotificationUtil notificationUtils = NotificationUtil.getNotificationUtils(this);
////        notificationUtils.clearAllNotification(this);
        PushNotificationManager.getInstance().clearAllNotification(this);
    }

    /**
     * 基本权限管理
     */
    private final String[] BASIC_PERMISSIONS = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE
    };

    private void requestBasicPermission() {
        MPermission.with(LoginActivity.this)
                .setRequestCode(BASIC_PERMISSION_REQUEST_CODE)
                .permissions(BASIC_PERMISSIONS)
                .request();
    }

    /**
     * 解析跳转意图
     * 在收到单点登录被踢回调时会弹出登录确认框
     */
    private void onParseIntent() {
        if (getIntent().getBooleanExtra(KICK_OUT, false)) {
            int type = NIMClient.getService(AuthService.class).getKickedClientType();
            String client;
            switch (type) {
                case ClientType.Web:
                    client = "网页端";
                    break;
                case ClientType.Windows:
                    client = "电脑端";
                    break;
                case ClientType.REST:
                    client = "服务端";
                    break;
                default:
                    client = "移动端";
                    break;
            }
            if (mTjMakeSureDialog == null || !mTjMakeSureDialog.isShowing()) {
                mTjMakeSureDialog = new TJMakeSureDialog(this, v1 -> {
                }).setTitleAndCotent(getString(R.string.kickout_notify), String.format(getString(R.string.kickout_content), client));
                if (!isFinishing()) {
                    mTjMakeSureDialog.show();
                }
            }
            EventBus.getDefault().post(new PublicStringEvent(PublicStringEvent.LOGOUT_SUCCESS));

            getIntent().putExtra(KICK_OUT, false);
        }
    }

    /**
     * 登录面板
     */
    private void setupLoginPanel() {
        mBaseContainerLayout.setOnClickListener(v -> showKeyboard(false));
        mRlvCleanBut.setOnClickListener(view -> {
            mMobileEt.setText("");
        });
        mMobileEt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
        mCheckNum.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        mMobileEt.addTextChangedListener(textWatcher);
        mCheckNum.addTextChangedListener(textWatcher);
        mRightManagerTv.setOnClickListener(v -> requestSendMsg());
        refreshSendStatus();
        mMobileEt.setText(UserInfoManager.getUserPhone(this));
        mMobileEt.setSelection(UserInfoManager.getUserPhone(this).length());
        mLoginBt.setOnClickListener(v -> {
            if (!FastClickAvoidUtil.isFastDoubleClick(mLoginBt.getId())) {
                AppConfig.KICK_OUT = false;
                login();
            }
        });
        mLoginAgree.setOnClickListener(view -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                ARouter.getInstance().build(RouterPathProvider.WEBNew)
                        .withString("url", UrlProvider.USERCENTER_RegistPolicy)
                        .withString("title", "")
                        .navigation();
            }
        });
        tvPrivacy.setOnClickListener(view -> {
            if (!FastClickAvoidUtil.isDoubleClick()) {
                ARouter.getInstance().build(RouterPathProvider.WEBNew)
                        .withString("url", UrlProvider.USERCENTER_Privacy)
                        .withString("title", "")
                        .navigation();
            }
        });
    }

    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            String mobileNum = mMobileEt.getText().toString().trim();
            if (!TextUtils.isEmpty(mobileNum)) {
                mRlvCleanBut.setVisibility(View.VISIBLE);
            } else {
                mRlvCleanBut.setVisibility(View.GONE);
            }
            //自动换行
            if (mobileNum.length() > 10) {
                mMobileEt.setFocusable(false);
                mMobileEt.setFocusableInTouchMode(true);
                mMobileEt.setClickable(false);
            } else {
                mMobileEt.setFocusable(true);
                mMobileEt.setFocusableInTouchMode(true);
                mMobileEt.setClickable(true);
            }
            if (status != 1 && status != 2) {
                if (!TextUtils.isEmpty(mobileNum) && mobileNum.length() == 11) {
                    status = 0;
                } else {
                    status = -1;
                }
                refreshSendStatus();
            }
        }
    };

    /**
     * ***************************************** 验证码 **************************************
     */
    private void requestSendMsg() {
        final String account = mMobileEt.getText().toString().toLowerCase();
        if (StringUtil.isEmpty(account)) {
            Toasty.normal(this, "输入的手机号不能为空").show();
            return;
        }
        if (account.length() < 11) {
            Toasty.normal(this, "输入正确手机号码").show();
            return;
        }

        if (!RegixUtil.checkMobile(account)) {
            Toasty.normal(this, "输入正确手机号码").show();
            return;
        }

        showProgressHUD(this, "发送中");

        UserInfoManager.saveUserPhone(LoginActivity.this, account);

        OMAppApiProvider.getInstance().getCode(account, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
                dismissProgressHUD();
            }

            @Override
            public void onError(Throwable e) {
                dismissProgressHUD();
                Toasty.normal(LoginActivity.this, "网络异常，请稍后重试").show();
            }

            @Override
            public void onNext(OMBaseResponse randomCodeResponse) {
                if (!randomCodeResponse.isSuccess()) {
                    Toasty.normal(LoginActivity.this, randomCodeResponse.msg).show();
                }
            }
        });
        mHandler.postDelayed(() -> {
            status = 2;
            refreshSendStatus();
            startCountDown();
        }, 500);
    }

    /**
     * 倒计时
     */
    //倒计时
    Timer mTimer;
    Handler mTimerHandler;
    int mSecond = 60;

    @SuppressLint("HandlerLeak")
    private void startCountDown() {
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Message message = new Message();
                message.what = 1;
                mTimerHandler.sendMessage(message);
            }
        }, 1000, 1000);
        mTimerHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        if (mSecond <= 0) {
                            mTimer.cancel();
                            status = 3;
                            refreshSendStatus();
                            return;
                        }
                        updateTimeCount();
                        break;
                }
                super.handleMessage(msg);
            }
        };
    }

    /**
     * 更改时间
     */
    private void updateTimeCount() {
        mSecond--;
        if (mSecond <= 0) {
            status = 3;
            refreshSendStatus();
            return;
        }
        status = 2;
        refreshSendStatus();
    }

    /**
     * 刷新发送按钮状态
     */
    int status = -1;

    private void refreshSendStatus() {
        switch (status) {
            case -1://不可发送
                mRightManagerTv.setClickable(false);
                mRightManagerTv.setText(getString(R.string.get_check_code));
                mRightManagerTv.setTextColor(Color.parseColor("#7b808f"));
                break;
            case 0://可发送
                mRightManagerTv.setClickable(true);
                mRightManagerTv.setText(getString(R.string.get_check_code));
                mRightManagerTv.setTextColor(Color.parseColor("#7b808f"));
                break;
            case 1://发送中
                mRightManagerTv.setClickable(false);
                mRightManagerTv.setText(getString(R.string.sending_check_code));
                mRightManagerTv.setTextColor(Color.parseColor("#7b808f"));
                break;
            case 2://倒计时ing
                mRightManagerTv.setClickable(false);
                mRightManagerTv.setText("重新获取 (" + mSecond + "s)");
                mRightManagerTv.setTextColor(Color.parseColor("#2671E9"));
                break;
            case 3://重新发送
                mSecond = 60;
                if (mTimer != null) {
                    mTimer.cancel();
                }
                mRightManagerTv.setClickable(true);
                mRightManagerTv.setText("重新获取");
                mRightManagerTv.setTextColor(Color.parseColor("#7b808f"));
                break;
             default:
                 break;
        }
    }

    /**
     * ***************************************** 登录 **************************************
     */
    public void login() {
        NetWorkUtils.checkNet(this);
        final String account = mMobileEt.getText().toString().toLowerCase();
        final String randomcode = mCheckNum.getText().toString().toLowerCase();
        if (StringUtil.isEmpty(account)) {
            Toasty.normal(this, "输入的手机号不能为空").show();
            return;
        }
        if (account.length() < 11) {
            Toasty.normal(this, "输入真确手机号码").show();
            return;
        }
        if (StringUtil.isEmpty(randomcode)) {
            Toasty.normal(this, "输入的验证码不能为空").show();
            return;
        }
        if (!cbLoginAgree.isChecked()) {
            Toasty.normal(this, "需同意直播招商云用户服务协议和隐私政策").show();
            return;
        }
        showProgressHUD(this, getString(R.string.loading));
        mProgressHUD.setCancelable(false);
        mLoginBt.setClickable(false);

        //清理上次的组织架构缓存数据
        MeetingPersonTon.getInstance().clear();
        OMAppApiProvider.getInstance().newLogin(account, randomcode, new Observer<LoginResponse>() {
            @Override
            public void onCompleted() {
                mLoginBt.setClickable(true);
                if (App.getInstance() != null) {
                    App.getInstance().canShowLoginNotice = true;
                }
            }

            @Override
            public void onError(Throwable e) {
                dismissProgressHUD();
                mLoginBt.setClickable(true);
                Toasty.normal(LoginActivity.this, "网络异常，请稍后尝试登录").show();
            }

            @Override
            public void onNext(LoginResponse loginResponse) {
                if (loginResponse.isSuccess()) {
                    //todo 登录新接口，成功后数据存储进处理了userid和access——token，后续跟进
                    UserInfoManager.saveUserMobile(LoginActivity.this, account);
                    UserInfoManager.setLoginUserInfo(LoginActivity.this, loginResponse.data);
                    loginSuccess();
                    EventBus.getDefault().post(new PublicStringEvent(PublicStringEvent.LOGIN_SUCCESS));
                } else {
                    dismissProgressHUD();
                    Toasty.normal(LoginActivity.this, loginResponse.msg).show();
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    @OnMPermissionGranted(PERMISSIONS_EXTERNAL_STORAGE_REQUEST_CODE)
    public void onBasicPermissionSuccess() {
        DeviceInfoUtil.getSerialNum(App.getInstance());
    }

    @Override
    protected void onResume() {
        super.onResume();
        onParseIntent();
        UpdateManager.getInstance().checkUpdate(this, false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mTjMakeSureDialog != null) {
            mTjMakeSureDialog.dismiss();
        }
    }

    private void doTokenInvalid() {
        if (getIntent().getBooleanExtra("tokenInvalid", false)) {
            LogoutHelper.logout();
           // LogUtil.v("LogoutHelper","doTokenInvalid 接口调试401");
            Toasty.normal(this, "登录过期，请重新登录").show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        /**
         * 关闭所有页面，清理栈. 不关闭页面，在收到被提的消息时，
         * 在登录页面点返回按键会返回到首页或之前停留的页面
         */
        ActivityStack.getActivityStack().popAllActivity(null);

    }
}
