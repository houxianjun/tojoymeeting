package com.tojoy.onlinemeeting.App.Usercenter.Setting.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

import com.tojoy.cloud.module_shop.Api.ShopModuleApiProvider;
import com.tojoy.common.Services.EventBus.PublicStringEvent;
import com.tojoy.onlinemeeting.App.Base.adapter.BaseSubmitModel;
import com.tojoy.onlinemeeting.App.Usercenter.Login.LogoutHelper;
import com.tojoy.onlinemeeting.BuildConfig;
import com.tojoy.onlinemeeting.R;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.EnvironmentInstance;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.RetrofitSingleton;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.string.StringUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;

public class EnvironmentSwitchAct extends UI {
    CheckBox cb_dev;
    CheckBox cb_test;
    RelativeLayout rlv_logout_bt;
    RelativeLayout rl_dev_main;
    RelativeLayout rl_test_main;

    public static void start(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, EnvironmentSwitchAct.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_environment_switch);
        setStatusBar();
        ButterKnife.bind(this);
        initTitle();
        initUI();
        initData();
    }


    private void initTitle() {
        setUpNavigationBar();
        showBack();
        setTitle("环境切换");
    }

    private void initUI() {
        cb_dev = (CheckBox) findViewById(R.id.cb_dev);
        cb_test = (CheckBox) findViewById(R.id.cb_test);
        rlv_logout_bt = (RelativeLayout) findViewById(R.id.rlv_logout_bt);
        rl_dev_main = (RelativeLayout) findViewById(R.id.rl_dev_main);
        rl_test_main = (RelativeLayout) findViewById(R.id.rl_test_main);
    }

    private void initData() {
        //开发环境
        if (AppConfig.getAppNowEnvriment() == EnvironmentInstance.DEV.getValue()) {
            cb_dev.setChecked(true);
            cb_test.setChecked(false);
        } else if (AppConfig.getAppNowEnvriment() == EnvironmentInstance.TEST.getValue()) {
            cb_dev.setChecked(false);
            cb_test.setChecked(true);
        }
        rlv_logout_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TJMakeSureDialog(EnvironmentSwitchAct.this, view -> {

                    LogoutHelper.loginOutClearCache(EnvironmentSwitchAct.class,new LogoutHelper.LoginOutListener() {
                        @Override
                        public void loginOutSuccess() {
                            if (cb_dev.isChecked()) {
                                AppConfig.environmentInstance = EnvironmentInstance.DEV;
                            } else if (cb_test.isChecked()) {
                                AppConfig.environmentInstance = EnvironmentInstance.TEST;
                            }
                            BaseUserInfoCache.setNowEnvironment(EnvironmentSwitchAct.this, AppConfig.environmentInstance.getValue());
                            RetrofitSingleton.initRetrofit();
                            OMAppApiProvider.initialize(EnvironmentSwitchAct.this).init();
                            ShopModuleApiProvider.initialize(EnvironmentSwitchAct.this).init();
                            RouterJumpUtil.startLoginActivity();
                            EnvironmentSwitchAct.this.finish();
                        }
                    });

                }).setTitleAndCotent("提示", "切换环境需重新登录！").show();

            }
        });
        cb_dev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cb_test.isChecked()) {
                    cb_test.setChecked(false);
                }

                cb_dev.setChecked(true);


            }
        });
        cb_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cb_dev.isChecked()) {
                    cb_dev.setChecked(false);
                }
                cb_test.setChecked(true);

            }
        });
        rl_dev_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cb_test.isChecked()) {
                    cb_test.setChecked(false);
                }

                cb_dev.setChecked(true);


            }
        });
        rl_test_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cb_dev.isChecked()) {
                    cb_dev.setChecked(false);
                }
                cb_test.setChecked(true);

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
