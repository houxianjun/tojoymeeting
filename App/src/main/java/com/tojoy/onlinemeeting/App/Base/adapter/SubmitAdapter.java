package com.tojoy.onlinemeeting.App.Base.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tojoy.tjoybaselib.ui.adapter.BaseAdapter;
import com.tojoy.tjoybaselib.util.sys.AppUtils;
import com.tojoy.onlinemeeting.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by chengyanfang on 2017/9/20.
 */

public class SubmitAdapter extends BaseAdapter<BaseSubmitModel> {

    LinearLayout.LayoutParams normalSegmentParams, selectedSegmentParams;

    RelativeLayout.LayoutParams titleLayoutParams;

    public SubmitAdapter(@NonNull Context context, @NonNull ArrayList<BaseSubmitModel> datas) {
        super(context, datas);
        normalSegmentParams = new LinearLayout.LayoutParams(AppUtils.dip2px(mContext, 138), AppUtils.dip2px(mContext, 48));
        selectedSegmentParams = new LinearLayout.LayoutParams(AppUtils.dip2px(mContext, 138), AppUtils.dip2px(mContext, 55));
        normalSegmentParams.setMargins(AppUtils.dip2px(context, 8), 0, AppUtils.dip2px(context, 8), 0);
        selectedSegmentParams.setMargins(AppUtils.dip2px(context, 8), 0, AppUtils.dip2px(context, 8), 0);
        titleLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        int titleWidth = (int) (6 * AppUtils.sp2px(context, 14));
        int leftMargin = AppUtils.getWidth(context) / 2 + AppUtils.dip2px(context, 8) + (AppUtils.dip2px(mContext, 138) - titleWidth) / 2;
        titleLayoutParams.setMargins(leftMargin, AppUtils.dip2px(context, 92), 0, 0);
    }

    @Override
    protected int getContentViewType(int position) {
        return mDatas.get(position).type.ordinal();
    }


    @Override
    protected int getResourceId(int viewType) {
        switch (viewType) {
            //空行
            case 0:
                return R.layout.item_base_blank_view;
            case 1:
                return R.layout.item_select_type_cell;
            case 2:
                return R.layout.item_select_type_cell_three;
            default:
                return R.layout.item_base_blank_view;
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateItemHolderViewHolder(View arg0, int viewType) {
        if (viewType == 0) {
            return new BlankViewHolder(arg0, mContext);
        } else if (viewType == 1) {
            return new SelectTypeViewHolder(arg0, mContext);
        } else if (viewType == 2) {
            //右侧箭头和文本
            return new SelectTypeViewHolder(arg0, mContext);
        }
        return new BlankViewHolder(arg0, mContext);
    }

    @Override
    protected void onBindContentViewHolder(RecyclerView.ViewHolder viewHolder, int position, int viewType) {
        BaseSubmitModel model = mDatas.get(position);

        viewHolder.itemView.setOnClickListener(view -> {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(position);
            }
        });

        if (viewType == 1 || viewType == 2) {
            final SelectTypeViewHolder selectTypeViewHolder = (SelectTypeViewHolder) viewHolder;
            selectTypeViewHolder.title.setText(model.title);
            selectTypeViewHolder.itemView.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            });

            /**
             * 分割线
             */
            if (model.showDivider) {
                selectTypeViewHolder.divider.setVisibility(View.VISIBLE);
            } else {
                selectTypeViewHolder.divider.setVisibility(View.GONE);
            }


            /**
             * 小红点
             */
            if (model.showRedTip) {
                selectTypeViewHolder.mRedTip.setVisibility(View.VISIBLE);
            } else {
                selectTypeViewHolder.mRedTip.setVisibility(View.GONE);
            }

            /**
             * 显得必填
             */
            if (model.isShowMustInput) {
                selectTypeViewHolder.mustInput.setVisibility(View.VISIBLE);
            } else {
                selectTypeViewHolder.mustInput.setVisibility(View.GONE);
            }

            /**
             * 右侧内容及箭头
             */
            if (model.hideRightArrow) {
                selectTypeViewHolder.mRightArrow.setVisibility(View.GONE);

                if (TextUtils.isEmpty(model.typeContent) && !"群标签".equals(model.title)) {
                    selectTypeViewHolder.mRightContent.setText("");
                } else {
                    selectTypeViewHolder.mRightContent.setText(model.typeContent);
                }

                selectTypeViewHolder.mRightContent.setTextColor(mContext.getResources().getColor(R.color.color_acacac));
            } else {
                if (TextUtils.isEmpty(model.typeContent)) {
                    selectTypeViewHolder.mRightContent.setText(mContext.getString(R.string.please_select));
                    selectTypeViewHolder.mRightContent.setTextColor(mContext.getResources().getColor(R.color.color_acacac));
                    if ("去编辑".equals(model.selectHint)) {
                        selectTypeViewHolder.mRightContent.setText("去编辑");
                    }
                } else {
                    selectTypeViewHolder.mRightContent.setText(model.typeContent);
                    selectTypeViewHolder.mRightContent.setTextColor(mContext.getResources().getColor(R.color.color_808080));
                }

                selectTypeViewHolder.mRightArrow.setVisibility(View.VISIBLE);
            }


            if (model.hideTypeContent) {
                selectTypeViewHolder.mRightContent.setVisibility(View.GONE);
            } else {
                selectTypeViewHolder.mRightContent.setVisibility(View.VISIBLE);
            }
        }
    }


    /**
     * ****************************   接口回调  ***********************************
     */

    /**
     * ItemClick
     */
    public interface OnItemClickListener {
        void onItemClick(int index);
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    /**
     * ****************************   ViewHolder  ***********************************
     */

    /**
     * SelectType Cell
     */
    public class SelectTypeViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextView title;

        @BindView(R.id.divider)
        View divider;

        @BindView(R.id.tv_right_content)
        TextView mRightContent;

        @BindView(R.id.redTip)
        TextView mRedTip;

        @BindView(R.id.arrow)
        ImageView mRightArrow;

        @BindView(R.id.tv_must_input)
        TextView mustInput;

        public SelectTypeViewHolder(View arg0, Context aContext) {
            super(arg0);
            ButterKnife.bind(this, itemView);
        }
    }

}
