package com.tojoy.onlinemeeting.App.Usercenter.Login;

import com.netease.nim.uikit.api.NimUIKit;
import com.tojoy.common.Services.EventBus.PublicStringEvent;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.model.meetingperson.SingleTonModel.MeetingPersonTon;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.ui.drop.DropManager;
import com.netease.nim.uikit.common.NIMCache;
import com.tojoy.tjoybaselib.util.sys.ActivityStack;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;

import org.greenrobot.eventbus.EventBus;

import rx.Observer;


/**
 * Created by chengyanfang on 2017/8/28.
 * <p>
 * 注销帮助类
 */

public class LogoutHelper {

    public static void logout() {
        ActivityStack.getActivityStack().popAllActivityExceptOne(LoginActivity.class);
        NimUIKit.logout();
        NIMCache.clear();
        DropManager.getInstance().destroy();
        UserInfoManager.logout();
    }

    /**
     *
     */
    public static void loginOutClearCache(Class cls,LoginOutListener loginOutListener){
        //个推解绑
        OMAppApiProvider.getInstance().pushUnBind(UserInfoManager.getClientId(NIMCache.getContext()), new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
                doLogout(cls,loginOutListener);
            }

            @Override
            public void onError(Throwable e) {


                doLogout(cls,loginOutListener);
            }

            @Override
            public void onNext(OMBaseResponse baseResponse) {
            }
        });


    }

    private static void doLogout(Class cls,LoginOutListener loginOutListener) {

        OMAppApiProvider.getInstance().logout(new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(OMBaseResponse omBaseResponse) {


            }
        });
        AppConfig.KICK_OUT=false;
        AppConfig.isShowing = false;
        AppConfig.isLoginIM = false;
        EventBus.getDefault().post(new PublicStringEvent(PublicStringEvent.LOGOUT_SUCCESS));
        UserInfoManager.logout();
        MeetingPersonTon.getInstance().clear();
        ActivityStack.getActivityStack().popAllActivity(cls);

        loginOutListener.loginOutSuccess();
    }

    /**
     * 退出登录监听
     */
    public interface LoginOutListener{
        public void loginOutSuccess();
    }

}
