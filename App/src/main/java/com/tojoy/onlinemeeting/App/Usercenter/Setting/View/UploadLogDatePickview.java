package com.tojoy.onlinemeeting.App.Usercenter.Setting.View;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.tojoy.onlinemeeting.R;
import com.tojoy.tjoybaselib.ui.dialog.PickerViewAnimateUtil;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * 上传日志选择日期
 * @author qululu
 */
public class UploadLogDatePickview {

    private Context context;
    protected ViewGroup contentContainer;
    private Animation outAnim;
    private Animation inAnim;
    private boolean isAnim = true;
    private boolean isShowing;
    private int gravity = Gravity.BOTTOM;
    public ViewGroup decorView;
    private ViewGroup rootView;
    protected View clickView;
    private boolean dismissing;


    private RecyclerView mRecyclerView;
    private LogDateListAdapter mAdapter;
    private List<String> mDateList = new ArrayList<>();


    public UploadLogDatePickview(Context context) {
        this.context = context;
        init();
        initViews();
    }


    private void initViews() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        if (decorView == null) {
            decorView =((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        }
        //将控件添加到decorView中
        rootView = (ViewGroup) layoutInflater.inflate(R.layout.layout_log_date_list, decorView, false);
        rootView.setLayoutParams(new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        ));
        rootView.setBackgroundColor(context.getResources().getColor(R.color.c66000000));
        contentContainer =  rootView.findViewById(R.id.content_container);
        setKeyBackCancelable(true);
        rootView.findViewById(R.id.iv_close).setOnClickListener(v -> dismiss());

        mRecyclerView = rootView.findViewById(R.id.recycler_date);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mDateList.add("2020-08-10");
        mDateList.add("2020-08-09");
        mDateList.add("2020-08-08");
        mDateList.add("2020-08-07");
        mDateList.add("2020-08-06");
        mDateList.add("2020-08-05");

        mAdapter = new LogDateListAdapter(context, mDateList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        mAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
//            for (ProvinceModel provinceModel : mOrderList) {
//                provinceModel.isChecked = false;
//            }
//            mOnSelectAreaCallback.onSelectAt(addressResponse.data.get(position).disName, position);
//            mOrderList.get(position).isChecked = true;
//            areaListAdapter.notifyDataSetChanged();
        });
    }

    public void setTitle(String titleStr) {
        setOutSideCancelable(true);
        rootView.findViewById(R.id.iv_close).setVisibility(View.GONE);
        ((TextView) rootView.findViewById(R.id.title)).setText(titleStr);
    }

    protected void init() {
        inAnim = getInAnimation();
        outAnim = getOutAnimation();
    }

    /**
     * @param v      (是通过哪个View弹出的)
     * @param isAnim 是否显示动画效果
     */
    public void show(View v, boolean isAnim) {
        this.clickView = v;
        this.isAnim = isAnim;
        show();
    }

    public void show(boolean isAnim) {
        this.isAnim = isAnim;
        show();
    }

    public void show(View v) {
        this.clickView = v;
        show();
    }

    /**
     * 添加View到根视图
     */
    public void show() {
        if (isShowing()) {
            return;
        }
        isShowing = true;
        onAttached(rootView);
        rootView.requestFocus();
    }

    /**
     * show的时候调用
     *
     * @param view 这个View
     */
    private void onAttached(View view) {
        decorView.addView(view);
        if (isAnim) {
            contentContainer.startAnimation(inAnim);
        }
    }


    /**
     * 检测该View是不是已经添加到根视图
     *
     * @return 如果视图已经存在该View返回true
     */
    public boolean isShowing() {
        return rootView.getParent() != null || isShowing;
    }


    public void dismiss() {
        if (dismissing) {
            return;
        }

        if (isAnim) {
            //消失动画
            outAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    dismissImmediately();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            contentContainer.startAnimation(outAnim);
        } else {
            dismissImmediately();
        }
        dismissing = true;
    }

    public void dismissImmediately() {
        decorView.post(() -> {
            //从根视图移除
            decorView.removeView(rootView);
            isShowing = false;
            dismissing = false;
        });
    }

    public Animation getInAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, true);
        return AnimationUtils.loadAnimation(context, res);
    }

    public Animation getOutAnimation() {
        int res = PickerViewAnimateUtil.getAnimationResource(this.gravity, false);
        return AnimationUtils.loadAnimation(context, res);
    }

    public void setKeyBackCancelable(boolean isCancelable) {
        ViewGroup View;
        View = rootView;
        View.setFocusable(isCancelable);
        View.setFocusableInTouchMode(isCancelable);
        if (isCancelable) {
            View.setOnKeyListener(onKeyBackListener);
        } else {
            View.setOnKeyListener(null);
        }
    }

    private View.OnKeyListener onKeyBackListener = (v, keyCode, event) -> {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == MotionEvent.ACTION_DOWN
                && isShowing()) {
            dismiss();
            return true;
        }
        return false;
    };

    public UploadLogDatePickview setOutSideCancelable(boolean isCancelable) {
        if (rootView != null) {
            View view = rootView.findViewById(R.id.outmost_container);

            if (isCancelable) {
                view.setOnTouchListener(onCancelableTouchListener);
            } else {
                view.setOnTouchListener(null);
            }
        }
        return this;
    }

    /**
     * Called when the user touch on black overlay in order to dismiss the dialog
     */
    @SuppressLint("ClickableViewAccessibility")
    private final View.OnTouchListener onCancelableTouchListener = (v, event) -> {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            dismiss();
        }
        return false;
    };


    public class LogDateListAdapter extends BaseRecyclerViewAdapter<String> {
        private Context mContext;

        public LogDateListAdapter(@NonNull Context context, @NonNull List<String> datas) {
            super(context, datas);
            this.mContext = context;
        }

        @Override
        protected int getViewType(int position, @NonNull String data) {
            return position;
        }

        @Override
        protected int getLayoutResId(int viewType) {
            return R.layout.item_upload_log_date;
        }

        @Override
        protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
            return new LogDateViewHoder(context, getLayoutResId(viewType));
        }

        public class LogDateViewHoder extends BaseRecyclerViewHolder<String> {
            TextView tvName;

            @Override
            public void onBindData(String data, int position) {
//                if (data.isChecked) {
//                    ivChecked.setVisibility(View.VISIBLE);
//                    tvAreaName.setTextColor(mContext.getResources().getColor(R.color.color_3A83F7));
//                } else {
//                    ivChecked.setVisibility(View.GONE);
//                    tvAreaName.setTextColor(mContext.getResources().getColor(R.color.color_222222));
//                }
                tvName.setText(data);
            }

            public LogDateViewHoder(Context context, int layoutResId) {
                super(context, layoutResId);
                tvName = (TextView) findViewById(R.id.tv_area_name);
            }
        }
    }
}
