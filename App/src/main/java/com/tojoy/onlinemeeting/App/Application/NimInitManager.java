package com.tojoy.onlinemeeting.App.Application;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.widget.Toast;

import com.netease.nim.uikit.api.model.user.PushMsgParser;
import com.netease.nim.uikit.api.model.user.SystemNotice;

import com.netease.nim.uikit.common.NIMCache;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.NimStrings;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.model.BroadcastMessage;
import com.netease.nimlib.sdk.msg.model.CustomNotification;
import com.netease.nimlib.sdk.team.constant.TeamFieldEnum;
import com.netease.nimlib.sdk.team.model.UpdateTeamAttachment;
import com.tojoy.onlinemeeting.Service.Push.PushNotificationManager;
import com.tojoy.tjoybaselib.ui.dialog.TJMakeSureDialog;
import com.tojoy.tjoybaselib.util.router.RouterJumpUtil;
import com.tojoy.tjoybaselib.util.string.StringUtil;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;
import com.tojoy.onlinemeeting.Service.SystemNotice.SystemMsgDBManager;
import com.netease.nim.uikit.common.UserPreferences;
import com.tojoy.onlinemeeting.NIM.reminder.ReminderManager;
import com.tojoy.onlinemeeting.R;
import com.tojoy.onlinemeeting.Service.BadgerManager.BadgerManager;
import com.tojoy.common.Services.EventBus.OrderPayResultEvent;
import com.tojoy.common.Services.EventBus.PublicStringEvent;
import com.tojoy.common.Services.Push.SystemNoticeType;
import com.tojoy.tjoybaselib.util.sys.ActivityStack;
import com.tojoy.tjoybaselib.util.time.TimeUtil;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * Created by chengyanfang on 2017/10/18.
 */

public class NimInitManager {
    private static final String TAG = "NIMInitManager";

    private NimInitManager() {
    }

    private static class InstanceHolder {
        static NimInitManager receivers = new NimInitManager();
    }

    public static NimInitManager getInstance() {
        return InstanceHolder.receivers;
    }

    public void init(boolean register) {
        // 注册通知消息过滤器
        registerIMMessageFilter();

        // 注册语言变化监听广播
        registerLocaleReceiver(register);

        // 注册全局云信sdk 观察者
        registerGlobalObservers(register);
    }

    private void registerGlobalObservers(boolean register) {
        // 注册云信全员广播
        registerBroadcastMessages(register);

        // 注册自定义消息通知
        registCustomNotification(register);
    }

    private void registerLocaleReceiver(boolean register) {
        if (register) {
            updateLocale();
            IntentFilter filter = new IntentFilter(Intent.ACTION_LOCALE_CHANGED);
            NIMCache.getContext().registerReceiver(localeReceiver, filter);
        } else {
            NIMCache.getContext().unregisterReceiver(localeReceiver);
        }
    }

    private BroadcastReceiver localeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_LOCALE_CHANGED)) {
                updateLocale();
            }
        }
    };

    private void updateLocale() {
        Context context = NIMCache.getContext();
        NimStrings strings = new NimStrings();
        strings.status_bar_multi_messages_incoming = context.getString(R.string.nim_status_bar_multi_messages_incoming);
        strings.status_bar_image_message = context.getString(R.string.nim_status_bar_image_message);
        strings.status_bar_audio_message = context.getString(R.string.nim_status_bar_audio_message);
        strings.status_bar_custom_message = context.getString(R.string.nim_status_bar_custom_message);
        strings.status_bar_file_message = context.getString(R.string.nim_status_bar_file_message);
        strings.status_bar_location_message = context.getString(R.string.nim_status_bar_location_message);
        strings.status_bar_notification_message = context.getString(R.string.nim_status_bar_notification_message);
        strings.status_bar_ticker_text = context.getString(R.string.nim_status_bar_ticker_text);
        strings.status_bar_unsupported_message = context.getString(R.string.nim_status_bar_unsupported_message);
        strings.status_bar_video_message = context.getString(R.string.nim_status_bar_video_message);
        strings.status_bar_hidden_message_content = context.getString(R.string.nim_status_bar_hidden_msg_content);
        NIMClient.updateStrings(strings);
    }

    /**
     * 通知消息过滤器（如果过滤则该消息不存储不上报）
     */
    private void registerIMMessageFilter() {
        NIMClient.getService(MsgService.class).registerIMMessageFilter(message -> {
            if (UserPreferences.getMsgIgnore() && message.getAttachment() != null) {
                if (message.getAttachment() instanceof UpdateTeamAttachment) {
                    UpdateTeamAttachment attachment = (UpdateTeamAttachment) message.getAttachment();
                    for (Map.Entry<TeamFieldEnum, Object> field : attachment.getUpdatedFields().entrySet()) {
                        if (field.getKey() == TeamFieldEnum.ICON) {
                            return true;
                        }
                    }
                }
            }
            return false;
        });
    }

    /**
     * 注册云信全服广播接收器
     *
     * @param register
     */
    private void registerBroadcastMessages(boolean register) {
        NIMClient.getService(MsgServiceObserve.class).observeBroadcastMessage((Observer<BroadcastMessage>) broadcastMessage -> Toast.makeText(NIMCache.getContext(), "收到全员广播 ：" + broadcastMessage.getContent(), Toast.LENGTH_SHORT).show(), register);
    }

    private void registCustomNotification(boolean register) {
        NIMClient.getService(MsgServiceObserve.class).observeCustomNotification((Observer<CustomNotification>) message -> {
            // 1.解析消息 及 数据的封装转化
            SystemNotice systemNotice = new PushMsgParser(message.getContent().getBytes()).doParser();
            if (systemNotice == null) {
                return;
            }
            //聊天室敏感词的过滤，这里不处理
            if (!TextUtils.isEmpty(systemNotice.getType()) && systemNotice.getType().equals(SystemNoticeType.SENSTIVE)) {
                return;
            }

            //收到会议邀请
            if (SystemNoticeType.ACTIVIT_INVITE.equals(systemNotice.getType())) {
                EventBus.getDefault().post(new PublicStringEvent(PublicStringEvent.INVITE_JOIN_MEETING));
                return;
            }

            if (SystemNoticeType.CHANGE_ROLE.equals(systemNotice.getType())) {
                //LogUtil.v("message-change","SystemNoticeType.CHANGE_ROLE"+ TimeUtil.getLoneTimeStr(System.currentTimeMillis()));
                EventBus.getDefault().postSticky(new PublicStringEvent(PublicStringEvent.ROLE_CHANGE));
                return;
            }


            if (SystemNoticeType.CR_MEMBER_TYPE_CHANGED.equals(systemNotice.getType())) {
                return;
            }

            if (SystemNoticeType.ORDER_PAY_SUC_RESULT.equals(systemNotice.getType())) {//订单支付结果变化
                boolean isSuc = systemNotice.content.equals("1") ? true : false;
                EventBus.getDefault().post(new OrderPayResultEvent(isSuc, systemNotice.title));
                return;
            }

            if (SystemNoticeType.OUT_LIVE_ROOM_LIVE_ADD.equals(systemNotice.getType()) || SystemNoticeType.OUT_LIVE_ROOM_LIVE_DECREASE.equals(systemNotice.getType())) {
                return;
            }


            String type = systemNotice.getType();
            if (SystemNoticeType.CLOSE_LIVE_STREAM.equals(type)) {
                return;
            }

            if (TextUtils.isEmpty(systemNotice.type)) {
                return;
            }

            BadgerManager.getInstance().updateBadger();


            //踢出提示
            if (!SystemNoticeType.SYSTEM_KICK_OUT.equals(systemNotice.type)) {
                SystemMsgDBManager.getInterface().insertNewNotice(systemNotice);
                int unreadSystemMsgNum = SystemMsgDBManager.getInterface().getUnReadNoticeCntByType(App.getInstance(),SystemNoticeType.SYSTEM_NOTICE);
                ReminderManager.getInstance().updateSessionUnreadNum(unreadSystemMsgNum);
            }

            //被系统强制踢下线
            if (systemNotice.getType().equals(SystemNoticeType.SYSTEM_KICK_OUT)) {
                //踢人下线
                Activity currentActivity = ActivityStack.getActivityStack().currentActivity();
//                Activity currentActivity = App.getInstance().getCurrentActivity();
                if (currentActivity != null && !currentActivity.isFinishing()) {
                    //从新或者下次登录时都清空当前app类型的通知栏消息
                    PushNotificationManager.getInstance().clearAllNotification(currentActivity);
                    new TJMakeSureDialog(currentActivity, v -> {
                        UserInfoManager.logout();
                        RouterJumpUtil.startLoginActivity();
                        EventBus.getDefault().post(new PublicStringEvent(PublicStringEvent.LOGOUT_SUCCESS));
                    }).setTitleAndCotent("系统提示", StringUtil.isEmpty(systemNotice.content) ? "您已被踢出账号" : systemNotice.content).showAlert();
                }
            }
        }, register);
    }

}
