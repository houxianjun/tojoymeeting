package com.tojoy.onlinemeeting.App.Application;

import android.text.TextUtils;
import android.util.Log;

import com.alibaba.android.arouter.launcher.ARouter;
import com.bumptech.glide.request.target.ViewTarget;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.meituan.android.walle.WalleChannelReader;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.smtt.export.external.TbsCoreSettings;
import com.tencent.smtt.sdk.QbSdk;
import com.tencent.tic.core.TICManager;
import com.tojoy.tjoybaselib.BaseLibKit;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.EnvironmentInstance;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.util.storage.AppFileHelper;
import com.tojoy.onlinemeeting.R;
import com.tojoy.cloud.online_meeting.App.Config.LiveRoomConfig;
import com.tojoy.cloud.online_meeting.BuildConfig;
import com.tojoy.tjoybaselib.util.sys.AppUtils;

import java.util.HashMap;

import es.dmoral.toasty.Toasty;

/**
 * Created by chengyanfang on 2017/8/21.
 */

public class App extends NIMApp {

    public static boolean firstEnter = true;


    public boolean canShowLoginNotice = false;

    private TICManager mTIC;

    public TICManager getTICManager() {
        return mTIC;
    }

    @Override
    public void onCreate() {
        mApp = this;
        BaseLibKit.init(this, BuildConfig.VERSION_NAME);
        //系统版本9时禁止使用反射，注解hide类时弹框提示
        AppUtils.disableAPIDialog();
        if (inMainProcess()) {
            // 只有切换环境才有此值
            int nowEnvironment = BaseUserInfoCache.getNowEnvironment(this);
            // 如果代码中配置为正式环境则不做切换环境的判断
            if (AppConfig.environmentInstance != EnvironmentInstance.FORMAL
                    && nowEnvironment > 0 && nowEnvironment <= EnvironmentInstance.FORMAL.getValue()){
                // 避免切换环境后，再次启动不生效，动态切换为上次设置的环境
                AppConfig.environmentInstance = EnvironmentInstance.typeOfValue(nowEnvironment);
            }
        }
        super.onCreate();


        /**
         * 配置存储目录
         */
        AppFileHelper.initStoryPath(this);


        /**
         * 配置 X5
         */
        initX5WebVIew();

        /**
         * 配置提示
         */
        Toasty.Config.getInstance().setErrorColor(getResources().getColor(R.color.main_red)).setSuccessColor(getResources().getColor(R.color.color_2a62dd)).setTextSize(14).apply();

        /**
         * 配置GREEN DAO
         */
        setupDatabase();

        // 渠道名称
//        String appKey = ChannelProvider.getCobubAppKeyByChannel(WalleChannelReader.getChannel(this));

        /**
         * 配置Router
         */
        ARouter.init(this);

        /**
         * 初始化Fresco
         */
        Fresco.initialize(this);

        /**
         * 防止glide加载的图片，刷新recycleview出现闪退现象
         */
        ViewTarget.setTagId(R.id.glide_tag);

        /**
         * 初始化腾讯云SDKs
         */
        initTecentSDK();

        /**
         * 初始化buglySDK
         */
        initBugly();

    }

    private void initBugly() {
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(getApplicationContext());
        strategy.setUploadProcess(inMainProcess());
        strategy.setAppChannel(WalleChannelReader.getChannel(this));
        strategy.setAppVersion(BuildConfig.VERSION_NAME);
        CrashReport.initCrashReport(getApplicationContext()
                , AppConfig.BUGLY_APP_ID
                , AppConfig.environmentInstance == EnvironmentInstance.FORMAL ? false : true
                , strategy);
    }

    public static App getInstance() {
        return (App) mApp;
    }

    /**
     * 初始化腾讯云SDK
     * <p>
     * 日志内容：
     * 1.ILVB
     * 2.SSOSDKReport
     */
    private void initTecentSDK() {

        if (inMainProcess()) {
            // 仅在主线程初始化
            // 初始化TIC
            mTIC = TICManager.getInstance();
            mTIC.init(this, LiveRoomConfig.getTXSDKAppID(), TICManager.TICDisableModule.TIC_DISABLE_MODULE_TRTC);

        }
    }
//
//    @Override
//    public void onTerminate() {
//        if (mTIC != null) {
//            mTIC.unInit();
//        }
//
//        super.onTerminate();
//    }

    private void initX5WebVIew() {
        // 在调用TBS初始化、创建WebView之前进行如下配置，以开启优化方案(仅Android 5.1+生效)
        HashMap<String, Object> map = new HashMap<>();
        map.put(TbsCoreSettings.TBS_SETTINGS_USE_PRIVATE_CLASSLOADER, true);
        QbSdk.initTbsSettings(map);
        QbSdk.PreInitCallback cb = new QbSdk.PreInitCallback() {
            @Override
            public void onViewInitFinished(boolean arg0) {                //x5內核初始化完成的回调，为true表示x5内核加载成功，否则表示x5内核加载失败，会自动切换到系统内核。
                Log.d("app", " onViewInitFinished is " + arg0);
            }

            @Override
            public void onCoreInitFinished() {
            }
        };
        QbSdk.initX5Environment(this, null);
        // 非wifi网络条件下是否允许下载内核，默认为false(针对用户没有安装微信/手Q/QQ空间[无内核]的情况下)
        QbSdk.setDownloadWithoutWifi(true);
    }

}
