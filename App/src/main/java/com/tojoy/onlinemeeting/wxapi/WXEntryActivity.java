package com.tojoy.onlinemeeting.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.common.Services.EventBus.PayEvent;
import com.tojoy.common.Services.EventBus.ShareEvent;

import org.greenrobot.eventbus.EventBus;

import es.dmoral.toasty.Toasty;

/**
 * Created by jgq on 2017/12/14.
 * <p>
 * 微信的返回结果类 分享完成或失败后 返回App 会会调用的一个类 做相应的处理
 */

public class WXEntryActivity extends Activity implements IWXAPIEventHandler {

    private static final String TAG = "MicroMsg.SDKSample.WXEntryActivity";

    public static final String PAYSUCCESS = "paysuccess";

    private IWXAPI api;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        api = WXAPIFactory.createWXAPI(this, AppConfig.WXPAY_APP_ID);
        try {
            api.handleIntent(getIntent(), this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq req) {
    }

    @Override
    public void onResp(BaseResp resp) {
        int type = resp.getType();
        Log.e("江国球", "paycallback2");
        LogUtil.e("tag", String.valueOf(type));
        //微信支付
        if (resp.getType() == ConstantsAPI.COMMAND_SENDMESSAGE_TO_WX) {//分享

            switch (resp.errCode) {
                case BaseResp.ErrCode.ERR_OK:
                    //分享成功;
                    //Toasty.normal(WXEntryActivity.this, "分享成功").show();
                    EventBus.getDefault().post(new ShareEvent(true, ""));
                    finish();
                    break;

                case BaseResp.ErrCode.ERR_USER_CANCEL:
                    //取消分享;
                    // Toasty.normal(WXEntryActivity.this, "取消分享").show();
                    finish();
                    break;

                case BaseResp.ErrCode.ERR_SENT_FAILED:
                    //分享失败;
                    Toasty.normal(WXEntryActivity.this, "分享失败").show();
                    finish();
                    break;
                default:
                    //未知原因;
                    finish();
                    break;
            }
        } else if (resp.getType() == ConstantsAPI.COMMAND_LAUNCH_WX_MINIPROGRAM) {
            //暂不处理小程序返回数据
            WXLaunchMiniProgram.Resp launchMiniProResp = (WXLaunchMiniProgram.Resp) resp;
            String extraData = launchMiniProResp.extMsg; // 对应下面小程序中的app-parameter字段的value
//            if (!TextUtils.isEmpty(extraData)) {
//                try {
//                    MiniGramPayEvent payEvent = new Gson().fromJson(extraData, MiniGramPayEvent.class);
//                    EventBus.getDefault().post(payEvent);
//                } catch (Exception e) {
//
//                }
//            }
            AppConfig.hasSkipToMini = true;
            EventBus.getDefault().post(new PayEvent(true));
            finish();
        }

    }


}
