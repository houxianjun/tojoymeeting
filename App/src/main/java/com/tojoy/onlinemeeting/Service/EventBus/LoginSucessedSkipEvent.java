package com.tojoy.onlinemeeting.Service.EventBus;

/**
 * @author fanxi
 * @date 2019/9/12.
 * description：
 */
public class LoginSucessedSkipEvent {
    //跳转type分类
    public  int type;

    public LoginSucessedSkipEvent(int type) {
        this.type = type;
    }

    public LoginSucessedSkipEvent() {
    }
}
