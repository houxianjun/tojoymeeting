package com.tojoy.onlinemeeting.Service.Push;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.igexin.sdk.GTIntentService;
import com.igexin.sdk.message.GTCmdMessage;
import com.igexin.sdk.message.GTNotificationMessage;
import com.igexin.sdk.message.GTTransmitMessage;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;
import com.tojoy.common.Services.Router.PushRouter;
import com.tojoy.tjoybaselib.util.time.TimeUtil;

import rx.Observer;

/**
 * Created by luuzhu on 2018/8/27 0027.
 * 继承 GTIntentService 接收来自个推的消息, 所有消息在线程中回调, 如果注册了该服务, 则务必要在 AndroidManifest中声明, 否则无法接受消息
 * onReceiveMessageData 处理透传消息
 * onReceiveClientId 接收 cid
 * onReceiveOnlineState cid 离线上线通知
 * onReceiveCommandResult 各种事件处理回执
 */

public class GTPushIntentService extends GTIntentService {

    @Override
    public void onReceiveServicePid(Context context, int pid) {
    }

    @Override
    public void onReceiveMessageData(Context context, GTTransmitMessage msg) {

        String jsonData = new String(msg.getPayload());
       // Log.d("push-message", "招商云应用内消息  message="+jsonData);
       // LogUtil.v("message-change","push-message 招商云应用内消息"+ TimeUtil.getLoneTimeStr(System.currentTimeMillis()));

        if (!TextUtils.isEmpty(jsonData) && jsonData.startsWith("{") && jsonData.endsWith("}")) {

             PushNotificationManager.getInstance().showNotification(context,jsonData);
        }
    }

    @Override
    public void onReceiveClientId(Context context, String clientid) {
        UserInfoManager.saveClientId(context, clientid);
       // Log.e("TAG", "clientid   =  " + clientid);
        //clientid传给后台
        OMAppApiProvider.getInstance().pushBind(clientid, new Observer<OMBaseResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(OMBaseResponse baseResponse) {
            }
        });
    }

    @Override
    public void onReceiveOnlineState(Context context, boolean online) {
    }

    @Override
    public void onReceiveCommandResult(Context context, GTCmdMessage cmdMessage) {
    }

    @Override
    public void onNotificationMessageArrived(Context context, GTNotificationMessage msg) {
    }

    @Override
    public void onNotificationMessageClicked(Context context, GTNotificationMessage msg) {
    }
}
