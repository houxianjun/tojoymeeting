package com.tojoy.onlinemeeting.Service.Push;

/**
 * Created by luuzhu on 2018/7/31 0031.
 */

public class GTPushBean {


    /**
     * action : nativepage
     * scheme : {"detail":"02aaaa419d9b4c44b0e6c2eb03288f4a","module":"dynamic"}
     */

    public String action;
    public SchemeBean scheme;
    public String content;
    public String title;

    public static class SchemeBean {
        /**
         * detail : 02aaaa419d9b4c44b0e6c2eb03288f4a
         * module : dynamic
         */
        public String detail;
        public String module;
    }
}
