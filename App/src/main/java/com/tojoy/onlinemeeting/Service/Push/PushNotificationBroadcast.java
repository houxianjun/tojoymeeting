package com.tojoy.onlinemeeting.Service.Push;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.tojoy.common.Services.Router.PushNotificationRouter;


/**
 * @author houxianjun
 */
public class PushNotificationBroadcast extends BroadcastReceiver {
    public static final String EXTRA_KEY_ACTION = "ACTION";
    public static final String EXTRA_KEY_MSG = "MSG";
    public static final int ACTION_CLICK = 10;
    public static final int ACTION_DISMISS = 11;
    public static final int EXTRA_ACTION_NOT_EXIST = -1;
    private static final String TAG = PushNotificationBroadcast.class.getName();
    @Override
    public void onReceive(Context context, Intent intent) {
        String message = intent.getStringExtra(EXTRA_KEY_MSG);
        int action = intent.getIntExtra(EXTRA_KEY_ACTION,
                EXTRA_ACTION_NOT_EXIST);
        try {
            switch (action) {
                case ACTION_DISMISS:
                    Log.d(TAG, "dismiss notification");
                    break;
                case ACTION_CLICK:
                    Log.d(TAG, "click notification");
                    try {
                        GTPushBean pushBean = new Gson().fromJson(message, GTPushBean.class);
                        try {
                            PushNotificationRouter.doPushJumpOnline(context, pushBean.title, pushBean.scheme!=null?pushBean.scheme.module:"", pushBean.scheme!=null?pushBean.scheme.detail:"", pushBean.content, pushBean.action);
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                    catch (Exception ex){
                        Log.d(TAG, "接收到的消息，解析时报错"+ex.getMessage());
                    }
                    break;
                default:
                    break;
            }
        }  catch (Exception e) {
            e.printStackTrace();
        }
    }

}
