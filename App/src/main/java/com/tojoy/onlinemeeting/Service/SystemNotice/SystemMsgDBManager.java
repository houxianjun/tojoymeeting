package com.tojoy.onlinemeeting.Service.SystemNotice;

import android.content.Context;
import android.text.TextUtils;

import com.netease.nim.uikit.api.model.user.SystemNotice;
import com.netease.nim.uikit.greendao.SystemNoticeDao;
import com.tojoy.onlinemeeting.App.Application.App;
import com.tojoy.onlinemeeting.NIM.reminder.ReminderManager;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;
import com.tojoy.common.Services.Push.SystemNoticeType;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

public class SystemMsgDBManager {

    public static final int PER_PAGE_COUNT = 10;
    private static SystemMsgDBManager mInterface;

    public static SystemMsgDBManager getInterface() {
        if (mInterface == null) {
            synchronized (SystemMsgDBManager.class) {
                mInterface = new SystemMsgDBManager();
            }
        }
        return mInterface;
    }

    /**
     * 插入一条新消息
     *
     * @param systemNotice
     */
    public void insertNewNotice(SystemNotice systemNotice) {

        if (TextUtils.isEmpty(systemNotice.userId)) {
            systemNotice.userId = UserInfoManager.getUserId(App.getInstance());
        }

        if (TextUtils.isEmpty(systemNotice.id)) {
            systemNotice.id = String.valueOf(System.currentTimeMillis());
        }

        systemNotice.userId = UserInfoManager.removeIMHeader(App.getInstance(), systemNotice.userId);
        App.getInstance().getDaoSession().getSystemNoticeDao().insertOrReplace(systemNotice);
    }


    /**
     * 获取全部类型的未读消息数目
     *
     * @return
     */
    public int getUnReadNoticeCnt(Context context) {
        SystemNoticeDao systemNoticeDao = App.getInstance().getDaoSession().getSystemNoticeDao();
        QueryBuilder qb = systemNoticeDao.queryBuilder();
        qb.where(SystemNoticeDao.Properties.UserId.eq(UserInfoManager.getUserId(context)));
        qb.where(SystemNoticeDao.Properties.IsRead.eq(false));
        return qb.list().size();
    }


    /**
     * ************************************ 获取系统消息数目
     *
     * @return
     */
    public int getUnReadNoticeCntByType(Context context, String type) {
        SystemNoticeDao systemNoticeDao = App.getInstance().getDaoSession().getSystemNoticeDao();
        QueryBuilder qb = systemNoticeDao.queryBuilder();
        qb.where(SystemNoticeDao.Properties.UserId.eq(UserInfoManager.getUserId(context)));
        qb.where(SystemNoticeDao.Properties.Type.eq(type));
        qb.where(SystemNoticeDao.Properties.IsRead.eq(false));
        List<SystemNotice> notices = qb.list();
        return notices.size();
    }

    /**
     * 获取系统消息列表
     *
     * @return
     */
    public List<SystemNotice> getNoticeListByType(Context context, int page, String type) {
        SystemNoticeDao systemNoticeDao = App.getInstance().getDaoSession().getSystemNoticeDao();
        QueryBuilder qb = systemNoticeDao.queryBuilder();

        qb.where(SystemNoticeDao.Properties.UserId.eq(UserInfoManager.getUserId(context)));
        qb.where(SystemNoticeDao.Properties.Type.eq(type));
        qb.offset((page - 1) * PER_PAGE_COUNT);
        qb.limit(PER_PAGE_COUNT);
        qb.orderDesc(SystemNoticeDao.Properties.Timestamp);

        return qb.list();
    }


    /**
     * 更新某类型下的消息为已读
     */
    public void updateNoticeReadedByType(Context context, String type) {
        try {
            String sql = "update SYSTEM_NOTICE set IS_READ = 1 where  USER_ID = " + UserInfoManager.getUserId(context);
            App.getInstance().getDataBase().execSQL(sql);
        } catch (Exception e) {

        }
        //更新小红点
        refreshRedTipCount();
    }


    private void refreshRedTipCount() {
        int unreadSystemMsgNum = SystemMsgDBManager.getInterface().getUnReadNoticeCntByType(App.getInstance(), SystemNoticeType.SYSTEM_NOTICE);
        ReminderManager.getInstance().updateSessionUnreadNum(unreadSystemMsgNum);
    }
}
