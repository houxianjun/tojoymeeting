package com.tojoy.onlinemeeting.Service.Address.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.intentionalorder.makesure.AddressDetailModel;
import com.tojoy.tjoybaselib.model.user.AddressResponseInfo;
import com.tojoy.tjoybaselib.model.user.CityModel;
import com.tojoy.tjoybaselib.model.user.CountyModel;
import com.tojoy.tjoybaselib.model.user.MyAddressListResponse;
import com.tojoy.tjoybaselib.model.user.ProvinceModel;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.net.OMBaseResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.util.string.RegixUtil;
import com.tojoy.tjoybaselib.util.sys.KeyboardControlUtil;
import com.tojoy.onlinemeeting.R;
import com.tojoy.common.Widgets.AddressPicker.NewAddreassPickview;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;
import com.tojoy.common.Services.EventBus.PublicStringEvent;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import rx.Observer;

@Route(path = RouterPathProvider.APP_ADD_ADDRESS)
public class AddAddressActivity extends UI {

    public static final String ADDRESS_EDIT = "ADDRESS_EDIT";
    public static final String ADDRESS_ADD = "ADDRESS_ADD";

    @BindView(R.id.ed_add_name)
    EditText mEdAddName;

    @BindView(R.id.ed_add_contact)
    EditText mEdAddContact;

    @BindView(R.id.ed_add_address)
    TextView mEdAddAddress;

    @BindView(R.id.ed_add_address_detail)
    EditText mEdAddAddressDetail;

    @BindView(R.id.bt_switch)
    ImageView mBtSwitch;

    @BindView(R.id.rlv_add_btn)
    RelativeLayout mRlvAddAddressBtn;

    @BindView(R.id.rlv_add_address)
    RelativeLayout mRlvAddAddress;

    String mProvinceId, mCityId, mAreaId, mType, mId, mFrom;

    ProvinceModel provinceModel;
    CityModel cityModel;
    CountyModel countyModel;

    boolean isDefault = true;

    NewAddreassPickview addreassPickview;

    public static void start(Context context, String type,
                             String id,
                             String pkId,
                             String proId,
                             String cityId,
                             String areaId,
                             String userName,
                             String mobile,
                             String isDefault,
                             String remark) {
        Intent intent = new Intent();
        intent.setClass(context, AddAddressActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("type", type);
        intent.putExtra("pkId", pkId);
        intent.putExtra("id", id);
        intent.putExtra("proId", proId);
        intent.putExtra("cityId", cityId);
        intent.putExtra("areaId", areaId);
        intent.putExtra("userName", userName);
        intent.putExtra("mobile", mobile);
        intent.putExtra("isDefault", isDefault);
        intent.putExtra("remark", remark);
        context.startActivity(intent);
    }

    public static void start(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, AddAddressActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address_layout);
        ButterKnife.bind(this);
        mType = getIntent().getStringExtra("type");
        mId = getIntent().getStringExtra("id");
        mFrom = getIntent().getStringExtra("from");

        setStatusBar();
        initView();
        initListener();
        setSwipeBackEnable(false);

        if (ADDRESS_EDIT.equals(mType)) {
            setTitle("编辑地址");
            mProvinceId = getIntent().getStringExtra("proId");
            mCityId = getIntent().getStringExtra("cityId");
            mAreaId = getIntent().getStringExtra("areaId");
            mEdAddName.setText(getIntent().getStringExtra("userName"));
            mEdAddName.setSelection(getIntent().getStringExtra("userName").length());
            mEdAddContact.setText(getIntent().getStringExtra("mobile"));
            mEdAddAddressDetail.setText(getIntent().getStringExtra("remark"));
            isDefault = Integer.parseInt(getIntent().getStringExtra("isDefault")) == 1;
            mEdAddAddress.setText(addreassPickview.setSelected(true, mProvinceId, mCityId, mAreaId));
        }

        refreshDefault();
        findViewById(R.id.rlv_set_default).setOnClickListener(v -> {
            isDefault = !isDefault;
            refreshDefault();
        });
    }

    private void initView() {
        setUpNavigationBar();
        showBack();
        setTitle("收货地址");
    }

    private void refreshDefault() {
        findViewById(R.id.bt_switch).setBackgroundResource(isDefault ? R.drawable.bg_default_check : R.drawable.bg_default_nocheck);
    }

    boolean isFirst = true;

    private void initListener() {
        if (addreassPickview == null) {

            if (TextUtils.isEmpty(UserInfoManager.getAddress(this))) {
                return;
            }


            addreassPickview = new NewAddreassPickview(this);
            addreassPickview.setSelectAreaCallback((provinceModel, cityModel, countyModel) -> {
                this.provinceModel = provinceModel;
                this.cityModel = cityModel;
                this.countyModel = countyModel;
                this.mEdAddAddress.setText(provinceModel.disName + " " + (cityModel == null ? "" : cityModel.disName) + " " + (countyModel == null ? "" : countyModel.disName));
            });
        }

        mRlvAddAddress.setOnClickListener(view -> {
            KeyboardControlUtil.hideKeyboard(mEdAddName);
            KeyboardControlUtil.hideKeyboard(mEdAddContact);
            KeyboardControlUtil.hideKeyboard(mEdAddAddressDetail);
            addreassPickview.show();

            if (ADDRESS_EDIT.equals(mType) && isFirst) {
                isFirst = false;
                addreassPickview.setSelected(false, mProvinceId, mCityId, mAreaId);
            }
        });

        mRlvAddAddressBtn.setOnClickListener(view -> {
            if (TextUtils.isEmpty(mEdAddName.getText().toString().trim())) {
                Toasty.normal(this, "请输入收货人姓名").show();
                return;
            }

            if (TextUtils.isEmpty(mEdAddContact.getText().toString().trim())) {
                Toasty.normal(this, "请输入手机号").show();
                return;
            }

            String mobileNum = mEdAddContact.getText().toString();
            if (mobileNum.length() != 11 || !RegixUtil.checkMobile(mobileNum)) {
                Toasty.normal(this, "请输入正确手机号").show();
                return;
            }

            if (provinceModel == null || cityModel == null || countyModel == null) {
                Toasty.normal(this, "请选择所在省市区").show();
                return;
            }

            if (TextUtils.isEmpty(mEdAddAddressDetail.getText().toString().trim())) {
                Toasty.normal(this, "请输入详细地址").show();
                return;
            }
            if (ADDRESS_EDIT.equals(mType)) {
                upAddress();
            } else {
                addAdress();
            }
        });


    }

    private void upAddress() {
        showProgressHUD(this, getResources().getString(R.string.refreshing));
        OMAppApiProvider.getInstance().editAddress(
                getIntent().getStringExtra("pkId"),
                BaseUserInfoCache.getUserId(this),
                provinceModel.id,
                provinceModel.disName,
                cityModel.id,
                cityModel.disName,
                countyModel.id,
                countyModel.disName,
                mEdAddName.getText().toString().trim(),
                "1",
                mEdAddContact.getText().toString().trim(),
                mEdAddAddressDetail.getText().toString().trim(),
                isDefault ? "1" : "0",
                new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        if (response.isSuccess()) {
                            Toasty.normal(AddAddressActivity.this, "编辑成功").show();
                            EventBus.getDefault().post(new PublicStringEvent(PublicStringEvent.ADD_ADDRESS_SUCCESS));
                            finish();
                        } else {
                            Toasty.normal(AddAddressActivity.this, response.msg).show();
                        }
                    }
                }
        );
    }

    private void addAdress() {
        showProgressHUD(this, getResources().getString(R.string.refreshing));
        OMAppApiProvider.getInstance().addAddress(
                BaseUserInfoCache.getUserId(this),
                provinceModel.id,
                provinceModel.disName,
                cityModel.id,
                cityModel.disName,
                countyModel.id,
                countyModel.disName,
                mEdAddName.getText().toString().trim(),
                "1",
                mEdAddContact.getText().toString().trim(),
                mEdAddAddressDetail.getText().toString().trim(),
                isDefault ? "1" : "0",
                new Observer<OMBaseResponse>() {
                    @Override
                    public void onCompleted() {
                        dismissProgressHUD();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(OMBaseResponse response) {
                        if (response.isSuccess()) {
                            Toasty.normal(AddAddressActivity.this, "添加成功").show();

                            if (mFrom != null && "2".equals(mFrom)) {//来自下单页面
                                OMAppApiProvider.getInstance().getMyAddressList(1, UserInfoManager.getUserId(AddAddressActivity.this), new Observer<MyAddressListResponse>() {
                                    @Override
                                    public void onCompleted() {
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                    }

                                    @Override
                                    public void onNext(MyAddressListResponse baseResponse) {
                                        if (baseResponse.isSuccess()) {
                                            if (!baseResponse.data.list.isEmpty()) {
                                                AddressResponseInfo data = baseResponse.data.list.get(0);
                                                AddressDetailModel addressDetailModel = new AddressDetailModel();
                                                addressDetailModel.pkId = data.pkId;
                                                addressDetailModel.provinceName = data.provinceName;
                                                addressDetailModel.userName = data.userName;
                                                addressDetailModel.userMobile = data.userMobile;
                                                addressDetailModel.remark = data.remark;
                                                addressDetailModel.cityName = data.cityName;
                                                addressDetailModel.districtName = data.districtName;

                                                EventBus.getDefault().post(addressDetailModel);
                                                finish();
                                            }
                                        }
                                    }
                                });
                            } else {
                                EventBus.getDefault().post(new PublicStringEvent(PublicStringEvent.ADD_ADDRESS_SUCCESS));
                                finish();
                            }
                        } else {
                            Toasty.normal(AddAddressActivity.this, response.msg).show();
                        }
                    }
                }
        );
    }


}
