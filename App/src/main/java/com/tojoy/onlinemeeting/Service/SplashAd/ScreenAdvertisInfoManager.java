package com.tojoy.onlinemeeting.Service.SplashAd;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;

import com.tojoy.tjoybaselib.util.storage.PreferencesUtil;
import com.tojoy.tjoybaselib.model.home.ScreenAdvertisResponse;
import com.tojoy.tjoybaselib.configs.OSSConfig;

import java.io.File;

/**
 * Created by chengyanfang on 2017/12/12.
 */

public class ScreenAdvertisInfoManager {

    public static final String USER_INFO = "screenadvertis_info";
    public static PreferencesUtil mScreenAdvertisInfoUtil = null;
    //保存广告图片的文件夹名
    public static String AD_IMAGE_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "dearxy";

    /**
     * Preferences文件
     */
    private static PreferencesUtil getPreferencesUtil(Context context) {
        if (mScreenAdvertisInfoUtil == null) {
            mScreenAdvertisInfoUtil = new PreferencesUtil(context, USER_INFO);
        }
        return mScreenAdvertisInfoUtil;
    }

    //保存开屏广告信息
    public static void setmScreenAdvertisInfo(Context context, ScreenAdvertisResponse.ScreenAdvertisImageInfo screenAdvertisImageInfo) {
        mScreenAdvertisInfoUtil = getPreferencesUtil(context);
        mScreenAdvertisInfoUtil.putString("img_url", OSSConfig.OOSURL + screenAdvertisImageInfo.imgUrl);
        mScreenAdvertisInfoUtil.putString("adLinkUrl", screenAdvertisImageInfo.adLinkUrl);
        mScreenAdvertisInfoUtil.putString("adLinkId", screenAdvertisImageInfo.adLinkId);
        mScreenAdvertisInfoUtil.putString("adLinkLiveId", screenAdvertisImageInfo.adLinkLiveId);
        mScreenAdvertisInfoUtil.putString("img_file_name", screenAdvertisImageInfo.imgUrl);
        mScreenAdvertisInfoUtil.putString("adLinkType", screenAdvertisImageInfo.adLinkType);
        mScreenAdvertisInfoUtil.putString("isPw", screenAdvertisImageInfo.isPw);
        mScreenAdvertisInfoUtil.putString("adLinkKey", screenAdvertisImageInfo.adLinkKey);
        mScreenAdvertisInfoUtil.putString("img_name", screenAdvertisImageInfo.adName);
        mScreenAdvertisInfoUtil.putLong("endTime", Long.valueOf(screenAdvertisImageInfo.adTimeEnd));
    }

    public static String getImageLink(Context context) {
        mScreenAdvertisInfoUtil = getPreferencesUtil(context);
        return mScreenAdvertisInfoUtil.getString("adLinkUrl");
    }

    public static String getImageUrl(Context context) {
        mScreenAdvertisInfoUtil = getPreferencesUtil(context);
        return mScreenAdvertisInfoUtil.getString("img_url");
    }

    public static String getImageFileName(Context context) {
        mScreenAdvertisInfoUtil = getPreferencesUtil(context);
        return mScreenAdvertisInfoUtil.getString("img_file_name");
    }

    public static String getImageJumpType(Context context) {
        mScreenAdvertisInfoUtil = getPreferencesUtil(context);
        return mScreenAdvertisInfoUtil.getString("adLinkType");
    }

    public static String getImageElementKey(Context context) {
        mScreenAdvertisInfoUtil = getPreferencesUtil(context);
        return mScreenAdvertisInfoUtil.getString("adLinkKey");
    }

    public static String getImageelColumnId(Context context) {
        mScreenAdvertisInfoUtil = getPreferencesUtil(context);
        return mScreenAdvertisInfoUtil.getString("columnId");
    }

    public static String getImageAdLinkId(Context context) {
        mScreenAdvertisInfoUtil = getPreferencesUtil(context);
        return mScreenAdvertisInfoUtil.getString("adLinkId");
    }

    public static String getImageAdLinkLiveId(Context context) {
        mScreenAdvertisInfoUtil = getPreferencesUtil(context);
        return mScreenAdvertisInfoUtil.getString("adLinkLiveId");
    }

    public static boolean getImageIsPw(Context context) {
        mScreenAdvertisInfoUtil = getPreferencesUtil(context);
        String isPw = mScreenAdvertisInfoUtil.getString("isPw");
        return !TextUtils.isEmpty(isPw) && "1".equals(isPw);
    }

    public static long getEndTime(Context context) {
        mScreenAdvertisInfoUtil = getPreferencesUtil(context);
        return mScreenAdvertisInfoUtil.getLong("endTime");
    }


}
