package com.tojoy.onlinemeeting.Service.BadgerManager;

import android.os.Handler;

import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.ResponseCode;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.tojoy.tjoybaselib.ui.badger.Badger;
import com.tojoy.onlinemeeting.App.Application.App;
import com.tojoy.onlinemeeting.Service.SystemNotice.SystemMsgDBManager;
import com.tojoy.common.Services.Push.SystemNoticeType;

import java.util.List;

/**
 * 更新条件：
 * <p>
 * 打开App
 * 收到系统消息
 * 监听收到新消息
 */
public class BadgerManager {

    private static BadgerManager instance;

    public static synchronized BadgerManager getInstance() {
        if (instance == null) {
            instance = new BadgerManager();
        }

        return instance;
    }

    public void updateBadger() {
        requestMessages();
    }

    /**
     * 获取聊天消息和系统消息
     */
    private static void requestMessages() {
        new Handler().postDelayed(() -> {
            // 查询最近联系人列表数据
            NIMClient.getService(MsgService.class).queryRecentContacts().setCallback(new RequestCallbackWrapper<List<RecentContact>>() {
                @Override
                public void onResult(int code, List<RecentContact> recents, Throwable exception) {
                    if (code != ResponseCode.RES_SUCCESS || recents == null) {
                        return;
                    }
                    refreshMessages(recents);
                }
            });

        }, 250);
    }

    private static void refreshMessages(List<RecentContact> recents) {
        int unreadNum = 0;
        for (RecentContact r : recents) {
            unreadNum += r.getUnreadCount();
        }

        //加上系统通知
        int unreadSystemMsgNum = SystemMsgDBManager.getInterface().getUnReadNoticeCntByType(App.getInstance(), SystemNoticeType.SYSTEM_NOTICE);
        unreadNum += unreadSystemMsgNum;
        Badger.updateBadgerCount(App.getInstance(), unreadNum);
    }

}
