package com.tojoy.onlinemeeting.Service.Push;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.tojoy.tjoybaselib.util.string.StringUtil;

/**
 * Created by luuzhu on 2018/7/26 0026.
 */

public class PushBean {
    /**
     * action : open_app
     * scheme : {"detail":"欢迎ANDROID光临","module":"open_app"}
     * msgId : 65476861
     */
    private String action;
    public String scheme;
    private String msgId;

    public String getModule() {
        Scheme bean = getScheme();
        if (bean == null) {
            return "";
        }
        return bean.module;
    }

    public String getAction() {

        if (StringUtil.isEmpty(action)) {
            return "";
        }

        return action;
    }

    public String getDetail() {
        Scheme bean = getScheme();
        if (bean == null) {
            return "";
        }
        return bean.detail;
    }

    private Scheme getScheme() {
        if (TextUtils.isEmpty(scheme)) {
            return null;
        }
        return new Gson().fromJson(scheme, Scheme.class);
    }

    private class Scheme {
        /**
         * detail : 欢迎ANDROID光临
         * module : open_app
         */
        public String detail;
        public String module;
    }
}
