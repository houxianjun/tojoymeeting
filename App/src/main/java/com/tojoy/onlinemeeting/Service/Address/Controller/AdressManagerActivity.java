package com.tojoy.onlinemeeting.Service.Address.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tojoy.tjoybaselib.configs.RouterPathProvider;
import com.tojoy.tjoybaselib.model.intentionalorder.makesure.AddressDetailModel;
import com.tojoy.tjoybaselib.model.user.MyAddressListResponse;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.ui.activity.UI;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.OnRefreshListener2;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.TJRecyclerView;
import com.tojoy.tjoybaselib.model.user.AddressResponseInfo;
import com.tojoy.onlinemeeting.Service.Address.View.AddressListAdapter;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;
import com.tojoy.onlinemeeting.R;
import com.tojoy.onlinemeeting.Service.Address.Helper.AddressCheckService;
import com.tojoy.common.Services.EventBus.PublicStringEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observer;

@Route(path = RouterPathProvider.App_AddressManager)
public class AdressManagerActivity extends UI {

    @BindView(R.id.rcy_daressmanager_recycle)
    TJRecyclerView mRecycleView;

    @BindView(R.id.rlv_add_btn)
    RelativeLayout mRlvAddBtn;

    ArrayList<AddressResponseInfo> mData = new ArrayList<>();

    AddressListAdapter mAdapter;

    int page = 1;

    public static void start(Context context, String from) {
        Intent intent = new Intent();
        intent.setClass(context, AdressManagerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("from", from);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adressmanager_layout);
        ButterKnife.bind(this);
        setStatusBar();
        initView();
        listMemberAddress();
        initRecycleView();
        if ("2".equals(getIntent().getStringExtra("from"))) {
            AddAddressActivity.start(this);
        }

        AddressCheckService.checkAddressVersion(this);
    }

    private void initRecycleView() {
        mAdapter = new AddressListAdapter(this, mData);
        mRecycleView.setAdapter(mAdapter);
        mRecycleView.setLoadMoreViewWithHide();
        mRecycleView.setEmptyImgAndTip(R.drawable.icon_noarrea, "您还没有添加收货地址");
        mRecycleView.setOnRefreshListener(new OnRefreshListener2() {
            @Override
            public void onRefresh() {
                page = 1;
                listMemberAddress();
            }

            @Override
            public void onLoadMore() {
                page++;
            }
        });

        mAdapter.setOnRecyclerViewItemClickListener((v, position, data) -> {
            if ("1".equals(getIntent().getStringExtra("from")) || "2".equals(getIntent().getStringExtra("from"))) {

                AddressDetailModel addressDetailModel = new AddressDetailModel();
                addressDetailModel.pkId = data.pkId;
                addressDetailModel.provinceName = data.provinceName;
                addressDetailModel.userName = data.userName;
                addressDetailModel.userMobile = data.userMobile;
                addressDetailModel.remark = data.remark;
                addressDetailModel.cityName = data.cityName;
                addressDetailModel.districtName = data.districtName;

                EventBus.getDefault().post(addressDetailModel);
                finish();
            }
        });

        mAdapter.editAddressCallback = pos -> AddAddressActivity.start(this,
                AddAddressActivity.ADDRESS_EDIT,
                mData.get(pos).id,
                mData.get(pos).pkId,
                mData.get(pos).provinceId,
                mData.get(pos).cityId,
                mData.get(pos).districtId,
                mData.get(pos).userName,
                mData.get(pos).userMobile,
                mData.get(pos).isDefault + "",
                mData.get(pos).remark);
    }

    private void listMemberAddress() {
        OMAppApiProvider.getInstance().getMyAddressList(page, UserInfoManager.getUserId(this), new Observer<MyAddressListResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(MyAddressListResponse baseResponse) {
                if (baseResponse.isSuccess()) {
                    if (page == 1) {
                        mData.clear();
                    }
                    mData.addAll(baseResponse.data.list);

                    if (mData.isEmpty()) {
                        findViewById(R.id.rlv_no_address).setVisibility(View.VISIBLE);
                    } else {
                        findViewById(R.id.rlv_no_address).setVisibility(View.GONE);
                    }

                    mRightManagerTv.setVisibility(View.VISIBLE);

                    mAdapter.updateData(mData);
                    mRecycleView.showFooterView();
                    mRecycleView.refreshLoadMoreView(mPage, baseResponse.data.list.size());
                }
            }
        });
    }

    @Override
    protected void onRightManagerTitleClick() {
        super.onRightManagerTitleClick();
        AddAddressActivity.start(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    private void initView() {
        setUpNavigationBar();
        showBack();
        setTitle("收货地址");
        setRightManagerTitle("添加新地址");
        mRightManagerTv.setVisibility(View.GONE);
        findViewById(R.id.rlv_add_btn).setOnClickListener(v -> onRightManagerTitleClick());
    }

    /**
     * EventBus
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPublicStringEvent(PublicStringEvent event) {
        //发布动态成功，刷新当前页面
        if (event.eventTitle.equals(PublicStringEvent.EDIT_ADDRESS_SUCCESS) || event.eventTitle.equals(PublicStringEvent.ADD_ADDRESS_SUCCESS)) {
            listMemberAddress();
        }

    }

}
