package com.tojoy.onlinemeeting.Service.Address.View;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewAdapter;
import com.tojoy.tjoybaselib.ui.tjrecyclerview.BaseRecyclerViewHolder;
import com.tojoy.tjoybaselib.model.user.AddressResponseInfo;
import com.tojoy.onlinemeeting.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddressListAdapter extends BaseRecyclerViewAdapter<AddressResponseInfo> {

    public AddressListAdapter(@NonNull Context context, @NonNull List<AddressResponseInfo> datas) {
        super(context, datas);
    }

    @Override
    protected int getViewType(int position, @NonNull AddressResponseInfo data) {
        return 0;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_address_list_layout;
    }

    @Override
    protected BaseRecyclerViewHolder getViewHolder(ViewGroup parent, View rootView, int viewType) {
        return new AddressListAdapterAdapterViewHolder(context, parent, getLayoutResId(viewType));
    }

    public interface EditAddressCallback {
        void onClickEdit(int pos);
    }

    public EditAddressCallback editAddressCallback;

    public class AddressListAdapterAdapterViewHolder extends BaseRecyclerViewHolder<AddressResponseInfo> {

        @BindView(R.id.tv_adress_name)
        TextView tvAdressName;

        @BindView(R.id.tv_first_name)
        TextView tvFirstName;

        @BindView(R.id.tv_adress_number)
        TextView tvAdressNumber;

        @BindView(R.id.tv_address_detail)
        TextView tvAddressDetail;

        @BindView(R.id.tv_adress_default)
        TextView tvAdressDefault;

        @BindView(R.id.tv_default)
        TextView tvDefaultTag;


        public AddressListAdapterAdapterViewHolder(Context context, ViewGroup viewGroup, int layoutResId) {
            super(context, viewGroup, layoutResId);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onBindData(AddressResponseInfo data, int position) {
            tvAdressName.setText(data.userName);
            tvFirstName.setText(data.userName.substring(0, 1));
            tvAdressNumber.setText(data.userMobile);

            tvDefaultTag.setVisibility(data.isDefault == 1 ? View.VISIBLE : View.GONE);
            if (data.isDefault == 0) {
                tvAddressDetail.setText(Html.fromHtml(data.provinceName + " " + data.cityName + " " + data.districtName + " " + data.remark));
            } else {
                tvAddressDetail.setText(Html.fromHtml("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + data.provinceName + " " + data.cityName + " " + data.districtName + " " + data.remark));
            }
            tvAdressDefault.setOnClickListener(v -> editAddressCallback.onClickEdit(position));
        }


    }
}
