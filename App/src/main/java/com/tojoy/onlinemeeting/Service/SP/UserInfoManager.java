package com.tojoy.onlinemeeting.Service.SP;

import android.content.Context;
import android.text.TextUtils;

import com.netease.nim.uikit.common.util.UserHeaderCache;
import com.tojoy.tjoybaselib.configs.AppConfig;
import com.tojoy.tjoybaselib.configs.OSSConfig;
import com.tojoy.tjoybaselib.configs.UserInfoConfig;
import com.tojoy.tjoybaselib.model.user.LoginResponse;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.util.storage.PreferencesUtil;

/**
 * @author chengyanfang
 * @version V 1.0.0
 * @function 用户信息、登录信息、开关信息 缓存
 */

public class UserInfoManager extends BaseUserInfoCache {
    public static final String USER_INFO = "userinfo_propertiesInfo";
    public static PreferencesUtil mInfoUtil = null;
    /**
     * Preferences文件
     */
    private static PreferencesUtil getPreferencesUtil(Context context) {
        if (mInfoUtil == null) {
            mInfoUtil = new PreferencesUtil(context, USER_INFO);
        }
        return mInfoUtil;
    }

    /**
     * 保存登录后用户信息
     */
    public static void setLoginUserInfo(Context context, LoginResponse.DataBean data) {

        mInfoUtil = getPreferencesUtil(context);

        mInfoUtil.putString("token", data.token);
        mInfoUtil.putString("userId", data.user_id);
        mInfoUtil.putString("trueName", data.trueName);
        mInfoUtil.putString("access_token",data.token_type+ " " + data.access_token);


        mInfoUtil.putString("avatar", OSSConfig.getOSSURLedStr(data.avatar));
        UserHeaderCache.saveUserHeaderToCache(context, addIMHeader(context, data.userId), OSSConfig.getOSSURLedStr(data.avatar), false);


        mInfoUtil.putString("userMobile", data.mobile);

        mInfoUtil.putString("roomId", data.roomId);

        mInfoUtil.putString("roomCode",data.roomCode);

        //公司相关
        mInfoUtil.putString("companyName", data.companyName);
        mInfoUtil.putString("companyCode", data.companyCode);
        mInfoUtil.putString("job", data.job);

        //权限相关
        UserInfoConfig.checkAuth = "1".equals(data.checkAuth);

        mInfoUtil.putString("checkAuth", data.checkAuth);

        syncIMInfo(context, data.userId);
    }


    public static void saveUserInfo(Context context, LoginResponse.DataBean data) {
        mInfoUtil.putString("trueName", data.trueName);
        mInfoUtil.putString("userId", data.userId);
        mInfoUtil.putString("companyName", data.companyName);
        mInfoUtil.putString("roomId", data.roomId);
        mInfoUtil.putString("companyCode", data.companyCode);

        mInfoUtil.putString("userMobile", data.mobile);
        mInfoUtil.putString("roomCode",data.roomCode);

        //权限相关
        UserInfoConfig.checkAuth = "1".equals(data.checkAuth);

        mInfoUtil.putString("userBackRole", data.userBackRole);
        mInfoUtil.putString("userFrontRole", data.userFrontRole);
        mInfoUtil.putString("checkAuth", data.checkAuth);
        mInfoUtil.putString("avatar", OSSConfig.getOSSURLedStr(data.avatar));
        UserHeaderCache.saveUserHeaderToCache(context, addIMHeader(context, data.userId), OSSConfig.getOSSURLedStr(data.avatar), false);

        mInfoUtil.putInt("ifDistributor",data.ifDistributor);
        mInfoUtil.putInt("distributorLevel",data.distributorLevel);

        StringBuilder permiss = new StringBuilder();
        if (data.permissions != null && data.permissions.size() > 0) {
            for (int i = 0; i < data.permissions.size(); i++) {
                if (data.permissions.get(i) != null) {
                    permiss.append(data.permissions.get(i));
                    if (i != data.permissions.size()-1) {
                        permiss.append(",");
                    }
                }
            }
        }
        mInfoUtil.putString("permissions",permiss.toString());
        StringBuilder role = new StringBuilder();
        if (data.roles != null && data.roles.size() > 0) {
            for (int i = 0; i < data.roles.size(); i++) {
                if (data.roles.get(i) != null) {
                    role.append(data.roles.get(i));
                    if (i != data.roles.size()-1) {
                        role.append(",");
                    }
                }
            }
        }
        mInfoUtil.putString("roles",role.toString());

    }

    /**
     * 同步IM信息
     *
     * @param context
     * @param userId
     */
    private static void syncIMInfo(Context context, String userId) {
        UserHeaderCache.saveUserHeaderToCache(context, addIMHeader(context, userId), getUserHeaderPic(context), false);

    }

    public static void saveUserMobile(Context context, String mobile) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putString("userMobile", mobile);
    }

    public static void saveUserPhone(Context context, String mobile) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putString("userPhone", mobile);
    }

    public static String getUserPhone(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getString("userPhone");
    }


    public static boolean isHasCompany(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        String companyCode = mInfoUtil.getString("companyCode");
        return !TextUtils.isEmpty(companyCode) && !"null".equals(companyCode);
    }


    /**
     * ********************** 地址管理
     */
    public static int getAddressVersion(Context context) {
        mInfoUtil = getPreferencesUtil(context);
        return mInfoUtil.getInt("address_version", 0);
    }

    public static void saveAddressVersion(Context context, int version) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putInt("address_version", version);
    }

    public static void saveAddress(Context context, String address) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putString("address", address);
    }


    /**
     * 查看是否因为环境关系，强制登出
     */
    public static boolean needChangeLoginStateWithEvn(Context context) {
        mInfoUtil = getPreferencesUtil(context);

        if (TextUtils.isEmpty(mInfoUtil.getString("EVN"))) {
            mInfoUtil.putString("EVN", String.valueOf(AppConfig.getAppNowEnvriment()));
            return false;
        }

        if (!String.valueOf(AppConfig.getAppNowEnvriment()).equals(mInfoUtil.getString("EVN"))) {
            mInfoUtil.putString("EVN", String.valueOf(AppConfig.getAppNowEnvriment()));
            return true;
        }

        return false;
    }


    public static void saveAvatar(Context context, String avatar) {
        mInfoUtil = getPreferencesUtil(context);
        mInfoUtil.putString("avatar", avatar);
    }

}
