package com.tojoy.onlinemeeting.Service.Address.Model;

public class AddressResponseInfo {


    /**
     * addAll : 重庆重庆綦江县
     * addressInfo : 啦啦啦
     * areaId : 3591
     * cityId : 271
     * createMemberId : 10116
     * createTime : 1539600645000
     * editMemberId : 10116
     * editTime : 1539600645000
     * id : 10058
     * memberId : 10116
     * mobile : 15712826323
     * nstatus : 1
     * operateChannel : 3
     * operateIp : 10.31.155.177
     * provinceId : 23
     * receiptName : 江国球
     * state : 2
     */

    public String addAll;
    public String addressInfo;
    public String areaId;
    public String cityId;
    public String createMemberId;
    public String createTime;
    public String editMemberId;
    public String editTime;
    public String id;
    public String memberId;
    public String mobile;
    public String nstatus;
    public String operateChannel;
    public String operateIp;
    public String provinceId;
    public String receiptName;
    public String state;
}
