package com.tojoy.onlinemeeting.Service.Address.Helper;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.tojoy.tjoybaselib.net.OMAppApiProvider;
import com.tojoy.tjoybaselib.services.cache.BaseUserInfoCache;
import com.tojoy.tjoybaselib.model.user.AddressResponse;
import com.tojoy.onlinemeeting.Service.SP.UserInfoManager;

import rx.Observer;

/**
 * Created by chengyanfang on 2017/11/2.
 *
 * @function:检测省市县地区列表
 */

public class AddressCheckService {

    //新的版本号
    private static int newVersionCnt = 0;


    /**
     */
    public static void checkAddressVersion(Context context) {
        //先检查本地版本是否为0
        if (UserInfoManager.getAddressVersion(context) == 0) {
            newVersionCnt = 1;

        }

        if (!TextUtils.isEmpty(BaseUserInfoCache.getAddress(context))) {
            String addressJson = BaseUserInfoCache.getAddress(context);
            try {
                AddressResponse addressResponse = new Gson().fromJson(addressJson, AddressResponse.class);
                if (addressResponse != null &&
                        addressResponse.data != null &&
                        addressResponse.data.size() > 0) {
                    return;
                }
            } catch (Exception e) {

            }
        }

        downloadAddress(context);
    }

    /**
     * 下载新版本地区数据
     */
    private static void downloadAddress(Context context) {
        OMAppApiProvider.getInstance().getAddress(UserInfoManager.getUserId(context), new Observer<AddressResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(AddressResponse baseResponse) {
                if (baseResponse.isSuccess()) {
                    String addressJSON = new Gson().toJson(baseResponse);
                    UserInfoManager.saveAddress(context, addressJSON);
                    UserInfoManager.saveAddressVersion(context, newVersionCnt);
                }
            }
        });
    }
}
