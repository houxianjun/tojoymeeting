package com.tojoy.onlinemeeting.Service.EventBus;

/**
 * Created by chengyanfang on 2017/11/29.
 */

public class TakePhotoEventBus {

    public String imagePath;

    public String from;

    public TakePhotoEventBus(String imagePath, String from) {
        this.imagePath = imagePath;
        this.from = from;
    }

}
