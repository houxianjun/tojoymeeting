package com.tojoy.onlinemeeting.Service.Push;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;

import com.google.gson.Gson;
import com.tojoy.common.Services.Push.NotifyView;
import com.tojoy.onlinemeeting.R;

public class PushNotificationManager {
    private static class SingletonHolder {
        private static final PushNotificationManager INSTANCE = new PushNotificationManager();
    }
    private PushNotificationManager(){}
    public static final PushNotificationManager getInstance() {
        return SingletonHolder.INSTANCE;
    }
    public  void showNotification(Context context,String msg) {
        GTPushBean bean = new Gson().fromJson(msg, GTPushBean.class);
        int id =(int) System.currentTimeMillis();
        NotificationManager manager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // 通知渠道的id
            String channel = "tojoyPushChannel";
            // 用户可以看到的通知渠道的名字.
            CharSequence name = "系统通知";
            // 用户可以看到的通知渠道的描述
            String description = "系统通知";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(channel, name, importance);
            // 配置通知渠道的属性
            mChannel.setDescription(description);
            // 设置通知出现时的闪灯（如果 android 设备支持的话）
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.GREEN);
            // 设置通知出现时的震动（如果 android 设备支持的话）
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            //最后在notificationmanager中创建该通知渠道
            manager.createNotificationChannel(mChannel);
        }
        Notification notification = getNotification(context,bean.title,bean.content,0);
        PendingIntent clickPendingIntent = getClickPendingIntent(context, msg);
        PendingIntent dismissPendingIntent = getDismissPendingIntent(context, msg);
        notification.deleteIntent = dismissPendingIntent;
        notification.contentIntent = clickPendingIntent;
        manager.notify(id, notification);

    }
    private  Notification getNotification(Context context,String title, String text, int num) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification notification = new Notification.Builder(context)
                    .setSmallIcon(R.mipmap.icon_launcher)
                    .setNumber(num)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_VIBRATE)
                    .setTicker(text)
                    .setContentTitle(title)
                    .setChannelId("tojoyPushChannel")
                    .build();

            notification.contentView = NotifyView.getPushRemoteViews(context, title, text);

            return notification;
        } else {

            Notification.Builder mBuilder = new Notification.Builder(context);
            mBuilder.setContentTitle(title)
                    .setContentText(text)
                    .setTicker(text)
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.mipmap.icon_launcher)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setPriority(Notification.PRIORITY_HIGH);

            Notification notification = mBuilder.getNotification();
            notification.icon = R.mipmap.icon_launcher;
            notification.tickerText = text;
            notification.defaults = Notification.DEFAULT_VIBRATE;
            notification.flags = Notification.FLAG_AUTO_CANCEL;
            notification.number = num;
            return notification;
        }
    }
    public  PendingIntent getClickPendingIntent(Context context, String msg) {
        Intent clickIntent = new Intent();
        clickIntent.setClass(context, PushNotificationBroadcast.class);
        clickIntent.putExtra(PushNotificationBroadcast.EXTRA_KEY_MSG,
                msg);
        clickIntent.putExtra(PushNotificationBroadcast.EXTRA_KEY_ACTION,
                PushNotificationBroadcast.ACTION_CLICK);
        PendingIntent clickPendingIntent = PendingIntent.getBroadcast(context,
                (int) (System.currentTimeMillis()),
                clickIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        return clickPendingIntent;
    }

    public  PendingIntent getDismissPendingIntent(Context context, String msg) {
        Intent deleteIntent = new Intent();
        deleteIntent.setClass(context, PushNotificationBroadcast.class);
        deleteIntent.putExtra(PushNotificationBroadcast.EXTRA_KEY_MSG,
                msg);
        deleteIntent.putExtra(
                PushNotificationBroadcast.EXTRA_KEY_ACTION,
                PushNotificationBroadcast.ACTION_DISMISS);
        PendingIntent deletePendingIntent = PendingIntent.getBroadcast(context,
                (int) (System.currentTimeMillis() + 1),
                deleteIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        return deletePendingIntent;
    }

    public void clearAllNotification(Context context){
      
        NotificationManager mNotifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);;

        mNotifyManager.cancelAll();
    }

}
